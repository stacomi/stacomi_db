ARG postgresql_version=14
ARG postgis_version=3.2
FROM postgis/postgis:${postgresql_version}-${postgis_version}
RUN localedef -i fr_FR -c -f UTF-8 -A /usr/share/locale/locale.alias fr_FR.UTF-8
ENV LANG=fr_FR.utf8

# script de construction de la base
# attention doit commencer à 11 car il y a un 10_postgis.sh
#RUN ls /docker-entrypoint-initdb.d
COPY initdb/ /docker-entrypoint-initdb.d

# insert in the first ligne the connection to the right db
# note the 3 \ to have 1 in the final sql
RUN sed -i ' 1i\\\connect bd_contmig_nat' /docker-entrypoint-initdb.d/20_create_ref.sql
RUN sed -i ' 1i\\\connect bd_contmig_nat' /docker-entrypoint-initdb.d/30_create_test_schema.sql
RUN sed -i ' 1i\\\connect bd_contmig_nat' /docker-entrypoint-initdb.d/40_change_owner.sql
RUN sed -i ' 1i\\\connect bd_contmig_nat' /docker-entrypoint-initdb.d/50_create_user_1_schema.sql
RUN sed -i ' 1i\\\connect bd_contmig_nat' /docker-entrypoint-initdb.d/60_change_owner_user_1.sql