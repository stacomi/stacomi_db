-- see https://forgemia.inra.fr/stacomi/stacomi_db/-/issues/22


ALTER ROLE user_1 WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS ;


GRANT ALL ON SCHEMA user_1 TO group_stacomi;
ALTER TABLE user_1.t_operation_ope OWNER TO user_1;
ALTER TABLE user_1.t_lot_lot OWNER TO user_1;
ALTER TABLE user_1.t_bilanmigrationjournalier_bjo OWNER TO user_1;
ALTER TABLE user_1.t_bilanmigrationmensuel_bme OWNER TO user_1;
ALTER TABLE user_1.t_ouvrage_ouv OWNER TO user_1;
ALTER TABLE user_1.tg_dispositif_dis OWNER TO user_1;
ALTER TABLE user_1.tj_stationmesure_stm OWNER TO user_1;
ALTER TABLE user_1.t_dispositifcomptage_dic OWNER TO user_1;
ALTER TABLE user_1.t_dispositiffranchissement_dif OWNER TO user_1;
ALTER TABLE user_1.t_marque_mqe OWNER TO user_1;
ALTER TABLE user_1.t_operationmarquage_omq OWNER TO user_1;
ALTER TABLE user_1.t_station_sta OWNER TO user_1;
ALTER TABLE user_1.tj_actionmarquage_act OWNER TO user_1;
ALTER TABLE user_1.tj_caracteristiquelot_car OWNER TO user_1;
ALTER TABLE user_1.tj_coefficientconversion_coe OWNER TO user_1;
ALTER TABLE user_1.tj_conditionenvironnementale_env OWNER TO user_1;
ALTER TABLE user_1.tj_dfestdestinea_dtx OWNER TO user_1;
ALTER TABLE user_1.tj_dfesttype_dft OWNER TO user_1;
ALTER TABLE user_1.tj_pathologieconstatee_pco OWNER TO user_1;
ALTER TABLE user_1.tj_prelevementlot_prl OWNER TO user_1;
ALTER TABLE user_1.tj_tauxechappement_txe OWNER TO user_1;
ALTER TABLE user_1.ts_maintenance_main OWNER TO user_1;
ALTER TABLE user_1.ts_masque_mas OWNER TO user_1;
ALTER TABLE user_1.ts_masquecaracteristiquelot_mac OWNER TO user_1;
ALTER TABLE user_1.ts_masqueconditionsenvironnementales_mae OWNER TO user_1;
ALTER TABLE user_1.ts_masquelot_mal OWNER TO user_1;
ALTER TABLE user_1.ts_masqueope_mao OWNER TO user_1;
ALTER TABLE user_1.ts_masqueordreaffichage_maa OWNER TO user_1;
ALTER TABLE user_1.ts_taillevideo_tav OWNER TO user_1;
ALTER TABLE user_1.ts_taxonvideo_txv OWNER TO user_1;
REASSIGN OWNED BY user_1 TO group_stacomi;
