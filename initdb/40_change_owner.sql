-- see https://forgemia.inra.fr/stacomi/stacomi_db/-/issues/22


ALTER ROLE stacomi_test WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS ;
