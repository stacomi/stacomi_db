CREATE ROLE user_1 LOGIN PASSWORD 'user_1';
CREATE ROLE stacomi_test LOGIN PASSWORD 'stacomi_test';
--CREATE ROLE group_stacomi_sequence NOINHERIT NOLOGIN; -- no inherit ensures that the privileges cannot be passed to another group role created later
CREATE ROLE group_stacomi INHERIT NOLOGIN; -- group_stacomi will be able to inherit from privileges set in  group_stacomi_sequence
--GRANT group_stacomi_sequence TO group_stacomi;
GRANT group_stacomi TO stacomi_test;

CREATE DATABASE bd_contmig_nat;

\connect bd_contmig_nat
CREATE EXTENSION postgis;