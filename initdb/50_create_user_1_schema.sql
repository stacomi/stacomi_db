--
-- PostgreSQL database dump
--

-- Dumped from database version 14.0
-- Dumped by pg_dump version 14.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: user_1; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA user_1;


ALTER SCHEMA user_1 OWNER TO postgres;

--
-- Name: SCHEMA user_1; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA user_1 IS 'schema for new user';


--
-- Name: breakpoint; Type: TYPE; Schema: user_1; Owner: postgres
--

CREATE TYPE user_1.breakpoint AS (
	func oid,
	linenumber integer,
	targetname text
);


ALTER TYPE user_1.breakpoint OWNER TO postgres;

--
-- Name: dblink_pkey_results; Type: TYPE; Schema: user_1; Owner: postgres
--

CREATE TYPE user_1.dblink_pkey_results AS (
	"position" integer,
	colname text
);


ALTER TYPE user_1.dblink_pkey_results OWNER TO postgres;

--
-- Name: frame; Type: TYPE; Schema: user_1; Owner: postgres
--

CREATE TYPE user_1.frame AS (
	level integer,
	targetname text,
	func oid,
	linenumber integer,
	args text
);


ALTER TYPE user_1.frame OWNER TO postgres;

--
-- Name: proxyinfo; Type: TYPE; Schema: user_1; Owner: postgres
--

CREATE TYPE user_1.proxyinfo AS (
	serverversionstr text,
	serverversionnum integer,
	proxyapiver integer,
	serverprocessid integer
);


ALTER TYPE user_1.proxyinfo OWNER TO postgres;

--
-- Name: targetinfo; Type: TYPE; Schema: user_1; Owner: postgres
--

CREATE TYPE user_1.targetinfo AS (
	target oid,
	schema oid,
	nargs integer,
	argtypes oidvector,
	targetname name,
	argmodes "char"[],
	argnames text[],
	targetlang oid,
	fqname text,
	returnsset boolean,
	returntype oid
);


ALTER TYPE user_1.targetinfo OWNER TO postgres;

--
-- Name: var; Type: TYPE; Schema: user_1; Owner: postgres
--

CREATE TYPE user_1.var AS (
	name text,
	varclass character(1),
	linenumber integer,
	isunique boolean,
	isconst boolean,
	isnotnull boolean,
	dtype oid,
	value text
);


ALTER TYPE user_1.var OWNER TO postgres;

--
-- Name: do_not_delete_default(); Type: FUNCTION; Schema: user_1; Owner: postgres
--

CREATE FUNCTION user_1.do_not_delete_default() RETURNS trigger
    LANGUAGE plpgsql
    AS $$   
DECLARE
_mas_code character varying(15);
    BEGIN      
        IF OLD.mas_code = 'ope_defaut'  THEN
            RAISE EXCEPTION 'Vous ne pouvez pas supprimer le masque par défaut';
        END IF;
          IF OLD.mas_code = 'lot_defaut'  THEN
            RAISE EXCEPTION 'Vous ne pouvez pas supprimer le masque par défaut';
        END IF;
RETURN OLD;
    END;
$$;


ALTER FUNCTION user_1.do_not_delete_default() OWNER TO postgres;

--
-- Name: do_not_update_default(); Type: FUNCTION; Schema: user_1; Owner: postgres
--

CREATE FUNCTION user_1.do_not_update_default() RETURNS trigger
    LANGUAGE plpgsql
    AS $$   
DECLARE
_mas_code character varying(15);
    BEGIN      
     IF OLD.mas_code = 'ope_defaut'  THEN
            RAISE EXCEPTION 'Vous ne pouvez pas modifier le masque par défaut';
        END IF;
          IF OLD.mas_code = 'lot_defaut'  THEN
            RAISE EXCEPTION 'Vous ne pouvez pas modifier le masque par défaut';
        END IF;
RETURN NEW;
    END;
$$;


ALTER FUNCTION user_1.do_not_update_default() OWNER TO postgres;

--
-- Name: fct_coe_date(); Type: FUNCTION; Schema: user_1; Owner: postgres
--

CREATE FUNCTION user_1.fct_coe_date() RETURNS trigger
    LANGUAGE plpgsql
    AS $$   

  DECLARE nbChevauchements INTEGER ;

  BEGIN
    -- verification des non-chevauchements pour les taux
    SELECT COUNT(*) INTO nbChevauchements
    FROM   user_1.tj_coefficientconversion_coe
    WHERE  coe_tax_code = NEW.coe_tax_code
           AND coe_std_code = NEW.coe_std_code
           AND coe_qte_code = NEW.coe_qte_code
           AND (coe_date_debut, coe_date_fin) OVERLAPS (NEW.coe_date_debut, NEW.coe_date_fin)
    ;

    -- Comme le trigger est declenche sur AFTER et non pas sur BEFORE, il faut (nbChevauchements > 1) et non pas >0, car l enregistrement a deja ete ajoute, donc il se chevauche avec lui meme, ce qui est normal !
    IF (nbChevauchements > 1) THEN
      RAISE EXCEPTION 'Les taux de conversion ne peuvent se chevaucher.'  ;
    END IF  ;

    RETURN NEW ;
  END  ;
$$;


ALTER FUNCTION user_1.fct_coe_date() OWNER TO postgres;

--
-- Name: fct_ope_date(); Type: FUNCTION; Schema: user_1; Owner: postgres
--

CREATE FUNCTION user_1.fct_ope_date() RETURNS trigger
    LANGUAGE plpgsql
    AS $$   

  DECLARE disCreation     TIMESTAMP  ;
  DECLARE disSuppression    TIMESTAMP  ;
  DECLARE nbChevauchements  INTEGER    ;

  BEGIN
    -- Recuperation des dates du dispositif dans des variables
    SELECT dis_date_creation, dis_date_suppression INTO disCreation, disSuppression
    FROM   user_1.tg_dispositif_dis
    WHERE  dis_identifiant = NEW.ope_dic_identifiant
    ;

    -- verification de la date de debut
    IF ((NEW.ope_date_debut < disCreation) OR (NEW.ope_date_debut > disSuppression)) THEN
      RAISE EXCEPTION 'Le debut de l operation doit etre inclus dans la periode d existence du DC.'  ;
    END IF  ;

    -- verification de la date de fin
    IF ((NEW.ope_date_fin < disCreation) OR (NEW.ope_date_fin > disSuppression)) THEN
      RAISE EXCEPTION 'La fin de l operation doit etre incluse dans la periode d existence du DC.'  ;
    END IF  ;

    -- verification des non-chevauchements pour les operations du dispositif
    SELECT COUNT(*) INTO nbChevauchements
    FROM   user_1.t_operation_ope
    WHERE  ope_dic_identifiant = NEW.ope_dic_identifiant 
           AND (ope_date_debut, ope_date_fin) OVERLAPS (NEW.ope_date_debut, NEW.ope_date_fin)
    ;

    -- Comme le trigger est declenche sur AFTER et non pas sur BEFORE, il faut (nbChevauchements > 1) et non pas >0, car l enregistrement a deja ete ajoute, donc il se chevauche avec lui meme, ce qui est normal !
    IF (nbChevauchements > 1) THEN 
      RAISE EXCEPTION 'Les operations ne peuvent se chevaucher.'  ;
    END IF  ;

    RETURN NEW ;
  END  ;
$$;


ALTER FUNCTION user_1.fct_ope_date() OWNER TO postgres;

--
-- Name: fct_per_date(); Type: FUNCTION; Schema: user_1; Owner: postgres
--

CREATE FUNCTION user_1.fct_per_date() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ 

  DECLARE disCreation     TIMESTAMP  ;
  DECLARE disSuppression    TIMESTAMP  ;
  DECLARE nbChevauchements  INTEGER    ;

  BEGIN
    -- Recuperation des dates du dispositif dans des variables
    SELECT dis_date_creation, dis_date_suppression INTO disCreation, disSuppression
    FROM   user_1.tg_dispositif_dis
    WHERE  dis_identifiant = NEW.per_dis_identifiant
    ;

    -- verification de la date de debut
    IF ((NEW.per_date_debut < disCreation) OR (NEW.per_date_debut > disSuppression)) THEN
      RAISE EXCEPTION 'Le debut de la periode doit etre inclus dans la periode d existence du dispositif.'  ;
    END IF  ;

    -- verification de la date de fin
    IF ((NEW.per_date_fin < disCreation) OR (NEW.per_date_fin > disSuppression)) THEN
      RAISE EXCEPTION 'La fin de la periode doit etre incluse dans la periode d existence du dispositif.'  ;
    END IF  ;

    -- verification des non-chevauchements pour les periodes du dispositif
    SELECT COUNT(*) INTO nbChevauchements
    FROM   user_1.t_periodefonctdispositif_per
    WHERE  per_dis_identifiant = NEW.per_dis_identifiant 
           AND (per_date_debut, per_date_fin) OVERLAPS (NEW.per_date_debut, NEW.per_date_fin)
    ;

    -- Comme le trigger est declenche sur AFTER et non pas sur BEFORE, il faut (nbChevauchements > 1) et non pas >0, car l enregistrement a deja ete ajoute, donc il se chevauche avec lui meme, ce qui est normal !
    IF (nbChevauchements > 1) THEN
      RAISE EXCEPTION 'Les periodes ne peuvent se chevaucher.'  ;
    END IF  ;
    
    RETURN NEW ;
  END  ;
$$;


ALTER FUNCTION user_1.fct_per_date() OWNER TO postgres;

--
-- Name: fct_per_suppression(); Type: FUNCTION; Schema: user_1; Owner: postgres
--

CREATE FUNCTION user_1.fct_per_suppression() RETURNS trigger
    LANGUAGE plpgsql
    AS $$   

  BEGIN
    -- La periode precedent celle supprimee est prolongee
    -- jusqu a la fin de la periode supprimee
    UPDATE user_1.t_periodefonctdispositif_per 
    SET    per_date_fin = OLD.per_date_fin 
    WHERE  per_date_fin= OLD.per_date_debut 
           AND per_dis_identifiant = OLD.per_dis_identifiant 
    ;
    RETURN NEW ;
  END  ;

$$;


ALTER FUNCTION user_1.fct_per_suppression() OWNER TO postgres;

--
-- Name: fct_txe_date(); Type: FUNCTION; Schema: user_1; Owner: postgres
--

CREATE FUNCTION user_1.fct_txe_date() RETURNS trigger
    LANGUAGE plpgsql
    AS $$   

  DECLARE nbChevauchements INTEGER ;

  BEGIN
    -- verification des non-chevauchements pour les taux
    SELECT COUNT(*) INTO nbChevauchements
    FROM   user_1.tj_tauxechappement_txe
    WHERE  txe_sta_code = NEW.txe_sta_code
           AND txe_tax_code = NEW.txe_tax_code
           AND txe_std_code = NEW.txe_std_code
           AND (txe_date_debut, txe_date_fin) OVERLAPS (NEW.txe_date_debut, NEW.txe_date_fin)
    ;

    -- Comme le trigger est declenche sur AFTER et non pas sur BEFORE, il faut (nbChevauchements > 1) et non pas >0, car l enregistrement a deja ete ajoute, donc il se chevauche avec lui meme, ce qui est normal !
    IF (nbChevauchements > 1) THEN
      RAISE EXCEPTION 'Les taux d echappement ne peuvent se chevaucher.'  ;
    END IF  ;

    RETURN NEW ;

  END  ;
$$;


ALTER FUNCTION user_1.fct_txe_date() OWNER TO postgres;

--
-- Name: masqueope(); Type: FUNCTION; Schema: user_1; Owner: postgres
--

CREATE FUNCTION user_1.masqueope() RETURNS trigger
    LANGUAGE plpgsql
    AS $$   
DECLARE user_1 text;


  BEGIN
    -- Recuperation du type
    SELECT mas_type into user_1
    FROM   user_1.ts_masque_mas
    WHERE  mas_id = NEW.mao_mas_id
    ;

    -- verification 
    IF (user_1!='ope') THEN
      RAISE EXCEPTION 'Attention le masque n'' est pas un masque operation, changez le type de masque'  ;
    END IF  ;     

    RETURN NEW ;
  END  ;
$$;


ALTER FUNCTION user_1.masqueope() OWNER TO postgres;

--
-- Name: t_lot_lot_lot_identifiant_seq; Type: SEQUENCE; Schema: user_1; Owner: postgres
--

CREATE SEQUENCE user_1.t_lot_lot_lot_identifiant_seq
    START WITH 300000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_1.t_lot_lot_lot_identifiant_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: t_lot_lot; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.t_lot_lot (
    lot_identifiant integer DEFAULT nextval('user_1.t_lot_lot_lot_identifiant_seq'::regclass) NOT NULL,
    lot_ope_identifiant integer NOT NULL,
    lot_tax_code character varying(6) NOT NULL,
    lot_std_code character varying(4) NOT NULL,
    lot_effectif double precision,
    lot_quantite double precision,
    lot_qte_code character varying(4),
    lot_methode_obtention character varying(10) NOT NULL,
    lot_lot_identifiant integer,
    lot_dev_code character varying(4),
    lot_commentaires text,
    lot_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_lot_lot_identifiant CHECK ((lot_lot_identifiant <> lot_identifiant)),
    CONSTRAINT c_ck_lot_methode_obtention CHECK (((upper((lot_methode_obtention)::text) = 'MESURE'::text) OR (upper((lot_methode_obtention)::text) = 'CALCULE'::text) OR (upper((lot_methode_obtention)::text) = 'EXPERT'::text) OR (upper((lot_methode_obtention)::text) = 'PONCTUEL'::text)))
);


ALTER TABLE user_1.t_lot_lot OWNER TO user_1;

--
-- Name: t_operation_ope; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.t_operation_ope (
    ope_identifiant integer NOT NULL,
    ope_dic_identifiant integer NOT NULL,
    ope_date_debut timestamp(0) without time zone NOT NULL,
    ope_date_fin timestamp(0) without time zone NOT NULL,
    ope_organisme character varying(35),
    ope_operateur character varying(35),
    ope_commentaires text,
    ope_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_ope_date_fin CHECK (((ope_date_fin >= ope_date_debut) AND (date_part('month'::text, age(ope_date_fin, ope_date_debut)) <= (1)::double precision)))
);


ALTER TABLE user_1.t_operation_ope OWNER TO user_1;

--
-- Name: tj_caracteristiquelot_car; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.tj_caracteristiquelot_car (
    car_lot_identifiant integer NOT NULL,
    car_par_code character varying(5) NOT NULL,
    car_methode_obtention character varying(10),
    car_val_identifiant integer,
    car_valeur_quantitatif real,
    car_precision real,
    car_commentaires text,
    car_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_car CHECK ((((car_val_identifiant IS NOT NULL) AND (car_valeur_quantitatif IS NULL)) OR ((car_val_identifiant IS NULL) AND (car_valeur_quantitatif IS NOT NULL)))),
    CONSTRAINT c_ck_car_methode_obtention CHECK (((upper((car_methode_obtention)::text) = 'MESURE'::text) OR (upper((car_methode_obtention)::text) = 'CALCULE'::text) OR (upper((car_methode_obtention)::text) = 'EXPERT'::text)))
);


ALTER TABLE user_1.tj_caracteristiquelot_car OWNER TO user_1;

--
-- Name: vue_lot_ope_car_qan; Type: VIEW; Schema: user_1; Owner: postgres
--

CREATE VIEW user_1.vue_lot_ope_car_qan AS
 SELECT t_operation_ope.ope_identifiant,
    t_lot_lot.lot_identifiant,
    t_operation_ope.ope_dic_identifiant,
    t_lot_lot.lot_lot_identifiant AS lot_pere,
    t_operation_ope.ope_date_debut,
    t_operation_ope.ope_date_fin,
    t_lot_lot.lot_effectif,
    t_lot_lot.lot_quantite,
    t_lot_lot.lot_tax_code,
    tr_taxon_tax.tax_nom_latin,
    tr_stadedeveloppement_std.std_libelle,
    tr_devenirlot_dev.dev_code,
    tr_devenirlot_dev.dev_libelle,
    tg_parametre_par.par_nom,
    tj_caracteristiquelot_car.car_par_code,
    tj_caracteristiquelot_car.car_methode_obtention,
    tj_caracteristiquelot_car.car_val_identifiant,
    tj_caracteristiquelot_car.car_valeur_quantitatif,
    tr_valeurparametrequalitatif_val.val_libelle
   FROM (((((((((user_1.t_operation_ope
     JOIN user_1.t_lot_lot ON ((t_lot_lot.lot_ope_identifiant = t_operation_ope.ope_identifiant)))
     LEFT JOIN ref.tr_typequantitelot_qte ON (((tr_typequantitelot_qte.qte_code)::text = (t_lot_lot.lot_qte_code)::text)))
     LEFT JOIN ref.tr_devenirlot_dev ON (((tr_devenirlot_dev.dev_code)::text = (t_lot_lot.lot_dev_code)::text)))
     JOIN ref.tr_taxon_tax ON (((tr_taxon_tax.tax_code)::text = (t_lot_lot.lot_tax_code)::text)))
     JOIN ref.tr_stadedeveloppement_std ON (((tr_stadedeveloppement_std.std_code)::text = (t_lot_lot.lot_std_code)::text)))
     JOIN user_1.tj_caracteristiquelot_car ON ((tj_caracteristiquelot_car.car_lot_identifiant = t_lot_lot.lot_identifiant)))
     LEFT JOIN ref.tg_parametre_par ON (((tj_caracteristiquelot_car.car_par_code)::text = (tg_parametre_par.par_code)::text)))
     LEFT JOIN ref.tr_parametrequantitatif_qan ON (((tr_parametrequantitatif_qan.qan_par_code)::text = (tg_parametre_par.par_code)::text)))
     LEFT JOIN ref.tr_valeurparametrequalitatif_val ON ((tj_caracteristiquelot_car.car_val_identifiant = tr_valeurparametrequalitatif_val.val_identifiant)))
  ORDER BY t_operation_ope.ope_date_debut;


ALTER TABLE user_1.vue_lot_ope_car_qan OWNER TO postgres;

--
-- Name: vue_ope_lot_ech_parqual; Type: VIEW; Schema: user_1; Owner: postgres
--

CREATE VIEW user_1.vue_ope_lot_ech_parqual AS
 SELECT t_operation_ope.ope_identifiant,
    t_operation_ope.ope_dic_identifiant,
    t_operation_ope.ope_date_debut,
    t_operation_ope.ope_date_fin,
    lot.lot_identifiant,
    lot.lot_methode_obtention,
    lot.lot_effectif,
    lot.lot_quantite,
    lot.lot_tax_code,
    lot.lot_std_code,
    tr_taxon_tax.tax_nom_latin,
    tr_stadedeveloppement_std.std_libelle,
    dev.dev_code,
    dev.dev_libelle,
    par.par_nom,
    car.car_par_code,
    car.car_methode_obtention,
    car.car_val_identifiant,
    val.val_libelle,
    lot.lot_lot_identifiant AS lot_pere,
    lot_pere.lot_effectif AS lot_pere_effectif,
    lot_pere.lot_quantite AS lot_pere_quantite,
    dev_pere.dev_code AS lot_pere_dev_code,
    dev_pere.dev_libelle AS lot_pere_dev_libelle,
    parqual.par_nom AS lot_pere_par_nom,
    parqual.car_par_code AS lot_pere_par_code,
    parqual.car_methode_obtention AS lot_pere_car_methode_obtention,
    parqual.car_val_identifiant AS lot_pere_val_identifiant,
    parqual.val_libelle AS lot_pere_val_libelle
   FROM (((((((((((((user_1.t_operation_ope
     JOIN user_1.t_lot_lot lot ON ((lot.lot_ope_identifiant = t_operation_ope.ope_identifiant)))
     LEFT JOIN ref.tr_typequantitelot_qte qte ON (((qte.qte_code)::text = (lot.lot_qte_code)::text)))
     LEFT JOIN ref.tr_devenirlot_dev dev ON (((dev.dev_code)::text = (lot.lot_dev_code)::text)))
     JOIN ref.tr_taxon_tax ON (((tr_taxon_tax.tax_code)::text = (lot.lot_tax_code)::text)))
     JOIN ref.tr_stadedeveloppement_std ON (((tr_stadedeveloppement_std.std_code)::text = (lot.lot_std_code)::text)))
     JOIN user_1.tj_caracteristiquelot_car car ON ((car.car_lot_identifiant = lot.lot_identifiant)))
     LEFT JOIN ref.tg_parametre_par par ON (((car.car_par_code)::text = (par.par_code)::text)))
     JOIN ref.tr_parametrequalitatif_qal qal ON (((qal.qal_par_code)::text = (par.par_code)::text)))
     LEFT JOIN ref.tr_valeurparametrequalitatif_val val ON ((car.car_val_identifiant = val.val_identifiant)))
     LEFT JOIN user_1.t_lot_lot lot_pere ON ((lot_pere.lot_identifiant = lot.lot_lot_identifiant)))
     LEFT JOIN ref.tr_typequantitelot_qte qte_pere ON (((qte_pere.qte_code)::text = (lot_pere.lot_qte_code)::text)))
     LEFT JOIN ref.tr_devenirlot_dev dev_pere ON (((dev_pere.dev_code)::text = (lot_pere.lot_dev_code)::text)))
     LEFT JOIN ( SELECT car_pere.car_lot_identifiant,
            car_pere.car_par_code,
            car_pere.car_methode_obtention,
            car_pere.car_val_identifiant,
            car_pere.car_valeur_quantitatif,
            car_pere.car_precision,
            car_pere.car_commentaires,
            par_pere.par_code,
            par_pere.par_nom,
            par_pere.par_unite,
            par_pere.par_nature,
            par_pere.par_definition,
            qal_pere.qal_par_code,
            qal_pere.qal_valeurs_possibles,
            val_pere.val_identifiant,
            val_pere.val_qal_code,
            val_pere.val_rang,
            val_pere.val_libelle
           FROM (((user_1.tj_caracteristiquelot_car car_pere
             LEFT JOIN ref.tg_parametre_par par_pere ON (((car_pere.car_par_code)::text = (par_pere.par_code)::text)))
             JOIN ref.tr_parametrequalitatif_qal qal_pere ON (((qal_pere.qal_par_code)::text = (par_pere.par_code)::text)))
             LEFT JOIN ref.tr_valeurparametrequalitatif_val val_pere ON ((car_pere.car_val_identifiant = val_pere.val_identifiant)))) parqual ON ((parqual.car_lot_identifiant = lot_pere.lot_identifiant)));


ALTER TABLE user_1.vue_ope_lot_ech_parqual OWNER TO postgres;

--
-- Name: civelle_taille_poids_stade; Type: VIEW; Schema: user_1; Owner: postgres
--

CREATE VIEW user_1.civelle_taille_poids_stade AS
 WITH poids AS (
         SELECT vue_lot_ope_car_qan.lot_identifiant,
            vue_lot_ope_car_qan.car_valeur_quantitatif AS poids
           FROM user_1.vue_lot_ope_car_qan
          WHERE ("overlaps"(vue_lot_ope_car_qan.ope_date_debut, vue_lot_ope_car_qan.ope_date_fin, ('1996-08-01'::date)::timestamp without time zone, ('2016-08-01'::date)::timestamp without time zone) AND (vue_lot_ope_car_qan.ope_dic_identifiant = 6) AND ((vue_lot_ope_car_qan.std_libelle)::text = 'civelle'::text) AND (vue_lot_ope_car_qan.lot_effectif = (1)::double precision) AND (upper((vue_lot_ope_car_qan.car_methode_obtention)::text) = 'MESURE'::text) AND ((vue_lot_ope_car_qan.car_par_code)::text = 'A111'::text) AND (vue_lot_ope_car_qan.car_valeur_quantitatif IS NOT NULL))
        ), taille AS (
         SELECT vue_lot_ope_car_qan.lot_identifiant,
            vue_lot_ope_car_qan.car_valeur_quantitatif AS taille
           FROM user_1.vue_lot_ope_car_qan
          WHERE ("overlaps"(vue_lot_ope_car_qan.ope_date_debut, vue_lot_ope_car_qan.ope_date_fin, ('1996-08-01'::date)::timestamp without time zone, ('2016-08-01'::date)::timestamp without time zone) AND (vue_lot_ope_car_qan.ope_dic_identifiant = 6) AND ((vue_lot_ope_car_qan.std_libelle)::text = 'civelle'::text) AND (vue_lot_ope_car_qan.lot_effectif = (1)::double precision) AND (upper((vue_lot_ope_car_qan.car_methode_obtention)::text) = 'MESURE'::text) AND ((vue_lot_ope_car_qan.car_par_code)::text = '1786'::text) AND (vue_lot_ope_car_qan.car_valeur_quantitatif IS NOT NULL))
        ), stade AS (
         SELECT vue_ope_lot_ech_parqual.lot_identifiant,
            vue_ope_lot_ech_parqual.val_libelle AS stade
           FROM user_1.vue_ope_lot_ech_parqual
          WHERE ("overlaps"(vue_ope_lot_ech_parqual.ope_date_debut, vue_ope_lot_ech_parqual.ope_date_fin, ('1996-08-01'::date)::timestamp without time zone, ('2016-08-01'::date)::timestamp without time zone) AND (vue_ope_lot_ech_parqual.ope_dic_identifiant = 6) AND ((vue_ope_lot_ech_parqual.std_libelle)::text = 'civelle'::text) AND (vue_ope_lot_ech_parqual.lot_effectif = (1)::double precision) AND (upper((vue_ope_lot_ech_parqual.car_methode_obtention)::text) = 'MESURE'::text) AND ((vue_ope_lot_ech_parqual.car_par_code)::text = '1791'::text))
        ), joined_query AS (
         SELECT
                CASE
                    WHEN (poids.lot_identifiant IS NOT NULL) THEN poids.lot_identifiant
                    WHEN ((poids.lot_identifiant IS NULL) AND (stade.lot_identifiant IS NULL)) THEN taille.lot_identifiant
                    ELSE NULL::integer
                END AS lot_identifiant,
            taille.taille,
            poids.poids,
            stade.stade
           FROM ((poids
             FULL JOIN taille ON ((poids.lot_identifiant = taille.lot_identifiant)))
             FULL JOIN stade ON ((stade.lot_identifiant = poids.lot_identifiant)))
        )
 SELECT t_operation_ope.ope_identifiant,
    t_lot_lot.lot_identifiant,
    t_operation_ope.ope_date_debut,
    t_operation_ope.ope_operateur,
    joined_query.taille,
    joined_query.poids,
    joined_query.stade
   FROM ((joined_query
     JOIN user_1.t_lot_lot ON ((joined_query.lot_identifiant = t_lot_lot.lot_identifiant)))
     JOIN user_1.t_operation_ope ON ((t_lot_lot.lot_ope_identifiant = t_operation_ope.ope_identifiant)))
  ORDER BY t_operation_ope.ope_date_debut;


ALTER TABLE user_1.civelle_taille_poids_stade OWNER TO postgres;

--
-- Name: t_bilanmigrationjournalier_bjo; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.t_bilanmigrationjournalier_bjo (
    bjo_identifiant integer NOT NULL,
    bjo_dis_identifiant integer NOT NULL,
    bjo_tax_code character varying(6) NOT NULL,
    bjo_std_code character varying(4) NOT NULL,
    bjo_annee integer NOT NULL,
    bjo_jour timestamp without time zone NOT NULL,
    bjo_labelquantite character varying(30),
    bjo_valeur double precision,
    bjo_horodateexport timestamp without time zone,
    bjo_org_code character varying(30) NOT NULL
);


ALTER TABLE user_1.t_bilanmigrationjournalier_bjo OWNER TO user_1;

--
-- Name: t_bilanmigrationjournalier_bjo_bjo_identifiant_seq; Type: SEQUENCE; Schema: user_1; Owner: user_1
--

CREATE SEQUENCE user_1.t_bilanmigrationjournalier_bjo_bjo_identifiant_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_1.t_bilanmigrationjournalier_bjo_bjo_identifiant_seq OWNER TO user_1;

--
-- Name: t_bilanmigrationjournalier_bjo_bjo_identifiant_seq; Type: SEQUENCE OWNED BY; Schema: user_1; Owner: user_1
--

ALTER SEQUENCE user_1.t_bilanmigrationjournalier_bjo_bjo_identifiant_seq OWNED BY user_1.t_bilanmigrationjournalier_bjo.bjo_identifiant;


--
-- Name: t_bilanmigrationmensuel_bme; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.t_bilanmigrationmensuel_bme (
    bme_identifiant integer NOT NULL,
    bme_dis_identifiant integer NOT NULL,
    bme_tax_code character varying(6) NOT NULL,
    bme_std_code character varying(4) NOT NULL,
    bme_annee integer NOT NULL,
    bme_mois integer NOT NULL,
    bme_labelquantite character varying(30),
    bme_valeur double precision,
    bme_horodateexport timestamp without time zone,
    bme_org_code character varying(30) NOT NULL
);


ALTER TABLE user_1.t_bilanmigrationmensuel_bme OWNER TO user_1;

--
-- Name: t_bilanmigrationmensuel_bme_bme_identifiant_seq; Type: SEQUENCE; Schema: user_1; Owner: user_1
--

CREATE SEQUENCE user_1.t_bilanmigrationmensuel_bme_bme_identifiant_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_1.t_bilanmigrationmensuel_bme_bme_identifiant_seq OWNER TO user_1;

--
-- Name: t_bilanmigrationmensuel_bme_bme_identifiant_seq; Type: SEQUENCE OWNED BY; Schema: user_1; Owner: user_1
--

ALTER SEQUENCE user_1.t_bilanmigrationmensuel_bme_bme_identifiant_seq OWNED BY user_1.t_bilanmigrationmensuel_bme.bme_identifiant;


--
-- Name: t_dispositifcomptage_dic; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.t_dispositifcomptage_dic (
    dic_dis_identifiant integer NOT NULL,
    dic_dif_identifiant integer NOT NULL,
    dic_code character varying(16) NOT NULL,
    dic_tdc_code integer NOT NULL,
    dic_org_code character varying(30) NOT NULL
);


ALTER TABLE user_1.t_dispositifcomptage_dic OWNER TO user_1;

--
-- Name: t_dispositiffranchissement_dif; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.t_dispositiffranchissement_dif (
    dif_dis_identifiant integer NOT NULL,
    dif_ouv_identifiant integer NOT NULL,
    dif_code character varying(16) NOT NULL,
    dif_localisation text,
    dif_orientation character varying(20) NOT NULL,
    dif_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_dif_orientation CHECK (((upper((dif_orientation)::text) = 'DESCENTE'::text) OR (upper((dif_orientation)::text) = 'MONTEE'::text)))
);


ALTER TABLE user_1.t_dispositiffranchissement_dif OWNER TO user_1;

--
-- Name: t_marque_mqe; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.t_marque_mqe (
    mqe_reference character varying(30) NOT NULL,
    mqe_loc_code character varying(4) NOT NULL,
    mqe_nmq_code character varying(4) NOT NULL,
    mqe_omq_reference character varying(30),
    mqe_commentaires text,
    mqe_org_code character varying(30) NOT NULL
);


ALTER TABLE user_1.t_marque_mqe OWNER TO user_1;

--
-- Name: t_operation_ope_ope_identifiant_seq; Type: SEQUENCE; Schema: user_1; Owner: user_1
--

CREATE SEQUENCE user_1.t_operation_ope_ope_identifiant_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_1.t_operation_ope_ope_identifiant_seq OWNER TO user_1;

--
-- Name: t_operation_ope_ope_identifiant_seq; Type: SEQUENCE OWNED BY; Schema: user_1; Owner: user_1
--

ALTER SEQUENCE user_1.t_operation_ope_ope_identifiant_seq OWNED BY user_1.t_operation_ope.ope_identifiant;


--
-- Name: t_operationmarquage_omq; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.t_operationmarquage_omq (
    omq_reference character varying(30) NOT NULL,
    omq_commentaires text,
    omq_org_code character varying(30) DEFAULT 'TEST'::character varying NOT NULL
);


ALTER TABLE user_1.t_operationmarquage_omq OWNER TO user_1;

--
-- Name: t_ouvrage_ouv_ouv_identifiant_seq; Type: SEQUENCE; Schema: user_1; Owner: postgres
--

CREATE SEQUENCE user_1.t_ouvrage_ouv_ouv_identifiant_seq
    START WITH 8
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_1.t_ouvrage_ouv_ouv_identifiant_seq OWNER TO postgres;

--
-- Name: t_ouvrage_ouv; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.t_ouvrage_ouv (
    ouv_identifiant integer DEFAULT nextval('user_1.t_ouvrage_ouv_ouv_identifiant_seq'::regclass) NOT NULL,
    ouv_sta_code character varying(8) NOT NULL,
    ouv_code character varying(12) NOT NULL,
    ouv_libelle character varying(40) NOT NULL,
    ouv_localisation text,
    ouv_coordonnee_x integer,
    ouv_coordonnee_y integer,
    ouv_altitude smallint,
    ouv_carte_localisation bytea,
    ouv_commentaires text,
    ouv_nov_code character varying(4) NOT NULL,
    ouv_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_ouv_altitude CHECK ((ouv_altitude >= 0)),
    CONSTRAINT c_ck_ouv_coordonnee_x CHECK (((ouv_coordonnee_x >= '-400000'::integer) AND (ouv_coordonnee_x < 1300000))),
    CONSTRAINT c_ck_ouv_coordonnee_y CHECK (((ouv_coordonnee_y >= 6000000) AND (ouv_coordonnee_y < 7500000)))
);


ALTER TABLE user_1.t_ouvrage_ouv OWNER TO user_1;

--
-- Name: COLUMN t_ouvrage_ouv.ouv_coordonnee_x; Type: COMMENT; Schema: user_1; Owner: user_1
--

COMMENT ON COLUMN user_1.t_ouvrage_ouv.ouv_coordonnee_x IS 'Coordonnée x en Lambert 93';


--
-- Name: COLUMN t_ouvrage_ouv.ouv_coordonnee_y; Type: COMMENT; Schema: user_1; Owner: user_1
--

COMMENT ON COLUMN user_1.t_ouvrage_ouv.ouv_coordonnee_y IS 'Coordonnée y en Lambert 93';


--
-- Name: t_periodefonctdispositif_per; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.t_periodefonctdispositif_per (
    per_dis_identifiant integer NOT NULL,
    per_date_debut timestamp(0) without time zone NOT NULL,
    per_date_fin timestamp(0) without time zone NOT NULL,
    per_commentaires text,
    per_etat_fonctionnement boolean NOT NULL,
    per_tar_code character varying(4),
    per_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_per_date_fin CHECK ((per_date_fin >= per_date_debut))
);


ALTER TABLE user_1.t_periodefonctdispositif_per OWNER TO user_1;

--
-- Name: t_station_sta; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.t_station_sta (
    sta_code character varying(8) NOT NULL,
    sta_nom character varying(40) NOT NULL,
    sta_localisation character varying(60),
    sta_coordonnee_x integer,
    sta_coordonnee_y integer,
    sta_altitude smallint,
    sta_carte_localisation bytea,
    sta_superficie integer,
    sta_distance_mer double precision,
    sta_date_creation date,
    sta_date_suppression date,
    sta_commentaires text,
    sta_dernier_import_conditions timestamp(0) without time zone,
    sta_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_sta_altitude CHECK ((sta_altitude >= 0)),
    CONSTRAINT c_ck_sta_coordonnee_x CHECK (((sta_coordonnee_x >= '-400000'::integer) AND (sta_coordonnee_x < 1300000))),
    CONSTRAINT c_ck_sta_coordonnee_y CHECK (((sta_coordonnee_y >= 6000000) AND (sta_coordonnee_y < 7500000))),
    CONSTRAINT c_ck_sta_date_suppression CHECK ((sta_date_suppression >= sta_date_creation)),
    CONSTRAINT c_ck_sta_distance_mer CHECK ((sta_distance_mer >= (0)::double precision)),
    CONSTRAINT c_ck_sta_superficie CHECK ((sta_superficie >= 0))
);


ALTER TABLE user_1.t_station_sta OWNER TO user_1;

--
-- Name: COLUMN t_station_sta.sta_coordonnee_x; Type: COMMENT; Schema: user_1; Owner: user_1
--

COMMENT ON COLUMN user_1.t_station_sta.sta_coordonnee_x IS 'Coordonnée x en Lambert 93';


--
-- Name: COLUMN t_station_sta.sta_coordonnee_y; Type: COMMENT; Schema: user_1; Owner: user_1
--

COMMENT ON COLUMN user_1.t_station_sta.sta_coordonnee_y IS 'Coordonnée y en Lambert 93';


--
-- Name: tg_dispositif_dis; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.tg_dispositif_dis (
    dis_identifiant integer NOT NULL,
    dis_date_creation date,
    dis_date_suppression date,
    dis_commentaires text,
    dis_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_dis_date_suppression CHECK ((dis_date_suppression >= dis_date_creation))
);


ALTER TABLE user_1.tg_dispositif_dis OWNER TO user_1;

--
-- Name: tg_dispositif_dis_dis_identifiant_seq; Type: SEQUENCE; Schema: user_1; Owner: user_1
--

CREATE SEQUENCE user_1.tg_dispositif_dis_dis_identifiant_seq
    START WITH 19
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_1.tg_dispositif_dis_dis_identifiant_seq OWNER TO user_1;

--
-- Name: tg_dispositif_dis_dis_identifiant_seq; Type: SEQUENCE OWNED BY; Schema: user_1; Owner: user_1
--

ALTER SEQUENCE user_1.tg_dispositif_dis_dis_identifiant_seq OWNED BY user_1.tg_dispositif_dis.dis_identifiant;


--
-- Name: tj_actionmarquage_act; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.tj_actionmarquage_act (
    act_lot_identifiant integer NOT NULL,
    act_mqe_reference character varying(30) NOT NULL,
    act_action character varying(20) NOT NULL,
    act_commentaires text,
    act_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_act_action CHECK (((upper((act_action)::text) = 'POSE'::text) OR (upper((act_action)::text) = 'LECTURE'::text) OR (upper((act_action)::text) = 'RETRAIT'::text)))
);


ALTER TABLE user_1.tj_actionmarquage_act OWNER TO user_1;

--
-- Name: tj_coefficientconversion_coe; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.tj_coefficientconversion_coe (
    coe_tax_code character varying(6) NOT NULL,
    coe_std_code character varying(4) NOT NULL,
    coe_qte_code character varying(4) NOT NULL,
    coe_date_debut date NOT NULL,
    coe_date_fin date NOT NULL,
    coe_valeur_coefficient real,
    coe_commentaires text,
    coe_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_coe_date_fin CHECK ((coe_date_fin >= coe_date_debut)),
    CONSTRAINT c_nn_coe_org_code CHECK ((coe_org_code IS NOT NULL))
);


ALTER TABLE user_1.tj_coefficientconversion_coe OWNER TO user_1;

--
-- Name: tj_conditionenvironnementale_env; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.tj_conditionenvironnementale_env (
    env_date_debut timestamp(0) without time zone NOT NULL,
    env_date_fin timestamp(0) without time zone NOT NULL,
    env_methode_obtention character varying(10),
    env_val_identifiant integer,
    env_valeur_quantitatif real,
    env_stm_identifiant integer NOT NULL,
    env_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_env CHECK ((((env_val_identifiant IS NOT NULL) AND (env_valeur_quantitatif IS NULL)) OR ((env_val_identifiant IS NULL) AND (env_valeur_quantitatif IS NOT NULL)))),
    CONSTRAINT c_ck_env_date_fin CHECK ((env_date_fin >= env_date_debut)),
    CONSTRAINT c_ck_env_methode_obtention CHECK (((upper((env_methode_obtention)::text) = 'MESURE'::text) OR (upper((env_methode_obtention)::text) = 'CALCULE'::text)))
);


ALTER TABLE user_1.tj_conditionenvironnementale_env OWNER TO user_1;

--
-- Name: tj_dfestdestinea_dtx; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.tj_dfestdestinea_dtx (
    dtx_dif_identifiant integer NOT NULL,
    dtx_tax_code character varying(6) NOT NULL,
    dtx_org_code character varying(30) NOT NULL
);


ALTER TABLE user_1.tj_dfestdestinea_dtx OWNER TO user_1;

--
-- Name: tj_dfesttype_dft; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.tj_dfesttype_dft (
    dft_df_identifiant integer NOT NULL,
    dft_rang smallint NOT NULL,
    dft_org_code character varying(30) NOT NULL,
    dft_tdf_code character varying(2) NOT NULL,
    CONSTRAINT c_ck_dft_rang CHECK ((dft_rang >= 0))
);


ALTER TABLE user_1.tj_dfesttype_dft OWNER TO user_1;

--
-- Name: tj_pathologieconstatee_pco; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.tj_pathologieconstatee_pco (
    pco_lot_identifiant integer NOT NULL,
    pco_pat_code character varying(4) NOT NULL,
    pco_loc_code character varying(4) NOT NULL,
    pco_commentaires text,
    pco_org_code character varying(30) NOT NULL,
    pco_imp_code integer
);


ALTER TABLE user_1.tj_pathologieconstatee_pco OWNER TO user_1;

--
-- Name: tj_prelevementlot_prl; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.tj_prelevementlot_prl (
    prl_pre_typeprelevement character varying(15) NOT NULL,
    prl_lot_identifiant integer NOT NULL,
    prl_code character varying(12) NOT NULL,
    prl_operateur character varying(35),
    prl_loc_code character varying(4),
    prl_commentaires text,
    prl_org_code character varying(30) NOT NULL
);


ALTER TABLE user_1.tj_prelevementlot_prl OWNER TO user_1;

--
-- Name: tj_stationmesure_stm; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.tj_stationmesure_stm (
    stm_identifiant integer NOT NULL,
    stm_libelle character varying(12),
    stm_sta_code character varying(8) NOT NULL,
    stm_par_code character varying(5) NOT NULL,
    stm_description text,
    stm_org_code character varying(30) NOT NULL
);


ALTER TABLE user_1.tj_stationmesure_stm OWNER TO user_1;

--
-- Name: COLUMN tj_stationmesure_stm.stm_description; Type: COMMENT; Schema: user_1; Owner: user_1
--

COMMENT ON COLUMN user_1.tj_stationmesure_stm.stm_description IS 'pour afficher la source de données (par exemple banque hydro), il faut ajouter le mot clé "source :" puis la source de données';


--
-- Name: tj_stationmesure_stm_stm_identifiant_seq; Type: SEQUENCE; Schema: user_1; Owner: user_1
--

CREATE SEQUENCE user_1.tj_stationmesure_stm_stm_identifiant_seq
    START WITH 19
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_1.tj_stationmesure_stm_stm_identifiant_seq OWNER TO user_1;

--
-- Name: tj_stationmesure_stm_stm_identifiant_seq; Type: SEQUENCE OWNED BY; Schema: user_1; Owner: user_1
--

ALTER SEQUENCE user_1.tj_stationmesure_stm_stm_identifiant_seq OWNED BY user_1.tj_stationmesure_stm.stm_identifiant;


--
-- Name: tj_tauxechappement_txe; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.tj_tauxechappement_txe (
    txe_tax_code character varying(6) NOT NULL,
    txe_std_code character varying(4) NOT NULL,
    txe_date_debut timestamp(0) without time zone NOT NULL,
    txe_date_fin timestamp(0) without time zone NOT NULL,
    txe_methode_estimation text,
    txe_ech_code character varying(4),
    txe_valeur_taux smallint,
    txe_commentaires text,
    txe_org_code character varying(30) NOT NULL,
    txe_sta_code character varying(8) NOT NULL,
    CONSTRAINT c_ck_txe_date_fin CHECK ((txe_date_fin >= txe_date_debut)),
    CONSTRAINT c_ck_txe_methode_estimation CHECK (((upper(txe_methode_estimation) = 'MESURE'::text) OR (upper(txe_methode_estimation) = 'CALCULE'::text) OR (upper(txe_methode_estimation) = 'EXPERT'::text))),
    CONSTRAINT c_ck_txe_valeur_taux CHECK ((((txe_valeur_taux >= 0) AND (txe_valeur_taux <= 100)) OR ((txe_valeur_taux IS NULL) AND (txe_ech_code IS NOT NULL))))
);


ALTER TABLE user_1.tj_tauxechappement_txe OWNER TO user_1;

--
-- Name: ts_maintenance_main; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.ts_maintenance_main (
    main_identifiant integer NOT NULL,
    main_ticket integer,
    main_description text
);


ALTER TABLE user_1.ts_maintenance_main OWNER TO user_1;

--
-- Name: TABLE ts_maintenance_main; Type: COMMENT; Schema: user_1; Owner: user_1
--

COMMENT ON TABLE user_1.ts_maintenance_main IS 'Table de suivi des operations de maintenance de la base';


--
-- Name: ts_maintenance_main_main_identifiant_seq; Type: SEQUENCE; Schema: user_1; Owner: user_1
--

CREATE SEQUENCE user_1.ts_maintenance_main_main_identifiant_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_1.ts_maintenance_main_main_identifiant_seq OWNER TO user_1;

--
-- Name: ts_maintenance_main_main_identifiant_seq; Type: SEQUENCE OWNED BY; Schema: user_1; Owner: user_1
--

ALTER SEQUENCE user_1.ts_maintenance_main_main_identifiant_seq OWNED BY user_1.ts_maintenance_main.main_identifiant;


--
-- Name: ts_masque_mas; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.ts_masque_mas (
    mas_id integer NOT NULL,
    mas_code character varying(15) NOT NULL,
    mas_description text,
    mas_raccourci text,
    mas_type character varying(3),
    CONSTRAINT c_ck_mas_type CHECK ((((mas_type)::text = 'lot'::text) OR ((mas_type)::text = 'ope'::text)))
);


ALTER TABLE user_1.ts_masque_mas OWNER TO user_1;

--
-- Name: ts_masque_mas_mas_id_seq; Type: SEQUENCE; Schema: user_1; Owner: user_1
--

CREATE SEQUENCE user_1.ts_masque_mas_mas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_1.ts_masque_mas_mas_id_seq OWNER TO user_1;

--
-- Name: ts_masque_mas_mas_id_seq; Type: SEQUENCE OWNED BY; Schema: user_1; Owner: user_1
--

ALTER SEQUENCE user_1.ts_masque_mas_mas_id_seq OWNED BY user_1.ts_masque_mas.mas_id;


--
-- Name: ts_masquecaracteristiquelot_mac; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.ts_masquecaracteristiquelot_mac (
    mac_id integer NOT NULL,
    mac_mal_id integer NOT NULL,
    mac_par_code character varying(5) NOT NULL,
    mac_affichagevaleur boolean,
    mac_affichageprecision boolean,
    mac_affichagemethodeobtention boolean,
    mac_affichagecommentaire boolean,
    mac_valeurquantitatifdefaut numeric,
    mac_valeurqualitatifdefaut integer,
    mac_precisiondefaut numeric,
    mac_methodeobtentiondefaut character varying(10),
    mac_commentairedefaut text
);


ALTER TABLE user_1.ts_masquecaracteristiquelot_mac OWNER TO user_1;

--
-- Name: ts_masquecaracteristiquelot_mac_mac_id_seq; Type: SEQUENCE; Schema: user_1; Owner: user_1
--

CREATE SEQUENCE user_1.ts_masquecaracteristiquelot_mac_mac_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_1.ts_masquecaracteristiquelot_mac_mac_id_seq OWNER TO user_1;

--
-- Name: ts_masquecaracteristiquelot_mac_mac_id_seq; Type: SEQUENCE OWNED BY; Schema: user_1; Owner: user_1
--

ALTER SEQUENCE user_1.ts_masquecaracteristiquelot_mac_mac_id_seq OWNED BY user_1.ts_masquecaracteristiquelot_mac.mac_id;


--
-- Name: ts_masqueconditionsenvironnementales_mae; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.ts_masqueconditionsenvironnementales_mae (
    mae_mao_id integer NOT NULL,
    mae_stm_identifiant integer NOT NULL,
    mae_affichage boolean,
    mae_valeurdefaut text
);


ALTER TABLE user_1.ts_masqueconditionsenvironnementales_mae OWNER TO user_1;

--
-- Name: ts_masquelot_mal; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.ts_masquelot_mal (
    mal_mas_id integer NOT NULL,
    mal_affichage boolean[],
    mal_valeurdefaut text[],
    CONSTRAINT c_ck_length_mal_affichage CHECK ((array_length(mal_affichage, 1) = 21)),
    CONSTRAINT c_ck_length_mal_valeurdefaut CHECK ((array_length(mal_valeurdefaut, 1) = 14))
);


ALTER TABLE user_1.ts_masquelot_mal OWNER TO user_1;

--
-- Name: ts_masqueope_mao; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.ts_masqueope_mao (
    mao_mas_id integer NOT NULL,
    mao_affichage boolean[],
    mao_valeurdefaut text[],
    CONSTRAINT c_ck_length_mao_affichage CHECK ((array_length(mao_affichage, 1) = 16)),
    CONSTRAINT c_ck_length_mao_valeurdefaut CHECK ((array_length(mao_valeurdefaut, 1) = 9))
);


ALTER TABLE user_1.ts_masqueope_mao OWNER TO user_1;

--
-- Name: ts_masqueordreaffichage_maa; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.ts_masqueordreaffichage_maa (
    maa_id integer NOT NULL,
    maa_mal_id integer,
    maa_table character varying(40) NOT NULL,
    maa_valeur text NOT NULL,
    maa_champdumasque character varying(30),
    maa_rang integer
);


ALTER TABLE user_1.ts_masqueordreaffichage_maa OWNER TO user_1;

--
-- Name: ts_masqueordreaffichage_maa_maa_id_seq; Type: SEQUENCE; Schema: user_1; Owner: user_1
--

CREATE SEQUENCE user_1.ts_masqueordreaffichage_maa_maa_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_1.ts_masqueordreaffichage_maa_maa_id_seq OWNER TO user_1;

--
-- Name: ts_masqueordreaffichage_maa_maa_id_seq; Type: SEQUENCE OWNED BY; Schema: user_1; Owner: user_1
--

ALTER SEQUENCE user_1.ts_masqueordreaffichage_maa_maa_id_seq OWNED BY user_1.ts_masqueordreaffichage_maa.maa_id;


--
-- Name: ts_taillevideo_tav; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.ts_taillevideo_tav (
    tav_dic_identifiant integer NOT NULL,
    tav_coefconversion numeric NOT NULL,
    tav_distance character varying(3) NOT NULL,
    tav_org_code character varying(30) NOT NULL
);


ALTER TABLE user_1.ts_taillevideo_tav OWNER TO user_1;

--
-- Name: ts_taxonvideo_txv; Type: TABLE; Schema: user_1; Owner: user_1
--

CREATE TABLE user_1.ts_taxonvideo_txv (
    txv_code character varying(3) NOT NULL,
    txv_tax_code character varying(6),
    txv_std_code character varying(4),
    txv_org_code character varying(30) NOT NULL
);


ALTER TABLE user_1.ts_taxonvideo_txv OWNER TO user_1;

--
-- Name: v_taxon_tax; Type: VIEW; Schema: user_1; Owner: postgres
--

CREATE VIEW user_1.v_taxon_tax AS
 SELECT tax.tax_code,
    tax.tax_nom_latin,
    tax.tax_nom_commun,
    tax.tax_ntx_code,
    tax.tax_tax_code,
    tax.tax_rang,
    txv.txv_code,
    txv.txv_tax_code,
    txv.txv_std_code,
    std.std_code,
    std.std_libelle
   FROM ((ref.tr_taxon_tax tax
     RIGHT JOIN user_1.ts_taxonvideo_txv txv ON (((tax.tax_code)::text = (txv.txv_tax_code)::text)))
     LEFT JOIN ref.tr_stadedeveloppement_std std ON (((txv.txv_std_code)::text = (std.std_code)::text)));


ALTER TABLE user_1.v_taxon_tax OWNER TO postgres;

--
-- Name: vue; Type: VIEW; Schema: user_1; Owner: postgres
--

CREATE VIEW user_1.vue AS
 SELECT t_operation_ope.ope_identifiant,
    t_lot_lot.lot_identifiant,
    t_operation_ope.ope_dic_identifiant,
    t_lot_lot.lot_lot_identifiant AS lot_pere,
    t_operation_ope.ope_date_debut,
    t_operation_ope.ope_date_fin,
    t_lot_lot.lot_tax_code,
    t_lot_lot.lot_effectif,
    t_lot_lot.lot_quantite,
    t_lot_lot.lot_std_code,
    tr_stadedeveloppement_std.std_libelle,
    tr_devenirlot_dev.dev_code,
    t_lot_lot.lot_commentaires
   FROM (((((user_1.t_operation_ope
     JOIN user_1.t_lot_lot ON ((t_lot_lot.lot_ope_identifiant = t_operation_ope.ope_identifiant)))
     LEFT JOIN ref.tr_typequantitelot_qte ON (((tr_typequantitelot_qte.qte_code)::text = (t_lot_lot.lot_qte_code)::text)))
     LEFT JOIN ref.tr_devenirlot_dev ON (((tr_devenirlot_dev.dev_code)::text = (t_lot_lot.lot_dev_code)::text)))
     JOIN ref.tr_taxon_tax ON (((tr_taxon_tax.tax_code)::text = (t_lot_lot.lot_tax_code)::text)))
     JOIN ref.tr_stadedeveloppement_std ON (((tr_stadedeveloppement_std.std_code)::text = (t_lot_lot.lot_std_code)::text)))
  ORDER BY t_operation_ope.ope_date_debut;


ALTER TABLE user_1.vue OWNER TO postgres;

--
-- Name: vue_lot_ope; Type: VIEW; Schema: user_1; Owner: postgres
--

CREATE VIEW user_1.vue_lot_ope AS
 SELECT t_operation_ope.ope_identifiant,
    t_lot_lot.lot_identifiant,
    t_operation_ope.ope_dic_identifiant,
    t_lot_lot.lot_lot_identifiant AS lot_pere,
    t_operation_ope.ope_date_debut,
    t_operation_ope.ope_date_fin,
    t_lot_lot.lot_effectif,
    t_lot_lot.lot_quantite,
    t_lot_lot.lot_tax_code,
    t_lot_lot.lot_std_code,
    tr_stadedeveloppement_std.std_libelle,
    tr_devenirlot_dev.dev_code,
    t_lot_lot.lot_commentaires
   FROM (((((user_1.t_operation_ope
     JOIN user_1.t_lot_lot ON ((t_lot_lot.lot_ope_identifiant = t_operation_ope.ope_identifiant)))
     LEFT JOIN ref.tr_typequantitelot_qte ON (((tr_typequantitelot_qte.qte_code)::text = (t_lot_lot.lot_qte_code)::text)))
     LEFT JOIN ref.tr_devenirlot_dev ON (((tr_devenirlot_dev.dev_code)::text = (t_lot_lot.lot_dev_code)::text)))
     JOIN ref.tr_taxon_tax ON (((tr_taxon_tax.tax_code)::text = (t_lot_lot.lot_tax_code)::text)))
     JOIN ref.tr_stadedeveloppement_std ON (((tr_stadedeveloppement_std.std_code)::text = (t_lot_lot.lot_std_code)::text)))
  ORDER BY t_operation_ope.ope_date_debut;


ALTER TABLE user_1.vue_lot_ope OWNER TO postgres;

--
-- Name: vue_lot_ope_car; Type: VIEW; Schema: user_1; Owner: postgres
--

CREATE VIEW user_1.vue_lot_ope_car AS
 SELECT t_operation_ope.ope_identifiant,
    t_lot_lot.lot_identifiant,
    t_operation_ope.ope_dic_identifiant,
    t_lot_lot.lot_lot_identifiant AS lot_pere,
    t_operation_ope.ope_date_debut,
    t_operation_ope.ope_date_fin,
    t_lot_lot.lot_effectif,
    t_lot_lot.lot_quantite,
    t_lot_lot.lot_tax_code,
    t_lot_lot.lot_std_code,
    tr_taxon_tax.tax_nom_latin,
    tr_stadedeveloppement_std.std_libelle,
    tr_devenirlot_dev.dev_code,
    tr_devenirlot_dev.dev_libelle,
    tg_parametre_par.par_nom,
    tj_caracteristiquelot_car.car_par_code,
    tj_caracteristiquelot_car.car_methode_obtention,
    tj_caracteristiquelot_car.car_val_identifiant,
    tj_caracteristiquelot_car.car_valeur_quantitatif,
    tr_valeurparametrequalitatif_val.val_libelle
   FROM (((((((((user_1.t_operation_ope
     JOIN user_1.t_lot_lot ON ((t_lot_lot.lot_ope_identifiant = t_operation_ope.ope_identifiant)))
     LEFT JOIN ref.tr_typequantitelot_qte ON (((tr_typequantitelot_qte.qte_code)::text = (t_lot_lot.lot_qte_code)::text)))
     LEFT JOIN ref.tr_devenirlot_dev ON (((tr_devenirlot_dev.dev_code)::text = (t_lot_lot.lot_dev_code)::text)))
     JOIN ref.tr_taxon_tax ON (((tr_taxon_tax.tax_code)::text = (t_lot_lot.lot_tax_code)::text)))
     JOIN ref.tr_stadedeveloppement_std ON (((tr_stadedeveloppement_std.std_code)::text = (t_lot_lot.lot_std_code)::text)))
     JOIN user_1.tj_caracteristiquelot_car ON ((tj_caracteristiquelot_car.car_lot_identifiant = t_lot_lot.lot_identifiant)))
     LEFT JOIN ref.tg_parametre_par ON (((tj_caracteristiquelot_car.car_par_code)::text = (tg_parametre_par.par_code)::text)))
     LEFT JOIN ref.tr_parametrequalitatif_qal ON (((tr_parametrequalitatif_qal.qal_par_code)::text = (tg_parametre_par.par_code)::text)))
     LEFT JOIN ref.tr_valeurparametrequalitatif_val ON ((tj_caracteristiquelot_car.car_val_identifiant = tr_valeurparametrequalitatif_val.val_identifiant)))
  ORDER BY t_operation_ope.ope_date_debut;


ALTER TABLE user_1.vue_lot_ope_car OWNER TO postgres;

--
-- Name: vue_ope_lot_ech_parquan; Type: VIEW; Schema: user_1; Owner: postgres
--

CREATE VIEW user_1.vue_ope_lot_ech_parquan AS
 SELECT t_operation_ope.ope_identifiant,
    t_operation_ope.ope_dic_identifiant,
    t_operation_ope.ope_date_debut,
    t_operation_ope.ope_date_fin,
    lot.lot_identifiant,
    lot.lot_methode_obtention,
    lot.lot_effectif,
    lot.lot_quantite,
    lot.lot_tax_code,
    lot.lot_std_code,
    tr_taxon_tax.tax_nom_latin,
    tr_stadedeveloppement_std.std_libelle,
    dev.dev_code,
    dev.dev_libelle,
    par.par_nom,
    car.car_par_code,
    car.car_methode_obtention,
    car.car_valeur_quantitatif,
    lot.lot_lot_identifiant AS lot_pere,
    lot_pere.lot_effectif AS lot_pere_effectif,
    lot_pere.lot_quantite AS lot_pere_quantite,
    dev_pere.dev_code AS lot_pere_dev_code,
    dev_pere.dev_libelle AS lot_pere_dev_libelle,
    parqual.par_nom AS lot_pere_par_nom,
    parqual.car_par_code AS lot_pere_par_code,
    parqual.car_methode_obtention AS lot_pere_car_methode_obtention,
    parqual.car_val_identifiant AS lot_pere_val_identifiant,
    parqual.val_libelle AS lot_pere_val_libelle
   FROM ((((((((((((user_1.t_operation_ope
     JOIN user_1.t_lot_lot lot ON ((lot.lot_ope_identifiant = t_operation_ope.ope_identifiant)))
     LEFT JOIN ref.tr_typequantitelot_qte qte ON (((qte.qte_code)::text = (lot.lot_qte_code)::text)))
     LEFT JOIN ref.tr_devenirlot_dev dev ON (((dev.dev_code)::text = (lot.lot_dev_code)::text)))
     JOIN ref.tr_taxon_tax ON (((tr_taxon_tax.tax_code)::text = (lot.lot_tax_code)::text)))
     JOIN ref.tr_stadedeveloppement_std ON (((tr_stadedeveloppement_std.std_code)::text = (lot.lot_std_code)::text)))
     JOIN user_1.tj_caracteristiquelot_car car ON ((car.car_lot_identifiant = lot.lot_identifiant)))
     LEFT JOIN ref.tg_parametre_par par ON (((car.car_par_code)::text = (par.par_code)::text)))
     JOIN ref.tr_parametrequantitatif_qan qan ON (((qan.qan_par_code)::text = (par.par_code)::text)))
     LEFT JOIN user_1.t_lot_lot lot_pere ON ((lot_pere.lot_identifiant = lot.lot_lot_identifiant)))
     LEFT JOIN ref.tr_typequantitelot_qte qte_pere ON (((qte_pere.qte_code)::text = (lot_pere.lot_qte_code)::text)))
     LEFT JOIN ref.tr_devenirlot_dev dev_pere ON (((dev_pere.dev_code)::text = (lot_pere.lot_dev_code)::text)))
     LEFT JOIN ( SELECT car_pere.car_lot_identifiant,
            car_pere.car_par_code,
            car_pere.car_methode_obtention,
            car_pere.car_val_identifiant,
            car_pere.car_valeur_quantitatif,
            car_pere.car_precision,
            car_pere.car_commentaires,
            par_pere.par_code,
            par_pere.par_nom,
            par_pere.par_unite,
            par_pere.par_nature,
            par_pere.par_definition,
            qal_pere.qal_par_code,
            qal_pere.qal_valeurs_possibles,
            val_pere.val_identifiant,
            val_pere.val_qal_code,
            val_pere.val_rang,
            val_pere.val_libelle
           FROM (((user_1.tj_caracteristiquelot_car car_pere
             LEFT JOIN ref.tg_parametre_par par_pere ON (((car_pere.car_par_code)::text = (par_pere.par_code)::text)))
             JOIN ref.tr_parametrequalitatif_qal qal_pere ON (((qal_pere.qal_par_code)::text = (par_pere.par_code)::text)))
             LEFT JOIN ref.tr_valeurparametrequalitatif_val val_pere ON ((car_pere.car_val_identifiant = val_pere.val_identifiant)))) parqual ON ((parqual.car_lot_identifiant = lot_pere.lot_identifiant)));


ALTER TABLE user_1.vue_ope_lot_ech_parquan OWNER TO postgres;

--
-- Name: t_bilanmigrationjournalier_bjo bjo_identifiant; Type: DEFAULT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_bilanmigrationjournalier_bjo ALTER COLUMN bjo_identifiant SET DEFAULT nextval('user_1.t_bilanmigrationjournalier_bjo_bjo_identifiant_seq'::regclass);


--
-- Name: t_bilanmigrationmensuel_bme bme_identifiant; Type: DEFAULT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_bilanmigrationmensuel_bme ALTER COLUMN bme_identifiant SET DEFAULT nextval('user_1.t_bilanmigrationmensuel_bme_bme_identifiant_seq'::regclass);


--
-- Name: t_operation_ope ope_identifiant; Type: DEFAULT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_operation_ope ALTER COLUMN ope_identifiant SET DEFAULT nextval('user_1.t_operation_ope_ope_identifiant_seq'::regclass);


--
-- Name: tg_dispositif_dis dis_identifiant; Type: DEFAULT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tg_dispositif_dis ALTER COLUMN dis_identifiant SET DEFAULT nextval('user_1.tg_dispositif_dis_dis_identifiant_seq'::regclass);


--
-- Name: tj_stationmesure_stm stm_identifiant; Type: DEFAULT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_stationmesure_stm ALTER COLUMN stm_identifiant SET DEFAULT nextval('user_1.tj_stationmesure_stm_stm_identifiant_seq'::regclass);


--
-- Name: ts_maintenance_main main_identifiant; Type: DEFAULT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_maintenance_main ALTER COLUMN main_identifiant SET DEFAULT nextval('user_1.ts_maintenance_main_main_identifiant_seq'::regclass);


--
-- Name: ts_masque_mas mas_id; Type: DEFAULT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_masque_mas ALTER COLUMN mas_id SET DEFAULT nextval('user_1.ts_masque_mas_mas_id_seq'::regclass);


--
-- Name: ts_masquecaracteristiquelot_mac mac_id; Type: DEFAULT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_masquecaracteristiquelot_mac ALTER COLUMN mac_id SET DEFAULT nextval('user_1.ts_masquecaracteristiquelot_mac_mac_id_seq'::regclass);


--
-- Name: ts_masqueordreaffichage_maa maa_id; Type: DEFAULT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_masqueordreaffichage_maa ALTER COLUMN maa_id SET DEFAULT nextval('user_1.ts_masqueordreaffichage_maa_maa_id_seq'::regclass);


--
-- Data for Name: t_bilanmigrationjournalier_bjo; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.t_bilanmigrationjournalier_bjo (bjo_identifiant, bjo_dis_identifiant, bjo_tax_code, bjo_std_code, bjo_annee, bjo_jour, bjo_labelquantite, bjo_valeur, bjo_horodateexport, bjo_org_code) FROM stdin;
\.


--
-- Data for Name: t_bilanmigrationmensuel_bme; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.t_bilanmigrationmensuel_bme (bme_identifiant, bme_dis_identifiant, bme_tax_code, bme_std_code, bme_annee, bme_mois, bme_labelquantite, bme_valeur, bme_horodateexport, bme_org_code) FROM stdin;
\.


--
-- Data for Name: t_dispositifcomptage_dic; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.t_dispositifcomptage_dic (dic_dis_identifiant, dic_dif_identifiant, dic_code, dic_tdc_code, dic_org_code) FROM stdin;
\.


--
-- Data for Name: t_dispositiffranchissement_dif; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.t_dispositiffranchissement_dif (dif_dis_identifiant, dif_ouv_identifiant, dif_code, dif_localisation, dif_orientation, dif_org_code) FROM stdin;
\.


--
-- Data for Name: t_lot_lot; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.t_lot_lot (lot_identifiant, lot_ope_identifiant, lot_tax_code, lot_std_code, lot_effectif, lot_quantite, lot_qte_code, lot_methode_obtention, lot_lot_identifiant, lot_dev_code, lot_commentaires, lot_org_code) FROM stdin;
\.


--
-- Data for Name: t_marque_mqe; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.t_marque_mqe (mqe_reference, mqe_loc_code, mqe_nmq_code, mqe_omq_reference, mqe_commentaires, mqe_org_code) FROM stdin;
\.


--
-- Data for Name: t_operation_ope; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.t_operation_ope (ope_identifiant, ope_dic_identifiant, ope_date_debut, ope_date_fin, ope_organisme, ope_operateur, ope_commentaires, ope_org_code) FROM stdin;
\.


--
-- Data for Name: t_operationmarquage_omq; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.t_operationmarquage_omq (omq_reference, omq_commentaires, omq_org_code) FROM stdin;
\.


--
-- Data for Name: t_ouvrage_ouv; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.t_ouvrage_ouv (ouv_identifiant, ouv_sta_code, ouv_code, ouv_libelle, ouv_localisation, ouv_coordonnee_x, ouv_coordonnee_y, ouv_altitude, ouv_carte_localisation, ouv_commentaires, ouv_nov_code, ouv_org_code) FROM stdin;
\.


--
-- Data for Name: t_periodefonctdispositif_per; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.t_periodefonctdispositif_per (per_dis_identifiant, per_date_debut, per_date_fin, per_commentaires, per_etat_fonctionnement, per_tar_code, per_org_code) FROM stdin;
\.


--
-- Data for Name: t_station_sta; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.t_station_sta (sta_code, sta_nom, sta_localisation, sta_coordonnee_x, sta_coordonnee_y, sta_altitude, sta_carte_localisation, sta_superficie, sta_distance_mer, sta_date_creation, sta_date_suppression, sta_commentaires, sta_dernier_import_conditions, sta_org_code) FROM stdin;
\.


--
-- Data for Name: tg_dispositif_dis; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.tg_dispositif_dis (dis_identifiant, dis_date_creation, dis_date_suppression, dis_commentaires, dis_org_code) FROM stdin;
\.


--
-- Data for Name: tj_actionmarquage_act; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.tj_actionmarquage_act (act_lot_identifiant, act_mqe_reference, act_action, act_commentaires, act_org_code) FROM stdin;
\.


--
-- Data for Name: tj_caracteristiquelot_car; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.tj_caracteristiquelot_car (car_lot_identifiant, car_par_code, car_methode_obtention, car_val_identifiant, car_valeur_quantitatif, car_precision, car_commentaires, car_org_code) FROM stdin;
\.


--
-- Data for Name: tj_coefficientconversion_coe; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.tj_coefficientconversion_coe (coe_tax_code, coe_std_code, coe_qte_code, coe_date_debut, coe_date_fin, coe_valeur_coefficient, coe_commentaires, coe_org_code) FROM stdin;
\.


--
-- Data for Name: tj_conditionenvironnementale_env; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.tj_conditionenvironnementale_env (env_date_debut, env_date_fin, env_methode_obtention, env_val_identifiant, env_valeur_quantitatif, env_stm_identifiant, env_org_code) FROM stdin;
\.


--
-- Data for Name: tj_dfestdestinea_dtx; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.tj_dfestdestinea_dtx (dtx_dif_identifiant, dtx_tax_code, dtx_org_code) FROM stdin;
\.


--
-- Data for Name: tj_dfesttype_dft; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.tj_dfesttype_dft (dft_df_identifiant, dft_rang, dft_org_code, dft_tdf_code) FROM stdin;
\.


--
-- Data for Name: tj_pathologieconstatee_pco; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.tj_pathologieconstatee_pco (pco_lot_identifiant, pco_pat_code, pco_loc_code, pco_commentaires, pco_org_code, pco_imp_code) FROM stdin;
\.


--
-- Data for Name: tj_prelevementlot_prl; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.tj_prelevementlot_prl (prl_pre_typeprelevement, prl_lot_identifiant, prl_code, prl_operateur, prl_loc_code, prl_commentaires, prl_org_code) FROM stdin;
\.


--
-- Data for Name: tj_stationmesure_stm; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.tj_stationmesure_stm (stm_identifiant, stm_libelle, stm_sta_code, stm_par_code, stm_description, stm_org_code) FROM stdin;
\.


--
-- Data for Name: tj_tauxechappement_txe; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.tj_tauxechappement_txe (txe_tax_code, txe_std_code, txe_date_debut, txe_date_fin, txe_methode_estimation, txe_ech_code, txe_valeur_taux, txe_commentaires, txe_org_code, txe_sta_code) FROM stdin;
\.


--
-- Data for Name: ts_maintenance_main; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.ts_maintenance_main (main_identifiant, main_ticket, main_description) FROM stdin;
\.


--
-- Data for Name: ts_masque_mas; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.ts_masque_mas (mas_id, mas_code, mas_description, mas_raccourci, mas_type) FROM stdin;
\.


--
-- Data for Name: ts_masquecaracteristiquelot_mac; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.ts_masquecaracteristiquelot_mac (mac_id, mac_mal_id, mac_par_code, mac_affichagevaleur, mac_affichageprecision, mac_affichagemethodeobtention, mac_affichagecommentaire, mac_valeurquantitatifdefaut, mac_valeurqualitatifdefaut, mac_precisiondefaut, mac_methodeobtentiondefaut, mac_commentairedefaut) FROM stdin;
\.


--
-- Data for Name: ts_masqueconditionsenvironnementales_mae; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.ts_masqueconditionsenvironnementales_mae (mae_mao_id, mae_stm_identifiant, mae_affichage, mae_valeurdefaut) FROM stdin;
\.


--
-- Data for Name: ts_masquelot_mal; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.ts_masquelot_mal (mal_mas_id, mal_affichage, mal_valeurdefaut) FROM stdin;
\.


--
-- Data for Name: ts_masqueope_mao; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.ts_masqueope_mao (mao_mas_id, mao_affichage, mao_valeurdefaut) FROM stdin;
\.


--
-- Data for Name: ts_masqueordreaffichage_maa; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) FROM stdin;
\.


--
-- Data for Name: ts_taillevideo_tav; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.ts_taillevideo_tav (tav_dic_identifiant, tav_coefconversion, tav_distance, tav_org_code) FROM stdin;
\.


--
-- Data for Name: ts_taxonvideo_txv; Type: TABLE DATA; Schema: user_1; Owner: user_1
--

COPY user_1.ts_taxonvideo_txv (txv_code, txv_tax_code, txv_std_code, txv_org_code) FROM stdin;
\.


--
-- Name: t_bilanmigrationjournalier_bjo_bjo_identifiant_seq; Type: SEQUENCE SET; Schema: user_1; Owner: user_1
--

SELECT pg_catalog.setval('user_1.t_bilanmigrationjournalier_bjo_bjo_identifiant_seq', 1, false);


--
-- Name: t_bilanmigrationmensuel_bme_bme_identifiant_seq; Type: SEQUENCE SET; Schema: user_1; Owner: user_1
--

SELECT pg_catalog.setval('user_1.t_bilanmigrationmensuel_bme_bme_identifiant_seq', 1, false);


--
-- Name: t_lot_lot_lot_identifiant_seq; Type: SEQUENCE SET; Schema: user_1; Owner: postgres
--

SELECT pg_catalog.setval('user_1.t_lot_lot_lot_identifiant_seq', 300000, false);


--
-- Name: t_operation_ope_ope_identifiant_seq; Type: SEQUENCE SET; Schema: user_1; Owner: user_1
--

SELECT pg_catalog.setval('user_1.t_operation_ope_ope_identifiant_seq', 1, false);


--
-- Name: t_ouvrage_ouv_ouv_identifiant_seq; Type: SEQUENCE SET; Schema: user_1; Owner: postgres
--

SELECT pg_catalog.setval('user_1.t_ouvrage_ouv_ouv_identifiant_seq', 8, false);


--
-- Name: tg_dispositif_dis_dis_identifiant_seq; Type: SEQUENCE SET; Schema: user_1; Owner: user_1
--

SELECT pg_catalog.setval('user_1.tg_dispositif_dis_dis_identifiant_seq', 19, false);


--
-- Name: tj_stationmesure_stm_stm_identifiant_seq; Type: SEQUENCE SET; Schema: user_1; Owner: user_1
--

SELECT pg_catalog.setval('user_1.tj_stationmesure_stm_stm_identifiant_seq', 19, false);


--
-- Name: ts_maintenance_main_main_identifiant_seq; Type: SEQUENCE SET; Schema: user_1; Owner: user_1
--

SELECT pg_catalog.setval('user_1.ts_maintenance_main_main_identifiant_seq', 1, false);


--
-- Name: ts_masque_mas_mas_id_seq; Type: SEQUENCE SET; Schema: user_1; Owner: user_1
--

SELECT pg_catalog.setval('user_1.ts_masque_mas_mas_id_seq', 1, false);


--
-- Name: ts_masquecaracteristiquelot_mac_mac_id_seq; Type: SEQUENCE SET; Schema: user_1; Owner: user_1
--

SELECT pg_catalog.setval('user_1.ts_masquecaracteristiquelot_mac_mac_id_seq', 1, false);


--
-- Name: ts_masqueordreaffichage_maa_maa_id_seq; Type: SEQUENCE SET; Schema: user_1; Owner: user_1
--

SELECT pg_catalog.setval('user_1.ts_masqueordreaffichage_maa_maa_id_seq', 1, false);


--
-- Name: tj_actionmarquage_act c_pk_act; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_actionmarquage_act
    ADD CONSTRAINT c_pk_act PRIMARY KEY (act_lot_identifiant, act_mqe_reference, act_org_code);


--
-- Name: t_bilanmigrationjournalier_bjo c_pk_bjo; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_bilanmigrationjournalier_bjo
    ADD CONSTRAINT c_pk_bjo PRIMARY KEY (bjo_identifiant, bjo_org_code);


--
-- Name: t_bilanmigrationmensuel_bme c_pk_bme; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_bilanmigrationmensuel_bme
    ADD CONSTRAINT c_pk_bme PRIMARY KEY (bme_identifiant, bme_org_code);


--
-- Name: tj_caracteristiquelot_car c_pk_car; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_caracteristiquelot_car
    ADD CONSTRAINT c_pk_car PRIMARY KEY (car_lot_identifiant, car_par_code, car_org_code);


--
-- Name: tj_coefficientconversion_coe c_pk_coe; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_coefficientconversion_coe
    ADD CONSTRAINT c_pk_coe PRIMARY KEY (coe_tax_code, coe_std_code, coe_qte_code, coe_date_debut, coe_date_fin, coe_org_code);


--
-- Name: tj_dfesttype_dft c_pk_dft; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_dfesttype_dft
    ADD CONSTRAINT c_pk_dft PRIMARY KEY (dft_df_identifiant, dft_tdf_code, dft_rang, dft_org_code);


--
-- Name: t_dispositifcomptage_dic c_pk_dic; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_dispositifcomptage_dic
    ADD CONSTRAINT c_pk_dic PRIMARY KEY (dic_code, dic_org_code);


--
-- Name: t_dispositiffranchissement_dif c_pk_dif; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_dispositiffranchissement_dif
    ADD CONSTRAINT c_pk_dif PRIMARY KEY (dif_code, dif_org_code);


--
-- Name: tg_dispositif_dis c_pk_dis; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tg_dispositif_dis
    ADD CONSTRAINT c_pk_dis PRIMARY KEY (dis_identifiant, dis_org_code);


--
-- Name: tj_dfestdestinea_dtx c_pk_dtx; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_dfestdestinea_dtx
    ADD CONSTRAINT c_pk_dtx PRIMARY KEY (dtx_dif_identifiant, dtx_tax_code, dtx_org_code);


--
-- Name: tj_conditionenvironnementale_env c_pk_env; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_conditionenvironnementale_env
    ADD CONSTRAINT c_pk_env PRIMARY KEY (env_stm_identifiant, env_date_debut, env_date_fin, env_org_code);


--
-- Name: t_lot_lot c_pk_lot; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_lot_lot
    ADD CONSTRAINT c_pk_lot PRIMARY KEY (lot_identifiant, lot_org_code);


--
-- Name: ts_masquecaracteristiquelot_mac c_pk_mac; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_masquecaracteristiquelot_mac
    ADD CONSTRAINT c_pk_mac PRIMARY KEY (mac_mal_id, mac_par_code);


--
-- Name: ts_masqueconditionsenvironnementales_mae c_pk_mae; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_masqueconditionsenvironnementales_mae
    ADD CONSTRAINT c_pk_mae PRIMARY KEY (mae_mao_id, mae_stm_identifiant);


--
-- Name: ts_masquelot_mal c_pk_mal_mas_id; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_masquelot_mal
    ADD CONSTRAINT c_pk_mal_mas_id PRIMARY KEY (mal_mas_id);


--
-- Name: ts_masqueope_mao c_pk_mao_mas_id; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_masqueope_mao
    ADD CONSTRAINT c_pk_mao_mas_id PRIMARY KEY (mao_mas_id);


--
-- Name: ts_masque_mas c_pk_mas_id; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_masque_mas
    ADD CONSTRAINT c_pk_mas_id PRIMARY KEY (mas_id);


--
-- Name: t_marque_mqe c_pk_mqe; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_marque_mqe
    ADD CONSTRAINT c_pk_mqe PRIMARY KEY (mqe_reference, mqe_org_code);


--
-- Name: t_operationmarquage_omq c_pk_omq; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_operationmarquage_omq
    ADD CONSTRAINT c_pk_omq PRIMARY KEY (omq_reference, omq_org_code);


--
-- Name: t_operation_ope c_pk_ope; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_operation_ope
    ADD CONSTRAINT c_pk_ope PRIMARY KEY (ope_identifiant, ope_org_code);


--
-- Name: t_ouvrage_ouv c_pk_ouv; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_ouvrage_ouv
    ADD CONSTRAINT c_pk_ouv PRIMARY KEY (ouv_identifiant, ouv_org_code);


--
-- Name: tj_pathologieconstatee_pco c_pk_pco; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_pathologieconstatee_pco
    ADD CONSTRAINT c_pk_pco PRIMARY KEY (pco_lot_identifiant, pco_pat_code, pco_loc_code, pco_org_code);


--
-- Name: t_periodefonctdispositif_per c_pk_per; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_periodefonctdispositif_per
    ADD CONSTRAINT c_pk_per PRIMARY KEY (per_dis_identifiant, per_org_code, per_date_debut, per_date_fin);


--
-- Name: tj_prelevementlot_prl c_pk_prl; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_prelevementlot_prl
    ADD CONSTRAINT c_pk_prl PRIMARY KEY (prl_pre_typeprelevement, prl_lot_identifiant, prl_code, prl_org_code);


--
-- Name: t_station_sta c_pk_sta; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_station_sta
    ADD CONSTRAINT c_pk_sta PRIMARY KEY (sta_code, sta_org_code);


--
-- Name: tj_stationmesure_stm c_pk_stm; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_stationmesure_stm
    ADD CONSTRAINT c_pk_stm PRIMARY KEY (stm_identifiant, stm_org_code);


--
-- Name: ts_taillevideo_tav c_pk_tav; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_taillevideo_tav
    ADD CONSTRAINT c_pk_tav PRIMARY KEY (tav_distance, tav_dic_identifiant, tav_org_code);


--
-- Name: tj_tauxechappement_txe c_pk_txe; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_tauxechappement_txe
    ADD CONSTRAINT c_pk_txe PRIMARY KEY (txe_sta_code, txe_org_code, txe_tax_code, txe_std_code, txe_date_debut, txe_date_fin);


--
-- Name: ts_taxonvideo_txv c_pk_txv; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_taxonvideo_txv
    ADD CONSTRAINT c_pk_txv PRIMARY KEY (txv_code, txv_org_code);


--
-- Name: t_dispositifcomptage_dic c_uk_dic_dis_identifiant; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_dispositifcomptage_dic
    ADD CONSTRAINT c_uk_dic_dis_identifiant UNIQUE (dic_dis_identifiant, dic_org_code);


--
-- Name: t_dispositiffranchissement_dif c_uk_dif_dis_identifiant; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_dispositiffranchissement_dif
    ADD CONSTRAINT c_uk_dif_dis_identifiant UNIQUE (dif_dis_identifiant, dif_org_code);


--
-- Name: ts_masqueordreaffichage_maa c_uk_maa; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_masqueordreaffichage_maa
    ADD CONSTRAINT c_uk_maa UNIQUE (maa_mal_id, maa_table, maa_valeur, maa_champdumasque);


--
-- Name: ts_maintenance_main c_uk_main_ticket; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_maintenance_main
    ADD CONSTRAINT c_uk_main_ticket UNIQUE (main_ticket);


--
-- Name: tj_prelevementlot_prl c_uk_prl_code; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_prelevementlot_prl
    ADD CONSTRAINT c_uk_prl_code UNIQUE (prl_code);


--
-- Name: tj_stationmesure_stm c_uk_stm; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_stationmesure_stm
    ADD CONSTRAINT c_uk_stm UNIQUE (stm_libelle);


--
-- Name: tj_stationmesure_stm c_uk_stm_identifiant; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_stationmesure_stm
    ADD CONSTRAINT c_uk_stm_identifiant UNIQUE (stm_identifiant);


--
-- Name: tj_stationmesure_stm c_uk_stm_libelle; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_stationmesure_stm
    ADD CONSTRAINT c_uk_stm_libelle UNIQUE (stm_libelle);


--
-- Name: t_dispositifcomptage_dic c_uq_dic; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_dispositifcomptage_dic
    ADD CONSTRAINT c_uq_dic UNIQUE (dic_dif_identifiant, dic_code);


--
-- Name: t_dispositiffranchissement_dif c_uq_dif; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_dispositiffranchissement_dif
    ADD CONSTRAINT c_uq_dif UNIQUE (dif_ouv_identifiant, dif_code);


--
-- Name: t_operation_ope c_uq_ope; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_operation_ope
    ADD CONSTRAINT c_uq_ope UNIQUE (ope_dic_identifiant, ope_date_debut, ope_date_fin);


--
-- Name: t_ouvrage_ouv c_uq_ouv; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_ouvrage_ouv
    ADD CONSTRAINT c_uq_ouv UNIQUE (ouv_sta_code, ouv_code);


--
-- Name: t_station_sta c_uq_sta_nom; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_station_sta
    ADD CONSTRAINT c_uq_sta_nom UNIQUE (sta_nom);


--
-- Name: ts_maintenance_main ts_maintenance_main_pkey; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_maintenance_main
    ADD CONSTRAINT ts_maintenance_main_pkey PRIMARY KEY (main_identifiant);


--
-- Name: ts_masque_mas ts_masque_mas_mas_code_key; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_masque_mas
    ADD CONSTRAINT ts_masque_mas_mas_code_key UNIQUE (mas_code);


--
-- Name: ts_masqueordreaffichage_maa ts_masqueordreaffichage_maa_pkey; Type: CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_masqueordreaffichage_maa
    ADD CONSTRAINT ts_masqueordreaffichage_maa_pkey PRIMARY KEY (maa_id);


--
-- Name: i_car_lotETpar; Type: INDEX; Schema: user_1; Owner: user_1
--

CREATE INDEX "i_car_lotETpar" ON user_1.tj_caracteristiquelot_car USING btree (car_lot_identifiant, car_par_code);


--
-- Name: i_lot_identifiant; Type: INDEX; Schema: user_1; Owner: user_1
--

CREATE INDEX i_lot_identifiant ON user_1.t_lot_lot USING btree (lot_identifiant);


--
-- Name: i_lot_lot_identifiant; Type: INDEX; Schema: user_1; Owner: user_1
--

CREATE INDEX i_lot_lot_identifiant ON user_1.t_lot_lot USING btree (lot_lot_identifiant);


--
-- Name: i_lot_ope_identifiant; Type: INDEX; Schema: user_1; Owner: user_1
--

CREATE INDEX i_lot_ope_identifiant ON user_1.t_lot_lot USING btree (lot_ope_identifiant);


--
-- Name: i_lot_taxETstd_code; Type: INDEX; Schema: user_1; Owner: user_1
--

CREATE INDEX "i_lot_taxETstd_code" ON user_1.t_lot_lot USING btree (lot_tax_code, lot_std_code);


--
-- Name: i_ope_date; Type: INDEX; Schema: user_1; Owner: user_1
--

CREATE INDEX i_ope_date ON user_1.t_operation_ope USING btree (ope_date_debut, ope_date_fin);


--
-- Name: i_ope_identifiant; Type: INDEX; Schema: user_1; Owner: user_1
--

CREATE INDEX i_ope_identifiant ON user_1.t_operation_ope USING btree (ope_identifiant);


--
-- Name: i_per_dates; Type: INDEX; Schema: user_1; Owner: user_1
--

CREATE INDEX i_per_dates ON user_1.t_periodefonctdispositif_per USING btree (per_date_debut, per_date_fin);


--
-- Name: i_txv_code; Type: INDEX; Schema: user_1; Owner: user_1
--

CREATE INDEX i_txv_code ON user_1.ts_taxonvideo_txv USING btree (txv_code);


--
-- Name: ts_masque_mas check_do_not_delete_defaut; Type: TRIGGER; Schema: user_1; Owner: user_1
--

CREATE TRIGGER check_do_not_delete_defaut BEFORE DELETE ON user_1.ts_masque_mas FOR EACH ROW EXECUTE FUNCTION user_1.do_not_delete_default();


--
-- Name: ts_masque_mas check_do_not_update_defaut; Type: TRIGGER; Schema: user_1; Owner: user_1
--

CREATE TRIGGER check_do_not_update_defaut BEFORE UPDATE ON user_1.ts_masque_mas FOR EACH ROW EXECUTE FUNCTION user_1.do_not_update_default();


--
-- Name: tj_coefficientconversion_coe trg_coe_date; Type: TRIGGER; Schema: user_1; Owner: user_1
--

CREATE TRIGGER trg_coe_date AFTER INSERT OR UPDATE ON user_1.tj_coefficientconversion_coe FOR EACH ROW EXECUTE FUNCTION user_1.fct_coe_date();


--
-- Name: t_operation_ope trg_ope_date; Type: TRIGGER; Schema: user_1; Owner: user_1
--

CREATE TRIGGER trg_ope_date AFTER INSERT OR UPDATE ON user_1.t_operation_ope FOR EACH ROW EXECUTE FUNCTION user_1.fct_ope_date();


--
-- Name: t_periodefonctdispositif_per trg_per_date; Type: TRIGGER; Schema: user_1; Owner: user_1
--

CREATE TRIGGER trg_per_date AFTER INSERT OR UPDATE ON user_1.t_periodefonctdispositif_per FOR EACH ROW EXECUTE FUNCTION user_1.fct_per_date();


--
-- Name: tj_tauxechappement_txe trg_txe_date; Type: TRIGGER; Schema: user_1; Owner: user_1
--

CREATE TRIGGER trg_txe_date AFTER INSERT OR UPDATE ON user_1.tj_tauxechappement_txe FOR EACH ROW EXECUTE FUNCTION user_1.fct_txe_date();


--
-- Name: tj_actionmarquage_act c_fk_act_lot_identifiant; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_actionmarquage_act
    ADD CONSTRAINT c_fk_act_lot_identifiant FOREIGN KEY (act_lot_identifiant, act_org_code) REFERENCES user_1.t_lot_lot(lot_identifiant, lot_org_code) ON UPDATE CASCADE;


--
-- Name: tj_actionmarquage_act c_fk_act_mqe_reference; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_actionmarquage_act
    ADD CONSTRAINT c_fk_act_mqe_reference FOREIGN KEY (act_mqe_reference, act_org_code) REFERENCES user_1.t_marque_mqe(mqe_reference, mqe_org_code) ON UPDATE CASCADE;


--
-- Name: tj_actionmarquage_act c_fk_act_org_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_actionmarquage_act
    ADD CONSTRAINT c_fk_act_org_code FOREIGN KEY (act_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


--
-- Name: t_bilanmigrationjournalier_bjo c_fk_bjo_org_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_bilanmigrationjournalier_bjo
    ADD CONSTRAINT c_fk_bjo_org_code FOREIGN KEY (bjo_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


--
-- Name: t_bilanmigrationjournalier_bjo c_fk_bjo_std_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_bilanmigrationjournalier_bjo
    ADD CONSTRAINT c_fk_bjo_std_code FOREIGN KEY (bjo_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code) ON UPDATE CASCADE;


--
-- Name: t_bilanmigrationjournalier_bjo c_fk_bjo_tax_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_bilanmigrationjournalier_bjo
    ADD CONSTRAINT c_fk_bjo_tax_code FOREIGN KEY (bjo_tax_code) REFERENCES ref.tr_taxon_tax(tax_code) ON UPDATE CASCADE;


--
-- Name: t_bilanmigrationmensuel_bme c_fk_bme_std_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_bilanmigrationmensuel_bme
    ADD CONSTRAINT c_fk_bme_std_code FOREIGN KEY (bme_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code) ON UPDATE CASCADE;


--
-- Name: t_bilanmigrationmensuel_bme c_fk_bme_tax_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_bilanmigrationmensuel_bme
    ADD CONSTRAINT c_fk_bme_tax_code FOREIGN KEY (bme_tax_code) REFERENCES ref.tr_taxon_tax(tax_code) ON UPDATE CASCADE;


--
-- Name: tj_caracteristiquelot_car c_fk_car_lot_identifiant; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_caracteristiquelot_car
    ADD CONSTRAINT c_fk_car_lot_identifiant FOREIGN KEY (car_lot_identifiant, car_org_code) REFERENCES user_1.t_lot_lot(lot_identifiant, lot_org_code) ON UPDATE CASCADE;


--
-- Name: tj_caracteristiquelot_car c_fk_car_org_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_caracteristiquelot_car
    ADD CONSTRAINT c_fk_car_org_code FOREIGN KEY (car_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


--
-- Name: tj_caracteristiquelot_car c_fk_car_par_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_caracteristiquelot_car
    ADD CONSTRAINT c_fk_car_par_code FOREIGN KEY (car_par_code) REFERENCES ref.tg_parametre_par(par_code) ON UPDATE CASCADE;


--
-- Name: tj_caracteristiquelot_car c_fk_car_val_identifiant; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_caracteristiquelot_car
    ADD CONSTRAINT c_fk_car_val_identifiant FOREIGN KEY (car_val_identifiant) REFERENCES ref.tr_valeurparametrequalitatif_val(val_identifiant) ON UPDATE CASCADE;


--
-- Name: tj_caracteristiquelot_car c_fk_car_val_par_identifiant; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_caracteristiquelot_car
    ADD CONSTRAINT c_fk_car_val_par_identifiant FOREIGN KEY (car_val_identifiant, car_par_code) REFERENCES ref.tr_valeurparametrequalitatif_val(val_identifiant, val_qal_code) ON UPDATE CASCADE;


--
-- Name: tj_coefficientconversion_coe c_fk_coe_org_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_coefficientconversion_coe
    ADD CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code) REFERENCES ref.ts_organisme_org(org_code) ON UPDATE CASCADE;


--
-- Name: tj_coefficientconversion_coe c_fk_coe_qte_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_coefficientconversion_coe
    ADD CONSTRAINT c_fk_coe_qte_code FOREIGN KEY (coe_qte_code) REFERENCES ref.tr_typequantitelot_qte(qte_code) ON UPDATE CASCADE;


--
-- Name: tj_coefficientconversion_coe c_fk_coe_std_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_coefficientconversion_coe
    ADD CONSTRAINT c_fk_coe_std_code FOREIGN KEY (coe_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code) ON UPDATE CASCADE;


--
-- Name: tj_coefficientconversion_coe c_fk_coe_tax_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_coefficientconversion_coe
    ADD CONSTRAINT c_fk_coe_tax_code FOREIGN KEY (coe_tax_code) REFERENCES ref.tr_taxon_tax(tax_code) ON UPDATE CASCADE;


--
-- Name: tj_dfesttype_dft c_fk_dft_df_identifiant; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_dfesttype_dft
    ADD CONSTRAINT c_fk_dft_df_identifiant FOREIGN KEY (dft_df_identifiant, dft_org_code) REFERENCES user_1.tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE;


--
-- Name: tj_dfesttype_dft c_fk_dft_org_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_dfesttype_dft
    ADD CONSTRAINT c_fk_dft_org_code FOREIGN KEY (dft_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


--
-- Name: tj_dfesttype_dft c_fk_dft_tdf_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_dfesttype_dft
    ADD CONSTRAINT c_fk_dft_tdf_code FOREIGN KEY (dft_tdf_code) REFERENCES ref.tr_typedf_tdf(tdf_code) ON UPDATE CASCADE;


--
-- Name: t_dispositifcomptage_dic c_fk_dic_dif_identifiant; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_dispositifcomptage_dic
    ADD CONSTRAINT c_fk_dic_dif_identifiant FOREIGN KEY (dic_dif_identifiant, dic_org_code) REFERENCES user_1.t_dispositiffranchissement_dif(dif_dis_identifiant, dif_org_code) ON UPDATE CASCADE;


--
-- Name: t_dispositifcomptage_dic c_fk_dic_dis_identifiant; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_dispositifcomptage_dic
    ADD CONSTRAINT c_fk_dic_dis_identifiant FOREIGN KEY (dic_dis_identifiant, dic_org_code) REFERENCES user_1.tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE;


--
-- Name: t_dispositifcomptage_dic c_fk_dic_org_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_dispositifcomptage_dic
    ADD CONSTRAINT c_fk_dic_org_code FOREIGN KEY (dic_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


--
-- Name: t_dispositifcomptage_dic c_fk_dic_tdc_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_dispositifcomptage_dic
    ADD CONSTRAINT c_fk_dic_tdc_code FOREIGN KEY (dic_tdc_code) REFERENCES ref.tr_typedc_tdc(tdc_code) ON UPDATE CASCADE;


--
-- Name: t_dispositiffranchissement_dif c_fk_dif_dis_identifiant; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_dispositiffranchissement_dif
    ADD CONSTRAINT c_fk_dif_dis_identifiant FOREIGN KEY (dif_dis_identifiant, dif_org_code) REFERENCES user_1.tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE;


--
-- Name: t_dispositiffranchissement_dif c_fk_dif_org_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_dispositiffranchissement_dif
    ADD CONSTRAINT c_fk_dif_org_code FOREIGN KEY (dif_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


--
-- Name: t_dispositiffranchissement_dif c_fk_dif_ouv_identifiant; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_dispositiffranchissement_dif
    ADD CONSTRAINT c_fk_dif_ouv_identifiant FOREIGN KEY (dif_ouv_identifiant, dif_org_code) REFERENCES user_1.t_ouvrage_ouv(ouv_identifiant, ouv_org_code) ON UPDATE CASCADE;


--
-- Name: tj_dfestdestinea_dtx c_fk_dtx_dif_identifiant; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_dfestdestinea_dtx
    ADD CONSTRAINT c_fk_dtx_dif_identifiant FOREIGN KEY (dtx_dif_identifiant, dtx_org_code) REFERENCES user_1.tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE;


--
-- Name: tj_dfestdestinea_dtx c_fk_dtx_org_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_dfestdestinea_dtx
    ADD CONSTRAINT c_fk_dtx_org_code FOREIGN KEY (dtx_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


--
-- Name: tj_dfestdestinea_dtx c_fk_dtx_tax_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_dfestdestinea_dtx
    ADD CONSTRAINT c_fk_dtx_tax_code FOREIGN KEY (dtx_tax_code) REFERENCES ref.tr_taxon_tax(tax_code) ON UPDATE CASCADE;


--
-- Name: tj_conditionenvironnementale_env c_fk_env_org_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_conditionenvironnementale_env
    ADD CONSTRAINT c_fk_env_org_code FOREIGN KEY (env_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


--
-- Name: tj_conditionenvironnementale_env c_fk_env_stm_identifiant; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_conditionenvironnementale_env
    ADD CONSTRAINT c_fk_env_stm_identifiant FOREIGN KEY (env_stm_identifiant, env_org_code) REFERENCES user_1.tj_stationmesure_stm(stm_identifiant, stm_org_code) ON UPDATE CASCADE;


--
-- Name: tj_conditionenvironnementale_env c_fk_env_val_identifiant; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_conditionenvironnementale_env
    ADD CONSTRAINT c_fk_env_val_identifiant FOREIGN KEY (env_val_identifiant) REFERENCES ref.tr_valeurparametrequalitatif_val(val_identifiant) ON UPDATE CASCADE;


--
-- Name: t_lot_lot c_fk_lot_dev_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_lot_lot
    ADD CONSTRAINT c_fk_lot_dev_code FOREIGN KEY (lot_dev_code) REFERENCES ref.tr_devenirlot_dev(dev_code) ON UPDATE CASCADE;


--
-- Name: t_lot_lot c_fk_lot_lot_identifiant; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_lot_lot
    ADD CONSTRAINT c_fk_lot_lot_identifiant FOREIGN KEY (lot_lot_identifiant, lot_org_code) REFERENCES user_1.t_lot_lot(lot_identifiant, lot_org_code) ON UPDATE CASCADE;


--
-- Name: t_lot_lot c_fk_lot_ope_identifiant; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_lot_lot
    ADD CONSTRAINT c_fk_lot_ope_identifiant FOREIGN KEY (lot_ope_identifiant, lot_org_code) REFERENCES user_1.t_operation_ope(ope_identifiant, ope_org_code) ON UPDATE CASCADE;


--
-- Name: t_lot_lot c_fk_lot_org_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_lot_lot
    ADD CONSTRAINT c_fk_lot_org_code FOREIGN KEY (lot_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


--
-- Name: t_lot_lot c_fk_lot_qte_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_lot_lot
    ADD CONSTRAINT c_fk_lot_qte_code FOREIGN KEY (lot_qte_code) REFERENCES ref.tr_typequantitelot_qte(qte_code) ON UPDATE CASCADE;


--
-- Name: t_lot_lot c_fk_lot_std_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_lot_lot
    ADD CONSTRAINT c_fk_lot_std_code FOREIGN KEY (lot_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code) ON UPDATE CASCADE;


--
-- Name: t_lot_lot c_fk_lot_tax_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_lot_lot
    ADD CONSTRAINT c_fk_lot_tax_code FOREIGN KEY (lot_tax_code) REFERENCES ref.tr_taxon_tax(tax_code) ON UPDATE CASCADE;


--
-- Name: ts_masqueordreaffichage_maa c_fk_maa_mal_id; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_masqueordreaffichage_maa
    ADD CONSTRAINT c_fk_maa_mal_id FOREIGN KEY (maa_mal_id) REFERENCES user_1.ts_masquelot_mal(mal_mas_id) ON DELETE CASCADE;


--
-- Name: ts_masquecaracteristiquelot_mac c_fk_mac_valeurqualitatifdefaut; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_masquecaracteristiquelot_mac
    ADD CONSTRAINT c_fk_mac_valeurqualitatifdefaut FOREIGN KEY (mac_valeurqualitatifdefaut) REFERENCES ref.tr_valeurparametrequalitatif_val(val_identifiant);


--
-- Name: ts_masqueconditionsenvironnementales_mae c_fk_mae_mao_id; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_masqueconditionsenvironnementales_mae
    ADD CONSTRAINT c_fk_mae_mao_id FOREIGN KEY (mae_mao_id) REFERENCES user_1.ts_masqueope_mao(mao_mas_id) ON DELETE CASCADE;


--
-- Name: ts_masqueconditionsenvironnementales_mae c_fk_mae_stm_identifiant; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_masqueconditionsenvironnementales_mae
    ADD CONSTRAINT c_fk_mae_stm_identifiant FOREIGN KEY (mae_stm_identifiant) REFERENCES user_1.tj_stationmesure_stm(stm_identifiant);


--
-- Name: ts_masquelot_mal c_fk_mal_mas_id; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_masquelot_mal
    ADD CONSTRAINT c_fk_mal_mas_id FOREIGN KEY (mal_mas_id) REFERENCES user_1.ts_masque_mas(mas_id) ON DELETE CASCADE;


--
-- Name: ts_masquecaracteristiquelot_mac c_fk_mal_mas_id; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_masquecaracteristiquelot_mac
    ADD CONSTRAINT c_fk_mal_mas_id FOREIGN KEY (mac_mal_id) REFERENCES user_1.ts_masquelot_mal(mal_mas_id) ON DELETE CASCADE;


--
-- Name: ts_masqueope_mao c_fk_mao_mas_id; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_masqueope_mao
    ADD CONSTRAINT c_fk_mao_mas_id FOREIGN KEY (mao_mas_id) REFERENCES user_1.ts_masque_mas(mas_id) ON DELETE CASCADE;


--
-- Name: t_marque_mqe c_fk_mqe_loc_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_marque_mqe
    ADD CONSTRAINT c_fk_mqe_loc_code FOREIGN KEY (mqe_loc_code) REFERENCES ref.tr_localisationanatomique_loc(loc_code) ON UPDATE CASCADE;


--
-- Name: t_marque_mqe c_fk_mqe_nmq_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_marque_mqe
    ADD CONSTRAINT c_fk_mqe_nmq_code FOREIGN KEY (mqe_nmq_code) REFERENCES ref.tr_naturemarque_nmq(nmq_code) ON UPDATE CASCADE;


--
-- Name: t_marque_mqe c_fk_mqe_omq_reference; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_marque_mqe
    ADD CONSTRAINT c_fk_mqe_omq_reference FOREIGN KEY (mqe_omq_reference, mqe_org_code) REFERENCES user_1.t_operationmarquage_omq(omq_reference, omq_org_code) ON UPDATE CASCADE;


--
-- Name: t_marque_mqe c_fk_mqe_org_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_marque_mqe
    ADD CONSTRAINT c_fk_mqe_org_code FOREIGN KEY (mqe_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


--
-- Name: t_operationmarquage_omq c_fk_omq_org_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_operationmarquage_omq
    ADD CONSTRAINT c_fk_omq_org_code FOREIGN KEY (omq_org_code) REFERENCES ref.ts_organisme_org(org_code) ON UPDATE CASCADE;


--
-- Name: t_operation_ope c_fk_ope_dic_identifiant; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_operation_ope
    ADD CONSTRAINT c_fk_ope_dic_identifiant FOREIGN KEY (ope_dic_identifiant, ope_org_code) REFERENCES user_1.tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE;


--
-- Name: t_operation_ope c_fk_ope_org_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_operation_ope
    ADD CONSTRAINT c_fk_ope_org_code FOREIGN KEY (ope_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


--
-- Name: t_ouvrage_ouv c_fk_ouv_nov_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_ouvrage_ouv
    ADD CONSTRAINT c_fk_ouv_nov_code FOREIGN KEY (ouv_nov_code) REFERENCES ref.tr_natureouvrage_nov(nov_code) ON UPDATE CASCADE;


--
-- Name: t_ouvrage_ouv c_fk_ouv_org_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_ouvrage_ouv
    ADD CONSTRAINT c_fk_ouv_org_code FOREIGN KEY (ouv_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


--
-- Name: t_ouvrage_ouv c_fk_ouv_sta_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_ouvrage_ouv
    ADD CONSTRAINT c_fk_ouv_sta_code FOREIGN KEY (ouv_sta_code, ouv_org_code) REFERENCES user_1.t_station_sta(sta_code, sta_org_code) ON UPDATE CASCADE;


--
-- Name: tj_pathologieconstatee_pco c_fk_pco_imp_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_pathologieconstatee_pco
    ADD CONSTRAINT c_fk_pco_imp_code FOREIGN KEY (pco_imp_code) REFERENCES ref.tr_importancepatho_imp(imp_code) ON UPDATE CASCADE;


--
-- Name: tj_pathologieconstatee_pco c_fk_pco_loc_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_pathologieconstatee_pco
    ADD CONSTRAINT c_fk_pco_loc_code FOREIGN KEY (pco_loc_code) REFERENCES ref.tr_localisationanatomique_loc(loc_code) ON UPDATE CASCADE;


--
-- Name: tj_pathologieconstatee_pco c_fk_pco_lot_identifiant; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_pathologieconstatee_pco
    ADD CONSTRAINT c_fk_pco_lot_identifiant FOREIGN KEY (pco_lot_identifiant, pco_org_code) REFERENCES user_1.t_lot_lot(lot_identifiant, lot_org_code) ON UPDATE CASCADE;


--
-- Name: tj_pathologieconstatee_pco c_fk_pco_org_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_pathologieconstatee_pco
    ADD CONSTRAINT c_fk_pco_org_code FOREIGN KEY (pco_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


--
-- Name: tj_pathologieconstatee_pco c_fk_pco_pat_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_pathologieconstatee_pco
    ADD CONSTRAINT c_fk_pco_pat_code FOREIGN KEY (pco_pat_code) REFERENCES ref.tr_pathologie_pat(pat_code) ON UPDATE CASCADE;


--
-- Name: t_periodefonctdispositif_per c_fk_per_dis_identifiant; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_periodefonctdispositif_per
    ADD CONSTRAINT c_fk_per_dis_identifiant FOREIGN KEY (per_dis_identifiant, per_org_code) REFERENCES user_1.tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE;


--
-- Name: t_periodefonctdispositif_per c_fk_per_org_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_periodefonctdispositif_per
    ADD CONSTRAINT c_fk_per_org_code FOREIGN KEY (per_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


--
-- Name: t_periodefonctdispositif_per c_fk_per_tar_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_periodefonctdispositif_per
    ADD CONSTRAINT c_fk_per_tar_code FOREIGN KEY (per_tar_code) REFERENCES ref.tr_typearretdisp_tar(tar_code) ON UPDATE CASCADE;


--
-- Name: tj_prelevementlot_prl c_fk_prl_loc_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_prelevementlot_prl
    ADD CONSTRAINT c_fk_prl_loc_code FOREIGN KEY (prl_loc_code) REFERENCES ref.tr_localisationanatomique_loc(loc_code) ON UPDATE CASCADE;


--
-- Name: tj_prelevementlot_prl c_fk_prl_lot_identifiant; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_prelevementlot_prl
    ADD CONSTRAINT c_fk_prl_lot_identifiant FOREIGN KEY (prl_lot_identifiant, prl_org_code) REFERENCES user_1.t_lot_lot(lot_identifiant, lot_org_code) ON UPDATE CASCADE;


--
-- Name: tj_prelevementlot_prl c_fk_prl_org_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_prelevementlot_prl
    ADD CONSTRAINT c_fk_prl_org_code FOREIGN KEY (prl_org_code) REFERENCES ref.ts_organisme_org(org_code) ON UPDATE CASCADE;


--
-- Name: tj_prelevementlot_prl c_fk_prl_pre_nom; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_prelevementlot_prl
    ADD CONSTRAINT c_fk_prl_pre_nom FOREIGN KEY (prl_pre_typeprelevement) REFERENCES ref.tr_prelevement_pre(pre_typeprelevement) ON UPDATE CASCADE;


--
-- Name: tj_prelevementlot_prl c_fk_prl_typeprelevement; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_prelevementlot_prl
    ADD CONSTRAINT c_fk_prl_typeprelevement FOREIGN KEY (prl_pre_typeprelevement) REFERENCES ref.tr_prelevement_pre(pre_typeprelevement) ON UPDATE CASCADE;


--
-- Name: t_station_sta c_fk_sta_org_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.t_station_sta
    ADD CONSTRAINT c_fk_sta_org_code FOREIGN KEY (sta_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


--
-- Name: tg_dispositif_dis c_fk_sta_org_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tg_dispositif_dis
    ADD CONSTRAINT c_fk_sta_org_code FOREIGN KEY (dis_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


--
-- Name: ts_taxonvideo_txv c_fk_std_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_taxonvideo_txv
    ADD CONSTRAINT c_fk_std_code FOREIGN KEY (txv_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code) ON UPDATE CASCADE;


--
-- Name: tj_stationmesure_stm c_fk_stm_org_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_stationmesure_stm
    ADD CONSTRAINT c_fk_stm_org_code FOREIGN KEY (stm_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


--
-- Name: tj_stationmesure_stm c_fk_stm_par_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_stationmesure_stm
    ADD CONSTRAINT c_fk_stm_par_code FOREIGN KEY (stm_par_code) REFERENCES ref.tg_parametre_par(par_code) ON UPDATE CASCADE;


--
-- Name: tj_stationmesure_stm c_fk_stm_sta_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_stationmesure_stm
    ADD CONSTRAINT c_fk_stm_sta_code FOREIGN KEY (stm_sta_code, stm_org_code) REFERENCES user_1.t_station_sta(sta_code, sta_org_code) ON UPDATE CASCADE;


--
-- Name: ts_taillevideo_tav c_fk_tav_dic_identifiant; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_taillevideo_tav
    ADD CONSTRAINT c_fk_tav_dic_identifiant FOREIGN KEY (tav_dic_identifiant, tav_org_code) REFERENCES user_1.t_dispositifcomptage_dic(dic_dis_identifiant, dic_org_code) ON UPDATE CASCADE;


--
-- Name: ts_taillevideo_tav c_fk_tav_org_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_taillevideo_tav
    ADD CONSTRAINT c_fk_tav_org_code FOREIGN KEY (tav_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


--
-- Name: tj_tauxechappement_txe c_fk_txe_ech_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_tauxechappement_txe
    ADD CONSTRAINT c_fk_txe_ech_code FOREIGN KEY (txe_ech_code) REFERENCES ref.tr_niveauechappement_ech(ech_code) ON UPDATE CASCADE;


--
-- Name: tj_tauxechappement_txe c_fk_txe_org_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_tauxechappement_txe
    ADD CONSTRAINT c_fk_txe_org_code FOREIGN KEY (txe_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


--
-- Name: tj_tauxechappement_txe c_fk_txe_sta_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_tauxechappement_txe
    ADD CONSTRAINT c_fk_txe_sta_code FOREIGN KEY (txe_sta_code, txe_org_code) REFERENCES user_1.t_station_sta(sta_code, sta_org_code) ON UPDATE CASCADE;


--
-- Name: tj_tauxechappement_txe c_fk_txe_std_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_tauxechappement_txe
    ADD CONSTRAINT c_fk_txe_std_code FOREIGN KEY (txe_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code) ON UPDATE CASCADE;


--
-- Name: tj_tauxechappement_txe c_fk_txe_tax_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.tj_tauxechappement_txe
    ADD CONSTRAINT c_fk_txe_tax_code FOREIGN KEY (txe_tax_code) REFERENCES ref.tr_taxon_tax(tax_code) ON UPDATE CASCADE;


--
-- Name: ts_taxonvideo_txv c_fk_txv_org_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_taxonvideo_txv
    ADD CONSTRAINT c_fk_txv_org_code FOREIGN KEY (txv_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


--
-- Name: ts_taxonvideo_txv c_fk_txv_std_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_taxonvideo_txv
    ADD CONSTRAINT c_fk_txv_std_code FOREIGN KEY (txv_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code) ON UPDATE CASCADE;


--
-- Name: ts_taxonvideo_txv c_fk_txv_tax_code; Type: FK CONSTRAINT; Schema: user_1; Owner: user_1
--

ALTER TABLE ONLY user_1.ts_taxonvideo_txv
    ADD CONSTRAINT c_fk_txv_tax_code FOREIGN KEY (txv_tax_code) REFERENCES ref.tr_taxon_tax(tax_code) ON UPDATE CASCADE;


--
-- Name: SCHEMA user_1; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA user_1 TO user_1;


--
-- PostgreSQL database dump complete
--

