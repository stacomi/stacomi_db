﻿-- Function: nat.fct_per_date()

-- DROP FUNCTION nat.fct_per_date();

CREATE OR REPLACE FUNCTION nat.fct_per_date()
  RETURNS trigger AS
$BODY$ 

 	DECLARE disCreation  	 	TIMESTAMP  ;
 	DECLARE disSuppression  	TIMESTAMP  ;
 	DECLARE nbChevauchements 	INTEGER    ;
 	BEGIN


 	 	-- Recuperation des dates du dispositif dans des variables


 	 	SELECT dis_date_creation, dis_date_suppression INTO disCreation, disSuppression
 	 	FROM   nat.tg_dispositif_dis
 	 	WHERE  dis_identifiant = NEW.per_dis_identifiant
 	 	AND    dis_org_code=NEW.per_org_code;

 	 	-- verification de la date de debut


 	 	IF ((NEW.per_date_debut < disCreation) OR (NEW.per_date_debut > disSuppression)) THEN
 	 		RAISE EXCEPTION 'Le debut de la periode doit etre inclus dans la periode d existence du dispositif.'  ;
 	 	END IF  ;





 	 	-- verification de la date de fin


 	 	IF ((NEW.per_date_fin < disCreation) OR (NEW.per_date_fin > disSuppression)) THEN
 	 	 	RAISE EXCEPTION 'La fin de la periode doit etre incluse dans la periode d existence du dispositif.'  ;

 	 	END IF  ;





 	 	-- verification des non-chevauchements pour les periodes du dispositif


 	 	SELECT COUNT(*) INTO nbChevauchements
 	 	FROM   nat.t_periodefonctdispositif_per
 	 	WHERE  (per_dis_identifiant,per_org_code) = (NEW.per_dis_identifiant,NEW.per_org_code)
 	 	       AND (per_date_debut, per_date_fin) OVERLAPS (NEW.per_date_debut, NEW.per_date_fin);
	-- Comme le trigger est declenche sur AFTER et non pas sur BEFORE, il faut (nbChevauchements > 1) et non pas >0, car l enregistrement a deja ete ajoute, donc il se chevauche avec lui meme, ce qui est normal !


 	 	IF (nbChevauchements > 1) THEN
 	 	 	RAISE EXCEPTION 'Les periodes ne peuvent se chevaucher.'  ;

 	 	END IF  ;
		RETURN NEW ;

 	END  ;


$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION nat.fct_per_date()
  OWNER TO postgres;
