﻿

SET client_min_messages TO WARNING;

select ref.updatesql('{"bgm","bresle","charente","fd80","iav","logrami","logramiang","migado","migradour","mrm","pmp","saumonrhin","smatah"}',
'
ALTER TABLE tj_caracteristiquelot_car
  DROP CONSTRAINT if exists c_fk_car_val_par_identifiant;

ALTER TABLE tj_caracteristiquelot_car
  DROP CONSTRAINT if exists c_fk_car_val_identifiant;

ALTER TABLE tj_caracteristiquelot_car
  ADD CONSTRAINT c_fk_car_val_identifiant FOREIGN KEY (car_val_identifiant, car_par_code)
      REFERENCES ref.tr_valeurparametrequalitatif_val (val_identifiant, val_qal_code) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
'); -- noms différentes pour la contrainte 
-- ne marche pas pour inra

update ref.tg_parametre_par set  
	(par_nom,
	par_definition)=
	('Age de mer déterminé depuis la taille',
	'Age de mer déterminé à partir de la taille du poisson qui est comparée à des valeurs de référence
	 obtenues lors de lectures d''écailles sur un bassin donné') 
  where par_code='A124';

/*
TICKET DE MARION SUR BREME DU DANUBE #221
*/
insert into ref.tr_taxon_tax (tax_code,tax_nom_latin,tax_nom_commun,tax_ntx_code,tax_rang)
values ('5204','Abramis sapa','Brême du Danube','15',(SELECT max(tax_rang)+1 FROM ref.tr_taxon_tax))

/*
TICKET DE MARION #227 problème de clé étrangère
*/
--Modification de la contrainte c_fk_ope_dic_identifiant qui ne permet pas de garantir qu'on met bien un --identifiant de DC dans ope_dic_identifiant (puisque table tg_dispositif_dis mélange DF et DC).
select ref.updatesql('{"bgm","bresle","charente","fd80","iav","logrami","logramiang","migado","migradour","mrm","pmp","saumonrhin","smatah"}',
'
alter table t_operation_ope drop constraint c_fk_ope_dic_identifiant;
alter table t_operation_ope add constraint c_fk_ope_dic_identifiant FOREIGN KEY (ope_dic_identifiant, ope_org_code) REFERENCES t_dispositifcomptage_dic (dic_dis_identifiant, dic_org_code) MATCH FULL ON UPDATE CASCADE ON DELETE NO ACTION;
');

/*
TICKET DE MARION
 #226 Modification de la clé étrangère pour les marques
*/
/*
Dans les bonnées BRESLE j'ai :
-- une marque 3058 de type Carlin 
-- une autre 3058 de type Spaghetti.
A ce jour on ne peut pas rentrer ces 2 marques dans la table t_marque_mqe car la clé primaire correspond à (mqe_reference, mqe_org_code).
Cette clé primaire est la clé étrangère de la table tj_actionmarquage_act.
Si on veut changer cette clé pour faire une contrainte triple du type (mqe_reference,mqe_nmq_code,mqe_org_code) il faut ajouter dans tj_actiomarquage_act la colonne act_mnq_code puis supprimer les contraintes de clé étrangère (tj_actionmarquage_act) et clé primaire (t_marque_mqe) puis les recréer avec les bons champs.
Voici le code SQL pour faire cela (à lancer sur tous les schémas dont chaque utilisateur dispose hors schéma public, ref et topology) :
*/
select ref.updatesql('{"bgm","charente","fd80","logrami","migado","migradour","nat","ngm","pmp","saumonrhin","seinormigr","smatah"}',
'
--Ajout de la colonne act_nmq_code dans tj_actionmarquage
ALTER TABLE tj_actionmarquage_act ADD COLUMN act_nmq_code character varying(4);
--Mise à jour de ce champ par rapport au champ mqe_nmq_code
UPDATE tj_actionmarquage_act SET act_nmq_code=m.mqe_nmq_code FROM t_marque_mqe m where act_mqe_reference=m.mqe_reference;
--On ajoute une contrainte not null sur le champ act_nmq_code
ALTER TABLE tj_actionmarquage_act ALTER COLUMN act_nmq_code SET NOT NULL;
--On supprime les contrainte de clé étrangère / clé primaire
ALTER TABLE tj_actionmarquage_act DROP CONSTRAINT c_fk_act_mqe_reference;
ALTER TABLE t_marque_mqe DROP CONSTRAINT c_pk_mqe;
--On les refait en ajoutant ce champ à la clé
ALTER TABLE t_marque_mqe ADD CONSTRAINT c_pk_mqe PRIMARY KEY (mqe_reference, mqe_nmq_code, mqe_org_code);
ALTER TABLE tj_actionmarquage_act ADD CONSTRAINT c_fk_act_mqe_reference FOREIGN KEY (act_mqe_reference, act_nmq_code, act_org_code) REFERENCES t_marque_mqe (mqe_reference, mqe_nmq_code, mqe_org_code) MATCH SIMPLE ON UPDATE CASCADE ON DELETE NO ACTION;
');
/*
TICKET DE MARION
 #224 Modification de la clé étrangère pour les marques
*/
/*
Ajout d'une contrainte not null sur t_marque_mqe.mqe_omq_reference
*/
select ref.updatesql('{"bgm","bresle","charente","fd80","iav","logrami","ngm","migado","migradour","pmp","saumonrhin","smatah"}',
'
alter table t_marque_mqe ALTER COLUMN mqe_omq_reference SET NOT NULL; 
');
/*
ticket 129

Entre la première présentation du saumon et le dernier passage il faudrait pouvoir stocker le temps dans un nouveau paramètre temps de passage.
*/
insert into ref.tg_parametre_par values
  ('TPSP' as par_code,
  'Temps de passage' as par_nom,
  's' as par_unite ,
  'BIOLOGIQUE' as par_nature ,
  'Temps de passage entre la première présentation d''un poisson et son passage effectif devant le dispositif de comptage' as par_definition);


/*
Demandes de Kevin pour migradour
Ajout de paramètres
*/
/*
o   Pour les poissons, il nous faudrait juste un paramètre quantitatif : celui de la Longueur de la Narine (mm).
o   Côté opération, il nous faudrait si possible 5 paramètres qualitatifs :
  Déversement du barrage (Oui/Non) ;
  Etat du cône (1 : Propre ; 2 : Intermédiaire ; 3 : Colmaté) ;
  Etat de la grille (1 : Propre ; 2 : Intermédiaire ; 3 : Colmatée) ;
  Niveau des eaux (1 : Eaux basses ; 2 : Eaux moyennes ; 3 : Eaux fortes) ;
  Tendance du débit (1 : Stabilité ; 2 : Diminution ; 3 : Augmentation ; 4 : Irrégularité).
*/

insert into ref.tg_parametre_par values
  ('LNAR', 
  'Longueur de narine', 
  'mm' ,
  'BIOLOGIQUE'  ,
  'Longueur de la narine' );
insert into ref.tr_parametrequantitatif_qan select 'LNAR';  

insert into ref.tg_parametre_par values
  ('FCTB',
  'Fonctionnement ouvrage',
  'logique',
  'ENVIRONNEMENTAL',
  'Ecoulement de l''ouvrage ou fonctionnement d''un organe d''un ouvrage');
   insert into ref.tr_parametrequalitatif_qal SELECT 'FCTB', 'VRAI/FAUX';
insert into ref.tr_valeurparametrequalitatif_val (val_identifiant,val_qal_code,	val_rang,val_libelle)
 SELECT 66, 'FCTB',1,'Fonctionne';
 insert into ref.tr_valeurparametrequalitatif_val (val_identifiant,val_qal_code,	val_rang,val_libelle)
 SELECT 67, 'FCTB',2,'Arrêt';

 
insert into ref.tg_parametre_par values
  ('ETCO',
  'Etat de colmatage',
  'Sans objet',
  'ENVIRONNEMENTAL',
  'Etat de colmatage d''une grille ou d''un cône d''un ouvrage');
  
 insert into ref.tr_parametrequalitatif_qal SELECT 'ETCO','Propre/Intermédiaire/Colmaté';
insert into ref.tr_valeurparametrequalitatif_val (val_identifiant,val_qal_code,	val_rang,val_libelle)
 SELECT 68, 'ETCO',1,'Propre';
 insert into ref.tr_valeurparametrequalitatif_val (val_identifiant,val_qal_code,val_rang,val_libelle)
 SELECT 69, 'ETCO',2,'Intermédiaire';
 insert into ref.tr_valeurparametrequalitatif_val (val_identifiant,val_qal_code,val_rang,val_libelle)
 SELECT 70, 'ETCO',3,'Colmaté';

 insert into ref.tg_parametre_par values
  ('NIVE',
  'Niveau des eaux',
  'Sans objet',
  'ENVIRONNEMENTAL',
  'Niveau des eaux');
  
 insert into ref.tr_parametrequalitatif_qal SELECT 'NIVE','Eaux basses/Eaux moyennes/Eaux fortes';
insert into ref.tr_valeurparametrequalitatif_val (val_identifiant,val_qal_code,	val_rang,val_libelle)
 SELECT 71, 'NIVE',1,'Eaux basses';
 insert into ref.tr_valeurparametrequalitatif_val (val_identifiant,val_qal_code,val_rang,val_libelle)
 SELECT 72, 'NIVE',2,'Eaux moyennes';
 insert into ref.tr_valeurparametrequalitatif_val (val_identifiant,val_qal_code,val_rang,val_libelle)
 SELECT 73, 'NIVE',3,'Eaux fortes';
  
 insert into ref.tg_parametre_par values
  ('TDEB',
  'Tendance du débit',
  'Sans objet',
  'ENVIRONNEMENTAL',
  'Tendance du débit');
  
 insert into ref.tr_parametrequalitatif_qal SELECT 'TDEB','Stabilité/Diminution/Augmentation/Irrégularité';
insert into ref.tr_valeurparametrequalitatif_val (val_identifiant,val_qal_code,	val_rang,val_libelle)
 SELECT 74, 'TDEB',1,'Stabilité';
 insert into ref.tr_valeurparametrequalitatif_val (val_identifiant,val_qal_code,val_rang,val_libelle)
 SELECT 75, 'TDEB',2,'Diminution';
 insert into ref.tr_valeurparametrequalitatif_val (val_identifiant,val_qal_code,val_rang,val_libelle)
 SELECT 76, 'TDEB',3,'Augmentation';
 insert into ref.tr_valeurparametrequalitatif_val (val_identifiant,val_qal_code,val_rang,val_libelle)
 SELECT 77, 'TDEB',4,'Irrégularité';
/*
!!!!!!!!!!!!!!!!!
LANCER vues_manquantes_recreation en adaptant le code pour chaque schéma
il y a des grant
!!!!!!!!!!!!!!!!!!!!!!!!!
*/
select ref.updatesql('{"iav"}',
'insert into ts_maintenance_main
(
  main_ticket,
  main_description
  ) values
  (221,''Insertion Breme du danube dans le référentiel taxon'')'
);