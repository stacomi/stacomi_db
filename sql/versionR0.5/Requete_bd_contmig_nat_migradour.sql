﻿--###################################################
--## Requête sur bd_contmig_nat - schéma migradour ##
--## Auteurs : Marion Legrand - LOGRAMI            ##
--##           Cédric Briand - EPTB-Vilaine        ##    
--###################################################

--Spécifie le schéma par défaut où aller chercher les tables appelées dans les requêtes
--(évite de remettre le nom du schéma à chaque fois)
SET search_path TO migradour, public;

--Avant de démarrer la requête en tant que telle je regarde tous les codes paramètres de lot qui sont rentrés dans votre schéma

SELECT car_par_code, count(*)
FROM tj_caracteristiquelot_car
group by 1

--Je me note le résultat de la requête ci-dessous pour avoir les codes paramètres qui vont bien et pouvoir écrire la requête
	--ATTENTION : en faisant ça je me rends compte que vous avez un code paramètre qui ne correspond à rien dans le référentiel des
	--codes --> LNAR
	--Ceci me fait me rendre compte qu'il vous manque une contraite sur votre table tj_caracteristiquelot_car
	--CONSTRAINT c_fk_car_par_code FOREIGN KEY (car_par_code)
	--      REFERENCES ref.tg_parametre_par (par_code) MATCH SIMPLE
	--      ON UPDATE CASCADE ON DELETE NO ACTION,
	--C'est cette contrainte qui assure qu'on ne met pas des codes de paramètre qui n'éxiste pas dans le référentiel !!
	--Le pb est à régler rapidement il faut me dire à quoi correspond LNAR et on règlera le pb

--age indiv=1789
--LT=1786
--poids lot =A111
--durée sejour eau douce =A123
--durée premier séjour en mer=A124
--taille vidéo=C001
--LF=1785
--Longueur machoire=1787
--Age écaille=A128
--LNAR --> ne correspond à aucun code dans le référentiel....


--#############
--## REQUETE
--#############

--Marion : J'écris la requête correspondant à la demande du mail de Kevin du 9/02/2018
	--Dans la partie SELECT je mets toutes les colonnes à afficher. Comme les noms des champs ne sont pas toujours très parlant
	--je fais systématiquement un AS "Nom_colonne" pour donner à chaque colonne le même nom que celui donné par Kevin dans son mail
	--le car_par_code est le champ qui stocke le nom du paramètre dans la table tj_caracteristiquelot_car (sexe, taille, êge, etc.)
	--je suis souvent obligée de faire des CASE WHEN...THEN...END --> en gros je dis : dans le cas où le paramètre est égal à un code donné
	--alors j'affiche les données contenues soit dans car_val_identifiant (= pour les paramètres qualitatifs) soit car_valeur_quantitaif (pour
	--les paramètres quantitatifs). Pour le champ sexe, je transforme également les codes de STACOMI en champ compréhensible grâce au CASE WHEN
	--(1=inconnu, 2=mâle, 3=femelle).
		--Dans la partie FROM je mets toutes les tables dont j'ai besoin en les liant les unes avec les autres grâce au JOIN ou LEFT JOIN
		--quand on fait un JOIN on prend tous les champs qui match entre 2 tables, quand on fait un LEFT JOIN on prend tous les champs de la table du 
		--dessus et seulement ceux de la table du dessous qui matchent. ça permet d'avoir des cases vides quand il n'y a pas de données (ex: vous n'avez
		--pas une donnée de taille pour tous les poissons mais dans ma requête je veux bien TOUS les poissons et n'afficher la taille que lorsqu'on l'a
		--sinon ne rien mettre dans le champ taille)
			--Dans le WHERE on réduit notre requête à des conditions : ici j'ai mis les 2 stations de la Nive et j'ai réduit à l'espèce saumon (code du taxon=2220)
				--Dans la partie order on trie les colonnes. J'ai donné les numéros des colonnes pour gagner du temps (plutôt que de réécrire les noms des champs
				--donc ici j'ai trié en premier lieu sur le nom des stations (1ere colonne), la date (6eme colonne) et le taxon (2eme colonne) 
	
-- Cédric : Kévin se plaint qu'il a plein de lignes répétées :-). Pour les éviter il faut passer d'un format long (plein de lignes) à un 
-- format large sur les caractéristiques quantitatives


-- Ci dessous je crée une vue un crosstab (format large et pas une valeur par colonne) des caracteristiques quantitatives
-- pour le créer, il faut
-- 1 installer tablefunc il faut lancer la commande CREATE EXTENSION tablefunc;
-- 2 utiliser la fonction tablefunc(text,text)
-- 21 dans le premier élément de tablefunc on met une requete ou on met dans la deuxième colonne des catégories (ici car_par_code) et dans la troisième les valeurs par catégories qu'on veut voir afficher dans les colonnes
-- 31  dans le second élement de tablefunc on met les catégories TRIEES ici : SELECT distinct car_par_code FROM tj_caracteristiquelot_car ...

-- A NE LANCER QU'UNE FOIS
CREATE EXTENSION tablefunc;

CREATE VIEW migradour.car_quan_sau as (
SELECT * FROM crosstab('SELECT car_lot_identifiant, car_par_code, car_valeur_quantitatif FROM tj_caracteristiquelot_car WHERE car_val_identifiant is NULL
                               and car_par_code in (''1785'',''1786'',''C001'',''A123'',''A124'')
			       ORDER BY car_lot_identifiant, car_par_code',
			'SELECT distinct car_par_code FROM tj_caracteristiquelot_car WHERE car_par_code in
					(''1785'',''1786'',''C001'',''A123'',''A124'') ORDER BY car_par_code')
			AS ct(car_lot_identifiant integer, 
			"Longueur fourche" numeric, --1785
			"Longueur totale" numeric, --1786
			"Âge de rivière" numeric, --A123
			"Âge de mer" numeric, --A124		
			"Longueur vidéo" numeric-- C001			
			)
			) ;
COMMENT ON VIEW  migradour.car_quan_sau is 'Vue des caracteristiques quantitatives de saumon dans un format large';	

-- Requète de Marion adaptée, voir commentaires ci-dessus
-- La différence c'est que je ne renvoit qu'une ligne pour les caractéristiques en sélectionnant seulement les car qualitatives (il n'y a qu'une caractéristiques qualitative qui est soit mâle, soit femelle soit inconnu pour le sexe
-- Les autres caractéristiques sont renvoyées par appel à la vue ci-dessus, Je remplace les case when de Marion par les noms des colonnes
-- marion avait mis type de taille, j'ai renvoyé trois colonnes à la place.... Pour ne pas avoir trois lignes avec des valeurs différentes.

SELECT  s.sta_nom AS "Nom station",
	t.tax_nom_commun AS "Espèce",
	extract(year FROM op.ope_date_debut) AS "Année",
	extract(month FROM op.ope_date_debut) AS "Mois",
	to_char(op.ope_date_debut,'WW') AS "Semaine",
	op.ope_date_debut AS "Date",
	lot_effectif AS "Effectif",
	lot_identifiant As "Identifiant",
	CASE WHEN c.car_par_code='1783' THEN 
		CASE WHEN c.car_val_identifiant='1' THEN 'Inconnu'
		WHEN c.car_val_identifiant='2' THEN 'Mâle' 
		WHEN c.car_val_identifiant='3' THEN 'Femelle' END
	END AS "Sexe",
	"Longueur totale",
	"Longueur vidéo",
	"Longueur fourche",
	"Âge de rivière",
	"Âge de mer",
	m.mqe_nmq_code AS "Type de marque",
	nmq.nmq_libelle AS "Libellé type de marque",
	CASE WHEN a.act_action='POSE' THEN a.act_mqe_reference END AS "N° de marque posée",
	CASE WHEN a.act_action='LECTURE' THEN a.act_mqe_reference END AS "N° de marque lue"
FROM t_station_sta s
LEFT JOIN t_ouvrage_ouv o on (s.sta_code=o.ouv_sta_code)
LEFT JOIN t_dispositiffranchissement_dif df on (df.dif_ouv_identifiant=o.ouv_identifiant)
LEFT JOIN t_dispositifcomptage_dic dc on (dc.dic_dif_identifiant=df.dif_dis_identifiant)
LEFT JOIN t_operation_ope op on (op.ope_dic_identifiant = dc.dic_dis_identifiant)
LEFT JOIN t_lot_lot l on (l.lot_ope_identifiant = op.ope_identifiant)
LEFT JOIN car_quan_sau cqs on (cqs.car_lot_identifiant=l.lot_identifiant) -- crosstab (voir code) adapté à la demande de Kévin
LEFT JOIN (select * from tj_caracteristiquelot_car where car_val_identifiant is not null) c on (c.car_lot_identifiant=l.lot_identifiant)
LEFT JOIN tj_actionmarquage_act a on (a.act_lot_identifiant=l.lot_identifiant)
LEFT JOIN t_marque_mqe m on (m.mqe_reference=a.act_mqe_reference) 
LEFT JOIN ref.tr_naturemarque_nmq nmq on (nmq.nmq_code=m.mqe_nmq_code)
JOIN ref.tr_taxon_tax t ON (t.tax_code::text = l.lot_tax_code::text)
JOIN ref.tr_stadedeveloppement_std st on (st.std_code=l.lot_std_code)
WHERE (s.sta_nom='Chopolo' or s.sta_nom='Halsou') and l.lot_tax_code='2220'
ORDER BY 1,6,2 



-- Ci dessous la requête originale de Marion et le script pour copy






/*

SELECT  s.sta_nom AS "Nom station",
	t.tax_nom_commun AS "Espèce",
	extract(year FROM op.ope_date_debut) AS "Année",
	extract(month FROM op.ope_date_debut) AS "Mois",
	to_char(op.ope_date_debut,'WW') AS "Semaine",
	op.ope_date_debut AS "Date",
	lot_effectif AS "Effectif",
	CASE WHEN c.car_par_code='1783' THEN 
		CASE WHEN c.car_val_identifiant='1' THEN 'Inconnu'
		WHEN c.car_val_identifiant='2' THEN 'Mâle' 
		WHEN c.car_val_identifiant='3' THEN 'Femelle' END
	END AS "Sexe",
	CASE WHEN c.car_par_code='1786' THEN 'Longueur totale'
	     WHEN c.car_par_code='C001' THEN 'Longueur vidéo'
	     WHEN c.car_par_code='1785' THEN 'Longueur fourche'
	     ELSE '' END AS "Type taille",
	CASE WHEN c.car_par_code='1786' or  c.car_par_code='C001' or  c.car_par_code='1785' THEN c.car_valeur_quantitatif END AS "Taille", 
	CASE WHEN c.car_par_code='A123' THEN c.car_valeur_quantitatif END AS "Âge de rivière",
	CASE WHEN c.car_par_code='A124' THEN c.car_valeur_quantitatif END AS "Âge de mer",
	m.mqe_nmq_code AS "Type de marque",
	nmq.nmq_libelle AS "Libellé type de marque",
	CASE WHEN a.act_action='POSE' THEN a.act_mqe_reference END AS "N° de marque posée",
	CASE WHEN a.act_action='LECTURE' THEN a.act_mqe_reference END AS "N° de marque lue"
FROM t_station_sta s
LEFT JOIN t_ouvrage_ouv o on (s.sta_code=o.ouv_sta_code)
LEFT JOIN t_dispositiffranchissement_dif df on (df.dif_ouv_identifiant=o.ouv_identifiant)
LEFT JOIN t_dispositifcomptage_dic dc on (dc.dic_dif_identifiant=df.dif_dis_identifiant)
LEFT JOIN t_operation_ope op on (op.ope_dic_identifiant = dc.dic_dis_identifiant)
LEFT JOIN t_lot_lot l on (l.lot_ope_identifiant = op.ope_identifiant)
LEFT JOIN tj_caracteristiquelot_car c on (c.car_lot_identifiant=l.lot_identifiant)
LEFT JOIN tj_actionmarquage_act a on (a.act_lot_identifiant=l.lot_identifiant)
LEFT JOIN t_marque_mqe m on (m.mqe_reference=a.act_mqe_reference) 
LEFT JOIN ref.tr_naturemarque_nmq nmq on (nmq.nmq_code=m.mqe_nmq_code)
JOIN ref.tr_taxon_tax t ON (t.tax_code::text = l.lot_tax_code::text)
JOIN ref.tr_stadedeveloppement_std st on (st.std_code=l.lot_std_code)
WHERE (s.sta_nom='Chopolo' or s.sta_nom='Halsou') and l.lot_tax_code='2220'
ORDER BY 1,6,2 --17460 lignes

*/










--##################################
--## EXPORT DE LA REQUÊTE EN CSV
--##################################

--on exporte la requête réalisée précédemment --> c'est la même chose. J'encapsule juste ma grosse requête dans un COPY(ma_requete) TO 'fichier_de_sortie.csv'
--Dans les options du COPY()TO on indique d'exporter également la ligne d'entête des colonnes =WITH CSV HEADER + on indique que le séparateur=';' + on met
--l'encodage de Windows 'WIN1252' pour ne pas avoir des soucis avec les accents dans le fichier d'export
--ATTENTION je vous conseille de mettre votre fichier de sortie dans un répertoire ne nécessitant pas de droit d'accès particulier, sinon vous risquez d'avoir une
--erreur du type "accès refusé". Pour ne pas être embêtée je mets souvent C:/Users/Public (ou nom d'utilisateur de la session). Attention aux "/" dans les chemins
--et non "\"
COPY(
SELECT  s.sta_nom AS "Nom station",
	t.tax_nom_commun AS "Espèce",
	extract(year FROM op.ope_date_debut) AS "Année",
	extract(month FROM op.ope_date_debut) AS "Mois",
	to_char(op.ope_date_debut,'WW') AS "Semaine",
	op.ope_date_debut AS "Date",
	lot_effectif AS "Effectif",
	CASE WHEN c.car_par_code='1783' THEN 
		CASE WHEN c.car_val_identifiant='1' THEN 'Inconnu'
		WHEN c.car_val_identifiant='2' THEN 'Mâle' 
		WHEN c.car_val_identifiant='3' THEN 'Femelle' END
	END AS "Sexe",
	CASE WHEN c.car_par_code='1786' THEN 'Longueur totale'
	     WHEN c.car_par_code='C001' THEN 'Longueur vidéo'
	     WHEN c.car_par_code='1785' THEN 'Longueur fourche'
	     ELSE '' END AS "Type taille",
	CASE WHEN c.car_par_code='1786' or  c.car_par_code='C001' or  c.car_par_code='1785' THEN c.car_valeur_quantitatif END AS "Taille", 
	CASE WHEN c.car_par_code='A123' THEN c.car_valeur_quantitatif END AS "Âge de rivière",
	CASE WHEN c.car_par_code='A124' THEN c.car_valeur_quantitatif END AS "Âge de mer",
	m.mqe_nmq_code AS "Type de marque",
	nmq.nmq_libelle AS "Libellé type de marque",
	CASE WHEN a.act_action='POSE' THEN a.act_mqe_reference END AS "N° de marque posée",
	CASE WHEN a.act_action='LECTURE' THEN a.act_mqe_reference END AS "N° de marque lue"
FROM t_station_sta s
LEFT JOIN t_ouvrage_ouv o on (s.sta_code=o.ouv_sta_code)
LEFT JOIN t_dispositiffranchissement_dif df on (df.dif_ouv_identifiant=o.ouv_identifiant)
LEFT JOIN t_dispositifcomptage_dic dc on (dc.dic_dif_identifiant=df.dif_dis_identifiant)
LEFT JOIN t_operation_ope op on (op.ope_dic_identifiant = dc.dic_dis_identifiant)
LEFT JOIN t_lot_lot l on (l.lot_ope_identifiant = op.ope_identifiant)
LEFT JOIN tj_caracteristiquelot_car c on (c.car_lot_identifiant=l.lot_identifiant)
LEFT JOIN tj_actionmarquage_act a on (a.act_lot_identifiant=l.lot_identifiant)
LEFT JOIN t_marque_mqe m on (m.mqe_reference=a.act_mqe_reference) 
LEFT JOIN ref.tr_naturemarque_nmq nmq on (nmq.nmq_code=m.mqe_nmq_code)
JOIN ref.tr_taxon_tax t ON (t.tax_code::text = l.lot_tax_code::text)
JOIN ref.tr_stadedeveloppement_std st on (st.std_code=l.lot_std_code)
WHERE (s.sta_nom='Chopolo' or s.sta_nom='Halsou') and l.lot_tax_code='2220'
ORDER BY 1,6,2 --17460 lignes
)
TO 'C:/Users/Public/2018_02_09_Req_migradour_Nive_sat.csv' WITH CSV HEADER DELIMITER ';' ENCODING 'WIN1252';

