﻿--###################################################
--## Requête sur bd_contmig_nat - schéma migradour ##
--## Auteur : Marion Legrand - LOGRAMI             ##
--###################################################

--Spécifie le schéma par défaut où aller chercher les tables appelées dans les requêtes
--(évite de remettre le nom du schéma à chaque fois)
SET search_path TO migradour, public;

--Avant de démarrer la requête en tant que telle je regarde tous les codes paramètres de lot qui sont rentrés dans votre schéma

select car_par_code, count(*)
from tj_caracteristiquelot_car
group by 1

--Je me note le résultat de la requête ci-dessous pour avoir les codes paramètres qui vont bien et pouvoir écrire la requête
	--ATTENTION : en faisant ça je me rends compte que vous avez un code paramètre qui ne correspond à rien dans le référentiel des
	--codes --> LNAR
	--Ceci me fait me rendre compte qu'il vous manque une contraite sur votre table tj_caracteristiquelot_car
	--CONSTRAINT c_fk_car_par_code FOREIGN KEY (car_par_code)
	--      REFERENCES ref.tg_parametre_par (par_code) MATCH SIMPLE
	--      ON UPDATE CASCADE ON DELETE NO ACTION,
	--C'est cette contrainte qui assure qu'on ne met pas des codes de paramètre qui n'éxiste pas dans le référentiel !!
	--Le pb est à régler rapidement il faut me dire à quoi correspond LNAR et on règlera le pb

--age indiv=1789
--LT=1786
--poids lot =A111
--durée sejour eau douce =A123
--durée premier séjour en mer=A124
--taille vidéo=C001
--LF=1785
--Longueur machoire=1787
--Age écaille=A128
--LNAR --> ne correspond à aucun code dans le référentiel....


--#############
--## REQUETE
--#############

--J'écris la requête correspondant à la demande du mail de Kevin du 9/02/2018
	--Dans la partie select je mets toutes les colonnes à afficher. Comme les noms des champs ne sont pas toujours très parlant
	--je fais systématiquement un AS "Nom_colonne" pour donner à chaque colonne le même nom que celui donné par Kevin dans son mail
	--le car_par_code est le champ qui stocke le nom du paramètre dans la table tj_caracteristiquelot_car (sexe, taille, êge, etc.)
	--je suis souvent obligée de faire des CASE WHEN...THEN...END --> en gros je dis : dans le cas où le paramètre est égal à un code donné
	--alors j'affiche les données contenues soit dans car_val_identifiant (= pour les paramètres qualitatifs) soit car_valeur_quantitaif (pour
	--les paramètres quantitatifs). Pour le champ sexe, je transforme également les codes de STACOMI en champ compréhensible grâce au CASE WHEN
	--(1=inconnu, 2=mâle, 3=femelle).
		--Dans la partie From je mets toutes les tables dont j'ai besoin en les liant les unes avec les autres grâce au JOIN ou Left join
		--quand on fait un JOIN on prend tous les champs qui match entre 2 tables, quand on fait un left join on prend tous les champs de la table du 
		--dessus et seulement ceux de la table du dessous qui matchent. ça permet d'avoir des cases vides quand il n'y a pas de données (ex: vous n'avez
		--pas une donnée de taille pour tous les poissons mais dans ma requête je veux bien TOUS les poissons et n'afficher la taille que lorsqu'on l'a
		--sinon ne rien mettre dans le champ taille)
			--Dans le where on réduit notre requête à des conditions : ici j'ai mis les 2 stations de la Nive et j'ai réduit à l'espèce saumon (code du taxon=2220)
				--Dans la partie order on trie les colonnes. J'ai donné les numéros des colonnes pour gagner du temps (plutôt que de réécrire les noms des champs
				--donc ici j'ai trié en premier lieu sur le nom des stations (1ere colonne), la date (6eme colonne) et le taxon (2eme colonne) 
	

Select  s.sta_nom AS "Nom station",
	t.tax_nom_commun AS "Espèce",
	extract(year from op.ope_date_debut) AS "Année",
	extract(month from op.ope_date_debut) AS "Mois",
	to_char(op.ope_date_debut,'WW') AS "Semaine",
	op.ope_date_debut AS "Date",
	lot_effectif AS "Effectif",
	CASE when c.car_par_code='1783' THEN 
		CASE when c.car_val_identifiant='1' THEN 'Inconnu'
		WHEN c.car_val_identifiant='2' THEN 'Mâle' 
		WHEN c.car_val_identifiant='3' THEN 'Femelle' END
	END AS "Sexe",
	CASE when c.car_par_code='1786' THEN 'Longueur totale'
	     WHEN c.car_par_code='C001' THEN 'Longueur vidéo'
	     WHEN c.car_par_code='1785' THEN 'Longueur fourche'
	     ELSE '' END AS "Type taille",
	CASE when c.car_par_code='1786' or  c.car_par_code='C001' or  c.car_par_code='1785' THEN c.car_valeur_quantitatif END AS "Taille", 
	CASE when c.car_par_code='A123' THEN c.car_valeur_quantitatif END AS "Âge de rivière",
	CASE when c.car_par_code='A124' THEN c.car_valeur_quantitatif END AS "Âge de mer",
	m.mqe_nmq_code AS "Type de marque",
	nmq.nmq_libelle AS "Libellé type de marque",
	CASE when a.act_action='POSE' THEN a.act_mqe_reference END AS "N° de marque posée",
	CASE when a.act_action='LECTURE' THEN a.act_mqe_reference END AS "N° de marque lue"
From t_station_sta s
left join t_ouvrage_ouv o on (s.sta_code=o.ouv_sta_code)
left join t_dispositiffranchissement_dif df on (df.dif_ouv_identifiant=o.ouv_identifiant)
left join t_dispositifcomptage_dic dc on (dc.dic_dif_identifiant=df.dif_dis_identifiant)
left join t_operation_ope op on (op.ope_dic_identifiant = dc.dic_dis_identifiant)
left join t_lot_lot l on (l.lot_ope_identifiant = op.ope_identifiant)
left join tj_caracteristiquelot_car c on (c.car_lot_identifiant=l.lot_identifiant)
left join tj_actionmarquage_act a on (a.act_lot_identifiant=l.lot_identifiant)
left join t_marque_mqe m on (m.mqe_reference=a.act_mqe_reference) 
left join ref.tr_naturemarque_nmq nmq on (nmq.nmq_code=m.mqe_nmq_code)
JOIN ref.tr_taxon_tax t ON (t.tax_code::text = l.lot_tax_code::text)
JOIN ref.tr_stadedeveloppement_std st on (st.std_code=l.lot_std_code)
where (s.sta_nom='Chopolo' or s.sta_nom='Halsou') and l.lot_tax_code='2220'
order by 1,6,2 --17460 lignes


--##################################
--## EXPORT DE LA REQUÊTE EN CSV
--##################################

--on exporte la requête réalisée précédemment --> c'est la même chose. J'encapsule juste ma grosse requête dans un copy(ma_requete) TO 'fichier_de_sortie.csv'
--Dans les options du copy()TO on indique d'exporter également la ligne d'entête des colonnes =WITH CSV HEADER + on indique que le séparateur=';' + on met
--l'encodage de Windows 'WIN1252' pour ne pas avoir des soucis avec les accents dans le fichier d'export
--ATTENTION je vous conseille de mettre votre fichier de sortie dans un répertoire ne nécessitant pas de droit d'accès particulier, sinon vous risquez d'avoir une
--erreur du type "accès refusé". Pour ne pas être embêtée je mets souvent C:/Users/Public (ou nom d'utilisateur de la session). Attention aux "/" dans les chemins
--et non "\"
Copy(
Select  s.sta_nom AS "Nom station",
	t.tax_nom_commun AS "Espèce",
	extract(year from op.ope_date_debut) AS "Année",
	extract(month from op.ope_date_debut) AS "Mois",
	to_char(op.ope_date_debut,'WW') AS "Semaine",
	op.ope_date_debut AS "Date",
	lot_effectif AS "Effectif",
	CASE when c.car_par_code='1783' THEN 
		CASE when c.car_val_identifiant='1' THEN 'Inconnu'
		WHEN c.car_val_identifiant='2' THEN 'Mâle' 
		WHEN c.car_val_identifiant='3' THEN 'Femelle' END
	END AS "Sexe",
	CASE when c.car_par_code='1786' THEN 'Longueur totale'
	     WHEN c.car_par_code='C001' THEN 'Longueur vidéo'
	     WHEN c.car_par_code='1785' THEN 'Longueur fourche'
	     ELSE '' END AS "Type taille",
	CASE when c.car_par_code='1786' or  c.car_par_code='C001' or  c.car_par_code='1785' THEN c.car_valeur_quantitatif END AS "Taille", 
	CASE when c.car_par_code='A123' THEN c.car_valeur_quantitatif END AS "Âge de rivière",
	CASE when c.car_par_code='A124' THEN c.car_valeur_quantitatif END AS "Âge de mer",
	m.mqe_nmq_code AS "Type de marque",
	nmq.nmq_libelle AS "Libellé type de marque",
	CASE when a.act_action='POSE' THEN a.act_mqe_reference END AS "N° de marque posée",
	CASE when a.act_action='LECTURE' THEN a.act_mqe_reference END AS "N° de marque lue"
From t_station_sta s
left join t_ouvrage_ouv o on (s.sta_code=o.ouv_sta_code)
left join t_dispositiffranchissement_dif df on (df.dif_ouv_identifiant=o.ouv_identifiant)
left join t_dispositifcomptage_dic dc on (dc.dic_dif_identifiant=df.dif_dis_identifiant)
left join t_operation_ope op on (op.ope_dic_identifiant = dc.dic_dis_identifiant)
left join t_lot_lot l on (l.lot_ope_identifiant = op.ope_identifiant)
left join tj_caracteristiquelot_car c on (c.car_lot_identifiant=l.lot_identifiant)
left join tj_actionmarquage_act a on (a.act_lot_identifiant=l.lot_identifiant)
left join t_marque_mqe m on (m.mqe_reference=a.act_mqe_reference) 
left join ref.tr_naturemarque_nmq nmq on (nmq.nmq_code=m.mqe_nmq_code)
JOIN ref.tr_taxon_tax t ON (t.tax_code::text = l.lot_tax_code::text)
JOIN ref.tr_stadedeveloppement_std st on (st.std_code=l.lot_std_code)
where (s.sta_nom='Chopolo' or s.sta_nom='Halsou') and l.lot_tax_code='2220'
order by 1,6,2 --17460 lignes
)
TO 'C:/Users/Public/2018_02_09_Req_migradour_Nive_sat.csv' WITH CSV HEADER DELIMITER ';' ENCODING 'WIN1252';

