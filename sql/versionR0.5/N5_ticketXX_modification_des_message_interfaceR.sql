﻿-- script créé le 22/12/2012

--select * from ref.ts_messagerlang_mrl where mrl_text like '%Pour de l''aide%'

-- ci dessous pour aide recherche d'un message en particulier
/*

select * from ref.ts_messager_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id
where msr_element='RefDC';

select * from ref.ts_messager_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id
where msr_element||'.'||msr_number='interface_graphique_menu.2.1';

select * from ref.ts_messager_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id
where msr_element||'.'||msr_number='Bilan_lot.4';

select * from ref.ts_messager_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id
where msr_element='interface_Bilan_lot' order by mrl_lang, cast(msr_number as integer)
*/
-- Bilan migration multiple 23/12/12

insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) 
	select max(msr_id)+1, 'interface_graphique_menu',2.14,'interface',FALSE,NULL from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Migration multiple"','French' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Migration multiple"','English' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Migración multiple"','Spanish' from ref.ts_messager_msr;

/*
select * from ref.ts_messager_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id
where msr_element='interface_graphique_menu' order by mrl_lang;
*/
-- msg$interface_graphique_menu.2.11 Existe déjà, autre traitement
update ref.ts_messagerlang_mrl set mrl_text='Traitement taille-âge salmonidés' where mrl_id=320;
update ref.ts_messagerlang_mrl set mrl_text='Tratamiento salmónidos tamaño - edad' where mrl_id=984;
update ref.ts_messagerlang_mrl set mrl_text='Treatment size - age salmonids' where mrl_id=652;

-- TODO check spanish
/*
select * from ref.ts_messager_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id
where msr_element='RefDC';
*/
insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) 
	select max(msr_id)+1, 'RefDC',8,'class',TRUE,NULL from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Le(s) DC(s) a(ont) ete selectionne(s) "','French' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"The DC(s) have been selected"','English' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"El(los) DC(s) ha sido seleccionado"','Spanish' from ref.ts_messager_msr;
-- TODO check spanish
/*
select * from ref.ts_messager_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id
where msr_element='RefTaxon';
*/
insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) 
	select max(msr_id)+1, 'RefTaxon',4,'class',TRUE,NULL from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Le(s) Taxons(s) a(ont) ete selectionne(s) "','French' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"The taxa(s) have been selected"','English' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"El(los) taxón(s) ha sido seleccionado"','Spanish' from ref.ts_messager_msr;
-- TODO check spanish



insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) 
	select max(msr_id)+1, 'RefTaxon',5,'class',TRUE,NULL from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Pas de valeur pour l''argument taxon"','French' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"No value for argument taxon"','English' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"No value for argument taxon"','Spanish' from ref.ts_messager_msr;
-- TODO check spanish


insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) 
	select max(msr_id)+1, 'RefTaxon',6,'class',TRUE,NULL from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Taxons non présents :"','French' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Taxa not present :"','English' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Taxa not present :"','Spanish' from ref.ts_messager_msr;

/*
select * from ref.ts_messager_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id
where msr_element='RefStades';
*/
insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) 
	select max(msr_id)+1, 'RefStades',7,'class',TRUE,NULL from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Pas de valeur pour l''argument stades"','French' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"No value for argument stage"','English' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"No value for argument stage"','Spanish' from ref.ts_messager_msr;
-- TODO check spanish
insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) 
	select max(msr_id)+1, 'RefStades',8,'class',TRUE,NULL from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Stades non présents :"','French' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Stage not present :"','English' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Stage not present"','Spanish' from ref.ts_messager_msr;


/*
report_mig_mult
select * from ref.ts_messager_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id
where msr_element='BilanMigration';
*/



insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) 
	select max(msr_id)+1, 'report_mig_mult',1,'class',FALSE,'Bouton d''action pour imprimer la commande générée par l''interface graphique' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Code"','French' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Code"','English' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Code"','Spanish' from ref.ts_messager_msr;

insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) 
	select max(msr_id)+1, 'report_mig_mult',2,'class',TRUE,'Message pour l''affichage de la commande out, quand le bilan n''est pas complet' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Il faut selectionner les DC, les taxons et les stades pour avoir une commande complète"','French' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Please select DC, taxa, and stages for a complete command"','English' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Please select DC, taxa, and stages for a complete command"','Spanish' from ref.ts_messager_msr;

insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) 
	select max(msr_id)+1, 'report_mig_mult',3,'class',TRUE,'Récupération des données' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"L''objet Bilan est stocke dans l''environnement envir_stacomi, ecrire report_mig_mult=get(''report_mig_mult'',envir_stacomi)"','French' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"The summary object is stored in environment envir_stacomi, write report_mig_mult=get(''report_mig_mult'',envir_stacomi)"','English' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"The summary object is stored in environment envir_stacomi, write report_mig_mult=get(''report_mig_mult'',envir_stacomi)"','Spanish' from ref.ts_messager_msr;

insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) 
	select max(msr_id)+1, 'report_mig_mult',4,'class',TRUE,'Récupération des données' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Les données brutes sont stockées dans report_mig_mult@data, les resultats sont stockés dans report_mig_mult@calcdata"','French' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Raw data are stored in report_mig_mult@data, processed data in report_mig_mult@calcdata"','English' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Raw data are stored in report_mig_mult@data, processed data in report_mig_mult@calcdata"','Spanish' from ref.ts_messager_msr;


/*
recherche d'un message particulier

select * from ref.ts_messager_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id where mrl_text like '%pour charger les migrations%'

select * from ref.ts_messager_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id
where msr_element='fungraph';
*/
update ref.ts_messagerlang_mrl set mrl_text='"Attention, il y a des quantite de lots rentrees pour un taxon autre que civelles, verifier"' where mrl_id=209;
update ref.ts_messagerlang_mrl set mrl_text='"Attention, there are batch quantities entered for another stage than glass eel, please check"' where mrl_id=541;


update ref.ts_messagerlang_mrl set mrl_text='c("mesure","calcule","expert","ponctuel")' where mrl_id=213;
update ref.ts_messagerlang_mrl set mrl_text='c("measured","calculated","expert","direct")' where mrl_id=545;
update ref.ts_messagerlang_mrl set mrl_text='c("mesure","calcule","expert","ponctuel")' where mrl_id=877;


/*
select * from ref.ts_messager_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id
where msr_element='RefStades';
*/
-- correction d'une erreur
update ref.ts_messagerlang_mrl set mrl_text='"Pour de l''aide cedric Briand - 02 99 90 88 44 - cedric.briand@eptb-vilaine.fr - https://groups.google.com/forum/?hl=fr#!forum/stacomi"' where mrl_id=298;

/*
select * from ref.ts_messager_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id
where msr_element='fungraph_civelle';
*/
-- abreviations
update ref.ts_messagerlang_mrl set mrl_text='Op' where mrl_id=535; -- a la place de Operation
update ref.ts_messagerlang_mrl set mrl_text='CD' where mrl_id=534; -- a la place de Counting device

CREATE ROLE test WITH LOGIN PASSWORD 'test' ;
CREATE database test OWNER test;


/*
BilanMigration
select * from ref.ts_messager_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id
where msr_element='BilanMigration';
*/


update ref.ts_messagerlang_mrl set mrl_text='"Pour accéder aux données calculées, tappez bilanMigration@calcdata"' where mrl_id=94;
update ref.ts_messagerlang_mrl set mrl_text='"To access calculated data, type bilanMigration@calcdata"' where mrl_id=426;
update ref.ts_messagerlang_mrl set mrl_text='"Para acceder a los datos calculado , tipo bilanMigration @ calcdata")' where mrl_id=758;



/*
select * from ref.vuemessage 
where msr_element='BilanFonctionnementDF';
select * from ref.ts_messager_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id
where msr_element='BilanFonctionnementDC';
*/
-- msg$interface_graphique_menu.2.11 Existe déjà, autre traitement

update ref.ts_messagerlang_mrl set mrl_text='"Fishway operation"' where mrl_id=419;
update ref.ts_messagerlang_mrl set mrl_text='c("Func.","Stop")' where mrl_id=408;

insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) 
	select max(msr_id)+1, 'BilanFonctionnementDF',11,'class',FALSE,'per_tar_code' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), 'c("Fct. normal","Arr. ponctuels","Arrêts","Dysfonct.","Inconnu")',
	'French' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), 'c("Normal operation","Operational stop","Stop","Dysfunct.","Unknown")',
	'English' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), 'c("Funcio. normal","parada. operativa","Stop","Disfunción.","Desconocido")',
	'Spanish' from ref.ts_messager_msr;



insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) 
	select max(msr_id)+1, 'BilanFonctionnementDF',12,'class',FALSE,'per_tar_code' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Durée en jours (types de fonctionnement):"',
	'French' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Duration in days (operation type):"',
	'English' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Duración en días (tipo de operación):"',
	'Spanish' from ref.ts_messager_msr;

insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) 
	select max(msr_id)+1, 'BilanFonctionnementDF',13,'class',FALSE,'per_tar_code' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Durée en jours (fonctionnement):"',
	'French' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Duration in days (operation):"',
	'English' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Duración en días (operación):"',
	'Spanish' from ref.ts_messager_msr;


/*
BilanMigration
select * from ref.ts_messager_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id
where msr_element like '%graphique%';
*/
/* ***********************
NEW CHANGE APPLY SOPHIE
************************/

delete from ref.ts_messagerlang_mrl where mrl_msr_id=299;
delete from ref.ts_messager_msr where msr_id=299;



update ref.ts_messagerlang_mrl set mrl_text='"Probleme lors du test, le lien ODBC fonctionne mais ne pointe pas vers la base version 0.5, verifiez le lien ODBC"' where mrl_id=331;
update ref.ts_messagerlang_mrl set mrl_text='""Problem during the test, the ODBC link works but doesn''t point to the database 0.5, check the ODBC link""' where mrl_id=663;
update ref.ts_messagerlang_mrl set mrl_text='"Problemas durante la prueba, la conexión ODBC funciona, pero no señala a la base de la versión 0.5, verificar el enlace ODBC")' where mrl_id=995;




insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) 
	select max(msr_id)+1, 'BilanOperation',1,'class',FALSE,'connect' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Chargement des données des opérations:"',
	'French' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Loading data for operations"',
	'English' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"La carga de datos para las operaciones:"',
	'Spanish' from ref.ts_messager_msr;




/*
select * from ref.vuemessage 
where msr_element='Bilan_carlot';
select * from ref.ts_messager_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id
where msr_element='BilanFonctionnementDC';
*/

update ref.ts_messager_msr set msr_element='Bilan_carlot' where msr_element='Bilan_lot';

/*
select * from ref.vuemessage 
where msr_element='Bilan_carlot';
select * from ref.ts_messager_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id
where msr_element='Bilan_carlot';
*/

update ref.ts_messagerlang_mrl set 
mrl_text='"Sample characteristics have been loaded from the database"' 
where mrl_id=345;


update ref.ts_messagerlang_mrl set 
mrl_text=
'"Pour recuperer le tableau, tapper : bilan_lot=get(''bilan_lot'',envir_stacomi)"'
where mrl_id=15;
update ref.ts_messagerlang_mrl set 
mrl_text=
'"To obtain te table, type : bilan_lot=get(''bilan_lot'',envir_stacomi)"'
where mrl_id=347;
update ref.ts_messagerlang_mrl set 
mrl_text=
'"Para recuperar la tabla, escribir: bilan_lot=get(''bilan_lot'',envir_stacomi)"'
where mrl_id=679;


update ref.ts_messagerlang_mrl set 
mrl_text=
'"Pour recuperer l''objet graphique, tapper : g<-get("g",envir_stacomi), voir http://trac.eptb-vilaine.fr:8066/tracstacomi/wiki/Recette%20BilanLot pour de l''aide"'
where mrl_id=16;
update ref.ts_messagerlang_mrl set 
mrl_text=
'"To obtain the graphical object, type :  g<-get("g",envir_stacomi), see http://trac.eptb-vilaine.fr:8066/tracstacomi/wiki/Recette%20BilanLot for help"""'
where mrl_id=348;
update ref.ts_messagerlang_mrl set 
mrl_text=
'"Para recuperar el objeto gráfico, escribir: g<-get("g",envir_stacomi), see http://trac.eptb-vilaine.fr:8066/tracstacomi/wiki/Recette%20BilanLot for help"'
where mrl_id=680;


/*
select * from ref.ts_messager_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id
where msr_element='interface_graphique_menu' order by mrl_lang;
*/
-- msg$interface_graphique_menu.2.11 Existe déjà, autre traitement
update ref.ts_messagerlang_mrl set mrl_text='Traitement taille-âge salmonidés' where mrl_id=320;
update ref.ts_messagerlang_mrl set mrl_text='Tratamiento salmónidos tamaño - edad' where mrl_id=984;
update ref.ts_messagerlang_mrl set mrl_text='Treatment size - age salmonids' where mrl_id=652;

-- Bilans Annuels

update ref.ts_messagerlang_mrl set mrl_text='Annuels' where mrl_id=321;
update ref.ts_messagerlang_mrl set mrl_text='Anual' where mrl_id=985;
update ref.ts_messagerlang_mrl set mrl_text='Yearly' where mrl_id=653;


insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) 
	select max(msr_id)+1, 'BilanAnnuels',1,'class',FALSE,'methode charge, récupération de l''objet' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"L''objet BilanAnnuels est stocké dans l''environnement stacomi, tappez bilA<-get("bilanAnnuels",envir_stacomi)"','French' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"The object BilanAnnuels is stored in the stacomi environment, type bilA <-get("bilanAnnuels",envir_stacomi)"','English' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"El objeto BilanAnnuels se almacena en el entorno stacomi, el tipo  bilA<-get("bilanAnnuels",envir_stacomi)"','Spanish' from ref.ts_messager_msr;


-- msg$interface_graphique_menu.2.11 Existe déjà, autre traitement
update ref.ts_messagerlang_mrl set mrl_text='Anguilles argentées' where mrl_id=319;
update ref.ts_messagerlang_mrl set mrl_text='Anguilas plateadas' where mrl_id=983;
update ref.ts_messagerlang_mrl set mrl_text='Silver eel' where mrl_id=651;	

-- bilan Argentées

insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) 
	select max(msr_id)+1, 'BilanArgentee',1,'class',FALSE,'methode connect, message de réussite' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Les données ont été chargées"','French' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Data loaded"','English' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Los datos se han cargado"','Spanish' from ref.ts_messager_msr;


insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) 
	select max(msr_id)+1, 'BilanArgentee',2,'class',FALSE,'methode calcul, error for no data' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"Pas de données d''argentées ou d''anguilles jaunes sur la période demandée"','French' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"No data of silver or yellow eel on the selected period"','English' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"No hay datos de plata o anguila amarilla en el período seleccionado"','Spanish' from ref.ts_messager_msr;



-- bd_conmig_nat serveur IAV a jour ici
-- 