﻿

with annualcoe as (
select extract("year" from coe_date_debut) as annee, * from iav.tj_coefficientconversion_coe),
coeff2019 as (
select coe_tax_code, 
coe_std_code,
coe_qte_code,
coe_date_debut,
coe_date_fin,
coe_valeur_coefficient,
coe_commentaires,
'SMATAH' as coe_org_code
from  annualcoe where annee=2019)
SELECT * from coeff2019

 psql -U postgres -h 192.168.1.198 bd_contmig_nat
\copy smatah.tj_coefficientconversion_coe FROM 'C:/temp/coe.csv'  with delimiter ';' CSV HEADER