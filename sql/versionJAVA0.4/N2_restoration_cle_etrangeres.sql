﻿/*
 Attention lancer ticket_81_mise_en_conformité sandre avant (ajout de colonnes et de tables)
 Ce script aura l'heureuse propriété de restaurer les clés étrangères qui ont sauté lors de changements de version.
 N2
 */




SET client_min_messages TO WARNING;
set search_path to iav,public;
--"iav"
-- select ref.updatesql('{"iav"}',


/* 
Mise à jour du référentiel géographique pour les bases compatibles postgis
--ticket 61 
*/
-- table stations
select ref.updatesql('{"azti","charente","inra","logrami","migado","migradour","mrm","nat","pmp","saumonrhin"}',
'
alter table t_station_sta  DROP CONSTRAINT IF EXISTS c_ck_sta_coordonnee_x; 
alter table t_station_sta  DROP CONSTRAINT IF EXISTS c_ck_sta_coordonnee_y; 
alter table t_station_sta add column geom geometry(Point,27572);
');


select ref.updatesql('{"azti","charente","inra","logrami","migradour","mrm","pmp","saumonrhin"}',
'

update t_station_sta set geom=ST_SetSRID(ST_MakePoint(sta_coordonnee_x, sta_coordonnee_y),27572);

update t_station_sta set sta_coordonnee_x=st_x(st_transform(geom,2154));
update t_station_sta set sta_coordonnee_y=st_y(st_transform(geom,2154));

comment on column t_station_sta.sta_coordonnee_x is ''Coordonnée x en Lambert 93'';
comment on column t_station_sta.sta_coordonnee_y is ''Coordonnée y en Lambert 93'';
alter table t_station_sta  ADD CONSTRAINT c_ck_sta_coordonnee_x 
CHECK (sta_coordonnee_x >= -400000 AND sta_coordonnee_x < 1300000); 
alter table t_station_sta  DROP CONSTRAINT IF EXISTS c_ck_sta_coordonnee_y; 
 alter table t_station_sta  ADD CONSTRAINT c_ck_sta_coordonnee_y 
 CHECK (sta_coordonnee_y >= 6000000 AND sta_coordonnee_y < 7100000);
 -- dans le même coup fait la transfo et change SRID
ALTER TABLE t_station_sta
 ALTER COLUMN geom TYPE geometry(POINT, 2154) USING ST_SetSRID(st_transform(geom,2154),2154) ;
');


-- table ouvrages
select ref.updatesql('{"azti","charente","inra","logrami","migado","migradour","mrm","pmp","saumonrhin"}',
'

alter table t_ouvrage_ouv  DROP CONSTRAINT IF EXISTS c_ck_ouv_coordonnee_x; 
alter table t_ouvrage_ouv  DROP CONSTRAINT IF EXISTS c_ck_ouv_coordonnee_y; 
alter table t_ouvrage_ouv add column geom geometry(Point,27572);
');

select ref.updatesql('{"azti","charente","inra","logrami","migado","migradour","mrm","pmp","saumonrhin"}',
'
update t_ouvrage_ouv set geom=st_geomfromtext(''POINT('' || ouv_coordonnee_x || '' '' || ouv_coordonnee_y || '')'',27572);



update t_ouvrage_ouv set ouv_coordonnee_x=st_x(st_transform(geom,2154));
update t_ouvrage_ouv set ouv_coordonnee_y=st_y(st_transform(geom,2154));

comment on column t_ouvrage_ouv.ouv_coordonnee_x is ''Coordonnée x en Lambert 93'';
comment on column t_ouvrage_ouv.ouv_coordonnee_y is ''Coordonnée y en Lambert 93'';

 alter table t_ouvrage_ouv  ADD CONSTRAINT c_ck_ouv_coordonnee_x 
 CHECK (ouv_coordonnee_x >= -400000 AND ouv_coordonnee_x < 1300000); 
 alter table t_ouvrage_ouv  ADD CONSTRAINT c_ck_ouv_coordonnee_y 
 CHECK (ouv_coordonnee_y >= 6000000 AND ouv_coordonnee_y < 7100000);
ALTER TABLE t_ouvrage_ouv
 ALTER COLUMN geom TYPE geometry(POINT, 2154) USING ST_SetSRID(st_transform(geom,2154),2154) ;

');

/* 
Pour les autres bases lancer
--ticket 61 
*/
-- station

select ref.updatesql('{"azti","charente","inra","logrami","migado","migradour","mrm","pmp","saumonrhin"}',
'
alter table t_station_sta  DROP CONSTRAINT IF EXISTS c_ck_sta_coordonnee_x; 
alter table t_station_sta  DROP CONSTRAINT IF EXISTS c_ck_sta_coordonnee_y; 
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (295059.796305358,6724814.04553663) where sta_code=''M4560001'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (316917.090092589,6731967.13390015) where sta_code=''M4440002'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (325073.677572592,6814181.00186765) where sta_code=''M4220001'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (1061572.35871031,6853954.19857137) where sta_code=''M0167002'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (1074808.67141229,6871295.99509865) where sta_code=''M167001'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (692356.242130878,6984057.63646068) where sta_code=''M4800001'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (616163.819341197,7001125.00260636) where sta_code=''M4800002'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (684789.536986487,6982330.28083399) where sta_code=''M4800003'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (845804.131053166,6321367.23197189) where sta_code=''MRM02'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (832217.265058218,6304181.95358149) where sta_code=''MRM01'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (837975.408842402,6334511.77994618) where sta_code=''MRM03'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (877398.475284672,6295769.16533951) where sta_code=''MRM04'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (841903.553816507,6271683.17641858) where sta_code=''MRM05'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (816914.061815432,6263016.66758402) where sta_code=''MRM06'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (173918.253114679,6812115.8892297) where sta_code=''M4290001'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (222197.20473166,6767853.93630765) where sta_code=''M4560001'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (385173.881789788,6844913.58105386) where sta_code=''M4500001'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (781493.671882723,6612717.75456281) where sta_code=''K1710001'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (512159.3586842,6637476.06409451) where sta_code=''L3860001'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (731741.17912543,6560330.8595322) where sta_code=''K3030001'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (734105.840170635,6637662.01183779) where sta_code=''K1580001'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (739698.970470657,6443979.56550937) where sta_code=''K2430001'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (753064.249531239,6427789.43994298) where sta_code=''K2430002'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (562919.84091293,6560253.97573747) where sta_code=''L5870001'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (383990.51495565,6276573.72575914) where sta_code=''5640012'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (524332.235458853,6655354.10022039) where sta_code=''L6370001'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (409267.963502858,6260689.83158306) where sta_code=''5640001'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (419353.461571104,6229413.63106304) where sta_code=''5640002'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (302062.327871579,6734366.22003421) where sta_code=''M4560003'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (337742.849305244,6755363.7973388) where sta_code=''M4350001'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (316741.147904605,6732081.48329355) where sta_code=''M4440001'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (316064.24517485,6744198.29539473) where sta_code=''M4560002'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (369794.258943853,6825914.52124732) where sta_code=''M4350001'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (323812.010218444,6842993.65203004) where sta_code=''M4220002'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (324401.814361328,6842401.2282521) where sta_code=''M4220003'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (285300.872960365,6838969.03974594) where sta_code=''M4220004'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (166262.737611886,6842480.28893417) where sta_code=''M4290002'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (175409.736715973,6819156.2300332) where sta_code=''M4290003'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (159868.149933445,6778726.35106166) where sta_code=''M4290004'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (372159.725647366,6278359.94072426) where sta_code=''5640003'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (389354.694770174,6275340.97017052) where sta_code=''5640013'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (385307.119233168,6245286.46438079) where sta_code=''5640006'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (339780.992954959,6264780.67859091) where sta_code=''5640007'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (341228.643676194,6262849.90484545) where sta_code=''5640008'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (401274.036682932,6246649.73108095) where sta_code=''5640004'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (395038.0384792,6253613.49692893) where sta_code=''5640011'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (331190.099650525,6260119.9394643) where sta_code=''5640010'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (328183.894368628,6262146.33415972) where sta_code=''5640009'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (407934.900667682,6235602.91480236) where sta_code=''5640005'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (505390,6224180) where sta_code=''5'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (573410,6279590) where sta_code=''6'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (526646,6419631) where sta_code=''7'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (528240,6336880) where sta_code=''1'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (512999,6419000) where sta_code=''2'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (555230,6244950) where sta_code=''3'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (509980,6224640) where sta_code=''4'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (438327.365989334,6514494.95730385) where sta_code=''M516001'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (289946.768385378,6253512.88749266) where sta_code=''oria'';
UPDATE t_station_sta set (sta_coordonnee_x,sta_coordonnee_y) = (591628.780445827,6992728.82712897) where sta_code=''Bresle'';
comment on column t_station_sta.sta_coordonnee_x is ''Coordonnée x en Lambert 93'';
comment on column t_station_sta.sta_coordonnee_y is ''Coordonnée y en Lambert 93'';
alter table t_station_sta  ADD CONSTRAINT c_ck_sta_coordonnee_x 
CHECK (sta_coordonnee_x >= -400000 AND sta_coordonnee_x < 1300000); 
alter table t_station_sta  DROP CONSTRAINT IF EXISTS c_ck_sta_coordonnee_y; 
 alter table t_station_sta  ADD CONSTRAINT c_ck_sta_coordonnee_y 
 CHECK (sta_coordonnee_y >= 6000000 AND sta_coordonnee_y < 7500000);
 ');
 
--ouvrage
select ref.updatesql('{"azti","charente","inra","logrami","migado","migradour","mrm","pmp","saumonrhin"}',
'
 alter table t_ouvrage_ouv  DROP CONSTRAINT IF EXISTS c_ck_ouv_coordonnee_x; 
 alter table t_ouvrage_ouv  DROP CONSTRAINT IF EXISTS c_ck_ouv_coordonnee_y; 
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (295059.796305358,6724814.04553663) where ouv_sta_code=''M4560001'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (316917.090092589,6731967.13390015) where ouv_sta_code=''M4440002'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (325073.677572592,6814181.00186765) where ouv_sta_code=''M4220001'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (1061572.35871031,6853954.19857137) where ouv_sta_code=''M0167002'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (1074808.67141229,6871295.99509865) where ouv_sta_code=''M167001'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (692356.242130878,6984057.63646068) where ouv_sta_code=''M4800001'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (616163.819341197,7001125.00260636) where ouv_sta_code=''M4800002'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (684789.536986487,6982330.28083399) where ouv_sta_code=''M4800003'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (845804.131053166,6321367.23197189) where ouv_sta_code=''MRM02'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (832217.265058218,6304181.95358149) where ouv_sta_code=''MRM01'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (837975.408842402,6334511.77994618) where ouv_sta_code=''MRM03'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (877398.475284672,6295769.16533951) where ouv_sta_code=''MRM04'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (841903.553816507,6271683.17641858) where ouv_sta_code=''MRM05'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (816914.061815432,6263016.66758402) where ouv_sta_code=''MRM06'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (173918.253114679,6812115.8892297) where ouv_sta_code=''M4290001'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (222197.20473166,6767853.93630765) where ouv_sta_code=''M4560001'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (385173.881789788,6844913.58105386) where ouv_sta_code=''M4500001'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (781493.671882723,6612717.75456281) where ouv_sta_code=''K1710001'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (512159.3586842,6637476.06409451) where ouv_sta_code=''L3860001'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (731741.17912543,6560330.8595322) where ouv_sta_code=''K3030001'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (734105.840170635,6637662.01183779) where ouv_sta_code=''K1580001'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (739698.970470657,6443979.56550937) where ouv_sta_code=''K2430001'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (753064.249531239,6427789.43994298) where ouv_sta_code=''K2430002'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (562919.84091293,6560253.97573747) where ouv_sta_code=''L5870001'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (383990.51495565,6276573.72575914) where ouv_sta_code=''5640012'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (524332.235458853,6655354.10022039) where ouv_sta_code=''L6370001'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (409267.963502858,6260689.83158306) where ouv_sta_code=''5640001'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (419353.461571104,6229413.63106304) where ouv_sta_code=''5640002'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (302062.327871579,6734366.22003421) where ouv_sta_code=''M4560003'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (337742.849305244,6755363.7973388) where ouv_sta_code=''M4350001'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (316741.147904605,6732081.48329355) where ouv_sta_code=''M4440001'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (316064.24517485,6744198.29539473) where ouv_sta_code=''M4560002'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (369794.258943853,6825914.52124732) where ouv_sta_code=''M4350001'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (323812.010218444,6842993.65203004) where ouv_sta_code=''M4220002'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (324401.814361328,6842401.2282521) where ouv_sta_code=''M4220003'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (285300.872960365,6838969.03974594) where ouv_sta_code=''M4220004'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (166262.737611886,6842480.28893417) where ouv_sta_code=''M4290002'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (175409.736715973,6819156.2300332) where ouv_sta_code=''M4290003'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (159868.149933445,6778726.35106166) where ouv_sta_code=''M4290004'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (372159.725647366,6278359.94072426) where ouv_sta_code=''5640003'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (389354.694770174,6275340.97017052) where ouv_sta_code=''5640013'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (385307.119233168,6245286.46438079) where ouv_sta_code=''5640006'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (339780.992954959,6264780.67859091) where ouv_sta_code=''5640007'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (341228.643676194,6262849.90484545) where ouv_sta_code=''5640008'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (401274.036682932,6246649.73108095) where ouv_sta_code=''5640004'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (395038.0384792,6253613.49692893) where ouv_sta_code=''5640011'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (331190.099650525,6260119.9394643) where ouv_sta_code=''5640010'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (328183.894368628,6262146.33415972) where ouv_sta_code=''5640009'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (407934.900667682,6235602.91480236) where ouv_sta_code=''5640005'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (505390,6224180) where ouv_sta_code=''5'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (573410,6279590) where ouv_sta_code=''6'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (526646,6419631) where ouv_sta_code=''7'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (528240,6336880) where ouv_sta_code=''1'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (512999,6419000) where ouv_sta_code=''2'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (555230,6244950) where ouv_sta_code=''3'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (509980,6224640) where ouv_sta_code=''4'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (438327.365989334,6514494.95730385) where ouv_sta_code=''M516001'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (289946.768385378,6253512.88749266) where ouv_sta_code=''oria'';
UPDATE t_ouvrage_ouv set (ouv_coordonnee_x,ouv_coordonnee_y) = (591628.780445827,6992728.82712897) where ouv_sta_code=''Bresle'';
comment on column t_ouvrage_ouv.ouv_coordonnee_x is ''Coordonnée x en Lambert 93'';
comment on column t_ouvrage_ouv.ouv_coordonnee_y is ''Coordonnée y en Lambert 93'';
 alter table t_ouvrage_ouv  ADD CONSTRAINT c_ck_ouv_coordonnee_x 
 CHECK (ouv_coordonnee_x >= -400000 AND ouv_coordonnee_x < 1300000); 
 alter table t_ouvrage_ouv  ADD CONSTRAINT c_ck_ouv_coordonnee_y 
 CHECK (ouv_coordonnee_y >= 6000000 AND ouv_coordonnee_y < 7500000);
');



-- problèmes de contraintes
select ref.updatesql('{"azti","charente","inra","logrami","migado","migradour","mrm","nat","pmp","saumonrhin"}',
'
alter table tj_stationmesure_stm drop constraint if exists c_uk_stm_libelle CASCADE;
alter table tj_stationmesure_stm add constraint c_uk_stm_libelle unique (stm_libelle);
alter table tj_stationmesure_stm drop constraint if exists c_uk_stm_identifiant CASCADE;
alter table tj_stationmesure_stm add constraint c_uk_stm_identifiant unique (stm_identifiant);

-- le fait de prolonger les périodes conduit à des résultats bizarres, 
-- en pratique on ne peut pas supprimer de périodes de fonctionnenemnt
-- je vire le trigger.
DROP TRIGGER IF EXISTS trg_per_suppression ON t_periodefonctdispositif_per;
');

select ref.updatesql('{"azti","charente","inra","logrami","migado","migradour","mrm","nat","pga","pmp","saumonrhin"}',
'insert into ts_maintenance_main
(
  main_ticket,
  main_description
  ) values
  (61,''Mise à jour vers la version 0.4 alpha, mise à jour ds constraintes stationmesure modification limites coordonnées géographiques'')'
);

-- AJOUT D'UNE CONTRAINTE UNIQUE POUR LA TABLE SYSTEME  !!!!!!!!
select ref.updatesql('{"azti","charente","inra","logrami","migado","migradour","mrm","nat","pga","pmp","saumonrhin"}',
'ALTER TABLE ts_maintenance_main DROP CONSTRAINT IF EXISTS c_uk_main_ticket;
ALTER TABLE ts_maintenance_main ADD CONSTRAINT c_uk_main_ticket unique (main_ticket);'
);


-- MISE A JOUR DE TOUTES LES CLES ETRANGERES  !!!!!!!!
--Testé dans bd_contmig_nat Marion. Ca marche pour certains schéma : logrami, iav, migado,nat, smatah
--et ça plante pour d'autre (sans indiquer d'erreur juste s'arrête sur "PL/pgSQL function ref.updatesql(character varying[],text) line 12 at instruction EXECUTE") : 
--bgm, charente, charente_bis, pmp,pmp

select ref.updatesql('{"azti","charente","inra","logrami","migado","migradour","mrm","nat","pga","pmp","saumonrhin"}',
'

ALTER TABLE ONLY tj_actionmarquage_act
    DROP CONSTRAINT IF EXISTS c_fk_act_lot_identifiant;

ALTER TABLE ONLY tj_actionmarquage_act
    ADD CONSTRAINT c_fk_act_lot_identifiant FOREIGN KEY (act_lot_identifiant, act_org_code) REFERENCES t_lot_lot(lot_identifiant, lot_org_code) ON UPDATE CASCADE;

ALTER TABLE ONLY tj_actionmarquage_act 
DROP CONSTRAINT IF EXISTS c_fk_act_mqe_reference ;

ALTER TABLE ONLY tj_actionmarquage_act
    ADD CONSTRAINT c_fk_act_mqe_reference FOREIGN KEY (act_mqe_reference, act_org_code) REFERENCES t_marque_mqe(mqe_reference, mqe_org_code) ON UPDATE CASCADE;

ALTER TABLE ONLY tj_actionmarquage_act
    DROP CONSTRAINT IF EXISTS c_fk_act_org_code ;

ALTER TABLE ONLY tj_actionmarquage_act
    ADD CONSTRAINT c_fk_act_org_code FOREIGN KEY (act_org_code) REFERENCES ref.ts_organisme_org(org_code)  MATCH FULL ON UPDATE CASCADE;

-------------------------------------------------------



ALTER TABLE ONLY t_bilanmigrationjournalier_bjo
    DROP CONSTRAINT IF EXISTS c_fk_bjo_std_code;

ALTER TABLE ONLY t_bilanmigrationjournalier_bjo
    ADD CONSTRAINT c_fk_bjo_std_code FOREIGN KEY (bjo_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_bilanmigrationjournalier_bjo
    DROP CONSTRAINT IF EXISTS c_fk_bjo_tax_code;

ALTER TABLE ONLY t_bilanmigrationjournalier_bjo
    ADD CONSTRAINT c_fk_bjo_tax_code FOREIGN KEY (bjo_tax_code) REFERENCES ref.tr_taxon_tax(tax_code) ON UPDATE CASCADE;

/* contrainte manquante v0.4*/


ALTER TABLE t_bilanmigrationjournalier_bjo
  DROP CONSTRAINT IF EXISTS c_fk_bjo_org_code;

ALTER TABLE t_bilanmigrationjournalier_bjo
  ADD CONSTRAINT c_fk_bjo_org_code FOREIGN KEY (bjo_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE CASCADE ON DELETE NO ACTION;

-------------------------------------------------------



ALTER TABLE ONLY t_bilanmigrationmensuel_bme
    DROP CONSTRAINT IF EXISTS c_fk_bme_std_code;

ALTER TABLE ONLY t_bilanmigrationmensuel_bme
    ADD CONSTRAINT c_fk_bme_std_code FOREIGN KEY (bme_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_bilanmigrationmensuel_bme
    DROP CONSTRAINT IF EXISTS c_fk_bme_tax_code;

ALTER TABLE ONLY t_bilanmigrationmensuel_bme
    ADD CONSTRAINT c_fk_bme_tax_code FOREIGN KEY (bme_tax_code) REFERENCES ref.tr_taxon_tax(tax_code) ON UPDATE CASCADE;


-------------------------------------------------------


ALTER TABLE ONLY tj_caracteristiquelot_car
    DROP CONSTRAINT IF EXISTS c_fk_car_lot_identifiant;

ALTER TABLE ONLY tj_caracteristiquelot_car
    ADD CONSTRAINT c_fk_car_lot_identifiant FOREIGN KEY (car_lot_identifiant, car_org_code) REFERENCES t_lot_lot(lot_identifiant, lot_org_code) ON UPDATE CASCADE;

ALTER TABLE ONLY tj_caracteristiquelot_car
    DROP CONSTRAINT IF EXISTS c_fk_car_org_code;
    
ALTER TABLE ONLY tj_caracteristiquelot_car
    ADD CONSTRAINT c_fk_car_org_code FOREIGN KEY (car_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;

ALTER TABLE ONLY tj_caracteristiquelot_car
    DROP CONSTRAINT IF EXISTS c_fk_car_par_code;
    
ALTER TABLE ONLY tj_caracteristiquelot_car
    ADD CONSTRAINT c_fk_car_par_code FOREIGN KEY (car_par_code) REFERENCES ref.tg_parametre_par(par_code) ON UPDATE CASCADE;

ALTER TABLE ONLY tj_caracteristiquelot_car
   DROP CONSTRAINT IF EXISTS c_fk_car_val_identifiant;

ALTER TABLE ONLY tj_caracteristiquelot_car
    ADD CONSTRAINT c_fk_car_val_identifiant FOREIGN KEY (car_val_identifiant) REFERENCES ref.tr_valeurparametrequalitatif_val(val_identifiant) ON UPDATE CASCADE;

-------------------------------------------------------


ALTER TABLE ONLY tj_coefficientconversion_coe
    DROP CONSTRAINT IF EXISTS c_fk_coe_org_code;
    
ALTER TABLE ONLY tj_coefficientconversion_coe
    ADD CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code) REFERENCES ref.ts_organisme_org(org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY tj_coefficientconversion_coe
    DROP CONSTRAINT IF EXISTS c_fk_coe_qte_code;

ALTER TABLE ONLY tj_coefficientconversion_coe
    ADD CONSTRAINT c_fk_coe_qte_code FOREIGN KEY (coe_qte_code) REFERENCES ref.tr_typequantitelot_qte(qte_code) ON UPDATE CASCADE;


ALTER TABLE ONLY tj_coefficientconversion_coe
    DROP CONSTRAINT IF EXISTS c_fk_coe_std_code;

ALTER TABLE ONLY tj_coefficientconversion_coe
    ADD CONSTRAINT c_fk_coe_std_code FOREIGN KEY (coe_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code) ON UPDATE CASCADE;

ALTER TABLE ONLY tj_coefficientconversion_coe
    DROP CONSTRAINT IF EXISTS c_fk_coe_tax_code;

ALTER TABLE ONLY tj_coefficientconversion_coe
    ADD CONSTRAINT c_fk_coe_tax_code FOREIGN KEY (coe_tax_code) REFERENCES ref.tr_taxon_tax(tax_code) ON UPDATE CASCADE;


-------------------------------------------------------


ALTER TABLE ONLY tj_dfesttype_dft
    DROP CONSTRAINT IF EXISTS c_fk_dft_df_identifiant;

ALTER TABLE ONLY tj_dfesttype_dft
    ADD CONSTRAINT c_fk_dft_df_identifiant FOREIGN KEY (dft_df_identifiant, dft_org_code) REFERENCES tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY tj_dfesttype_dft
    DROP CONSTRAINT IF EXISTS c_fk_dft_org_code;

ALTER TABLE ONLY tj_dfesttype_dft
    ADD CONSTRAINT c_fk_dft_org_code FOREIGN KEY (dft_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;

-------------------------------------------------------


-- ? contrainte unique manquante? 


ALTER TABLE ONLY t_dispositiffranchissement_dif
    DROP CONSTRAINT IF EXISTS c_uk_dif_dis_identifiant CASCADE;

ALTER TABLE ONLY t_dispositiffranchissement_dif
    ADD CONSTRAINT  c_uk_dif_dis_identifiant unique (dif_dis_identifiant,dif_org_code);

    --(manque une contrainte unique dans la table t_dispositifcomptage_dic
ALTER TABLE t_dispositifcomptage_dic DROP CONSTRAINT IF EXISTS c_uk_dic_dis_identifiant CASCADE;

ALTER TABLE t_dispositifcomptage_dic ADD CONSTRAINT c_uk_dic_dis_identifiant unique (dic_dis_identifiant,dic_org_code);

    
ALTER TABLE ONLY t_dispositifcomptage_dic
    DROP CONSTRAINT IF EXISTS c_fk_dic_dif_identifiant ;

ALTER TABLE ONLY t_dispositifcomptage_dic
    ADD CONSTRAINT c_fk_dic_dif_identifiant FOREIGN KEY (dic_dif_identifiant, dic_org_code) REFERENCES t_dispositiffranchissement_dif(dif_dis_identifiant, dif_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_dispositifcomptage_dic
    DROP CONSTRAINT IF EXISTS c_fk_dic_dis_identifiant;
    
ALTER TABLE ONLY t_dispositifcomptage_dic
    ADD CONSTRAINT c_fk_dic_dis_identifiant FOREIGN KEY (dic_dis_identifiant, dic_org_code) REFERENCES tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_dispositifcomptage_dic
    DROP CONSTRAINT IF EXISTS c_fk_dic_org_code;

ALTER TABLE ONLY t_dispositifcomptage_dic
    ADD CONSTRAINT c_fk_dic_org_code FOREIGN KEY (dic_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


ALTER TABLE ONLY t_dispositifcomptage_dic
    DROP CONSTRAINT IF EXISTS c_fk_dic_tdc_code;

ALTER TABLE ONLY t_dispositifcomptage_dic
    ADD CONSTRAINT c_fk_dic_tdc_code FOREIGN KEY (dic_tdc_code) REFERENCES ref.tr_typedc_tdc(tdc_code) ON UPDATE CASCADE;



-------------------------------------------------------




ALTER TABLE ONLY t_dispositiffranchissement_dif
    DROP CONSTRAINT IF EXISTS c_fk_dif_dis_identifiant;

ALTER TABLE ONLY t_dispositiffranchissement_dif
    ADD CONSTRAINT c_fk_dif_dis_identifiant FOREIGN KEY (dif_dis_identifiant, dif_org_code) REFERENCES tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_dispositiffranchissement_dif
    DROP CONSTRAINT IF EXISTS c_fk_dif_org_code;

ALTER TABLE ONLY t_dispositiffranchissement_dif
    ADD CONSTRAINT c_fk_dif_org_code FOREIGN KEY (dif_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


ALTER TABLE ONLY t_dispositiffranchissement_dif
    DROP CONSTRAINT IF EXISTS c_fk_dif_ouv_identifiant;

ALTER TABLE ONLY t_dispositiffranchissement_dif
    ADD CONSTRAINT c_fk_dif_ouv_identifiant FOREIGN KEY (dif_ouv_identifiant, dif_org_code) REFERENCES t_ouvrage_ouv(ouv_identifiant, ouv_org_code) ON UPDATE CASCADE;


-------------------------------------------------------




ALTER TABLE ONLY tj_dfestdestinea_dtx
    DROP CONSTRAINT IF EXISTS c_fk_dtx_dif_identifiant;

ALTER TABLE ONLY tj_dfestdestinea_dtx
    ADD CONSTRAINT c_fk_dtx_dif_identifiant FOREIGN KEY (dtx_dif_identifiant, dtx_org_code) REFERENCES tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY tj_dfestdestinea_dtx
    DROP CONSTRAINT IF EXISTS c_fk_dtx_org_code;

ALTER TABLE ONLY tj_dfestdestinea_dtx
    ADD CONSTRAINT c_fk_dtx_org_code FOREIGN KEY (dtx_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


ALTER TABLE ONLY tj_dfestdestinea_dtx
    DROP CONSTRAINT IF EXISTS c_fk_dtx_tax_code;

ALTER TABLE ONLY tj_dfestdestinea_dtx
    ADD CONSTRAINT c_fk_dtx_tax_code FOREIGN KEY (dtx_tax_code) REFERENCES ref.tr_taxon_tax(tax_code) ON UPDATE CASCADE;



-------------------------------------------------------
-- valeur manquante (iav)

ALTER TABLE tj_dfesttype_dft
  DROP CONSTRAINT IF EXISTS c_fk_dft_tdf_code;


ALTER TABLE tj_dfesttype_dft
  ADD CONSTRAINT c_fk_dft_tdf_code FOREIGN KEY (dft_tdf_code)
      REFERENCES ref.tr_typedf_tdf (tdf_code) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;

-------------------------------------------------------



ALTER TABLE ONLY tj_conditionenvironnementale_env
    DROP CONSTRAINT IF EXISTS c_fk_env_org_code;

ALTER TABLE ONLY tj_conditionenvironnementale_env
    ADD CONSTRAINT c_fk_env_org_code FOREIGN KEY (env_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


ALTER TABLE ONLY tj_conditionenvironnementale_env
    DROP CONSTRAINT IF EXISTS c_fk_env_stm_identifiant;

ALTER TABLE ONLY tj_conditionenvironnementale_env
    ADD CONSTRAINT c_fk_env_stm_identifiant FOREIGN KEY (env_stm_identifiant, env_org_code) REFERENCES tj_stationmesure_stm(stm_identifiant, stm_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY tj_conditionenvironnementale_env
    DROP CONSTRAINT IF EXISTS c_fk_env_val_identifiant;

ALTER TABLE ONLY tj_conditionenvironnementale_env
    ADD CONSTRAINT c_fk_env_val_identifiant FOREIGN KEY (env_val_identifiant) REFERENCES ref.tr_valeurparametrequalitatif_val(val_identifiant) ON UPDATE CASCADE;



-------------------------------------------------------

ALTER TABLE ONLY t_lot_lot
    DROP CONSTRAINT IF EXISTS c_fk_lot_ope_identifiant;

ALTER TABLE ONLY t_lot_lot
    ADD CONSTRAINT c_fk_lot_ope_identifiant FOREIGN KEY (lot_ope_identifiant,lot_org_code) REFERENCES t_operation_ope(ope_identifiant,ope_org_code) ON UPDATE CASCADE;




ALTER TABLE ONLY t_lot_lot
    DROP CONSTRAINT IF EXISTS c_fk_lot_dev_code;

ALTER TABLE ONLY t_lot_lot
    ADD CONSTRAINT c_fk_lot_dev_code FOREIGN KEY (lot_dev_code) REFERENCES ref.tr_devenirlot_dev(dev_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_lot_lot
    DROP CONSTRAINT IF EXISTS c_fk_lot_lot_identifiant;

ALTER TABLE ONLY t_lot_lot
    ADD CONSTRAINT c_fk_lot_lot_identifiant FOREIGN KEY (lot_lot_identifiant, lot_org_code) REFERENCES t_lot_lot(lot_identifiant, lot_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_lot_lot
    DROP CONSTRAINT IF EXISTS c_fk_lot_org_code;

ALTER TABLE ONLY t_lot_lot
    ADD CONSTRAINT c_fk_lot_org_code FOREIGN KEY (lot_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


ALTER TABLE ONLY t_lot_lot
    DROP CONSTRAINT IF EXISTS c_fk_lot_qte_code;

ALTER TABLE ONLY t_lot_lot
    ADD CONSTRAINT c_fk_lot_qte_code FOREIGN KEY (lot_qte_code) REFERENCES ref.tr_typequantitelot_qte(qte_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_lot_lot
    DROP CONSTRAINT IF EXISTS c_fk_lot_std_code;

ALTER TABLE ONLY t_lot_lot
    ADD CONSTRAINT c_fk_lot_std_code FOREIGN KEY (lot_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_lot_lot
    DROP CONSTRAINT IF EXISTS c_fk_lot_tax_code;

ALTER TABLE ONLY t_lot_lot
    ADD CONSTRAINT c_fk_lot_tax_code FOREIGN KEY (lot_tax_code) REFERENCES ref.tr_taxon_tax(tax_code) ON UPDATE CASCADE;


-------------------------------------------------------




ALTER TABLE ONLY t_marque_mqe
    DROP CONSTRAINT IF EXISTS c_fk_mqe_loc_code;

ALTER TABLE ONLY t_marque_mqe
    ADD CONSTRAINT c_fk_mqe_loc_code FOREIGN KEY (mqe_loc_code) REFERENCES ref.tr_localisationanatomique_loc(loc_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_marque_mqe
    DROP CONSTRAINT IF EXISTS c_fk_mqe_nmq_code;

ALTER TABLE ONLY t_marque_mqe
    ADD CONSTRAINT c_fk_mqe_nmq_code FOREIGN KEY (mqe_nmq_code) REFERENCES ref.tr_naturemarque_nmq(nmq_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_marque_mqe
    DROP CONSTRAINT IF EXISTS c_fk_mqe_omq_reference;

ALTER TABLE ONLY t_marque_mqe
    ADD CONSTRAINT c_fk_mqe_omq_reference FOREIGN KEY (mqe_omq_reference, mqe_org_code) REFERENCES t_operationmarquage_omq(omq_reference, omq_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_marque_mqe
    DROP CONSTRAINT IF EXISTS c_fk_mqe_org_code;

ALTER TABLE ONLY t_marque_mqe
    ADD CONSTRAINT c_fk_mqe_org_code FOREIGN KEY (mqe_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;



-------------------------------------------------------



ALTER TABLE ONLY t_operationmarquage_omq
    DROP CONSTRAINT IF EXISTS c_fk_omq_org_code;

ALTER TABLE ONLY t_operationmarquage_omq
    ADD CONSTRAINT c_fk_omq_org_code FOREIGN KEY (omq_org_code) REFERENCES ref.ts_organisme_org(org_code) ON UPDATE CASCADE;

-------------------------------------------------------



ALTER TABLE ONLY t_operation_ope
    DROP CONSTRAINT IF EXISTS c_fk_ope_dic_identifiant;

ALTER TABLE ONLY t_operation_ope
    ADD CONSTRAINT c_fk_ope_dic_identifiant FOREIGN KEY (ope_dic_identifiant, ope_org_code) REFERENCES tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_operation_ope
    DROP CONSTRAINT IF EXISTS c_fk_ope_org_code;

ALTER TABLE ONLY t_operation_ope
    ADD CONSTRAINT c_fk_ope_org_code FOREIGN KEY (ope_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


-------------------------------------------------------


ALTER TABLE ONLY t_ouvrage_ouv
    DROP CONSTRAINT IF EXISTS c_fk_ouv_nov_code;

ALTER TABLE ONLY t_ouvrage_ouv
    ADD CONSTRAINT c_fk_ouv_nov_code FOREIGN KEY (ouv_nov_code) REFERENCES ref.tr_natureouvrage_nov(nov_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_ouvrage_ouv
    DROP CONSTRAINT IF EXISTS c_fk_ouv_org_code;

ALTER TABLE ONLY t_ouvrage_ouv
    ADD CONSTRAINT c_fk_ouv_org_code FOREIGN KEY (ouv_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;

-- contrainte manquante au schéma IAV

ALTER TABLE t_ouvrage_ouv
  DROP CONSTRAINT IF EXISTS c_fk_ouv_sta_code;

ALTER TABLE t_ouvrage_ouv
  ADD CONSTRAINT c_fk_ouv_sta_code FOREIGN KEY (ouv_sta_code, ouv_org_code)
      REFERENCES t_station_sta (sta_code, sta_org_code) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;

-------------------------------------------------------




ALTER TABLE ONLY tj_pathologieconstatee_pco
    DROP CONSTRAINT IF EXISTS c_fk_pco_loc_code;

ALTER TABLE ONLY tj_pathologieconstatee_pco
    ADD CONSTRAINT c_fk_pco_loc_code FOREIGN KEY (pco_loc_code) REFERENCES ref.tr_localisationanatomique_loc(loc_code) ON UPDATE CASCADE;


ALTER TABLE ONLY tj_pathologieconstatee_pco
    DROP CONSTRAINT IF EXISTS c_fk_pco_lot_identifiant;

ALTER TABLE ONLY tj_pathologieconstatee_pco
    ADD CONSTRAINT c_fk_pco_lot_identifiant FOREIGN KEY (pco_lot_identifiant, pco_org_code) REFERENCES t_lot_lot(lot_identifiant, lot_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY tj_pathologieconstatee_pco
    DROP CONSTRAINT IF EXISTS c_fk_pco_org_code ;

ALTER TABLE ONLY tj_pathologieconstatee_pco
    ADD CONSTRAINT c_fk_pco_org_code FOREIGN KEY (pco_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


ALTER TABLE ONLY tj_pathologieconstatee_pco
    DROP CONSTRAINT IF EXISTS c_fk_pco_pat_code ;

ALTER TABLE ONLY tj_pathologieconstatee_pco
    ADD CONSTRAINT c_fk_pco_pat_code FOREIGN KEY (pco_pat_code) REFERENCES ref.tr_pathologie_pat(pat_code) ON UPDATE CASCADE;

/*
--v0.4 insertion d''une contrainte vers une nouvelle table.

 alter table tj_pathologieconstatee_pco DROP CONSTRAINT IF EXISTS c_fk_pco_imp_code ;

 alter table tj_pathologieconstatee_pco ADD CONSTRAINT c_fk_pco_imp_code FOREIGN KEY (pco_imp_code)
      REFERENCES ref.tr_importancepatho_imp (imp_code) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION; 
*/

-------------------------------------------------------



ALTER TABLE ONLY t_periodefonctdispositif_per
    DROP CONSTRAINT IF EXISTS c_fk_per_dis_identifiant;

ALTER TABLE ONLY t_periodefonctdispositif_per
    ADD CONSTRAINT c_fk_per_dis_identifiant FOREIGN KEY (per_dis_identifiant, per_org_code) REFERENCES tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_periodefonctdispositif_per
    DROP CONSTRAINT IF EXISTS c_fk_per_org_code ;

ALTER TABLE ONLY t_periodefonctdispositif_per
    ADD CONSTRAINT c_fk_per_org_code FOREIGN KEY (per_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


ALTER TABLE ONLY t_periodefonctdispositif_per
    DROP CONSTRAINT IF EXISTS c_fk_per_tar_code;

ALTER TABLE ONLY t_periodefonctdispositif_per
    ADD CONSTRAINT c_fk_per_tar_code FOREIGN KEY (per_tar_code) REFERENCES ref.tr_typearretdisp_tar(tar_code) ON UPDATE CASCADE;



-------------------------------------------------------



ALTER TABLE ONLY tj_prelevementlot_prl
    DROP CONSTRAINT IF EXISTS c_fk_prl_loc_code;

ALTER TABLE ONLY tj_prelevementlot_prl
    ADD CONSTRAINT c_fk_prl_loc_code FOREIGN KEY (prl_loc_code) REFERENCES ref.tr_localisationanatomique_loc(loc_code) ON UPDATE CASCADE;



ALTER TABLE ONLY tj_prelevementlot_prl
    DROP CONSTRAINT IF EXISTS c_fk_prl_lot_identifiant;

ALTER TABLE ONLY tj_prelevementlot_prl
    ADD CONSTRAINT c_fk_prl_lot_identifiant FOREIGN KEY (prl_lot_identifiant, prl_org_code) REFERENCES t_lot_lot(lot_identifiant, lot_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY tj_prelevementlot_prl
    DROP CONSTRAINT IF EXISTS c_fk_prl_typeprelevement;

ALTER TABLE ONLY tj_prelevementlot_prl
    ADD CONSTRAINT c_fk_prl_typeprelevement FOREIGN KEY (prl_pre_typeprelevement) REFERENCES ref.tr_prelevement_pre(pre_typeprelevement) ON UPDATE CASCADE;

ALTER TABLE ONLY tj_prelevementlot_prl
    DROP CONSTRAINT IF EXISTS c_fk_prl_org_code;

ALTER TABLE ONLY tj_prelevementlot_prl
    ADD CONSTRAINT c_fk_prl_org_code FOREIGN KEY (prl_org_code) REFERENCES ref.ts_organisme_org (org_code) ON UPDATE CASCADE;

ALTER TABLE ONLY tj_prelevementlot_prl
    DROP CONSTRAINT IF EXISTS c_fk_prl_pre_nom;

ALTER TABLE ONLY tj_prelevementlot_prl 
    ADD CONSTRAINT c_fk_prl_pre_nom FOREIGN KEY (prl_pre_typeprelevement)
      REFERENCES ref.tr_prelevement_pre (pre_typeprelevement) MATCH SIMPLE
      ON UPDATE CASCADE;

-------------------------------------------------------


ALTER TABLE ONLY t_station_sta
    DROP CONSTRAINT IF EXISTS c_fk_sta_org_code;

ALTER TABLE ONLY t_station_sta
    ADD CONSTRAINT c_fk_sta_org_code FOREIGN KEY (sta_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


-------------------------------------------------------



ALTER TABLE ONLY tg_dispositif_dis
    DROP CONSTRAINT IF EXISTS c_fk_sta_org_code;

ALTER TABLE ONLY tg_dispositif_dis
    ADD CONSTRAINT c_fk_sta_org_code FOREIGN KEY (dis_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;




-------------------------------------------------------






ALTER TABLE ONLY tj_stationmesure_stm
    DROP CONSTRAINT IF EXISTS c_fk_stm_org_code;

ALTER TABLE ONLY tj_stationmesure_stm
    ADD CONSTRAINT c_fk_stm_org_code FOREIGN KEY (stm_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;



ALTER TABLE ONLY tj_stationmesure_stm
    DROP CONSTRAINT IF EXISTS c_fk_stm_par_code;

ALTER TABLE ONLY tj_stationmesure_stm
    ADD CONSTRAINT c_fk_stm_par_code FOREIGN KEY (stm_par_code) REFERENCES ref.tg_parametre_par(par_code) ON UPDATE CASCADE;



ALTER TABLE ONLY tj_stationmesure_stm
    DROP CONSTRAINT IF EXISTS c_fk_stm_sta_code;

ALTER TABLE ONLY tj_stationmesure_stm
    ADD CONSTRAINT c_fk_stm_sta_code FOREIGN KEY (stm_sta_code, stm_org_code) REFERENCES t_station_sta(sta_code, sta_org_code) ON UPDATE CASCADE;





-------------------------------------------------------



ALTER TABLE ONLY tj_tauxechappement_txe
    DROP CONSTRAINT IF EXISTS c_fk_txe_ech_code;

ALTER TABLE ONLY tj_tauxechappement_txe
    ADD CONSTRAINT c_fk_txe_ech_code FOREIGN KEY (txe_ech_code) REFERENCES ref.tr_niveauechappement_ech(ech_code) ON UPDATE CASCADE;



ALTER TABLE ONLY tj_tauxechappement_txe
    DROP CONSTRAINT IF EXISTS c_fk_txe_org_code;

ALTER TABLE ONLY tj_tauxechappement_txe
    ADD CONSTRAINT c_fk_txe_org_code FOREIGN KEY (txe_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;

/*
--changement v0.4 ci dessous la clé étrangère se rattache à une station 
-- ATTENTION NECESSSITE V0.4

ALTER TABLE ONLY tj_tauxechappement_txe
    DROP CONSTRAINT IF EXISTS c_fk_txe_sta_identifiant;

ALTER TABLE ONLY tj_tauxechappement_txe
    ADD CONSTRAINT c_fk_txe_sta_identifiant FOREIGN KEY (txe_sta_identifiant, txe_org_code) REFERENCES t_station_sta(sta_identifiant, ouv_org_code) ON UPDATE CASCADE;
*/

ALTER TABLE ONLY tj_tauxechappement_txe
    DROP CONSTRAINT IF EXISTS c_fk_txe_std_code;

ALTER TABLE ONLY tj_tauxechappement_txe
    ADD CONSTRAINT c_fk_txe_std_code FOREIGN KEY (txe_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code) ON UPDATE CASCADE;



ALTER TABLE ONLY tj_tauxechappement_txe
    DROP CONSTRAINT IF EXISTS c_fk_txe_tax_code;

ALTER TABLE ONLY tj_tauxechappement_txe
    ADD CONSTRAINT c_fk_txe_tax_code FOREIGN KEY (txe_tax_code) REFERENCES ref.tr_taxon_tax(tax_code) ON UPDATE CASCADE;


-------------------------------------------------------


-- clé manquante org

ALTER TABLE ONLY ts_taxonvideo_txv
    DROP CONSTRAINT IF EXISTS c_fk_txv_org_code;

ALTER TABLE ONLY ts_taxonvideo_txv
    ADD CONSTRAINT c_fk_txv_org_code FOREIGN KEY (txv_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;

ALTER TABLE ONLY ts_taxonvideo_txv
    DROP CONSTRAINT IF EXISTS c_fk_txv_tax_code ;

ALTER TABLE ONLY ts_taxonvideo_txv
    ADD CONSTRAINT c_fk_txv_tax_code FOREIGN KEY (txv_tax_code) REFERENCES ref.tr_taxon_tax(tax_code) ON UPDATE CASCADE;

ALTER TABLE ONLY ts_taxonvideo_txv
    DROP CONSTRAINT IF EXISTS c_fk_std_code;

ALTER TABLE ONLY ts_taxonvideo_txv
    ADD CONSTRAINT c_fk_std_code FOREIGN KEY (txv_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code) ON UPDATE CASCADE;


-------------------------------------------------------




ALTER TABLE ONLY ts_taillevideo_tav
    DROP CONSTRAINT IF EXISTS c_fk_tav_dic_identifiant;

ALTER TABLE ONLY ts_taillevideo_tav
    ADD CONSTRAINT c_fk_tav_dic_identifiant FOREIGN KEY (tav_dic_identifiant, tav_org_code) REFERENCES t_dispositifcomptage_dic(dic_dis_identifiant, dic_org_code) ON UPDATE CASCADE;

-- clé manquante org

ALTER TABLE ONLY ts_taillevideo_tav
    DROP CONSTRAINT IF EXISTS c_fk_tav_org_code;

ALTER TABLE ONLY ts_taillevideo_tav
    ADD CONSTRAINT c_fk_tav_org_code FOREIGN KEY (tav_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;




');



select ref.updatesql('{"bgm"}',
'insert into ts_maintenance_main
(
  main_ticket,
  main_description
  ) values
  (152,''Mise à jour vers la version 0.4 alpha, problèmes de clé étrangères, script total'')'
);

     