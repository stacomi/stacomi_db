﻿/*
Corrections d'erreurs dans les schémas
*/
DROP TABLE charente.tj_coefficientconversion_coe;
CREATE TABLE charente.tj_coefficientconversion_coe
(
  coe_tax_code character varying(6) NOT NULL,
  coe_std_code character varying(4) NOT NULL,
  coe_qte_code character varying(4) NOT NULL,
  coe_date_debut date NOT NULL,
  coe_date_fin date NOT NULL,
  coe_valeur_coefficient real,
  coe_commentaires text,
  coe_org_code character varying(30) NOT NULL,
  CONSTRAINT c_pk_coe PRIMARY KEY (coe_tax_code, coe_std_code, coe_qte_code, coe_date_debut, coe_date_fin, coe_org_code),
  CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT c_fk_coe_qte_code FOREIGN KEY (coe_qte_code)
      REFERENCES ref.tr_typequantitelot_qte (qte_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT c_fk_coe_std_code FOREIGN KEY (coe_std_code)
      REFERENCES ref.tr_stadedeveloppement_std (std_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT c_fk_coe_tax_code FOREIGN KEY (coe_tax_code)
      REFERENCES ref.tr_taxon_tax (tax_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT c_ck_coe_date_fin CHECK (coe_date_fin >= coe_date_debut)
)
WITH (
  OIDS=TRUE
);
ALTER TABLE charente.tj_coefficientconversion_coe
  OWNER TO postgres;
GRANT ALL ON TABLE charente.tj_coefficientconversion_coe TO postgres;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE charente.tj_coefficientconversion_coe TO charente;
GRANT SELECT ON TABLE charente.tj_coefficientconversion_coe TO invite;

 DROP TABLE charente.t_operationmarquage_omq CASCADE;

CREATE TABLE charente.t_operationmarquage_omq
(
  omq_reference character varying(30) NOT NULL,
  omq_commentaires text,
  omq_org_code character varying(30) NOT NULL,
  CONSTRAINT c_pk_omq PRIMARY KEY (omq_reference, omq_org_code),
  CONSTRAINT c_fk_omq_org_code FOREIGN KEY (omq_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=TRUE
);
ALTER TABLE charente.t_operationmarquage_omq
  OWNER TO postgres;
GRANT ALL ON TABLE charente.t_operationmarquage_omq TO postgres;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE charente.t_operationmarquage_omq TO charente;
GRANT SELECT ON TABLE charente.t_operationmarquage_omq TO invite;


DROP TABLE charente.tj_actionmarquage_act;

CREATE TABLE charente.tj_actionmarquage_act
(
  act_lot_identifiant integer NOT NULL,
  act_mqe_reference character varying(30) NOT NULL,
  act_action character varying(20) NOT NULL,
  act_commentaires text,
  act_org_code character varying(30) NOT NULL,
  CONSTRAINT c_pk_act PRIMARY KEY (act_lot_identifiant, act_mqe_reference, act_org_code),
  CONSTRAINT c_fk_act_lot_identifiant FOREIGN KEY (act_lot_identifiant, act_org_code)
      REFERENCES charente.t_lot_lot (lot_identifiant, lot_org_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT c_fk_act_mqe_reference FOREIGN KEY (act_mqe_reference, act_org_code)
      REFERENCES charente.t_marque_mqe (mqe_reference, mqe_org_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT c_fk_act_org_code FOREIGN KEY (act_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT c_ck_act_action CHECK (upper(act_action::text) = 'POSE'::text OR upper(act_action::text) = 'LECTURE'::text OR upper(act_action::text) = 'RETRAIT'::text)
)
WITH (
  OIDS=TRUE
);
ALTER TABLE charente.tj_actionmarquage_act
  OWNER TO postgres;
GRANT ALL ON TABLE charente.tj_actionmarquage_act TO postgres;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE charente.tj_actionmarquage_act TO charente;
GRANT SELECT ON TABLE charente.tj_actionmarquage_act TO invite;


DROP TABLE migradour.tj_coefficientconversion_coe;
CREATE TABLE migradour.tj_coefficientconversion_coe
(
  coe_tax_code character varying(6) NOT NULL,
  coe_std_code character varying(4) NOT NULL,
  coe_qte_code character varying(4) NOT NULL,
  coe_date_debut date NOT NULL,
  coe_date_fin date NOT NULL,
  coe_valeur_coefficient real,
  coe_commentaires text,
  coe_org_code character varying(30) NOT NULL,
  CONSTRAINT c_pk_coe PRIMARY KEY (coe_tax_code, coe_std_code, coe_qte_code, coe_date_debut, coe_date_fin, coe_org_code),
  CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT c_fk_coe_qte_code FOREIGN KEY (coe_qte_code)
      REFERENCES ref.tr_typequantitelot_qte (qte_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT c_fk_coe_std_code FOREIGN KEY (coe_std_code)
      REFERENCES ref.tr_stadedeveloppement_std (std_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT c_fk_coe_tax_code FOREIGN KEY (coe_tax_code)
      REFERENCES ref.tr_taxon_tax (tax_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT c_ck_coe_date_fin CHECK (coe_date_fin >= coe_date_debut)
)
WITH (
  OIDS=TRUE
);
ALTER TABLE migradour.tj_coefficientconversion_coe
  OWNER TO postgres;
GRANT ALL ON TABLE migradour.tj_coefficientconversion_coe TO postgres;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE migradour.tj_coefficientconversion_coe TO migradour;
GRANT SELECT ON TABLE migradour.tj_coefficientconversion_coe TO invite;

 DROP TABLE migradour.t_operationmarquage_omq CASCADE;

CREATE TABLE migradour.t_operationmarquage_omq
(
  omq_reference character varying(30) NOT NULL,
  omq_commentaires text,
  omq_org_code character varying(30) NOT NULL,
  CONSTRAINT c_pk_omq PRIMARY KEY (omq_reference, omq_org_code),
  CONSTRAINT c_fk_omq_org_code FOREIGN KEY (omq_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=TRUE
);
ALTER TABLE migradour.t_operationmarquage_omq
  OWNER TO postgres;
GRANT ALL ON TABLE migradour.t_operationmarquage_omq TO postgres;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE migradour.t_operationmarquage_omq TO migradour;
GRANT SELECT ON TABLE migradour.t_operationmarquage_omq TO invite;


DROP TABLE migradour.tj_actionmarquage_act;

CREATE TABLE migradour.tj_actionmarquage_act
(
  act_lot_identifiant integer NOT NULL,
  act_mqe_reference character varying(30) NOT NULL,
  act_action character varying(20) NOT NULL,
  act_commentaires text,
  act_org_code character varying(30) NOT NULL,
  CONSTRAINT c_pk_act PRIMARY KEY (act_lot_identifiant, act_mqe_reference, act_org_code),
  CONSTRAINT c_fk_act_lot_identifiant FOREIGN KEY (act_lot_identifiant, act_org_code)
      REFERENCES migradour.t_lot_lot (lot_identifiant, lot_org_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT c_fk_act_mqe_reference FOREIGN KEY (act_mqe_reference, act_org_code)
      REFERENCES migradour.t_marque_mqe (mqe_reference, mqe_org_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT c_fk_act_org_code FOREIGN KEY (act_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT c_ck_act_action CHECK (upper(act_action::text) = 'POSE'::text OR upper(act_action::text) = 'LECTURE'::text OR upper(act_action::text) = 'RETRAIT'::text)
)
WITH (
  OIDS=TRUE
);
ALTER TABLE migradour.tj_actionmarquage_act
  OWNER TO postgres;
GRANT ALL ON TABLE migradour.tj_actionmarquage_act TO postgres;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE migradour.tj_actionmarquage_act TO migradour;
GRANT SELECT ON TABLE migradour.tj_actionmarquage_act TO invite;


