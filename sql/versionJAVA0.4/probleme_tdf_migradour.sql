﻿ alter table migradour.tj_dfesttype_dft add CONSTRAINT c_fk_dft_tdf_code FOREIGN KEY (dft_tdf_code)
      REFERENCES ref.tr_typedf_tdf (tdf_code) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
  update migradour.tj_dfesttype_dft set dft_tdf_code=dft_tdf_codetemp;
  alter table migradour.tj_dfesttype_dft drop column dft_tdf_codetemp;