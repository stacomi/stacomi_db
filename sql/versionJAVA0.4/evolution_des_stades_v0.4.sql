﻿/*
Stade de développment. Script d'évolution des stades
note fait appel à la fonction update sql pour lancer les modifications en séquence sur plusieurs schémas.
13/06/2016
Cédric Briand
*/
	
-- Function: ref.updatesql(character varying[], text)
-- DROP FUNCTION ref.updatesql(character varying[], text);

CREATE OR REPLACE FUNCTION ref.updatesql(myschemas character varying[], scriptsql text)
  RETURNS integer AS
$BODY$
	DECLARE
	totalcount int;	
	nbschema int;
	i integer;
	BEGIN	  
		select INTO nbschema array_length(myschemas,1);
		i=1;
		While (i <=nbschema) LOOP
		EXECUTE 'SET search_path TO '||myschemas[i]||', public';
		RAISE INFO 'execute sql pour schema %',myschemas[i] ;
		EXECUTE scriptsql;		
		i=i+1;
		END LOOP;	
	RETURN nbschema;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION ref.updatesql(character varying[], text)
  OWNER TO postgres;
COMMENT ON FUNCTION ref.updatesql(character varying[], text) IS 'fonction pour lancer un script de mise à jour sur chaque schema';

-- Suppression du rang (remplacé par les masques, correction en JAVA effectuée)
-- Je corrige d'abord la vue 'v_taxon_tax'


select ref.updatesql('{"azti","bgm","charente","fd80","inra","logrami","migado","saumonrhin","iav","migradour","smatah","nat"}',
'DROP VIEW v_taxon_tax');
select ref.updatesql('{"azti","bgm","charente","fd80","inra","logrami","migado","saumonrhin","iav","migradour","smatah","nat"}',
'CREATE OR REPLACE VIEW v_taxon_tax AS 
 SELECT tax.tax_code, tax.tax_nom_latin, tax.tax_nom_commun, tax.tax_ntx_code, tax.tax_tax_code, tax.tax_rang, txv.txv_code, txv.txv_tax_code, txv.txv_std_code, std.std_code, std.std_libelle
   FROM ref.tr_taxon_tax tax
   RIGHT JOIN migradour.ts_taxonvideo_txv txv ON tax.tax_code::text = txv.txv_tax_code::text
   LEFT JOIN ref.tr_stadedeveloppement_std std ON txv.txv_std_code::text = std.std_code::text');
ALTER TABLE ref.tr_stadedeveloppement_std drop column std_rang;

-- correction d'une erreur sur le stade MAD (merci Marion)
--suppression des contraintes de clé

--t_bilanmigrationjournalier_bjo
select ref.updatesql('{"azti","bgm","charente","fd80","inra","logrami","migado","saumonrhin","iav","migradour","smatah","nat"}',
'ALTER TABLE t_bilanmigrationjournalier_bjo DROP CONSTRAINT IF EXISTS c_fk_bjo_std_code ');
--t_bilanmigrationmensuel_bme
select ref.updatesql('{"azti","bgm","charente","fd80","inra","logrami","migado","saumonrhin","iav","migradour","smatah","nat"}',
'ALTER TABLE t_bilanmigrationmensuel_bme DROP CONSTRAINT IF EXISTS c_fk_bme_std_code ');
--t_lot_lot
select ref.updatesql('{"azti","bgm","charente","fd80","inra","logrami","migado","saumonrhin","iav","migradour","smatah","nat"}',
'ALTER TABLE t_lot_lot DROP CONSTRAINT IF EXISTS c_fk_lot_std_code ');
--ts_taxonvideo_txv
select ref.updatesql('{"azti","bgm","charente","fd80","inra","logrami","migado","saumonrhin","iav","migradour","smatah","nat"}',
'ALTER TABLE ts_taxonvideo_txv DROP CONSTRAINT IF EXISTS c_fk_std_code ');

-- correction du stade 'MAD(espace)' dans toutes les tables
select ref.updatesql('{"azti","bgm","charente","fd80","inra","logrami","migado","saumonrhin","iav","migradour","smatah","nat"}',
'UPDATE t_lot_lot set lot_std_code=''MAD'' where lot_std_code like ''MAD ''');
select ref.updatesql('{"azti","bgm","charente","fd80","inra","logrami","migado","saumonrhin","iav","migradour","smatah","nat"}',
'UPDATE t_bilanmigrationmensuel_bme SET bme_std_code=''MAD'' where bme_std_code like ''MAD ''');
select ref.updatesql('{"azti","bgm","charente","fd80","inra","logrami","migado","saumonrhin","iav","migradour","smatah","nat"}',
'UPDATE t_bilanmigrationjournalier_bjo SET bjo_std_code=''MAD'' where bjo_std_code like ''MAD ''');
select ref.updatesql('{"azti","bgm","charente","fd80","inra","logrami","migado","saumonrhin","iav","migradour","smatah","nat"}',
'UPDATE ts_taxonvideo_txv SET txv_std_code=''MAD'' where txv_std_code like ''MAD ''');
UPDATE ref.tr_stadedeveloppement_std set std_code='MAD' where std_code like 'MAD ';

-- réinsertion des contraintes de clé étrangère avec un UPDATE CASCADE mais je garde un delete no action pour la sécurité
 --  t_bilanmigrationjournalier_bjo 
select ref.updatesql('{"azti","bgm","charente","fd80","inra","logrami","migado","saumonrhin","iav","migradour","smatah","nat"}',
'ALTER TABLE t_bilanmigrationjournalier_bjo
  ADD CONSTRAINT c_fk_bjo_std_code FOREIGN KEY (bjo_std_code)
      REFERENCES ref.tr_stadedeveloppement_std (std_code) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION');
 --  t_bilanmigrationmensuel_bme   
select ref.updatesql('{"azti","bgm","charente","fd80","inra","logrami","migado","saumonrhin","iav","migradour","smatah","nat"}',      
'ALTER TABLE t_bilanmigrationmensuel_bme
  ADD CONSTRAINT c_fk_bme_std_code FOREIGN KEY (bme_std_code)
      REFERENCES ref.tr_stadedeveloppement_std (std_code) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION');
 -- t_lot_lot     
 select ref.updatesql('{"azti","bgm","charente","fd80","inra","logrami","migado","saumonrhin","iav","migradour","smatah","nat"}',
'ALTER TABLE t_lot_lot
  ADD CONSTRAINT c_fk_lot_std_code FOREIGN KEY (lot_std_code)
      REFERENCES ref.tr_stadedeveloppement_std (std_code) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION');     
-- ts_taxonvideo_txv      
select ref.updatesql('{"azti","bgm","charente","fd80","inra","logrami","migado","saumonrhin","iav","migradour","smatah","nat"}',
'ALTER TABLE ts_taxonvideo_txv
  ADD CONSTRAINT c_fk_txv_std_code FOREIGN KEY (txv_std_code)
      REFERENCES ref.tr_stadedeveloppement_std (std_code) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION');



-- ajout d'une colonne définition dans les stades

ALTER TABLE ref.tr_stadedeveloppement_std ADD COLUMN std_definition text;

-- modification de tous les stades dans le référentiel (soit ajout d'une définition, soit suppression soit modification
-- La décision est de geler ou écarter tous les stades avec une notion d'âge ou de taille, mais par contre de laisser les stades 
-- emportant une notion de phénotype ou d'histoire de vie, qui est distincte entre les espèces.
-- Indéterminé
set client_encoding to 'UTF8';
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Stade indéterminé (note: ce stade peut correspondre à inconnu (0) ou indeterminé (1) dans la nomenclature du SANDRE)' WHERE std_code ='IND';
-- Suppression du stade alevin de 6 mois à 1 an, cette définition dans le sandre correspondrait à oeuf mais j'en ai pas besoin pour stacomi
DELETE FROM ref.tr_stadedeveloppement_std where std_code='2';
-- Larvaire (Ajout) stade du sandre / suppression de l'alevin de 6 mois à un an
-- Définition adaptée depuis le tableau de Nadine (INRA)
UPDATE ref.tr_stadedeveloppement_std SET (std_libelle,std_definition)=('Larvaire','Stade de sa vie distinct du stade juvénile qui suit l''éclosion jusqu''à la métamorphose. La phase larvaire intervient avant la première prise de nourriture et la nage libre chez les poissons,  mais correspond à une phase plus longue (plusieurs années) chez les lamproies.') WHERE std_code ='3';
 -- Juvénle 
 UPDATE ref.tr_stadedeveloppement_std SET std_definition='Phase entre la phase larvaire et la phase adulte, 
 correspondant à la principale phase de croissance.' WHERE std_code ='4';
  -- Supression du stade Juvenile de 1 à 2 ans
 DELETE FROM ref.tr_stadedeveloppement_std where std_code='5';
-- Adulte (attention la requête prend plusieurs minutes)
 UPDATE ref.tr_stadedeveloppement_std SET std_code='5' where std_code='7';
 UPDATE ref.tr_stadedeveloppement_std SET std_definition='Stade correspondant à la maturité sexuelle.' where std_code='5';

 -- Suppresssion du stade immature (pas cohérent avec juvénile), remplacé par juvénile
 select ref.updatesql('{"azti","bgm","charente","fd80","inra","logrami","migado","saumonrhin","iav","migradour","smatah","nat"}',
'UPDATE t_lot_lot set lot_std_code=''4'' where lot_std_code =''6''');
select ref.updatesql('{"azti","bgm","charente","fd80","inra","logrami","migado","saumonrhin","iav","migradour","smatah","nat"}',
'UPDATE t_bilanmigrationmensuel_bme SET bme_std_code=''4'' where bme_std_code =''6''');
select ref.updatesql('{"azti","bgm","charente","fd80","inra","logrami","migado","saumonrhin","iav","migradour","smatah","nat"}',
'UPDATE t_bilanmigrationjournalier_bjo SET bjo_std_code=''4'' where bjo_std_code =''6''');
select ref.updatesql('{"azti","bgm","charente","fd80","inra","logrami","migado","saumonrhin","iav","migradour","smatah","nat"}',
'UPDATE ts_taxonvideo_txv SET txv_std_code=''4'' where txv_std_code = ''6''');
 DELETE FROM ref.tr_stadedeveloppement_std where std_code='6';
 --Modification du code du stade géniteur pour mise en conformité avec le référentiel du SANDRE
 -- Je propose une définition pour différentier du stade Adulte.
 UPDATE ref.tr_stadedeveloppement_std SET std_code='11' where std_code='8';
 UPDATE ref.tr_stadedeveloppement_std SET std_definition=
 'Stade mature (adulte) ou en cours de maturation qui effectue la migration vers les zones de reproduction.' 
 where std_code='11';
 --Trois stades ci dessous pour mémoire, mais probablement pas d'intérêt pour stacomi, je donne des définitions...;
 -- Nadine n'a pas identifié ça mais ces stades existent dans le dernier dictionnaire des prélèvements biologiques
INSERT INTO ref.tr_stadedeveloppement_std values ('7','Alevins vésiculés',
'Alevins qui après l''éclosion, n''ont pas encore résorbé leur vésicule vittéline. Chez les salmonidés, ce stade correspond à l''émergence du gravier avant la phase de nage/alimentation libre.');
INSERT INTO ref.tr_stadedeveloppement_std values ('12','Alevins à vésicule résorbée', 
'Alevins ayant résorbé leur vésicule vittéline mais n''ayant pas repris leur alimentation');
INSERT INTO ref.tr_stadedeveloppement_std values ('13','Alevins nourrit', 'Alevins ayant repris leur alimentation.');
-- STADES ANGUILLE
-- insertion d'un nouveau stade (normalement pas d'usage pour les stacomi)
INSERT INTO ref.tr_stadedeveloppement_std values ('AGP',
'Argentée pré-migrante', 
'Anguille ayant débuté le processus de métamorphose, rencontré en rivière généralement entre la fin du printemps et l''automne, avant sa migration de dévalaison.');
INSERT INTO ref.tr_stadedeveloppement_std values ('LEP',
'Leptocéphale', 
'Phase larvaire marine de l''anguille qui se métamorphose en civelle à partir du talus continental.');

-- La définion ci-dessous reprennent partiellement les définions du CIEM 
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Phase migrante suivant la phase anguille jaune. Les anguilles argentées sont caractérisées par un dos assombri, un ventre argenté avec une ligne latérale clairement marquée, et une augmentation du diamètre des yeux. Les anguilles argentées effectuent la migration vers la mer (catadrome) et la migration marine vers l''ouest.' 
 where std_code='AGG';
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Anguille entièrement pigmentée, qui réalise la principale partie du processus de colonisation continentale et constitue la phase de croissance jusqu''au stade argenté. Cette phase est souvent nommée sédentaire mais peut effectuer des migrations dans la rivière, entre rivières, et effectuer des déplacements entre les estuaires et les eaux douces dans les deux sens.' 
where std_code='AGJ';
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Jeune anguille non pigmentée ou en cours de pigmentation, qui marque la fin de la métamorphose depuis le stade marin leptocéphale et la migration des eaux continentales depuis le talus continental. C''est au cours du stade civelle que se fait la reprise de l''alimentation.'
 where std_code='CIV';
 
-- ALOSES 
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Juvénile d''alose qui effectue au cours de sa première année la migration depuis les zones de fraie en rivière vers l''estuaire puis les zones côtières.' 
 WHERE std_code='ALS';
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Alose ayant effectué sa reproduction, ces individus amaigris sont parfois rencontrés en dévalaison dans les fleuves.' 
 WHERE std_code='SAB';

 -- LAMPROIE (les phases géniteurs et smolt s'appliquent aux lamproies)
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Stade larvaire de la Lamproie vivant enfouie dans les sédiments ou les fond vaseux des rivières jusqu''à la métamorphose.' 
 WHERE std_code='AMM';

 -- TRM
 UPDATE ref.tr_stadedeveloppement_std SET std_definition='Truite immature souvent dans leur primière année après la migration au stade smolt, trouvés dans les estuaires ou les parties aval des rivières. ' 
 WHERE std_code='FIN';
 -- définition Nadine (INRA)
 UPDATE ref.tr_stadedeveloppement_std SET std_definition='Truite fario légèrement brillante mais considéré comme non amphihaline. ' 
 WHERE std_code='TRFV';
-- Je gèle les codes qui emportent une notion de saisonnalité et d'âge et pas de notion d'histoire de vie ou de phénotype
-- Ces codes n'apparaitront plus dans l'interface
-- Je laisse les stades castillons et grand saumon, pour des raisons d'usage, bien que ces stades correspondent plus à une notion de taille
-- en pratique il vaut mieux utiliser le stade de géniteur puis déterminer les proportions de Grand Saumon et Castillon par post traitement.
-- PSE petit saumon d'été
-- PSP Petit saumon de printemps
-- TDH Truite deux hivers
-- TUH Truite à 1 hivers

-- GANG code grande anguille utilisé pour pouvoir importer des données historiques (marais poitevin)
-- PANG code petite anguille utilisé pour pouvoir importer des données historiques (marais poitevin)
-- POS La notion de post smolt correspond à un juvénile ayant smoltifié et déjà en eau de mer. Je ne vois pas l'interêt d'inclure ici
-- une notiion de position vis à vis de la mer.

 ALTER TABLE ref.tr_stadedeveloppement_std add column std_statut character varying(10);
UPDATE ref.tr_stadedeveloppement_std set std_statut='gelé' where std_code in ('PSE','PSP','TDH','TUH','GANG','PANG','POS');
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Juvénile de saumon durant sa phase en eau douce et avant sa transformation en smolt.' 
 WHERE std_code='PAR';
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Juvénile en cours de transformation physiologique pour passer de l''eau douce à l''eau de mer (il devient argenté). Ce stade peut être divisé en fonction de la maturation du smolt en 1/4, 1/2, 3/4 de smolt'
 WHERE std_code='PRS';
UPDATE ref.tr_stadedeveloppement_std SET std_libelle='Présmolt' WHERE std_code='PRS';
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Juvénile ayant terminé sa transformation physiologique pour passer de l''eau douce à l''eau de mer (argenté).' 
 WHERE std_code='SML';
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Madeleineau ou Castillon, géniteur saumon de retour à sa rivière natale après une hiver de mer.' 
 WHERE std_code='MAD';
 UPDATE ref.tr_stadedeveloppement_std SET std_definition='Grand géniteur de saumon de retour après plusieurs hiver de mer'
 WHERE std_code='GST';
 INSERT INTO ref.tr_stadedeveloppement_std values ('BER','Bécard reconditionné (smolt)', 'Adulte de saumon ou truite après la reproduction,  avant leur nouveau départ vers la mer et ayant smoltifié',NULL);
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Adulte de migrateur potamotoque qui s''est reproduit. Chez le saumon (Béccard), ce stade ne correspond qu''aux individus qui n''ont pas encore smoltifié pour préparer leur passage en eau salée. Ce stade peut s''appliquer par extension aux lamproies et Aloses'
 WHERE std_code='BEC';
-- pour l'export final
--select * from ref.tr_stadedeveloppement_std order by std_statut DESC, std_code;

