﻿/*

SCRIPT N1 du passage à la version v0.4
Ticket 81
A vérifier une second fois

*/
set search_path to iav;
SET client_min_messages TO WARNING;
-- "iav","inra","logrami","migado","migradour","mrm","nat","saumonrhin","smatah"

-- DROP FUNCTION ref.updatesql(character varying[], text);

CREATE OR REPLACE FUNCTION ref.updatesql(myschemas character varying[], scriptsql text)
  RETURNS integer AS
$BODY$
	DECLARE
	totalcount int;	
	nbschema int;
	i integer;
	BEGIN	  
		select INTO nbschema array_length(myschemas,1);
		i=1;
		While (i <=nbschema) LOOP
		EXECUTE 'SET search_path TO '||myschemas[i]||', public';
		RAISE INFO 'execute sql pour schema %',myschemas[i] ;
		EXECUTE scriptsql;		
		i=i+1;
		END LOOP;	
	RETURN nbschema;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION ref.updatesql(character varying[], text)
  OWNER TO postgres;
COMMENT ON FUNCTION ref.updatesql(character varying[], text) IS 'fonction pour lancer un script de mise à jour sur chaque schema';


drop table if exists ref.ts_nomenclature_nom;
create table ref.ts_nomenclature_nom(
nom_id serial primary key,
nom_nomtable character varying(60),
nom_nomenclaturesandreid integer,
nom_datemiseajour date,
nom_commentaire text);


/* ***************

OUVRAGE
* ****************/
/* CODE DE OUVRAGE*/

alter table t_ouvrage_ouv alter column ouv_code type character varying(12); -- comme le ROE à vérifier

/* NATURES OUVRAGE*/


--select * from ref.tr_natureouvrage_nov;
-- Les notions reprennent les notions d'obstacle à l'écoulement, mais la notion d'obstacle au poisson est également importante dans la station de contrôle
-- code X X.1 et X.2
/*
	OBST_ECOUL	Obstacles à l'écoulement	Un obstacle à l’écoulement est un ouvrage lié à l’eau qui est à l’origine d’une modification de l’écoulement des eaux de surface (dans les talwegs, lits mineurs et majeurs de cours d'eau et zones de submersion marine). Seuls les obstacles artificiels (provenant de l’activité humaine) sont pris en compte.	Validé	04/06/2003	04/06/2003
1.1	BAR	Barrage		Validé	04/06/2003	04/06/2003
1.2	SEUIL	Seuil en rivière		Validé	04/06/2003	04/06/2003
1.3	DIGUE	Digue		Validé	04/06/2003	04/06/2003
1.4	PONT	Pont		Validé	04/06/2003	04/06/2003
1.5	EPIS	Epis en rivière	Sur une partie de la largeur du lit mineur ou lit majeur	Validé	04/06/2003	04/06/2003
"0";"Nature de l'ouvrage inconnue"
"1";"Barrage"
"2";"Seuil déversant"
"3";"Filet barrage"
"4";"Usine"
"5";"pas d’ouvrage"
*/
 select ref.updatesql('{"iav"}',
'
alter table t_ouvrage_ouv DROP CONSTRAINT IF EXISTS c_fk_ouv_nov_code;
update t_ouvrage_ouv set ouv_nov_code=''1.1'' where ouv_nov_code=''1'';
update t_ouvrage_ouv set ouv_nov_code=''1.2'' where ouv_nov_code=''2'';
update t_ouvrage_ouv set ouv_nov_code=''0'' where ouv_nov_code=''4'';
update t_ouvrage_ouv set ouv_nov_code=''X.2'' where ouv_nov_code=''3'';
update t_ouvrage_ouv set ouv_nov_code=''X.0'' where ouv_nov_code=''5'';
')



update ref.tr_natureouvrage_nov set nov_code='1.1' where nov_code='1';
update ref.tr_natureouvrage_nov set (nov_code,nov_nom)=('1.2','Seuil en rivière') where nov_code='2';
update ref.tr_natureouvrage_nov set nov_code='X.0' where nov_code='5';
update ref.tr_natureouvrage_nov set nov_code='X.2' where nov_code='3';
delete from ref.tr_natureouvrage_nov where nov_code='4'; --usine on vire
insert into ref.tr_natureouvrage_nov values ('X.1','barrière électrique');

 alter table t_ouvrage_ouv ADD CONSTRAINT c_fk_ouv_nov_code FOREIGN KEY (ouv_nov_code)
      REFERENCES ref.tr_natureouvrage_nov (nov_code) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;


select ref.updatesql('{"iav"}',
'
alter table t_ouvrage_ouv drop column ouv_denivelee_max;
');

insert into ref.ts_nomenclature_nom(
				nom_nomtable,
				nom_nomenclaturesandreid,
				nom_datemiseajour,
				nom_commentaire) 
			values (
				'ref.tr_natureouvrage_nov',
				284,
				'14/06/2016',
				'En plus du référentiel des obstacles à l''écoulement, la notion de barrage pour la migration est retenue'
				);
/* ECHAPPEMENT*/

-- TODO remplacer ouvrage par station pour les taux d'échappement
-- méthode d'estimation du taux d'échappement
/*
Code	Mnémonique	Libellé
0	Mode inconnu	Mode d'estimation inconnu
1	Calcul	Par calcul sur des données élémentaires
2	Avis d'experts	Sur avis d'experts
3	Non estimable	Non estimable
4	Mesuré	Mesuré
*/
-- TODO rajouter une table référentielle et mettre le lien dans txe_methode_estimation 
-- rajouter une contrainte de clé étrangère et faire sauter c_ck_txe_methode_estimation
-- vérifier compatibilité JAVA

-- idem pour niveaux d'échappement
/*
Code	Mnémonique	Libellé	Définition
0	Inconnu	Niveau d'échappement inconnu	
1	Nul	Niveau d'échappement nul	Taux d'échappement de l'ordre de 0%
2	Faible	Niveau d'échappement faible	Taux d'échappement < 33 %
3	Moyen	Niveau d'échappement moyen	Taux d'échappemententre 33 % et 66 %
4	Fort	Niveau d'échappement fort	Taux d'échappement > 66 %
*/


select ref.updatesql('{"iav"}',
'
ALTER TABLE tj_tauxechappement_txe drop column if exists txe_sta_code ;
ALTER TABLE tj_tauxechappement_txe add column txe_sta_code character varying(8);
');
-- mise à jour des valeurs de l''ouvrage
select ref.updatesql('{"iav"}',
'UPDATE tj_tauxechappement_txe set txe_sta_code=sub.ouv_sta_code from (
select ouv_sta_code,txe_ouv_identifiant from t_ouvrage_ouv join tj_tauxechappement_txe on txe_ouv_identifiant=ouv_identifiant) as sub
where sub.txe_ouv_identifiant=tj_tauxechappement_txe.txe_ouv_identifiant;
ALTER TABLE tj_tauxechappement_txe drop column txe_ouv_identifiant cascade;
ALTER TABLE tj_tauxechappement_txe ADD CONSTRAINT c_pk_txe PRIMARY KEY(txe_sta_code, txe_org_code, txe_tax_code, txe_std_code, txe_date_debut, txe_date_fin);
ALTER TABLE tj_tauxechappement_txe
    DROP CONSTRAINT IF EXISTS c_fk_txe_sta_code;
ALTER TABLE  tj_tauxechappement_txe
    ADD CONSTRAINT c_fk_txe_sta_code FOREIGN KEY (txe_sta_code, txe_org_code) REFERENCES t_station_sta(sta_code, sta_org_code) ON UPDATE CASCADE;
');
/*
DF
*/

/*
Code 	Mnemonique 	Libellé 	Définition
1	PASSERALENTI	Passe à ralentisseurs	La passe à ralentisseurs est un canal rectiligne à pente relativement forte (entre 1/10 et 1/5 suivant le type de passe et l'espèce considérée), de section rectangulaire, dans lequel sont installés sur le fond uniquement (passes à ralentisseurs de fond suractifs, passes à ralentisseurs à chevrons épais) ou à la fois sur le fond et les parois latérales (passes à ralentisseurs plans) des déflecteurs destinés à réduire les vitesses moyennes de l'écoulement. Ces déflecteurs, de formes plus ou moins complexes, donnent naissance à des courants hélicoïdaux qui assurent une forte dissipation d'énergie au sein de l'écoulement.
2	PASSEBASSIN	Passes à bassins successifs	Dispositif très commun et de conception relativement ancienne, consistant à diviser la hauteur à franchir en plusieurs petites chutes formant une série de bassins. Il existe plusieurs types de communications entre bassins, le passage de l'eau pouvant s'effectuer soit par déversement de surface, soit par écoulement à travers un ou plusieurs orifices ménagés dans la cloison, soit encore par une ou plusieurs fentes ou échancrures. On rencontre également des passes de type mixte.
3	ECLUSEPOISS	Ecluse à poisson	ascenseur à poissons permet de remonter les poissons, piégés dans une cuve, et de les déverser en amont de l'obstacle. A noter que les écluses à bateau permettent aussi le franchissement piscicole.
4	EXUTOIREDEVAL	Exutoire de dévalaison	
5	PASANG	Passe à anguille	Rampe équipée d'un matériau facilitant la progression des jeunes anguilles à la montaison. Les matériaux employés peuvent être d'origine naturelle (cailloux, branchages, bruyère, paille) ou artificielle (brosses , plots en béton...). Ce sont essentiellement des substrats de type brosse qui sont utilisés aujourd'hui en France. L'espacement entre chaque faisceau de soies dépend de la taille des individus à faire passer.
5a	TAPBROSSE	Tapis brosse	
5b	SUBRUGU	Substrat rugueux	
5c	PASSPIEGE	Passe piège	
7	PREBAR	Pré-barrage	Dispositifs formés de plusieurs petits seuils, le plus souvent en béton ou enrochements jointoyés, créant à l'aval de l'obstacle des grands bassins qui fractionnent la chute à franchir. Ces prébarrages sont généralement implantés sur une partie de la largeur de l'obstacle, à proximité de l'une des deux rives pour en faciliter l'entretien.
8	RAMPE	Rampe	
8a	RPEPARTLARG	Rampe sur partie de la largeur	
8b	RPETOTLARG	Rampe sur totalité de la largeur	
9	RIVIERE	Rivière de contournement	Dispositif consistant à relier biefs amont et aval par un chenal dans lequel l'énergie est dissipée et les vitesses réduites par la rugosité du fond et celle des parois ainsi que par une succession d'obstacles (blocs, épis, seuils) plus ou moins régulièrement répartis, reproduisant en quelque sorte l'écoulement dans un cours d'eau naturel.
10	AUTRES	Autre type de dispositif	
*/
--select * from ref.tr_typedf_tdf order by tdf_code;
-- TODO mettre à jour avec les infos du SANDRE, faire sauter les contraintes, reprendre table du sandre tel quel (ajouter une mnémonique), 
--.modifier les codes dans t_dispositiffranchissement_dif
--.
--Attention il s'agit d'une table de jointure tj_dfesttype_tdf
--suppression de la constrainte des tables
select ref.updatesql('{"iav"}',
'ALTER TABLE tj_dfesttype_dft
  DROP CONSTRAINT IF EXISTS c_fk_dft_tdf_code;');
ALTER TABLE ref.tr_typedf_tdf add column tdf_mnemonique character varying(15);
ALTER TABLE ref.tr_typedf_tdf add column tdf_definition text; 

UPDATE ref.tr_typedf_tdf set (tdf_mnemonique,tdf_definition)=('PASSEBASSIN','Dispositif très commun et de conception relativement ancienne, consistant à diviser la hauteur à franchir en plusieurs petites chutes formant une série de bassins. Il existe plusieurs types de communications entre bassins, le passage de l''eau pouvant s''effectuer soit par déversement de surface, soit par écoulement à travers un ou plusieurs orifices ménagés dans la cloison, soit encore par une ou plusieurs fentes ou échancrures. On rencontre également des passes de type mixte.')
WHERE tdf_code=1;
UPDATE ref.tr_typedf_tdf set (tdf_mnemonique,tdf_definition)=('PASSERALENTI','La passe à ralentisseurs est un canal rectiligne à pente relativement forte (entre 1/10 et 1/5 suivant le type de passe et l''espèce considérée), de section rectangulaire, dans lequel sont installés sur le fond uniquement (passes à ralentisseurs de fond suractifs, passes à ralentisseurs à chevrons épais) ou à la fois sur le fond et les parois latérales (passes à ralentisseurs plans) des déflecteurs destinés à réduire les vitesses moyennes de l''écoulement. Ces déflecteurs, de formes plus ou moins complexes, donnent naissance à des courants hélicoïdaux qui assurent une forte dissipation d''énergie au sein de l''écoulement.')
WHERE tdf_code=2;
-- modification de la définition pour virer l'ascenseur	
UPDATE ref.tr_typedf_tdf set (tdf_mnemonique,tdf_definition)=('ECLUSEPOISS','L’écluse à poissons est un dispositif au fonctionnement voisin de celui observé pour une écluse de navigation. Les poissons sont attirés dans une chambre puis éclusés comme on écluserait un bateau.  On incite le poisson à sortir de l''écluse en créant à l''intérieur de celle- ci un courant descendant grâce à l''ouverture d''un by-pass situé dans la partie inférieure du dispositif. ')
WHERE tdf_code=3;
-- Récupération de la définition depuis écluse
UPDATE ref.tr_typedf_tdf set (tdf_mnemonique,tdf_definition)=('ASCPOISS','L''ascenseur à poissons permet de remonter les poissons, piégés dans une cuve, et de les déverser en amont de l''obstacle.')
WHERE tdf_code=4;	
UPDATE ref.tr_typedf_tdf set (tdf_mnemonique,tdf_definition)=('PREBAR','Dispositifs formés de plusieurs petits seuils, le plus souvent en béton ou enrochements jointoyés, créant à l''aval de l''obstacle des grands bassins qui fractionnent la chute à franchir. Ces prébarrages sont généralement implantés sur une partie de la largeur de l''obstacle, à proximité de l''une des deux rives pour en faciliter l''entretien.')
WHERE tdf_code=5;	
UPDATE ref.tr_typedf_tdf set (tdf_mnemonique,tdf_definition)=('RIVIERE','Dispositif consistant à relier biefs amont et aval par un chenal dans lequel l''énergie est dissipée et les vitesses réduites par la rugosité du fond et celle des parois ainsi que par une succession d''obstacles (blocs, épis, seuils) plus ou moins régulièrement répartis, reproduisant en quelque sorte l''écoulement dans un cours d''eau naturel.')
WHERE tdf_code=6;	
UPDATE ref.tr_typedf_tdf set (tdf_mnemonique,tdf_definition)=('PASANG','Rampe équipée d''un matériau facilitant la progression des jeunes anguilles à la montaison. Les matériaux employés peuvent être d''origine naturelle (cailloux, branchages, bruyère, paille) ou artificielle (brosses , plots en béton...). Ce sont essentiellement des substrats de type brosse qui sont utilisés aujourd''hui en France. L''espacement entre chaque faisceau de soies dépend de la taille des individus à faire passer.')
WHERE tdf_code=7;	
UPDATE ref.tr_typedf_tdf set (tdf_mnemonique,tdf_definition)=('GUIDE','Le dispositif consiste en une grille ou un guidage par une barrière électrique permettant d''orienter les poissons vers le piège.')
WHERE tdf_code=8;	
-- Définition cédric ci dessous
UPDATE ref.tr_typedf_tdf set (tdf_mnemonique,tdf_definition)=('EXUTOIREDEVAL','Dispositif facilitant le franchissement d''un barrage par les poissons lors de leur migration vers l''aval. Ce dispositifif peut être.')
WHERE tdf_code=9;	
UPDATE ref.tr_typedf_tdf set (tdf_mnemonique,tdf_definition)=('AUTRES',NULL)
WHERE tdf_code=10;	

ALTER TABLE ref.tr_typedf_tdf rename column tdf_code to tdf_codetemp;
ALTER TABLE ref.tr_typedf_tdf add column tdf_code character varying(2);
 select ref.updatesql('{"iav"}',
'
ALTER TABLE tj_dfesttype_dft rename column dft_tdf_code to dft_tdf_codetemp;
ALTER TABLE tj_dfesttype_dft add column dft_tdf_code character varying(2);
')


UPDATE ref.tr_typedf_tdf set tdf_code='1' where tdf_codetemp=2;
UPDATE ref.tr_typedf_tdf set tdf_code='2' where tdf_codetemp=1;
UPDATE ref.tr_typedf_tdf set tdf_code='3' where tdf_codetemp=3;
UPDATE ref.tr_typedf_tdf set tdf_code='6' where tdf_codetemp=4;
UPDATE ref.tr_typedf_tdf set tdf_code='7' where tdf_codetemp=5;
UPDATE ref.tr_typedf_tdf set tdf_code='9' where tdf_codetemp=6;
UPDATE ref.tr_typedf_tdf set tdf_code='5' where tdf_codetemp=7;
UPDATE ref.tr_typedf_tdf set tdf_code='11' where tdf_codetemp=8;
UPDATE ref.tr_typedf_tdf set tdf_code='4' where tdf_codetemp=9;
UPDATE ref.tr_typedf_tdf set tdf_code='10' where tdf_codetemp=10;
insert into ref.tr_typedf_tdf(tdf_codetemp,tdf_code,tdf_mnemonique,tdf_libelle,tdf_definition) values
(
11,'5a','TAPBROSSE','Tapis Brosse',NULL
);
insert into ref.tr_typedf_tdf(tdf_codetemp,tdf_code,tdf_mnemonique,tdf_libelle,tdf_definition) values
(
12,'5b','SUBRUGU','Substrat rugueux',NULL
);
insert into ref.tr_typedf_tdf(tdf_codetemp,tdf_code,tdf_mnemonique,tdf_libelle,tdf_definition) values
(
13,'5c','PASSPIEGE','Passe piège',NULL
);
insert into ref.tr_typedf_tdf(tdf_codetemp,tdf_code,tdf_mnemonique,tdf_libelle,tdf_definition) values
(
14,'8','RAMPE','Rampe',NULL
);


update ref.tr_typedf_tdf set tdf_libelle='Passe à anguille' where tdf_code='5';
update ref.tr_typedf_tdf set tdf_libelle='Pré-barrage' where tdf_code='7';
update ref.tr_typedf_tdf set tdf_libelle='Ascenseur à poissons' where tdf_code='6';
update ref.tr_typedf_tdf set tdf_libelle='Autre type de dispositif' where tdf_code='10';
ALTER TABLE ref.tr_typedf_tdf DROP COLUMN tdf_codetemp CASCADE;
ALTER TABLE ref.tr_typedf_tdf
  ADD CONSTRAINT c_pk_tdf PRIMARY KEY(tdf_code);
 ALTER TABLE ref.tr_typedf_tdf
  ADD CONSTRAINT c_uk_tdfmnemonique UNIQUE(tdf_mnemonique); 

-- modification des tables tj_dfesttype_dft;
-- et réinsertion des contraintes de clé

  select ref.updatesql('{"iav"}',
'
UPDATE tj_dfesttype_dft set dft_tdf_code=''1'' where dft_tdf_codetemp=2;
UPDATE tj_dfesttype_dft set dft_tdf_code=''2'' where dft_tdf_codetemp=1;
UPDATE tj_dfesttype_dft set dft_tdf_code=''3'' where dft_tdf_codetemp=3;
UPDATE tj_dfesttype_dft set dft_tdf_code=''6'' where dft_tdf_codetemp=4;
UPDATE tj_dfesttype_dft set dft_tdf_code=''7'' where dft_tdf_codetemp=5;
UPDATE tj_dfesttype_dft set dft_tdf_code=''9'' where dft_tdf_codetemp=6;
UPDATE tj_dfesttype_dft set dft_tdf_code=''5'' where dft_tdf_codetemp=7;
UPDATE tj_dfesttype_dft set dft_tdf_code=''11'' where dft_tdf_codetemp=8;
UPDATE tj_dfesttype_dft set dft_tdf_code=''4'' where dft_tdf_codetemp=9;
UPDATE tj_dfesttype_dft set dft_tdf_code=''10'' where dft_tdf_codetemp=10;
ALTER TABLE tj_dfesttype_dft DROP COLUMN dft_tdf_codetemp;
ALTER TABLE tj_dfesttype_dft
  ADD CONSTRAINT c_pk_dft PRIMARY KEY(dft_df_identifiant, dft_tdf_code, dft_rang, dft_org_code);
ALTER TABLE tj_dfesttype_dft
  ADD CONSTRAINT c_fk_dft_tdf_code FOREIGN KEY (dft_tdf_code)
      REFERENCES ref.tr_typedf_tdf (tdf_code) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;        
');

 
  


insert into ref.ts_nomenclature_nom(nom_nomtable,nom_nomenclaturesandreid,nom_datemiseajour,nom_commentaire) values ('ref.tr_typedf_tdf',571,'14/06/2016','Ajout d''un type (barrière, filet, barrière électrique) qui ne correspond pas au référentiel, mais est utile pour la description des stations (ex : Cerisel)');


      
/*
DC
*/



--select * from ref.tr_typedc_tdc
/*
Code 	Mnemonique 		Libellé 	Définition
0	Dispositif inconnu	Type de dispositif inconnu	
1	Piégeage		Piégeage	
2	Résistivité		Compteur à résistivité	
3	Analyse visuelle d'image	Analyse visuelle d'image (reconnaissance directe, par bande vidéo, assistée par ordinateur, ...)	
4	Accoustique		Comptage accoustique	
5	Optoélectronique	Comptage optoélectronique
*/
alter table ref.tr_typedc_tdc add column tdc_definition text;
update ref.tr_typedc_tdc set tdc_definition='Tube vers lequel le poisson est guidé à l''aide de grilles et qui utilise les variations de résistivité au moment du passage du poisson pour compter un passage. Ces dispositifs nécessitent une calibration et ne permettent pas d''identifier l''espèce.';
update ref.tr_typedc_tdc set tdc_definition='Analyse visuelle d''image, reconnaissance par bande vidéo, assitée par un ordinateur, inclue les echos générés par des radars multifaisceaux permettant l''interpretation d''échos accoustiques.';
-- je ne sais pas ce qu'est un comptage opto-éléctronique.
delete from ref.tr_typedc_tdc where tdc_code=5; 
insert into ref.tr_typedc_tdc values (5,'Comptage radio','Les comptages utilisent la technologie RFID, qui utilise des champs électromagnétiques générés par des antennes, des boucles ou des cables pour déclencher la réponse d''une marque (pit tag, cables NEDAP...).');
update ref.tr_typedc_tdc set tdc_definition='Comptage du passage sur la base de balises accoustiques qui détectent les signaux envoyés par les balises implantées sur les poissons.'
where tdc_code=4;
--suppression d'engins de capture au profit d'une seule ligne
 select ref.updatesql('{"iav"}',
'ALTER TABLE t_dispositifcomptage_dic  DROP CONSTRAINT IF EXISTS c_fk_dic_tdc_code ');

delete from ref.tr_typedc_tdc where tdc_code in (6,7,8);
insert into ref.tr_typedc_tdc values(6, 'Engin de pêche','Utilisation d''engins de pêche comme des verveux, filets, tézelle, dideau pour intercepter les poissons en migration, ces piégeages peuvent être partiels ou complets');
 select ref.updatesql('{"iav"}',
'update t_dispositifcomptage_dic  set dic_tdc_code=6 where dic_tdc_code in (7,8) ');

 select ref.updatesql('{"iav"}',
'ALTER TABLE t_dispositifcomptage_dic  ADD CONSTRAINT c_fk_dic_tdc_code FOREIGN KEY (dic_tdc_code)
      REFERENCES ref.tr_typedc_tdc (tdc_code) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
 ');
insert into ref.ts_nomenclature_nom(nom_nomtable,nom_nomenclaturesandreid,nom_datemiseajour,nom_commentaire) values ('ref.tr_typedc_tdc',131,'14/06/2016','Référentiel du SANDRE a mettre à jour, modification des définitions. Ajout de types.');


/*
OPERATION
*/


/*
LOT
*/

/*
Stade de développment
*/
	

-- Suppression du rang (remplacé par les masques, correction en JAVA effectuée)
-- Je corrige d'abord la vue 'v_taxon_tax'


 select ref.updatesql('{"iav"}',
'DROP VIEW v_taxon_tax');
 select ref.updatesql('{"iav"}',
'CREATE OR REPLACE VIEW v_taxon_tax AS 
 SELECT tax.tax_code, tax.tax_nom_latin, tax.tax_nom_commun, tax.tax_ntx_code, tax.tax_tax_code, tax.tax_rang, txv.txv_code, txv.txv_tax_code, txv.txv_std_code, std.std_code, std.std_libelle
   FROM ref.tr_taxon_tax tax
   RIGHT JOIN ts_taxonvideo_txv txv ON tax.tax_code::text = txv.txv_tax_code::text
   LEFT JOIN ref.tr_stadedeveloppement_std std ON txv.txv_std_code::text = std.std_code::text');
ALTER TABLE ref.tr_stadedeveloppement_std drop column std_rang;

-- correction d'une erreur sur le stade MAD (merci Marion)
--suppression des contraintes de clé

--t_bilanmigrationjournalier_bjo
 select ref.updatesql('{"iav"}',
'ALTER TABLE t_bilanmigrationjournalier_bjo DROP CONSTRAINT IF EXISTS c_fk_bjo_std_code ');
--t_bilanmigrationmensuel_bme
 select ref.updatesql('{"iav"}',
'ALTER TABLE t_bilanmigrationmensuel_bme DROP CONSTRAINT IF EXISTS c_fk_bme_std_code ');
--t_lot_lot
 select ref.updatesql('{"iav"}',
'ALTER TABLE t_lot_lot DROP CONSTRAINT IF EXISTS c_fk_lot_std_code ');
--ts_taxonvideo_txv
 select ref.updatesql('{"iav"}',
'ALTER TABLE ts_taxonvideo_txv DROP CONSTRAINT IF EXISTS c_fk_std_code ');

-- correction du stade 'MAD(espace)' dans toutes les tables
 select ref.updatesql('{"iav"}',
'UPDATE t_lot_lot set lot_std_code=''MAD'' where lot_std_code like ''MAD ''');
 select ref.updatesql('{"iav"}',
'UPDATE t_bilanmigrationmensuel_bme SET bme_std_code=''MAD'' where bme_std_code like ''MAD ''');
 select ref.updatesql('{"iav"}',
'UPDATE t_bilanmigrationjournalier_bjo SET bjo_std_code=''MAD'' where bjo_std_code like ''MAD ''');
 select ref.updatesql('{"iav"}',
'UPDATE ts_taxonvideo_txv SET txv_std_code=''MAD'' where txv_std_code like ''MAD ''');
UPDATE ref.tr_stadedeveloppement_std set std_code='MAD' where std_code like 'MAD ';

-- réinsertion des contraintes de clé étrangère avec un UPDATE CASCADE
 --  t_bilanmigrationjournalier_bjo 
select ref.updatesql('{"iav"}',
'ALTER TABLE t_bilanmigrationjournalier_bjo
  ADD CONSTRAINT c_fk_bjo_std_code FOREIGN KEY (bjo_std_code)
      REFERENCES ref.tr_stadedeveloppement_std (std_code) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION');
 --  t_bilanmigrationmensuel_bme   
select ref.updatesql('{"iav"}',   
'ALTER TABLE t_bilanmigrationmensuel_bme
  ADD CONSTRAINT c_fk_bme_std_code FOREIGN KEY (bme_std_code)
      REFERENCES ref.tr_stadedeveloppement_std (std_code) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION');
 -- t_lot_lot     
select ref.updatesql('{"iav"}',
'ALTER TABLE t_lot_lot
  ADD CONSTRAINT c_fk_lot_std_code FOREIGN KEY (lot_std_code)
      REFERENCES ref.tr_stadedeveloppement_std (std_code) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION');     
-- ts_taxonvideo_txv      
select ref.updatesql('{"iav"}',
'ALTER TABLE ts_taxonvideo_txv
  ADD CONSTRAINT c_fk_txv_std_code FOREIGN KEY (txv_std_code)
      REFERENCES ref.tr_stadedeveloppement_std (std_code) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION');



-- ajout d'une colonne définition dans les stades

ALTER TABLE ref.tr_stadedeveloppement_std ADD COLUMN std_definition text;

-- modification de tous les stades dans le référentiel (soit ajout d'une définition, soit suppression soit modification
-- La décision est de geler ou écarter tous les stades avec une notion d'âge ou de taille, mais par contre de laisser les stades 
-- emportant une notion de phénotype ou d'histoire de vie, qui est distincte entre les espèces.
-- Indéterminé
set client_encoding to 'UTF8';
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Stade indéterminé (note: ce stade peut correspondre à inconnu (0) ou indeterminé (1) dans la nomenclature du SANDRE)' WHERE std_code ='IND';
-- Suppression du stade alevin de 6 mois à 1 an, cette définition dans le sandre correspondrait à oeuf mais j'en ai pas besoin pour stacomi
DELETE FROM ref.tr_stadedeveloppement_std where std_code='2';
-- Larvaire (Ajout) stade du sandre / suppression de l'alevin de 6 mois à un an
-- Définition adaptée depuis le tableau de Nadine (INRA)
UPDATE ref.tr_stadedeveloppement_std SET (std_libelle,std_definition)=('Larvaire','Stade de sa vie distinct du stade juvénile qui suit l''éclosion jusqu''à la métamorphose. La phase larvaire intervient avant la première prise de nourriture et la nage libre chez les poissons,  mais correspond à une phase plus longue (plusieurs années) chez les lamproies.') WHERE std_code ='3';
 -- Juvénle 
 UPDATE ref.tr_stadedeveloppement_std SET std_definition='Phase entre la phase larvaire et la phase adulte, 
 correspondant à la principale phase de croissance.' WHERE std_code ='4';
  -- Supression du stade Juvenile de 1 à 2 ans
 DELETE FROM ref.tr_stadedeveloppement_std where std_code='5';
-- Adulte (attention la requête prend plusieurs minutes)
 UPDATE ref.tr_stadedeveloppement_std SET std_code='5' where std_code='7';
 UPDATE ref.tr_stadedeveloppement_std SET std_definition='Stade correspondant à la maturité sexuelle.' where std_code='5';

 -- Suppresssion du stade immature (pas cohérent avec juvénile), remplacé par juvénile
  select ref.updatesql('{"iav"}',
'UPDATE t_lot_lot set lot_std_code=''4'' where lot_std_code =''6''');
 select ref.updatesql('{"iav"}',
'UPDATE t_bilanmigrationmensuel_bme SET bme_std_code=''4'' where bme_std_code =''6''');
 select ref.updatesql('{"iav"}',
'UPDATE t_bilanmigrationjournalier_bjo SET bjo_std_code=''4'' where bjo_std_code =''6''');
 select ref.updatesql('{"iav"}',
'UPDATE ts_taxonvideo_txv SET txv_std_code=''4'' where txv_std_code = ''6''');
 DELETE FROM ref.tr_stadedeveloppement_std where std_code='6';
 --Modification du code du stade géniteur pour mise en conformité avec le référentiel du SANDRE
 -- Je propose une définition pour différentier du stade Adulte.
 UPDATE ref.tr_stadedeveloppement_std SET std_code='11' where std_code='8';
 UPDATE ref.tr_stadedeveloppement_std SET std_definition=
 'Stade mature (adulte) ou en cours de maturation qui effectue la migration vers les zones de reproduction.' 
 where std_code='11';
 --Trois stades ci dessous pour mémoire, mais probablement pas d'intérêt pour stacomi, je donne des définitions...;
 -- Nadine n'a pas identifié ça mais ces stades existent dans le dernier dictionnaire des prélèvements biologiques
INSERT INTO ref.tr_stadedeveloppement_std values ('7','Alevins vésiculés',
'Alevins qui après l''éclosion, n''ont pas encore résorbé leur vésicule vittéline. Chez les salmonidés, ce stade correspond à l''émergence du gravier avant la phase de nage/alimentation libre.');
INSERT INTO ref.tr_stadedeveloppement_std values ('12','Alevins à vésicule résorbée', 
'Alevins ayant résorbé leur vésicule vittéline mais n''ayant pas repris leur alimentation');
INSERT INTO ref.tr_stadedeveloppement_std values ('13','Alevins nourrit', 'Alevins ayant repris leur alimentation.');
-- STADES ANGUILLE
-- insertion d'un nouveau stade (normalement pas d'usage pour les stacomi)
INSERT INTO ref.tr_stadedeveloppement_std values ('AGP',
'Argentée pré-migrante', 
'Anguille ayant débuté le processus de métamorphose, rencontré en rivière généralement entre la fin du printemps et l''automne, avant sa migration de dévalaison.');
INSERT INTO ref.tr_stadedeveloppement_std values ('LEP',
'Leptocéphale', 
'Phase larvaire marine de l''anguille qui se métamorphose en civelle à partir du talus continental.');

-- La définion ci-dessous reprennent partiellement les définions du CIEM 
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Phase migrante suivant la phase anguille jaune. Les anguilles argentées sont caractérisées par un dos assombri, un ventre argenté avec une ligne latérale clairement marquée, et une augmentation du diamètre des yeux. Les anguilles argentées effectuent la migration vers la mer (catadrome) et la migration marine vers l''ouest.' 
 where std_code='AGG';
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Anguille entièrement pigmentée, qui réalise la principale partie du processus de colonisation continentale et constitue la phase de croissance jusqu''au stade argenté. Cette phase est souvent nommée sédentaire mais peut effectuer des migrations dans la rivière, entre rivières, et effectuer des déplacements entre les estuaires et les eaux douces dans les deux sens.' 
where std_code='AGJ';
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Jeune anguille non pigmentée ou en cours de pigmentation, qui marque la fin de la métamorphose depuis le stade marin leptocéphale et la migration des eaux continentales depuis le talus continental. C''est au cours du stade civelle que se fait la reprise de l''alimentation.'
 where std_code='CIV';
 
-- ALOSES 
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Juvénile d''alose qui effectue au cours de sa première année la migration depuis les zones de fraie en rivière vers l''estuaire puis les zones côtières.' 
 WHERE std_code='ALS';
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Alose ayant effectué sa reproduction, ces individus amaigris sont parfois rencontrés en dévalaison dans les fleuves.' 
 WHERE std_code='SAB';

 -- LAMPROIE (les phases géniteurs et smolt s'appliquent aux lamproies)
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Stade larvaire de la Lamproie vivant enfouie dans les sédiments ou les fond vaseux des rivières jusqu''à la métamorphose.' 
 WHERE std_code='AMM';

 -- TRM
 UPDATE ref.tr_stadedeveloppement_std SET std_definition='Truite immature souvent dans leur primière année après la migration au stade smolt, trouvés dans les estuaires ou les parties aval des rivières. ' 
 WHERE std_code='FIN';
 -- définition Nadine (INRA)
 UPDATE ref.tr_stadedeveloppement_std SET std_definition='Truite fario légèrement brillante mais considéré comme non amphihaline. ' 
 WHERE std_code='TRFV';
-- Je gèle les codes qui emportent une notion de saisonnalité et d'âge et pas de notion d'histoire de vie ou de phénotype
-- Ces codes n'apparaitront plus dans l'interface
-- Je laisse les stades castillons et grand saumon, pour des raisons d'usage, bien que ces stades correspondent plus à une notion de taille
-- en pratique il vaut mieux utiliser le stade de géniteur puis déterminer les proportions de Grand Saumon et Castillon par post traitement.
-- PSE petit saumon d'été
-- PSP Petit saumon de printemps
-- TDH Truite deux hivers
-- TUH Truite à 1 hivers

-- GANG code grande anguille utilisé pour pouvoir importer des données historiques (marais poitevin)
-- PANG code petite anguille utilisé pour pouvoir importer des données historiques (marais poitevin)
-- POS La notion de post smolt correspond à un juvénile ayant smoltifié et déjà en eau de mer. Je ne vois pas l'interêt d'inclure ici
-- une notiion de position vis à vis de la mer.

 ALTER TABLE ref.tr_stadedeveloppement_std add column std_statut character varying(10);
 
UPDATE ref.tr_stadedeveloppement_std set std_statut='gelé' where std_code in ('PSE','PSP','TDH','TUH','GANG','PANG','POS','SAB');
-- il est très possible que PANG et GANG ne soient pas dans la base
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Juvénile de saumon durant sa phase en eau douce et avant sa transformation en smolt.' 
 WHERE std_code='PAR';
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Juvénile en cours de transformation physiologique pour passer de l''eau douce à l''eau de mer (il devient argenté). Ce stade peut être divisé en fonction de la maturation du smolt en 1/4, 1/2, 3/4 de smolt'
 WHERE std_code='PRS';
UPDATE ref.tr_stadedeveloppement_std SET std_libelle='Présmolt' WHERE std_code='PRS';
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Juvénile ayant terminé sa transformation physiologique pour passer de l''eau douce à l''eau de mer (argenté).' 
 WHERE std_code='SML';
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Madeleineau ou Castillon, géniteur saumon de retour après un hiver de mer.' 
 WHERE std_code='MAD';
 UPDATE ref.tr_stadedeveloppement_std SET std_definition='Grand géniteur de saumon de plusieurs hiver de mer.'
 WHERE std_code='GST';
 INSERT INTO ref.tr_stadedeveloppement_std values ('BER','Bécard reconditionné (smolt)', 'Adulte de saumon ou truite après la reproduction,  avant leur nouveau départ vers la mer et ayant smoltifié',NULL);
UPDATE ref.tr_stadedeveloppement_std SET std_definition='Adulte de migrateur potamotoque qui s''est reproduit. Chez le saumon (Béccard), ce stade ne correspond qu''aux individus qui n''ont pas encore smoltifié pour préparer leur passage en eau salée. Ce stade peut s''appliquer par extension aux lamproies et Aloses'
 WHERE std_code='BEC';
--select * from ref.tr_stadedeveloppement_std order by std_statut DESC, std_code;


insert into ref.ts_nomenclature_nom(nom_nomtable,nom_nomenclaturesandreid,nom_datemiseajour,nom_commentaire) values ('ref.tr_stadedeveloppement_std',497,'14/06/2016','Correspondance uniquement partielle vers le référentiel. Les définitions ont été reprises. De nombreux stades ne sont pas dans le référentiel.');


 
--TODO renseigner les tax_tax_code
/*
- Méthode d'obtention : Moyen d'obtention de la mesure de l’effectif du lot de poissons. La liste des valeurs autorisées est définie par le Sandre dans la nomenclature n°141.

Code	Mnémonique	Libellé	Définition	Statut	Création	Modification
0	Mode inconnu	Mode d'estimation inconnu		Validé	04/09/1998	26/03/2010
1	Calcul	Par calcul sur des données élémentaires		Validé	04/09/1998	26/03/2010
2	Avis d'experts	Sur avis d'experts		Validé	04/09/1998	26/03/2010
3	Non estimable	Non estimable		Validé	04/09/1998	26/03/2010
4	Mesuré	Mesuré		Validé	02/04/2010	02/04/2010
*/
--TODO Modifier la contrainte, faire appel à une table référentielle, en pratique c'est la même que pour les taux d'échappement.


-- DEVENIR
-- TODO changer les libéllés et les dernières lignes
/*
Code	Mnémonique	Libellé	Définition
0	Inconnu	Devenir inconnu	
1	Relaché	Relâché au droit de la station	Relâché au droit de la station
2	Trépassé	Trépassé	Trépassé
3	Transféré	Transféré dans le milieu naturel en dehors de la station	Transféré dans le milieu naturel en dehors de la station
4	Elevage	Mis en élevage	Mis en élevage
5	Prelévé	Prélevé pour étude	Prélevé pour étude
6	Relâché pour recapture	Relâché avant la station et susceptible d’être recapturé	
*/
--select * from ref.tr_devenirlot_dev;
/*
"1";"Relâché au droit de la station";1
"0";"Devenir inconnu";2
"2";"Trépassé";3
"3";"Transporté dans le milieu naturel ";4
"5";"Relâché avant l'entrée de la station, susceptible d'être recapturé dans la même station";5
"4";"Mis en élevage";6
*/
--select count(*) from information_schema.constraint_column_usage where table_catalog='bd_contmig_nat' ;
--select * from information_schema.constraint_column_usage where constraint_name='c_fk_lot_dev_code';
--select * from pg_catalog.pg_constraint where conname='c_fk_lot_dev_code';
select ref.updatesql('{"iav"}',
'ALTER TABLE t_lot_lot DROP CONSTRAINT if exists c_fk_lot_dev_code ');--12
update ref.tr_devenirlot_dev set dev_code='6' where dev_code='5';
select ref.updatesql('{"iav"}',
'update t_lot_lot set lot_dev_code=''6'' where lot_dev_code=''5''');--13
insert into ref.tr_devenirlot_dev values ('5','Prélevé pour étude',7);
select ref.updatesql('{"iav"}',
'ALTER TABLE t_lot_lot ADD CONSTRAINT c_fk_lot_dev_code FOREIGN KEY (lot_dev_code)
      REFERENCES ref.tr_devenirlot_dev (dev_code) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION');

insert into ref.ts_nomenclature_nom(nom_nomtable,nom_nomenclaturesandreid,nom_datemiseajour,nom_commentaire) values ('ref.tr_devenirlot_dev',127,'14/06/2016',NULL);


/* 
PATHOLOGIE
*/
/*
Code	Mnémonique	Libellé	Définition
00	00	Ni poux, ni traces de poux	Le poisson, généralement un salmonidé migrateur venu de la mer, n'héberge aucun pou de mer et ne présente aucune lésion visible consécutive à une colonisation par le pou de mer (qui est en fait un crustacé parasite des salmonidés migrateurs)
AA	AA	Altération de l'aspect	Le corps du poisson examiné présente des altérations morphologiques caractérisées, pouvant éventuellement être détaillées ou non.
AC	AC	Altération de la couleur	La pigmentation présente des altérations entrainant une coloration anormale de tout ou partie du corps du poisson.
AD	AD	Difforme	Le corps du poisson présente des déformations anormales se traduisant par des acures ou des bosselures,extériorisation possible d'une atteinte interne, virale par exemple (ex : nécrose pancréatique infectieuse de la truite arc-en-ciel)
AG	AG	Grosseur, excroissance	Le corps du poisson présente une ou des déformations marquées constituant des excroissances ou des tumeurs
AH	AH	Aspect hérissé (écailles)	Les écailles du poisson ont tendance à se relever perpendiculairement au corps, à la suite généralement d'une infection au niveau des téguments
AM	AM	Maigreur	Le corps du poisson présente une minceur marquée par rapport à la normalité
AO	AO	Absence d'organe	L'altération morphologique observée sur le poisson se traduit par l'absence d'un organe (nageoire, opercule, oeil, machoire)
BG	BG	Bulle de gaz	Présence de bulle de gaz pouvant être observées sous la peau, au bord des nageoires, au niveau des yeux, des branchies ou de la bouche.
CA	CA	Coloration anormale	L'altération de la pigmentation entraîne la différenciation de zones diversement colorées, avec en particulier des zones sombres.
9CB	CB	Branchiures (Argulus...)	Présence visible, à la surface du corps ou des branchies du poisson, de crustacés branchiures à un stade donné de leur cycle de développement.
CC	CC	Copépodes (Ergasilus, Lerna,...)	Présence visible, à la surface du corps ou des branchies du poisson, de crustacés parasites, à un stade donné de leur cycle de développement.
CO	CO	Coloration opaque (oeil)	L'altération de la coloration se traduit par une opacification de l'un ou des deux yeux.
CR	CR	Crustacés	Présence visible, ?? la surface du corps ou des branchies du poisson, de crustacés parasites, à un stade donné de leur cycle de développement.
CS	CS	Coloration sombre	L'altération de la coloration du corps du poisson se traduit par un assombrissement de tout ou partie de celui-ci (noircissement).
CT	CT	Coloration terne (pâle)	L'altération de la coloration du corps du poisson se traduit par une absence de reflets lui conférant un aspect terne, pâle, voire une décoloration.
HA	HA	Acanthocéphales	Présence visible, à la surface du corps ou des branchies du poisson, d'acanthocéphales à un stade donné de leur cycle de développement.
HC	HC	Cestodes (Ligula, Bothriocephalus, ...)	Présence visible, à la surface du corps ou des branchies du poisson, de cestodes à un stade donné de leur cycle de développement.
HE	HE	Hémorragie	Ecoulement de sang pouvant être observé à la surface du corps ou au niveau des branchies.
HH	HH	Hirudinés (Piscicola)	Présence visible sur le poisson de sangsue(s)
HN	HN	Nématodes (Philometra, Philimena...)	Présence visible, à la surface du corps ou des branchies du poisson, de nématodes à un stade donné de leur cycle de développement.
HS	HS	Stade pre-mortem	Le poisson présente un état pathologique tel qu'il n'est plus capable de se mouvoir normalement dans son milieu et qu'il est voué à une mort certaine à brève échéance.
HT	HT	Trématodes (Bucephalus, ...)	Présence visible, à la surface du corps ou des branchies du poisson, de trématodes parasites à un stade donné de leur cycle de développement.
IS	IS	Individu sain	Après examen du poisson, aucun signe externe, caractéristique d'une pathologie quelconque, n'est décelable à l'oeil nu
LD	LD	Lésions diverses	Les téguments présentent une altération quelconque de leur intégrité.
NE	NE	Nécrose	Lésion(s) observée(s) à la surface du corps avec mortification des tissus.
NN	NN	Pathologie non renseigné	L'aspect pathologique du poisson n'a fait l'objet d'aucun examen et aucune information n'est fournie à ce sujet
PA	PA	Parasitisme	Présence visible, à la surface du corps ou des branchies du poisson, d'organismes parasites vivant à ses dépens.
PB	PB	Points blancs	Présence de points blancs consécutive à la prolif??ration de certains protozoaires parasites comme Ichtyopthtirius (ne pas confondre avec les boutons de noces, formations kératinisées apparaissant lors de la période de reproduction)
PC	PC	Champignons (mousse, ...)	Présence d'un développement à la surface du corps, d'un mycélium formant une sorte de plaque rappelant l'aspect de la mousse et appartenant à une espèce de champignon colonisant les tissus du poisson.
PL	PL	Plaie - blessure	Présence d'une ou plusieurs lésions à la surface du tégument généralement due à un prédateur (poisson, oiseau,.)
PN	PN	Points noirs	Présence de tâches noires bien individualisées sur la surface du tégument du poisson
SM	SM	Sécrétion de mucus importante	Présence anormale de mucus sur le corps ou au niveau de la chambre branchiale.
UH	UH	Ulcère hémorragique	Ecoulement de sang observé au niveau d'une zone d'altération des tissus.
VL	VL	Vésicule contenant un liquide	Présence d'un oedème constituant une excroissance.
ZO	ZO	Etat pathologie multiforme	Le poisson présente plus de deux caractéristiques pathologiques différentes
01	01	Traces de poux	Le poisson ne porte aucun pou mais présente des lésions cutanées consécutives à une colonisation par le pou de mer. La présence du poisson en eau douce a été suffisante pour obliger les poux à quitter leur hôte.
11	11	< 10 poux ; sans flagelles	Le poisson présente moins de 10 poux de mer, mais ces derniers, en raison d'un présence prolongée en eau douce, ont déjà perdu leur flagelle
21	21	< 10 poux ; avec flagelles	Le poisson présente moins de 10 poux de mer, mais ces derniers, compte-tenu de l'arrivée récente de leur hôte en eau douce, n'ont pas encore perdu leur flagelle
31	31	> 10 poux, sans flagelles	Le poisson présente plus de 10 poux de mer, mais ces derniers, en raison d'un présence prolongée en eau douce, ont déjà perdu leur flagelle
42	42	> 10 poux, avec flagelles	Le poisson présente plus de 10 poux de mer, mais ces derniers, compte-tenu de l'arrivée récente de leur hôte en eau douce, n'ont pas encore perdu leur flagelle
*/
-- select * from ref.tr_pathologie_pat order by pat_code


/*
en trop
=> 51
=> UH (en trop dans le sandre) 

Autres parasites que les 4 précédents (7) N/S PX
Parasites (une des 5 catégories au-dessus) (7) N/S PT
=> US
=> ER Erosion
=> EX Exophtalmie
=>OO

NC NC Signe pathologique d'origine
inconnue
Signe pathologique d'origine inconnue
PT PT Parasites (PB ou PC ou CR
ou HH ou PX)
PX PX Autres parasites (autre que
CR, HH, PB, PC)
*/

insert into ref.tr_pathologie_pat (pat_code,pat_libelle,pat_definition) values ('PX','Autres parasites (que CR, HH, PB, PC)', '');
insert into ref.tr_pathologie_pat (pat_code,pat_libelle,pat_definition) values  ('PT','Parasites (PB ou PC ou CR ou HH ou PX)', '');
insert into ref.tr_pathologie_pat (pat_code,pat_libelle,pat_definition) values  ('OO','Absence de lésions ou de parasites', '');
insert into ref.tr_pathologie_pat (pat_code,pat_libelle,pat_definition) values  ('NC','Signe pathologique d`origine inconnue', '');


-- ci dessous comme il y a des clé étrangère, obligé de lancer sur tous les schémas
select ref.updatesql('{"iav"}',
'ALTER TABLE tj_pathologieconstatee_pco DROP CONSTRAINT IF EXISTS c_fk_pco_pat_code ');--12
select ref.updatesql('{"iav"}',
'update tj_pathologieconstatee_pco set pco_pat_code=''UH'' where pco_pat_code=''UL''');--12
update ref.tr_pathologie_pat set pat_code='UH' where pat_code='UL';
select ref.updatesql('{"iav"}',
'ALTER TABLE tj_pathologieconstatee_pco ADD CONSTRAINT c_fk_pco_pat_code FOREIGN KEY (pco_pat_code)
      REFERENCES ref.tr_pathologie_pat (pat_code) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION');


insert into ref.ts_nomenclature_nom(nom_nomtable,nom_nomenclaturesandreid,nom_datemiseajour,nom_commentaire) values ('ref.tr_pathologie_pat',129,'11/06/2016','La pathologie 51 pas dans le Sandre a du sens, je la garde');




/*
'Code d'importance des pathologies, Beaulaton et Penil, 2009
Je n'ai pas trouvé pour l'instant ces codes dans le dictionnaire processus d'acquisition des données biologiques 4.2
Le guide pour l'instant est celui de Beaulaton et Penil....
Nombre (N) Code
N= 0 0
N=< 3 1
N= 4 à 6 2
N= 7 à 10 3
N >10 4
Surface atteinte (S) Code
S = 0% 0
S < 5% 1
S = 5 à 10% 2
S = 10 à 20% 3
S > 20% 4
Degré d’altération (D) Code
Nul 0
Faible 1
Moyen 2
Fort 3
Très fort 4
*/


drop table if exists ref.tr_importancepatho_imp CASCADE;
 create table ref.tr_importancepatho_imp 
 (
 imp_code integer primary key,
 imp_libelle character varying(100),
 imp_definition text
 );
 insert into ref.tr_importancepatho_imp values (0,'Nul (0)','Nombre (N) 0, Surface atteinte (S) 0%, Degré d''altération (D) nul.');
 insert into ref.tr_importancepatho_imp values (1,'Faible (1)','Nombre (N) <3, Surface atteinte (S) <5%, Degré d''altération (D) faible.');
 insert into ref.tr_importancepatho_imp values (2,'Moyen (2)','Nombre (N) 4 à 6, Surface atteinte (S) 5 à 10 %, Degré d''altération (D) moyen.');
 insert into ref.tr_importancepatho_imp values (3,'Fort (3)','Nombre (N) 7 à 10, Surface atteinte (S) 10 à 20 %, Degré d''altération (D) important.');
 insert into ref.tr_importancepatho_imp values (4,'Très fort (4)','Nombre (N) >10, Surface atteinte (S) >20%, Degré d''altération (D) très important.');    
GRANT ALL ON TABLE ref.tr_importancepatho_imp TO postgres;
GRANT SELECT ON TABLE ref.tr_importancepatho_imp TO iav;
GRANT SELECT ON TABLE ref.tr_importancepatho_imp TO invite;
GRANT SELECT ON TABLE ref.tr_importancepatho_imp TO inra;
GRANT SELECT ON TABLE ref.tr_importancepatho_imp TO logrami;
GRANT SELECT ON TABLE ref.tr_importancepatho_imp TO mrm;
GRANT SELECT ON TABLE ref.tr_importancepatho_imp TO migado;
GRANT SELECT ON TABLE ref.tr_importancepatho_imp TO saumonrhin;
GRANT SELECT ON TABLE ref.tr_importancepatho_imp TO migradour;
GRANT SELECT ON TABLE ref.tr_importancepatho_imp TO charente;
GRANT SELECT ON TABLE ref.tr_importancepatho_imp TO fd80;
GRANT SELECT ON TABLE ref.tr_importancepatho_imp TO smatah;
GRANT SELECT ON TABLE ref.tr_importancepatho_imp TO azti;
GRANT SELECT ON TABLE ref.tr_importancepatho_imp TO bgm;
GRANT SELECT ON TABLE ref.tr_importancepatho_imp TO smatah;

 select ref.updatesql('{"iav"}',
 'alter table tj_pathologieconstatee_pco add column pco_imp_code integer;
 alter table tj_pathologieconstatee_pco ADD CONSTRAINT c_fk_pco_imp_code FOREIGN KEY (pco_imp_code)
      REFERENCES ref.tr_importancepatho_imp (imp_code) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
      ');

      
 -- select * from ref.tr_importancepatho_imp

/*
LOCALISATION 
*/

--select * from ref.tr_localisationanatomique_loc order by loc_code;

-- en trop (rajouter au sandre)
/*
ABD
ADP
ANA
CAB
CAH
... tous 
*/
 -- localisations anatomiques
insert into ref.tr_localisationanatomique_loc values ('X','Orifice anal');
update ref.tr_localisationanatomique_loc set loc_libelle='Nageoire principale' where loc_code='N';
insert into ref.tr_localisationanatomique_loc values ('P','Nageoire pectorale'); 
insert into ref.tr_localisationanatomique_loc values ('V','Colonne vertébrale'); 
insert into ref.ts_nomenclature_nom(nom_nomtable,nom_nomenclaturesandreid,nom_datemiseajour,nom_commentaire) values ('ref.tr_localisationanatomique_loc',NULL,'11/06/2016','Les localisations correspondent à la fois aux loc anatomiques et loc de marquage. Vérifier compatibilité');


/*
Information dans les schémas concernant la mise à jour
*/

select ref.updatesql('{"iav"}',
'insert into ts_maintenance_main
(
  main_ticket,
  main_description
  ) values
  (81,''Mise à jour vers la version 0.4 alpha, mise à jour des référentiels du SANDRE, script ticjet81_mise_en_conformite_sandre, révision 98'')'
);
 --taxons retrouver code marion
