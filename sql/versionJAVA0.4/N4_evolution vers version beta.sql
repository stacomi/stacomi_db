﻿/*

Script de modification N4 pour passer à la version béta

*/
"bgm","charente","charente_bis","iav","logrami","migado","migradour","ngm","pmp","nat","smatah"

set search_path to iav;
SET client_min_messages TO WARNING;


-- ci dessous obligé de lancer dans chaque schéma indépendemment !!!

CREATE OR REPLACE FUNCTION fct_txe_date()
  RETURNS trigger AS
$BODY$   

 	DECLARE nbChevauchements INTEGER ;

 	BEGIN
 	 	-- verification des non-chevauchements pour les taux
 	 	SELECT COUNT(*) INTO nbChevauchements
 	 	FROM   iav.tj_tauxechappement_txe
 	 	WHERE  txe_sta_code = NEW.txe_sta_code
 	 	       AND txe_tax_code = NEW.txe_tax_code
 	 	       AND txe_std_code = NEW.txe_std_code
 	 	       AND (txe_date_debut, txe_date_fin) OVERLAPS (NEW.txe_date_debut, NEW.txe_date_fin)
 	 	;

		-- Comme le trigger est declenche sur AFTER et non pas sur BEFORE, il faut (nbChevauchements > 1) et non pas >0, car l enregistrement a deja ete ajoute, donc il se chevauche avec lui meme, ce qui est normal !
 	 	IF (nbChevauchements > 1) THEN
 	 	 	RAISE EXCEPTION 'Les taux d echappement ne peuvent se chevaucher.'  ;
 	 	END IF  ;

		RETURN NEW ;

 	END  ;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION iav.fct_txe_date()
  OWNER TO postgres;



Insert into ref.tr_taxon_tax (tax_code,tax_nom_latin,tax_nom_commun,tax_ntx_code,tax_rang) VALUES ('9814','cyprinus carpio','carpe koï','15','100');
Insert into ref.tr_taxon_tax (tax_code,tax_nom_latin,tax_nom_commun,tax_ntx_code,tax_rang) VALUES ('2067','cabilis taenia','loche de rivière','15','100');
Insert into ref.tr_taxon_tax (tax_code,tax_nom_latin,tax_nom_commun,tax_ntx_code,tax_rang) VALUES ('2127','pimephales promelas','tête de boule','15','100');
-- ticket #200 (marion)
ALTER TABLE ref.tr_taxon_tax DROP CONSTRAINT c_uq_tax_nom_latin;
INSERT INTO ref.tr_taxon_tax VALUES (2111,'Cyprinus carpio','Carpe cuir','15',NULL,40);
INSERT INTO ref.tr_localisationanatomique_loc(loc_code,loc_libelle) VALUES ('PV','Pelvienne');

/*
Ajout des codes cohorte pour spécifier la cohorte de reproduction
*/
INSERT INTO ref.tg_parametre_par
(par_code,par_nom,par_unite,par_nature,par_definition) values ('COHO','Cohorte de reproduction','Sans objet','BIOLOGIQUE','Indication de la cohorte de reproduction dont dépend le poisson (expertise dépendant de la distance à l''estuaire de la station, de l''état du poisson et de sa date d''arrivée)');


INSERT INTO ref.tr_parametrequalitatif_qal
(qal_par_code,qal_valeurs_possibles) values ('COHO','cohorte année reproduction n / cohorte année reproduction n+1 / cohorte année reproduction n-1');


INSERT INTO ref.tr_valeurparametrequalitatif_val
(  val_identifiant,val_qal_code ,  val_rang,  val_libelle) values (62,'COHO',1,'Cohorte de reproduction de l''année n');
INSERT INTO ref.tr_valeurparametrequalitatif_val
(  val_identifiant,val_qal_code ,  val_rang,  val_libelle) values (63,'COHO',2,'Cohorte de reproduction de l''année n+1');
INSERT INTO ref.tr_valeurparametrequalitatif_val
(  val_identifiant,val_qal_code ,  val_rang,  val_libelle) values (64,'COHO',3,'Cohorte de reproduction de l''année n-1');


/*
Mise en conformité avec le SANDRE pour le sexe du poisson
*/
INSERT INTO ref.tr_valeurparametrequalitatif_val
(  val_identifiant,val_qal_code ,  val_rang,  val_libelle) values (65,'1783',5,'Recherché mais non indentifié');
UPDATE ref.tr_valeurparametrequalitatif_val set val_libelle='Inconnu' where val_libelle='Indéterminé';



/*
TODO
A geler ALS SAB CAS GSB FIN TRFV

Rajouter Indeterminé dans les codes.

Changer C tout le corps pour C corps (à la différence de ensemble du corps)
N nageoire (sans précision)
*/
UPDATE ref.tr_stadedeveloppement_std set std_statut= 'gelé' where std_code='ALS';
UPDATE ref.tr_stadedeveloppement_std set std_statut= 'gelé' where std_code='SAB';
UPDATE ref.tr_stadedeveloppement_std set std_statut= 'gelé' where std_code='MAD';
UPDATE ref.tr_stadedeveloppement_std set std_statut= 'gelé' where std_code='GST';
UPDATE ref.tr_stadedeveloppement_std set std_statut= 'gelé' where std_code='FIN';
UPDATE ref.tr_stadedeveloppement_std set std_statut= 'gelé' where std_code='TRFV';
/*
tiket 184 paramètres
*/


/* 
Construction de la requête de test

SELECT par_code, par_nom from ref.tg_parametre_par 
EXCEPT
		(SELECT 
		par_code, par_nom from ref.tg_parametre_par 
		INNER  JOIN   ref.tr_parametrequalitatif_qal 
		on  tg_parametre_par.par_code = tr_parametrequalitatif_qal.qal_par_code
	UNION 
		SELECT 
		par_code, par_nom from ref.tg_parametre_par 	
		INNER JOIN   ref.tr_parametrequantitatif_qan 
			on   tg_parametre_par.par_code = tr_parametrequantitatif_qan.qan_par_code
		);
*/
-- Les paramètres manquants sont tous quantitatifs
insert into ref.tr_parametrequantitatif_qan 
SELECT par_code from ref.tg_parametre_par 
EXCEPT
		(SELECT 
		par_code from ref.tg_parametre_par 
		INNER  JOIN   ref.tr_parametrequalitatif_qal 
		on  tg_parametre_par.par_code = tr_parametrequalitatif_qal.qal_par_code
	UNION 
		SELECT 
		par_code from ref.tg_parametre_par 	
		INNER JOIN   ref.tr_parametrequantitatif_qan 
			on   tg_parametre_par.par_code = tr_parametrequantitatif_qan.qan_par_code
		); --6



GRANT SELECT ON TABLE ref.ts_organisme_org TO iav;
GRANT SELECT ON TABLE ref.ts_organisme_org TO bgm;
GRANT SELECT ON TABLE ref.ts_organisme_org TO invite;
GRANT SELECT ON TABLE ref.ts_organisme_org TO inra;
GRANT SELECT ON TABLE ref.ts_organisme_org TO logrami;
GRANT SELECT ON TABLE ref.ts_organisme_org TO mrm;
GRANT SELECT ON TABLE ref.ts_organisme_org TO ngm;
GRANT SELECT ON TABLE ref.ts_organisme_org TO migado;
GRANT SELECT ON TABLE ref.ts_organisme_org TO saumonrhin;
GRANT SELECT ON TABLE ref.ts_organisme_org TO migradour;
GRANT SELECT ON TABLE ref.ts_organisme_org TO charente;
GRANT SELECT ON TABLE ref.ts_organisme_org TO fd80;
GRANT SELECT ON TABLE ref.ts_organisme_org TO smatah;
GRANT SELECT ON TABLE ref.ts_organisme_org TO azti;
GRANT SELECT ON TABLE ref.ts_organisme_org TO bgm;

/*
On revoit la contrainte sur la table tj_caracteristiquelot_car pour mettre une double contrainte
*/

alter table ref.tr_valeurparametrequalitatif_val add CONSTRAINT c_uk_val_identifiant UNIQUE (val_identifiant,val_qal_code);
select ref.updatesql('{"bgm","charente","iav","logrami","migado","migradour","pmp","nat","smatah"}',
'
alter table tj_caracteristiquelot_car drop CONSTRAINT c_fk_car_val_identifiant ;
alter table tj_caracteristiquelot_car add CONSTRAINT c_fk_car_val_par_identifiant FOREIGN KEY (car_val_identifiant, car_par_code) 
	REFERENCES ref.tr_valeurparametrequalitatif_val(val_identifiant, val_qal_code) ON UPDATE CASCADE
');



-- ticket 214

select ref.updatesql('{"iav"}',
'insert into ts_maintenance_main
(
  main_ticket,
  main_description
  ) values
  (214,''Mise a jour vers la version alpha'')'
);