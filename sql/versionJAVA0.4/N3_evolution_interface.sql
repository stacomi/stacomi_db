﻿/* 
SCRIPT DE CREATION DES TABLES SYSTEME DE LA VERSION JAVA0.4
LANCER EN DERNIER (N3)
A LANCER SUR CHAQUE SCHEMA
*/



set search_path to bgm,ref,public;
-- attention remplacer bgm dans les triggers
/*
Modifications nécessaire pour la base
Les masques sont pour un organisme, on va pas les compiler au niveau national
Ils n'ont pas besoin d'avoir un org_code qui complique les choses
Mais pour aller chercher les stations de mesure on a besoin d'une clé étrangère
Je rajoute une contrainte d'unicité, dans la pratique ça ne change rien



31/12/2015
Problèmes de droits sur les séquences pour leur mise à jour dans JAVA
j'ai dû remettre les doits aux rôles qui les avaient perdu (il y avait une expiration dans la base, vérifier ça en maintenance)
*/






/* 
ci dessous ne suffit pas, et le fait que postgres ne supporte pas l'usage de la séquence (via all) par exemple est inquiétant
*/

GRANT SELECT, UPDATE, INSERT, DELETE ON ALL TABLES in schema bgm to bgm;
GRANT SELECT ON ALL TABLES in schema ref to bgm;
grant select,usage,update on all sequences in schema bgm to bgm;

grant usage on schema bgm to bgm;

/*
Il faut modifier tous les propriétaires des table à séquences
*/


alter table bgm.t_operation_ope owner to bgm;
alter table bgm.t_lot_lot owner to bgm;
alter table bgm.t_ouvrage_ouv owner to bgm;
alter table bgm.tg_dispositif_dis owner to bgm;
alter table bgm.tj_stationmesure_stm owner to bgm;

ALTER SEQUENCE bgm.t_lot_lot_lot_identifiant_seq owner to bgm;
ALTER SEQUENCE bgm.t_operation_ope_ope_identifiant_seq owner to bgm;
ALTER SEQUENCE bgm.t_ouvrage_ouv_ouv_identifiant_seq owner to bgm;
ALTER SEQUENCE bgm.tg_dispositif_dis_dis_identifiant_seq owner to bgm;
ALTER SEQUENCE bgm.tj_stationmesure_stm_stm_identifiant_seq owner to bgm;

/*

Pour le marais poitevin
Insert into ref.tr_stadedeveloppement_std (std_code,std_libelle)
SELECT 'PANG','Petite anguille de moins de 150mm';
Insert into ref.tr_stadedeveloppement_std (std_code,std_libelle)
SELECT 'GANG','Grande anguille de plus de 150mm';

-- suppression de deux lignes pour un paramètre qualitatif erroné
DELETE FROM ref.tr_valeurparametrequalitatif_val where val_identifiant in (60,61)

Insert into ref.tg_parametre_par(par_code,par_nom,par_unite,par_nature,par_definition)
SELECT 'B003','Classe de taille (anguilles Marais Poitevin)','Sans objet','BIOLOGIQUE','Classe pour la séparation des petites et grandes anguilles lors du piégeage';
Insert into ref.tr_parametrequalitatif_qal(qal_par_code,qal_valeurs_possibles)
SELECT 'B003','<150;>150';
Insert into ref.tr_valeurparametrequalitatif_val(val_identifiant,val_qal_code,val_rang,val_libelle)
SELECT 60,'B003',1,'<150';
Insert into ref.tr_valeurparametrequalitatif_val(val_identifiant,val_qal_code,val_rang,val_libelle)
SELECT 61,'B003',2,'>150';
select ref.updatesql('{"azti","bgm","charente","fd80","bgm","inra","logrami","migado","bgm","saumonrhin","smatah"}',
'INSERT INTO ts_maintenance_main(main_ticket,main_description) values (124,''Version 0.4,Ajout dun paramètre qualitatif classe de taille pour les anguilles'')');
*/

/*
La table masque contient l'ensemble des masques de l'interface
Affichage par defaut du masque operation =ope_defaut
Affichage par defaut du masque lot = lot_defaut
Je ne met pas org_code dans ces tables => je n'aurai pas besoin de les rassembler...
*/
drop table if exists ts_masque_mas CASCADE;
create table ts_masque_mas (
mas_id serial,
mas_code character varying(15) UNIQUE NOT NULL,   
mas_description text,
mas_raccourci text,
mas_type character varying(3), --type du masque soit operation soit lot
CONSTRAINT c_pk_mas_id PRIMARY KEY (mas_id),
CONSTRAINT c_ck_mas_type CHECK (mas_type='lot' or mas_type='ope'));
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE ts_masque_mas TO bgm;
GRANT SELECT ON TABLE ts_masque_mas TO invite;
insert into ts_masque_mas(mas_code,mas_description,mas_raccourci,mas_type) VALUES ('ope_defaut','Masque par defaut pour les operations',NULL,'ope');
insert into ts_masque_mas(mas_code,mas_description,mas_raccourci,mas_type) VALUES ('lot_defaut','Masque par defaut pour les lots',NULL,'lot');
ALTER TABLE ts_masque_mas
  OWNER TO bgm;
/*
La table masqueoope_mao contient l'ensemble des masques relatifs aux operations
	//  mao_mas_id character varying(15) NOT NULL,
	//  mao_affichage boolean[],
	//			1 Station (defaut 0)
	//			2 Ouvrage (defaut 0)
	//			3 DF (defaut 0)
	//			4 DC (defaut 0)
	//			5 heuredebut (defaut 1)
	//			6 heurefin (defaut 1)
	//			7 operateur (defaut 0)
	//			8 organisme (defaut 0)
	//			9 heurerearmement  (defaut 0)
	//-----------------------------------------------------
	// les élements logiques
	//--------------------------------------------------
	//			10 is_debut_auto (defaut 0) (oui non) 
	//			11 is_fin_auto  (defaut 0)(valeur oui non dans le combo)
	//			12 is_rearmement   (defaut 0)(La date de réarmement sert à renseigner le début de l'opération suivante (si debutauto) et le fonctionnement du dc
	//			13 is_rearmementDF  (defaut 0) la valeur sert aussi à renseigner le DF par seulement le DC
	//-----------------------------------------------------
	// les élements qui n'ont pas de valeur pas défaut
	//--------------------------------------------------
	//			14 datedebut (defaut 1)
	//			15 datefin  (defaut 1)
	//			16 commentaires	 (defaut 0)	
	//			
	
	//  mao_valeurdefaut String[]
	//			1 Station
	//			2 Ouvrage
	//			3 DF
	//			4 DC
	//			5 heuredebut
	//			6 heurefin
	//			7 operateur
	//			8 organisme
	//			9 heurerearmement 	

Il peut y avoir dix valeurs possibles pour l'affichage, vrai ou faux, stocké sous forme de vecteur
Il peut y avoir 17 valeurs par défaut pour le champ stocké sous forme de vecteur

*/

/*
Trigger pour empecher la suppression du masque par défaut
Sinon JAVA va planter car utilise Preference pour stocker les préferences de masque utilisateur
Avec pour valeur par défaut ope_defaut
SELECT * from ts_masque_mas WHERE mas_code='ope_defaut'  ;
DELETE FROM ts_masque_mas WHERE mas_code='ope_defaut'  ; 
*/

CREATE OR REPLACE FUNCTION do_not_delete_default() RETURNS trigger AS 
$do_not_delete_default$   
DECLARE
_mas_code character varying(15);
    BEGIN      
        IF OLD.mas_code = 'ope_defaut'  THEN
            RAISE EXCEPTION 'Vous ne pouvez pas supprimer le masque par défaut';
        END IF;
          IF OLD.mas_code = 'lot_defaut'  THEN
            RAISE EXCEPTION 'Vous ne pouvez pas supprimer le masque par défaut';
        END IF;
RETURN OLD;
    END;
$do_not_delete_default$   
LANGUAGE 'plpgsql';
DROP TRIGGER IF EXISTS check_do_not_delete_defaut ON ts_masque_mas;
CREATE TRIGGER check_do_not_delete_defaut
BEFORE DELETE ON ts_masque_mas
FOR EACH ROW EXECUTE PROCEDURE do_not_delete_default();


CREATE OR REPLACE FUNCTION do_not_update_default() RETURNS trigger AS 
$do_not_update_default$   
DECLARE
_mas_code character varying(15);
    BEGIN      
     IF OLD.mas_code = 'ope_defaut'  THEN
            RAISE EXCEPTION 'Vous ne pouvez pas modifier le masque par défaut';
        END IF;
          IF OLD.mas_code = 'lot_defaut'  THEN
            RAISE EXCEPTION 'Vous ne pouvez pas modifier le masque par défaut';
        END IF;
RETURN NEW;
    END;
$do_not_update_default$   
LANGUAGE 'plpgsql';
DROP TRIGGER IF EXISTS check_do_not_update_defaut ON ts_masque_mas;
CREATE TRIGGER check_do_not_update_defaut
BEFORE UPDATE ON ts_masque_mas
FOR EACH ROW EXECUTE PROCEDURE do_not_update_default();




drop table if exists ts_masqueope_mao CASCADE;
create table ts_masqueope_mao(
mao_mas_id integer,
mao_affichage boolean[16], -- edition possible des champs (valeur stockee sous forme de vecteur)
mao_valeurdefaut text[9], -- Valeurs par defaut stockees sous forme de vecteur
CONSTRAINT c_pk_mao_mas_id PRIMARY KEY (mao_mas_id),
CONSTRAINT c_ck_length_mao_affichage CHECK (array_length(mao_affichage,1)=16), -- CONSTRAINTE dix valeurs affichees
CONSTRAINT c_ck_length_mao_valeurdefaut CHECK (array_length(mao_valeurdefaut,1)=9),
CONSTRAINT c_fk_mao_mas_id FOREIGN KEY (mao_mas_id) REFERENCES ts_masque_mas(mas_id) ON DELETE CASCADE
); 
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE ts_masqueope_mao TO bgm;
GRANT SELECT ON TABLE ts_masqueope_mao TO invite;
insert into ts_masqueope_mao VALUES (
(select mas_id from ts_masque_mas where mas_code='ope_defaut'),
'{0,0,0,0,1,1,0,0,0,0,0,0,0,1,1,0}',
'{NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL}');





/*
Trigger pour vérifier à l'insertion d'un nouveau masqueope que le type du masque est bien ope
//DELETE FROM ts_masque_mas WHERE mas_code='ope_defaut'  ; 
*/
DROP FUNCTION if exists masqueope() CASCADE;  
CREATE OR REPLACE FUNCTION masqueope()
  RETURNS trigger AS
$BODY$   
DECLARE test text;


 	BEGIN
 	 	-- Recuperation du type
 	 	SELECT mas_type into test
 	 	FROM   bgm.ts_masque_mas
 	 	WHERE  mas_id = NEW.mao_mas_id
 	 	;

 	 	-- verification 
 	 	IF (test!='ope') THEN
 	 	 	RAISE EXCEPTION 'Attention le masque n'' est pas un masque operation, changez le type de masque'  ;
 	 	END IF  ; 	 	

		RETURN NEW ;
 	END  ;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION masqueope()
  OWNER TO postgres;
  

CREATE TRIGGER trg_masqueope
  BEFORE INSERT OR UPDATE
  ON ts_masqueope_mao
  FOR EACH ROW
  EXECUTE PROCEDURE masqueope();




/*
test ...
delete from ts_masque where mas_code; => erreur
delete from ts_masqueope_mao where mao_mas_id=10;
delete from ts_masque_mas where mas_id=1; => erreur contrainte de clé étrangère
*/




/*
La table ts_masqueconditionsenvironnementales_mae contient l'ensemble des masques relatifs aux conditions environnementales
en lien 1:n avec la table ts_masqueope_mao
*/



drop table if exists ts_masqueconditionsenvironnementales_mae;
create table ts_masqueconditionsenvironnementales_mae(
mae_mao_id integer,-- le code du masque
mae_stm_identifiant integer ,
mae_affichage boolean, -- affichage des champs de chaque condition environnementale
mae_valeurdefaut text,
CONSTRAINT c_pk_mae PRIMARY KEY (mae_mao_id,mae_stm_identifiant),-- il ne peut y avoir qu'une condition environnementale par masque (pas deux fois la même)
CONSTRAINT c_fk_mae_mao_id FOREIGN KEY (mae_mao_id) REFERENCES ts_masqueope_mao(mao_mas_id) ON DELETE CASCADE, -- les données correspondent à une station de mesure (rattachée à la station)
CONSTRAINT c_fk_mae_stm_identifiant FOREIGN KEY (mae_stm_identifiant)
      REFERENCES tj_stationmesure_stm (stm_identifiant)
); 
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE ts_masqueconditionsenvironnementales_mae TO bgm;
GRANT SELECT ON TABLE ts_masqueconditionsenvironnementales_mae TO invite;



/*
La table ts_masquelot_mal contient l'ensemble des masques relatifs aux lots
*/
/* Affichage
0 taxon
1 stade
2 quantite
3 type-quantite
4 methode_obtention
5 devenir
6 commentaires
7 pathologie
8 localisation
9 code prélèvement
10 opérateur prélèvement
11 type_prélèvement
12 localisation prélèvement
13 commentaire prélèvement
14 operation_marquage
15 localisation marquage
16 nature marquage
17 action marquage
18 commentaire marquage
19 effectif
20 importance pathologie
(longueur 21 commence à zéro)

Valeurs défaut
0 quantite
1 type quantite
2 méthode obtention
3 devenir
4 code prélèvement
5 opérateur prélèvement
6 type prélèvement
7 localisation prélèvement
8 operation marquage
9 localisation
10 nature marque
11 action marquage
12 effectif
13 importance patho 
(longueur 14 commence à 0)
*/

drop table if exists ts_masquelot_mal CASCADE;
create table ts_masquelot_mal(
mal_mas_id integer,
mal_affichage boolean[], -- affichage des champs stocke sous forme de vecteur
mal_valeurdefaut text[], -- Valeurs par defaut stockee sous forme de vecteur
CONSTRAINT c_pk_mal_mas_id PRIMARY KEY (mal_mas_id),
CONSTRAINT c_fk_mal_mas_id FOREIGN KEY (mal_mas_id) REFERENCES ts_masque_mas(mas_id) ON DELETE CASCADE,
CONSTRAINT c_ck_length_mal_affichage CHECK (array_length(mal_affichage,1)=21), 
CONSTRAINT c_ck_length_mal_valeurdefaut CHECK (array_length(mal_valeurdefaut,1)=14));
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE ts_masquelot_mal TO bgm;
GRANT SELECT ON TABLE ts_masquelot_mal To invite;
insert into ts_masquelot_mal VALUES (
(select mas_id from ts_masque_mas where mas_code='lot_defaut'),
'{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}',
'{NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL}');
/*
La table ts_caracteristique_mac contient les elements relatifs aux caracteristiques
Elles sont au nombre de 3, 
si c'est une caracteristique quantitative, l'affichage correspond a  "valeur" "precision" "commentaires"
si c'est une caracteristique qualitative, l'affichage correspond a  "valeur" "valeur par defaut" "commentaires"
*/
drop table if exists ts_masquecaracteristiquelot_mac;
create table ts_masquecaracteristiquelot_mac(
mac_id serial,
mac_mal_id integer,
mac_par_code character varying(5) NOT NULL,
mac_affichagevaleur boolean, -- affichage des champs 
mac_affichageprecision boolean,
mac_affichagemethodeobtention boolean,
mac_affichagecommentaire boolean,
mac_valeurquantitatifdefaut numeric, -- Valeurs par defaut
mac_valeurqualitatifdefaut integer, -- code de la valeur dans la table ref.tr_valeurparametrequalitatif_val
mac_precisiondefaut numeric,
mac_methodeobtentiondefaut character varying(10),
mac_commentairedefaut text,

CONSTRAINT c_pk_mac PRIMARY KEY (mac_mal_id,mac_par_code),
CONSTRAINT c_fk_mal_mas_id FOREIGN KEY (mac_mal_id) REFERENCES ts_masquelot_mal(mal_mas_id) ON DELETE CASCADE,
CONSTRAINT C_fk_mac_valeurqualitatifdefaut FOREIGN KEY (mac_valeurqualitatifdefaut) REFERENCES ref.tr_valeurparametrequalitatif_val(val_identifiant)
);


GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE ts_masquecaracteristiquelot_mac TO bgm;
GRANT SELECT ON TABLE ts_masquecaracteristiquelot_mac TO invite;
ALTER TABLE ts_masquecaracteristiquelot_mac owner to bgm;

/*
La table ts_masqueaffichage_maa contient les valeurs de rang de l'affichage par defaut des tables
Lors de la creation d'un nouveau masque, il faudra prendre exemple sur la construction du masque par defaut
Insert into ts_masqueaffichage_maa(..) values..., select * from ts_masqueaffichage_maa where maa_mal_id=2
*/
drop table if exists ts_masqueordreaffichage_maa;
create table ts_masqueordreaffichage_maa(
maa_id serial PRIMARY KEY,
maa_mal_id integer,
maa_table character varying(40) NOT NULL, --tr_taxon_tax,tr_stadedeveloppement_std,tr_pathologie_pat,tr_localisationanatomique_loc
maa_valeur text NOT NULL,
maa_champdumasque character varying (30), --peut être null sinon précise le champ de destination, car peut être multiple (cas de localisation)
maa_rang integer, --ordre d'affichage de la table de reference. Si une valeur n'est pas referencee, le taxon n'est pas affiche dans le masque
CONSTRAINT c_uk_maa UNIQUE(maa_mal_id,maa_table,maa_valeur,maa_champdumasque),
CONSTRAINT c_fk_maa_mal_id FOREIGN KEY (maa_mal_id) REFERENCES ts_masquelot_mal(mal_mas_id) ON DELETE CASCADE
);

-- du fait de la séquence il faut un owner to bgm
ALTER TABLE ts_masqueordreaffichage_maa OWNER TO bgm;
GRANT SELECT ON TABLE ts_masqueordreaffichage_maa TO invite;

DROP   sequence if exists seq;
create temporary sequence seq start with 1;
insert into ts_masqueordreaffichage_maa (maa_mal_id,maa_table,maa_valeur,maa_champdumasque,maa_rang)
select (select mas_id from ts_masque_mas where mas_code='lot_defaut'),'tr_taxon_tax', tax_code,'duallist_taxon',tax_rang from ref.tr_taxon_tax;--81
insert into ts_masqueordreaffichage_maa (maa_mal_id,maa_table,maa_valeur,maa_champdumasque,maa_rang)
select (select mas_id from ts_masque_mas where mas_code='lot_defaut'),'tr_stadedeveloppement_std', std_code,'duallist_stade', nextval('seq') from ref.tr_stadedeveloppement_std;--27
alter sequence seq restart with 1;
insert into ts_masqueordreaffichage_maa (maa_mal_id,maa_table,maa_valeur,maa_champdumasque,maa_rang)
select (select mas_id from ts_masque_mas where mas_code='lot_defaut'),'tr_pathologie_pat', pat_code, 'duallist_pathologie',nextval('seq') from ref.tr_pathologie_pat order by pat_code;--45
alter sequence seq restart with 1;
insert into ts_masqueordreaffichage_maa (maa_mal_id,maa_table,maa_valeur,maa_champdumasque,maa_rang)
select (select mas_id from ts_masque_mas where mas_code='lot_defaut'),'tr_localisationanatomique_loc', loc_code, 'duallist_localisationpatho', nextval('seq') from ref.tr_localisationanatomique_loc order by loc_code;--45


-- Pour creer les lignes en fin de requête
/*
 cd F:\projets\stacomi\Evol_interface 
F:
pg_dump --username postgres --table=bgm.ts_masque_mas --data-only --column-inserts bd_contmig_nat> ts_masque_mas.sql

pg_dump --username postgres --table=bgm.ts_masqueordreaffichage_maa --data-only --column-inserts bd_contmig_nat> ts_masqueordreaffichage_maa.sql
pg_dump --username postgres --table=bgm.ts_masquecaracteristiquelot_mac --data-only --column-inserts bd_contmig_nat> ts_masquecaracteristiquelot_mac.sql

*/
insert into ts_masque_mas(mas_code,mas_description,mas_raccourci,mas_type) VALUES ('lot_test','Essai',NULL,'lot');
insert into ts_masque_mas(mas_code,mas_description,mas_raccourci,mas_type) VALUES ('ope_test','Essai',NULL,'ope');
insert into ts_masqueope_mao VALUES (
(select mas_id from ts_masque_mas where mas_code='ope_test'),
'{0,0,0,0,1,1,0,0,0,0,0,0,0,1,1,0}',
'{NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL}');
delete from ts_masque_mas where mas_code='ope_test' ;
delete from ts_masque_mas where mas_code='lot_test' ;


insert into ts_maintenance_main
(
  main_ticket,
  main_description
  ) values
  (147,'Mise à jour vers la version 0.4 alpha, creation des masques');


/*
pg_dump --username postgres --table=bgm.ts_masque_mas --data-only --column-inserts bd_contmig_nat> ts_masque_mas.sql
pg_dump --username postgres --table=bgm.ts_masquelot_mal --data-only --column-inserts bd_contmig_nat> ts_masquelot_mal.sql
pg_dump --username postgres --table=bgm.ts_masqueordreaffichage_maa --data-only --column-inserts bd_contmig_nat> ts_masqueordreaffichage_maa.sql
pg_dump --username postgres --table=bgm.ts_masquecaracteristiquelot_mac --data-only --column-inserts bd_contmig_nat> ts_masquecaracteristiquelot_mac.sql
texte à rechercher pour expressions regulières = \d+, 3
 */


 
set search_path to bgm;
DELETE FROM ts_masque_mas where mas_id >2;
DELETE FROM ts_masquecaracteristiquelot_mac;
DELETE FROM ts_masqueordreaffichage_maa;

INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (1, 2, 'tr_taxon_tax', '2085', 'duallist_taxon', 10);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (2, 2, 'tr_taxon_tax', '2099', 'duallist_taxon', 11);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 2, 'tr_taxon_tax', '2086', 'duallist_taxon', 12);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 2, 'tr_taxon_tax', '2031', 'duallist_taxon', 13);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 2, 'tr_taxon_tax', '3218', 'duallist_taxon', 14);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 2, 'tr_taxon_tax', '2032', 'duallist_taxon', 15);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 2, 'tr_taxon_tax', '2088', 'duallist_taxon', 16);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (8, 2, 'tr_taxon_tax', '2090', 'duallist_taxon', 17);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (9, 2, 'tr_taxon_tax', '2055', 'duallist_taxon', 18);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (10, 2, 'tr_taxon_tax', '2056', 'duallist_taxon', 19);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (11, 2, 'tr_taxon_tax', '2057', 'duallist_taxon', 20);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (12, 2, 'tr_taxon_tax', '2058', 'duallist_taxon', 21);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (13, 2, 'tr_taxon_tax', '2177', 'duallist_taxon', 22);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (14, 2, 'tr_taxon_tax', '2094', 'duallist_taxon', 25);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (15, 2, 'tr_taxon_tax', '2095', 'duallist_taxon', 26);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (16, 2, 'tr_taxon_tax', '2096', 'duallist_taxon', 27);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (17, 2, 'tr_taxon_tax', '2097', 'duallist_taxon', 28);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (18, 2, 'tr_taxon_tax', '2098', 'duallist_taxon', 29);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (19, 2, 'tr_taxon_tax', '2100', 'duallist_taxon', 30);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (20, 2, 'tr_taxon_tax', '2101', 'duallist_taxon', 31);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (21, 2, 'tr_taxon_tax', '2102', 'duallist_taxon', 32);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (22, 2, 'tr_taxon_tax', '2179', 'duallist_taxon', 33);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (23, 2, 'tr_taxon_tax', '2180', 'duallist_taxon', 34);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (24, 2, 'tr_taxon_tax', '2104', 'duallist_taxon', 35);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (25, 2, 'tr_taxon_tax', '2105', 'duallist_taxon', 36);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (26, 2, 'tr_taxon_tax', '2080', 'duallist_taxon', 37);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (27, 2, 'tr_taxon_tax', '2107', 'duallist_taxon', 38);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (28, 2, 'tr_taxon_tax', '2108', 'duallist_taxon', 39);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (29, 2, 'tr_taxon_tax', '2109', 'duallist_taxon', 40);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (30, 2, 'tr_taxon_tax', '2234', 'duallist_taxon', 41);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (31, 2, 'tr_taxon_tax', '2151', 'duallist_taxon', 42);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (32, 2, 'tr_taxon_tax', '2113', 'duallist_taxon', 43);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (33, 2, 'tr_taxon_tax', '2191', 'duallist_taxon', 44);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (34, 2, 'tr_taxon_tax', '2010', 'duallist_taxon', 45);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (35, 2, 'tr_taxon_tax', '2012', 'duallist_taxon', 47);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (36, 2, 'tr_taxon_tax', '2117', 'duallist_taxon', 49);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (37, 2, 'tr_taxon_tax', '2050', 'duallist_taxon', 48);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (38, 2, 'tr_taxon_tax', '2118', 'duallist_taxon', 50);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (39, 2, 'tr_taxon_tax', '2123', 'duallist_taxon', 51);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (40, 2, 'tr_taxon_tax', '2120', 'duallist_taxon', 52);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (41, 2, 'tr_taxon_tax', '2121', 'duallist_taxon', 53);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (42, 2, 'tr_taxon_tax', '2122', 'duallist_taxon', 54);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (43, 2, 'tr_taxon_tax', '2119', 'duallist_taxon', 55);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (44, 2, 'tr_taxon_tax', '2181', 'duallist_taxon', 56);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (45, 2, 'tr_taxon_tax', '2182', 'duallist_taxon', 57);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (46, 2, 'tr_taxon_tax', '2183', 'duallist_taxon', 58);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (47, 2, 'tr_taxon_tax', '3267', 'duallist_taxon', 59);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (48, 2, 'tr_taxon_tax', '2156', 'duallist_taxon', 60);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (49, 2, 'tr_taxon_tax', '2051', 'duallist_taxon', 61);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (50, 2, 'tr_taxon_tax', '2052', 'duallist_taxon', 62);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (51, 2, 'tr_taxon_tax', '2053', 'duallist_taxon', 63);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (52, 2, 'tr_taxon_tax', '2185', 'duallist_taxon', 64);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (53, 2, 'tr_taxon_tax', '2216', 'duallist_taxon', 65);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (54, 2, 'tr_taxon_tax', '2193', 'duallist_taxon', 66);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (55, 2, 'tr_taxon_tax', '2014', 'duallist_taxon', 67);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (56, 2, 'tr_taxon_tax', '2125', 'duallist_taxon', 68);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (57, 2, 'tr_taxon_tax', '2203', 'duallist_taxon', 69);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (58, 2, 'tr_taxon_tax', '2205', 'duallist_taxon', 70);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (59, 2, 'tr_taxon_tax', '2133', 'duallist_taxon', 71);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (60, 2, 'tr_taxon_tax', '2219', 'duallist_taxon', 72);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (61, 2, 'tr_taxon_tax', '2220', 'duallist_taxon', 73);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (62, 2, 'tr_taxon_tax', '2221', 'duallist_taxon', 74);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (63, 2, 'tr_taxon_tax', '2222', 'duallist_taxon', 75);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (64, 2, 'tr_taxon_tax', '2224', 'duallist_taxon', 76);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (65, 2, 'tr_taxon_tax', '2227', 'duallist_taxon', 77);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (66, 2, 'tr_taxon_tax', '2195', 'duallist_taxon', 78);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (67, 2, 'tr_taxon_tax', '2135', 'duallist_taxon', 79);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (68, 2, 'tr_taxon_tax', '2238', 'duallist_taxon', 80);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (69, 2, 'tr_taxon_tax', '2247', 'duallist_taxon', 81);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (70, 2, 'tr_taxon_tax', '2137', 'duallist_taxon', 82);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (71, 2, 'tr_taxon_tax', '2197', 'duallist_taxon', 83);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (72, 2, 'tr_taxon_tax', '2038', 'duallist_taxon', 1);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (73, 2, 'tr_taxon_tax', '2037', 'duallist_taxon', 2);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (74, 2, 'tr_taxon_tax', '0', 'duallist_taxon', 82);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (75, 2, 'tr_taxon_tax', '872', 'duallist_taxon', 87);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (76, 2, 'tr_taxon_tax', '2115', 'duallist_taxon', 85);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (77, 2, 'tr_taxon_tax', '2071', 'duallist_taxon', 84);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (78, 2, 'tr_taxon_tax', '2106', 'duallist_taxon', 86);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (79, 2, 'tr_taxon_tax', '2011', 'duallist_taxon', 46);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (80, 2, 'tr_taxon_tax', '2138', 'duallist_taxon', 90);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (81, 2, 'tr_taxon_tax', '2139', 'duallist_taxon', 91);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (82, 2, 'tr_taxon_tax', '9814', 'duallist_taxon', 100);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (83, 2, 'tr_taxon_tax', '2067', 'duallist_taxon', 100);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (84, 2, 'tr_taxon_tax', '2127', 'duallist_taxon', 100);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (85, 2, 'tr_stadedeveloppement_std', 'BEC', 'duallist_stade', 1);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (86, 2, 'tr_stadedeveloppement_std', 'PRS', 'duallist_stade', 2);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (87, 2, 'tr_stadedeveloppement_std', 'SML', 'duallist_stade', 3);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (88, 2, 'tr_stadedeveloppement_std', 'IND', 'duallist_stade', 4);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (89, 2, 'tr_stadedeveloppement_std', '3', 'duallist_stade', 5);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (90, 2, 'tr_stadedeveloppement_std', '4', 'duallist_stade', 6);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (91, 2, 'tr_stadedeveloppement_std', '5', 'duallist_stade', 7);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (92, 2, 'tr_stadedeveloppement_std', 'GST', 'duallist_stade', 8);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (93, 2, 'tr_stadedeveloppement_std', '11', 'duallist_stade', 9);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (94, 2, 'tr_stadedeveloppement_std', '7', 'duallist_stade', 10);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (95, 2, 'tr_stadedeveloppement_std', '12', 'duallist_stade', 11);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (96, 2, 'tr_stadedeveloppement_std', '13', 'duallist_stade', 12);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (97, 2, 'tr_stadedeveloppement_std', 'AGP', 'duallist_stade', 13);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (98, 2, 'tr_stadedeveloppement_std', 'LEP', 'duallist_stade', 14);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (99, 2, 'tr_stadedeveloppement_std', 'AGG', 'duallist_stade', 15);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (100, 2, 'tr_stadedeveloppement_std', 'AGJ', 'duallist_stade', 16);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (101, 2, 'tr_stadedeveloppement_std', 'CIV', 'duallist_stade', 17);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (102, 2, 'tr_stadedeveloppement_std', 'ALS', 'duallist_stade', 18);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (103, 2, 'tr_stadedeveloppement_std', 'SAB', 'duallist_stade', 19);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (104, 2, 'tr_stadedeveloppement_std', 'AMM', 'duallist_stade', 20);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (105, 2, 'tr_stadedeveloppement_std', 'FIN', 'duallist_stade', 21);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (106, 2, 'tr_stadedeveloppement_std', 'TRFV', 'duallist_stade', 22);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (107, 2, 'tr_stadedeveloppement_std', 'PSP', 'duallist_stade', 23);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (108, 2, 'tr_stadedeveloppement_std', 'PSE', 'duallist_stade', 24);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (109, 2, 'tr_stadedeveloppement_std', 'POS', 'duallist_stade', 25);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (110, 2, 'tr_stadedeveloppement_std', 'TUH', 'duallist_stade', 26);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (111, 2, 'tr_stadedeveloppement_std', 'TDH', 'duallist_stade', 27);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (112, 2, 'tr_stadedeveloppement_std', 'PANG', 'duallist_stade', 28);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (113, 2, 'tr_stadedeveloppement_std', 'GANG', 'duallist_stade', 29);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (114, 2, 'tr_stadedeveloppement_std', 'PAR', 'duallist_stade', 30);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (115, 2, 'tr_stadedeveloppement_std', 'BER', 'duallist_stade', 31);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (116, 2, 'tr_stadedeveloppement_std', 'MAD', 'duallist_stade', 32);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (117, 2, 'tr_pathologie_pat', '00', 'duallist_pathologie', 42);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (118, 2, 'tr_pathologie_pat', '01', 'duallist_pathologie', 33);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (119, 2, 'tr_pathologie_pat', '11', 'duallist_pathologie', 29);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (120, 2, 'tr_pathologie_pat', '21', 'duallist_pathologie', 30);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (121, 2, 'tr_pathologie_pat', '31', 'duallist_pathologie', 31);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (122, 2, 'tr_pathologie_pat', '42', 'duallist_pathologie', 32);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (123, 2, 'tr_pathologie_pat', '51', 'duallist_pathologie', 44);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (124, 2, 'tr_pathologie_pat', 'AA', 'duallist_pathologie', 1);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (125, 2, 'tr_pathologie_pat', 'AC', 'duallist_pathologie', 2);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (126, 2, 'tr_pathologie_pat', 'AD', 'duallist_pathologie', 3);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (127, 2, 'tr_pathologie_pat', 'AG', 'duallist_pathologie', 38);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (128, 2, 'tr_pathologie_pat', 'AH', 'duallist_pathologie', 4);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (129, 2, 'tr_pathologie_pat', 'AM', 'duallist_pathologie', 5);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (130, 2, 'tr_pathologie_pat', 'AO', 'duallist_pathologie', 6);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (131, 2, 'tr_pathologie_pat', 'BG', 'duallist_pathologie', 7);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (132, 2, 'tr_pathologie_pat', 'CA', 'duallist_pathologie', 8);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (133, 2, 'tr_pathologie_pat', 'CB', 'duallist_pathologie', 43);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (134, 2, 'tr_pathologie_pat', 'CC', 'duallist_pathologie', 9);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (135, 2, 'tr_pathologie_pat', 'CO', 'duallist_pathologie', 10);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (136, 2, 'tr_pathologie_pat', 'CR', 'duallist_pathologie', 39);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (137, 2, 'tr_pathologie_pat', 'CS', 'duallist_pathologie', 11);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (138, 2, 'tr_pathologie_pat', 'CT', 'duallist_pathologie', 12);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (139, 2, 'tr_pathologie_pat', 'ER', 'duallist_pathologie', 36);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (140, 2, 'tr_pathologie_pat', 'EX', 'duallist_pathologie', 35);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (141, 2, 'tr_pathologie_pat', 'HA', 'duallist_pathologie', 13);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (142, 2, 'tr_pathologie_pat', 'HC', 'duallist_pathologie', 14);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (143, 2, 'tr_pathologie_pat', 'HE', 'duallist_pathologie', 15);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (144, 2, 'tr_pathologie_pat', 'HH', 'duallist_pathologie', 16);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (145, 2, 'tr_pathologie_pat', 'HN', 'duallist_pathologie', 17);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (146, 2, 'tr_pathologie_pat', 'HS', 'duallist_pathologie', 18);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (147, 2, 'tr_pathologie_pat', 'HT', 'duallist_pathologie', 19);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (148, 2, 'tr_pathologie_pat', 'IS', 'duallist_pathologie', 20);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (149, 2, 'tr_pathologie_pat', 'LD', 'duallist_pathologie', 21);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (150, 2, 'tr_pathologie_pat', 'NC', 'duallist_pathologie', 47);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (151, 2, 'tr_pathologie_pat', 'NE', 'duallist_pathologie', 37);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (152, 2, 'tr_pathologie_pat', 'NN', 'duallist_pathologie', 22);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (153, 2, 'tr_pathologie_pat', 'OO', 'duallist_pathologie', 46);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (154, 2, 'tr_pathologie_pat', 'PA', 'duallist_pathologie', 23);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (155, 2, 'tr_pathologie_pat', 'PB', 'duallist_pathologie', 24);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (156, 2, 'tr_pathologie_pat', 'PC', 'duallist_pathologie', 40);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (157, 2, 'tr_pathologie_pat', 'PL', 'duallist_pathologie', 25);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (158, 2, 'tr_pathologie_pat', 'PN', 'duallist_pathologie', 26);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (159, 2, 'tr_pathologie_pat', 'PT', 'duallist_pathologie', 49);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (160, 2, 'tr_pathologie_pat', 'PX', 'duallist_pathologie', 45);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (161, 2, 'tr_pathologie_pat', 'SM', 'duallist_pathologie', 41);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (162, 2, 'tr_pathologie_pat', 'UH', 'duallist_pathologie', 48);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (163, 2, 'tr_pathologie_pat', 'US', 'duallist_pathologie', 34);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (164, 2, 'tr_pathologie_pat', 'VL', 'duallist_pathologie', 27);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (165, 2, 'tr_pathologie_pat', 'ZO', 'duallist_pathologie', 28);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (166, 2, 'tr_localisationanatomique_loc', 'A', 'duallist_localisationpatho', 3);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (167, 2, 'tr_localisationanatomique_loc', 'ABD', 'duallist_localisationpatho', 34);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (168, 2, 'tr_localisationanatomique_loc', 'ADP', 'duallist_localisationpatho', 24);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (169, 2, 'tr_localisationanatomique_loc', 'ANA', 'duallist_localisationpatho', 19);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (170, 2, 'tr_localisationanatomique_loc', 'B', 'duallist_localisationpatho', 10);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (171, 2, 'tr_localisationanatomique_loc', 'C', 'duallist_localisationpatho', 1);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (172, 2, 'tr_localisationanatomique_loc', 'CAB', 'duallist_localisationpatho', 21);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (173, 2, 'tr_localisationanatomique_loc', 'CAH', 'duallist_localisationpatho', 20);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (174, 2, 'tr_localisationanatomique_loc', 'D', 'duallist_localisationpatho', 12);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (175, 2, 'tr_localisationanatomique_loc', 'D1', 'duallist_localisationpatho', 22);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (176, 2, 'tr_localisationanatomique_loc', 'D2', 'duallist_localisationpatho', 23);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (177, 2, 'tr_localisationanatomique_loc', 'DAR', 'duallist_localisationpatho', 31);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (178, 2, 'tr_localisationanatomique_loc', 'DAV', 'duallist_localisationpatho', 30);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (179, 2, 'tr_localisationanatomique_loc', 'E', 'duallist_localisationpatho', 14);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (180, 2, 'tr_localisationanatomique_loc', 'EC', 'duallist_localisationpatho', 42);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (181, 2, 'tr_localisationanatomique_loc', 'FL', 'duallist_localisationpatho', 43);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (182, 2, 'tr_localisationanatomique_loc', 'G', 'duallist_localisationpatho', 5);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (183, 2, 'tr_localisationanatomique_loc', 'GAR', 'duallist_localisationpatho', 29);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (184, 2, 'tr_localisationanatomique_loc', 'GAV', 'duallist_localisationpatho', 28);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (185, 2, 'tr_localisationanatomique_loc', 'H', 'duallist_localisationpatho', 2);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (186, 2, 'tr_localisationanatomique_loc', 'IND', 'duallist_localisationpatho', 44);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (187, 2, 'tr_localisationanatomique_loc', 'K', 'duallist_localisationpatho', 46);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (188, 2, 'tr_localisationanatomique_loc', 'L', 'duallist_localisationpatho', 7);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (189, 2, 'tr_localisationanatomique_loc', 'M', 'duallist_localisationpatho', 6);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (190, 2, 'tr_localisationanatomique_loc', 'MDD', 'duallist_localisationpatho', 33);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (191, 2, 'tr_localisationanatomique_loc', 'MDG', 'duallist_localisationpatho', 32);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (192, 2, 'tr_localisationanatomique_loc', 'MID', 'duallist_localisationpatho', 40);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (193, 2, 'tr_localisationanatomique_loc', 'MIG', 'duallist_localisationpatho', 41);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (194, 2, 'tr_localisationanatomique_loc', 'MSD', 'duallist_localisationpatho', 39);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (195, 2, 'tr_localisationanatomique_loc', 'MSG', 'duallist_localisationpatho', 38);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (196, 2, 'tr_localisationanatomique_loc', 'N', 'duallist_localisationpatho', 48);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (197, 2, 'tr_localisationanatomique_loc', 'NAS', 'duallist_localisationpatho', 27);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (198, 2, 'tr_localisationanatomique_loc', 'O', 'duallist_localisationpatho', 9);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (199, 2, 'tr_localisationanatomique_loc', 'OED', 'duallist_localisationpatho', 25);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (200, 2, 'tr_localisationanatomique_loc', 'OEG', 'duallist_localisationpatho', 26);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (201, 2, 'tr_localisationanatomique_loc', 'OPD', 'duallist_localisationpatho', 36);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (202, 2, 'tr_localisationanatomique_loc', 'OPG', 'duallist_localisationpatho', 37);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (203, 2, 'tr_localisationanatomique_loc', 'P', 'duallist_localisationpatho', 49);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (204, 2, 'tr_localisationanatomique_loc', 'PCD', 'duallist_localisationpatho', 17);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (205, 2, 'tr_localisationanatomique_loc', 'PCG', 'duallist_localisationpatho', 18);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (206, 2, 'tr_localisationanatomique_loc', 'PV', 'duallist_localisationpatho', 51);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (207, 2, 'tr_localisationanatomique_loc', 'PVD', 'duallist_localisationpatho', 15);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (208, 2, 'tr_localisationanatomique_loc', 'PVG', 'duallist_localisationpatho', 16);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (209, 2, 'tr_localisationanatomique_loc', 'Q', 'duallist_localisationpatho', 11);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (210, 2, 'tr_localisationanatomique_loc', 'T', 'duallist_localisationpatho', 4);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (211, 2, 'tr_localisationanatomique_loc', 'TDI', 'duallist_localisationpatho', 35);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (212, 2, 'tr_localisationanatomique_loc', 'U', 'duallist_localisationpatho', 13);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (213, 2, 'tr_localisationanatomique_loc', 'V', 'duallist_localisationpatho', 50);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (214, 2, 'tr_localisationanatomique_loc', 'W', 'duallist_localisationpatho', 45);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (215, 2, 'tr_localisationanatomique_loc', 'X', 'duallist_localisationpatho', 47);
INSERT INTO ts_masqueordreaffichage_maa (maa_id, maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (216, 2, 'tr_localisationanatomique_loc', 'Y', 'duallist_localisationpatho', 8);


ALTER SEQUENCE ts_masque_mas_mas_id_seq RESTART WITH 3;
ALTER SEQUENCE ts_masquecaracteristiquelot_mac_mac_id_seq RESTART WITH 1;
ALTER SEQUENCE ts_masqueordreaffichage_maa_maa_id_seq RESTART WITH 217;




INSERT INTO ts_masque_mas (mas_code, mas_description, mas_raccourci, mas_type) VALUES ('lot_ang', 'Masque pour les anguilles jaunes (passe piège)', NULL, 'lot'); --3
INSERT INTO ts_masque_mas (mas_code, mas_description, mas_raccourci, mas_type) VALUES ('lot_gd_salmo', 'Masque pour les salmonidés', NULL, 'lot'); --4
INSERT INTO ts_masque_mas (mas_code, mas_description, mas_raccourci, mas_type) VALUES ('lot_civ', 'Masque pour les lots de civelles (poids)', NULL, 'lot'); --5
INSERT INTO ts_masque_mas (mas_code, mas_description, mas_raccourci, mas_type) VALUES ('lot_civ_indiv', 'Masque pour les lots de civelles (taille-poids-stade)', NULL, 'lot');--6
INSERT INTO ts_masque_mas (mas_code, mas_description, mas_raccourci, mas_type) VALUES ('lot_argentée', 'Masque pour la saisie des argentées', NULL, 'lot');--7
INSERT INTO ts_masque_mas (mas_code, mas_description, mas_raccourci, mas_type) VALUES ('lot_smot', 'Masque pour la saisie des données pour les smolts', NULL, 'lot');--8
INSERT INTO ts_masque_mas (mas_code, mas_description, mas_raccourci, mas_type) VALUES ('lot_I', 'Masque avec saisie d''échantillons', NULL, 'lot');--9
INSERT INTO ts_masque_mas (mas_code, mas_description, mas_raccourci, mas_type) VALUES ('lot_N', 'Masque pour la saisie des lots en individuel', NULL, 'lot');--10

INSERT INTO ts_masquelot_mal (mal_mas_id, mal_affichage, mal_valeurdefaut) VALUES (3, '{f,f,t,f,f,f,f,t,t,t,t,t,t,t,t,t,t,t,t,NULL,t}', '{NULL,NULL,MESURE,"Relâché au droit de la station",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL}');
INSERT INTO ts_masquelot_mal (mal_mas_id, mal_affichage, mal_valeurdefaut) VALUES (4, '{t,f,f,f,f,t,f,t,t,t,t,t,t,t,t,t,t,t,t,NULL,t}', '{1,NULL,MESURE,"Relâché au droit de la station",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL}');
INSERT INTO ts_masquelot_mal (mal_mas_id, mal_affichage, mal_valeurdefaut) VALUES (5, '{f,f,t,f,f,f,f,t,t,t,t,t,t,t,t,t,t,t,t,NULL,t}', '{NULL,NULL,MESURE,"Relâché au droit de la station",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL}');
INSERT INTO ts_masquelot_mal (mal_mas_id, mal_affichage, mal_valeurdefaut) VALUES (6, '{t,f,f,f,f,t,f,t,t,t,t,t,t,t,t,t,t,t,t,NULL,t}', '{1,NULL,MESURE,"Relâché au droit de la station",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL}');
INSERT INTO ts_masquelot_mal (mal_mas_id, mal_affichage, mal_valeurdefaut) VALUES (7, '{f,f,f,f,f,f,f,t,t,t,t,t,t,t,t,t,t,t,t,NULL,t}', '{1,NULL,MESURE,"Relâché au droit de la station",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL}');
INSERT INTO ts_masquelot_mal (mal_mas_id, mal_affichage, mal_valeurdefaut) VALUES (8, '{t,f,f,f,f,t,f,t,t,t,t,t,t,t,t,t,t,t,t,NULL,t}', '{1,NULL,MESURE,"Relâché au droit de la station",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL}');
INSERT INTO ts_masquelot_mal (mal_mas_id, mal_affichage, mal_valeurdefaut) VALUES (9, '{t,f,f,f,f,t,f,t,t,t,t,t,t,t,t,t,t,t,t,NULL,t}', '{1,NULL,MESURE,"Relâché au droit de la station",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL}');
INSERT INTO ts_masquelot_mal (mal_mas_id, mal_affichage, mal_valeurdefaut) VALUES (10, '{t,f,f,f,f,t,f,t,t,t,t,t,t,t,t,t,t,t,t,NULL,t}', '{1,NULL,MESURE,"Relâché au droit de la station",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL}');


--lot_ang
INSERT INTO ts_masquecaracteristiquelot_mac (mac_mal_id, mac_par_code, mac_affichagevaleur, mac_affichageprecision, mac_affichagemethodeobtention, mac_affichagecommentaire, mac_valeurquantitatifdefaut, mac_valeurqualitatifdefaut, mac_precisiondefaut, mac_methodeobtentiondefaut, mac_commentairedefaut) 
VALUES (3, '1786', true, true, false, false, NULL, NULL, 1.0, 'MESURE', NULL);
INSERT INTO ts_masquecaracteristiquelot_mac (mac_mal_id, mac_par_code, mac_affichagevaleur, mac_affichageprecision, mac_affichagemethodeobtention, mac_affichagecommentaire, mac_valeurquantitatifdefaut, mac_valeurqualitatifdefaut, mac_precisiondefaut, mac_methodeobtentiondefaut, mac_commentairedefaut) 
VALUES (3, 'A111', true, true, false, false, NULL, NULL, 1.0, 'MESURE', NULL);

INSERT INTO ts_masqueordreaffichage_maa (maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_stadedeveloppement_std', 'AGJ', 'duallist_stade', 1);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'NN', 'duallist_pathologie', 1);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'IS', 'duallist_pathologie', 2);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'AA', 'duallist_pathologie', 3);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'AC', 'duallist_pathologie', 4);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'AD', 'duallist_pathologie', 5);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'AG', 'duallist_pathologie', 6);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'AM', 'duallist_pathologie', 7);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'AO', 'duallist_pathologie', 8);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'CB', 'duallist_pathologie', 10);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'CC', 'duallist_pathologie', 3);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'CO', 'duallist_pathologie', 12);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'BG', 'duallist_pathologie', 13);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'CR', 'duallist_pathologie', 14);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'CS', 'duallist_pathologie', 15);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'CT', 'duallist_pathologie', 16);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'ER', 'duallist_pathologie', 17);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'EX', 'duallist_pathologie', 18);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'HA', 'duallist_pathologie', 19);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'HC', 'duallist_pathologie', 20);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'HE', 'duallist_pathologie', 21);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'HH', 'duallist_pathologie', 22);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'HN', 'duallist_pathologie', 23);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'HS', 'duallist_pathologie', 24);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'HT', 'duallist_pathologie', 25);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'LD', 'duallist_pathologie', 26);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'OO', 'duallist_pathologie', 27);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'PA', 'duallist_pathologie', 28);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'PB', 'duallist_pathologie', 29);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'PL', 'duallist_pathologie', 31);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'PN', 'duallist_pathologie', 32);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'PT', 'duallist_pathologie', 33);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'PX', 'duallist_pathologie', 34);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'SM', 'duallist_pathologie', 35);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'UH', 'duallist_pathologie', 36);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'US', 'duallist_pathologie', 37);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'VL', 'duallist_pathologie', 38);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'ZO', 'duallist_pathologie', 39);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'NC', 'duallist_pathologie', 40);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'NE', 'duallist_pathologie', 41);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'IND', 'duallist_localisationpatho', 1);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'C', 'duallist_localisationpatho', 2);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'EC', 'duallist_localisationpatho', 3);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'T', 'duallist_localisationpatho', 4);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'W', 'duallist_localisationpatho', 5);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'K', 'duallist_localisationpatho', 6);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'H', 'duallist_localisationpatho', 7);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'A', 'duallist_localisationpatho', 9);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'N', 'duallist_localisationpatho', 10);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'P', 'duallist_localisationpatho', 3);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'PCD', 'duallist_localisationpatho', 12);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'PCG', 'duallist_localisationpatho', 13);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'PV', 'duallist_localisationpatho', 14);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'PVD', 'duallist_localisationpatho', 15);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'PVG', 'duallist_localisationpatho', 16);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'M', 'duallist_localisationpatho', 17);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'Y', 'duallist_localisationpatho', 18);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'OED', 'duallist_localisationpatho', 19);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'OEG', 'duallist_localisationpatho', 20);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'L', 'duallist_localisationpatho', 21);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'O', 'duallist_localisationpatho', 22);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'G', 'duallist_localisationpatho', 23);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_taxon_tax', '2038', 'duallist_taxon', 1);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'CA', 'duallist_pathologie', 9);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_pathologie_pat', 'PC', 'duallist_pathologie', 30);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'FL', 'duallist_localisationpatho', 8);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'B', 'duallist_localisationpatho', 24);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'X', 'duallist_localisationpatho', 25);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'U', 'duallist_localisationpatho', 26);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (3, 'tr_localisation_loc', 'V', 'duallist_localisationpatho', 27);
-- lot_gd_salmo
INSERT INTO ts_masquecaracteristiquelot_mac (mac_mal_id, mac_par_code, mac_affichagevaleur, mac_affichageprecision, mac_affichagemethodeobtention, mac_affichagecommentaire, mac_valeurquantitatifdefaut, mac_valeurqualitatifdefaut, mac_precisiondefaut, mac_methodeobtentiondefaut, mac_commentairedefaut) 
VALUES (4, '1786', true, true, true, true, NULL, NULL, 1.0, 'MESURE', NULL);
INSERT INTO ts_masquecaracteristiquelot_mac (mac_mal_id, mac_par_code, mac_affichagevaleur, mac_affichageprecision, mac_affichagemethodeobtention, mac_affichagecommentaire, mac_valeurquantitatifdefaut, mac_valeurqualitatifdefaut, mac_precisiondefaut, mac_methodeobtentiondefaut, mac_commentairedefaut) 
VALUES (4, '1785', true, true, true, true, NULL, NULL, 1.0, 'MESURE', NULL);
INSERT INTO ts_masquecaracteristiquelot_mac (mac_mal_id, mac_par_code, mac_affichagevaleur, mac_affichageprecision, mac_affichagemethodeobtention, mac_affichagecommentaire, mac_valeurquantitatifdefaut, mac_valeurqualitatifdefaut, mac_precisiondefaut, mac_methodeobtentiondefaut, mac_commentairedefaut) 
VALUES (4, 'A111', true, true, true, true, NULL, NULL, 1.0, 'MESURE', NULL);
INSERT INTO ts_masquecaracteristiquelot_mac (mac_mal_id, mac_par_code, mac_affichagevaleur, mac_affichageprecision, mac_affichagemethodeobtention, mac_affichagecommentaire, mac_valeurquantitatifdefaut, mac_valeurqualitatifdefaut, mac_precisiondefaut, mac_methodeobtentiondefaut, mac_commentairedefaut) 
VALUES (4, '1787', true, true, true, true, NULL, NULL, 1.0, 'MESURE', NULL);
INSERT INTO ts_masquecaracteristiquelot_mac (mac_mal_id, mac_par_code, mac_affichagevaleur, mac_affichageprecision, mac_affichagemethodeobtention, mac_affichagecommentaire, mac_valeurquantitatifdefaut, mac_valeurqualitatifdefaut, mac_precisiondefaut, mac_methodeobtentiondefaut, mac_commentairedefaut) 
VALUES (4, '1783', true, false, true, true, NULL, 1, NULL, 'MESURE', NULL);
INSERT INTO ts_masquecaracteristiquelot_mac (mac_mal_id, mac_par_code, mac_affichagevaleur, mac_affichageprecision, mac_affichagemethodeobtention, mac_affichagecommentaire, mac_valeurquantitatifdefaut, mac_valeurqualitatifdefaut, mac_precisiondefaut, mac_methodeobtentiondefaut, mac_commentairedefaut) 
VALUES (4, '1784', true, false, true, true, NULL, 13, NULL, 'EXPERT', NULL);
INSERT INTO ts_masquecaracteristiquelot_mac (mac_mal_id, mac_par_code, mac_affichagevaleur, mac_affichageprecision, mac_affichagemethodeobtention, mac_affichagecommentaire, mac_valeurquantitatifdefaut, mac_valeurqualitatifdefaut, mac_precisiondefaut, mac_methodeobtentiondefaut, mac_commentairedefaut) 
VALUES (4, 'A126', true, true, true, true, NULL, NULL, NULL, 'MESURE', NULL);

INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_taxon_tax', '2220', 'duallist_taxon', 1);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_taxon_tax', '2224', 'duallist_taxon', 2);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_stadedeveloppement_std', '11', 'duallist_stade', 1);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_stadedeveloppement_std', 'BEC', 'duallist_stade', 2);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_stadedeveloppement_std', 'BER', 'duallist_stade', 3);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', '00', 'duallist_pathologie', 1);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', '01', 'duallist_pathologie', 2);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', '11', 'duallist_pathologie', 3);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', '21', 'duallist_pathologie', 4);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', '31', 'duallist_pathologie', 5);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', '42', 'duallist_pathologie', 6);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', '51', 'duallist_pathologie', 7);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'AA', 'duallist_pathologie', 8);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'AC', 'duallist_pathologie', 9);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'AD', 'duallist_pathologie', 10);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'AG', 'duallist_pathologie', 11);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'AH', 'duallist_pathologie', 12);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'AM', 'duallist_pathologie', 13);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'AO', 'duallist_pathologie', 14);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'BG', 'duallist_pathologie', 15);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'CA', 'duallist_pathologie', 16);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'CB', 'duallist_pathologie', 17);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'CC', 'duallist_pathologie', 18);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'CO', 'duallist_pathologie', 19);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'CR', 'duallist_pathologie', 20);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'CS', 'duallist_pathologie', 21);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'CT', 'duallist_pathologie', 22);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'ER', 'duallist_pathologie', 23);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'EX', 'duallist_pathologie', 24);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'HA', 'duallist_pathologie', 25);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'HC', 'duallist_pathologie', 26);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'HE', 'duallist_pathologie', 27);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'HH', 'duallist_pathologie', 28);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'HN', 'duallist_pathologie', 29);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'HS', 'duallist_pathologie', 30);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'HT', 'duallist_pathologie', 31);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'IS', 'duallist_pathologie', 32);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'LD', 'duallist_pathologie', 33);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'NC', 'duallist_pathologie', 34);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'NE', 'duallist_pathologie', 35);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'NN', 'duallist_pathologie', 36);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'OO', 'duallist_pathologie', 37);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'PA', 'duallist_pathologie', 38);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'PB', 'duallist_pathologie', 39);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'PC', 'duallist_pathologie', 40);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'PL', 'duallist_pathologie', 41);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'PN', 'duallist_pathologie', 42);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'PT', 'duallist_pathologie', 43);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'PX', 'duallist_pathologie', 44);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'SM', 'duallist_pathologie', 45);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'UH', 'duallist_pathologie', 46);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'US', 'duallist_pathologie', 47);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'VL', 'duallist_pathologie', 48);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_pathologie_pat', 'ZO', 'duallist_pathologie', 49);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'C', 'duallist_localisationpatho', 1);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'EC', 'duallist_localisationpatho', 2);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'T', 'duallist_localisationpatho', 3);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'W', 'duallist_localisationpatho', 4);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'K', 'duallist_localisationpatho', 5);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'H', 'duallist_localisationpatho', 6);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'FL', 'duallist_localisationpatho', 7);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'A', 'duallist_localisationpatho', 8);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'N', 'duallist_localisationpatho', 9);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'P', 'duallist_localisationpatho', 10);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'ANA', 'duallist_localisationpatho', 11);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'D', 'duallist_localisationpatho', 12);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'Q', 'duallist_localisationpatho', 13);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'PV', 'duallist_localisationpatho', 14);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'ADP', 'duallist_localisationpatho', 15);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'NAS', 'duallist_localisationpatho', 16);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'G', 'duallist_localisationpatho', 17);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'M', 'duallist_localisationpatho', 18);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'L', 'duallist_localisationpatho', 19);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'Y', 'duallist_localisationpatho', 20);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'O', 'duallist_localisationpatho', 21);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'B', 'duallist_localisationpatho', 22);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'U', 'duallist_localisationpatho', 23);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'X', 'duallist_localisationpatho', 24);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'V', 'duallist_localisationpatho', 25);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (4, 'tr_localisation_loc', 'IND', 'duallist_localisationpatho', 26);

--lot_civ

INSERT INTO ts_masquecaracteristiquelot_mac (mac_mal_id, mac_par_code, mac_affichagevaleur, mac_affichageprecision, mac_affichagemethodeobtention, mac_affichagecommentaire, 
mac_valeurquantitatifdefaut, mac_valeurqualitatifdefaut, mac_precisiondefaut, mac_methodeobtentiondefaut, 
mac_commentairedefaut) VALUES (5, 'A111', true, true, false, false, NULL, NULL, 1.0, 'MESURE', NULL);


INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_stadedeveloppement_std', 'CIV', 'duallist_stade', 1);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'NN', 'duallist_pathologie', 1);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'IS', 'duallist_pathologie', 2);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'AA', 'duallist_pathologie', 3);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'AC', 'duallist_pathologie', 4);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'AD', 'duallist_pathologie', 5);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'AG', 'duallist_pathologie', 6);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'AM', 'duallist_pathologie', 7);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'AO', 'duallist_pathologie', 8);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'CB', 'duallist_pathologie', 10);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'CC', 'duallist_pathologie', 3);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'CO', 'duallist_pathologie', 12);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'BG', 'duallist_pathologie', 13);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'CR', 'duallist_pathologie', 14);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'CS', 'duallist_pathologie', 15);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'CT', 'duallist_pathologie', 16);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'ER', 'duallist_pathologie', 17);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'EX', 'duallist_pathologie', 18);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'HA', 'duallist_pathologie', 19);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'HC', 'duallist_pathologie', 20);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'HE', 'duallist_pathologie', 21);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'HH', 'duallist_pathologie', 22);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'HN', 'duallist_pathologie', 23);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'HS', 'duallist_pathologie', 24);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'HT', 'duallist_pathologie', 25);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'LD', 'duallist_pathologie', 26);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'OO', 'duallist_pathologie', 27);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'PA', 'duallist_pathologie', 28);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'PB', 'duallist_pathologie', 29);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'PL', 'duallist_pathologie', 31);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'PN', 'duallist_pathologie', 32);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'PT', 'duallist_pathologie', 33);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'PX', 'duallist_pathologie', 34);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'SM', 'duallist_pathologie', 35);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'UH', 'duallist_pathologie', 36);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'US', 'duallist_pathologie', 37);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'VL', 'duallist_pathologie', 38);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'ZO', 'duallist_pathologie', 39);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'NC', 'duallist_pathologie', 40);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'NE', 'duallist_pathologie', 41);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'IND', 'duallist_localisationpatho', 1);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'C', 'duallist_localisationpatho', 2);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'EC', 'duallist_localisationpatho', 3);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'T', 'duallist_localisationpatho', 4);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'W', 'duallist_localisationpatho', 5);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'K', 'duallist_localisationpatho', 6);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'H', 'duallist_localisationpatho', 7);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'A', 'duallist_localisationpatho', 9);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'N', 'duallist_localisationpatho', 10);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'P', 'duallist_localisationpatho', 3);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'PCD', 'duallist_localisationpatho', 12);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'PCG', 'duallist_localisationpatho', 13);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'PV', 'duallist_localisationpatho', 14);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'PVD', 'duallist_localisationpatho', 15);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'PVG', 'duallist_localisationpatho', 16);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'M', 'duallist_localisationpatho', 17);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'Y', 'duallist_localisationpatho', 18);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'OED', 'duallist_localisationpatho', 19);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'OEG', 'duallist_localisationpatho', 20);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'L', 'duallist_localisationpatho', 21);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'O', 'duallist_localisationpatho', 22);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'G', 'duallist_localisationpatho', 23);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_taxon_tax', '2038', 'duallist_taxon', 1);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'CA', 'duallist_pathologie', 9);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_pathologie_pat', 'PC', 'duallist_pathologie', 30);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'FL', 'duallist_localisationpatho', 8);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'B', 'duallist_localisationpatho', 24);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'X', 'duallist_localisationpatho', 25);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'U', 'duallist_localisationpatho', 26);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (5, 'tr_localisation_loc', 'V', 'duallist_localisationpatho', 27);


--lot_civ_indiv

INSERT INTO ts_masquecaracteristiquelot_mac (mac_mal_id, mac_par_code, mac_affichagevaleur, mac_affichageprecision, mac_affichagemethodeobtention, mac_affichagecommentaire, mac_valeurquantitatifdefaut, mac_valeurqualitatifdefaut, mac_precisiondefaut, mac_methodeobtentiondefaut, mac_commentairedefaut) 
VALUES (6, 'A111', true, true, false, false, NULL, NULL, 1.0, 'MESURE', NULL);
INSERT INTO ts_masquecaracteristiquelot_mac (mac_mal_id, mac_par_code, mac_affichagevaleur, mac_affichageprecision, mac_affichagemethodeobtention, mac_affichagecommentaire, mac_valeurquantitatifdefaut, mac_valeurqualitatifdefaut, mac_precisiondefaut, mac_methodeobtentiondefaut, mac_commentairedefaut) 
VALUES (6, '1786', true, true, false, false, NULL, NULL, 1.0, 'MESURE', NULL);
INSERT INTO ts_masquecaracteristiquelot_mac (mac_mal_id, mac_par_code, mac_affichagevaleur, mac_affichageprecision, mac_affichagemethodeobtention, mac_affichagecommentaire, mac_valeurquantitatifdefaut, mac_valeurqualitatifdefaut, mac_precisiondefaut, mac_methodeobtentiondefaut, mac_commentairedefaut) 
VALUES (6, '1791', true, true, false, false, NULL, NULL, 14, 'MESURE', NULL);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_stadedeveloppement_std', 'CIV', 'duallist_stade', 1);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_taxon_tax', '2038', 'duallist_taxon', 1);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'NN', 'duallist_pathologie', 1);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'IS', 'duallist_pathologie', 2);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'AA', 'duallist_pathologie', 3);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'AC', 'duallist_pathologie', 4);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'AD', 'duallist_pathologie', 5);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'AG', 'duallist_pathologie', 6);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'AM', 'duallist_pathologie', 7);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'AO', 'duallist_pathologie', 8);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'CA', 'duallist_pathologie', 9);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'CB', 'duallist_pathologie', 10);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'CC', 'duallist_pathologie', 3);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'CO', 'duallist_pathologie', 12);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'BG', 'duallist_pathologie', 13);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'CR', 'duallist_pathologie', 14);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'CS', 'duallist_pathologie', 15);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'CT', 'duallist_pathologie', 16);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'ER', 'duallist_pathologie', 17);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'EX', 'duallist_pathologie', 18);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'HA', 'duallist_pathologie', 19);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'HC', 'duallist_pathologie', 20);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'HE', 'duallist_pathologie', 21);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'HH', 'duallist_pathologie', 22);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'HN', 'duallist_pathologie', 23);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'HS', 'duallist_pathologie', 24);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'HT', 'duallist_pathologie', 25);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'LD', 'duallist_pathologie', 26);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'OO', 'duallist_pathologie', 27);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'PA', 'duallist_pathologie', 28);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'PB', 'duallist_pathologie', 29);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'PC', 'duallist_pathologie', 30);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'PL', 'duallist_pathologie', 31);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'PN', 'duallist_pathologie', 32);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'PT', 'duallist_pathologie', 33);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'PX', 'duallist_pathologie', 34);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'SM', 'duallist_pathologie', 35);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'UH', 'duallist_pathologie', 36);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'US', 'duallist_pathologie', 37);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'VL', 'duallist_pathologie', 38);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'ZO', 'duallist_pathologie', 39);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'NC', 'duallist_pathologie', 40);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_pathologie_pat', 'NE', 'duallist_pathologie', 41);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'IND', 'duallist_localisationpatho', 1);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'C', 'duallist_localisationpatho', 2);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'EC', 'duallist_localisationpatho', 3);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'P', 'duallist_localisationpatho', 4);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'T', 'duallist_localisationpatho', 5);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'W', 'duallist_localisationpatho', 6);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'K', 'duallist_localisationpatho', 7);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'H', 'duallist_localisationpatho', 8);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'FL', 'duallist_localisationpatho', 9);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'A', 'duallist_localisationpatho', 10);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'N', 'duallist_localisationpatho', 11);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'PCD', 'duallist_localisationpatho', 12);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'PCG', 'duallist_localisationpatho', 13);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'PV', 'duallist_localisationpatho', 14);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'PVD', 'duallist_localisationpatho', 15);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'PVG', 'duallist_localisationpatho', 16);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'M', 'duallist_localisationpatho', 17);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'Y', 'duallist_localisationpatho', 18);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'OED', 'duallist_localisationpatho', 19);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'OEG', 'duallist_localisationpatho', 20);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'L', 'duallist_localisationpatho', 21);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'O', 'duallist_localisationpatho', 22);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'G', 'duallist_localisationpatho', 23);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'B', 'duallist_localisationpatho', 24);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'X', 'duallist_localisationpatho', 25);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'U', 'duallist_localisationpatho', 26);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (6, 'tr_localisation_loc', 'V', 'duallist_localisationpatho', 27);

--lot argentee

INSERT INTO ts_masquecaracteristiquelot_mac (mac_mal_id, mac_par_code, mac_affichagevaleur, mac_affichageprecision, mac_affichagemethodeobtention, mac_affichagecommentaire, mac_valeurquantitatifdefaut, mac_valeurqualitatifdefaut, mac_precisiondefaut, mac_methodeobtentiondefaut, mac_commentairedefaut) VALUES (7, '1786', true, true, true, true, NULL, NULL, 1.0, 'MESURE', NULL);
INSERT INTO ts_masquecaracteristiquelot_mac (mac_mal_id, mac_par_code, mac_affichagevaleur, mac_affichageprecision, mac_affichagemethodeobtention, mac_affichagecommentaire, mac_valeurquantitatifdefaut, mac_valeurqualitatifdefaut, mac_precisiondefaut, mac_methodeobtentiondefaut, mac_commentairedefaut) VALUES (7, 'A111', true, true, true, true, NULL, NULL, 1.0, 'MESURE', NULL);
INSERT INTO ts_masquecaracteristiquelot_mac (mac_mal_id, mac_par_code, mac_affichagevaleur, mac_affichageprecision, mac_affichagemethodeobtention, mac_affichagecommentaire, mac_valeurquantitatifdefaut, mac_valeurqualitatifdefaut, mac_precisiondefaut, mac_methodeobtentiondefaut, mac_commentairedefaut) VALUES (7, 'PECT', true, true, true, true, NULL, NULL, 1.0, 'MESURE', NULL);
INSERT INTO ts_masquecaracteristiquelot_mac (mac_mal_id, mac_par_code, mac_affichagevaleur, mac_affichageprecision, mac_affichagemethodeobtention, mac_affichagecommentaire, mac_valeurquantitatifdefaut, mac_valeurqualitatifdefaut, mac_precisiondefaut, mac_methodeobtentiondefaut, mac_commentairedefaut) VALUES (7, 'CCCC', true, true, true, true, NULL, NULL, 1.0, 'MESURE', NULL);
INSERT INTO ts_masquecaracteristiquelot_mac (mac_mal_id, mac_par_code, mac_affichagevaleur, mac_affichageprecision, mac_affichagemethodeobtention, mac_affichagecommentaire, mac_valeurquantitatifdefaut, mac_valeurqualitatifdefaut, mac_precisiondefaut, mac_methodeobtentiondefaut, mac_commentairedefaut) VALUES (7, 'BBBB', true, true, true, true, NULL, NULL, 1.0, 'MESURE', NULL);
INSERT INTO ts_masquecaracteristiquelot_mac (mac_mal_id, mac_par_code, mac_affichagevaleur, mac_affichageprecision, mac_affichagemethodeobtention, mac_affichagecommentaire, mac_valeurquantitatifdefaut, mac_valeurqualitatifdefaut, mac_precisiondefaut, mac_methodeobtentiondefaut, mac_commentairedefaut) VALUES (7, 'LINP', true, false, true, true, NULL, 59, NULL, 'EXPERT', NULL);
INSERT INTO ts_masquecaracteristiquelot_mac (mac_mal_id, mac_par_code, mac_affichagevaleur, mac_affichageprecision, mac_affichagemethodeobtention, mac_affichagecommentaire, mac_valeurquantitatifdefaut, mac_valeurqualitatifdefaut, mac_precisiondefaut, mac_methodeobtentiondefaut, mac_commentairedefaut) VALUES (7, 'CONT', true, false, true, true, NULL, 57, NULL, 'EXPERT', NULL);

INSERT INTO ts_masqueordreaffichage_maa (maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_taxon_tax', '2038', 'duallist_taxon', 1);
INSERT INTO ts_masqueordreaffichage_maa (maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_stadedeveloppement_std', 'AGG', 'duallist_stade', 1);
INSERT INTO ts_masqueordreaffichage_maa (maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_stadedeveloppement_std', 'AGJ', 'duallist_stade', 2);
INSERT INTO ts_masqueordreaffichage_maa (maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_stadedeveloppement_std', 'AGP', 'duallist_stade', 3);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'NN', 'duallist_pathologie', 1);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'IS', 'duallist_pathologie', 2);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'AA', 'duallist_pathologie', 3);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'AC', 'duallist_pathologie', 4);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'AD', 'duallist_pathologie', 5);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'AG', 'duallist_pathologie', 6);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'AM', 'duallist_pathologie', 7);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'AO', 'duallist_pathologie', 8);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'CB', 'duallist_pathologie', 10);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'CC', 'duallist_pathologie', 3);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'CO', 'duallist_pathologie', 12);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'BG', 'duallist_pathologie', 13);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'CR', 'duallist_pathologie', 14);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'CS', 'duallist_pathologie', 15);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'CT', 'duallist_pathologie', 16);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'ER', 'duallist_pathologie', 17);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'EX', 'duallist_pathologie', 18);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'HA', 'duallist_pathologie', 19);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'HC', 'duallist_pathologie', 20);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'HE', 'duallist_pathologie', 21);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'HH', 'duallist_pathologie', 22);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'HN', 'duallist_pathologie', 23);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'HS', 'duallist_pathologie', 24);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'HT', 'duallist_pathologie', 25);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'LD', 'duallist_pathologie', 26);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'OO', 'duallist_pathologie', 27);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'PA', 'duallist_pathologie', 28);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'PB', 'duallist_pathologie', 29);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'PL', 'duallist_pathologie', 31);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'PN', 'duallist_pathologie', 32);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'PT', 'duallist_pathologie', 33);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'PX', 'duallist_pathologie', 34);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'SM', 'duallist_pathologie', 35);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'UH', 'duallist_pathologie', 36);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'US', 'duallist_pathologie', 37);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'VL', 'duallist_pathologie', 38);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'ZO', 'duallist_pathologie', 39);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'NC', 'duallist_pathologie', 40);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'NE', 'duallist_pathologie', 41);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'IND', 'duallist_localisationpatho', 1);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'C', 'duallist_localisationpatho', 2);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'EC', 'duallist_localisationpatho', 3);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'T', 'duallist_localisationpatho', 4);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'W', 'duallist_localisationpatho', 5);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'K', 'duallist_localisationpatho', 6);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'H', 'duallist_localisationpatho', 7);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'A', 'duallist_localisationpatho', 9);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'N', 'duallist_localisationpatho', 10);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'P', 'duallist_localisationpatho', 3);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'PCD', 'duallist_localisationpatho', 12);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'PCG', 'duallist_localisationpatho', 13);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'PV', 'duallist_localisationpatho', 14);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'PVD', 'duallist_localisationpatho', 15);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'PVG', 'duallist_localisationpatho', 16);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'M', 'duallist_localisationpatho', 17);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'Y', 'duallist_localisationpatho', 18);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'OED', 'duallist_localisationpatho', 19);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'OEG', 'duallist_localisationpatho', 20);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'L', 'duallist_localisationpatho', 21);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'O', 'duallist_localisationpatho', 22);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'G', 'duallist_localisationpatho', 23);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'CA', 'duallist_pathologie', 9);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_pathologie_pat', 'PC', 'duallist_pathologie', 30);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'FL', 'duallist_localisationpatho', 8);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'B', 'duallist_localisationpatho', 24);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'X', 'duallist_localisationpatho', 25);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'U', 'duallist_localisationpatho', 26);
INSERT INTO ts_masqueordreaffichage_maa ( maa_mal_id, maa_table, maa_valeur, maa_champdumasque, maa_rang) VALUES (7, 'tr_localisation_loc', 'V', 'duallist_localisationpatho', 27);
