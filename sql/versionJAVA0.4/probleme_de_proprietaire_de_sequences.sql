﻿/*
31/12/2015
Problèmes de droits sur les séquences pour leur mise à jour dans JAVA
j'ai dû remettre les doits aux rôles qui les avaient perdu (il y avait une expiration dans la base, vérifier ça en maintenance)
*/



ALTER SEQUENCE "iav".t_operation_ope_ope_identifiant_seq RESTART WITH 194166


/* 
ci dessous ne suffit pas, et le fait que postgres ne supporte pas l'usage de la séquence (via all) par exemple est inquiétant
*/

GRANT SELECT, UPDATE, INSERT, DELETE ON ALL TABLES in schema iav to iav;
grant select,usage,update on all sequences in schema iav to iav;
grant usage on schema iav to iav;

/*
Il faut modifier tous les propriétaires des table à séquences
*/


alter table iav.t_operation_ope owner to iav;
alter table iav.t_lot_lot owner to iav;
alter table iav.t_ouvrage_ouv owner to iav;
alter table iav.tg_dispositif_dis owner to iav;
alter table iav.tj_stationmesure_stm owner to iav;

ALTER SEQUENCE iav.t_lot_lot_lot_identifiant_seq owner to iav;
ALTER SEQUENCE iav.t_operation_ope_ope_identifiant_seq owner to iav;
ALTER SEQUENCE iav.t_ouvrage_ouv_ouv_identifiant_seq owner to iav;
ALTER SEQUENCE iav.tg_dispositif_dis_dis_identifiant_seq owner to iav;
ALTER SEQUENCE iav.tj_stationmesure_stm_stm_identifiant_seq owner to iav;

