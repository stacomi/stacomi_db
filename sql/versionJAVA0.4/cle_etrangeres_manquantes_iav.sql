﻿/*
08/05/2016 clés manquantes sur schéma iav
*/
set search_path to 'iav';
ALTER TABLE ONLY tj_caracteristiquelot_car
    ADD CONSTRAINT c_fk_car_val_identifiant FOREIGN KEY (car_val_identifiant) 
	REFERENCES ref.tr_valeurparametrequalitatif_val(val_identifiant) ON UPDATE CASCADE;


ALTER TABLE  ts_masqueconditionsenvironnementales_mae add CONSTRAINT c_fk_mae_stm_identifiant FOREIGN KEY (mae_stm_identifiant)
      REFERENCES tj_stationmesure_stm (stm_identifiant); 


ALTER TABLE ONLY tj_prelevementlot_prl
    ADD CONSTRAINT c_fk_prl_org_code FOREIGN KEY (prl_org_code) REFERENCES ref.ts_organisme_org (org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY tj_prelevementlot_prl 
    ADD CONSTRAINT c_fk_prl_pre_nom FOREIGN KEY (prl_pre_typeprelevement)
      REFERENCES ref.tr_prelevement_pre (pre_typeprelevement) MATCH SIMPLE
      ON UPDATE CASCADE;   
