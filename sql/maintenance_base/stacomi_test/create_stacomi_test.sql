--========================================
-- Création du rôle de connexion test
--========================================
CREATE ROLE stacomi_test;
ALTER ROLE stacomi_test WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'stacomi_test';

--Restaurer le dump du schéma test (vérifier que le owner est bien stacomi_test)

--quand c'est fait on crée un rôle de groupe nommé group_stacomi
CREATE ROLE group_stacomi;
--on donne les droits qui vont bien
GRANT group_stacomi TO stacomi_test;
GRANT ALL ON SCHEMA test TO group_stacomi;
REASSIGN OWNED BY stacomi_test TO group_stacomi;


--On met a jour la table ts_maintenance_main
select ref.updatesql('{"test"}',
'
insert into ts_maintenance_main
(
  main_ticket,
  main_description
  ) SELECT
  23,''Update to version 0.6.0''
  WHERE NOT EXISTS (
    SELECT 1 FROM test.ts_maintenance_main WHERE main_ticket=23)
');
