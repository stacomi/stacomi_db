WITH jjj AS (
SELECT extract(YEAR FROM ope_date_debut) annee, lot_std_code,lot_effectif FROM iav.t_operation_ope JOIN iav.t_lot_lot
ON lot_ope_identifiant=ope_identifiant WHERE ope_dic_identifiant=5
AND lot_tax_code = '2038' AND lot_lot_identifiant IS NULL 
)

SELECT sum(lot_effectif) AS effectif, annee, lot_std_code FROM jjj 
GROUP BY annee, lot_std_code
ORDER BY annee, lot_std_code

-- 1. P�riode de Migration
-- Anguilles de plus de 30 cm qui d�valent apr�s ao�t 
-- sont des argent�es
WITH big AS (
SELECT
ope_date_debut, 
extract("month" FROM ope_date_debut) mois,
extract(YEAR FROM ope_date_debut) annee,
lot_identifiant,
lot_std_code,
lot_effectif,
car_valeur_quantitatif
FROM iav.t_operation_ope 
JOIN iav.t_lot_lot ON lot_ope_identifiant=ope_identifiant 
JOIN iav.tj_caracteristiquelot_car tcc ON tcc.car_lot_identifiant = lot_identifiant 
WHERE ope_dic_identifiant=5
AND lot_tax_code = '2038'
AND car_par_code='C001'
AND car_valeur_quantitatif>= 300
),
-- argent�es � corriger
silver AS (
SELECT  * FROM big
WHERE (mois>8 OR mois <5)
AND lot_effectif=-1
AND lot_std_code != 'AGG'
)

--SELECT * FROM silver --173

UPDATE iav.t_lot_lot  SET lot_std_code= 'AGG' FROM silver WHERE  silver.lot_identifiant = t_lot_lot.lot_identifiant; --173

-- Anguilles de plus de 30 cm qui montent apr�s ao�t 
-- et qui sont class�es comme IND sont des jaunes
WITH big AS (
SELECT
ope_date_debut, 
extract("month" FROM ope_date_debut) mois,
extract(YEAR FROM ope_date_debut) annee,
lot_identifiant,
lot_std_code,
lot_effectif,
car_valeur_quantitatif
FROM iav.t_operation_ope 
JOIN iav.t_lot_lot ON lot_ope_identifiant=ope_identifiant 
JOIN iav.tj_caracteristiquelot_car tcc ON tcc.car_lot_identifiant = lot_identifiant 
WHERE ope_dic_identifiant=5
AND lot_tax_code = '2038'
AND car_par_code='C001'
AND car_valeur_quantitatif>= 300
),
-- argent�es � corriger
silver AS (
SELECT  * FROM big
WHERE (mois>8 OR mois <5)
AND lot_effectif = 1
AND lot_std_code = 'IND'
)

--SELECT * FROM silver --173

UPDATE iav.t_lot_lot  SET lot_std_code= 'AGJ' FROM silver WHERE  silver.lot_identifiant = t_lot_lot.lot_identifiant; --16

-- Anguilles de moins de 30 cm toutes p�riodes de migration sont des AGJ quel que soit leur sens

WITH small AS (
SELECT
ope_date_debut, 
extract("month" FROM ope_date_debut) mois,
extract(YEAR FROM ope_date_debut) annee,
lot_identifiant,
lot_std_code,
lot_effectif,
car_valeur_quantitatif
FROM iav.t_operation_ope 
JOIN iav.t_lot_lot ON lot_ope_identifiant=ope_identifiant 
JOIN iav.tj_caracteristiquelot_car tcc ON tcc.car_lot_identifiant = lot_identifiant 
WHERE ope_dic_identifiant=5
AND lot_tax_code = '2038'
AND car_par_code='C001'
AND car_valeur_quantitatif<= 300
),

yellow AS (
SELECT  * FROM small
WHERE (mois>8 OR mois <5)
AND lot_std_code != 'AGJ'
)

--SELECT * FROM yellow

UPDATE iav.t_lot_lot  SET lot_std_code= 'AGJ' FROM yellow WHERE  yellow.lot_identifiant = t_lot_lot.lot_identifiant;--101



-- 2 Hors p�riode migration : jaunes
-- Anguilles de toute taille qui d�valent entre mai et ao�t  sont consid�r�es comme jaunes
WITH extracteel AS (
SELECT
ope_date_debut, 
extract("month" FROM ope_date_debut) mois,
extract(YEAR FROM ope_date_debut) annee,
lot_identifiant,
lot_std_code,
lot_effectif,
car_valeur_quantitatif
FROM iav.t_operation_ope 
JOIN iav.t_lot_lot ON lot_ope_identifiant=ope_identifiant 
JOIN iav.tj_caracteristiquelot_car tcc ON tcc.car_lot_identifiant = lot_identifiant 
WHERE ope_dic_identifiant=5
AND lot_tax_code = '2038'
AND car_par_code='C001'
AND lot_std_code !='AGJ'),

yellow AS (
SELECT  * FROM extracteel
WHERE (mois<=8 AND mois >=5)
)

UPDATE iav.t_lot_lot  SET lot_std_code= 'AGJ' FROM yellow WHERE  yellow.lot_identifiant = t_lot_lot.lot_identifiant;--375



-- Anguilles sans taille qui sont class�es comme IND  sont des AGJ
-- J'ai deux types d'effectifs
-- en 1996 il y des effectifs ENORMES
WITH eel AS (
SELECT
ope_date_debut, 
extract("month" FROM ope_date_debut) mois,
extract(YEAR FROM ope_date_debut) annee,
lot_identifiant,
lot_std_code,
lot_effectif,
car_valeur_quantitatif
FROM iav.t_operation_ope 
JOIN iav.t_lot_lot ON lot_ope_identifiant=ope_identifiant 
LEFT JOIN iav.tj_caracteristiquelot_car tcc ON tcc.car_lot_identifiant = lot_identifiant 
WHERE ope_dic_identifiant=5
AND lot_tax_code = '2038'
AND car_valeur_quantitatif IS NULL
),

yellow AS (
SELECT  * FROM eel
--WHERE (mois>8 OR mois <5)
--AND lot_effectif=-1
WHERE lot_std_code = 'IND'
)

--SELECT * FROM yellow
UPDATE iav.t_lot_lot  SET lot_std_code= 'AGJ' FROM yellow WHERE  yellow.lot_identifiant = t_lot_lot.lot_identifiant;--208

/*
-- traitement 1996, c'est bizarre c'est des gros effectifs mais il y a bien des suivis diff�rents sur la passe les m�mes jours...

WITH eel AS (
SELECT
ope_dic_identifiant,
ope_date_debut, 
extract("month" FROM ope_date_debut) mois,
extract(YEAR FROM ope_date_debut) annee,
lot_identifiant,
lot_std_code,
lot_effectif
FROM iav.t_operation_ope 
JOIN iav.t_lot_lot ON lot_ope_identifiant=ope_identifiant 
 WHERE ope_dic_identifiant IN (5,6)
AND lot_tax_code = '2038'
AND lot_std_code IN ('AGJ','IND')
ORDER BY ope_date_debut
)

SELECT * FROM eel 
WHERE annee = 1996

*/








