﻿
-- correction d'une erreur anguilles jaunes rentrées comme quantité de lot
-- on recherche d'abord les lots incriminés...
select * from iav.vue_lot_ope where lot_std_code='AGJ' and lot_quantite is not null and extract("year" from ope_date_debut)>2007 limit 100 



-- sauvegarde de la base
/*
E:\base>pg_dump -U postgres -f "iav_bd_contmig_nat_iav.sql" -h "w3.eptb-vilaine.fr" --schema iav --verbose bd_contmig_nat
*/
-- on remplace l'effectif par la quantite, l'un ou l'autre doit être nul !

BEGIN;
update iav.t_lot_lot set (lot_effectif,lot_quantite) = (sub.lot_quantite,NULL) from
(select lot_quantite, lot_identifiant from iav.vue_lot_ope where lot_std_code='AGJ' 
and lot_quantite is not null and extract("year" from ope_date_debut)>2007 ) sub
where sub.lot_identifiant=t_lot_lot.lot_identifiant;
COMMIT;

-- il reste un problème en 2006 qu'il faudra aller voir


select * from iav.t_lot_lot where lot_quantite is NULL and lot_qte_code is not NULL;

BEGIN;
update iav.t_lot_lot set lot_qte_code= NULL where lot_quantite is NULL and lot_qte_code is NOT NULL;
COMMIT;



SELECT * FROM iav.t_bilanmigrationmensuel_bme tbb WHERE bme_dis_identifiant=5 AND bme_tax_code='2038' AND bme_std_code='AGJ' ORDER BY bme_annee, bme_mois