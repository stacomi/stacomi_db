﻿/*
sommes_annuelles_pour_rapportage.sql
Script de travail en vue du rapportage 2012
Travail sur les bases stacomi et EDA
Auteur Cédric Briand IAV, Virginie Berger ONEMA, Laurent Beaulaton ONEMA
Les tables sont récupérées à partir de stacomi ou de copies de bilan annuels dans la table intermédiaire quand possible.
Les bilans annuels sont comparés aux sorties EDA.
*/


--#############################################"
-- TRAVAIL DE COMPILATION DU SCHEMA NATIONAL. 
--############################################"
-- NOTE CETTE ETAPE LONGUE ET FASTIDIEUSE REQUIERT LES MISES A JOUR DU TICKET 102 et la mise à jour du script compilation_nationale.sql car les fonctions ont été finalisées.
-- MAIS UN DUMP DU SCHEMA NATIONAL PEUT AUSSI FAIRE L'AFFAIRE

-- copie des tables interessantes dans le schema nat
-- attention ça prend un peu de temps.
select nat.nettoye(); -- vidange des tables nationales
select nat.compile('iav'); --890609 remplissage de toutes les tables iav dans le schéma nat, Oooooh  c'est beau !
select nat.compile('bgm'); --660812
select nat.compile('saumonrhin');--393353
select nat.compile('fd80'); --43
select nat.compile('mrm'); --6
select nat.compile('smatah');--72984
-- remplissage de seulement la table station pour les stations ci dessous (pour récupérer la carte des stations)
select nat.compiletable('azti','t_station_sta');--1
--select nat.compiletable('charente','t_station_sta'); rien
select nat.compiletable('inra','t_station_sta');--4
select nat.compiletable('logrami','t_station_sta');--18
delete from nat.t_station_sta where sta_nom='Uxondoa';
delete from nat.t_station_sta where sta_nom='Olha';
select nat.compiletable('MIGADO','t_station_sta');--7
select nat.compiletable('migradour','t_station_sta');--13
select nat.compiletable('charente','t_station_sta');--1

drop VIEW nat.vue_lot_ope;
CREATE OR REPLACE VIEW nat.vue_lot_ope AS 
 SELECT t_operation_ope.ope_identifiant, t_lot_lot.lot_identifiant, t_operation_ope.ope_dic_identifiant, t_lot_lot.lot_lot_identifiant AS lot_pere, t_operation_ope.ope_date_debut, t_operation_ope.ope_date_fin, t_lot_lot.lot_effectif, t_lot_lot.lot_quantite, 
 t_lot_lot.lot_tax_code, t_lot_lot.lot_std_code, tr_taxon_tax.tax_nom_latin, tr_stadedeveloppement_std.std_libelle, tr_devenirlot_dev.dev_code, tr_devenirlot_dev.dev_libelle, ope_org_code
   FROM nat.t_operation_ope
   JOIN nat.t_lot_lot ON (t_lot_lot.lot_ope_identifiant,t_lot_lot.lot_org_code) = (t_operation_ope.ope_identifiant,t_operation_ope.ope_org_code)
   LEFT JOIN ref.tr_typequantitelot_qte ON tr_typequantitelot_qte.qte_code::text = t_lot_lot.lot_qte_code::text
   LEFT JOIN ref.tr_devenirlot_dev ON tr_devenirlot_dev.dev_code::text = t_lot_lot.lot_dev_code::text
   JOIN ref.tr_taxon_tax ON tr_taxon_tax.tax_code::text = t_lot_lot.lot_tax_code::text
   JOIN ref.tr_stadedeveloppement_std ON tr_stadedeveloppement_std.std_code::text = t_lot_lot.lot_std_code::text
   ORDER BY t_operation_ope.ope_date_debut;

--*******************************
--Mise à jour des géométries
--****************************

-- examen des données
--select * from nat.t_station_sta where sta_coordonnee_y is not null and the_geom is null;
--select sta_coordonnee_x,sta_coordonnee_y from nat.t_station_sta where sta_coordonnee_y is not null;
-- ajout d'une colonne géométrique dans la table nat.t_station_sta
--SELECT AddGeometryColumn('nat', 't_station_sta','the_geom', 27572,'POINT',2);
update nat.t_station_sta set the_geom=st_transform(PointFromText('POINT(' || sta_coordonnee_x || ' ' || sta_coordonnee_y || ')',27572),2154); --37
update nat.t_station_sta set the_geom=PointFromText('POINT(' || sta_coordonnee_x || ' ' || sta_coordonnee_y || ')',2154) where sta_org_code='MIGADO';--7
update nat.t_station_sta set the_geom=st_transform(geomfromtext('POINT(' || -0.362552 || ' ' || 45.679864 || ')', 4326),2154) where sta_org_code='CHARENTE';
update nat.t_station_sta set the_geom=st_transform(PointFromText('POINT(-2.050632 43.262734)',4326),2154) where sta_code='oria'; -- 1 ici je suis allé sur google maps pour chercher les coordonnées
insert into nat.t_station_sta(sta_code, sta_nom, sta_org_code,the_geom) 
select 'Bresle','Bresle','ONEMA', st_transform(PointFromText('POINT(1.489071 50.023685)',4326),2154);

-- mise à jour des x et y en lambert 93

update nat.t_station_sta set sta_coordonnee_x=  round(st_x(the_geom));--51
update nat.t_station_sta set sta_coordonnee_y= round(st_y(the_geom));--51

-- pour récupérer la carte sous google

--alter table nat.t_station_sta add column googlemapcoord text;
update  nat.t_station_sta set googlemapcoord='http://maps.google.fr/maps?q='||st_y(st_transform(the_geom,4326))||','||st_x(st_transform(the_geom,4326));

--selection et sauvegarde des données 
select * from nat.t_station_sta;
set client_encoding to 'win1252';
COPY nat.t_station_sta to 'E:/IAV/stacomi/donnees_externes/tous/nat.t_station_sta.csv' with csv header delimiter as ';';

--###################################################
-- FIN DU TRAVAIL DE COMPILATION DU SCHEMA NATIONAL. 
--###################################################

--##############################################################################################################
-- CREATION D'UNE TABLE 'A LA MAIN' QUI CONTIENT LES IDENTIFIANTS DES STATIONS ET DES DISPOSITIFS DE COMPTAGE. 
--##############################################################################################################"
create schema pga;

-- CREATION D'UNE TABLE POUR LES DIFFERENTES STATIONS A INTEGRER
drop table if exists pga.sites;
create table pga.sites (
  pga_id serial,--une cle primaire
  pga_nom character varying (50),
  pga_nomshort character varying (4),
  pga_idnational integer, -- l'identifiant national dans la table de Virginie
  pga_sta_code character varying(8) , -- code de la station de contrôle
  pga_sta_nom character varying(40) , -- nom de la station
  pga_dic_dis_identifiant integer, -- identifiant du dispostif de comptage
  pga_org_code character varying (30), -- organime = schema
  pga_xl93 integer,
  pga_yl93 integer,
  the_geom geometry,
  CONSTRAINT pk_pga_id PRIMARY KEY (pga_id),
  CONSTRAINT uk_pga unique(pga_dic_dis_identifiant,pga_org_code),
  CONSTRAINT uk_pga_nomshort unique(pga_nomshort),  
  CONSTRAINT enforce_srid_the_geom CHECK (st_srid(the_geom) = 2154),
  CONSTRAINT enforce_dims_the_geom CHECK (ndims(the_geom) = 2),
  CONSTRAINT enforce_geotype_the_geom CHECK (geometrytype(the_geom) = 'POINT'::text OR the_geom IS NULL)
);
--REMPLISSAGE DE LA TABLE
insert into pga.sites (pga_nom, pga_nomshort,pga_idnational,pga_sta_code,pga_sta_nom,pga_dic_dis_identifiant, pga_org_code, pga_xl93,pga_yl93,the_geom) 
	values ('Passe piège principale Vilaine','vil1',NULL,'M4560001','Barrage d''Arzal',6,'IAV',NULL,NULL,NULL);--VILAINE LA GROSSE PASSE HISTORIQUE
insert into pga.sites (pga_nom, pga_nomshort,pga_idnational,pga_sta_code,pga_sta_nom,pga_dic_dis_identifiant, pga_org_code, pga_xl93,pga_yl93,the_geom) 
	values ('Passe piège secondaire Vilaine','vil2',NULL,'M4560001','Barrage d''Arzal',12,'IAV',NULL,NULL,NULL);--VILAINE LA PETITE PASSE DE 2007 EN RIVE DROITE
insert into pga.sites (pga_nom, pga_nomshort,pga_idnational,pga_sta_code,pga_sta_nom,pga_dic_dis_identifiant, pga_org_code, pga_xl93,pga_yl93,the_geom) 
	values ('Passe à bassins Vilaine','vil3',NULL,'M4560001','Barrage d''Arzal',5,'IAV',NULL,NULL,NULL);--VILAINE LA PASSE A BASSINS
insert into pga.sites (pga_nom, pga_nomshort,pga_idnational,pga_sta_code,pga_sta_nom,pga_dic_dis_identifiant, pga_org_code, pga_xl93,pga_yl93,the_geom) 
	values ('gambsheim video','rhi1',NULL,NULL,NULL,5,'SAUMONRHIN',NULL,NULL,NULL);--GAMBSHEIM
insert into pga.sites (pga_nom, pga_nomshort,pga_idnational,pga_sta_code,pga_sta_nom,pga_dic_dis_identifiant, pga_org_code, pga_xl93,pga_yl93,the_geom) 
	values ('Iffesheim video','rhi2',NULL,NULL,NULL,2,'SAUMONRHIN',NULL,NULL,NULL);--Iffesheim	
insert into pga.sites (pga_nom, pga_nomshort,pga_idnational,pga_sta_code,pga_sta_nom,pga_dic_dis_identifiant, pga_org_code, pga_xl93,pga_yl93,the_geom) 
	values ('Ascenseur du Bois JoliI','fre2',NULL,NULL,NULL,13,'BGM',NULL,NULL,NULL);--Frémur Bois Joli	
insert into pga.sites (pga_nom, pga_nomshort,pga_idnational,pga_sta_code,pga_sta_nom,pga_dic_dis_identifiant, pga_org_code, pga_xl93,pga_yl93,the_geom) 
	values ('Pont es Omnes montée','fre1',NULL,NULL,NULL,20,'BGM',NULL,NULL,NULL);--Frémur Pont es Omnes
insert into pga.sites (pga_nom, pga_nomshort,pga_idnational,pga_sta_code,pga_sta_nom,pga_dic_dis_identifiant, pga_org_code, pga_xl93,pga_yl93,the_geom) 
	values ('Pont es Omnes descente','fre3',NULL,NULL,NULL,21,'BGM',NULL,NULL,NULL);--Frémur Pont es Omnes
--mise à jour des données, j'utilise (pga_org_code,pga_dic_dis_identifiant) comme pivot pour identifier les lignes à remplir.
update pga.sites set (pga_sta_code,pga_sta_nom,pga_xl93,pga_yl93,the_geom) = (sub.sta_code,sub.sta_nom,sub.sta_coordonnee_x,sub.sta_coordonnee_y,sub.the_geom) FROM
(select sta_code,sta_nom,sta_coordonnee_x,sta_coordonnee_y,the_geom,sta_org_code,dic_dis_identifiant  from nat.t_station_sta 
	join nat.t_ouvrage_ouv on (ouv_sta_code,ouv_org_code)=(sta_code,sta_org_code)	
	join nat.t_dispositiffranchissement_dif on (dif_ouv_identifiant,dif_org_code)=(ouv_identifiant,ouv_org_code)
	join nat.t_dispositifcomptage_dic on (dic_dif_identifiant,dic_org_code)=(dif_dis_identifiant,dif_org_code)) as sub
where (sub.sta_org_code,sub.dic_dis_identifiant)=(pga_org_code,pga_dic_dis_identifiant	);
--select * from pga.sites;

--##########################################
--Creation des tables de bilan
--##########################################


drop table if exists pga.agj1;
create table pga.agj1 as(
select sum(lot_effectif), extract(year from ope_date_debut) as year,ope_org_code,ope_dic_identifiant from nat.vue_lot_ope 
where lot_tax_code='2038'
and lot_std_code='AGJ'
and lot_pere is null 
group by ope_org_code,ope_dic_identifiant,year
order by ope_org_code,ope_dic_identifiant,year);--121

drop table if exists pga.agg1;
create table pga.agg1 as(
select sum(lot_effectif), extract(year from ope_date_debut) as year,ope_org_code,ope_dic_identifiant from nat.vue_lot_ope 
where lot_tax_code='2038'
and lot_std_code='AGG'
and lot_pere is null 
group by ope_org_code,ope_dic_identifiant,year
order by ope_org_code,ope_dic_identifiant,year);--121

--select * from pga.agg1;



-- J'ai besoin d'un identifiant unique pour faciliter les jointures

alter table pga.agj1 add column pga_nomshort character varying (4);
update pga.agj1 set pga_nomshort=s.pga_nomshort
from pga.sites s
where(ope_org_code,ope_dic_identifiant)=(s.pga_org_code,s.pga_dic_dis_identifiant); --85


alter table pga.agg1 add column pga_nomshort character varying (4);
update pga.agg1 set pga_nomshort=s.pga_nomshort
from pga.sites s
where(ope_org_code,ope_dic_identifiant)=(s.pga_org_code,s.pga_dic_dis_identifiant); --85

-- je ne garde que les données qui sont dans la table pga.sites (il y des données dans la table qui correspondent à d'autres stations de contrôle avec des anguilles jaunes

delete from pga.agj1 where pga_nomshort is null;--36

-- select * from pga.agj1
--######################################## POUR VIRGINIE !#######################################
-- a ce stade il  faut ajouter les données manquantes à la table à l'aide d'une commande insert....
-- exemple bidon avec le Vaccares
-- insert into pga.agj1 (sum,year,ope_org_code,ope_dic_identifiant,pga_nomshort) value (12,2006,'MRM',NULL,'vac1')
--#################################################################################################"


-- CREATION D'UN TABLEAU CROISE
-- Il est nécessaire d'installer les contrib tablefunc (exécuter le script sql) pour utiliser la fonction crosstab ci dessous
-- pgsql/contrib/tablefunc/tablefunc.sql

-- Pour envoyer un résultat il faut avoir trois colonnes rowid, attribute, value
-- ici rowid c'est le nom de l'ouvrage, attribute le type de note, la valeur value

--######################################## POUR VIRGINIE !#######################################
-- ci dessous il faut ajouter les 'nouvelles station dans le AS de crosstab, par exemple vac1 pour le vaccarres
--#################################################################################################"

SELECT distinct on (pga_nomshort) pga_nomshort from pga.agj1;

drop table if exists pga.agj2;
create table pga.agj2 as(
SELECT * FROM crosstab('select  year,pga_nomshort,sum from pga.agj1 order by 1,2',
			        'SELECT distinct on (pga_nomshort) pga_nomshort from pga.agj1')
AS ct(year numeric, fre1 numeric, fre2 numeric,fre3 numeric,rhi1 numeric,rhi2 numeric,vil1 numeric,vil2 numeric,vil3 numeric));--18
 
--select * from pga.agj2
alter table pga.agj2 add column vil_ang numeric;
update pga.agj2 SET vil_ang=vil1+case when vil2 is null then 0 else vil2 end +case when vil3 is null then 0 else vil3 end;--18

-- select * from pga.agg1
delete from pga.agg1 where pga_nomshort is null;--20

drop table if exists pga.agg2;
create table pga.agg2 as(
SELECT * FROM crosstab('select  year,pga_nomshort,sum from pga.agg1 order by 1,2',
			        'SELECT distinct on (pga_nomshort) pga_nomshort from pga.agg1')
AS ct(year numeric,fre3 numeric,vil1 numeric,vil2 numeric));
-- select * from pga.agg2

--######################################## POUR VIRGINIE !#######################################
-- SCRIPTS VILAINE UTILISES POUR LE RAPPORTAGE POSE
--  A FINIR
--#################################################################################################
--######################################## POUR VIRGINIE !#######################################
-- requete faisant les somme des effectifs de civelles par an
-- attention cette requête suppose que les coefficients de conversion sont entrés journalièrement



-- effectif journaliers convertis Vilaine
drop table if exists pga.civ_vil;
create table pga.civ_vil as(
select case when ope_date_eff is not null then ope_date_eff
		else ope_date 
		end as date,
	case when effectif IS NOT null and effectif_from_poids is not null
			then effectif+poids
		when  effectif is null 
			then  poids
		else  effectif 
		end  as effectif_total,
	effectif,
	poids,
	effectif_from_poids,
	coe_valeur_coefficient
 from (
-- sous requete qui permet de récupérer les effectifs par jour
 SELECT cast(t_operation_ope.ope_date_debut as date) AS ope_date_eff,  
 sum(t_lot_lot.lot_effectif) as effectif
   FROM iav.t_operation_ope
   JOIN iav.t_lot_lot ON t_lot_lot.lot_ope_identifiant = t_operation_ope.ope_identifiant
      where ope_dic_identifiant in (6,5,12)
       and lot_tax_code='2038'
       and lot_std_code='CIV'
       and lot_effectif>0
    group by ope_date_eff
    order by ope_date_eff) As effectif_seulement
full outer join (
-- sous requete pour extraire les qte de lots par jour 
-- elle suppose que les valeurs de quantités de lots sont par jour
SELECT ope_date, round(poids*coe_valeur_coefficient) as effectif_from_poids, poids, coe_valeur_coefficient FROM (
 SELECT cast(t_operation_ope.ope_date_debut as date) AS ope_date,  
 sum(t_lot_lot.lot_quantite) as poids
   FROM iav.t_operation_ope
   JOIN iav.t_lot_lot ON t_lot_lot.lot_ope_identifiant = t_operation_ope.ope_identifiant
      where ope_dic_identifiant in (6,5,12)
       and lot_tax_code='2038'
       and lot_std_code='CIV'
    group by ope_date
  ORDER BY ope_date) AS qte
  left join (
	select   coe_date_debut, coe_valeur_coefficient from  iav.tj_coefficientconversion_coe
	where coe_qte_code='1'
	and coe_tax_code='2038'
	and coe_std_code='CIV') as coe
on coe_date_debut=ope_date
where poids is not null) as poids_seulement
on poids_seulement.ope_date=effectif_seulement.ope_date_eff);

--select * from pga.civ_vil;
-- Somme annuelle des effectifs
drop table if exists pga.civ_vi_an;
create table pga.civ_vil_an as(
select extract(year from date) as year, round(sum(effectif_total)) as effectif from pga.civ_vil group by year order by year);
delete from pga.civ_vil_an where year=2012 ; -- Celle là n'est pas complète



-- réinsertion dans la table pga_agj2

alter table pga.agj2 add column civ_vil numeric;
update pga.agj2 a set civ_vil=effectif from pga.civ_vil_an c where c.year=a.year;

-- select * from pga.agj2 order by year
--#############################################"
--#############################################"
-- Calcul des abondances EDA en amont des stations
--############################################"
--#############################################"
select * from pga.sites
-- dans postgres\share\contrib installer dblink



--small trial
SELECT *
FROM dblink('dbname=eda2.0_RHT password=postgres','select surface, abondance from rht.crosstab_rhtvs2 limit 10 ')
AS eda( surface numeric,abondance numeric);
-- Essai Vilaine
SELECT *
FROM dblink('dbname=eda2.0_RHT password=postgres','select sum(surface), sum(abondance) from rht.crosstab_rhtvs2 where id_drain in  (select rht.upstream_segments(212340)) ')
AS eda( surface numeric,abondance numeric); --7s 31.16 (surface) 1750939
select 1750939*0.05 --87546 OK


-- pas super je passe dans rht
-- sauvegarde de la table pga.sites et restauration

--Now running in eda2.0_RHT
--jointure spatiale

select distinct on (pga_nom) pga_nom, min(distance) as distance,id_drain from (
select pga_nom,id_drain as id_drain, st_distance(st_transform(s.the_geom,3035),r.the_geom) as distance from pga.sites s
-- as distance
join rht.rhtvs2 r on st_dwithin (st_transform(s.the_geom,3035),r.the_geom, 1000)) sub
group by pga_nom, distance,id_drain


select sum(surface), sum(abondance) from rht.crosstab_rhtvs2 where id_drain in  (select rht.upstream_segments(212340));
select sum(surface), sum(abondance)*0.05 from rht.crosstab_rhtvs2 where id_drain in  (select rht.upstream_segments(200870));-- 0.03 2372
select sum(surface), sum(abondance) from rht.crosstab_rhtvs2 where id_drain in  (select rht.upstream_segments(503204));--gamb 19.13 18665.7
select sum(surface), sum(abondance) from rht.crosstab_rhtvs2 where id_drain in  (select rht.upstream_segments(502548));--iff 5.01 5514.67
# frémur
select sum(surface), sum(abondance)*0.05,sum(abondance) from rht.crosstab_rhtvs2 where id_drain in  (select rht.upstream_segments(200870))
union
select sum(surface), sum(abondance)*0.05,sum(abondance) from rht.crosstab_rhtvs2 where id_drain =200870
--###############
-- calculs finaux
--################
select * from pga.agj2;
select * from pga.agg1;
select * from pga.sites;
select sum(vil_ang+civ_vil) from pga.agj2 where year<=2009 and year>=1997;
select avg(vil_ang+civ_vil) from pga.agj2 where year<=1999 and year>=1995; --404731
select cast(1750939 as numeric)/cast (2145061 as numeric); -- 81%
select 87870.0/404731.0  --21%  --EDA surestime
select 206.0/2372
select avg(rhi1)*0.05 from pga.agj2; -- les effectis ressemblent
select avg(rhi2) from pga.agj2;--3357