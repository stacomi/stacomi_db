--DROP table ts_sequence_seq;
create table ts_sequence_seq (
seq_sequence varchar(60),
seq_table varchar(40),
seq_column varchar(40),
CONSTRAINT c_seq_sequence PRIMARY KEY (seq_sequence));

insert into ts_sequence_seq values ('t_lot_lot_lot_identifiant_seq','t_lot_lot','lot_lot_identifiant');
insert into ts_sequence_seq values ('t_operation_ope_ope_identifiant_seq','t_operation_ope','ope_identifiant');
insert into ts_sequence_seq values ('t_ouvrage_ouv_ouv_identifiant_seq','t_ouvrage_ouv','ouv_identifiant_seq');
insert into ts_sequence_seq values ('tg_dispositif_dis_dis_identifiant_seq','tg_dispositif_dis','dis_identifiant_seq');
insert into ts_sequence_seq values ('tr_valeurparametrequalitatif_val_val_identifiant_seq','tr_valeurparametrequalitatif_val',
					'val_identifiant_seq');