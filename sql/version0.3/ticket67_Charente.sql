﻿--Ticket 67 script update one organism

alter table charente.tj_prelevementlot_prl  drop CONSTRAINT c_pk_prl;
alter table charente.tj_prelevementlot_prl add prl_org_code character varying(30) NOT NULL;
alter table charente.tj_prelevementlot_prl add constraint c_pk_prl PRIMARY KEY (prl_pre_typeprelevement, prl_lot_identifiant, prl_code,prl_org_code);
ALTER TABLE charente.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_org_code FOREIGN KEY (prl_org_code) REFERENCES ref.ts_organisme_org(org_code);
-- si des données sont déjà présentes
/*
alter table charente.tj_prelevementlot_prl  drop CONSTRAINT c_pk_prl;
alter table charente.tj_prelevementlot_prl add prl_org_code character varying(30);
update charente.tj_prelevementlot_prl set prl_org_code='charente';
alter table charente.tj_prelevementlot_prl ADD constraint c_nn_prl_org_code check (prl_org_code IS NOT NULL);
alter table charente.tj_prelevementlot_prl add constraint c_pk_prl PRIMARY KEY (prl_pre_typeprelevement, prl_lot_identifiant, prl_code,prl_org_code);
ALTER TABLE charente.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_org_code FOREIGN KEY (prl_org_code) REFERENCES ref.ts_organisme_org(org_code);
*/

alter table charente.tj_coefficientconversion_coe  drop CONSTRAINT c_pk_coe;
alter table charente.tj_coefficientconversion_coe add coe_org_code character varying(30) NOT NULL;
alter table charente.tj_coefficientconversion_coe add constraint c_pk_coe PRIMARY KEY (coe_tax_code, coe_std_code, coe_qte_code, coe_date_debut, coe_date_fin,coe_org_code);
ALTER TABLE charente.tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code) REFERENCES ref.ts_organisme_org(org_code);
-- si des données sont déjà présentes
/*
alter table charente.tj_coefficientconversion_coe  drop CONSTRAINT c_pk_coe;
alter table charente.tj_coefficientconversion_coe add coe_org_code character varying(30);
update charente.tj_coefficientconversion_coe set coe_org_code='charente';
alter table charente.tj_coefficientconversion_coe ADD constraint c_nn_coe_org_code check (coe_org_code IS NOT NULL);
alter table charente.tj_coefficientconversion_coe add constraint c_pk_coe PRIMARY KEY (coe_tax_code, coe_std_code, coe_qte_code, coe_date_debut, coe_date_fin,coe_org_code);
ALTER TABLE charente.tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code) REFERENCES ref.ts_organisme_org(org_code);
*/

alter table charente.t_operationmarquage_omq  drop CONSTRAINT c_pk_omg CASCADE;
alter table charente.t_operationmarquage_omq add omq_org_code character varying(30) NOT NULL;
alter table charente.t_operationmarquage_omq ADD CONSTRAINT c_pk_omq PRIMARY KEY (omq_reference,omq_org_code);
ALTER TABLE charente.t_operationmarquage_omq ADD CONSTRAINT c_fk_omq_org_code FOREIGN KEY (omq_org_code) REFERENCES ref.ts_organisme_org(org_code);
ALTER TABLE charente.t_marque_mqe ADD CONSTRAINT c_fk_mqe_omq_reference FOREIGN KEY (mqe_omq_reference,mqe_org_code)
      REFERENCES charente.t_operationmarquage_omq (omq_reference,omq_org_code) ;

-- mise à jour de la table de maintenance
INSERT INTO charente.ts_maintenance_main( main_ticket,main_description ) VALUES (67,'org code rajouté dans les tables t_operationmarquage_omq, tj_coefficientconversion_coe,tj_prelevementlot_prl');

