CREATE TABLE iav.ts_maintenance_main(
main_identifiant serial PRIMARY KEY,
main_ticket integer,
main_description text);
COMMENT ON  TABLE iav.ts_maintenance_main IS 'Table de suivi des operations de maintenance de la base';

INSERT INTO iav.ts_maintenance_main( main_ticket,main_description ) VALUES (59,'creation de la table de maintenance');
INSERT INTO iav.ts_maintenance_main( main_ticket,main_description ) VALUES (40,'ajout des cl� �trang�res manquantes');
INSERT INTO iav.ts_maintenance_main( main_ticket,main_description ) VALUES (42,'modification des propri�taires sur les tables � s�quence et grant select sur ref.tr_typedf_tdf oubli�');
INSERT INTO iav.ts_maintenance_main( main_ticket,main_description ) VALUES (67,'org code rajout� dans les tables t_operationmarquage_omq, tj_coefficientconversion_coe,tj_prelevementlot_prl');
