﻿/*
select distinct on (msr_element) msr_element from ref.ts_messageR_msr;
select msr_element, msr_number,msr_endofline,mrl_text from  ref.ts_messageR_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id 
	where msr_element ='BilanMigration'
	and mrl_lang='French';
*/
/* ? pas retrouvé
insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) values (335,'BilanMigration',10,'class',TRUE,'program indication');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (335,'Graphs','English');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (335,'Graphs','Spanish');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (335,'Graphs','French');
*/

select msr_element, msr_number,msr_endofline,mrl_text from  ref.ts_messageR_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id 
	where msr_element ='fungraph'
	and mrl_lang='Spanish';

insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) values (336,'fungraph',8,'class',TRUE,'pasted output for numbers');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (336,'Jours','French');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (336,'Days','English');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (336,'Dias','Spanish');

insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) values (333,'BilanMigration',10,'class',TRUE,'developpement version 0.3');
insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) values (334,'BilanMigrationInterannuelle',11,'class',FALSE,'developpement version 0.3');

insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (333,'"Il n''y a pas d''effectif pour le taxon, le stade, et la période sélectionnée"','French');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (334,'"Ecriture du bilanMigrationInterannuelle dans l''environnement envir_stacomi : ecrire bmi=get(''bilanMigrationInterannuelle'',envir_stacomi) "','French');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (333,'"There are no values for the taxa, stage and selected period"','English');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (334,'"Writing bilanMigrationInterannuelle in the environment envir_stacomi : write bmi=get(''bilanMigrationInterannuelle'',envir_stacomi) "','English');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (333,'"please translate this : Il n''y a pas d''effectif pour le taxon, le stade, et la période sélectionnée"','Spanish');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (334,'"please translate this : Writing bilanMigrationInterannuelle in the environment envir_stacomi : write bmi=get(''bilanMigrationInterannuelle'',envir_stacomi) "','Spanish');

/*
delete from ref.ts_messagerlang_mrl where mrl_msr_id='334';
delete from ref.ts_messager_msr where msr_id='334';
*/
