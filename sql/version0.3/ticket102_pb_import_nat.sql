﻿alter table iav.t_operationmarquage_omq add column omq_org_code character varying(30) NOT NULL DEFAULT 'IAV';
alter table iav.t_operationmarquage_omq ADD CONSTRAINT c_fk_omq_org_code FOREIGN KEY (omq_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
alter table nat.t_dispositiffranchissement_dif DROP  CONSTRAINT c_pk_dif;
alter table nat.t_dispositiffranchissement_dif ADD  CONSTRAINT c_pk_dif PRIMARY KEY (dif_dis_identifiant, dif_org_code);
alter table nat.t_ouvrage_ouv drop constraint c_ck_ouv_coordonnee_x;
alter table nat.t_operation_ope DROP CONSTRAINT c_uq_ope ;
alter table nat.t_operation_ope ADD CONSTRAINT c_uq_ope UNIQUE (ope_dic_identifiant, ope_date_debut, ope_date_fin,ope_org_code);
alter table nat.t_station_sta drop constraint c_ck_sta_coordonnee_y;
alter table nat.t_station_sta drop constraint c_ck_sta_coordonnee_x;
update fd80.t_station_sta set (sta_coordonnee_x, sta_coordonnee_y)=(563661,2567558) where sta_code='M4800002';
update fd80.t_station_sta set (sta_coordonnee_x, sta_coordonnee_y)=( 632479,2549330) where sta_code='M4800003';

