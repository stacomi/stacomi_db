DROP TABLE IF EXISTS temp_operation;
CREATE TEMP TABLE  temp_operation (
  ope_identifiant serial NOT NULL,
  ope_dic_identifiant integer NOT NULL,
  ope_date_debut timestamp(0) without time zone NOT NULL,
  ope_date_fin timestamp(0) without time zone NOT NULL,
  ope_organisme character varying(35),
  ope_operateur character varying(35),
  ope_commentaires text,
  CONSTRAINT c_pk_ope PRIMARY KEY (ope_identifiant));

   SELECT * FROM temp_operation where (ope_date_debut,ope_date_fin) OVERLAPS (ope_date_debut,ope_date_fin);

SET CLIENT_ENCODING TO 'WIN1252';
 COPY temp_operation FROM 'C:/base/t_operation_ope_MONT2.csv' USING DELIMITERS ';' WITH CSV HEADER NULL AS '';


--DROP FUNCTION chercheoverlaps()
CREATE OR REPLACE FUNCTION chercheoverlaps() RETURNS SETOF text AS
$BODY$
DECLARE
    nbChevauchements 	INTEGER    ;
    idope               INTEGER ;
    r 			temp_operation%rowtype;
    resultat           text;
BEGIN
    FOR r IN SELECT * FROM temp_operation
    LOOP
        SELECT COUNT(*) INTO nbChevauchements
 	 	FROM   temp_operation
 	 	WHERE  (ope_date_debut, ope_date_fin) OVERLAPS (r.ope_date_debut, r.ope_date_fin);
		IF (nbChevauchements > 1) THEN
		resultat=r.ope_identifiant;
		-- je vais chercher la deuxi�me op�ration incrimin�e
		SELECT ope_identifiant INTO idope
 	 	        FROM   temp_operation
 	 	       WHERE  (ope_date_debut, ope_date_fin) OVERLAPS (r.ope_date_debut, r.ope_date_fin)
 	 	       AND ope_identifiant<>r.ope_identifiant;
		-- Il existe un chevauchement		
 	 	RAISE NOTICE 'Les dates se chevauchent : premiere operation --->%',resultat;
 	 	RAISE NOTICE 'deuxi�me op�ration --->%',idope;
 	 	END IF  ;
 	 RETURN NEXT r;         
    END LOOP;
    RETURN;
END;
$BODY$
LANGUAGE 'plpgsql' ;



SELECT COUNT(*) FROM chercheoverlaps(); 





