/* 
script de correction des cl� �trang�res manquantes
correspond au ticket 40
c_fk_dic_dis_identifiant
c_fk_dic_dif_identifiant
c_fk_dif_ouv_identifiant
c_fk_dif_dis_identifiant
c_fk_dtx_dif_identifiant
c_fk_dft_df_identifiant
c_fk_prl_lot_identifiant
c_fk_tav_dic_identifiant
30 avril 2010
 */


--c_fk_dic_dis_identifiant
Alter table iav.t_dispositifcomptage_dic ADD CONSTRAINT c_fk_dic_dis_identifiant 
	FOREIGN KEY (dic_dis_identifiant,dic_org_code) 
	REFERENCES iav.tg_dispositif_dis (dis_identifiant,dis_org_code);
-- la contrainte de table est construite sur le code alors qu'elle devrait l'�tre sur l'identifiant
-- je la supprime	
Alter table iav.t_dispositiffranchissement_dif DROP CONSTRAINT c_pk_dif ;
-- je la recr�e avec les bonnes valeurs
Alter table iav.t_dispositiffranchissement_dif ADD CONSTRAINT c_pk_dif PRIMARY KEY (dif_dis_identifiant, dif_org_code);
-- pareil pour les dispositifs de comptage
Alter table iav.t_dispositifcomptage_dic DROP CONSTRAINT c_pk_dic ;
-- 
Alter table iav.t_dispositifcomptage_dic ADD CONSTRAINT c_pk_dic PRIMARY KEY (dic_dis_identifiant, dic_org_code);

--c_fk_dic_dif_identifiant     		
Alter table iav.t_dispositifcomptage_dic ADD CONSTRAINT c_fk_dic_dif_identifiant 
	FOREIGN KEY (dic_dif_identifiant,dic_org_code) 
	REFERENCES iav.t_dispositiffranchissement_dif (dif_dis_identifiant,dif_org_code);
--c_fk_dif_ouv_identifiant 
Alter table iav.t_dispositiffranchissement_dif ADD CONSTRAINT c_fk_dif_ouv_identifiant 
	FOREIGN KEY (dif_ouv_identifiant,dif_org_code) 
	REFERENCES iav.t_ouvrage_ouv (ouv_identifiant,ouv_org_code);
-- c_fk_dif_dis_identifiant	
Alter table iav.t_dispositiffranchissement_dif ADD CONSTRAINT c_fk_dif_dis_identifiant	
	FOREIGN KEY (dif_dis_identifiant,dif_org_code) 
	REFERENCES iav.tg_dispositif_dis (dis_identifiant,dis_org_code);
-- c_fk_dtx_dif_identifiant
Alter table iav.tj_dfestdestinea_dtx ADD CONSTRAINT c_fk_dtx_dif_identifiant	
	FOREIGN KEY (dtx_dif_identifiant,dtx_org_code) 
	REFERENCES iav.tg_dispositif_dis (dis_identifiant,dis_org_code);
--c_fk_dft_df_identifiant
Alter table iav.tj_dfesttype_dft ADD CONSTRAINT c_fk_dft_df_identifiant	
	FOREIGN KEY (dft_df_identifiant,dft_org_code) 
	REFERENCES iav.tg_dispositif_dis (dis_identifiant,dis_org_code);
--c_fk_prl_lot_identifiant
-- pour la table des pr�l�vements il manque les organismes il faut faire les modifs qui suivent
Alter table iav.tj_prelevementlot_prl add column prl_org_code character varying(30) NOT NULL;
Alter table iav.tj_prelevementlot_prl DROP CONSTRAINT c_pk_prl;
Alter table iav.tj_prelevementlot_prl ADD CONSTRAINT c_pk_prl PRIMARY KEY (prl_pre_typeprelevement, prl_lot_identifiant, prl_code,prl_org_code);
Alter table iav.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_lot_identifiant	
	FOREIGN KEY (prl_lot_identifiant,prl_org_code) 
	REFERENCES iav.t_lot_lot (lot_identifiant,lot_org_code);
--Le nom ci dessous c'est pas terrible cht de c_fk_prl_pre_nom a c_fk_prl_pre_typeprelevement;
ALTER TABLE iav.tj_prelevementlot_prl DROP CONSTRAINT c_fk_prl_pre_nom;
ALTER TABLE iav.tj_prelevementlot_prl ADD CONSTRAINT 
 c_fk_prl_typeprelevement FOREIGN KEY (prl_pre_typeprelevement)
      REFERENCES ref.tr_prelevement_pre (pre_typeprelevement) MATCH SIMPLE;

-- c_fk_tav_dic_identifiant
Alter table iav.ts_taillevideo_tav ADD CONSTRAINT c_fk_tav_dic_identifiant	
	FOREIGN KEY (tav_dic_identifiant,tav_org_code) 
	REFERENCES iav.t_dispositifcomptage_dic (dic_dis_identifiant,dic_org_code);
     
	 
	 