--DROP TABLE ts_taxonvideo_txv CASCADE

CREATE TABLE ts_taxonvideo_txv
(
  txv_code character varying(3) NOT NULL,
  txv_tax_code character varying(6) ,
  txv_std_code character varying(4) ,
  CONSTRAINT c_pk_txv PRIMARY KEY (txv_code),
  CONSTRAINT c_fk_txv_tax_code FOREIGN KEY (txv_tax_code)
      REFERENCES tr_taxon_tax (tax_code) MATCH SIMPLE
       ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT c_fk_std_code FOREIGN KEY (txv_std_code)
      REFERENCES tr_stadedeveloppement_std (std_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (OIDS=TRUE);
ALTER TABLE ts_taxonvideo_txv OWNER TO postgres;

-- Index: i_txv_code

-- DROP INDEX i_txv_code;

CREATE INDEX i_txv_code
  ON ts_taxonvideo_txv
  USING btree
  (txv_code);

  
COPY ts_taxonvideo_txv 
--FROM 'C:/Documents%20and%20Settings/cedric/Mes%20documents/Migrateur/programmes/workspace/stacomi/docs/ts_taxonvideo_txv' 
--FROM 'C:/temp/correspondance_taxon.csv'
FROM 'd:/PostgresDataStore/bd_contmig/correspondance_taxon.csv'
WITH DELIMITER ';'
     NULL AS ''
     CSV HEADER
    FORCE NOT NULL  txv_code;

DROP VIEW IF EXISTS v_taxon_tax;
create view v_taxon_tax AS

SELECT * FROM tr_taxon_tax tax 
RIGHT JOIN ts_taxonvideo_txv txv ON tax.tax_code=txv.txv_tax_code
LEFT JOIN tr_stadedeveloppement_std std ON txv.txv_std_code=std.std_code;