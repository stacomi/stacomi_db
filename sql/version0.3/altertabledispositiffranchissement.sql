/*

Ce fichier est destin� � cr�er la connection avec les types de dispositifs oubli�e par Sam
Lors de sa programmation

*/

alter table t_dispositiffranchissement_dif add column dif_tdf_code character varying(4);
alter table t_dispositiffranchissement_dif ADD CONSTRAINT c_fk_dif_tdf_code FOREIGN KEY (dif_tdf_code) REFERENCES tr_typedf_tdf (tdf_code) MATCH SIMPLE;
--select * from t_dispositiffranchissement_dif order by dif_dis_identifiant;
Update t_dispositiffranchissement_dif set dif_tdf_code= 1 where dif_dis_identifiant =1; -- arzal = passe � bassins
Update t_dispositiffranchissement_dif set dif_tdf_code= 7 where dif_dis_identifiant >1; -- Les autres sont des passes � anguille


alter table t_dispositiffranchissement_dif drop column dif_tdf_code ;