﻿--ticket #68
/*
select * from iav.t_bilanmigrationmensuel_bme where
bme_dis_identifiant = 6 and
bme_std_code = 'AGJ' and
bme_annee = 2010
order by bme_annee, bme_mois,bme_labelquantite;
*/
-- utilisation de la fonction ref.updatesql du ticket 81
select ref.updatesql('{"azti","bgm","charente","fd80","iav","inra","logrami","migado","migradour","mrm","saumonrhin","smatah"}','delete from t_bilanmigrationmensuel_bme;');
select ref.updatesql('{"azti","bgm","charente","fd80","iav","inra","logrami","migado","migradour","mrm","saumonrhin","smatah"}','alter table t_bilanmigrationmensuel_bme add constraint c_uk_t_bilanmigrationmensuel_bme unique(bme_dis_identifiant,bme_tax_code,bme_std_code,bme_annee,bme_mois,bme_labelquantite);');
select ref.updatesql('{"azti","bgm","charente","fd80","iav","inra","logrami","migado","migradour","mrm","saumonrhin","smatah"}',
'INSERT INTO ts_maintenance_main( main_ticket,main_description) VALUES (68,''ajout d`une contrainte pour ne pas écrire plusieurs bilan migration mensuels'');')