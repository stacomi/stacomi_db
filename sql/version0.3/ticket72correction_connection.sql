--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = ref, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ts_messager_msr; Type: TABLE; Schema: ref; Owner: postgres; Tablespace: 
--

CREATE TABLE ts_messager_msr (
    msr_id integer NOT NULL,
    msr_element character varying(50),
    msr_number character varying(5),
    msr_type character varying(20),
    msr_endofline boolean,
    msr_comment text
);


ALTER TABLE ref.ts_messager_msr OWNER TO postgres;

--
-- Name: TABLE ts_messager_msr; Type: COMMENT; Schema: ref; Owner: postgres
--

COMMENT ON TABLE ts_messager_msr IS 'this table contains the name and descriptions the string msr_element.msr_number which is used in t_messagelang_mrl 
to put strings for each langage, it is separated as the comment and the place in the program applies only once';


--
-- Name: COLUMN ts_messager_msr.msr_endofline; Type: COMMENT; Schema: ref; Owner: postgres
--

COMMENT ON COLUMN ts_messager_msr.msr_endofline IS 'will the string be terminated by antislash+n';


--
-- Name: ts_messager_msr_msr_id_seq; Type: SEQUENCE; Schema: ref; Owner: postgres
--

CREATE SEQUENCE ts_messager_msr_msr_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ref.ts_messager_msr_msr_id_seq OWNER TO postgres;

--
-- Name: ts_messager_msr_msr_id_seq; Type: SEQUENCE OWNED BY; Schema: ref; Owner: postgres
--

ALTER SEQUENCE ts_messager_msr_msr_id_seq OWNED BY ts_messager_msr.msr_id;


--
-- Name: ts_messager_msr_msr_id_seq; Type: SEQUENCE SET; Schema: ref; Owner: postgres
--

SELECT pg_catalog.setval('ts_messager_msr_msr_id_seq', 1, true);


--
-- Name: ts_messagerlang_mrl; Type: TABLE; Schema: ref; Owner: postgres; Tablespace: 
--

CREATE TABLE ts_messagerlang_mrl (
    mrl_id integer NOT NULL,
    mrl_msr_id integer,
    mrl_text text,
    mrl_lang character varying(10),
    CONSTRAINT c_ck_mrl_lang CHECK (((((mrl_lang)::text = 'French'::text) OR ((mrl_lang)::text = 'English'::text)) OR ((mrl_lang)::text = 'Spanish'::text)))
);


ALTER TABLE ref.ts_messagerlang_mrl OWNER TO postgres;

--
-- Name: TABLE ts_messagerlang_mrl; Type: COMMENT; Schema: ref; Owner: postgres
--

COMMENT ON TABLE ts_messagerlang_mrl IS 'this table contains the string of code to appear in the graphical R interface, one for each langage';


--
-- Name: ts_messagerlang_mrl_mrl_id_seq; Type: SEQUENCE; Schema: ref; Owner: postgres
--

CREATE SEQUENCE ts_messagerlang_mrl_mrl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ref.ts_messagerlang_mrl_mrl_id_seq OWNER TO postgres;

--
-- Name: ts_messagerlang_mrl_mrl_id_seq; Type: SEQUENCE OWNED BY; Schema: ref; Owner: postgres
--

ALTER SEQUENCE ts_messagerlang_mrl_mrl_id_seq OWNED BY ts_messagerlang_mrl.mrl_id;


--
-- Name: ts_messagerlang_mrl_mrl_id_seq; Type: SEQUENCE SET; Schema: ref; Owner: postgres
--

SELECT pg_catalog.setval('ts_messagerlang_mrl_mrl_id_seq', 1018, true);


--
-- Name: msr_id; Type: DEFAULT; Schema: ref; Owner: postgres
--

ALTER TABLE ONLY ts_messager_msr ALTER COLUMN msr_id SET DEFAULT nextval('ts_messager_msr_msr_id_seq'::regclass);


--
-- Name: mrl_id; Type: DEFAULT; Schema: ref; Owner: postgres
--

ALTER TABLE ONLY ts_messagerlang_mrl ALTER COLUMN mrl_id SET DEFAULT nextval('ts_messagerlang_mrl_mrl_id_seq'::regclass);


--
-- Data for Name: ts_messager_msr; Type: TABLE DATA; Schema: ref; Owner: postgres
--

COPY ts_messager_msr (msr_id, msr_element, msr_number, msr_type, msr_endofline, msr_comment) FROM stdin;
109	BilanMigrationInterannuelle	1	class	t	\N
333	BilanMigration	10	class	t	\N
334	BilanMigrationInterannuelle	11	class	f	\N
336	fungraph	8	class	t	pasted output for numbers
230	interface_Bilan_poids_moyen	1	interface	f	glabel of title bilan poids moyen
343	funtraitement_poids	3	function	t	warning for incomplete coefficient input
18	Bilan_poids_moyen	2	class	t	 context nrows found for conversion coefficients
20	Bilan_poids_moyen	4	class	f	 at the start of a warning
21	Bilan_poids_moyen	5	class	f	 context weight of undrained glass eels
23	Bilan_poids_moyen	7	class	f	 all weights both cases selected
24	Bilan_poids_moyen	8	class	f	graphs
25	Bilan_poids_moyen	9	class	f	graphs
26	Bilan_poids_moyen	10	class	f	graphs, within a paste
27	Bilan_poids_moyen	11	class	f	graphs
28	Bilan_poids_moyen	12	class	f	graphs
29	Bilan_poids_moyen	13	class	f	graphs
32	Bilan_poids_moyen	16	class	f	frame question
33	Bilan_poids_moyen	17	class	f	frame title
34	Bilan_poids_moyen	18	class	f	button message for graphes
35	Bilan_poids_moyen	19	class	f	button message for coefficients
36	Bilan_poids_moyen	20	class	f	button message for size (bubbles of different size according to the numbers
37	Bilan_poids_moyen	21	class	f	button message for regression
38	Bilan_poids_moyen	22	class	f	button message to export the data
39	Bilan_poids_moyen	23	class	f	button message button to quit
42	Bilan_stades_pigm	3	class	f	 glablel
68	BilanFonctionnementDC	3	class	f	graph xlabel
69	BilanFonctionnementDC	4	class	f	graph ylabel
70	BilanFonctionnementDC	5	class	f	graph title
71	BilanFonctionnementDC	6	class	f	graph legend
72	BilanFonctionnementDC	7	class	f	graph legend
74	BilanFonctionnementDC	9	class	f	graph boxes ylab
75	BilanFonctionnementDC	10	class	f	graph boxes legend
76	BilanFonctionnementDC	11	class	f	graph boxes legend
77	BilanFonctionnementDC	12	class	f	graph boxes upper box title
78	BilanFonctionnementDC	13	class	f	graph boxes lower box title
79	BilanFonctionnementDC	14	class	f	… path …
84	BilanFonctionnementDF	4	class	f	 progressbar
85	BilanFonctionnementDF	5	class	f	 progressbar
110	BilanMigrationInterannuelle	2	class	f	 part of a message
112	BilanMigrationInterannuelle	4	class	f	graph : pasted within the title
114	BilanMigrationInterannuelle	6	class	f	graph cum group
115	BilanMigrationInterannuelle	7	class	f	graph cum X
116	BilanMigrationInterannuelle	8	class	f	graph cum Y
1	ref	1	referential	t	\N
117	BilanMigrationInterannuelle	9	class	f	graph cum within title
342	interface_graphique	21	interface	t	message de lancement du handler des migrations multiples
125	BilanMigrationPar	7	class	f	 path
150	RefDC	4	class	f	 button
151	RefDC	5	class	f	 title
163	Refparquan	1	class	f	TODO check this class
164	Refperiode	1	class	f	TODO check this class
165	RefpoidsMoyenPeche	1	class	f	TODO check this class
168	RefStades	3	class	f	 this is a frame label
169	RefStades	4	class	f	 this is a frame label
171	RefStades	6	class	f	 this is a frame label
174	RefStationMesure	3	class	f	 frame label
177	RefTaxon	2	class	f	 frame label
191	fungraph_civelle	1	function	f	 followed by dis_commentaire
192	fungraph_civelle	2	function	f	 title
215	fungraph	7	function	f	 column names
222	interface_Bilan_lot	2	interface	f	 glablel
227	interface_Bilan_lot	7	interface	f	 tooltip
228	interface_Bilan_lot	8	interface	f	 tooltip
229	interface_Bilan_lot	9	interface	f	 tootip and name
237	interface_Bilan_taille	2	interface	f	 tooltip
239	interface_BilanConditionEnv	2	interface	f	 tootip
243	interface_BilanEspeces	2	interface	f	 glabel title
248	interface_BilanEspeces	7	interface	f	 label de la boite liste
250	interface_BilanMigration	2	interface	f	 tooltip
254	interface_BilanMigrationConditionEnv	1	interface	f	 
261	interface_BilanMigrationInterannuelle	6	interface	f	 tooltip
262	interface_BilanMigrationInterannuelle	7	interface	f	 tooltip
263	interface_BilanMigrationInterannuelle	8	interface	f	 tooltip
264	interface_BilanMigrationInterannuelle	9	interface	f	 tooltip
281	interface_BilanFonctionnementDC	2	interface	f	 tootip
300	interface_graphique	16	interface	f	 TITRE PRINCIPAL !!!
2	ref	2	referential	t	\N
3	ref	3	referential	t	\N
4	ref	4	referential	t	\N
5	ref	5	referential	t	\N
6	ref	6	referential	t	\N
7	ref	7	referential	t	\N
8	ref	8	referential	t	\N
9	ref	9	referential	t	\N
10	ref	10	referential	t	\N
11	ref	11	referential	t	\N
12	ref	12	referential	t	\N
13	Bilan_lot	1	class	t	\N
14	Bilan_lot	2	class	t	\N
15	Bilan_lot	3	class	t	\N
16	Bilan_lot	4	class	t	\N
17	Bilan_poids_moyen	1	class	t	\N
19	Bilan_poids_moyen	3	class	t	\N
22	Bilan_poids_moyen	6	class	f	\N
30	Bilan_poids_moyen	14	class	t	\N
31	Bilan_poids_moyen	15	class	f	\N
40	Bilan_stades_pigm	1	class	t	\N
41	Bilan_stades_pigm	2	class	t	\N
43	Bilan_stades_pigm	4	class	f	\N
44	Bilan_stades_pigm	5	class	f	\N
45	Bilan_stades_pigm	6	class	f	\N
46	Bilan_taille	1	class	t	\N
47	Bilan_taille	2	class	t	\N
48	Bilan_taille	3	class	t	\N
49	Bilan_taille	4	class	t	\N
50	Bilan_taille	5	class	t	\N
51	Bilan_taille	6	class	t	\N
52	Bilan_taille	7	class	t	\N
53	Bilan_taille	8	class	t	\N
54	BilanCondtionEnv	1	class	t	\N
55	BilanCondtionEnv	2	class	t	\N
56	BilanCondtionEnv	3	class	t	\N
57	BilanCondtionEnv	4	class	t	\N
58	BilanCondtionEnv	5	class	t	\N
59	BilanEspeces	1	class	t	\N
60	BilanEspeces	2	class	t	\N
61	BilanEspeces	3	class	t	\N
62	BilanEspeces	4	class	t	\N
63	BilanEspeces	5	class	t	\N
64	BilanEspeces	6	class	t	\N
65	BilanEspeces	7	class	t	\N
66	BilanFonctionnementDC	1	class	t	\N
67	BilanFonctionnementDC	2	class	t	\N
73	BilanFonctionnementDC	8	class	t	\N
80	BilanFonctionnementDC	15	class	t	\N
81	BilanFonctionnementDF	1	class	t	\N
82	BilanFonctionnementDF	2	class	t	\N
83	BilanFonctionnementDF	3	class	t	\N
86	BilanFonctionnementDF	6	class	f	\N
87	BilanFonctionnementDF	7	class	f	\N
88	BilanFonctionnementDF	8	class	t	\N
89	BilanFonctionnementDF	9	class	f	\N
90	BilanFonctionnementDF	10	class	f	\N
91	BilanMigration	1	class	t	\N
92	BilanMigration	2	class	t	\N
93	BilanMigration	3	class	t	\N
94	BilanMigration	4	class	t	\N
95	BilanMigration	5	class	t	\N
96	BilanMigration	6	class	f	\N
97	BilanMigration	7	class	f	\N
98	BilanMigration	8	class	t	\N
99	BilanMigration	9	class	t	\N
100	BilanMigrationConditionEnv	1	class	t	\N
101	BilanMigrationConditionEnv	2	class	t	\N
102	BilanMigrationConditionEnv	3	class	t	\N
103	BilanMigrationConditionEnv	4	class	t	\N
104	BilanMigrationConditionEnv	5	class	f	\N
105	BilanMigrationConditionEnv	6	class	f	\N
106	BilanMigrationConditionEnv	7	class	f	\N
107	BilanMigrationConditionEnv	8	class	f	\N
108	BilanMigrationConditionEnv	9	class	t	\N
111	BilanMigrationInterannuelle	3	class	f	\N
113	BilanMigrationInterannuelle	5	class	f	\N
118	BilanMigrationInterannuelle	10	class	f	\N
119	BilanMigrationPar	1	class	t	\N
120	BilanMigrationPar	2	class	t	\N
121	BilanMigrationPar	3	class	t	\N
122	BilanMigrationPar	4	class	t	\N
123	BilanMigrationPar	5	class	t	\N
124	BilanMigrationPar	6	class	t	\N
132	PasdeTemps	1	class	f	\N
133	PasdeTemps	2	class	f	\N
134	PasdeTemps	3	class	t	\N
135	PasdeTemps	4	class	f	\N
136	PasdeTempsJournalier	1	class	f	\N
137	PasdeTempsJournalier	2	class	f	\N
138	PasdeTempsJournalier	3	class	f	\N
139	PasdeTempsJournalier	4	class	f	\N
140	PasdeTempsJournalier	5	class	f	\N
141	PasdeTempsJournalier	6	class	f	\N
142	PasdeTempsJournalier	7	class	f	\N
143	PasdeTempsJournalier	8	class	t	\N
144	RefAnnee	1	class	f	\N
145	RefAnnee	2	class	t	\N
146	RefAnnee	3	class	f	\N
147	RefDC	1	class	t	\N
148	RefDC	2	class	t	\N
149	RefDC	3	class	f	\N
152	RefDC	6	class	f	\N
153	RefDC	7	class	t	\N
154	RefDF	1	class	t	\N
155	RefDF	2	class	f	\N
156	RefDF	3	class	f	\N
157	RefDF	4	class	t	\N
158	Refpar	1	class	t	\N
159	Refpar	2	class	t	\N
160	Refpar	3	class	t	\N
161	Refpar	4	class	t	\N
162	Refparqual	1	class	f	\N
166	RefStades	1	class	t	\N
167	RefStades	2	class	t	\N
170	RefStades	5	class	t	\N
172	RefStationMesure	1	class	t	\N
173	RefStationMesure	2	class	t	\N
175	RefStationMesure	4	class	t	\N
176	RefTaxon	1	class	t	\N
178	RefTaxon	3	class	t	\N
179	RequeteODBC	1	class	t	\N
180	RequeteODBC	2	class	f	\N
181	RequeteODBC	3	class	f	\N
182	RequeteODBC	4	class	t	\N
183	RequeteODBC	5	class	t	\N
184	RequeteODBC	6	class	t	\N
185	fn_EcritBilanJournalier	1	function	f	\N
186	fn_EcritBilanJournalier	2	function	f	\N
187	fn_EcritBilanJournalier	3	function	f	\N
188	fn_EcritBilanJournalier	4	function	f	\N
189	fn_EcritBilanJournalier	5	function	f	\N
190	fn_EcritBilanMensuel	1	function	f	\N
193	fungraph_civelle	3	function	f	\N
194	fungraph_civelle	4	function	f	\N
195	fungraph_civelle	5	function	f	\N
196	fungraph_civelle	6	function	f	\N
197	fungraph_civelle	7	function	f	\N
198	fungraph_civelle	8	function	f	\N
199	fungraph_civelle	9	function	f	\N
200	fungraph_civelle	10	function	f	\N
201	fungraph_civelle	11	function	f	\N
202	fungraph_civelle	12	function	f	\N
203	fungraph_civelle	13	function	f	\N
204	fungraph_civelle	14	function	f	\N
205	fungraph_civelle	15	function	f	\N
206	fungraph_civelle	16	function	f	\N
207	fungraph_env	1	function	f	\N
208	fungraph_env	2	function	t	\N
209	fungraph	1	function	f	\N
210	fungraph	2	function	f	\N
211	fungraph	3	function	f	\N
212	fungraph	4	function	f	\N
213	fungraph	5	function	f	\N
214	fungraph	6	function	f	\N
216	funstat	1	function	t	\N
217	funstat	2	function	f	\N
218	funtable	1	function	f	\N
219	funtraitement_poids	1	function	t	\N
220	funtraitement_poids	2	function	t	\N
221	interface_Bilan_lot	1	interface	t	\N
223	interface_Bilan_lot	3	interface	f	\N
224	interface_Bilan_lot	4	interface	f	\N
225	interface_Bilan_lot	5	interface	t	\N
226	interface_Bilan_lot	6	interface	t	\N
235	interface_Bilan_poids_moyen	6	interface	f	\N
236	interface_Bilan_taille	1	interface	f	\N
238	interface_BilanConditionEnv	1	interface	t	\N
240	interface_BilanConditionEnv	3	interface	f	\N
241	interface_BilanConditionEnv	4	interface	f	\N
242	interface_BilanEspeces	1	interface	t	\N
244	interface_BilanEspeces	3	interface	f	\N
245	interface_BilanEspeces	4	interface	f	\N
246	interface_Bilanespeces	5	interface	f	\N
247	interface_BilanEspeces	6	interface	f	\N
249	interface_BilanMigration	1	interface	t	\N
251	interface_BilanMigration	3	interface	f	\N
252	interface_BilanMigration	4	interface	f	\N
253	interface_BilanMigration	5	interface	f	\N
255	interface_BilanMigrationConditionEnv	2	interface	t	\N
256	interface_BilanMigrationInterannuelle	1	interface	t	\N
257	interface_BilanMigrationInterannuelle	2	interface	t	\N
258	interface_BilanMigrationInterannuelle	3	interface	f	\N
259	interface_BilanMigrationInterannuelle	4	interface	t	\N
260	interface_BilanMigrationInterannuelle	5	interface	f	\N
265	interface_BilanMigrationInterannuelle	10	interface	f	\N
266	interface_BilanMigrationInterannuelle	11	interface	f	\N
267	interface_BilanMigrationInterannuelle	12	interface	f	\N
268	interface_BilanMigrationInterannuelle	13	interface	f	\N
269	interface_BilanMigrationInterannuelle	14	interface	f	\N
270	interface_BilanMigrationInterannuelle	15	interface	f	\N
271	interface_BilanMigrationInterannuelle	16	interface	f	\N
272	interface_BilanMigrationInterannuelle	17	interface	f	\N
273	interface_BilanMigrationPar	1	interface	t	\N
274	interface_BilanMigrationPar	2	interface	f	\N
275	interface_BilanMigrationPar	3	interface	f	\N
276	interface_BilanMigrationPar	4	interface	f	\N
277	interface_BilanMigrationPar	5	interface	f	\N
278	interface_BilanMigrationPar	6	interface	f	\N
279	interface_BilanMigrationPar	7	interface	f	\N
280	interface_BilanFonctionnementDC	1	interface	t	\N
282	interface_BilanFonctionnementDC	3	interface	f	\N
283	interface_BilanFonctionnementDC	4	interface	f	\N
284	interface_BilanFonctionnementDC	5	interface	f	\N
285	interface_graphique	1	interface	t	\N
286	interface_graphique	2	interface	t	\N
287	interface_graphique	3	interface	t	\N
288	interface_graphique	4	interface	t	\N
289	interface_graphique	5	interface	t	\N
290	interface_graphique	6	interface	t	\N
291	interface_graphique	7	interface	t	\N
292	interface_graphique	8	interface	t	\N
293	interface_graphique	9	interface	t	\N
294	interface_graphique	10	interface	t	\N
295	interface_graphique	11	interface	t	\N
296	interface_graphique	12	interface	t	\N
297	interface_graphique	13	interface	t	\N
298	interface_graphique	14	interface	t	\N
299	interface_graphique	15	interface	t	\N
301	interface_graphique	17	interface	f	\N
302	interface_graphique	18	interface	t	\N
303	interface_graphique	19	interface	t	\N
304	interface_graphique_menu	1	interface	f	\N
305	interface_graphique_menu	1.1	interface	f	\N
306	interface_graphique_menu	1.2	interface	f	\N
307	interface_graphique_menu	1.3	interface	f	\N
308	interface_graphique_menu	1.4	interface	f	\N
309	interface_graphique_menu	2	interface	f	\N
310	interface_graphique_menu	2.1	interface	f	\N
311	interface_graphique_menu	2.2	interface	f	\N
312	interface_graphique_menu	2.3	interface	f	\N
313	interface_graphique_menu	2.4	interface	f	\N
314	interface_graphique_menu	2.5	interface	f	\N
315	interface_graphique_menu	2.6	interface	f	\N
316	interface_graphique_menu	2.7	interface	f	\N
317	interface_graphique_menu	2.8	interface	f	\N
318	interface_graphique_menu	2.9	interface	f	\N
319	interface_graphique_menu	2.10	interface	f	\N
320	interface_graphique_menu	2.11	interface	f	\N
321	interface_graphique_menu	2.12	interface	f	\N
322	interface_graphique_menu	2.13	interface	f	\N
323	interface_graphique_menu	3	interface	f	\N
324	interface_graphique_log	1	interface	f	\N
325	interface_graphique_log	2	interface	f	\N
326	interface_graphique_log	3	interface	f	\N
327	interface_graphique_log	4	interface	f	\N
328	interface_graphique_log	5	interface	f	\N
329	interface_graphique_log	6	interface	f	\N
231	interface_Bilan_poids_moyen	2	interface	f	gbutton
232	interface_Bilan_poids_moyen	3	interface	f	gbutton
233	interface_Bilan_poids_moyen	4	interface	f	gbutton
234	interface_Bilan_poids_moyen	5	interface	f	frame
330	interface_graphique_log	7	interface	f	\N
331	interface_graphique_log	8	interface	f	\N
332	interface_graphique_log	9	interface	f	\N
344	interface_Bilan_lot	10	interface	f	tooltip and name
345	interface_Bilan_lot	11	interface	f	tooltip and name
126	ConnectionODBC	1	class	t	note messages here are not printed in the console, sono use for this, kept for record
127	ConnectionODBC	2	class	t	note messages here are not printed in the console, sono use for this, kept for record
128	ConnectionODBC	3	class	t	note messages here are not printed in the console, sono use for this, kept for record
129	ConnectionODBC	4	class	t	note messages here are not printed in the console, sono use for this, kept for record
130	ConnectionODBC	5	class	f	note messages here are not printed in the console, sono use for this, kept for record
131	ConnectionODBC	6	class	f	\N
\.


--
-- Data for Name: ts_messagerlang_mrl; Type: TABLE DATA; Schema: ref; Owner: postgres
--

COPY ts_messagerlang_mrl (mrl_id, mrl_msr_id, mrl_text, mrl_lang) FROM stdin;
1	1	"Il faut choisir un dispositif de comptage,cliquez sur valider "	French
2	2	"Il faut choisir un taxon,cliquez sur valider "	French
3	3	"Il faut choisir un stade,cliquez sur valider "	French
4	4	"Il faut choisir un parametre,cliquez sur valider "	French
5	5	"Il faut choisir la date de debut"	French
6	6	"Il faut choisir la date de fin"	French
7	7	"Il faut choisir un paramètre quantitatif"	French
8	8	"Il faut choisir un parametre qualitatif"	French
9	9	"Il faut choisir une categorie d'effectif"	French
10	10	"Il faut choisir l'annee de debut"	French
11	11	"Il faut choisir l'annee de fin"	French
12	12	"il faut choisir un dispositif de franchissement,cliquez sur valider "	French
13	13	"La requete est effectuee pour charger les caracteristiques de lot "	French
14	14	"Aucune donnee pour ces lots dans la periode selectionnee"	French
17	17	"La requete est effectuee pour charger les coefficients de conversion "	French
18	18	"lignes trouvees pour les coefficients de conversion"	French
20	20	"effectif non renseigne, lots" 	French
21	21	"humides" 	French
22	22	"secs"	French
23	23	"humides et secs" 	French
24	24	"date"	French
25	25	"poids moyens"	French
26	26	"Tendance saisonniere des poids" 	French
27	27	", anneedebut="	French
28	28	", anneefin="	French
29	29	"modele sinusoidal,a.cos.2pi.(jour-T)/365)+b "	French
31	31	"Repertoire d'ecriture des donnees :"	French
32	32	"Voulez vous ecrire les donnees dans la base ?"	French
33	33	"attention"	French
34	34	"Gra" 	French
35	35	"Coe" 	French
36	36	"Tail" 	French
37	37	"Reg" 	French
38	38	"export" 	French
39	39	"quitter" 	French
40	40	"La requete a été effectuée pour charger les stades pigmentaires"	French
41	41	"Bilan des stades pigmentaires"	French
42	42	"BILAN STADES PIGMENTAIRES" 	French
43	43	"Stades pigmentaires"	French
44	44	"et dates d'arrivees en estuaire"	French
45	45	"Choix du titre"	French
46	46	"Il faut choisir au moins une caracteristique quantitative ou qualitative"	French
47	47	"Attention cette requete (croisement de deux vues) peut prendre plusieurs minutes, soyez patient(e)..."	French
49	49	"La requete : requete=get("bilan_taille",envir_stacomi)@requete@sql "	French
51	51	"Donnees directement issues de la requete : don=get("bilan_taille",envir_stacomi)@requete@query"	French
52	52	"Il faut d'abord faire le calcul, appuyez sur calcul"	French
53	53	"Requete effectuee"	French
54	54	"La requete est effectuee pour charger les conditions environnementales "	French
55	55	"Il faut choisir une station de mesure puis cliquer sur valider "	French
56	56	"Certaines stations de mesure n'ont pas de valeurs associees"	French
57	57	"Aucune valeur de conditions environnementales pour les stations de mesure selectionnees (BilanConditionEnv.r)"	French
58	58	"Statistiques :"	French
60	60	"Il faut faire tourner les calculs avant, cliquer sur calc "	French
61	61	"Echec de la requete vers la vue vue_ope_lot_car "	French
62	62	"Il faut lancer les calculs avant cliquez sur calcul"	French
63	63	"Il n'y a aucun poisson dans la base sur cette periode"	French
64	64	"Attention certains effectifs négatifs sont transformés en positifs"	French
65	65	"Verifications des objets et lancement de la requete"	French
66	66	"La requete est effectuee pour charger les pas de temps du DC "	French
67	67	"Il n'y a pas de donnees sur ce DC "	French
68	68	"mois" 	French
69	69	"temps en heures" 	French
70	70	"Fonctionnement du dispositif de comptage" 	French
71	71	"suivi video" 	French
72	72	"arret video" 	French
74	74	"DC" 	French
75	75	c("Fonc","Arr","Fonc normal")	French
76	76	c("Fonc","Arr")	French
77	77	"Fonctionnement DC" 	French
78	78	"Types d'arrets du DC" 	French
79	79	"Ecriture de" 	French
80	80	"peut prendre un peu de temps, soyez patient...."	French
81	81	"La requete est effectuee pour charger les pas de temps du DF "	French
82	82	"Il n'y a pas de donnees sur ce DF "	French
83	83	"Construction du graphe, patientez "	French
84	84	"calcul..." 	French
85	85	"progression %" 	French
86	86	c("duree","type_fonct.","fonctionnement")	French
87	87	"Fonctionnement DF"	French
89	89	"DF"	French
73	73	"Ecriture de tableau dans l'environnement envir_stacomi : ecrire periodeDC=get('periodeDC',envir_stacomi) "	French
15	15	"Pour recuperer le tableau, tapper : bilan_lot=get('bilan_lot',envir_stacomi), requete=bilan_lot@requete@sql"	French
19	19	"Pour recuperer le tableau, tapper : bilan_poids_moyen=get('bilan_poids_moyen',envir_stacomi)@data"	French
30	30	"Pour recuperer le tableau, tapper : import_coe=get('import_coe',envir_stacomi)"	French
48	48	"Pour recuperer le tableau, tapper : bilan_taille=get('bilan_taille',envir_stacomi)"	French
50	50	"Les donnees : donnees_taille=get('bilan_taille',envir_stacomi)@data"	French
59	59	"L'objet Bilan est stocke dans l'environnement envir_stacomi, ecrire  ecrire bilanEspeces=get('bilanEspeces',envir_stacomi), "	French
90	90	"Types d'arrets du DF"	French
290	290	"Bilan migration inter-annuel"	French
91	91	"Attention le choix du pas de temps n'a pas ete effectue, calcul avec la valeur par defaut "	French
92	92	"Debut du bilan migration... patientez "	French
95	95	"Il faut faire tourner les calculs avant, cliquer sur calc "	French
96	96	"Migration cumulee"	French
97	97	"Effectif cumule, "	French
98	98	"Attention cette fonction est pour les bilans annuels "	French
99	99	"Statistiques concernant la migration : "	French
100	100	"L'objet Bilan est stocke dans l'environnement envir_stacomi"	French
101	101	"Il faut faire tourner les calculs avant, cliquer sur calc"	French
102	102	"Vous n'avez pas de conditions environnementales sur la periode de temps"	French
103	103	"pas station selectionnee => graphe simple"	French
104	104	"le nombre de lignes du tableau des conditions environnentales ("	French
105	105	") ne correspond pas a la duree du bilan Migration ("	French
106	106	"Attention : sur une des stations :"	French
107	107	"il y a plusieurs enregistrements pour la même journée : "	French
108	108	"seule la première valeur sera intégrée dans le bilan "	French
109	109	"Attention il n'existe pas de bilan migration pour l'annee "	French
110	110	", ce taxon et ce stade (BilanMigrationInterAnnuelle.r)" 	French
111	111	"La requete est effectuee pour charger les migrations sur les annees"	French
112	112	"Effectifs" 	French
113	113	"ATTENTION : Veuillez effectuer un Bilan Migration pour au moins une des annees selectionnees avant de lancer un bilan inter-annuel"	French
114	114	"annee" 	French
115	115	"date" 	French
116	116	"Pourcentage de la migration annuelle" 	French
119	119	"Debut du bilan migration avec parametres... patientez "	French
120	120	"Il faut choisir au moins une caracteristique quantitative ou qualitative"	French
121	121	"Attention, ce traitement ne s'effectue pas sur les quantites de lots "	French
123	123	"Il faut faire tourner les calculs avant"	French
125	125	"Ecriture de"	French
126	126	"Il faut definir un vecteur baseODBC avec le lien ODBC, l'utilistateur et le mot de passe"	French
127	127	"La librairie RODBC est necessaire, chargez le package ! "	French
128	128	"Essai de connexion, attention cette classe ne doit être utilisée que pour les tests : "	French
129	129	"Connexion impossible :"	French
130	130	"Connexion établie"	French
131	131	"Connexion en cours"	French
132	132	"Choix des Pas de Temps"	French
133	133	"Choix du nombre de pas de temps"	French
134	134	"Erreur interne : le tableau des pas de temps ne contient aucune ligne"	French
135	135	"Date de fin"	French
136	136	"la duree du pas devrait etre journaliere"	French
137	137	"le pas de temps ne doit pas etre a cheval sur plusieurs annnees"	French
138	138	"Choix des Pas de Temps (duree 1 an)"	French
139	139	"Date de debut"	French
140	140	"Pas de temps"	French
141	141	"Nb jour"	French
142	142	"Date de fin"	French
143	143	"Les pas de temps ont ete charges"	French
144	144	"Choix de l'annee"	French
145	145	"Annee selectionnee"	French
146	146	"probleme lors du chargement des donnees ou pas de donnees dans la base (lien ODBC ?)"	French
147	147	"Le DC a ete selectionne "	French
148	148	"selection des taxons du DC (pour l'instant sur toutes les periodes) "	French
149	149	"Donnees sur les Dispositifs de Comptage"	French
150	150	"fermer" 	French
151	151	"Choix du Dispositif de Comptage"	French
152	152	"Tableau"	French
153	153	"Erreur : Aucun DC n'est rentre dans la base (aucune ligne de retour de la requete)"	French
154	154	"Le DF a ete selectionne "	French
155	155	"Donnees sur les Dispositifs de Franchissement"	French
156	156	"Choix du Dispositif de Franchissement"	French
157	157	"Aucun DF n'est rentre dans la base (aucune ligne de retour de la requete)"	French
158	158	"La requete est effectuee pour charger les parametres "	French
159	159	"Pas de donnees pour ce DC, ce taxon et ce stade "	French
160	160	"La caracteristique a ete selectionnee "	French
161	161	"erreur interne, aucune caracteristique n'a pu être chargée pour faire le choix "	French
162	162	"Erreur interne : il devrait y avoir une ligne dans Refparqual@data, or nbligne="	French
163	163	0	French
164	164	0	French
165	165	0	French
166	166	"Pas de donnees pour ce DC, et ce taxon "	French
167	167	"Le stade a ete selectionne "	French
168	168	"Caracteristique qualitative" 	French
169	169	"Caracteristique quantitative" 	French
170	170	"Stop erreur interne : charger les donnees pour faire le choix "	French
171	171	"Choix du Stade" 	French
172	172	"selectionnez au moins une valeur"	French
173	173	"Les stations de mesure ont ete selectionnees "	French
174	174	"Choix des stations de mesure" 	French
175	175	"Stop il n'y  a aucune donnee de station de mesure (pb lien ODBC ?) "	French
94	94	"Ecriture du tableau de bilan des migrations dans l'environnement envir_stacomi : ecrire tableau=get('tableau',envir_stacomi)"	French
118	118	"Ecriture de l'objet graphique dans l'environnement envir_stacomi : ecrire g=get('gi',envir_stacomi) avec "	French
122	122	"Ecriture de data dans l'environnement envir_stacomi : ecrire data=get('data',envir_stacomi) "	French
124	124	"Ecriture de l'objet graphique dans l'environnement envir_stacomi : ecrire g=get('g',envir_stacomi) "	French
176	176	"Le taxon a ete selectionne "	French
177	177	"Choix du Taxon" 	French
178	178	"Stop il n'y  a aucune ligne dans la table des taxons (pb lien ODBC ?) "	French
179	179	"Erreur ODBC =>Il faut definir un vecteur baseODBC avec le lien ODBC, l'utilistateur et le mot de passe "	French
180	180	"Essai de connexion :"	French
181	181	"Connexion impossible :"	French
182	182	"Connexion reussie "	French
183	183	"Essai de la requete "	French
184	184	"Requete reussie "	French
185	185	"Un Bilan a deja ete ecrit dans la base le :"	French
186	186	"voulez vous le remplacer ?"	French
187	187	"ecriture du bilan journalier dans la base"	French
188	188	"progression %"	French
189	189	"ecriture du bilan journalier dans la base"	French
190	190	"ecriture du bilan mensuel dans la base"	French
191	191	"graph civelle :" 	French
192	192	"Effectif de civelles (x1000)" 	French
193	193	"Effectif estimes,"	French
194	194	c("eff. journ. poids","eff. journ. compt.")	French
195	195	"nombre d'operations="	French
196	196	"duree moyenne du piegeage="	French
197	197	"duree max="	French
198	198	"duree min="	French
199	199	c("Fonc","Arr","Fonc normal")	French
200	200	c("Fonc","Arr")	French
201	201	"DF"	French
202	202	"DC"	French
203	203	"OP"	French
204	204	"Mois"	French
205	205	"Effectif (x1000)"	French
206	206	c("eff. mens. poids","eff. mens. compt.")	French
207	207	"Effectif"	French
209	209	"ATTENTION, il y a des quantite de lots rentrees pour un taxon autre que civelles, verifier"	French
210	210	"effectif"	French
211	211	"Date"	French
212	212	"Effectif estime, "	French
213	213	c("Ponctuel","expert","calcule","mesure")	French
214	214	"somme effectifs ="	French
215	215	c("Effectifs","type","duree","mois","quinzaine","semaine","jour_365") 	French
216	216	"calcul des bilans mensuels"	French
217	217		French
218	218	"ecriture de"	French
219	219	"Conversion poids effectif "	French
220	220	"Attention somme =0,vous n'avez pas encore rentre les coef de conversion"	French
221	221	"chargement de la vue (vue_ope_lot) et choix du dc et des pas de temps"	French
222	222	"BILAN LOTS" 	French
223	223	"horodate de debut"	French
224	224	"horodate de fin"	French
225	225	"La date de debut a ete choisie"	French
226	226	"La date de fin a ete choisie"	French
228	228	"tableau" 	French
229	229	"Quitter" 	French
235	235		French
236	236		French
237	237	"requete croisee taille/ caract qualitative"	French
238	238	"Chargement des stations de mesure "	French
239	239	"graphe bilan" 	French
240	240	"tables bilan en .csv"	French
241	241	"Quitter"	French
242	242	"Bilan des especes presentes sur le DC"	French
243	243	"Bilan Especes" 	French
244	244	"Chargement"	French
245	245	"Graphe PieChart"	French
246	246	"Histogramme"	French
247	247	"Tables bilan en .csv et XML"	French
248	248	"Choix du decoupage" 	French
249	249	"Chargement des listes taxons et stades et dc"	French
250	250	"Calcul des effectifs par pas de temps" 	French
251	251	"Graphe bilan"	French
252	252	"Graphe cumul"	French
253	253	"Tables bilan en .csv"	French
254	254	"calcul des condition environnementales par pas de temps" 	French
255	255	"Chargement des listes taxons et stades et dc et stations de mesure "	French
256	256	"Chargement des bilanJournaliers existants"	French
257	257	"L'annee de debut a ete choisie"	French
258	258	"Annee de debut"	French
259	259	"L'annee de fin a ete choisie"	French
260	260	"Annee de fin"	French
261	261	"Migration de toutes les annees dans le meme graphique" 	French
262	262	"cumul migratoires en %" 	French
263	263	"Tableau" 	French
264	264	"Quitter" 	French
265	265	"jour"	French
266	266	"Migration journalière"	French
267	267	"sem"	French
268	268	"Migration hebdomadaire"	French
269	269	"quin"	French
270	270	"Migration par quinzaine"	French
271	271	"mois"	French
272	272	"Migration mensuelle"	French
273	273	"Chargement des listes taxons,stades,dc, parametres qualitatifs et quantitatifs  "	French
274	274	"Choix du type de lot, inclusion des echantillons ?"	French
275	275	"Calcul des effectifs par pas de temps"	French
276	276	"graphe mensuel"	French
277	277	"graphe journalier"	French
278	278	"tables bilan en .csv"	French
279	279	"quitter"	French
280	280	"Chargement des listes dc et choix pas de temps"	French
281	281	"Graphe mensuel"	French
282	282	"Diagramme en boites"	French
283	283	"tableau"	French
284	284	"Quitte"	French
285	285	"Calculs du fonctionnement du df"	French
286	286	"Calculs du fonctionnement du dc"	French
230	230	"Bilan Poids Moyen"	French
232	232	"table"	French
231	231	"charge"	French
233	233	"quitter"	French
234	234	"choix de la catégorie d'effectif"	French
287	287	"Bilan des operations d'un dispositif ...a developper"	French
288	288	"Bilan croises du fonctionnement du DF et du DC,a developper "	French
289	289	"Bilan migration (pour une espèce et un stade)"	French
291	291	"Bilan migration conditions environnementales"	French
292	292	"Bilan migration avec parametres"	French
293	293	"Bilan des conditions environnementales"	French
294	294	"Bilan lots par appel de la vue vue lot ope"	French
295	295	"Bilan tailles "	French
297	297	"Calcul des stades pigmentaires "	French
298	298	paste("Pour de l'aide cedric Briand - 02 99 90 88 44 - cedric.briand@lavilaine.com"	French
299	299	"Lancement de Rcmdr ( menu donnees, jeu de donnees actif, selectionner le jeu de donnees actif"	French
300	300	"Traitement migrateur" 	French
301	301	"TODO à développer" 	French
302	302	"sorties du programme"	French
303	303	"Bilan des espèces du DC"	French
304	304	"Station"	French
305	305	"DF"	French
306	306	"DC"	French
307	307	"Operation (TODO)"	French
308	308	"DF sans DC (TODO)"	French
309	309	"Bilan"	French
310	310	"Migration"	French
311	311	"Cond. Env."	French
312	312	"Migr.~Cond. Env."	French
313	313	"Migr./ parm. quant / parm. qual"	French
314	314	"Migr. interannuel"	French
315	315	"Parm. de lot"	French
316	316	"Poids Moyen civelle"	French
317	317	"Tailles"	French
318	318	"Stades pigmentaires"	French
319	319	"Migr. juvéniles (TODO)"	French
321	321	"parm qual (proportions) (TODO)"	French
322	322	"Especes"	French
323	323	"Aide"	French
324	324	"Connexion"	French
325	325	"Utilisateur"	French
326	326	"Mot de passe"	French
327	327	"Login"	French
328	328	"Erreur"  title of the frame	French
329	329	"Probleme lors du test de la connexion ODBC" 	French
330	330	"Erreur dans l'utilisation de la methode connect de la classe ConnexionODBC" 	French
331	331	"Probleme lors du test, le lien ODBC fonctionne mais ne pointe pas vers la base version 0.3, verifiez le lien ODBC"	French
332	332	"Lien ODBC"	French
93	93	"L'objet Bilan est stocke dans l'environnement envir_stacomi, ecrire  ecrire bilanMigration=get('bilanMigration',envir_stacomi)"	French
88	88	"ecriture de tableau dans l'environnement envir_stacomi : ecrire periodeDF=get('periodeDF',envir_stacomi) "	French
208	208	"Ecriture de l'objet graphique dans l'environnement envir_stacomi : ecrire g=get('g',envir_stacomi) "	French
333	1	"You need to choose a counting device, clic on validate"	English
334	2	"You need to choose a taxa, clic on validate"	English
335	3	"You need to choose a stage, clic on validate"	English
336	4	"You need to choose a parameter, clic on validate"	English
337	5	"You need to choose the starting date"	English
338	6	"You need to choose the ending date"	English
339	7	"You need to choose a quantitative parameter"	English
340	8	"You need to choose a qualitative parameter"	English
341	9	"You need to choose a size class"	English
342	10	"You need to choose the starting year"	English
343	11	"You need to choose the ending year"	English
344	12	"You need to choose a crossing device, clic on validate"	English
345	13	"The query to load lot's characteristics is achieved"	English
346	14	"No information for this lots into the selected period"	English
347	15	"To obtain the table, type : bilan_lot=get('bilan_lot',envir_stacomi), requete=bilan_lot@requete@sql"	English
349	17	"The query to load the coefficients of conversion is finished"	English
350	18	"lines founded for the coefficients of conversion"	English
351	19	"To obtain the table, type : bilan_poids_moyen=get('bilan_poids_moyen',envir_stacomi)@data"	English
352	20	"size is missing, lots"	English
353	21	"wet"	English
354	22	"dry"	English
355	23	"wet and dry"	English
356	24	"date"	English
357	25	"mean weights"	English
358	26	"Seasonnal trend of weights"	English
359	27	", beginningyear="	English
360	28	", endingyear="	English
361	29	"sinusoidal model, a.cos.2pi.(jour-T)/365)+b "	English
362	30	"To obtain the table, type : import_coe=get(import_coe",envir_stacomi)"	English
363	31	"data directory :"	English
364	32	"Do you want to write data in the database ?"	English
365	33	"attention"	English
366	34	"Gra"	English
367	35	"Coe"	English
368	36	"Leng"	English
369	37	"Reg"	English
370	38	"export"	English
371	39	"exit"	English
372	40	"Pigmentation stages loading query completed"	English
373	41	"Summary of pigmentation stages"	English
374	42	"SUMMARY OF PIGMENTATION STAGES"	English
375	43	"Pigmentation stages"	English
376	44	"and incoming dates in estuary"	English
377	45	"Title choice"	English
378	46	"You need to choose at least one quantitative or qualitative feature"	English
320	320	"Bilan Migration multiples"	French
296	296	"Bilan des poids moyens en vue du calcul des relations poids effectif."	French
379	47	"Attention, this query might take a while, be patient ..."	English
380	48	"To get the table, type : bilan_taille=get('bilan_taille',envir_stacomi)"	English
381	49	"The query : requete=get('bilan_taille',envir_stacomi)@requete@sql "	English
382	50	"The data : donnees_taille=get('bilan_taille',envir_stacomi)@data"	English
383	51	"Data outcome from the query : don=get('bilan_taille',envir_stacomi)@requete@query"	English
384	52	"You need to launch computation first, clic on calc"	English
385	53	"Query completed"	English
386	54	"Environmental conditions loading query completed"	English
387	55	"You need to choose a monitoring station, clic on validate"	English
388	56	"Some monitoring stations lack associated values"	English
389	57	"No environmental conditions values for selected monitoring stations (BilanConditionEnv.r)"	English
390	58	"Statistics :"	English
391	59	"Summary object is stocked into envir_stacomi environment : write bilanEspeces=get('bilanEspeces',envir_stacomi)"	English
392	60	"You need to launch computation first, clic on calc"	English
393	61	"Query failed for the view vue_ope_lot_car "	English
394	62	"You need to launch computation first, clic on calc"	English
395	63	"No fish in the database for this period"	English
396	64	"Attention, some negative counts are transformed into positive ones"	English
397	65	"Checking objects and launching query"	English
398	66	"Time steps loaded fot this counting device"	English
399	67	"No data for this counting device"	English
400	68	"month"	English
401	69	"time in hours"	English
402	70	"Working of the counting device"	English
403	71	matting ON 	English
404	72	matting OFF 	English
405	73	"Writing the table into envir_stacomi environment : write periodeDC=get('periodeDC',envir_stacomi)"	English
406	74	"Counting device"	English
407	75	c("Fonc","Arr","Fonc normal")	English
408	76	c("Fonc","Arr")	English
409	77	"Working of the counting device"	English
410	78	"Shutdowns types for this counting device"	English
411	79	"Writing of "	English
412	80	"this might take a while, please be patient …"	English
413	81	"Time steps of the fishway loaded"	English
414	82	"Shutdowns types for this counting device"	English
415	83	"No data for this fishway"	English
416	84	"computing ..."	English
417	85	"Progress %"	English
418	86	c("duree","type_fonct.","fonctionnement")	English
419	87	"Working of the fishway "	English
420	88	"Writing the table into envir_stacomi environment : write periodeDF=get('periodeDF',envir_stacomi)"	English
421	89	"Fishway"	English
422	90	"Shutdowns types for this fishway "	English
423	91	"Attention, no time step selected, compunting with default value"	English
424	92	"Starting migration summary ... be patient"	English
425	93	"Summary object is stocked into envir_stacomi environment : write bilanMigration=get(bilanMigration",envir_stacomi)"	English
426	94	"Writing the migration summary table into envir_stacomi environment : write tableau=get(tableau",envir_stacomi)"	English
427	95	"You need to launch computation first, clic on calc"	English
428	96	"Cumulative migration"	English
429	97	"Cumulative count"	English
430	98	"Attention, this function applies for annual summaries"	English
431	99	"Statistics about migration :"	English
432	100	"Summary object is stocked into envir_stacomi environment"	English
433	101	"You need to launch computation first, clic on calc"	English
434	102	"You don't have any environmental conditions within the time period"	English
435	103	"no selected station => simple graph"	English
436	104	"The number of lines of the environmental conditions table ("	English
437	105	") doesn't fit the duration of the migration summary  ("	English
438	106	"Attention, on one station :"	English
439	107	"there are several entries for the same day : "	English
440	108	"only the first value will be incuded in the summary"	English
441	109	"Attention, there is no migration summary for this year"	English
442	110	", this taxon and this stage (BilanMigrationInterAnnuelle.r)"	English
443	111	"Annual migrations query completed"	English
444	112	"Counts"	English
445	113	"Attention : you have to complete a migration summary for at least one of the selected year before launching a inter-annual summary"	English
446	114	"year"	English
447	115	"date"	English
448	116	"Annual migration percentage"	English
450	118	"Writing the graphical object into envir_stacomi environment : write g=get(gi",envir_stacomi) with "	English
451	119	"Starting migration summary with parameters, be patient …"	English
452	120	"You need to choose at least one quantitative or qualitative attribute"	English
453	121	"Be Attention, the processing doesnt take lot"s quantities into account"	English
454	122	"Writing data into envir_stacomi environment : write data=get(data",envir_stacomi)"	English
455	123	"You need to launch computation first, clic on calc"	English
456	124	"Writing the graphical object into envir_stacomi environment : write g=get(g",envir_stacomi)"	English
457	125	"Writing of"	English
458	126	"You need to define a baseODBC vector with the ODBC link, the user and the password"	English
459	127	"RODBC library is necessary, load the package !"	English
460	128	"Testing the connection, be Attention this class must only be used for testing :"	English
461	129	"Connection failed :"	English
462	130	"Connection established"	English
463	131	"Connection in progress"	English
464	132	"Time steps choice"	English
465	133	"Number of time steps choice"	English
466	134	"Internal error : no entry in time steps table"	English
467	135	"End date"	English
468	136	"Time step duration should be daily"	English
469	137	"Time step can't include more than one year"	English
470	138	"Time steps choice (1 year duration)"	English
471	139	"Start date"	English
472	140	"Time step"	English
473	141	"Number of days"	English
474	142	"End date"	English
475	143	"Time steps loaded"	English
476	144	"Year choice"	English
477	145	"Year selected"	English
478	146	"Problem when loading data or no data in the database (ODBC link ?)"	English
479	147	"Counting device selected"	English
480	148	"Select taxa for this counting device (for all periods for now)"	English
481	149	"Counting devices data"	English
482	150	"close"	English
483	151	"Counting devices choice"	English
484	152	"Table"	English
485	153	"Error : no counting device in the database (the query returns 0 entry)"	English
486	154	"Fishway selected"	English
487	155	"Fishways data"	English
488	156	"Fishway choice"	English
489	157	"No fishway in the database (the query returns 0 entry)"	English
490	158	"Loading parameters query completed"	English
491	159	"No data for selected device, taxon and stage"	English
492	160	"Feature has been selected"	English
493	161	"Internal error : unable to load any feature to make the choice"	English
494	162	"Internal error : there must have one line in Refparqual@data, or nbligne="	English
495	163	0	English
496	164	0	English
497	165	0	English
498	166	No data for this counting device and this taxon	English
499	167	Stage selected	English
500	168	Qualitative feature	English
501	169	Quantitative feature	English
502	170	Stop internal error : load data to make a choice	English
503	171	Stage selection	English
504	172	Select at least one value	English
505	173	The monitoring stations have been selected	English
506	174	Monitoring stations selection	English
507	175	Stop : no data for selected monitoring station (problem with the ODBC link ?)	English
508	176	Taxon selected	English
509	177	Taxon selection	English
510	178	Stop there is no line in the taxons table (problem with the ODBC link ?)	English
511	179	Error ODBC => you must define a vector baseODBC with an ODBC link, a user and a password	English
512	180	Testing connection	English
513	181	Connection failed	English
514	182	Connection successful	English
515	183	Testing query	English
516	184	Query successful	English
517	185	A summary has already been written in the database the :	English
518	186	Overwrite ?	English
519	187	Writing daily balance sheet in the database	English
520	188	Progression %	English
521	189	Writing daily summary in the database	English
522	190	Writing monthly summary in the database	English
523	191	Glass eels graph	English
524	192	Number of glass eels (x1000)	English
525	193	Estimated numbers	English
526	194	c("weight of the daily number","daily number counted")	English
527	195	number of operations =	English
528	196	average trapping time = 	English
529	197	maximum term =	English
530	198	minimum term =	English
531	199	c("work","stop","work normaly")	English
532	200	c("work","stop")	English
533	201	Fishway	English
534	202	Counting device	English
535	203	Operation	English
536	204	Month	English
537	205	Number (x1000)	English
538	206	c("weight of monthly number","monthly number counted")	English
539	207	Number	English
540	208	Writing of the graphical object in the environment envir_stacomi : write g=get(g",envir_stacomi)"	English
541	209	Attention, there are batch quantiles entered for another taxon than glass eel, please check)	English
542	210	Number	English
543	211	Date	English
544	212	estimated number,	English
545	213	c("punctual","expert","calculated","measured")	English
546	214	Sum of numbers =	English
547	215	c("Numbers","type","period","month","fortnight","week","day_365")	English
548	216	Calculation of the monthly balance sheet	English
549	217		English
550	218	writing of	English
551	219	Conversion weight / number	English
552	220	Be Attention sum=0, you didn't enter the coefficient of conversion	English
553	221	Loading of the view vue_ope_lot, and choice of the counting device and of the time steps	English
554	222	LOTS SUMMARY	English
555	223	Start of timestamp	English
556	224	End of timestamp	English
557	225	Beginning date has been chosen	English
558	226	Ending date has been chosen	English
560	228	Table	English
561	229	Exit	English
567	235		English
568	236		English
569	237	crossed query length / qualitative feature	English
570	238	Loading of the monitoring stations	English
564	232	"table"	English
563	231	"load"	English
565	233	"exit"	English
571	239	summary graphic	English
572	240	Summary tables in .csv	English
573	241	Exit	English
574	242	Summary of encountered species for the counting device	English
575	243	Species summary	English
576	244	Loading	English
577	245	Pie chart graphic	English
578	246	Histogram	English
579	247	Summary tables in .csv and XML	English
580	248	Choice of cutting	English
581	249	Loading of the lists for taxons, stages and counting devices	English
582	250	Calculation of numbers by time step	English
583	251	Balance graphic	English
584	252	Cumulative graphic	English
585	253	Balance sheet in .csv	English
586	254	Calculation of environnemental conditions by time step	English
587	255	Loading of the lists for taxons, stages, counting devices and monitoring stations	English
588	256	Loading of the existing daily summaries	English
589	257	The year of beginning has been chosen	English
590	258	Beginning year	English
591	259	The year of end has been chosen	English
592	260	Ending year	English
593	261	Migration of all the years in the same graphic	English
594	262	cumulated migrations %	English
595	263	Table	English
596	264	Exit	English
597	265	day	English
598	266	Daily migration	English
599	267	week	English
600	268	weekly migration	English
601	269	fortnight	English
602	270	Fortnight Migration	English
603	271	month	English
604	272	Monthly migration	English
605	273	Loading of the lists for taxons, stages, counting devices, qualitative and quantitative parameters	English
606	274	Choice of batch type, inclusion of samples ?	English
607	275	Calculation of numbers by time step	English
608	276	Monthly graphic	English
609	277	Daily graphic	English
610	278	Summary in .csv	English
611	279	Exit	English
612	280	Loading of the list for fishways and choice of the time step	English
613	281	Mensual graphic	English
614	282	Boxplot	English
615	283	Table	English
616	284	Exit	English
617	285	Calculation of the operating fishway	English
618	286	Calculation of the operating counting device	English
619	287	Summary of the operations of a device ... to do	English
620	288	Summary between the operating fishway and the counting device … to do	English
621	289	Migration summary (for a species and a stage)	English
622	290	Summary of interannual migration	English
623	291	Summary of migration environnemental conditions	English
624	292	Summary of migration with parameters	English
625	293	Summary of the environnemental conditions	English
626	294	Summary of batch by calling the application vue lot ope	English
627	295	Lengths summary	English
629	297	Calculation of the pigmentary stages	English
630	298	Paste (For help, contact Cédric Briand - 0033 29 99 08 844 - cedric.briand@lavilaine.com")"	English
631	299	Launch of Rcmdr (data menu, active dataset, select the active dataset)	English
632	300	Migratory treatment	English
633	301	TODO to develop	English
634	302	Output of the program	English
635	303	Species summary of the counting device	English
636	304	Station	English
637	305	Fishway	English
638	306	Counting Device	English
639	307	Operation (TODO)	English
640	308	Fishway without counting device (TODO)	English
641	309	Summary	English
642	310	Migration	English
643	311	Environnemental conditions	English
644	312	Migration. ~Environnemental conditions	English
645	313	Migration / quant. param. / qual. param.	English
646	314	Inter annual migration	English
647	315		English
648	316	Average weight glass eel	English
649	317	Lenghts	English
650	318	Pigmentary stages	English
651	319	Juveniles migration (TODO)	English
653	321		English
654	322	Species	English
655	323	Help	English
656	324	Connection	English
657	325	User	English
658	326	Password	English
659	327	Login	English
660	328	"Error" title of the frame	English
661	329	Problem when testing the ODBC connection	English
662	330	Error when using the method connect of the ConnectionODBC class	English
663	331	Problem during the test, the ODBC link works but doesn't point to the database 0.3, check the ODBC link	English
664	332	ODBC link	English
665	1	"Elige un dispositivo de cuenta, haga click en validar"	Spanish
666	2	"Elige un taxon, haga click en validar"	Spanish
667	3	"Elige un estadio, haga click en validar"	Spanish
668	4	"Elige la fecha de inicio"	Spanish
669	5	"Elige la fecha de inicio"	Spanish
670	6	"Elige la fecha final"	Spanish
671	7	"Elige un parámetro cuantitativo"	Spanish
672	8	"Elige un parámetro cualitativo"	Spanish
673	9	"Elige una categoria de cantidad"	Spanish
674	10	"Elige el año de inicio"	Spanish
675	11	"Elige el año final"	Spanish
676	12	"Elige un disposotivo de paso, haga click en validar"	Spanish
677	13	"Se ha efectuado la  petición para cargar las características del lote (grupo)"	Spanish
678	14	"No hay ningún dato de estos lotes en el período seleccionado"	Spanish
679	15	"Para recuperar la tabla, escribir: bilan_lot=get('bilan_lot',envir_stacomi), requete=bilan_lot@requete@sql"	Spanish
681	17	"Se ha efectuado la petición para cargar los coeficientes de conversión"	Spanish
682	18	"Filas encontradas para los coeficientes de conversión"	Spanish
683	19	"Para recuperar la tabla, escribir: bilan_poids_moyen=get('bilan_poids_moyen',envir_stacomi)@data"	Spanish
684	20	"cantidad no conocida, lotes"	Spanish
685	21	"Húmedos"	Spanish
686	22	"Secos"	Spanish
687	23	"Húmedos y secos"	Spanish
688	24	"Fecha"	Spanish
689	25	"Pesos medios"	Spanish
690	26	"Tendencia estacional de los pesos"	Spanish
691	27	",añoinicio="	Spanish
692	28	",añofin="	Spanish
693	29	"modelo sinusoidal,a.cos.2pi.(jour-T)/365)+b "	Spanish
694	30	"Para recuperar la tabla, escribir: import_coe=get('import_coe',envir_stacomi)"	Spanish
695	31	"Directorio de escritura de los datos"	Spanish
696	32	"¿Quiere escribir los datos en la base?"	Spanish
697	33	"atención"	Spanish
698	34	"Gra"	Spanish
699	35	"Coe"	Spanish
700	36	"Tail"	Spanish
701	37	"Reg"	Spanish
702	38	"exportar"	Spanish
703	39	"salir"	Spanish
704	40	"Se ha efectuado la petición para cargar los estadíos pigmentarios"	Spanish
705	41	" Conclusión de los estadíos pigmentarios"	Spanish
706	42	"CONCLUSIÓN DE ESTADIOS PIGMENTARIOS"	Spanish
707	43	"Estadíos pigmentarios"	Spanish
708	44	"y fechas de llegada al estuario"	Spanish
709	45	"Elegir el  título"	Spanish
710	46	"Elige al menos una característica cuantitativa o cualitativa"	Spanish
711	47	"Atención esta petición (cruce de dos vistas) puede necesitar unos minutos, sea paciente"	Spanish
712	48	"Para recuperar la tabla, escribir: bilan_taille=get('bilan_taille',envir_stacomi)"	Spanish
713	49	"La peteción:  requete=get('bilan_taille',envir_stacomi)@requete@sql "	Spanish
714	50	"Los datos: donnees_taille=get('bilan_taille',envir_stacomi)@data"	Spanish
715	51	"Datos resultantes de la petición: don=get('bilan_taille',envir_stacomi)@requete@query"	Spanish
716	52	"Tienes que hacer los cálculos en primer lugar,hacer click en calcular"	Spanish
717	53	"Petición efectuada"	Spanish
718	54	"Se ha efectuado la petición para cargar las condicones ambientales"	Spanish
719	55	"Elige una estación de medida, después haz click sobre validar"	Spanish
720	56	"Algunas estaciones de medida no tienen valores asociados"	Spanish
721	57	"Las estaciónes de medidas selecionadas no tienen valores de condiciones ambientales (BilanConditionEnv.r)"	Spanish
722	58	"Estadísticas:"	Spanish
723	59	"El objeto Conclusión está almacenado en el entorno envir_stacomi, escribir bilanEspeces=get('bilanEspeces',envir_stacomi), "	Spanish
724	60	"Se deben de realizar los cálculos antes, haga click sobre calc"	Spanish
725	61	"Error en la petición sobre la vista vue_ope_lot_car "	Spanish
726	62	"Debemos lanzar los cálulos antes de hacer click sobre calc"	Spanish
727	63	"No hay ningún pez en la base de datos para este periodo "	Spanish
728	64	"Atención algunos cantidades negativas son transformados en positivas"	Spanish
729	65	"Verificación de los objetos y lanzamiento de la petición"	Spanish
730	66	"Se ha efectuado la petición para cargar los pasos de los tiempos de DC"	Spanish
731	67	"Se ha efectuado la petición para cargar los pasos de los tiempos de DC"	Spanish
732	68	"mes"	Spanish
733	69	"tiempo en horas"	Spanish
734	70	"Funcionamiento del dispositivo de recuento"	Spanish
735	71	"ver video"	Spanish
736	72	"parar video"	Spanish
737	73	"Escritura de tabla en el entorno  envir_stacomi : escribir periodeDC=get('periodeDC',envir_stacomi) "	Spanish
738	74	"DC"	Spanish
739	75	c("Fonc","Arr","Fonc normal")	Spanish
740	76	c("Fonc","Arr")	Spanish
741	77	"Funcionamiento DC"	Spanish
742	78	"Tipos de paradas de DC"	Spanish
743	79	"Escritura de"	Spanish
744	80	"puede tardar un poco, sea paciente..."	Spanish
745	81	"Se efectua la petición para cargar los pasos de los tiempos de DF"	Spanish
746	82	"No hay datos en este DF"	Spanish
747	83	"Construcción de gráfica, espere"	Spanish
748	84	"cálculo..."	Spanish
749	85	"progresión% "	Spanish
750	86	c("duree","type_fonct.","fonctionnement")	Spanish
751	87	"Funcionamiento DF"	Spanish
752	88	"escritura de tablas con el entorno envir_stacomi : escribir periodeDF=get('periodeDF',envir_stacomi) "	Spanish
753	89	"DF"	Spanish
754	90	"Tipos de paradas de DF"	Spanish
755	91	"Atención la elección de periodo no ha sido efectuada, cálculo con el valor por defecto"	Spanish
756	92	"Comienzo del Informe de migración… espere"	Spanish
757	93	"El objeto Conclusión está almacenado en el entorno envir_stacomi, escribir  ecrire bilanMigration=get('bilanMigration',envir_stacomi)"	Spanish
758	94	"Escritura de tabla de Conclusión de migraciones con el medio ambiente envir_stacomi : escribir tableau=get('tableau',envir_stacomi)"	Spanish
759	95	"Se deben de realizar los cálculos antes, haga click sobre calc"	Spanish
760	96	"Migración acumulada"	Spanish
761	97	"Cantidad acumulada"	Spanish
762	98	"Atención esta función es para los Conclusiones anuales"	Spanish
763	99	"Estadisticas concernientes a la migración"	Spanish
764	100	"El objeto Conclusión está almacenado por el entorno  envir_stacomi"	Spanish
765	101	"Hay que realizar los cálculos antes, haga click sobre calc"	Spanish
766	102	"No tiene condiciones medio ambientales en el periodo de tiempo"	Spanish
767	103	"no se ha seleccionado estación => gráfico simple"	Spanish
768	104	"el número de líneas de la tabla de condiciones ambientales ("	Spanish
769	105	")no corresponde a la duración de la Conclusión de la migración"	Spanish
770	106	"Atención: en una de las estaciones:"	Spanish
771	107	"Hay varios registros para el mismo día"	Spanish
772	108	"solo el primer valor será integrado en el informe"	Spanish
773	109	"Atención no existe Conclusión de migración para este año"	Spanish
774	110	",este taxón y este estadío (BilanMigrationInterAnnuelle.r)" 	Spanish
775	111	"Se ha efectuado la petición para cargar las migraciones de los años""	Spanish
776	112	"Cantidad"	Spanish
777	113	"ATENCIÓN:  extraiga una conclusión de Migración para al menos uno de los años seleccionados antes de lanzar un balance interanual" 	Spanish
778	114	"año"	Spanish
779	115	"fecha"	Spanish
780	116	"Porcentaje de la migración anual"	Spanish
782	118	"Escribiendo el objeto gráfico en el entorno envir_stacomi : escribir g=get("gi",envir_stacomi) avec "	Spanish
783	119	"Inicio del Informe de migración con parámetros… espere"	Spanish
784	120	"Tienes que elegir al menos una característica cuantitativa o cualitativa"	Spanish
785	121	"Atención, este tratamiento no ha sido efectuado sobre las cantidades de los lotes"	Spanish
786	122	"Escritura de los datos en el entorno envir_stacomi : escribir data=get('data',envir_stacomi) "	Spanish
787	123	"Tiene que realizar los cálculos antes"	Spanish
788	124	"Escribir el objeto gráfico en el entorno envir_stacomi : escribir g=get('g',envir_stacomi) "	Spanish
789	125	"Escritura de"	Spanish
790	126	"Se debe definir un vector baseODBC con el enlace ODBC, usuario y contraseña"	Spanish
791	127	"La biblioteca RODBC es necesaria, cargue el paquete!"	Spanish
792	128	"Conexión de prueba, esta clase solo debe de ser utilizada para las pruebas:"	Spanish
793	129	"No se puede conectar:"	Spanish
794	130	"Conexión establecida"	Spanish
795	131	"Conexión en curso"	Spanish
796	132	"Elección sin tiempo"	Spanish
797	133	"Seleccionar el número de tiempos"	Spanish
798	134	"Error interno: la tabla de paso del tiempo no contiene ninguna fila de tiempo"	Spanish
799	135	"Fecha de finalización"	Spanish
800	136	"La  duración del paso debe de ser diaria"	Spanish
801	137	"el paso del tiempo no debe comprender varios años"	Spanish
802	138	"Elección del periodo (duración 1 año)"	Spanish
803	139	"Fecha de inicio"	Spanish
804	140	"Paso del tiempo"	Spanish
805	141	"día Nb"	Spanish
806	142	"Fecha de finalización"	Spanish
807	143	" El paso  del tiempo ha sido cargado"	Spanish
808	144	"Selección del año"	Spanish
809	145	"Año seleccionado"	Spanish
810	146	"problema durante la carga de datos o no hay datos en la base (enlace ODBC?)"	Spanish
811	147	"El DC ha sido seleccionado"	Spanish
812	148	"selección de los taxones de DC (por el momento de todos los períodos)"	Spanish
813	149	"Datos en los Dispositivos de Contaje"	Spanish
814	150	"cerrar"	Spanish
815	151	"Selección de Dispositivo de Contaje"	Spanish
816	152	"Tabla"	Spanish
817	153	"Error: ningún DC se ejecuta en la base de datos (no hay ninguna línea de retorno de la solicitud)	Spanish
818	154	"El DF ha sido seleccionado"	Spanish
819	155	"Datos de los Dispositivos de Franqueo"	Spanish
820	156	"Elección del Dispositivo de Franqueo"	Spanish
821	157	"Ningún DF se ejecuta en la base  (no hay ninguna línea de retorno de la solicitud)"	Spanish
822	158	"La consulta se ha realizado para cargar los parámetros"	Spanish
823	159	"No hay datos para este DC, este taxón y esta etapa"	Spanish
824	160	"La característica ha sido seleccionada"	Spanish
825	161	"error interno, ninguna característica ha podido ser seleccionada para realizar la elección"	Spanish
826	162	"error interno, no puede haber una fila en Refparqual@data, or nbligne="	Spanish
827	163	0	Spanish
828	164	0	Spanish
829	165	0	Spanish
830	166	"No hay datos para este DC, y este taxón"	Spanish
831	167	"El estadío ha sido seleccionado"	Spanish
832	168	"Característica cualitativa"	Spanish
833	169	"Característica cuantitativa"	Spanish
834	170	"stop error interno: cargar los datos para hacer la elección"	Spanish
835	171	"Elección del estadío"	Spanish
836	172	"seleccione al menos un valor"	Spanish
837	173	"La estaciones de muestreo han sido seleccionadas"	Spanish
838	174	"Elección de estaciones de muestreo"	Spanish
839	175	"Stop no hay ningún dato de estación de muestreo (pb enlace ODBC?)"	Spanish
840	176	"El taxón ha sido seleccionado"	Spanish
841	177	"Selección de Taxón"	Spanish
842	178	"Stop no hay ninguna fila en la tabla de taxones (pb enlace ODBC?)"	Spanish
843	179	"Error ODBC=>hay que definir un vector base ODBC con el enlace ODBC, el usuario y la contraseña"	Spanish
844	180	"Prueba de conexión:"	Spanish
845	181	"Conexión imposible:"	Spanish
846	182	"Conectado con éxito"	Spanish
847	183	"Prueba de peticion"	Spanish
848	184	"Petición exitosa"	Spanish
849	185	"Una Conclusión ha sido escrita ya en la base:"	Spanish
850	186	"¿desea reemplazarlo?"	Spanish
958	294	"Conclusión lotes llamando a la vista vue lot ope"	Spanish
851	187	"escritura de la conclusión diaria en la base de datos"	Spanish
852	188	"% progresión"	Spanish
853	189	"escritura de la conclusión diaria en la base de datos"	Spanish
854	190	"escritura de la conclusión mensual en la base de datos"	Spanish
855	191	"gráfica de la angula:"	Spanish
856	192	"Cantidad angulas (x1000)"	Spanish
857	193	"Cantidad estimada,"	Spanish
858	194	c("eff. journ. poids","eff. journ. Compt.")	Spanish
859	195	"número de operaciones="	Spanish
971	307	"Operación (TODO)"	Spanish
860	196	"duración media de la captura="	Spanish
861	197	"duración max="	Spanish
862	198	"duración min="	Spanish
863	199	c("Fonc","Arr","Fonc normal")	Spanish
864	200	c("Fonc","Arr")	Spanish
865	201	"DF"	Spanish
866	202	"DC"	Spanish
867	203	"OP"	Spanish
868	204	"Mes"	Spanish
869	205	"Cantidad (x1000)	Spanish
870	206	c("eff. mens. poids","eff. mens. Compt.")	Spanish
871	207	"Cantidad"	Spanish
872	208	"Escritura el objeto gráfico en el entorno envir_stacomi : escribir g=get("g",envir_stacomi) "	Spanish
873	209	"ATENCIÓN, hay cantidad de entradas de lotes de un taxón diferente a angulas, comprobar"	Spanish
874	210	"cantidad"	Spanish
875	211	"fecha"	Spanish
876	212	"Cantidad estimada"	Spanish
877	213	c("Ponctuel","expert","calcule","mesure")	Spanish
878	214	"suma cantidades:"	Spanish
879	215	c("Effectifs","type","duree","mois","quinzaine","semaine","jour_365")	Spanish
880	216	"calculo de las conclusiones mensuales"	Spanish
881	217		Spanish
882	218	"escritura de"	Spanish
883	219	"Conversión de la cantidad de peso"	Spanish
884	220	"Atención suma==0, no ha introducido el coeficiente de conversión"	Spanish
885	221	"carga de la vista (vue_ope_lot) y elección del dc y del periodo de tiempo"	Spanish
886	222	"CONCLUSIÓN LOTES"	Spanish
887	223	"fecha y hora de inicio"	Spanish
888	224	"fecha y hora de fin"	Spanish
889	225	"La fecha de inicio ha sido elegida"	Spanish
890	226	"La fecha de fin ha sido elegida"	Spanish
892	228	"Tabla"	Spanish
893	229	"Salir"	Spanish
899	235		Spanish
900	236		Spanish
901	237	"Petición cruzada talla/caract cualitativa"	Spanish
902	238	"Carga de las estaciones de muestreo"	Spanish
903	239	"gráfica conclusiones"	Spanish
904	240	"tabla conclusiones en .csv"	Spanish
905	241	"Salir"	Spanish
906	242	"Conclusión de las especies presentes en el DC"	Spanish
907	243	"Conclusión de las especies"	Spanish
908	244	"Cargando"	Spanish
909	245	"Gráfico de PieChart"	Spanish
910	246	"Histograma"	Spanish
911	247	"Tablas de Conclusión en .csv y XML"	Spanish
912	248	"Elección de recorte"	Spanish
913	249	"Carga de listas de taxones, estadíos y dc"	Spanish
914	250	"Cálculo de la cantidad por periodos"	Spanish
915	251	"Gráfica conclusiones"	Spanish
916	252	"Gráfico de acumulación"	Spanish
917	253	"Tabla Conclusiones en .csv"	Spanish
918	254	"cálculo de las condiciones ambientales por  periodos"	Spanish
919	255	"Carga de los taxones y los estadíos y estaciones dc y de muestreo"	Spanish
920	256	"carga de conclusionesDiarias existentes"	Spanish
921	257	"El año de inicio ha sido elegido"	Spanish
922	258	"Año de inicio"	Spanish
923	259	"El año final ha sido elegido"	Spanish
924	260	"Año final"	Spanish
925	261	"Migración de todos los años en el mismo gráfico"	Spanish
926	262	"Acumulación de migradores en %"	Spanish
927	263	"Tabla"	Spanish
928	264	"Salir"	Spanish
929	265	"día"	Spanish
930	266	"Migración diaria"	Spanish
931	267	"sem"	Spanish
932	268	"Migración semanal"	Spanish
933	269	"quin"	Spanish
934	270	"Migración quincenal"	Spanish
935	271	"mes"	Spanish
936	272	"Migración mensual"	Spanish
937	273	"Carga de listas de taxones, estadíos, DC, parámetros cualitativos y cuantitativos"	Spanish
938	274	"Elegir tipo de lote, incluir las muestras?"	Spanish
939	275	"Cálculo del número por periodo"	Spanish
940	276	"gráfica mensual"	Spanish
941	277	"gráfica diaria"	Spanish
942	278	"tabla informe en .csv"	Spanish
943	279	"Salir"	Spanish
944	280	"Carga de listas dc y elección de periodo"	Spanish
945	281	"gráfica mensual"	Spanish
946	282	"Diagrama de barras"	Spanish
947	283	"Tabla"	Spanish
948	284	"Salir"	Spanish
949	285	"Cálculo del funcionamiento de dc"	Spanish
950	286	"Cálculo del funcionamiento de df"	Spanish
951	287	"Conclusión de las operaciones de un dispositivo… a desarrollar"	Spanish
952	288	"Conclusión de cruces de funcionamiento de DF y DC, a desarrollar"	Spanish
953	289	"Conclusión migración (para una especie y un estadío)	Spanish
954	290	"Conclusión migración interanual"	Spanish
955	291	"Conclusión migración condiciones ambientales"	Spanish
956	292	"Conclusión migración con los parámetros"	Spanish
957	293	"Conclusión de condiciones ambientales"	Spanish
896	232	"table"	Spanish
895	231	"load"	Spanish
897	233	"salir"	Spanish
959	295	"Conclusión tallas"	Spanish
961	297	"Calcular estadíos pigmentarios"	Spanish
962	298	pegar ("para ayuda cedric Briand - 02 99 90 88 44 - cedric.briand@lavilaine.com"	Spanish
963	299	"Lanzamiento Rcmdr (datos del menú, conjunto de datos activos, seleccione el conjunto de datos activos"	Spanish
964	300	"Tratamiento migratorio"	Spanish
965	301	"TODO a desarrollar"	Spanish
966	302	"salidas del programa"	Spanish
967	303	"conclusión de especies de DC"	Spanish
968	304	"Estación"	Spanish
969	305	"DF"	Spanish
970	306	"DC"	Spanish
972	308	"DF sin DC (TODO)"	Spanish
973	309	"Conclusión"	Spanish
974	310	"Migración"	Spanish
975	311	"Cond. Env."	Spanish
976	312	"Migr.~Cond. Env."	Spanish
977	313	"Migr./ parm. quant / parm. qual"	Spanish
978	314	"Migr. interanual"	Spanish
979	315	"Parm. de lot"	Spanish
980	316	"Pesos Medios angula"	Spanish
981	317	"Tallas"	Spanish
982	318	"Estadíos pigmentarios"	Spanish
983	319	"Migr. Juveniles (TODO)"	Spanish
985	321	"parm qual (proporciones) (TODO)"	Spanish
986	322	"Especies"	Spanish
987	323	"Ayuda"	Spanish
988	324	"Conexión"	Spanish
989	325	"Usuario"	Spanish
990	326	"Contraseña"	Spanish
991	327	"Registro"	Spanish
992	328	"Error" título del marco	Spanish
993	329	"Problema al comprobar la conexión ODBC"	Spanish
994	330	"Error en el uso del método de conexión de la clase ConexiónODBC"	Spanish
995	331	"Problemas durante la prueba, la conexión ODBC funciona, pero no señala a la base de la versión 0.3, verificar el enlace ODBC""	Spanish
996	332	"Enlace ODBC"	Spanish
997	333	"There are no values for the taxa, stage and selected period"	English
998	333	"Il n'y a pas d'effectif pour le taxon, le stade, et la période sélectionnée"	French
999	333	"please translate this : Il n'y a pas d'effectif pour le taxon, le stade, et la période sélectionnée"	Spanish
1000	334	"Ecriture du bilanMigrationInterannuelle dans l'environnement envir_stacomi : ecrire bmi=get('bilanMigrationInterannuelle',envir_stacomi) "	French
1001	334	"please translate this : Writing bilanMigrationInterannuelle in the environment envir_stacomi : write bmi=get('bilanMigrationInterannuelle',envir_stacomi) "	Spanish
1002	334	"Writing bilanMigrationInterannuelle in the environment envir_stacomi : write bmi=get('bilanMigrationInterannuelle',envir_stacomi) "	English
1003	336	Jours	French
1004	336	Days	English
1005	336	Dias	Spanish
652	320	"Multiple migration report"	English
984	320	"Informes de migración múltiples "	Spanish
1006	342	"Bilan migration pour plusieurs DC, taxons, ou stades"	French
1007	342	"Daily migration for several DC, species, or stage"	English
1008	342	"La migración al día durante varios DC, especie, o etapa de la vida"	Spanish
562	230	"Mean weight report"	English
894	230	"Informes de pesos moyen "	Spanish
628	296	"Summary of average weight for the calculation of the relation between length and number."	English
960	296	"conclusión de pesos medios para calcular la relaciones de las cantidades de pesos"	Spanish
566	234	"choice of number in sample (one, several,all)"	English
898	234	"elección de número en la muestra (uno, varios o todos)"	Spanish
1010	343	Attention vous n'avez probablement pas rentre tous les coefficients dans la base, vérifier car les poids depuis effectif ou effectifs depuis poids seront faux	French
1011	343	Attention there are probably missing coefficients in the database, verify or the conversion from number to weights and weights to number might be wrong	English
1012	343	La atención es probable que haya perdido los coeficientes de la base de datos, verificar o la conversión de número a los pesos y pesos con el número podría estar equivocado	Spanish
117	117	", Effectifs cumulés"	French
449	117	", Cumulated numbers"	English
781	117	", Acumulado real"	Spanish
559	227	"dotplot"	English
891	227	"dotplot"	Spanish
1013	344	"boxplot"	French
1014	344	"boxplot"	English
1015	344	"boxplot"	Spanish
1016	345	"densité"	French
1017	345	"density"	English
1018	345	"density"	Spanish
227	227	"dotplot"	French
16	16	"Pour recuperer l'objet graphique, tapper : g, voir http://trac.eptb-vilaine.fr:8066/tracstacomi/wiki/Recette%20BilanLot pour de l'aide"	French
348	16	"To obtain the graphical object, type : g, see http://trac.eptb-vilaine.fr:8066/tracstacomi/wiki/Recette%20BilanLot for help"	English
680	16	"Para recuperar el objeto gráfico, escribir: g, see http://trac.eptb-vilaine.fr:8066/tracstacomi/wiki/Recette%20BilanLot for help"	Spanish
\.


--
-- Name: c_uk2_ts_messagerlang_mrl; Type: CONSTRAINT; Schema: ref; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ts_messagerlang_mrl
    ADD CONSTRAINT c_uk2_ts_messagerlang_mrl UNIQUE (mrl_msr_id, mrl_lang);


--
-- Name: c_uk_ts_messager_msr; Type: CONSTRAINT; Schema: ref; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ts_messager_msr
    ADD CONSTRAINT c_uk_ts_messager_msr UNIQUE (msr_element, msr_number);


--
-- Name: c_uk_ts_messagerlang_mrl; Type: CONSTRAINT; Schema: ref; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ts_messagerlang_mrl
    ADD CONSTRAINT c_uk_ts_messagerlang_mrl UNIQUE (mrl_id, mrl_lang);


--
-- Name: ts_messager_msr_pkey; Type: CONSTRAINT; Schema: ref; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ts_messager_msr
    ADD CONSTRAINT ts_messager_msr_pkey PRIMARY KEY (msr_id);


--
-- Name: ts_messagerlang_mrl_pkey; Type: CONSTRAINT; Schema: ref; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ts_messagerlang_mrl
    ADD CONSTRAINT ts_messagerlang_mrl_pkey PRIMARY KEY (mrl_id);


--
-- Name: c_fk_mrl_msr_id; Type: FK CONSTRAINT; Schema: ref; Owner: postgres
--

ALTER TABLE ONLY ts_messagerlang_mrl
    ADD CONSTRAINT c_fk_mrl_msr_id FOREIGN KEY (mrl_msr_id) REFERENCES ts_messager_msr(msr_id);


--
-- Name: ts_messager_msr; Type: ACL; Schema: ref; Owner: postgres
--

REVOKE ALL ON TABLE ts_messager_msr FROM PUBLIC;
REVOKE ALL ON TABLE ts_messager_msr FROM postgres;
GRANT ALL ON TABLE ts_messager_msr TO postgres;
GRANT SELECT ON TABLE ts_messager_msr TO iav;
GRANT SELECT ON TABLE ts_messager_msr TO invite;
GRANT SELECT ON TABLE ts_messager_msr TO inra;
GRANT SELECT ON TABLE ts_messager_msr TO logrami;
GRANT SELECT ON TABLE ts_messager_msr TO mrm;
GRANT SELECT ON TABLE ts_messager_msr TO migado;
GRANT SELECT ON TABLE ts_messager_msr TO saumonrhin;
GRANT SELECT ON TABLE ts_messager_msr TO migradour;
GRANT SELECT ON TABLE ts_messager_msr TO charente;
GRANT SELECT ON TABLE ts_messager_msr TO fd80;
GRANT SELECT ON TABLE ts_messager_msr TO smatah;
GRANT SELECT ON TABLE ts_messager_msr TO azti;
GRANT SELECT ON TABLE ts_messager_msr TO bgm;


--
-- Name: ts_messagerlang_mrl; Type: ACL; Schema: ref; Owner: postgres
--

REVOKE ALL ON TABLE ts_messagerlang_mrl FROM PUBLIC;
REVOKE ALL ON TABLE ts_messagerlang_mrl FROM postgres;
GRANT ALL ON TABLE ts_messagerlang_mrl TO postgres;
GRANT SELECT ON TABLE ts_messagerlang_mrl TO iav;
GRANT SELECT ON TABLE ts_messagerlang_mrl TO invite;
GRANT SELECT ON TABLE ts_messagerlang_mrl TO inra;
GRANT SELECT ON TABLE ts_messagerlang_mrl TO logrami;
GRANT SELECT ON TABLE ts_messagerlang_mrl TO mrm;
GRANT SELECT ON TABLE ts_messagerlang_mrl TO migado;
GRANT SELECT ON TABLE ts_messagerlang_mrl TO saumonrhin;
GRANT SELECT ON TABLE ts_messagerlang_mrl TO migradour;
GRANT SELECT ON TABLE ts_messagerlang_mrl TO charente;
GRANT SELECT ON TABLE ts_messagerlang_mrl TO fd80;
GRANT SELECT ON TABLE ts_messagerlang_mrl TO smatah;
GRANT SELECT ON TABLE ts_messagerlang_mrl TO azti;
GRANT SELECT ON TABLE ts_messagerlang_mrl TO bgm;


--
-- PostgreSQL database dump complete
--

