﻿
GRANT ALL ON SCHEMA bgm TO bgm;
GRANT SELECT,INSERT,UPDATE ON bgm.t_station_sta TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.t_bilanmigrationjournalier_bjo TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.t_bilanmigrationmensuel_bme TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.tj_caracteristiquelot_car TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.tj_conditionenvironnementale_env TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.tj_stationmesure_stm TO bgm;
GRANT SELECT ON ref.tr_valeurparametrequalitatif_val TO bgm;
GRANT SELECT ON ref.tr_parametrequalitatif_qal TO bgm;
GRANT SELECT ON ref.tr_parametrequantitatif_qan TO bgm;
GRANT SELECT ON ref.tg_parametre_par TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.tj_dfesttype_dft TO bgm;
GRANT SELECT ON ref.tr_typedf_tdf TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.tj_dfestdestinea_dtx TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.tj_tauxechappement_txe TO bgm;
GRANT SELECT ON ref.tr_niveauechappement_ech TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.tj_coefficientconversion_coe TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.tj_pathologieconstatee_pco TO bgm;
GRANT SELECT ON ref.tr_pathologie_pat TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.tj_actionmarquage_act TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.t_marque_mqe TO bgm;
GRANT SELECT ON ref.tr_localisationanatomique_loc TO bgm;
GRANT SELECT ON ref.tr_naturemarque_nmq TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.t_operationmarquage_omq TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.t_lot_lot TO bgm;
GRANT SELECT ON ref.tr_devenirlot_dev TO bgm;
GRANT SELECT ON ref.tr_typequantitelot_qte TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.ts_taxonvideo_txv TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.ts_taillevideo_tav TO bgm;
GRANT SELECT ON ref.tr_stadedeveloppement_std TO bgm;
GRANT SELECT ON ref.tr_taxon_tax TO bgm;
GRANT SELECT ON ref.tr_niveautaxonomique_ntx TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.t_operation_ope TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.t_periodefonctdispositif_per TO bgm;
GRANT SELECT ON ref.tr_typearretdisp_tar TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.t_dispositifcomptage_dic TO bgm;
GRANT SELECT ON ref.tr_typedc_tdc TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.t_dispositiffranchissement_dif TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.tg_dispositif_dis TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.t_ouvrage_ouv TO bgm;
GRANT SELECT ON ref.tr_natureouvrage_nov TO bgm;
GRANT SELECT ON ref.tr_prelevement_pre TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON ref.ts_sequence_seq TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.tj_stationmesure_stm TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.tj_prelevementlot_prl TO bgm;
GRANT SELECT,INSERT,UPDATE,DELETE ON bgm.t_marque_mqe TO bgm;

GRANT SELECT, UPDATE ON bgm.tg_dispositif_dis_dis_identifiant_seq TO bgm;
GRANT SELECT, UPDATE ON bgm.t_lot_lot_lot_identifiant_seq TO bgm;
GRANT SELECT, UPDATE ON bgm.t_operation_ope_ope_identifiant_seq TO bgm;
GRANT SELECT, UPDATE ON bgm.t_ouvrage_ouv_ouv_identifiant_seq TO bgm;
GRANT SELECT, UPDATE ON bgm.tj_stationmesure_stm_stm_identifiant_seq TO bgm;
GRANT SELECT, UPDATE ON bgm.t_bilanmigrationmensuel_bme_bme_identifiant_seq TO bgm;
GRANT SELECT, UPDATE ON bgm.t_bilanmigrationjournalier_bjo_bjo_identifiant_seq TO bgm;

GRANT SELECT ON ref.ts_sequence_seq TO bgm;
GRANT SELECT ON TABLE ref.ts_sequence_seq TO invite;

-- vues
GRANT SELECT, UPDATE, INSERT ON TABLE bgm.v_taxon_tax TO bgm;
GRANT SELECT, UPDATE, INSERT ON TABLE bgm.vue_lot_ope_car TO bgm;
GRANT SELECT, UPDATE, INSERT ON TABLE bgm.vue_lot_ope_car_qan TO bgm;
GRANT SELECT, UPDATE, INSERT ON TABLE bgm.vue_ope_lot_ech_parqual TO bgm;
GRANT SELECT, UPDATE, INSERT ON TABLE bgm.vue_ope_lot_ech_parquan TO bgm;



GRANT SELECT ON bgm.t_station_sta TO invite;
GRANT SELECT ON bgm.t_bilanmigrationjournalier_bjo TO invite;
GRANT SELECT ON bgm.t_bilanmigrationmensuel_bme TO invite;
GRANT SELECT ON bgm.tj_caracteristiquelot_car TO invite;
GRANT SELECT ON bgm.tj_conditionenvironnementale_env TO invite;
GRANT SELECT ON bgm.tj_stationmesure_stm TO invite;
GRANT SELECT ON ref.tr_valeurparametrequalitatif_val TO invite;
GRANT SELECT ON ref.tr_parametrequalitatif_qal TO invite;
GRANT SELECT ON ref.tr_parametrequantitatif_qan TO invite;
GRANT SELECT ON ref.tg_parametre_par TO invite;
GRANT SELECT ON bgm.tj_dfesttype_dft TO invite;
GRANT SELECT ON ref.tr_typedf_tdf TO invite;
GRANT SELECT ON bgm.tj_dfestdestinea_dtx TO invite;
GRANT SELECT ON bgm.tj_tauxechappement_txe TO invite;
GRANT SELECT ON ref.tr_niveauechappement_ech TO invite;
GRANT SELECT ON bgm.tj_coefficientconversion_coe TO invite;
GRANT SELECT ON bgm.tj_pathologieconstatee_pco TO invite;
GRANT SELECT ON ref.tr_pathologie_pat TO invite;
GRANT SELECT ON bgm.tj_actionmarquage_act TO invite;
GRANT SELECT ON bgm.t_marque_mqe TO invite;
GRANT SELECT ON ref.tr_localisationanatomique_loc TO invite;
GRANT SELECT ON ref.tr_naturemarque_nmq TO invite;
GRANT SELECT ON bgm.t_operationmarquage_omq TO invite;
GRANT SELECT ON bgm.t_lot_lot TO invite;
GRANT SELECT ON ref.tr_devenirlot_dev TO invite;
GRANT SELECT ON ref.tr_typequantitelot_qte TO invite;
GRANT SELECT ON bgm.ts_taxonvideo_txv TO invite;
GRANT SELECT ON bgm.ts_taillevideo_tav TO invite;
GRANT SELECT ON ref.tr_stadedeveloppement_std TO invite;
GRANT SELECT ON ref.tr_taxon_tax TO invite;
GRANT SELECT ON ref.tr_niveautaxonomique_ntx TO invite;
GRANT SELECT ON bgm.t_operation_ope TO invite;
GRANT SELECT ON bgm.t_periodefonctdispositif_per TO invite;
GRANT SELECT ON ref.tr_typearretdisp_tar TO invite;
GRANT SELECT ON bgm.t_dispositifcomptage_dic TO invite;
GRANT SELECT ON ref.tr_typedc_tdc TO invite;
GRANT SELECT ON bgm.t_dispositiffranchissement_dif TO invite;
GRANT SELECT ON bgm.tg_dispositif_dis TO invite;
GRANT SELECT ON bgm.t_ouvrage_ouv TO invite;
GRANT SELECT ON ref.tr_natureouvrage_nov TO invite;
GRANT SELECT ON bgm.t_station_sta TO invite;
GRANT SELECT ON ref.tr_prelevement_pre TO invite;
GRANT SELECT ON ref.ts_organisme_org TO invite;
GRANT SELECT ON ref.ts_sequence_seq TO invite;
GRANT SELECT ON bgm.tj_stationmesure_stm TO invite;
GRANT SELECT ON bgm.tj_prelevementlot_prl TO invite;
GRANT SELECT ON bgm.t_marque_mqe TO invite;
--vues
GRANT SELECT ON bgm.v_taxon_tax TO invite;
GRANT SELECT ON bgm.vue_lot_ope_car TO invite;
GRANT SELECT ON bgm.vue_lot_ope_car_qan TO invite;
GRANT SELECT ON bgm.vue_ope_lot_ech_parqual TO invite;
GRANT SELECT ON bgm.vue_ope_lot_ech_parquan TO invite;
-- sequences
GRANT SELECT, UPDATE ON bgm.tg_dispositif_dis_dis_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON bgm.t_lot_lot_lot_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON bgm.t_operation_ope_ope_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON bgm.t_ouvrage_ouv_ouv_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON bgm.tj_stationmesure_stm_stm_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON bgm.t_bilanmigrationmensuel_bme_bme_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON bgm.t_bilanmigrationjournalier_bjo_bjo_identifiant_seq TO invite;

--ticket 40

--c_fk_dic_dis_identifiant
Alter table bgm.t_dispositifcomptage_dic ADD CONSTRAINT c_fk_dic_dis_identifiant 
	FOREIGN KEY (dic_dis_identifiant,dic_org_code) 
	REFERENCES bgm.tg_dispositif_dis (dis_identifiant,dis_org_code);
-- la contrainte de table est construite sur le code alors qu'elle devrait l'être sur l'identifiant
-- je la supprime	
Alter table bgm.t_dispositiffranchissement_dif DROP CONSTRAINT c_pk_dif ;
-- je la recrée avec les bonnes valeurs
Alter table bgm.t_dispositiffranchissement_dif ADD CONSTRAINT c_pk_dif PRIMARY KEY (dif_dis_identifiant, dif_org_code);
-- pareil pour les dispositifs de comptage
Alter table bgm.t_dispositifcomptage_dic DROP CONSTRAINT c_pk_dic ;
-- 
Alter table bgm.t_dispositifcomptage_dic ADD CONSTRAINT c_pk_dic PRIMARY KEY (dic_dis_identifiant, dic_org_code);

--c_fk_dic_dif_identifiant     		
Alter table bgm.t_dispositifcomptage_dic ADD CONSTRAINT c_fk_dic_dif_identifiant 
	FOREIGN KEY (dic_dif_identifiant,dic_org_code) 
	REFERENCES bgm.t_dispositiffranchissement_dif (dif_dis_identifiant,dif_org_code);
--c_fk_dif_ouv_identifiant 
Alter table bgm.t_dispositiffranchissement_dif ADD CONSTRAINT c_fk_dif_ouv_identifiant 
	FOREIGN KEY (dif_ouv_identifiant,dif_org_code) 
	REFERENCES bgm.t_ouvrage_ouv (ouv_identifiant,ouv_org_code);
-- c_fk_dif_dis_identifiant	
Alter table bgm.t_dispositiffranchissement_dif ADD CONSTRAINT c_fk_dif_dis_identifiant	
	FOREIGN KEY (dif_dis_identifiant,dif_org_code) 
	REFERENCES bgm.tg_dispositif_dis (dis_identifiant,dis_org_code);
-- c_fk_dtx_dif_identifiant
Alter table bgm.tj_dfestdestinea_dtx ADD CONSTRAINT c_fk_dtx_dif_identifiant	
	FOREIGN KEY (dtx_dif_identifiant,dtx_org_code) 
	REFERENCES bgm.tg_dispositif_dis (dis_identifiant,dis_org_code);
--c_fk_dft_df_identifiant
Alter table bgm.tj_dfesttype_dft ADD CONSTRAINT c_fk_dft_df_identifiant	
	FOREIGN KEY (dft_df_identifiant,dft_org_code) 
	REFERENCES bgm.tg_dispositif_dis (dis_identifiant,dis_org_code);
--c_fk_prl_lot_identifiant
-- pour la table des prélèvements il manque les organismes il faut faire les modifs qui suivent
Alter table bgm.tj_prelevementlot_prl add column prl_org_code character varying(30) NOT NULL;
Alter table bgm.tj_prelevementlot_prl DROP CONSTRAINT c_pk_prl;
Alter table bgm.tj_prelevementlot_prl ADD CONSTRAINT c_pk_prl PRIMARY KEY (prl_pre_typeprelevement, prl_lot_identifiant, prl_code,prl_org_code);
Alter table bgm.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_lot_identifiant	
	FOREIGN KEY (prl_lot_identifiant,prl_org_code) 
	REFERENCES bgm.t_lot_lot (lot_identifiant,lot_org_code);
--Le nom ci dessous c'est pas terrible cht de c_fk_prl_pre_nom a c_fk_prl_pre_typeprelevement;
ALTER TABLE bgm.tj_prelevementlot_prl DROP CONSTRAINT c_fk_prl_pre_nom;
ALTER TABLE bgm.tj_prelevementlot_prl ADD CONSTRAINT 
 c_fk_prl_typeprelevement FOREIGN KEY (prl_pre_typeprelevement)
      REFERENCES ref.tr_prelevement_pre (pre_typeprelevement) MATCH SIMPLE;

-- c_fk_tav_dic_identifiant
Alter table bgm.ts_taillevideo_tav ADD CONSTRAINT c_fk_tav_dic_identifiant	
	FOREIGN KEY (tav_dic_identifiant,tav_org_code) 
	REFERENCES bgm.t_dispositifcomptage_dic (dic_dis_identifiant,dic_org_code);
     
-- ticket 40-42
ALTER TABLE bgm.t_operation_ope OWNER TO bgm;
ALTER TABLE bgm.t_lot_lot OWNER TO bgm;
ALTER TABLE bgm.t_bilanmigrationjournalier_bjo OWNER TO bgm;
ALTER TABLE bgm.t_bilanmigrationmensuel_bme OWNER TO bgm;
ALTER TABLE bgm.t_ouvrage_ouv OWNER TO bgm;
ALTER TABLE bgm.tg_dispositif_dis OWNER TO bgm;
ALTER TABLE bgm.tj_stationmesure_stm OWNER TO bgm;
GRANT SELECT ON ref.tr_typedf_tdf TO bgm;

-- ticket59

CREATE TABLE bgm.ts_maintenance_main(
main_identifiant serial PRIMARY KEY,
main_ticket integer,
main_description text);
COMMENT ON  TABLE bgm.ts_maintenance_main IS 'Table de suivi des operations de maintenance de la base';

INSERT INTO bgm.ts_maintenance_main( main_ticket,main_description ) VALUES (59,'creation de la table de maintenance');
INSERT INTO bgm.ts_maintenance_main( main_ticket,main_description ) VALUES (40,'ajout des clé étrangères manquantes');
INSERT INTO bgm.ts_maintenance_main( main_ticket,main_description ) VALUES (42,'modification des propriétaires sur les tables à séquence et grant select sur ref.tr_typedf_tdf oublié');
INSERT INTO bgm.ts_maintenance_main( main_ticket,main_description ) VALUES (67,'org code rajouté dans les tables t_operationmarquage_omq, tj_coefficientconversion_coe,tj_prelevementlot_prl');

--Ticket 67 script update one organism

alter table bgm.tj_prelevementlot_prl  drop CONSTRAINT c_pk_prl;
alter table bgm.tj_prelevementlot_prl add prl_org_code character varying(30) NOT NULL;
alter table bgm.tj_prelevementlot_prl add constraint c_pk_prl PRIMARY KEY (prl_pre_typeprelevement, prl_lot_identifiant, prl_code,prl_org_code);
ALTER TABLE bgm.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_org_code FOREIGN KEY (prl_org_code) REFERENCES ref.ts_organisme_org(org_code);

-- si des données sont déjà présentes
/*
alter table bgm.tj_prelevementlot_prl  drop CONSTRAINT c_pk_prl;
alter table bgm.tj_prelevementlot_prl add prl_org_code character varying(30);
update bgm.tj_prelevementlot_prl set prl_org_code='bgm';
alter table bgm.tj_prelevementlot_prl ADD constraint c_nn_prl_org_code check (prl_org_code IS NOT NULL);
alter table bgm.tj_prelevementlot_prl add constraint c_pk_prl PRIMARY KEY (prl_pre_typeprelevement, prl_lot_identifiant, prl_code,prl_org_code);
ALTER TABLE bgm.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_org_code FOREIGN KEY (prl_org_code) REFERENCES ref.ts_organisme_org(org_code);
*/
alter table bgm.tj_coefficientconversion_coe  drop CONSTRAINT c_pk_coe;
alter table bgm.tj_coefficientconversion_coe add coe_org_code character varying(30) NOT NULL;
alter table bgm.tj_coefficientconversion_coe add constraint c_pk_coe PRIMARY KEY (coe_tax_code, coe_std_code, coe_qte_code, coe_date_debut, coe_date_fin,coe_org_code);
ALTER TABLE bgm.tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code) REFERENCES ref.ts_organisme_org(org_code);


-- si des données sont déjà présentes
/*
alter table bgm.tj_coefficientconversion_coe  drop CONSTRAINT c_pk_coe;
alter table bgm.tj_coefficientconversion_coe add coe_org_code character varying(30);
update bgm.tj_coefficientconversion_coe set coe_org_code='bgm';
alter table bgm.tj_coefficientconversion_coe ADD constraint c_nn_coe_org_code check (coe_org_code IS NOT NULL);
alter table bgm.tj_coefficientconversion_coe add constraint c_pk_coe PRIMARY KEY (coe_tax_code, coe_std_code, coe_qte_code, coe_date_debut, coe_date_fin,coe_org_code);
ALTER TABLE bgm.tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code) REFERENCES ref.ts_organisme_org(org_code);
*/

alter table bgm.t_operationmarquage_omq  drop CONSTRAINT c_pk_omg CASCADE;
alter table bgm.t_operationmarquage_omq add omq_org_code character varying(30) NOT NULL;
alter table bgm.t_operationmarquage_omq ADD CONSTRAINT c_pk_omq PRIMARY KEY (omq_reference,omq_org_code);
ALTER TABLE bgm.t_operationmarquage_omq ADD CONSTRAINT c_fk_omq_org_code FOREIGN KEY (omq_org_code) REFERENCES ref.ts_organisme_org(org_code);
ALTER TABLE bgm.t_marque_mqe ADD CONSTRAINT c_fk_mqe_omq_reference FOREIGN KEY (mqe_omq_reference,mqe_org_code)
      REFERENCES bgm.t_operationmarquage_omq (omq_reference,omq_org_code) ;

-- mise à jour de la table de maintenance

INSERT INTO bgm.ts_maintenance_main( main_ticket,main_description) VALUES (72,'creation d''une ref.ts_messagerlang_mrl  pour l''internationalisation');
INSERT INTO bgm.ts_maintenance_main( main_ticket,main_description) VALUES (74,'Insertion de deux nouveaux paramètres');
