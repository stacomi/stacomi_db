-- Table: ts_taillevideo_tav

-- DROP TABLE ts_taillevideo_tav;

CREATE TABLE ts_taillevideo_tav
(
  tav_dic_identifiant integer NOT NULL,
  tav_coefconversion numeric NOT NULL,
  tav_distance character varying(1),
  CONSTRAINT c_pk_tav PRIMARY KEY (tav_distance,tav_dic_identifiant),
  CONSTRAINT c_fk_tav_dic FOREIGN KEY(tav_dic_identifiant) REFERENCES t_dispositifcomptage_dic (dic_dis_identifiant) 
 ) 
WITH OIDS;
ALTER TABLE ts_taillevideo_tav OWNER TO postgres;


