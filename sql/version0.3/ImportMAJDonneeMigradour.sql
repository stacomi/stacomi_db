﻿-- Script import des données de MAJ de Migradour (base ACCESS)
-- @uteur Marion LEGRAND
-- Date : 13/09/2016

SET search_path TO adourtemp, public;

--Import des données de la base access dans le schema adourtemp
	--operations
drop table if exists adourtemp.operations;
CREATE TABLE adourtemp.operations
(
  numopera character varying(12) NOT NULL,
  numoperaancien character varying(12),
  num1 character varying(4),
  num2 character varying(5),
  num3 character varying(8),
  typcont integer,
  codesta character varying(30),
  date timestamp without time zone,
  datea timestamp without time zone,
  heura timestamp without time zone,
  dateheurearmement character varying(30),
  dater timestamp without time zone,
  heurer timestamp without time zone,
  dateheuredesarmement timestamp without time zone,
  etatpasse integer,
  etatpiege1 integer,
  etatpiege2 integer,
  etatpiege_r text,
  piegeur integer,
  tempair numeric,
  tempeau numeric,
  condhydro integer,
  tenddebit integer,
  turbidite integer,
  atmos integer,
  devbar integer,
  creation timestamp without time zone,
  auteur_creation character varying(30),
  pbdate boolean DEFAULT false,
  CONSTRAINT operations_pkey PRIMARY KEY (numopera)
)
WITH (
  OIDS=FALSE
);
set client_encoding to 'WIN1252';
copy adourtemp.operations from 'C:/Mes Documents/Tableau de bord/PROJETS/These/Donnees/STACOMI/Migradour/MAJ_sept2016/operations.csv' with csv header delimiter as ';';--38029 contre 32742  1ere version

	-- piegeurs
set client_encoding to 'UTF8';

drop table if exists adourtemp."Piegeurs";
CREATE TABLE adourtemp."Piegeurs"
(
  "RefPiegeurs" integer,
  "Nom" character varying(50),
  "Prénom" character varying(50)
)
WITH (
  OIDS=FALSE
);
set client_encoding to 'WIN1252';
copy adourtemp."Piegeurs" from 'C:/Mes Documents/Tableau de bord/PROJETS/These/Donnees/STACOMI/Migradour/MAJ_sept2016/Piegeurs.csv' with csv delimiter as ';';--16

	-- Rivières
set client_encoding to 'UTF8';

drop table if exists adourtemp."Rivieres";
CREATE TABLE adourtemp."Rivieres"
(
  "MNEMONIQUE" character varying(50),
  "RIVIERE" character varying(50),
  "AFFLUENT" character varying(50),
  "AGENCE" character varying(2),
  "MILAQUAT" character varying(1),
  "FLAGBHP" character varying(50),
  "TRANSFER" text,
  "NIVEAU" smallint,
  "LG_KM" real,
  "B0" character varying(25),
  "B1" character varying(25),
  "B2" character varying(25),
  "B3" character varying(25),
  "B4" character varying(25),
  "B5" character varying(25),
  "B6" character varying(25),
  "B7" character varying(25),
  "B8" character varying(25),
  "Alti-source (m)" real,
  "Localisa source" character varying(100),
  "Alti-confluence (m)" real,
  "9" character(1),
  "11" character(1),
  "12" character(1),
  "24" character(1),
  "31" character(1),
  "32" character(1),
  "33" character(1),
  "40" character(1),
  "46" character(1),
  "47" character(1),
  "64" character(1),
  "65" character(1),
  "81" character(1),
  "82" character(1),
  "Typo_riviere" text,
  "Creation" timestamp with time zone,
  "Auteur creation" character varying(50),
  "Mise a jour" timestamp with time zone,
  "Auteur mise a jour" character varying(50),
  "Classement riviere" smallint
)
WITH (
  OIDS=FALSE
);
set client_encoding to 'WIN1252';
copy adourtemp."Rivieres" from 'C:/Mes Documents/Tableau de bord/PROJETS/These/Donnees/STACOMI/Migradour/MAJ_sept2016/Rivieres.csv' with csv delimiter as ';';--6

	-- Stations

DROP TABLE adourtemp."Stations";
CREATE TABLE adourtemp."Stations"
(
  "CODESTA" character varying(8),
  "Codesta ancien" character varying(50),
  "Code_AMENA" character varying(50),
  "CodeAxe" smallint,
  "S_LIEU_DIT" character varying(32),
  "SousBassin" character varying(50),
  "CODEINSEE" character varying(5),
  "S_DEPART" character varying(2),
  "MNEMONIQUE" character varying(12),
  "codNumopera" character varying(50),
  "S_GEOLLIT" character varying(2),
  "S_GEOLREG" character varying(2),
  "S_POLICE" character varying(1),
  "S_NATURE" character varying(1),
  "S_PKMODIF" character varying(1),
  "S_ABSCISSE" real,
  "S_ALTITUDE" smallint,
  "S_SURFBASS" real,
  "C_NATIONAL" character varying(8),
  "S_DISTSOUR" real,
  "S_CODHYDRO" character varying(8),
  "S_LATITUDE" real,
  "S_LIMITES" character varying(75),
  "S_LONGITUD" real,
  "S_ORDONNEE" real,
  "S_PK" real,
  "S_PENTEIGN" real,
  "S_STAMERID" character varying(1),
  "S_FINALITE" character varying(70),
  "S_LOCASTA" character varying(40),
  "FLAGBHP" character varying(1),
  "TRANSFER" character varying(1),
  "Dist_conf (km)" integer,
  "Titre" character varying(250),
  "latitude degrés" real,
  "longitude degrés" real,
  "IGN 50" character varying(50),
  "IGN 25" character varying(50),
  "Filtre" character varying(50),
  "Création" timestamp with time zone,
  "Auteur création" character varying(50),
  "Mise à jour" timestamp with time zone,
  "Auteur mise à jour" character varying(50),
  "AIDAE" integer,
  "DistPkamont" real,
  "DistPkaval" real,
  "CalculDist" character varying(1),
  "S_CRIV" character varying(50),
  "Dist_Mer (km)" real,
  "AIDAEconf" integer,
  "S_CRIVconf" character varying(50),
  dic_identifiant integer,
  CONSTRAINT c_uk_dic_identifiant UNIQUE (dic_identifiant)
)
WITH (
  OIDS=FALSE
);

copy adourtemp."Stations" from 'C:/Mes Documents/Tableau de bord/PROJETS/These/Donnees/STACOMI/Migradour/MAJ_sept2016/Stations.csv' with csv delimiter as ';';--12

	--poissons

DROP TABLE adourtemp.poissons;
CREATE TABLE adourtemp.poissons
(
  p_numopera character varying(11),
  p_refzone character varying(6),
  p_codesp character varying(3),
  p_sens_pas character varying(1),
  p_lmin smallint,
  p_lmax smallint,
  p_lmach double precision,
  p_ladip double precision,
  p_lnaseau double precision,
  p_masse integer,
  p_codelot character varying(1),
  p_eff smallint,
  p_sexe character varying(1),
  p_matu character varying(1),
  p_codepatho character varying(3),
  p_etat character varying(50),
  p_observation text,
  p_ecaille character(1),
  p_age character varying(3),
  p_opmarq character varying(1),
  p_typemarq character varying(4),
  p_locamarq character varying(3),
  p_nummarq character varying(6),
  p_nature_marque_ancienne character varying(50),
  p_detail_marque_ancienne character varying(50),
  p_c_lot integer,
  p_info_lot_marque_ancien character varying(50),
  p_nature_marque_nouvelle character varying(50),
  p_detail_marque_nouvelle character varying(50),
  p_npeche smallint,
  p_codestim character varying(1),
  p_flagbhp character varying(1),
  p_tansfer character varying(1),
  p_origine character varying(1),
  p_creation timestamp with time zone,
  p_auteur_creation character varying(50),
  p_mise_a_jour timestamp with time zone,
  p_auteur_mise_a_jour character varying(50),
  p_age_m smallint,
  p_age_m1 smallint,
  p_age_m2 smallint,
  p_age_r smallint,
  p_an_naiss smallint,
  p_age_t smallint,
  p_mode_determination_age character varying(50),
  p_deja_capture integer
 -- p_numlot integer NOT NULL DEFAULT nextval('adourtemp.poissons_numlot_seq'::regclass)
)
WITH (
  OIDS=FALSE
);

copy adourtemp."poissons" from 'C:/Mes Documents/Tableau de bord/PROJETS/These/Donnees/STACOMI/Migradour/MAJ_sept2016/poissons.csv' with csv delimiter as ';';--85457

--Import de migradour.t_operation_ope
--Pour l'import on a fait un remplacement en masse de migradour par adourtemp pour que créé la table dans le schéma adourtemp et non plus migradour
--A créé des erreurs sur code organisme. On remet ça

update adourtemp.t_operation_ope_schema_migradour set ope_organisme='migradour'--69677

--récupération des numopéra dans les commentaires de t_operation_ope_schema_migradour
set client_encoding to 'WIN1252';

select ope_commentaires,SUBSTRING(ope_commentaires,'numopera=.[^|]*') as regexp
from adourtemp.t_operation_ope_schema_migradour

select count(ope_identifiant)
from migradour.t_operation_ope_schema_migradour o
where o.ope_commentaires is null --34972 lignes sans numero operation

select count(ope_identifiant)
from migradour.t_operation_ope_schema_migradour o
where o.ope_commentaires is not null --34705 lignes sans numero operation

select count(ope_identifiant)
from migradour.t_operation_ope_schema_migradour o
where o.ope_dic_identifiant <> 20 and o.ope_dic_identifiant <> 22 --34345 lignes sans numero operation

select count(ope_identifiant)
from migradour.t_operation_ope_schema_migradour o
where o.ope_dic_identifiant = 20 or o.ope_dic_identifiant = 22 --35332 lignes sans numero operation



select count(*)
from adourtemp.operations
where numopera is null --0

--Il y avait déjà des données entrées dans stacomi schéma migradour avant qu'on insert données base ACCESS.
--Ces données correspondent à des lignes de comptages vidéo donc n'ont pas de numopera;

--requête pour connaitre les DC à dégager pour ne pas être embêtée avec ces lignes de comptage vidéo
select distinct dic_dis_identifiant, count(ope_identifiant)
from migradour.t_dispositifcomptage_dic dc 
left join migradour.t_operation_ope op on (op.ope_dic_identifiant = dc.dic_dis_identifiant)
left join migradour.t_lot_lot l on (l.lot_ope_identifiant = op.ope_identifiant)
where op.ope_commentaires is null
group by 1
--on dégage ope_dic_identifiant=20 et 22

select count(ope_identifiant)
from adourtemp.t_operation_ope_schema_migradour o
where o.ope_commentaires is null and (o.ope_dic_identifiant <> 20 and o.ope_dic_identifiant <> 22)  --0 lignes

--On utilise le champ operations.creation pour retrouver les nouveaux enregistrements
	-- dernière ligne insérée dans la base ACCESS qui nous a permis de rentrer les premières données MIGRADOUR = 17/09/2015 17:21:20
	--Il suffit donc dans la nouvelle version de la base ACCESS de ne conserver que les lignes pour lesquelle operations.creation>'2015-09-17 17:21:20'

select *
from adourtemp.operations
where creation>'2015-09-17 17:21:20' --915 lignes

select *
from adourtemp.operations
where creation<='2015-09-17 17:21:20' --37108 lignes

select *
from adourtemp.operations
where creation<='2015-09-17 17:21:20' or creation is null --37114 lignes


--on supprime toutes les lignes operations qui ont été créées avant cette date ou qui ont une date creation null

delete from adourtemp.operations
where creation<='2015-09-17 17:21:20' or creation is null --37114 lignes (il reste 915 lignes)

--ci dessous la sequence génère un entier autoincrémenté, je la remet à max+1 de migradour.t_operation_ope pour ope_identifiant
--create temporary sequence seq; 

select max(ope_identifiant)
from migradour.t_operation_ope --110695


alter sequence seq restart with 110696;


select *
from operations o
where datea is null--1 ligne qui n'a pas de date. On ne la prend pas en compte

delete
from operations
where numopera='STC9AA5193'

-- Voici la table t_operation_ope telle qu'elle doit être importée
set client_encoding to 'UTF8';

drop table if exists adourtemp.t_operation_ope;
create table adourtemp.t_operation_ope as(				
select 	nextval('seq') as ope_identifiant,
	sta.dic_identifiant as ope_dic_identifiant,
	case when heura is null then datea
	else datea+(heura-'1899-12-30') 
	end as ope_date_debut,
	case 	when heurer is null and dater is not null then dater
		when heurer is null and dater is null then date
		when heurer is not null and dater is null then date+(heurer-'1899-12-30') 
		when heurer is not null and dater is not null then dater+(heurer-'1899-12-30') 
	end as ope_date_fin,
	--date,
	'MIGRADOUR' as ope_organisme,
	CASE WHEN "Prenom" is not null THEN  "Prenom" ||' '|| "Nom" ELSE "Nom" END as ope_operateur,
	'numopera='||numopera||case when etatpiege_r is not null then ', '||etatpiege_r
	else '' end 
	as ope_commentaires,
	'MIGRADOUR' as ope_org_code	
	 from operations ope join "Stations" sta on "CODESTA"=codesta 	 
 left join "Piegeurs" on  "RefPiegeurs"=piegeur
	order by 3); --914 
	
--voit nbr lignes par DC dans t_operation_ope
select ope_dic_identifiant, count(ope_identifiant)
from t_operation_ope
group by 1
order by 1



--##############################################
-- ON POURSUIT AVEC LE SCRIPT Chevauchement.r
--##############################################


--t_operation_ope_new = les données avec dates vérifiées via le script chevauchement.r
--il reste à corriger les pb d'accent pas beau
update adourtemp.t_operation_ope_new set ope_commentaires=replace(ope_commentaires, 'Ã©', 'e');
update adourtemp.t_operation_ope_new set ope_commentaires=replace(ope_commentaires, 'Ã© ', 'e');
update adourtemp.t_operation_ope_new set ope_commentaires=replace(ope_commentaires, 'ÃƒÂ©','e');
update adourtemp.t_operation_ope_new set ope_commentaires=replace(ope_commentaires, 'ÃÂ©','e');
update adourtemp.t_operation_ope_new set ope_commentaires=replace(ope_commentaires, 'Ãª','e');
update adourtemp.t_operation_ope_new set ope_commentaires=replace(ope_commentaires, 'Ã¨','e');
update adourtemp.t_operation_ope_new set ope_commentaires=replace(ope_commentaires, 'Ã´','o');
update adourtemp.t_operation_ope_new set ope_commentaires=replace(ope_commentaires, 'Ã','a');

--Pb de chevauchement pour 4 DC sur 5

select *
from operations
where numopera>'STC9AA5002' and codesta='STC9'
order by 1

--On vérifie qu'il n'y a pas d'opération sans ligne poissons
select *
from operations
left join poissons on (poissons.p_numopera=operations.numopera)
where p_numopera is null--1 ligne justement pour le numopera "STC9AA5004"
--On supprime cette opération de t_operation_ope_new et de t_operation_ope
delete
from t_operation_ope_new
where ope_date_debut = '2015-10-06 00:00:00'--1 ligne justement pour le numopera "STC9AA5004"

delete
from t_operation_ope
where ope_date_debut = '2015-10-06 00:00:00'--1 ligne justement pour le numopera "STC9AA5004"

--DIC 39 une opération qu'on ne peut pas corriger avec le script r on le fait directement en sql
select *
from t_operation_ope
where ope_identifiant = 115976 --1 ligne justement pour le numopera "STC9AA5004"

update t_operation_ope set ope_date_debut='2015-11-26 11:00:00' where ope_identifiant = 115976

--DC 37 une opération qu'on ne peut pas corriger avec le script r on le fait directement en sql

select *
from t_operation_ope
where ope_identifiant = 115307 --1 ligne justement pour le numopera "STC9AA5004"

update t_operation_ope set ope_date_debut='2015-09-22 10:15:00' where ope_identifiant = 115307


--Insertion des données dans migradour.t_operation_ope (on fait d'abord une sauvegarde du schéma migradour)
begin;
insert into migradour.t_operation_ope select * from adourtemp.t_operation_ope_new where ope_dic_identifiant=34; --168
insert into migradour.t_operation_ope select * from adourtemp.t_operation_ope_new where ope_dic_identifiant=36; --19
insert into migradour.t_operation_ope select * from adourtemp.t_operation_ope_new where ope_dic_identifiant=37; --183
insert into migradour.t_operation_ope select * from adourtemp.t_operation_ope_new where ope_dic_identifiant=38; --270
insert into migradour.t_operation_ope select * from adourtemp.t_operation_ope_new where ope_dic_identifiant=39; --273
commit;
	--les 913 lignes ont été importées dans migradour.t_operation_ope

--On supprime les lignes poissons qui ne correspondent pas au 913 lignes opérations importées
select  p_numopera
from poissons where p_numopera not in (select numopera from operations) or p_numopera is null--83 347

delete from poissons where p_numopera not in (select numopera from operations) or p_numopera is null--83 347

select  p_numopera
from poissons where p_numopera in (select numopera from operations) --2110

--On ajoute un numéro poisson à la table adourtemp.poissons
ALTER TABLE adourtemp.poissons ADD COLUMN p_numlot integer;
	--On récupère le numéro poisson max de migradour.t_lot_lot
	select max(cast(substring(lot_commentaires,'po=(.[^|]*)') as integer))
	from migradour.t_lot_lot --83356

	--On poursuit la séquence
	create temporary sequence seqpois; 
	alter sequence seqpois restart with 83357;

	update adourtemp.poissons set p_numlot=nextval('seqpois')
select max(p_numlot)
from adourtemp.poissons


--#######################################################
-- ON POURSUIT AVEC LE SCRIPT Import_des_donnees_lot.r
--#######################################################


--#########################################################################
--On importe les données de adourtemp.t_lot_lot dans migradour.t_lot_lot

begin;
INSERT INTO migradour.t_lot_lot select * from adourtemp.t_lot_lot --1602
rollback;
commit;

select lot_std_code, count(*)
from adourtemp.t_lot_lot
group by 1 --2 lignes sans code stade

--on corrige les 2 lignes sans stade en mettant IND
update adourtemp.t_lot_lot set lot_std_code='IND' where lot_std_code=''

--###########################################
--Import des périodes de fonctionnement

begin;
INSERT INTO migradour.t_periodefonctdispositif_per select * from adourtemp.t_periodefonctdispositif_per --1796
rollback;
commit;

--Attention avec le script R on a créé un chevauchement car on a pour le DC 38
--une première ope le 28/03/2015 et une 2eme le 08/09/2015 d'où quand on fait les périodes
--de fermeture entre 2 opérations de piégeage dans R on créé une periode de 28/03/2015 au 08/09/2015 ce qui créé un chaveauchement avec les oéprations
--déja entrées. --> on dégage cette ligne de adourtemp.t_periodefonctdispositif_per
--D'autre part on modifie dans schéma migradour la ligne en FALSE2015-03-28 09:00:00 pour DC 38 en modifiant date de fin au lieu de 10:00 on met 09:20

select *
from migradour.t_periodefonctdispositif_per
where per_dis_identifiant=38 and per_date_fin='2015-03-29 10:00:00'

update migradour.t_periodefonctdispositif_per set per_date_fin='2015-03-28 09:20:00' where per_dis_identifiant=38 and per_date_fin='2015-03-29 10:00:00'

select *
from migradour.t_periodefonctdispositif_per
where per_dis_identifiant=38 and per_date_debut='2015-03-28 09:00:00'

--Quand on importera les données d'adourtemp.t_periodefonctdispositif_per ça ajoutera en True 2015-03-28 09:20:00 --> 2015-03-29 09:00:00
--Il faut encore ajouter à migradour.t_periodefonctdispositif_per la ligne False 2015-03-29 09:00:00 --> 2015-03-29 10:00:00
begin;
insert into migradour.t_periodefonctdispositif_per VALUES(38,'2015-03-29 09:00:00','2015-03-29 10:00:00','Arrêt correspondant à la relève du piège',FALSE,'2','MIGRADOUR')
commit;

--###########################################
--Caractéristiques lot

begin;
INSERT INTO migradour.tj_caracteristiquelot_car select * from adourtemp.tj_caracteristiquelot_car --3741
commit;

--##########################################
--Marquage

	--t_operationmarquage_omq
	--Pas de nouveaux types de marquage=pas d'ajout

	--t_marque_mqe
begin;	
INSERT INTO migradour.t_marque_mqe select * from adourtemp.t_marque_mqe where mqe_reference not in (SELECT t_marque_mqe.mqe_reference FROM migradour.t_marque_mqe)--180
commit;

	--tj_actionmarquage_act
begin;
INSERT INTO migradour.tj_actionmarquage_act select * from adourtemp.tj_actionmarquage_act --207
commit;




--#################
-- POUR MEMO
--#################

--METHODE AVANT DE S'APERCEVOIR QUE LE CHAMP "Création"PERMETTAIT DE SAVOIR LES DERNIERES LIGNES INSEREES

-- --reprise de l'expression régulière
-- CREATE temporary table numop as(
-- select ope_commentaires, SUBSTRING(ope_commentaires, 'numopera=([^|]+)') as numopera
-- from adourtemp.t_operation_ope_schema_migradour o
-- where o.ope_dic_identifiant <> 20 and o.ope_dic_identifiant <> 22) --34345  
-- 
-- --On cherche tous les operations.numopera qui ne sont pas parmi les numopera de t_operation_ope_schema_migradour
-- select distinct o.numopera
-- from adourtemp.operations o
-- where o.numopera not in (select numopera from numop) --3688
-- 
-- select distinct o.numopera
-- from adourtemp.operations o
-- where o.numopera in (select numopera from numop) --34341
-- 
-- --on supprime les lignes pour lesquelles on a déjà le numopera dans le schema migradour
-- delete from adourtemp.operations
-- where numopera in (select numopera from numop) --34341 (reste donc dans la table operations 3688 lignes)
-- 
-- select p_numopera
-- from poissons
-- where p_numopera in (select numopera from operations) --8297 lignes
-- group by 1 --3568 numopera différents
-- 
-- select *
-- from operations
-- left join poissons on (poissons.p_numopera=operations.numopera)
-- where p_numopera is null --120 lignes sans poissons pour une opération
-- 
-- --On créé une nouvelle table pour conserver les nouvelles opérations pour lesquelles on n'a pas de poissons
-- CREATE TABLE adourtemp.operations_sans_poissons as (
-- select operations.*
-- from operations
-- left join poissons on (poissons.p_numopera=operations.numopera)
-- where p_numopera is null)
-- 
-- --on supprime ces opérations sans poissons de la table operations
-- begin ;
-- delete from operations
-- where numopera in (select n.numopera from operations_sans_poissons n) --120 lignes
-- commit;
-- 
-- --on supprime tous les poissons ne se rattachant pas aux numopera de la table operations
-- select p_numopera
-- from poissons
-- where p_numopera not in (select numopera from operations) --77151 lignes
-- 
-- select p_numopera
-- from poissons
-- where p_numopera in (select numopera from operations) --8297 lignes
-- 
-- select *
-- from poissons
-- where p_numopera is null --9 lignes
-- 	--on supprime les lignes poissons avec un numopera null
-- 	delete from poissons
-- 	where p_numopera is null --9 lignes
-- 
-- 	--on supprime les poissons qui ne match pas numopera de operation
-- 	delete from poissons
-- 	where p_numopera not in (select numopera from operations) --77151 lignes
-- 
-- --reste 8297 lignes
-- 
-- --quelques corrections à la main
--  update adourtemp.operations set pbdate=TRUE where date!=dater; --20 lignes
--  select date, datea, dater from adourtemp.operations where date!=dater;
--  select * from adourtemp.operations where cast(num2 as numeric) >=10361 and cast(num2 as numeric)<=10380 and codesta='STC6';
--  update adourtemp.operations set datea=datea- interval '1 year' 
-- 	where cast(num2 as numeric) >=10361 and cast(num2 as numeric)<=10380 and codesta='STC6';--19
--   update adourtemp.operations set dater=dater- interval '1 year' 
-- 	where cast(num2 as numeric) >=10361 and cast(num2 as numeric)<=10380 and codesta='STC6';--19	
--  select heurer-heura from adourtemp.operations where dater=datea;
--   select date, datea, dater from adourtemp.operations where date!=dater; -- plus que 1
-- 
-- select 
-- 	ouv_sta_code as sta_code,
-- 	ouv_code,
-- 	ouv_libelle,
-- 	dis2.dis_identifiant as df,
-- 	dis2.dis_commentaires as df_commentaires,
-- 	dif_code,
-- 	dis1.dis_identifiant as dc, 
-- 	dis1.dis_commentaires as dc_commentaires,
-- 	dic_code	
-- 	from migradour.tg_dispositif_dis dis1
-- 	join migradour.t_dispositifcomptage_dic on dic_dis_identifiant=dis_identifiant
-- 	join migradour.t_dispositiffranchissement_dif dif on dic_dif_identifiant=dif_dis_identifiant
-- 	join migradour.tg_dispositif_dis dis2 on dif_dis_identifiant=dis2.dis_identifiant
-- 	join migradour.t_ouvrage_ouv ouv on dif_ouv_identifiant=ouv_identifiant;
-- 
-- -- je rajoute les identifiants stacomi dans la table Stations
-- alter table "Stations" add constraint c_uk_dic_identifiant unique (dic_identifiant);
-- select * from "Stations" where dic_identifiant is null; --12 lignes
-- -- Ci dessous la correspondance se fait à la main
-- update "Stations" set dic_identifiant=32 where "CODESTA"='STC1';--sorde
-- --update "Stations" set dic_identifiant= where "CODESTA"='STC2'; -- Dognen =ancienne station à réintégrer plus tard
-- update "Stations" set dic_identifiant=33 where "CODESTA"='STC3';--guerlain
-- update "Stations" set dic_identifiant=22 where "CODESTA"='STC4';--saint crick
-- update "Stations" set dic_identifiant=34 where "CODESTA"='STC5';--Soeix
-- update "Stations" set dic_identifiant=35 where "CODESTA"='STC6';--Chéraute
-- update "Stations" set dic_identifiant=36 where "CODESTA"='STC7';--chopolo
-- update "Stations" set dic_identifiant=38 where "CODESTA"='STC8';--Uxondoa
-- update "Stations" set dic_identifiant=39 where "CODESTA"='STC9';--Ohla
-- --update "Stations" set dic_identifiant= where "CODESTA"='STC10';--Puyoo =ancienne station à réintégrer plus tard
-- update "Stations" set dic_identifiant=37 where "CODESTA"='STC11';--Halsou
-- --update "Stations" set dic_identifiant= where "CODESTA"='STC12';--Baigts = ancienne station à réintégrer plus tard
-- 
-- 
-- -- rectification des champs heures
-- 
-- 
-- -- Avec une jointure je ne garde que les stations qui ont un dic_identifiant non nul
-- select * from operations where date< datea; --2 lignes
-- -- ci dessous la sequence me génère un entier autoincrémenté, je la remet à max+1 de migradour.t_operation_ope pour ope_identifiant
-- create temporary sequence seq; 
-- 
-- select max(ope_identifiant)
-- from migradour.t_operation_ope --110695
-- 
-- alter sequence seq restart with 110696;
-- 
-- 
-- select *
-- from operations ope join "Stations" sta on "CODESTA"=codesta 	 
--  join "Piegeurs" on  "RefPiegeurs"=piegeur
-- 	where dic_identifiant is not null --954
-- 
-- select *
-- from operations ope join "Stations" sta on "CODESTA"=codesta 	 
--  join "Piegeurs" on  "RefPiegeurs"=piegeur --3393
-- 
--  select *
-- from operations ope join "Stations" sta on "CODESTA"=codesta 	 
--  left join "Piegeurs" on  "RefPiegeurs"=piegeur --3568
-- 
--  select *
--  from operations
--  where piegeur is null --175 lignes sans indications de piégeurs dans table operation
-- 
-- select codesta
-- from operations ope join "Stations" sta on "CODESTA"=codesta 	 
--  left join "Piegeurs" on  "RefPiegeurs"=piegeur
-- 	where dic_identifiant is null --2494
-- 	group by 1 --3 lignes correspondant aux 3 DC qu'on n'intègre pas pour le moment
-- 
-- select *
-- from operations o
-- join "Stations" s on (s."CODESTA"=o.codesta)
-- where datea is null and s.dic_identifiant is not null --74 lignes qui n'ont qu'une date et pas de datea ni dater. On ne les prend pas en compte
-- 
-- 
-- -- Voici la table t_operation_ope telle qu'elle doit être importée
-- set client_encoding to 'UTF8';
-- 
-- drop table if exists adourtemp.t_operation_ope;
-- create table adourtemp.t_operation_ope as(				
-- select 	nextval('seq') as ope_identifiant,
-- 	sta.dic_identifiant as ope_dic_identifiant,
-- 	case when heura is null then datea
-- 	else datea+(heura-'1899-12-30') 
-- 	end as ope_date_debut,
-- 	case 	when heurer is null and dater is not null then dater
-- 		when heurer is null and dater is null then date
-- 		when heurer is not null and dater is null then date+(heurer-'1899-12-30') 
-- 		when heurer is not null and dater is not null then dater+(heurer-'1899-12-30') 
-- 	end as ope_date_fin,
-- 	--date,
-- 	'MIGRADOUR' as ope_organisme,
-- 	"Prenom"  ||' '|| "Nom" as ope_operateur,
-- 	'numopera='||numopera||case when etatpiege_r is not null then ', '||etatpiege_r
-- 	else '' end 
-- 	as ope_commentaires,
-- 	'MIGRADOUR' as ope_org_code	
-- 	 from operations ope join "Stations" sta on "CODESTA"=codesta 	 
--  left join "Piegeurs" on  "RefPiegeurs"=piegeur
-- 	where dic_identifiant is not null and datea is not null
-- 	order by 3); --1000 (on n'intègre pas les données operations des 3 DC correspondant aux CODESTA 'STC12','STC10' et 'STC2' (2494 lignes) ni les 74 lignes qui n'ont pas de datea ni de dater)
-- 
-- --voit nbr lignes par DC dans t_operation_ope
-- select ope_dic_identifiant, count(ope_identifiant)
-- from t_operation_ope
-- group by 1
-- order by 1
-- 
-- 
-- select count(ope_identifiant)
-- from t_operation_ope
-- 
-- 
-- --##############################################
-- -- ON POURSUIT AVEC LE SCRIPT Chevauchement.r
-- --##############################################
-- 
-- --Sur DC 36 pb pour l'opération numopéra=STC7AA1603
-- --Date début=date fin. Une seule ligne poisson associée avec un code espèce =XXX
-- --on considère cette opération comme une opération nulle = on supprime la ligne
-- 
-- select *
-- from adourtemp.t_operation_ope
-- where SUBSTRING(ope_commentaires,'numopera=(.[^|]*)')='STC7AA1603'
-- 
-- delete from adourtemp.t_operation_ope
-- where SUBSTRING(ope_commentaires,'numopera=(.[^|]*)')='STC7AA1603' --1 ligne
-- 
-- --Sur DC 36 pb pour l'opération numopéra=STC7AA2971
-- --Date début=date fin. Une seule ligne poisson associée avec un code espèce =XXX
-- --on considère cette opération comme une opération nulle = on supprime la ligne
-- select *
-- from adourtemp.t_operation_ope
-- where SUBSTRING(ope_commentaires,'numopera=(.[^|]*)')='STC7AA2971'
-- 
-- delete from adourtemp.t_operation_ope
-- where SUBSTRING(ope_commentaires,'numopera=(.[^|]*)')='STC7AA2971' --1 ligne
-- 
-- --t_operation_ope_new = les données avec dates vérifiées via le script chevauchement.r
-- --il reste à corriger les pb d'accent pas beau
-- update adourtemp.t_operation_ope_new set ope_commentaires=replace(ope_commentaires, 'Ã©', 'e');
-- update adourtemp.t_operation_ope_new set ope_commentaires=replace(ope_commentaires, 'Ã© ', 'e');
-- update adourtemp.t_operation_ope_new set ope_commentaires=replace(ope_commentaires, 'ÃƒÂ©','e');
-- update adourtemp.t_operation_ope_new set ope_commentaires=replace(ope_commentaires, 'ÃÂ©','e');
-- update adourtemp.t_operation_ope_new set ope_commentaires=replace(ope_commentaires, 'Ãª','e');
-- update adourtemp.t_operation_ope_new set ope_commentaires=replace(ope_commentaires, 'Ã¨','e');
-- update adourtemp.t_operation_ope_new set ope_commentaires=replace(ope_commentaires, 'Ã´','o');
-- update adourtemp.t_operation_ope_new set ope_commentaires=replace(ope_commentaires, 'Ã','a');
-- 
-- select *
-- from adourtemp.t_operation_ope_new
-- 
-- --Insertion des données dans migradour.t_operation_ope (on fait d'abord une sauvegarde du schéma migradour)
-- insert into migradour.t_operation_ope select * from adourtemp.t_operation_ope_new where ope_dic_identifiant=32; --1 OK
-- insert into migradour.t_operation_ope select * from adourtemp.t_operation_ope_new where ope_dic_identifiant=34; --pb chevauchement
-- insert into migradour.t_operation_ope select * from adourtemp.t_operation_ope_new where ope_dic_identifiant=35; --35, 1998-01-07 00:00:00, 1998-01-08 00:00:00
-- insert into migradour.t_operation_ope select * from adourtemp.t_operation_ope_new where ope_dic_identifiant=36; --36, 1999-05-02 00:00:00, 1999-05-03 00:00:00
-- insert into migradour.t_operation_ope select * from adourtemp.t_operation_ope_new where ope_dic_identifiant=37; --37, 1999-07-12 08:00:00, 1999-07-13 07:30:00
-- insert into migradour.t_operation_ope select * from adourtemp.t_operation_ope_new where ope_dic_identifiant=38; --pb chevauchement
-- insert into migradour.t_operation_ope select * from adourtemp.t_operation_ope_new where ope_dic_identifiant=39; --39, 2008-09-30 08:35:00, 2008-09-30 16:10:00


