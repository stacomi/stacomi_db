﻿--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: bresle; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA bresle;


ALTER SCHEMA bresle OWNER TO postgres;

--
-- Name: SCHEMA bresle; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA bresle IS 'restauration du 31/01/2013';


SET search_path = bresle, pg_catalog;

--
-- Name: breakpoint; Type: TYPE; Schema: bresle; Owner: postgres
--

CREATE TYPE breakpoint AS (
	func oid,
	linenumber integer,
	targetname text
);


ALTER TYPE bresle.breakpoint OWNER TO postgres;

--
-- Name: dblink_pkey_results; Type: TYPE; Schema: bresle; Owner: postgres
--

CREATE TYPE dblink_pkey_results AS (
	"position" integer,
	colname text
);


ALTER TYPE bresle.dblink_pkey_results OWNER TO postgres;

--
-- Name: frame; Type: TYPE; Schema: bresle; Owner: postgres
--

CREATE TYPE frame AS (
	level integer,
	targetname text,
	func oid,
	linenumber integer,
	args text
);


ALTER TYPE bresle.frame OWNER TO postgres;

--
-- Name: proxyinfo; Type: TYPE; Schema: bresle; Owner: postgres
--

CREATE TYPE proxyinfo AS (
	serverversionstr text,
	serverversionnum integer,
	proxyapiver integer,
	serverprocessid integer
);


ALTER TYPE bresle.proxyinfo OWNER TO postgres;

--
-- Name: targetinfo; Type: TYPE; Schema: bresle; Owner: postgres
--

CREATE TYPE targetinfo AS (
	target oid,
	schema oid,
	nargs integer,
	argtypes oidvector,
	targetname name,
	argmodes "char"[],
	argnames text[],
	targetlang oid,
	fqname text,
	returnsset boolean,
	returntype oid
);


ALTER TYPE bresle.targetinfo OWNER TO postgres;

--
-- Name: var; Type: TYPE; Schema: bresle; Owner: postgres
--

CREATE TYPE var AS (
	name text,
	varclass character(1),
	linenumber integer,
	isunique boolean,
	isconst boolean,
	isnotnull boolean,
	dtype oid,
	value text
);


ALTER TYPE bresle.var OWNER TO postgres;

--
-- Name: fct_coe_date(); Type: FUNCTION; Schema: bresle; Owner: postgres
--

CREATE FUNCTION fct_coe_date() RETURNS trigger
    LANGUAGE plpgsql
    AS $$   

 	DECLARE nbChevauchements INTEGER ;

 	BEGIN
 	 	-- verification des non-chevauchements pour les taux
 	 	SELECT COUNT(*) INTO nbChevauchements
 	 	FROM   bresle.tj_coefficientconversion_coe
 	 	WHERE  coe_tax_code = NEW.coe_tax_code
 	 	       AND coe_std_code = NEW.coe_std_code
 	 	       AND coe_qte_code = NEW.coe_qte_code
 	 	       AND (coe_date_debut, coe_date_fin) OVERLAPS (NEW.coe_date_debut, NEW.coe_date_fin)
 	 	;

		-- Comme le trigger est declenche sur AFTER et non pas sur BEFORE, il faut (nbChevauchements > 1) et non pas >0, car l enregistrement a deja ete ajoute, donc il se chevauche avec lui meme, ce qui est normal !
 	 	IF (nbChevauchements > 1) THEN
 	 	 	RAISE EXCEPTION 'Les taux de conversion ne peuvent se chevaucher.'  ;
 	 	END IF  ;

		RETURN NEW ;
 	END  ;
$$;


ALTER FUNCTION bresle.fct_coe_date() OWNER TO postgres;

--
-- Name: fct_ope_date(); Type: FUNCTION; Schema: bresle; Owner: postgres
--

CREATE FUNCTION fct_ope_date() RETURNS trigger
    LANGUAGE plpgsql
    AS $$   

 	DECLARE disCreation  	 	TIMESTAMP  ;
 	DECLARE disSuppression  	TIMESTAMP  ;
 	DECLARE nbChevauchements 	INTEGER    ;

 	BEGIN
 	 	-- Recuperation des dates du dispositif dans des variables
 	 	SELECT dis_date_creation, dis_date_suppression INTO disCreation, disSuppression
 	 	FROM   bresle.tg_dispositif_dis
 	 	WHERE  dis_identifiant = NEW.ope_dic_identifiant
 	 	;

 	 	-- verification de la date de debut
 	 	IF ((NEW.ope_date_debut < disCreation) OR (NEW.ope_date_debut > disSuppression)) THEN
 	 	 	RAISE EXCEPTION 'Le debut de l operation doit etre inclus dans la periode d existence du DC.'  ;
 	 	END IF  ;

 	 	-- verification de la date de fin
 	 	IF ((NEW.ope_date_fin < disCreation) OR (NEW.ope_date_fin > disSuppression)) THEN
 	 	 	RAISE EXCEPTION 'La fin de l operation doit etre incluse dans la periode d existence du DC.'  ;
 	 	END IF  ;

 	 	-- verification des non-chevauchements pour les operations du dispositif
 	 	SELECT COUNT(*) INTO nbChevauchements
 	 	FROM   bresle.t_operation_ope
 	 	WHERE  ope_dic_identifiant = NEW.ope_dic_identifiant 
 	 	       AND (ope_date_debut, ope_date_fin) OVERLAPS (NEW.ope_date_debut, NEW.ope_date_fin)
 	 	;

		-- Comme le trigger est declenche sur AFTER et non pas sur BEFORE, il faut (nbChevauchements > 1) et non pas >0, car l enregistrement a deja ete ajoute, donc il se chevauche avec lui meme, ce qui est normal !
 	 	IF (nbChevauchements > 1) THEN 
 	 		RAISE EXCEPTION 'Les operations ne peuvent se chevaucher.'  ;
 	 	END IF  ;

		RETURN NEW ;
 	END  ;
$$;


ALTER FUNCTION bresle.fct_ope_date() OWNER TO postgres;

--
-- Name: fct_per_date(); Type: FUNCTION; Schema: bresle; Owner: postgres
--

CREATE FUNCTION fct_per_date() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ 

 	DECLARE disCreation  	 	TIMESTAMP  ;
 	DECLARE disSuppression  	TIMESTAMP  ;
 	DECLARE nbChevauchements 	INTEGER    ;

 	BEGIN
 	 	-- Recuperation des dates du dispositif dans des variables
 	 	SELECT dis_date_creation, dis_date_suppression INTO disCreation, disSuppression
 	 	FROM   bresle.tg_dispositif_dis
 	 	WHERE  dis_identifiant = NEW.per_dis_identifiant
 	 	;

 	 	-- verification de la date de debut
 	 	IF ((NEW.per_date_debut < disCreation) OR (NEW.per_date_debut > disSuppression)) THEN
 	 		RAISE EXCEPTION 'Le debut de la periode doit etre inclus dans la periode d existence du dispositif.'  ;
 	 	END IF  ;

 	 	-- verification de la date de fin
 	 	IF ((NEW.per_date_fin < disCreation) OR (NEW.per_date_fin > disSuppression)) THEN
 	 	 	RAISE EXCEPTION 'La fin de la periode doit etre incluse dans la periode d existence du dispositif.'  ;
 	 	END IF  ;

 	 	-- verification des non-chevauchements pour les periodes du dispositif
 	 	SELECT COUNT(*) INTO nbChevauchements
 	 	FROM   bresle.t_periodefonctdispositif_per
 	 	WHERE  per_dis_identifiant = NEW.per_dis_identifiant 
 	 	       AND (per_date_debut, per_date_fin) OVERLAPS (NEW.per_date_debut, NEW.per_date_fin)
 	 	;

		-- Comme le trigger est declenche sur AFTER et non pas sur BEFORE, il faut (nbChevauchements > 1) et non pas >0, car l enregistrement a deja ete ajoute, donc il se chevauche avec lui meme, ce qui est normal !
 	 	IF (nbChevauchements > 1) THEN
 	 	 	RAISE EXCEPTION 'Les periodes ne peuvent se chevaucher.'  ;
 	 	END IF  ;
		
		RETURN NEW ;
 	END  ;
$$;


ALTER FUNCTION bresle.fct_per_date() OWNER TO postgres;

--
-- Name: fct_per_suppression(); Type: FUNCTION; Schema: bresle; Owner: postgres
--

CREATE FUNCTION fct_per_suppression() RETURNS trigger
    LANGUAGE plpgsql
    AS $$   

 	BEGIN
 	 	-- La periode precedent celle supprimee est prolongee
 	 	-- jusqu a la fin de la periode supprimee
 	 	UPDATE bresle.t_periodefonctdispositif_per 
 	 	SET    per_date_fin = OLD.per_date_fin 
 	 	WHERE  per_date_fin= OLD.per_date_debut 
 	 	       AND per_dis_identifiant = OLD.per_dis_identifiant 
 	 	;
		RETURN NEW ;
 	END  ;

$$;


ALTER FUNCTION bresle.fct_per_suppression() OWNER TO postgres;

--
-- Name: fct_txe_date(); Type: FUNCTION; Schema: bresle; Owner: postgres
--

CREATE FUNCTION fct_txe_date() RETURNS trigger
    LANGUAGE plpgsql
    AS $$   

 	DECLARE nbChevauchements INTEGER ;

 	BEGIN
 	 	-- verification des non-chevauchements pour les taux
 	 	SELECT COUNT(*) INTO nbChevauchements
 	 	FROM   bresle.tj_tauxechappement_txe
 	 	WHERE  txe_ouv_identifiant = NEW.txe_ouv_identifiant
 	 	       AND txe_tax_code = NEW.txe_tax_code
 	 	       AND txe_std_code = NEW.txe_std_code
 	 	       AND (txe_date_debut, txe_date_fin) OVERLAPS (NEW.txe_date_debut, NEW.txe_date_fin)
 	 	;

		-- Comme le trigger est declenche sur AFTER et non pas sur BEFORE, il faut (nbChevauchements > 1) et non pas >0, car l enregistrement a deja ete ajoute, donc il se chevauche avec lui meme, ce qui est normal !
 	 	IF (nbChevauchements > 1) THEN
 	 	 	RAISE EXCEPTION 'Les taux d echappement ne peuvent se chevaucher.'  ;
 	 	END IF  ;

		RETURN NEW ;

 	END  ;
$$;


ALTER FUNCTION bresle.fct_txe_date() OWNER TO postgres;

--
-- Name: plpgsql_call_handler(); Type: FUNCTION; Schema: bresle; Owner: postgres
--

CREATE FUNCTION plpgsql_call_handler() RETURNS language_handler
    LANGUAGE c
    AS '$libdir/plpgsql', 'plpgsql_call_handler';


ALTER FUNCTION bresle.plpgsql_call_handler() OWNER TO postgres;

--
-- Name: xpath_list(text, text); Type: FUNCTION; Schema: bresle; Owner: postgres
--

CREATE FUNCTION xpath_list(text, text) RETURNS text
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$SELECT xpath_list($1,$2,',')$_$;


ALTER FUNCTION bresle.xpath_list(text, text) OWNER TO postgres;

--
-- Name: xpath_nodeset(text, text); Type: FUNCTION; Schema: bresle; Owner: postgres
--

CREATE FUNCTION xpath_nodeset(text, text) RETURNS text
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$SELECT xpath_nodeset($1,$2,'','')$_$;


ALTER FUNCTION bresle.xpath_nodeset(text, text) OWNER TO postgres;

--
-- Name: xpath_nodeset(text, text, text); Type: FUNCTION; Schema: bresle; Owner: postgres
--

CREATE FUNCTION xpath_nodeset(text, text, text) RETURNS text
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$SELECT xpath_nodeset($1,$2,'',$3)$_$;


ALTER FUNCTION bresle.xpath_nodeset(text, text, text) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: t_bilanmigrationjournalier_bjo; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE t_bilanmigrationjournalier_bjo (
    bjo_identifiant integer NOT NULL,
    bjo_dis_identifiant integer NOT NULL,
    bjo_tax_code character varying(6) NOT NULL,
    bjo_std_code character varying(4) NOT NULL,
    bjo_annee integer NOT NULL,
    bjo_jour timestamp without time zone NOT NULL,
    bjo_labelquantite character varying(30),
    bjo_valeur double precision,
    bjo_horodateexport timestamp without time zone,
    bjo_org_code character varying(30) NOT NULL
);


ALTER TABLE bresle.t_bilanmigrationjournalier_bjo OWNER TO postgres;

--
-- Name: t_bilanmigrationjournalier_bjo_bjo_identifiant_seq; Type: SEQUENCE; Schema: bresle; Owner: postgres
--

CREATE SEQUENCE t_bilanmigrationjournalier_bjo_bjo_identifiant_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bresle.t_bilanmigrationjournalier_bjo_bjo_identifiant_seq OWNER TO postgres;

--
-- Name: t_bilanmigrationjournalier_bjo_bjo_identifiant_seq; Type: SEQUENCE OWNED BY; Schema: bresle; Owner: postgres
--

ALTER SEQUENCE t_bilanmigrationjournalier_bjo_bjo_identifiant_seq OWNED BY t_bilanmigrationjournalier_bjo.bjo_identifiant;


--
-- Name: t_bilanmigrationmensuel_bme; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE t_bilanmigrationmensuel_bme (
    bme_identifiant integer NOT NULL,
    bme_dis_identifiant integer NOT NULL,
    bme_tax_code character varying(6) NOT NULL,
    bme_std_code character varying(4) NOT NULL,
    bme_annee integer NOT NULL,
    bme_mois integer NOT NULL,
    bme_labelquantite character varying(30),
    bme_valeur double precision,
    bme_horodateexport timestamp without time zone,
    bme_org_code character varying(30) NOT NULL
);


ALTER TABLE bresle.t_bilanmigrationmensuel_bme OWNER TO postgres;

--
-- Name: t_bilanmigrationmensuel_bme_bme_identifiant_seq; Type: SEQUENCE; Schema: bresle; Owner: postgres
--

CREATE SEQUENCE t_bilanmigrationmensuel_bme_bme_identifiant_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bresle.t_bilanmigrationmensuel_bme_bme_identifiant_seq OWNER TO postgres;

--
-- Name: t_bilanmigrationmensuel_bme_bme_identifiant_seq; Type: SEQUENCE OWNED BY; Schema: bresle; Owner: postgres
--

ALTER SEQUENCE t_bilanmigrationmensuel_bme_bme_identifiant_seq OWNED BY t_bilanmigrationmensuel_bme.bme_identifiant;


SET default_with_oids = true;

--
-- Name: t_dispositifcomptage_dic; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE t_dispositifcomptage_dic (
    dic_dis_identifiant integer NOT NULL,
    dic_dif_identifiant integer NOT NULL,
    dic_code character varying(16) NOT NULL,
    dic_tdc_code integer NOT NULL,
    dic_org_code character varying(30) NOT NULL
);


ALTER TABLE bresle.t_dispositifcomptage_dic OWNER TO postgres;

--
-- Name: t_dispositiffranchissement_dif; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE t_dispositiffranchissement_dif (
    dif_dis_identifiant integer NOT NULL,
    dif_ouv_identifiant integer NOT NULL,
    dif_code character varying(16) NOT NULL,
    dif_localisation text,
    dif_orientation character varying(20) NOT NULL,
    dif_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_dif_orientation CHECK (((upper((dif_orientation)::text) = 'DESCENTE'::text) OR (upper((dif_orientation)::text) = 'MONTEE'::text)))
);


ALTER TABLE bresle.t_dispositiffranchissement_dif OWNER TO postgres;

--
-- Name: t_lot_lot; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE t_lot_lot (
    lot_identifiant integer NOT NULL,
    lot_ope_identifiant integer NOT NULL,
    lot_tax_code character varying(6) NOT NULL,
    lot_std_code character varying(4) NOT NULL,
    lot_effectif double precision,
    lot_quantite double precision,
    lot_qte_code character varying(4),
    lot_methode_obtention character varying(10) NOT NULL,
    lot_lot_identifiant integer,
    lot_dev_code character varying(4),
    lot_commentaires text,
    lot_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_lot_lot_identifiant CHECK ((lot_lot_identifiant <> lot_identifiant)),
    CONSTRAINT c_ck_lot_methode_obtention CHECK (((((upper((lot_methode_obtention)::text) = 'MESURE'::text) OR (upper((lot_methode_obtention)::text) = 'CALCULE'::text)) OR (upper((lot_methode_obtention)::text) = 'EXPERT'::text)) OR (upper((lot_methode_obtention)::text) = 'PONCTUEL'::text)))
);


ALTER TABLE bresle.t_lot_lot OWNER TO postgres;

--
-- Name: t_lot_lot_lot_identifiant_seq; Type: SEQUENCE; Schema: bresle; Owner: postgres
--

CREATE SEQUENCE t_lot_lot_lot_identifiant_seq
    START WITH 175116
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bresle.t_lot_lot_lot_identifiant_seq OWNER TO postgres;

--
-- Name: t_lot_lot_lot_identifiant_seq; Type: SEQUENCE OWNED BY; Schema: bresle; Owner: postgres
--

ALTER SEQUENCE t_lot_lot_lot_identifiant_seq OWNED BY t_lot_lot.lot_identifiant;


--
-- Name: t_marque_mqe; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE t_marque_mqe (
    mqe_reference character varying(30) NOT NULL,
    mqe_loc_code character varying(4) NOT NULL,
    mqe_nmq_code character varying(4) NOT NULL,
    mqe_omq_reference character varying(30),
    mqe_commentaires text,
    mqe_org_code character varying(30) NOT NULL
);


ALTER TABLE bresle.t_marque_mqe OWNER TO postgres;

--
-- Name: t_operation_ope; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE t_operation_ope (
    ope_identifiant integer NOT NULL,
    ope_dic_identifiant integer NOT NULL,
    ope_date_debut timestamp(0) without time zone NOT NULL,
    ope_date_fin timestamp(0) without time zone NOT NULL,
    ope_organisme character varying(35),
    ope_operateur character varying(35),
    ope_commentaires text,
    ope_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_ope_date_fin CHECK (((ope_date_fin >= ope_date_debut) AND (date_part('month'::text, age(ope_date_fin, ope_date_debut)) <= (1)::double precision)))
);


ALTER TABLE bresle.t_operation_ope OWNER TO postgres;

--
-- Name: t_operation_ope_ope_identifiant_seq; Type: SEQUENCE; Schema: bresle; Owner: postgres
--

CREATE SEQUENCE t_operation_ope_ope_identifiant_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bresle.t_operation_ope_ope_identifiant_seq OWNER TO postgres;

--
-- Name: t_operation_ope_ope_identifiant_seq; Type: SEQUENCE OWNED BY; Schema: bresle; Owner: postgres
--

ALTER SEQUENCE t_operation_ope_ope_identifiant_seq OWNED BY t_operation_ope.ope_identifiant;


--
-- Name: t_operationmarquage_omq; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE t_operationmarquage_omq (
    omq_reference character varying(30) NOT NULL,
    omq_commentaires text
);


ALTER TABLE bresle.t_operationmarquage_omq OWNER TO postgres;

--
-- Name: t_ouvrage_ouv; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE t_ouvrage_ouv (
    ouv_identifiant integer NOT NULL,
    ouv_sta_code character varying(8) NOT NULL,
    ouv_code character varying(5) NOT NULL,
    ouv_libelle character varying(40) NOT NULL,
    ouv_localisation text,
    ouv_coordonnee_x integer,
    ouv_coordonnee_y integer,
    ouv_altitude smallint,
    ouv_carte_localisation bytea,
    ouv_denivelee_max double precision,
    ouv_commentaires text,
    ouv_nov_code character varying(4) NOT NULL,
    ouv_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_ouv_altitude CHECK ((ouv_altitude >= 0)),
    CONSTRAINT c_ck_ouv_coordonnee_x CHECK (((ouv_coordonnee_x >= 0) AND (ouv_coordonnee_x < 1000000))),
    CONSTRAINT c_ck_ouv_coordonnee_y CHECK (((ouv_coordonnee_y >= 2000000) AND (ouv_coordonnee_y < 3000000))),
    CONSTRAINT c_ck_ouv_denivelee_max CHECK ((ouv_denivelee_max >= (0)::double precision))
);


ALTER TABLE bresle.t_ouvrage_ouv OWNER TO postgres;

--
-- Name: t_ouvrage_ouv_ouv_identifiant_seq; Type: SEQUENCE; Schema: bresle; Owner: postgres
--

CREATE SEQUENCE t_ouvrage_ouv_ouv_identifiant_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bresle.t_ouvrage_ouv_ouv_identifiant_seq OWNER TO postgres;

--
-- Name: t_ouvrage_ouv_ouv_identifiant_seq; Type: SEQUENCE OWNED BY; Schema: bresle; Owner: postgres
--

ALTER SEQUENCE t_ouvrage_ouv_ouv_identifiant_seq OWNED BY t_ouvrage_ouv.ouv_identifiant;


--
-- Name: t_periodefonctdispositif_per; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE t_periodefonctdispositif_per (
    per_dis_identifiant integer NOT NULL,
    per_date_debut timestamp(0) without time zone NOT NULL,
    per_date_fin timestamp(0) without time zone NOT NULL,
    per_commentaires text,
    per_etat_fonctionnement boolean NOT NULL,
    per_tar_code character varying(4),
    per_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_per_date_fin CHECK ((per_date_fin >= per_date_debut))
);


ALTER TABLE bresle.t_periodefonctdispositif_per OWNER TO postgres;

--
-- Name: t_station_sta; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE t_station_sta (
    sta_code character varying(8) NOT NULL,
    sta_nom character varying(40) NOT NULL,
    sta_localisation character varying(60),
    sta_coordonnee_x integer,
    sta_coordonnee_y integer,
    sta_altitude smallint,
    sta_carte_localisation bytea,
    sta_superficie integer,
    sta_distance_mer double precision,
    sta_date_creation date,
    sta_date_suppression date,
    sta_commentaires text,
    sta_dernier_import_conditions timestamp(0) without time zone,
    sta_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_sta_altitude CHECK ((sta_altitude >= 0)),
    CONSTRAINT c_ck_sta_coordonnee_x CHECK (((sta_coordonnee_x >= 0) AND (sta_coordonnee_x < 1000000))),
    CONSTRAINT c_ck_sta_coordonnee_y CHECK (((sta_coordonnee_y >= 2000000) AND (sta_coordonnee_y < 3000000))),
    CONSTRAINT c_ck_sta_date_suppression CHECK ((sta_date_suppression >= sta_date_creation)),
    CONSTRAINT c_ck_sta_distance_mer CHECK ((sta_distance_mer >= (0)::double precision)),
    CONSTRAINT c_ck_sta_superficie CHECK ((sta_superficie >= 0))
);


ALTER TABLE bresle.t_station_sta OWNER TO postgres;

--
-- Name: tg_dispositif_dis; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE tg_dispositif_dis (
    dis_identifiant integer NOT NULL,
    dis_date_creation date,
    dis_date_suppression date,
    dis_commentaires text,
    dis_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_dis_date_suppression CHECK ((dis_date_suppression >= dis_date_creation))
);


ALTER TABLE bresle.tg_dispositif_dis OWNER TO postgres;

--
-- Name: tg_dispositif_dis_dis_identifiant_seq; Type: SEQUENCE; Schema: bresle; Owner: postgres
--

CREATE SEQUENCE tg_dispositif_dis_dis_identifiant_seq
    START WITH 19
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bresle.tg_dispositif_dis_dis_identifiant_seq OWNER TO postgres;

--
-- Name: tg_dispositif_dis_dis_identifiant_seq; Type: SEQUENCE OWNED BY; Schema: bresle; Owner: postgres
--

ALTER SEQUENCE tg_dispositif_dis_dis_identifiant_seq OWNED BY tg_dispositif_dis.dis_identifiant;


--
-- Name: tj_actionmarquage_act; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE tj_actionmarquage_act (
    act_lot_identifiant integer NOT NULL,
    act_mqe_reference character varying(30) NOT NULL,
    act_action character varying(20) NOT NULL,
    act_commentaires text,
    act_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_act_action CHECK ((((upper((act_action)::text) = 'POSE'::text) OR (upper((act_action)::text) = 'LECTURE'::text)) OR (upper((act_action)::text) = 'RETRAIT'::text)))
);


ALTER TABLE bresle.tj_actionmarquage_act OWNER TO postgres;

--
-- Name: tj_caracteristiquelot_car; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE tj_caracteristiquelot_car (
    car_lot_identifiant integer NOT NULL,
    car_par_code character varying(5) NOT NULL,
    car_methode_obtention character varying(10),
    car_val_identifiant integer,
    car_valeur_quantitatif real,
    car_precision real,
    car_commentaires text,
    car_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_car CHECK ((((car_val_identifiant IS NOT NULL) AND (car_valeur_quantitatif IS NULL)) OR ((car_val_identifiant IS NULL) AND (car_valeur_quantitatif IS NOT NULL)))),
    CONSTRAINT c_ck_car_methode_obtention CHECK ((((upper((car_methode_obtention)::text) = 'MESURE'::text) OR (upper((car_methode_obtention)::text) = 'CALCULE'::text)) OR (upper((car_methode_obtention)::text) = 'EXPERT'::text)))
);


ALTER TABLE bresle.tj_caracteristiquelot_car OWNER TO postgres;

--
-- Name: tj_coefficientconversion_coe; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE tj_coefficientconversion_coe (
    coe_tax_code character varying(6) NOT NULL,
    coe_std_code character varying(4) NOT NULL,
    coe_qte_code character varying(4) NOT NULL,
    coe_date_debut date NOT NULL,
    coe_date_fin date NOT NULL,
    coe_valeur_coefficient real,
    coe_commentaires text,
    coe_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_coe_date_fin CHECK ((coe_date_fin >= coe_date_debut)),
    CONSTRAINT c_nn_coe_org_code CHECK ((coe_org_code IS NOT NULL))
);


ALTER TABLE bresle.tj_coefficientconversion_coe OWNER TO postgres;

--
-- Name: tj_conditionenvironnementale_env; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE tj_conditionenvironnementale_env (
    env_date_debut timestamp(0) without time zone NOT NULL,
    env_date_fin timestamp(0) without time zone NOT NULL,
    env_methode_obtention character varying(10),
    env_val_identifiant integer,
    env_valeur_quantitatif real,
    env_stm_identifiant integer NOT NULL,
    env_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_env CHECK ((((env_val_identifiant IS NOT NULL) AND (env_valeur_quantitatif IS NULL)) OR ((env_val_identifiant IS NULL) AND (env_valeur_quantitatif IS NOT NULL)))),
    CONSTRAINT c_ck_env_date_fin CHECK ((env_date_fin >= env_date_debut)),
    CONSTRAINT c_ck_env_methode_obtention CHECK (((upper((env_methode_obtention)::text) = 'MESURE'::text) OR (upper((env_methode_obtention)::text) = 'CALCULE'::text)))
);


ALTER TABLE bresle.tj_conditionenvironnementale_env OWNER TO postgres;

--
-- Name: tj_dfestdestinea_dtx; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE tj_dfestdestinea_dtx (
    dtx_dif_identifiant integer NOT NULL,
    dtx_tax_code character varying(6) NOT NULL,
    dtx_org_code character varying(30) NOT NULL
);


ALTER TABLE bresle.tj_dfestdestinea_dtx OWNER TO postgres;

--
-- Name: tj_dfesttype_dft; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE tj_dfesttype_dft (
    dft_df_identifiant integer NOT NULL,
    dft_tdf_code integer NOT NULL,
    dft_rang smallint NOT NULL,
    dft_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_dft_rang CHECK ((dft_rang >= 0))
);


ALTER TABLE bresle.tj_dfesttype_dft OWNER TO postgres;

--
-- Name: tj_pathologieconstatee_pco; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE tj_pathologieconstatee_pco (
    pco_lot_identifiant integer NOT NULL,
    pco_pat_code character varying(4) NOT NULL,
    pco_loc_code character varying(4) NOT NULL,
    pco_commentaires text,
    pco_org_code character varying(30) NOT NULL
);


ALTER TABLE bresle.tj_pathologieconstatee_pco OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: tj_prelevementlot_prl; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE tj_prelevementlot_prl (
    prl_pre_typeprelevement character varying(15) NOT NULL,
    prl_lot_identifiant integer NOT NULL,
    prl_code character varying(12) NOT NULL,
    prl_operateur character varying(35),
    prl_loc_code character varying(4),
    prl_commentaires text,
    prl_org_code character varying(30) NOT NULL
);


ALTER TABLE bresle.tj_prelevementlot_prl OWNER TO postgres;

--
-- Name: tj_stationmesure_stm; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE tj_stationmesure_stm (
    stm_identifiant integer NOT NULL,
    stm_libelle character varying(12),
    stm_sta_code character varying(8) NOT NULL,
    stm_par_code character varying(5) NOT NULL,
    stm_description text,
    stm_org_code character varying(30) NOT NULL
);


ALTER TABLE bresle.tj_stationmesure_stm OWNER TO postgres;

--
-- Name: tj_stationmesure_stm_stm_identifiant_seq; Type: SEQUENCE; Schema: bresle; Owner: postgres
--

CREATE SEQUENCE tj_stationmesure_stm_stm_identifiant_seq
    START WITH 19
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bresle.tj_stationmesure_stm_stm_identifiant_seq OWNER TO postgres;

--
-- Name: tj_stationmesure_stm_stm_identifiant_seq; Type: SEQUENCE OWNED BY; Schema: bresle; Owner: postgres
--

ALTER SEQUENCE tj_stationmesure_stm_stm_identifiant_seq OWNED BY tj_stationmesure_stm.stm_identifiant;


SET default_with_oids = true;

--
-- Name: tj_tauxechappement_txe; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE tj_tauxechappement_txe (
    txe_ouv_identifiant integer NOT NULL,
    txe_tax_code character varying(6) NOT NULL,
    txe_std_code character varying(4) NOT NULL,
    txe_date_debut timestamp(0) without time zone NOT NULL,
    txe_date_fin timestamp(0) without time zone NOT NULL,
    txe_methode_estimation text,
    txe_ech_code character varying(4),
    txe_valeur_taux smallint,
    txe_commentaires text,
    txe_org_code character varying(30) NOT NULL,
    CONSTRAINT c_ck_txe_date_fin CHECK ((txe_date_fin >= txe_date_debut)),
    CONSTRAINT c_ck_txe_methode_estimation CHECK ((((upper(txe_methode_estimation) = 'MESURE'::text) OR (upper(txe_methode_estimation) = 'CALCULE'::text)) OR (upper(txe_methode_estimation) = 'EXPERT'::text))),
    CONSTRAINT c_ck_txe_valeur_taux CHECK ((((txe_valeur_taux >= 0) AND (txe_valeur_taux <= 100)) OR ((txe_valeur_taux IS NULL) AND (txe_ech_code IS NOT NULL))))
);


ALTER TABLE bresle.tj_tauxechappement_txe OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: ts_maintenance_main; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE ts_maintenance_main (
    main_identifiant integer NOT NULL,
    main_ticket integer,
    main_description text
);


ALTER TABLE bresle.ts_maintenance_main OWNER TO postgres;

--
-- Name: TABLE ts_maintenance_main; Type: COMMENT; Schema: bresle; Owner: postgres
--

COMMENT ON TABLE ts_maintenance_main IS 'Table de suivi des operations de maintenance de la base';


--
-- Name: ts_maintenance_main_main_identifiant_seq; Type: SEQUENCE; Schema: bresle; Owner: postgres
--

CREATE SEQUENCE ts_maintenance_main_main_identifiant_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bresle.ts_maintenance_main_main_identifiant_seq OWNER TO postgres;

--
-- Name: ts_maintenance_main_main_identifiant_seq; Type: SEQUENCE OWNED BY; Schema: bresle; Owner: postgres
--

ALTER SEQUENCE ts_maintenance_main_main_identifiant_seq OWNED BY ts_maintenance_main.main_identifiant;


SET default_with_oids = true;

--
-- Name: ts_taillevideo_tav; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE ts_taillevideo_tav (
    tav_dic_identifiant integer NOT NULL,
    tav_coefconversion numeric NOT NULL,
    tav_distance character varying(3) NOT NULL,
    tav_org_code character varying(30) NOT NULL
);


ALTER TABLE bresle.ts_taillevideo_tav OWNER TO postgres;

--
-- Name: ts_taxonvideo_txv; Type: TABLE; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE TABLE ts_taxonvideo_txv (
    txv_code character varying(3) NOT NULL,
    txv_tax_code character varying(6),
    txv_std_code character varying(4),
    txv_org_code character varying(30) NOT NULL
);


ALTER TABLE bresle.ts_taxonvideo_txv OWNER TO postgres;

--
-- Name: v_taxon_tax; Type: VIEW; Schema: bresle; Owner: postgres
--

CREATE VIEW v_taxon_tax AS
    SELECT tax.tax_code, tax.tax_nom_latin, tax.tax_nom_commun, tax.tax_ntx_code, tax.tax_tax_code, tax.tax_rang, txv.txv_code, txv.txv_tax_code, txv.txv_std_code, std.std_code, std.std_libelle, std.std_rang FROM ((ref.tr_taxon_tax tax RIGHT JOIN ts_taxonvideo_txv txv ON (((tax.tax_code)::text = (txv.txv_tax_code)::text))) LEFT JOIN ref.tr_stadedeveloppement_std std ON (((txv.txv_std_code)::text = (std.std_code)::text)));


ALTER TABLE bresle.v_taxon_tax OWNER TO postgres;

--
-- Name: vue_lot_ope_car; Type: VIEW; Schema: bresle; Owner: postgres
--

CREATE VIEW vue_lot_ope_car AS
    SELECT t_operation_ope.ope_identifiant, t_lot_lot.lot_identifiant, t_operation_ope.ope_dic_identifiant, t_lot_lot.lot_lot_identifiant AS lot_pere, t_operation_ope.ope_date_debut, t_operation_ope.ope_date_fin, t_lot_lot.lot_effectif, t_lot_lot.lot_quantite, t_lot_lot.lot_tax_code, t_lot_lot.lot_std_code, tr_taxon_tax.tax_nom_latin, tr_stadedeveloppement_std.std_libelle, tr_devenirlot_dev.dev_code, tr_devenirlot_dev.dev_libelle, tg_parametre_par.par_nom, tj_caracteristiquelot_car.car_par_code, tj_caracteristiquelot_car.car_methode_obtention, tj_caracteristiquelot_car.car_val_identifiant, tj_caracteristiquelot_car.car_valeur_quantitatif, tr_valeurparametrequalitatif_val.val_libelle FROM (((((((((t_operation_ope JOIN t_lot_lot ON ((t_lot_lot.lot_ope_identifiant = t_operation_ope.ope_identifiant))) LEFT JOIN ref.tr_typequantitelot_qte ON (((tr_typequantitelot_qte.qte_code)::text = (t_lot_lot.lot_qte_code)::text))) LEFT JOIN ref.tr_devenirlot_dev ON (((tr_devenirlot_dev.dev_code)::text = (t_lot_lot.lot_dev_code)::text))) JOIN ref.tr_taxon_tax ON (((tr_taxon_tax.tax_code)::text = (t_lot_lot.lot_tax_code)::text))) JOIN ref.tr_stadedeveloppement_std ON (((tr_stadedeveloppement_std.std_code)::text = (t_lot_lot.lot_std_code)::text))) JOIN tj_caracteristiquelot_car ON ((tj_caracteristiquelot_car.car_lot_identifiant = t_lot_lot.lot_identifiant))) LEFT JOIN ref.tg_parametre_par ON (((tj_caracteristiquelot_car.car_par_code)::text = (tg_parametre_par.par_code)::text))) LEFT JOIN ref.tr_parametrequalitatif_qal ON (((tr_parametrequalitatif_qal.qal_par_code)::text = (tg_parametre_par.par_code)::text))) LEFT JOIN ref.tr_valeurparametrequalitatif_val ON ((tj_caracteristiquelot_car.car_val_identifiant = tr_valeurparametrequalitatif_val.val_identifiant))) ORDER BY t_operation_ope.ope_date_debut;


ALTER TABLE bresle.vue_lot_ope_car OWNER TO postgres;

--
-- Name: vue_lot_ope_car_qan; Type: VIEW; Schema: bresle; Owner: postgres
--

CREATE VIEW vue_lot_ope_car_qan AS
    SELECT t_operation_ope.ope_identifiant, t_lot_lot.lot_identifiant, t_operation_ope.ope_dic_identifiant, t_lot_lot.lot_lot_identifiant AS lot_pere, t_operation_ope.ope_date_debut, t_operation_ope.ope_date_fin, t_lot_lot.lot_effectif, t_lot_lot.lot_quantite, t_lot_lot.lot_tax_code, tr_taxon_tax.tax_nom_latin, tr_stadedeveloppement_std.std_libelle, tr_devenirlot_dev.dev_code, tr_devenirlot_dev.dev_libelle, tg_parametre_par.par_nom, tj_caracteristiquelot_car.car_par_code, tj_caracteristiquelot_car.car_methode_obtention, tj_caracteristiquelot_car.car_val_identifiant, tj_caracteristiquelot_car.car_valeur_quantitatif, tr_valeurparametrequalitatif_val.val_libelle FROM (((((((((t_operation_ope JOIN t_lot_lot ON ((t_lot_lot.lot_ope_identifiant = t_operation_ope.ope_identifiant))) LEFT JOIN ref.tr_typequantitelot_qte ON (((tr_typequantitelot_qte.qte_code)::text = (t_lot_lot.lot_qte_code)::text))) LEFT JOIN ref.tr_devenirlot_dev ON (((tr_devenirlot_dev.dev_code)::text = (t_lot_lot.lot_dev_code)::text))) JOIN ref.tr_taxon_tax ON (((tr_taxon_tax.tax_code)::text = (t_lot_lot.lot_tax_code)::text))) JOIN ref.tr_stadedeveloppement_std ON (((tr_stadedeveloppement_std.std_code)::text = (t_lot_lot.lot_std_code)::text))) JOIN tj_caracteristiquelot_car ON ((tj_caracteristiquelot_car.car_lot_identifiant = t_lot_lot.lot_identifiant))) LEFT JOIN ref.tg_parametre_par ON (((tj_caracteristiquelot_car.car_par_code)::text = (tg_parametre_par.par_code)::text))) LEFT JOIN ref.tr_parametrequantitatif_qan ON (((tr_parametrequantitatif_qan.qan_par_code)::text = (tg_parametre_par.par_code)::text))) LEFT JOIN ref.tr_valeurparametrequalitatif_val ON ((tj_caracteristiquelot_car.car_val_identifiant = tr_valeurparametrequalitatif_val.val_identifiant))) ORDER BY t_operation_ope.ope_date_debut;


ALTER TABLE bresle.vue_lot_ope_car_qan OWNER TO postgres;

--
-- Name: vue_ope_lot_ech_parqual; Type: VIEW; Schema: bresle; Owner: postgres
--

CREATE VIEW vue_ope_lot_ech_parqual AS
    SELECT t_operation_ope.ope_identifiant, t_operation_ope.ope_dic_identifiant, t_operation_ope.ope_date_debut, t_operation_ope.ope_date_fin, lot.lot_identifiant, lot.lot_methode_obtention, lot.lot_effectif, lot.lot_quantite, lot.lot_tax_code, lot.lot_std_code, tr_taxon_tax.tax_nom_latin, tr_stadedeveloppement_std.std_libelle, dev.dev_code, dev.dev_libelle, par.par_nom, car.car_par_code, car.car_methode_obtention, car.car_val_identifiant, val.val_libelle, lot.lot_lot_identifiant AS lot_pere, lot_pere.lot_effectif AS lot_pere_effectif, lot_pere.lot_quantite AS lot_pere_quantite, dev_pere.dev_code AS lot_pere_dev_code, dev_pere.dev_libelle AS lot_pere_dev_libelle, parqual.par_nom AS lot_pere_par_nom, parqual.car_par_code AS lot_pere_par_code, parqual.car_methode_obtention AS lot_pere_car_methode_obtention, parqual.car_val_identifiant AS lot_pere_val_identifiant, parqual.val_libelle AS lot_pere_val_libelle FROM (((((((((((((t_operation_ope JOIN t_lot_lot lot ON ((lot.lot_ope_identifiant = t_operation_ope.ope_identifiant))) LEFT JOIN ref.tr_typequantitelot_qte qte ON (((qte.qte_code)::text = (lot.lot_qte_code)::text))) LEFT JOIN ref.tr_devenirlot_dev dev ON (((dev.dev_code)::text = (lot.lot_dev_code)::text))) JOIN ref.tr_taxon_tax ON (((tr_taxon_tax.tax_code)::text = (lot.lot_tax_code)::text))) JOIN ref.tr_stadedeveloppement_std ON (((tr_stadedeveloppement_std.std_code)::text = (lot.lot_std_code)::text))) JOIN tj_caracteristiquelot_car car ON ((car.car_lot_identifiant = lot.lot_identifiant))) LEFT JOIN ref.tg_parametre_par par ON (((car.car_par_code)::text = (par.par_code)::text))) JOIN ref.tr_parametrequalitatif_qal qal ON (((qal.qal_par_code)::text = (par.par_code)::text))) LEFT JOIN ref.tr_valeurparametrequalitatif_val val ON ((car.car_val_identifiant = val.val_identifiant))) LEFT JOIN t_lot_lot lot_pere ON ((lot_pere.lot_identifiant = lot.lot_lot_identifiant))) LEFT JOIN ref.tr_typequantitelot_qte qte_pere ON (((qte_pere.qte_code)::text = (lot_pere.lot_qte_code)::text))) LEFT JOIN ref.tr_devenirlot_dev dev_pere ON (((dev_pere.dev_code)::text = (lot_pere.lot_dev_code)::text))) LEFT JOIN (SELECT car_pere.car_lot_identifiant, car_pere.car_par_code, car_pere.car_methode_obtention, car_pere.car_val_identifiant, car_pere.car_valeur_quantitatif, car_pere.car_precision, car_pere.car_commentaires, par_pere.par_code, par_pere.par_nom, par_pere.par_unite, par_pere.par_nature, par_pere.par_definition, qal_pere.qal_par_code, qal_pere.qal_valeurs_possibles, val_pere.val_identifiant, val_pere.val_qal_code, val_pere.val_rang, val_pere.val_libelle FROM (((tj_caracteristiquelot_car car_pere LEFT JOIN ref.tg_parametre_par par_pere ON (((car_pere.car_par_code)::text = (par_pere.par_code)::text))) JOIN ref.tr_parametrequalitatif_qal qal_pere ON (((qal_pere.qal_par_code)::text = (par_pere.par_code)::text))) LEFT JOIN ref.tr_valeurparametrequalitatif_val val_pere ON ((car_pere.car_val_identifiant = val_pere.val_identifiant)))) parqual ON ((parqual.car_lot_identifiant = lot_pere.lot_identifiant)));


ALTER TABLE bresle.vue_ope_lot_ech_parqual OWNER TO postgres;

--
-- Name: vue_ope_lot_ech_parquan; Type: VIEW; Schema: bresle; Owner: postgres
--

CREATE VIEW vue_ope_lot_ech_parquan AS
    SELECT t_operation_ope.ope_identifiant, t_operation_ope.ope_dic_identifiant, t_operation_ope.ope_date_debut, t_operation_ope.ope_date_fin, lot.lot_identifiant, lot.lot_methode_obtention, lot.lot_effectif, lot.lot_quantite, lot.lot_tax_code, lot.lot_std_code, tr_taxon_tax.tax_nom_latin, tr_stadedeveloppement_std.std_libelle, dev.dev_code, dev.dev_libelle, par.par_nom, car.car_par_code, car.car_methode_obtention, car.car_valeur_quantitatif, lot.lot_lot_identifiant AS lot_pere, lot_pere.lot_effectif AS lot_pere_effectif, lot_pere.lot_quantite AS lot_pere_quantite, dev_pere.dev_code AS lot_pere_dev_code, dev_pere.dev_libelle AS lot_pere_dev_libelle, parqual.par_nom AS lot_pere_par_nom, parqual.car_par_code AS lot_pere_par_code, parqual.car_methode_obtention AS lot_pere_car_methode_obtention, parqual.car_val_identifiant AS lot_pere_val_identifiant, parqual.val_libelle AS lot_pere_val_libelle FROM ((((((((((((t_operation_ope JOIN t_lot_lot lot ON ((lot.lot_ope_identifiant = t_operation_ope.ope_identifiant))) LEFT JOIN ref.tr_typequantitelot_qte qte ON (((qte.qte_code)::text = (lot.lot_qte_code)::text))) LEFT JOIN ref.tr_devenirlot_dev dev ON (((dev.dev_code)::text = (lot.lot_dev_code)::text))) JOIN ref.tr_taxon_tax ON (((tr_taxon_tax.tax_code)::text = (lot.lot_tax_code)::text))) JOIN ref.tr_stadedeveloppement_std ON (((tr_stadedeveloppement_std.std_code)::text = (lot.lot_std_code)::text))) JOIN tj_caracteristiquelot_car car ON ((car.car_lot_identifiant = lot.lot_identifiant))) LEFT JOIN ref.tg_parametre_par par ON (((car.car_par_code)::text = (par.par_code)::text))) JOIN ref.tr_parametrequantitatif_qan qan ON (((qan.qan_par_code)::text = (par.par_code)::text))) LEFT JOIN t_lot_lot lot_pere ON ((lot_pere.lot_identifiant = lot.lot_lot_identifiant))) LEFT JOIN ref.tr_typequantitelot_qte qte_pere ON (((qte_pere.qte_code)::text = (lot_pere.lot_qte_code)::text))) LEFT JOIN ref.tr_devenirlot_dev dev_pere ON (((dev_pere.dev_code)::text = (lot_pere.lot_dev_code)::text))) LEFT JOIN (SELECT car_pere.car_lot_identifiant, car_pere.car_par_code, car_pere.car_methode_obtention, car_pere.car_val_identifiant, car_pere.car_valeur_quantitatif, car_pere.car_precision, car_pere.car_commentaires, par_pere.par_code, par_pere.par_nom, par_pere.par_unite, par_pere.par_nature, par_pere.par_definition, qal_pere.qal_par_code, qal_pere.qal_valeurs_possibles, val_pere.val_identifiant, val_pere.val_qal_code, val_pere.val_rang, val_pere.val_libelle FROM (((tj_caracteristiquelot_car car_pere LEFT JOIN ref.tg_parametre_par par_pere ON (((car_pere.car_par_code)::text = (par_pere.par_code)::text))) JOIN ref.tr_parametrequalitatif_qal qal_pere ON (((qal_pere.qal_par_code)::text = (par_pere.par_code)::text))) LEFT JOIN ref.tr_valeurparametrequalitatif_val val_pere ON ((car_pere.car_val_identifiant = val_pere.val_identifiant)))) parqual ON ((parqual.car_lot_identifiant = lot_pere.lot_identifiant)));


ALTER TABLE bresle.vue_ope_lot_ech_parquan OWNER TO postgres;

--
-- Name: bjo_identifiant; Type: DEFAULT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_bilanmigrationjournalier_bjo ALTER COLUMN bjo_identifiant SET DEFAULT nextval('t_bilanmigrationjournalier_bjo_bjo_identifiant_seq'::regclass);


--
-- Name: bme_identifiant; Type: DEFAULT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_bilanmigrationmensuel_bme ALTER COLUMN bme_identifiant SET DEFAULT nextval('t_bilanmigrationmensuel_bme_bme_identifiant_seq'::regclass);


--
-- Name: lot_identifiant; Type: DEFAULT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_lot_lot ALTER COLUMN lot_identifiant SET DEFAULT nextval('t_lot_lot_lot_identifiant_seq'::regclass);


--
-- Name: ope_identifiant; Type: DEFAULT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_operation_ope ALTER COLUMN ope_identifiant SET DEFAULT nextval('t_operation_ope_ope_identifiant_seq'::regclass);


--
-- Name: ouv_identifiant; Type: DEFAULT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_ouvrage_ouv ALTER COLUMN ouv_identifiant SET DEFAULT nextval('t_ouvrage_ouv_ouv_identifiant_seq'::regclass);


--
-- Name: dis_identifiant; Type: DEFAULT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tg_dispositif_dis ALTER COLUMN dis_identifiant SET DEFAULT nextval('tg_dispositif_dis_dis_identifiant_seq'::regclass);


--
-- Name: stm_identifiant; Type: DEFAULT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_stationmesure_stm ALTER COLUMN stm_identifiant SET DEFAULT nextval('tj_stationmesure_stm_stm_identifiant_seq'::regclass);


--
-- Name: main_identifiant; Type: DEFAULT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY ts_maintenance_main ALTER COLUMN main_identifiant SET DEFAULT nextval('ts_maintenance_main_main_identifiant_seq'::regclass);


--
-- Name: c_pk_act; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tj_actionmarquage_act
    ADD CONSTRAINT c_pk_act PRIMARY KEY (act_lot_identifiant, act_mqe_reference, act_org_code);


--
-- Name: c_pk_bjo; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_bilanmigrationjournalier_bjo
    ADD CONSTRAINT c_pk_bjo PRIMARY KEY (bjo_identifiant, bjo_org_code);


--
-- Name: c_pk_bme; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_bilanmigrationmensuel_bme
    ADD CONSTRAINT c_pk_bme PRIMARY KEY (bme_identifiant, bme_org_code);


--
-- Name: c_pk_car; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tj_caracteristiquelot_car
    ADD CONSTRAINT c_pk_car PRIMARY KEY (car_lot_identifiant, car_par_code, car_org_code);


--
-- Name: c_pk_coe; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tj_coefficientconversion_coe
    ADD CONSTRAINT c_pk_coe PRIMARY KEY (coe_tax_code, coe_std_code, coe_qte_code, coe_date_debut, coe_date_fin, coe_org_code);


--
-- Name: c_pk_dft; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tj_dfesttype_dft
    ADD CONSTRAINT c_pk_dft PRIMARY KEY (dft_df_identifiant, dft_tdf_code, dft_rang, dft_org_code);


--
-- Name: c_pk_dic; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_dispositifcomptage_dic
    ADD CONSTRAINT c_pk_dic PRIMARY KEY (dic_code, dic_org_code);


--
-- Name: c_pk_dif; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_dispositiffranchissement_dif
    ADD CONSTRAINT c_pk_dif PRIMARY KEY (dif_code, dif_org_code);


--
-- Name: c_pk_dis; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tg_dispositif_dis
    ADD CONSTRAINT c_pk_dis PRIMARY KEY (dis_identifiant, dis_org_code);


--
-- Name: c_pk_dtx; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tj_dfestdestinea_dtx
    ADD CONSTRAINT c_pk_dtx PRIMARY KEY (dtx_dif_identifiant, dtx_tax_code, dtx_org_code);


--
-- Name: c_pk_env; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tj_conditionenvironnementale_env
    ADD CONSTRAINT c_pk_env PRIMARY KEY (env_stm_identifiant, env_date_debut, env_date_fin, env_org_code);


--
-- Name: c_pk_lot; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_lot_lot
    ADD CONSTRAINT c_pk_lot PRIMARY KEY (lot_identifiant, lot_org_code);


--
-- Name: c_pk_mqe; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_marque_mqe
    ADD CONSTRAINT c_pk_mqe PRIMARY KEY (mqe_reference, mqe_org_code);


--
-- Name: c_pk_omg; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_operationmarquage_omq
    ADD CONSTRAINT c_pk_omg PRIMARY KEY (omq_reference);


--
-- Name: c_pk_ope; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_operation_ope
    ADD CONSTRAINT c_pk_ope PRIMARY KEY (ope_identifiant, ope_org_code);


--
-- Name: c_pk_ouv; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_ouvrage_ouv
    ADD CONSTRAINT c_pk_ouv PRIMARY KEY (ouv_identifiant, ouv_org_code);


--
-- Name: c_pk_pco; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tj_pathologieconstatee_pco
    ADD CONSTRAINT c_pk_pco PRIMARY KEY (pco_lot_identifiant, pco_pat_code, pco_loc_code, pco_org_code);


--
-- Name: c_pk_per; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_periodefonctdispositif_per
    ADD CONSTRAINT c_pk_per PRIMARY KEY (per_dis_identifiant, per_org_code, per_date_debut, per_date_fin);


--
-- Name: c_pk_prl; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tj_prelevementlot_prl
    ADD CONSTRAINT c_pk_prl PRIMARY KEY (prl_pre_typeprelevement, prl_lot_identifiant, prl_code, prl_org_code);


--
-- Name: c_pk_sta; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_station_sta
    ADD CONSTRAINT c_pk_sta PRIMARY KEY (sta_code, sta_org_code);


--
-- Name: c_pk_stm; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tj_stationmesure_stm
    ADD CONSTRAINT c_pk_stm PRIMARY KEY (stm_identifiant, stm_org_code);


--
-- Name: c_pk_tav; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ts_taillevideo_tav
    ADD CONSTRAINT c_pk_tav PRIMARY KEY (tav_distance, tav_dic_identifiant, tav_org_code);


--
-- Name: c_pk_txe; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tj_tauxechappement_txe
    ADD CONSTRAINT c_pk_txe PRIMARY KEY (txe_ouv_identifiant, txe_org_code, txe_tax_code, txe_std_code, txe_date_debut, txe_date_fin);


--
-- Name: c_pk_txv; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ts_taxonvideo_txv
    ADD CONSTRAINT c_pk_txv PRIMARY KEY (txv_code, txv_org_code);


--
-- Name: c_uk_prl_code; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tj_prelevementlot_prl
    ADD CONSTRAINT c_uk_prl_code UNIQUE (prl_code);


--
-- Name: c_uk_stm; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tj_stationmesure_stm
    ADD CONSTRAINT c_uk_stm UNIQUE (stm_libelle);


--
-- Name: c_uq_dic; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_dispositifcomptage_dic
    ADD CONSTRAINT c_uq_dic UNIQUE (dic_dif_identifiant, dic_code);


--
-- Name: c_uq_dif; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_dispositiffranchissement_dif
    ADD CONSTRAINT c_uq_dif UNIQUE (dif_ouv_identifiant, dif_code);


--
-- Name: c_uq_ope; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_operation_ope
    ADD CONSTRAINT c_uq_ope UNIQUE (ope_dic_identifiant, ope_date_debut, ope_date_fin);


--
-- Name: c_uq_ouv; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_ouvrage_ouv
    ADD CONSTRAINT c_uq_ouv UNIQUE (ouv_sta_code, ouv_code);


--
-- Name: c_uq_sta_nom; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_station_sta
    ADD CONSTRAINT c_uq_sta_nom UNIQUE (sta_nom);


--
-- Name: ts_maintenance_main_pkey; Type: CONSTRAINT; Schema: bresle; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ts_maintenance_main
    ADD CONSTRAINT ts_maintenance_main_pkey PRIMARY KEY (main_identifiant);


--
-- Name: i_car_lotETpar; Type: INDEX; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE INDEX "i_car_lotETpar" ON tj_caracteristiquelot_car USING btree (car_lot_identifiant, car_par_code);


--
-- Name: i_lot_identifiant; Type: INDEX; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE INDEX i_lot_identifiant ON t_lot_lot USING btree (lot_identifiant);


--
-- Name: i_lot_lot_identifiant; Type: INDEX; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE INDEX i_lot_lot_identifiant ON t_lot_lot USING btree (lot_lot_identifiant);


--
-- Name: i_lot_ope_identifiant; Type: INDEX; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE INDEX i_lot_ope_identifiant ON t_lot_lot USING btree (lot_ope_identifiant);


--
-- Name: i_lot_taxETstd_code; Type: INDEX; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE INDEX "i_lot_taxETstd_code" ON t_lot_lot USING btree (lot_tax_code, lot_std_code);


--
-- Name: i_ope_date; Type: INDEX; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE INDEX i_ope_date ON t_operation_ope USING btree (ope_date_debut, ope_date_fin);


--
-- Name: i_ope_identifiant; Type: INDEX; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE INDEX i_ope_identifiant ON t_operation_ope USING btree (ope_identifiant);


--
-- Name: i_per_dates; Type: INDEX; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE INDEX i_per_dates ON t_periodefonctdispositif_per USING btree (per_date_debut, per_date_fin);


--
-- Name: i_txv_code; Type: INDEX; Schema: bresle; Owner: postgres; Tablespace: 
--

CREATE INDEX i_txv_code ON ts_taxonvideo_txv USING btree (txv_code);


--
-- Name: trg_coe_date; Type: TRIGGER; Schema: bresle; Owner: postgres
--

CREATE TRIGGER trg_coe_date AFTER INSERT OR UPDATE ON tj_coefficientconversion_coe FOR EACH ROW EXECUTE PROCEDURE fct_coe_date();


--
-- Name: trg_ope_date; Type: TRIGGER; Schema: bresle; Owner: postgres
--

CREATE TRIGGER trg_ope_date AFTER INSERT OR UPDATE ON t_operation_ope FOR EACH ROW EXECUTE PROCEDURE fct_ope_date();


--
-- Name: trg_per_date; Type: TRIGGER; Schema: bresle; Owner: postgres
--

CREATE TRIGGER trg_per_date AFTER INSERT OR UPDATE ON t_periodefonctdispositif_per FOR EACH ROW EXECUTE PROCEDURE fct_per_date();


--
-- Name: trg_per_suppression; Type: TRIGGER; Schema: bresle; Owner: postgres
--

CREATE TRIGGER trg_per_suppression AFTER DELETE ON t_periodefonctdispositif_per FOR EACH ROW EXECUTE PROCEDURE fct_per_suppression();


--
-- Name: trg_txe_date; Type: TRIGGER; Schema: bresle; Owner: postgres
--

CREATE TRIGGER trg_txe_date AFTER INSERT OR UPDATE ON tj_tauxechappement_txe FOR EACH ROW EXECUTE PROCEDURE fct_txe_date();


--
-- Name: c_fk_act_lot_identifiant; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_actionmarquage_act
    ADD CONSTRAINT c_fk_act_lot_identifiant FOREIGN KEY (act_lot_identifiant, act_org_code) REFERENCES t_lot_lot(lot_identifiant, lot_org_code);


--
-- Name: c_fk_act_mqe_reference; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_actionmarquage_act
    ADD CONSTRAINT c_fk_act_mqe_reference FOREIGN KEY (act_mqe_reference, act_org_code) REFERENCES t_marque_mqe(mqe_reference, mqe_org_code);


--
-- Name: c_fk_act_org_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_actionmarquage_act
    ADD CONSTRAINT c_fk_act_org_code FOREIGN KEY (act_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL;


--
-- Name: c_fk_bjo_std_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_bilanmigrationjournalier_bjo
    ADD CONSTRAINT c_fk_bjo_std_code FOREIGN KEY (bjo_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code);


--
-- Name: c_fk_bjo_tax_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_bilanmigrationjournalier_bjo
    ADD CONSTRAINT c_fk_bjo_tax_code FOREIGN KEY (bjo_tax_code) REFERENCES ref.tr_taxon_tax(tax_code);


--
-- Name: c_fk_bme_std_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_bilanmigrationmensuel_bme
    ADD CONSTRAINT c_fk_bme_std_code FOREIGN KEY (bme_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code);


--
-- Name: c_fk_bme_tax_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_bilanmigrationmensuel_bme
    ADD CONSTRAINT c_fk_bme_tax_code FOREIGN KEY (bme_tax_code) REFERENCES ref.tr_taxon_tax(tax_code);


--
-- Name: c_fk_car_lot_identifiant; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_caracteristiquelot_car
    ADD CONSTRAINT c_fk_car_lot_identifiant FOREIGN KEY (car_lot_identifiant, car_org_code) REFERENCES t_lot_lot(lot_identifiant, lot_org_code);


--
-- Name: c_fk_car_org_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_caracteristiquelot_car
    ADD CONSTRAINT c_fk_car_org_code FOREIGN KEY (car_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL;


--
-- Name: c_fk_car_par_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_caracteristiquelot_car
    ADD CONSTRAINT c_fk_car_par_code FOREIGN KEY (car_par_code) REFERENCES ref.tg_parametre_par(par_code);


--
-- Name: c_fk_car_val_identifiant; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_caracteristiquelot_car
    ADD CONSTRAINT c_fk_car_val_identifiant FOREIGN KEY (car_val_identifiant) REFERENCES ref.tr_valeurparametrequalitatif_val(val_identifiant);


--
-- Name: c_fk_coe_org_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_coefficientconversion_coe
    ADD CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code) REFERENCES ref.ts_organisme_org(org_code);


--
-- Name: c_fk_coe_qte_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_coefficientconversion_coe
    ADD CONSTRAINT c_fk_coe_qte_code FOREIGN KEY (coe_qte_code) REFERENCES ref.tr_typequantitelot_qte(qte_code);


--
-- Name: c_fk_coe_std_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_coefficientconversion_coe
    ADD CONSTRAINT c_fk_coe_std_code FOREIGN KEY (coe_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code);


--
-- Name: c_fk_coe_tax_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_coefficientconversion_coe
    ADD CONSTRAINT c_fk_coe_tax_code FOREIGN KEY (coe_tax_code) REFERENCES ref.tr_taxon_tax(tax_code);


--
-- Name: c_fk_dft_org_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_dfesttype_dft
    ADD CONSTRAINT c_fk_dft_org_code FOREIGN KEY (dft_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL;


--
-- Name: c_fk_dic_org_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_dispositifcomptage_dic
    ADD CONSTRAINT c_fk_dic_org_code FOREIGN KEY (dic_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL;


--
-- Name: c_fk_dic_tdc_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_dispositifcomptage_dic
    ADD CONSTRAINT c_fk_dic_tdc_code FOREIGN KEY (dic_tdc_code) REFERENCES ref.tr_typedc_tdc(tdc_code);


--
-- Name: c_fk_dif_org_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_dispositiffranchissement_dif
    ADD CONSTRAINT c_fk_dif_org_code FOREIGN KEY (dif_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL;


--
-- Name: c_fk_dtx_org_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_dfestdestinea_dtx
    ADD CONSTRAINT c_fk_dtx_org_code FOREIGN KEY (dtx_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL;


--
-- Name: c_fk_dtx_tax_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_dfestdestinea_dtx
    ADD CONSTRAINT c_fk_dtx_tax_code FOREIGN KEY (dtx_tax_code) REFERENCES ref.tr_taxon_tax(tax_code);


--
-- Name: c_fk_env_org_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_conditionenvironnementale_env
    ADD CONSTRAINT c_fk_env_org_code FOREIGN KEY (env_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL;


--
-- Name: c_fk_env_stm_identifiant; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_conditionenvironnementale_env
    ADD CONSTRAINT c_fk_env_stm_identifiant FOREIGN KEY (env_stm_identifiant, env_org_code) REFERENCES tj_stationmesure_stm(stm_identifiant, stm_org_code);


--
-- Name: c_fk_env_val_identifiant; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_conditionenvironnementale_env
    ADD CONSTRAINT c_fk_env_val_identifiant FOREIGN KEY (env_val_identifiant) REFERENCES ref.tr_valeurparametrequalitatif_val(val_identifiant);


--
-- Name: c_fk_lot_dev_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_lot_lot
    ADD CONSTRAINT c_fk_lot_dev_code FOREIGN KEY (lot_dev_code) REFERENCES ref.tr_devenirlot_dev(dev_code);


--
-- Name: c_fk_lot_lot_identifiant; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_lot_lot
    ADD CONSTRAINT c_fk_lot_lot_identifiant FOREIGN KEY (lot_lot_identifiant, lot_org_code) REFERENCES t_lot_lot(lot_identifiant, lot_org_code);


--
-- Name: c_fk_lot_ope_identifiant; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_lot_lot
    ADD CONSTRAINT c_fk_lot_ope_identifiant FOREIGN KEY (lot_ope_identifiant, lot_org_code) REFERENCES t_operation_ope(ope_identifiant, ope_org_code);


--
-- Name: c_fk_lot_org_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_lot_lot
    ADD CONSTRAINT c_fk_lot_org_code FOREIGN KEY (lot_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL;


--
-- Name: c_fk_lot_qte_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_lot_lot
    ADD CONSTRAINT c_fk_lot_qte_code FOREIGN KEY (lot_qte_code) REFERENCES ref.tr_typequantitelot_qte(qte_code);


--
-- Name: c_fk_lot_std_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_lot_lot
    ADD CONSTRAINT c_fk_lot_std_code FOREIGN KEY (lot_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code);


--
-- Name: c_fk_lot_tax_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_lot_lot
    ADD CONSTRAINT c_fk_lot_tax_code FOREIGN KEY (lot_tax_code) REFERENCES ref.tr_taxon_tax(tax_code);


--
-- Name: c_fk_mqe_loc_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_marque_mqe
    ADD CONSTRAINT c_fk_mqe_loc_code FOREIGN KEY (mqe_loc_code) REFERENCES ref.tr_localisationanatomique_loc(loc_code);


--
-- Name: c_fk_mqe_nmq_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_marque_mqe
    ADD CONSTRAINT c_fk_mqe_nmq_code FOREIGN KEY (mqe_nmq_code) REFERENCES ref.tr_naturemarque_nmq(nmq_code);


--
-- Name: c_fk_mqe_omq_reference; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_marque_mqe
    ADD CONSTRAINT c_fk_mqe_omq_reference FOREIGN KEY (mqe_omq_reference) REFERENCES t_operationmarquage_omq(omq_reference);


--
-- Name: c_fk_mqe_org_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_marque_mqe
    ADD CONSTRAINT c_fk_mqe_org_code FOREIGN KEY (mqe_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL;


--
-- Name: c_fk_ope_dic_identifiant; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_operation_ope
    ADD CONSTRAINT c_fk_ope_dic_identifiant FOREIGN KEY (ope_dic_identifiant, ope_org_code) REFERENCES tg_dispositif_dis(dis_identifiant, dis_org_code);


--
-- Name: c_fk_ope_org_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_operation_ope
    ADD CONSTRAINT c_fk_ope_org_code FOREIGN KEY (ope_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL;


--
-- Name: c_fk_ouv_nov_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_ouvrage_ouv
    ADD CONSTRAINT c_fk_ouv_nov_code FOREIGN KEY (ouv_nov_code) REFERENCES ref.tr_natureouvrage_nov(nov_code);


--
-- Name: c_fk_ouv_org_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_ouvrage_ouv
    ADD CONSTRAINT c_fk_ouv_org_code FOREIGN KEY (ouv_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL;


--
-- Name: c_fk_pco_loc_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_pathologieconstatee_pco
    ADD CONSTRAINT c_fk_pco_loc_code FOREIGN KEY (pco_loc_code) REFERENCES ref.tr_localisationanatomique_loc(loc_code);


--
-- Name: c_fk_pco_lot_identifiant; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_pathologieconstatee_pco
    ADD CONSTRAINT c_fk_pco_lot_identifiant FOREIGN KEY (pco_lot_identifiant, pco_org_code) REFERENCES t_lot_lot(lot_identifiant, lot_org_code);


--
-- Name: c_fk_pco_org_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_pathologieconstatee_pco
    ADD CONSTRAINT c_fk_pco_org_code FOREIGN KEY (pco_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL;


--
-- Name: c_fk_pco_pat_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_pathologieconstatee_pco
    ADD CONSTRAINT c_fk_pco_pat_code FOREIGN KEY (pco_pat_code) REFERENCES ref.tr_pathologie_pat(pat_code);


--
-- Name: c_fk_per_dis_identifiant; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_periodefonctdispositif_per
    ADD CONSTRAINT c_fk_per_dis_identifiant FOREIGN KEY (per_dis_identifiant, per_org_code) REFERENCES tg_dispositif_dis(dis_identifiant, dis_org_code);


--
-- Name: c_fk_per_org_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_periodefonctdispositif_per
    ADD CONSTRAINT c_fk_per_org_code FOREIGN KEY (per_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL;


--
-- Name: c_fk_per_tar_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_periodefonctdispositif_per
    ADD CONSTRAINT c_fk_per_tar_code FOREIGN KEY (per_tar_code) REFERENCES ref.tr_typearretdisp_tar(tar_code);


--
-- Name: c_fk_prl_loc_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_prelevementlot_prl
    ADD CONSTRAINT c_fk_prl_loc_code FOREIGN KEY (prl_loc_code) REFERENCES ref.tr_localisationanatomique_loc(loc_code);


--
-- Name: c_fk_prl_org_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_prelevementlot_prl
    ADD CONSTRAINT c_fk_prl_org_code FOREIGN KEY (prl_org_code) REFERENCES ref.ts_organisme_org(org_code);


--
-- Name: c_fk_prl_pre_nom; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_prelevementlot_prl
    ADD CONSTRAINT c_fk_prl_pre_nom FOREIGN KEY (prl_pre_typeprelevement) REFERENCES ref.tr_prelevement_pre(pre_typeprelevement);


--
-- Name: c_fk_sta_org_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY t_station_sta
    ADD CONSTRAINT c_fk_sta_org_code FOREIGN KEY (sta_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL;


--
-- Name: c_fk_sta_org_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tg_dispositif_dis
    ADD CONSTRAINT c_fk_sta_org_code FOREIGN KEY (dis_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL;


--
-- Name: c_fk_std_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY ts_taxonvideo_txv
    ADD CONSTRAINT c_fk_std_code FOREIGN KEY (txv_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code);


--
-- Name: c_fk_stm_org_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_stationmesure_stm
    ADD CONSTRAINT c_fk_stm_org_code FOREIGN KEY (stm_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL;


--
-- Name: c_fk_stm_par_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_stationmesure_stm
    ADD CONSTRAINT c_fk_stm_par_code FOREIGN KEY (stm_par_code) REFERENCES ref.tg_parametre_par(par_code);


--
-- Name: c_fk_stm_sta_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_stationmesure_stm
    ADD CONSTRAINT c_fk_stm_sta_code FOREIGN KEY (stm_sta_code, stm_org_code) REFERENCES t_station_sta(sta_code, sta_org_code);


--
-- Name: c_fk_tav_org_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY ts_taillevideo_tav
    ADD CONSTRAINT c_fk_tav_org_code FOREIGN KEY (tav_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL;


--
-- Name: c_fk_txe_ech_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_tauxechappement_txe
    ADD CONSTRAINT c_fk_txe_ech_code FOREIGN KEY (txe_ech_code) REFERENCES ref.tr_niveauechappement_ech(ech_code);


--
-- Name: c_fk_txe_org_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_tauxechappement_txe
    ADD CONSTRAINT c_fk_txe_org_code FOREIGN KEY (txe_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL;


--
-- Name: c_fk_txe_ouv_identifiant; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_tauxechappement_txe
    ADD CONSTRAINT c_fk_txe_ouv_identifiant FOREIGN KEY (txe_ouv_identifiant, txe_org_code) REFERENCES t_ouvrage_ouv(ouv_identifiant, ouv_org_code);


--
-- Name: c_fk_txe_std_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_tauxechappement_txe
    ADD CONSTRAINT c_fk_txe_std_code FOREIGN KEY (txe_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code);


--
-- Name: c_fk_txe_tax_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY tj_tauxechappement_txe
    ADD CONSTRAINT c_fk_txe_tax_code FOREIGN KEY (txe_tax_code) REFERENCES ref.tr_taxon_tax(tax_code);


--
-- Name: c_fk_txv_org_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY ts_taxonvideo_txv
    ADD CONSTRAINT c_fk_txv_org_code FOREIGN KEY (txv_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL;


--
-- Name: c_fk_txv_tax_code; Type: FK CONSTRAINT; Schema: bresle; Owner: postgres
--

ALTER TABLE ONLY ts_taxonvideo_txv
    ADD CONSTRAINT c_fk_txv_tax_code FOREIGN KEY (txv_tax_code) REFERENCES ref.tr_taxon_tax(tax_code);


--
-- Name: bresle; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA bresle FROM PUBLIC;
REVOKE ALL ON SCHEMA bresle FROM postgres;
GRANT ALL ON SCHEMA bresle TO postgres;
GRANT ALL ON SCHEMA bresle TO PUBLIC;
GRANT ALL ON SCHEMA bresle TO bresle;


--
-- Name: t_bilanmigrationjournalier_bjo; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE t_bilanmigrationjournalier_bjo FROM PUBLIC;
REVOKE ALL ON TABLE t_bilanmigrationjournalier_bjo FROM postgres;
GRANT ALL ON TABLE t_bilanmigrationjournalier_bjo TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE t_bilanmigrationjournalier_bjo TO bresle;
GRANT SELECT ON TABLE t_bilanmigrationjournalier_bjo TO invite;


--
-- Name: t_bilanmigrationjournalier_bjo_bjo_identifiant_seq; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON SEQUENCE t_bilanmigrationjournalier_bjo_bjo_identifiant_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE t_bilanmigrationjournalier_bjo_bjo_identifiant_seq FROM postgres;
GRANT ALL ON SEQUENCE t_bilanmigrationjournalier_bjo_bjo_identifiant_seq TO postgres;
GRANT SELECT,UPDATE ON SEQUENCE t_bilanmigrationjournalier_bjo_bjo_identifiant_seq TO bresle;
GRANT SELECT,UPDATE ON SEQUENCE t_bilanmigrationjournalier_bjo_bjo_identifiant_seq TO invite;


--
-- Name: t_bilanmigrationmensuel_bme; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE t_bilanmigrationmensuel_bme FROM PUBLIC;
REVOKE ALL ON TABLE t_bilanmigrationmensuel_bme FROM postgres;
GRANT ALL ON TABLE t_bilanmigrationmensuel_bme TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE t_bilanmigrationmensuel_bme TO bresle;
GRANT SELECT ON TABLE t_bilanmigrationmensuel_bme TO invite;


--
-- Name: t_bilanmigrationmensuel_bme_bme_identifiant_seq; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON SEQUENCE t_bilanmigrationmensuel_bme_bme_identifiant_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE t_bilanmigrationmensuel_bme_bme_identifiant_seq FROM postgres;
GRANT ALL ON SEQUENCE t_bilanmigrationmensuel_bme_bme_identifiant_seq TO postgres;
GRANT SELECT,UPDATE ON SEQUENCE t_bilanmigrationmensuel_bme_bme_identifiant_seq TO bresle;
GRANT SELECT,UPDATE ON SEQUENCE t_bilanmigrationmensuel_bme_bme_identifiant_seq TO invite;


--
-- Name: t_dispositifcomptage_dic; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE t_dispositifcomptage_dic FROM PUBLIC;
REVOKE ALL ON TABLE t_dispositifcomptage_dic FROM postgres;
GRANT ALL ON TABLE t_dispositifcomptage_dic TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE t_dispositifcomptage_dic TO bresle;
GRANT SELECT ON TABLE t_dispositifcomptage_dic TO invite;


--
-- Name: t_dispositiffranchissement_dif; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE t_dispositiffranchissement_dif FROM PUBLIC;
REVOKE ALL ON TABLE t_dispositiffranchissement_dif FROM postgres;
GRANT ALL ON TABLE t_dispositiffranchissement_dif TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE t_dispositiffranchissement_dif TO bresle;
GRANT SELECT ON TABLE t_dispositiffranchissement_dif TO invite;


--
-- Name: t_lot_lot; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE t_lot_lot FROM PUBLIC;
REVOKE ALL ON TABLE t_lot_lot FROM postgres;
GRANT ALL ON TABLE t_lot_lot TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE t_lot_lot TO bresle;
GRANT SELECT ON TABLE t_lot_lot TO invite;


--
-- Name: t_lot_lot_lot_identifiant_seq; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON SEQUENCE t_lot_lot_lot_identifiant_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE t_lot_lot_lot_identifiant_seq FROM postgres;
GRANT ALL ON SEQUENCE t_lot_lot_lot_identifiant_seq TO postgres;
GRANT SELECT,UPDATE ON SEQUENCE t_lot_lot_lot_identifiant_seq TO bresle;
GRANT SELECT,UPDATE ON SEQUENCE t_lot_lot_lot_identifiant_seq TO invite;


--
-- Name: t_marque_mqe; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE t_marque_mqe FROM PUBLIC;
REVOKE ALL ON TABLE t_marque_mqe FROM postgres;
GRANT ALL ON TABLE t_marque_mqe TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE t_marque_mqe TO bresle;
GRANT SELECT ON TABLE t_marque_mqe TO invite;


--
-- Name: t_operation_ope; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE t_operation_ope FROM PUBLIC;
REVOKE ALL ON TABLE t_operation_ope FROM postgres;
GRANT ALL ON TABLE t_operation_ope TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE t_operation_ope TO bresle;
GRANT SELECT ON TABLE t_operation_ope TO invite;


--
-- Name: t_operation_ope_ope_identifiant_seq; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON SEQUENCE t_operation_ope_ope_identifiant_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE t_operation_ope_ope_identifiant_seq FROM postgres;
GRANT ALL ON SEQUENCE t_operation_ope_ope_identifiant_seq TO postgres;
GRANT SELECT,UPDATE ON SEQUENCE t_operation_ope_ope_identifiant_seq TO bresle;
GRANT SELECT,UPDATE ON SEQUENCE t_operation_ope_ope_identifiant_seq TO invite;


--
-- Name: t_operationmarquage_omq; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE t_operationmarquage_omq FROM PUBLIC;
REVOKE ALL ON TABLE t_operationmarquage_omq FROM postgres;
GRANT ALL ON TABLE t_operationmarquage_omq TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE t_operationmarquage_omq TO bresle;
GRANT SELECT ON TABLE t_operationmarquage_omq TO invite;


--
-- Name: t_ouvrage_ouv; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE t_ouvrage_ouv FROM PUBLIC;
REVOKE ALL ON TABLE t_ouvrage_ouv FROM postgres;
GRANT ALL ON TABLE t_ouvrage_ouv TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE t_ouvrage_ouv TO bresle;
GRANT SELECT ON TABLE t_ouvrage_ouv TO invite;


--
-- Name: t_ouvrage_ouv_ouv_identifiant_seq; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON SEQUENCE t_ouvrage_ouv_ouv_identifiant_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE t_ouvrage_ouv_ouv_identifiant_seq FROM postgres;
GRANT ALL ON SEQUENCE t_ouvrage_ouv_ouv_identifiant_seq TO postgres;
GRANT SELECT,UPDATE ON SEQUENCE t_ouvrage_ouv_ouv_identifiant_seq TO bresle;
GRANT SELECT,UPDATE ON SEQUENCE t_ouvrage_ouv_ouv_identifiant_seq TO invite;


--
-- Name: t_periodefonctdispositif_per; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE t_periodefonctdispositif_per FROM PUBLIC;
REVOKE ALL ON TABLE t_periodefonctdispositif_per FROM postgres;
GRANT ALL ON TABLE t_periodefonctdispositif_per TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE t_periodefonctdispositif_per TO bresle;
GRANT SELECT ON TABLE t_periodefonctdispositif_per TO invite;


--
-- Name: t_station_sta; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE t_station_sta FROM PUBLIC;
REVOKE ALL ON TABLE t_station_sta FROM postgres;
GRANT ALL ON TABLE t_station_sta TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE t_station_sta TO bresle;
GRANT SELECT ON TABLE t_station_sta TO invite;


--
-- Name: tg_dispositif_dis; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE tg_dispositif_dis FROM PUBLIC;
REVOKE ALL ON TABLE tg_dispositif_dis FROM postgres;
GRANT ALL ON TABLE tg_dispositif_dis TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE tg_dispositif_dis TO bresle;
GRANT SELECT ON TABLE tg_dispositif_dis TO invite;


--
-- Name: tg_dispositif_dis_dis_identifiant_seq; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON SEQUENCE tg_dispositif_dis_dis_identifiant_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE tg_dispositif_dis_dis_identifiant_seq FROM postgres;
GRANT ALL ON SEQUENCE tg_dispositif_dis_dis_identifiant_seq TO postgres;
GRANT SELECT,UPDATE ON SEQUENCE tg_dispositif_dis_dis_identifiant_seq TO bresle;
GRANT SELECT,UPDATE ON SEQUENCE tg_dispositif_dis_dis_identifiant_seq TO invite;


--
-- Name: tj_actionmarquage_act; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE tj_actionmarquage_act FROM PUBLIC;
REVOKE ALL ON TABLE tj_actionmarquage_act FROM postgres;
GRANT ALL ON TABLE tj_actionmarquage_act TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE tj_actionmarquage_act TO bresle;
GRANT SELECT ON TABLE tj_actionmarquage_act TO invite;


--
-- Name: tj_caracteristiquelot_car; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE tj_caracteristiquelot_car FROM PUBLIC;
REVOKE ALL ON TABLE tj_caracteristiquelot_car FROM postgres;
GRANT ALL ON TABLE tj_caracteristiquelot_car TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE tj_caracteristiquelot_car TO bresle;
GRANT SELECT ON TABLE tj_caracteristiquelot_car TO invite;


--
-- Name: tj_coefficientconversion_coe; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE tj_coefficientconversion_coe FROM PUBLIC;
REVOKE ALL ON TABLE tj_coefficientconversion_coe FROM postgres;
GRANT ALL ON TABLE tj_coefficientconversion_coe TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE tj_coefficientconversion_coe TO bresle;
GRANT SELECT ON TABLE tj_coefficientconversion_coe TO invite;


--
-- Name: tj_conditionenvironnementale_env; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE tj_conditionenvironnementale_env FROM PUBLIC;
REVOKE ALL ON TABLE tj_conditionenvironnementale_env FROM postgres;
GRANT ALL ON TABLE tj_conditionenvironnementale_env TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE tj_conditionenvironnementale_env TO bresle;
GRANT SELECT ON TABLE tj_conditionenvironnementale_env TO invite;


--
-- Name: tj_dfestdestinea_dtx; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE tj_dfestdestinea_dtx FROM PUBLIC;
REVOKE ALL ON TABLE tj_dfestdestinea_dtx FROM postgres;
GRANT ALL ON TABLE tj_dfestdestinea_dtx TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE tj_dfestdestinea_dtx TO bresle;
GRANT SELECT ON TABLE tj_dfestdestinea_dtx TO invite;


--
-- Name: tj_dfesttype_dft; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE tj_dfesttype_dft FROM PUBLIC;
REVOKE ALL ON TABLE tj_dfesttype_dft FROM postgres;
GRANT ALL ON TABLE tj_dfesttype_dft TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE tj_dfesttype_dft TO bresle;
GRANT SELECT ON TABLE tj_dfesttype_dft TO invite;


--
-- Name: tj_pathologieconstatee_pco; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE tj_pathologieconstatee_pco FROM PUBLIC;
REVOKE ALL ON TABLE tj_pathologieconstatee_pco FROM postgres;
GRANT ALL ON TABLE tj_pathologieconstatee_pco TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE tj_pathologieconstatee_pco TO bresle;
GRANT SELECT ON TABLE tj_pathologieconstatee_pco TO invite;


--
-- Name: tj_prelevementlot_prl; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE tj_prelevementlot_prl FROM PUBLIC;
REVOKE ALL ON TABLE tj_prelevementlot_prl FROM postgres;
GRANT ALL ON TABLE tj_prelevementlot_prl TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE tj_prelevementlot_prl TO bresle;
GRANT SELECT ON TABLE tj_prelevementlot_prl TO invite;


--
-- Name: tj_stationmesure_stm; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE tj_stationmesure_stm FROM PUBLIC;
REVOKE ALL ON TABLE tj_stationmesure_stm FROM postgres;
GRANT ALL ON TABLE tj_stationmesure_stm TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE tj_stationmesure_stm TO bresle;
GRANT SELECT ON TABLE tj_stationmesure_stm TO invite;


--
-- Name: tj_stationmesure_stm_stm_identifiant_seq; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON SEQUENCE tj_stationmesure_stm_stm_identifiant_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE tj_stationmesure_stm_stm_identifiant_seq FROM postgres;
GRANT ALL ON SEQUENCE tj_stationmesure_stm_stm_identifiant_seq TO postgres;
GRANT SELECT,UPDATE ON SEQUENCE tj_stationmesure_stm_stm_identifiant_seq TO bresle;
GRANT SELECT,UPDATE ON SEQUENCE tj_stationmesure_stm_stm_identifiant_seq TO invite;


--
-- Name: tj_tauxechappement_txe; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE tj_tauxechappement_txe FROM PUBLIC;
REVOKE ALL ON TABLE tj_tauxechappement_txe FROM postgres;
GRANT ALL ON TABLE tj_tauxechappement_txe TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE tj_tauxechappement_txe TO bresle;
GRANT SELECT ON TABLE tj_tauxechappement_txe TO invite;


--
-- Name: ts_taillevideo_tav; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE ts_taillevideo_tav FROM PUBLIC;
REVOKE ALL ON TABLE ts_taillevideo_tav FROM postgres;
GRANT ALL ON TABLE ts_taillevideo_tav TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ts_taillevideo_tav TO bresle;
GRANT SELECT ON TABLE ts_taillevideo_tav TO invite;


--
-- Name: ts_taxonvideo_txv; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE ts_taxonvideo_txv FROM PUBLIC;
REVOKE ALL ON TABLE ts_taxonvideo_txv FROM postgres;
GRANT ALL ON TABLE ts_taxonvideo_txv TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ts_taxonvideo_txv TO bresle;
GRANT SELECT ON TABLE ts_taxonvideo_txv TO invite;


--
-- Name: v_taxon_tax; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE v_taxon_tax FROM PUBLIC;
REVOKE ALL ON TABLE v_taxon_tax FROM postgres;
GRANT ALL ON TABLE v_taxon_tax TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE v_taxon_tax TO bresle;
GRANT SELECT ON TABLE v_taxon_tax TO invite;


--
-- Name: vue_lot_ope_car; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE vue_lot_ope_car FROM PUBLIC;
REVOKE ALL ON TABLE vue_lot_ope_car FROM postgres;
GRANT ALL ON TABLE vue_lot_ope_car TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE vue_lot_ope_car TO bresle;
GRANT SELECT ON TABLE vue_lot_ope_car TO invite;


--
-- Name: vue_lot_ope_car_qan; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE vue_lot_ope_car_qan FROM PUBLIC;
REVOKE ALL ON TABLE vue_lot_ope_car_qan FROM postgres;
GRANT ALL ON TABLE vue_lot_ope_car_qan TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE vue_lot_ope_car_qan TO bresle;
GRANT SELECT ON TABLE vue_lot_ope_car_qan TO invite;


--
-- Name: vue_ope_lot_ech_parqual; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE vue_ope_lot_ech_parqual FROM PUBLIC;
REVOKE ALL ON TABLE vue_ope_lot_ech_parqual FROM postgres;
GRANT ALL ON TABLE vue_ope_lot_ech_parqual TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE vue_ope_lot_ech_parqual TO bresle;
GRANT SELECT ON TABLE vue_ope_lot_ech_parqual TO invite;


--
-- Name: vue_ope_lot_ech_parquan; Type: ACL; Schema: bresle; Owner: postgres
--

REVOKE ALL ON TABLE vue_ope_lot_ech_parquan FROM PUBLIC;
REVOKE ALL ON TABLE vue_ope_lot_ech_parquan FROM postgres;
GRANT ALL ON TABLE vue_ope_lot_ech_parquan TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE vue_ope_lot_ech_parquan TO bresle;
GRANT SELECT ON TABLE vue_ope_lot_ech_parquan TO invite;


--
-- PostgreSQL database dump complete
--

