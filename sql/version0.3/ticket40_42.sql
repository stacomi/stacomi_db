-- Database update script 27/04/2010
-- Author Cedric
-- the objective of this script is to update database v0.3 to avoid bug
-- this script corresponds to tickets #40 and #42


-- ******************************
-- UPDATING SEQUENCES
-- *****************************

-- Only the owner of a sequence has the right to change and update the restart value
-- via alter sequence hence the crash described in ticket #45
-- below I'm changing ownership for all sequences
-- it seems that using a restoration script puts back the ownerships to postgres and not the schema owner

-- the relation present in table ref.ts_sequence_seq which points to ref has no use to be
-- managed along the other sequences
DELETE FROM ref.ts_sequence_seq where seq_sequence= 'tr_valeurparametrequalitatif_val_val_identifiant_seq';
/*
Liste of users in the current version
"BGM"
"BRESLE"
"IAV"
"INRA"
"invite"
"LOGRAMI"
"MIGADO"
"MIGRADOUR"
"MRM"
"SAUMONRHIN"
"SEINORMIGR"
*/

ALTER TABLE iav.t_operation_ope OWNER TO iav;
ALTER TABLE iav.t_lot_lot OWNER TO iav;
ALTER TABLE iav.t_bilanmigrationjournalier_bjo OWNER TO iav;
ALTER TABLE iav.t_bilanmigrationmensuel_bme OWNER TO iav;
ALTER TABLE iav.t_ouvrage_ouv OWNER TO iav;
ALTER TABLE iav.tg_dispositif_dis OWNER TO iav;
ALTER TABLE iav.tj_stationmesure_stm OWNER TO iav;

ALTER TABLE bgm.t_operation_ope OWNER TO bgm;
ALTER TABLE bgm.t_lot_lot OWNER TO bgm;
ALTER TABLE bgm.t_bilanmigrationjournalier_bjo OWNER TO bgm;
ALTER TABLE bgm.t_bilanmigrationmensuel_bme OWNER TO bgm;
ALTER TABLE bgm.t_ouvrage_ouv OWNER TO bgm;
ALTER TABLE bgm.tg_dispositif_dis OWNER TO bgm;
ALTER TABLE bgm.tj_stationmesure_stm OWNER TO bgm;

ALTER TABLE inra.t_operation_ope OWNER TO inra;
ALTER TABLE inra.t_lot_lot OWNER TO inra;
ALTER TABLE inra.t_bilanmigrationjournalier_bjo OWNER TO inra;
ALTER TABLE inra.t_bilanmigrationmensuel_bme OWNER TO inra;
ALTER TABLE inra.t_ouvrage_ouv OWNER TO inra;
ALTER TABLE inra.tg_dispositif_dis OWNER TO inra;
ALTER TABLE inra.tj_stationmesure_stm OWNER TO inra;

ALTER TABLE migado.t_operation_ope OWNER TO migado;
ALTER TABLE migado.t_lot_lot OWNER TO migado;
ALTER TABLE migado.t_bilanmigrationjournalier_bjo OWNER TO migado;
ALTER TABLE migado.t_bilanmigrationmensuel_bme OWNER TO migado;
ALTER TABLE migado.t_ouvrage_ouv OWNER TO migado;
ALTER TABLE migado.tg_dispositif_dis OWNER TO migado;
ALTER TABLE migado.tj_stationmesure_stm OWNER TO migado;

ALTER TABLE migradour.t_operation_ope OWNER TO migradour;
ALTER TABLE migradour.t_lot_lot OWNER TO migradour;
ALTER TABLE migradour.t_bilanmigrationjournalier_bjo OWNER TO migradour;
ALTER TABLE migradour.t_bilanmigrationmensuel_bme OWNER TO migradour;
ALTER TABLE migradour.t_ouvrage_ouv OWNER TO migradour;
ALTER TABLE migradour.tg_dispositif_dis OWNER TO migradour;
ALTER TABLE migradour.tj_stationmesure_stm OWNER TO migradour;

ALTER TABLE mrm.t_operation_ope OWNER TO mrm;
ALTER TABLE mrm.t_lot_lot OWNER TO mrm;
ALTER TABLE mrm.t_bilanmigrationjournalier_bjo OWNER TO mrm;
ALTER TABLE mrm.t_bilanmigrationmensuel_bme OWNER TO mrm;
ALTER TABLE mrm.t_ouvrage_ouv OWNER TO mrm;
ALTER TABLE mrm.tg_dispositif_dis OWNER TO mrm;
ALTER TABLE mrm.tj_stationmesure_stm OWNER TO mrm;

ALTER TABLE saumonrhin.t_operation_ope OWNER TO saumonrhin;
ALTER TABLE saumonrhin.t_lot_lot OWNER TO saumonrhin;
ALTER TABLE saumonrhin.t_bilanmigrationjournalier_bjo OWNER TO saumonrhin;
ALTER TABLE saumonrhin.t_bilanmigrationmensuel_bme OWNER TO saumonrhin;
ALTER TABLE saumonrhin.t_ouvrage_ouv OWNER TO saumonrhin;
ALTER TABLE saumonrhin.tg_dispositif_dis OWNER TO saumonrhin;
ALTER TABLE saumonrhin.tj_stationmesure_stm OWNER TO saumonrhin;

ALTER TABLE seinormigr.t_operation_ope OWNER TO seinormigr;
ALTER TABLE seinormigr.t_lot_lot OWNER TO seinormigr;
ALTER TABLE seinormigr.t_bilanmigrationjournalier_bjo OWNER TO seinormigr;
ALTER TABLE seinormigr.t_bilanmigrationmensuel_bme OWNER TO seinormigr;
ALTER TABLE seinormigr.t_ouvrage_ouv OWNER TO seinormigr;
ALTER TABLE seinormigr.tg_dispositif_dis OWNER TO seinormigr;
ALTER TABLE seinormigr.tj_stationmesure_stm OWNER TO seinormigr;

ALTER TABLE logrami.t_operation_ope OWNER TO logrami;
ALTER TABLE logrami.t_lot_lot OWNER TO logrami;
ALTER TABLE logrami.t_bilanmigrationjournalier_bjo OWNER TO logrami;
ALTER TABLE logrami.t_bilanmigrationmensuel_bme OWNER TO logrami;
ALTER TABLE logrami.t_ouvrage_ouv OWNER TO logrami;
ALTER TABLE logrami.tg_dispositif_dis OWNER TO logrami;
ALTER TABLE logrami.tj_stationmesure_stm OWNER TO logrami;

ALTER TABLE azti.t_operation_ope OWNER TO azti;
ALTER TABLE azti.t_lot_lot OWNER TO azti;
ALTER TABLE azti.t_bilanmigrationjournalier_bjo OWNER TO azti;
ALTER TABLE azti.t_bilanmigrationmensuel_bme OWNER TO azti;
ALTER TABLE azti.t_ouvrage_ouv OWNER TO azti;
ALTER TABLE azti.tg_dispositif_dis OWNER TO azti;
ALTER TABLE azti.tj_stationmesure_stm OWNER TO azti;

-- ticket 42 suite
GRANT SELECT ON ref.tr_typedf_tdf TO logrami;
GRANT SELECT ON ref.tr_typedf_tdf TO inra;
GRANT SELECT ON ref.tr_typedf_tdf TO bgm;
GRANT SELECT ON ref.tr_typedf_tdf TO migradour;
GRANT SELECT ON ref.tr_typedf_tdf TO bgm;
GRANT SELECT ON ref.tr_typedf_tdf TO migado;
GRANT SELECT ON ref.tr_typedf_tdf TO mrm;
GRANT SELECT ON ref.tr_typedf_tdf TO saumonrhin;
GRANT SELECT ON ref.tr_typedf_tdf TO azti;

