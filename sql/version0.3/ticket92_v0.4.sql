﻿

select max(msr_id) from ref.ts_messager_msr; --343 ok on en est à 342 avec la version 0.5
insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) values (343,'funtraitement_poids',3,'function',TRUE,'warning for incomplete coefficient input');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (343,'Attention vous n''avez probablement pas rentre tous les coefficients dans la base, vérifier car les poids depuis effectif ou effectifs depuis poids seront faux','French');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (343,'Attention there are probably missing coefficients in the database, verify or the conversion from number to weights and weights to number might be wrong','English');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (343,'La atención es probable que haya perdido los coeficientes de la base de datos, verificar o la conversión de número a los pesos y pesos con el número podría estar equivocado','Spanish');