﻿
GRANT ALL ON SCHEMA logrami TO logrami;
GRANT SELECT,INSERT,UPDATE ON logrami.t_station_sta TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.t_bilanmigrationjournalier_bjo TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.t_bilanmigrationmensuel_bme TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.tj_caracteristiquelot_car TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.tj_conditionenvironnementale_env TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.tj_stationmesure_stm TO logrami;
GRANT SELECT ON ref.tr_valeurparametrequalitatif_val TO logrami;
GRANT SELECT ON ref.tr_parametrequalitatif_qal TO logrami;
GRANT SELECT ON ref.tr_parametrequantitatif_qan TO logrami;
GRANT SELECT ON ref.tg_parametre_par TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.tj_dfesttype_dft TO logrami;
GRANT SELECT ON ref.tr_typedf_tdf TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.tj_dfestdestinea_dtx TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.tj_tauxechappement_txe TO logrami;
GRANT SELECT ON ref.tr_niveauechappement_ech TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.tj_coefficientconversion_coe TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.tj_pathologieconstatee_pco TO logrami;
GRANT SELECT ON ref.tr_pathologie_pat TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.tj_actionmarquage_act TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.t_marque_mqe TO logrami;
GRANT SELECT ON ref.tr_localisationanatomique_loc TO logrami;
GRANT SELECT ON ref.tr_naturemarque_nmq TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.t_operationmarquage_omq TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.t_lot_lot TO logrami;
GRANT SELECT ON ref.tr_devenirlot_dev TO logrami;
GRANT SELECT ON ref.tr_typequantitelot_qte TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.ts_taxonvideo_txv TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.ts_taillevideo_tav TO logrami;
GRANT SELECT ON ref.tr_stadedeveloppement_std TO logrami;
GRANT SELECT ON ref.tr_taxon_tax TO logrami;
GRANT SELECT ON ref.tr_niveautaxonomique_ntx TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.t_operation_ope TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.t_periodefonctdispositif_per TO logrami;
GRANT SELECT ON ref.tr_typearretdisp_tar TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.t_dispositifcomptage_dic TO logrami;
GRANT SELECT ON ref.tr_typedc_tdc TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.t_dispositiffranchissement_dif TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.tg_dispositif_dis TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.t_ouvrage_ouv TO logrami;
GRANT SELECT ON ref.tr_natureouvrage_nov TO logrami;
GRANT SELECT ON ref.tr_prelevement_pre TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON ref.ts_sequence_seq TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.tj_stationmesure_stm TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.tj_prelevementlot_prl TO logrami;
GRANT SELECT,INSERT,UPDATE,DELETE ON logrami.t_marque_mqe TO logrami;

GRANT SELECT, UPDATE ON logrami.tg_dispositif_dis_dis_identifiant_seq TO logrami;
GRANT SELECT, UPDATE ON logrami.t_lot_lot_lot_identifiant_seq TO logrami;
GRANT SELECT, UPDATE ON logrami.t_operation_ope_ope_identifiant_seq TO logrami;
GRANT SELECT, UPDATE ON logrami.t_ouvrage_ouv_ouv_identifiant_seq TO logrami;
GRANT SELECT, UPDATE ON logrami.tj_stationmesure_stm_stm_identifiant_seq TO logrami;
GRANT SELECT, UPDATE ON logrami.t_bilanmigrationmensuel_bme_bme_identifiant_seq TO logrami;
GRANT SELECT, UPDATE ON logrami.t_bilanmigrationjournalier_bjo_bjo_identifiant_seq TO logrami;

GRANT SELECT ON ref.ts_sequence_seq TO logrami;
GRANT SELECT ON TABLE ref.ts_sequence_seq TO invite;

-- vues
GRANT SELECT, UPDATE, INSERT ON TABLE logrami.v_taxon_tax TO logrami;
GRANT SELECT, UPDATE, INSERT ON TABLE logrami.vue_lot_ope_car TO logrami;
GRANT SELECT, UPDATE, INSERT ON TABLE logrami.vue_lot_ope_car_qan TO logrami;
GRANT SELECT, UPDATE, INSERT ON TABLE logrami.vue_ope_lot_ech_parqual TO logrami;
GRANT SELECT, UPDATE, INSERT ON TABLE logrami.vue_ope_lot_ech_parquan TO logrami;



GRANT SELECT ON logrami.t_station_sta TO invite;
GRANT SELECT ON logrami.t_bilanmigrationjournalier_bjo TO invite;
GRANT SELECT ON logrami.t_bilanmigrationmensuel_bme TO invite;
GRANT SELECT ON logrami.tj_caracteristiquelot_car TO invite;
GRANT SELECT ON logrami.tj_conditionenvironnementale_env TO invite;
GRANT SELECT ON logrami.tj_stationmesure_stm TO invite;
GRANT SELECT ON ref.tr_valeurparametrequalitatif_val TO invite;
GRANT SELECT ON ref.tr_parametrequalitatif_qal TO invite;
GRANT SELECT ON ref.tr_parametrequantitatif_qan TO invite;
GRANT SELECT ON ref.tg_parametre_par TO invite;
GRANT SELECT ON logrami.tj_dfesttype_dft TO invite;
GRANT SELECT ON ref.tr_typedf_tdf TO invite;
GRANT SELECT ON logrami.tj_dfestdestinea_dtx TO invite;
GRANT SELECT ON logrami.tj_tauxechappement_txe TO invite;
GRANT SELECT ON ref.tr_niveauechappement_ech TO invite;
GRANT SELECT ON logrami.tj_coefficientconversion_coe TO invite;
GRANT SELECT ON logrami.tj_pathologieconstatee_pco TO invite;
GRANT SELECT ON ref.tr_pathologie_pat TO invite;
GRANT SELECT ON logrami.tj_actionmarquage_act TO invite;
GRANT SELECT ON logrami.t_marque_mqe TO invite;
GRANT SELECT ON ref.tr_localisationanatomique_loc TO invite;
GRANT SELECT ON ref.tr_naturemarque_nmq TO invite;
GRANT SELECT ON logrami.t_operationmarquage_omq TO invite;
GRANT SELECT ON logrami.t_lot_lot TO invite;
GRANT SELECT ON ref.tr_devenirlot_dev TO invite;
GRANT SELECT ON ref.tr_typequantitelot_qte TO invite;
GRANT SELECT ON logrami.ts_taxonvideo_txv TO invite;
GRANT SELECT ON logrami.ts_taillevideo_tav TO invite;
GRANT SELECT ON ref.tr_stadedeveloppement_std TO invite;
GRANT SELECT ON ref.tr_taxon_tax TO invite;
GRANT SELECT ON ref.tr_niveautaxonomique_ntx TO invite;
GRANT SELECT ON logrami.t_operation_ope TO invite;
GRANT SELECT ON logrami.t_periodefonctdispositif_per TO invite;
GRANT SELECT ON ref.tr_typearretdisp_tar TO invite;
GRANT SELECT ON logrami.t_dispositifcomptage_dic TO invite;
GRANT SELECT ON ref.tr_typedc_tdc TO invite;
GRANT SELECT ON logrami.t_dispositiffranchissement_dif TO invite;
GRANT SELECT ON logrami.tg_dispositif_dis TO invite;
GRANT SELECT ON logrami.t_ouvrage_ouv TO invite;
GRANT SELECT ON ref.tr_natureouvrage_nov TO invite;
GRANT SELECT ON logrami.t_station_sta TO invite;
GRANT SELECT ON ref.tr_prelevement_pre TO invite;
GRANT SELECT ON ref.ts_organisme_org TO invite;
GRANT SELECT ON ref.ts_sequence_seq TO invite;
GRANT SELECT ON logrami.tj_stationmesure_stm TO invite;
GRANT SELECT ON logrami.tj_prelevementlot_prl TO invite;
GRANT SELECT ON logrami.t_marque_mqe TO invite;
--vues
GRANT SELECT ON logrami.v_taxon_tax TO invite;
GRANT SELECT ON logrami.vue_lot_ope_car TO invite;
GRANT SELECT ON logrami.vue_lot_ope_car_qan TO invite;
GRANT SELECT ON logrami.vue_ope_lot_ech_parqual TO invite;
GRANT SELECT ON logrami.vue_ope_lot_ech_parquan TO invite;
-- sequences
GRANT SELECT, UPDATE ON logrami.tg_dispositif_dis_dis_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON logrami.t_lot_lot_lot_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON logrami.t_operation_ope_ope_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON logrami.t_ouvrage_ouv_ouv_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON logrami.tj_stationmesure_stm_stm_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON logrami.t_bilanmigrationmensuel_bme_bme_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON logrami.t_bilanmigrationjournalier_bjo_bjo_identifiant_seq TO invite;

--ticket 40

--c_fk_dic_dis_identifiant
Alter table logrami.t_dispositifcomptage_dic ADD CONSTRAINT c_fk_dic_dis_identifiant 
	FOREIGN KEY (dic_dis_identifiant,dic_org_code) 
	REFERENCES logrami.tg_dispositif_dis (dis_identifiant,dis_org_code);
-- la contrainte de table est construite sur le code alors qu'elle devrait l'être sur l'identifiant
-- je la supprime	
Alter table logrami.t_dispositiffranchissement_dif DROP CONSTRAINT c_pk_dif ;
-- je la recrée avec les bonnes valeurs
Alter table logrami.t_dispositiffranchissement_dif ADD CONSTRAINT c_pk_dif PRIMARY KEY (dif_dis_identifiant, dif_org_code);
-- pareil pour les dispositifs de comptage
Alter table logrami.t_dispositifcomptage_dic DROP CONSTRAINT c_pk_dic ;
-- 
Alter table logrami.t_dispositifcomptage_dic ADD CONSTRAINT c_pk_dic PRIMARY KEY (dic_dis_identifiant, dic_org_code);

--c_fk_dic_dif_identifiant     		
Alter table logrami.t_dispositifcomptage_dic ADD CONSTRAINT c_fk_dic_dif_identifiant 
	FOREIGN KEY (dic_dif_identifiant,dic_org_code) 
	REFERENCES logrami.t_dispositiffranchissement_dif (dif_dis_identifiant,dif_org_code);
--c_fk_dif_ouv_identifiant 
Alter table logrami.t_dispositiffranchissement_dif ADD CONSTRAINT c_fk_dif_ouv_identifiant 
	FOREIGN KEY (dif_ouv_identifiant,dif_org_code) 
	REFERENCES logrami.t_ouvrage_ouv (ouv_identifiant,ouv_org_code);
-- c_fk_dif_dis_identifiant	
Alter table logrami.t_dispositiffranchissement_dif ADD CONSTRAINT c_fk_dif_dis_identifiant	
	FOREIGN KEY (dif_dis_identifiant,dif_org_code) 
	REFERENCES logrami.tg_dispositif_dis (dis_identifiant,dis_org_code);
-- c_fk_dtx_dif_identifiant
Alter table logrami.tj_dfestdestinea_dtx ADD CONSTRAINT c_fk_dtx_dif_identifiant	
	FOREIGN KEY (dtx_dif_identifiant,dtx_org_code) 
	REFERENCES logrami.tg_dispositif_dis (dis_identifiant,dis_org_code);
--c_fk_dft_df_identifiant
Alter table logrami.tj_dfesttype_dft ADD CONSTRAINT c_fk_dft_df_identifiant	
	FOREIGN KEY (dft_df_identifiant,dft_org_code) 
	REFERENCES logrami.tg_dispositif_dis (dis_identifiant,dis_org_code);
--c_fk_prl_lot_identifiant
-- pour la table des prélèvements il manque les organismes il faut faire les modifs qui suivent
Alter table logrami.tj_prelevementlot_prl add column prl_org_code character varying(30) NOT NULL;
Alter table logrami.tj_prelevementlot_prl DROP CONSTRAINT c_pk_prl;
Alter table logrami.tj_prelevementlot_prl ADD CONSTRAINT c_pk_prl PRIMARY KEY (prl_pre_typeprelevement, prl_lot_identifiant, prl_code,prl_org_code);
Alter table logrami.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_lot_identifiant	
	FOREIGN KEY (prl_lot_identifiant,prl_org_code) 
	REFERENCES logrami.t_lot_lot (lot_identifiant,lot_org_code);
--Le nom ci dessous c'est pas terrible cht de c_fk_prl_pre_nom a c_fk_prl_pre_typeprelevement;
ALTER TABLE logrami.tj_prelevementlot_prl DROP CONSTRAINT c_fk_prl_pre_nom;
ALTER TABLE logrami.tj_prelevementlot_prl ADD CONSTRAINT 
 c_fk_prl_typeprelevement FOREIGN KEY (prl_pre_typeprelevement)
      REFERENCES ref.tr_prelevement_pre (pre_typeprelevement) MATCH SIMPLE;

-- c_fk_tav_dic_identifiant
Alter table logrami.ts_taillevideo_tav ADD CONSTRAINT c_fk_tav_dic_identifiant	
	FOREIGN KEY (tav_dic_identifiant,tav_org_code) 
	REFERENCES logrami.t_dispositifcomptage_dic (dic_dis_identifiant,dic_org_code);
     
-- ticket 40-42
ALTER TABLE logrami.t_operation_ope OWNER TO logrami;
ALTER TABLE logrami.t_lot_lot OWNER TO logrami;
ALTER TABLE logrami.t_bilanmigrationjournalier_bjo OWNER TO logrami;
ALTER TABLE logrami.t_bilanmigrationmensuel_bme OWNER TO logrami;
ALTER TABLE logrami.t_ouvrage_ouv OWNER TO logrami;
ALTER TABLE logrami.tg_dispositif_dis OWNER TO logrami;
ALTER TABLE logrami.tj_stationmesure_stm OWNER TO logrami;
GRANT SELECT ON ref.tr_typedf_tdf TO logrami;

-- ticket59

CREATE TABLE logrami.ts_maintenance_main(
main_identifiant serial PRIMARY KEY,
main_ticket integer,
main_description text);
COMMENT ON  TABLE logrami.ts_maintenance_main IS 'Table de suivi des operations de maintenance de la base';

INSERT INTO logrami.ts_maintenance_main( main_ticket,main_description ) VALUES (59,'creation de la table de maintenance');
INSERT INTO logrami.ts_maintenance_main( main_ticket,main_description ) VALUES (40,'ajout des clé étrangères manquantes');
INSERT INTO logrami.ts_maintenance_main( main_ticket,main_description ) VALUES (42,'modification des propriétaires sur les tables à séquence et grant select sur ref.tr_typedf_tdf oublié');
INSERT INTO logrami.ts_maintenance_main( main_ticket,main_description ) VALUES (67,'org code rajouté dans les tables t_operationmarquage_omq, tj_coefficientconversion_coe,tj_prelevementlot_prl');

--Ticket 67 script update one organism

alter table logrami.tj_prelevementlot_prl  drop CONSTRAINT c_pk_prl;
alter table logrami.tj_prelevementlot_prl add prl_org_code character varying(30) NOT NULL;
alter table logrami.tj_prelevementlot_prl add constraint c_pk_prl PRIMARY KEY (prl_pre_typeprelevement, prl_lot_identifiant, prl_code,prl_org_code);
ALTER TABLE logrami.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_org_code FOREIGN KEY (prl_org_code) REFERENCES ref.ts_organisme_org(org_code);

-- si des données sont déjà présentes
/*
alter table logrami.tj_prelevementlot_prl  drop CONSTRAINT c_pk_prl;
alter table logrami.tj_prelevementlot_prl add prl_org_code character varying(30);
update logrami.tj_prelevementlot_prl set prl_org_code='logrami';
alter table logrami.tj_prelevementlot_prl ADD constraint c_nn_prl_org_code check (prl_org_code IS NOT NULL);
alter table logrami.tj_prelevementlot_prl add constraint c_pk_prl PRIMARY KEY (prl_pre_typeprelevement, prl_lot_identifiant, prl_code,prl_org_code);
ALTER TABLE logrami.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_org_code FOREIGN KEY (prl_org_code) REFERENCES ref.ts_organisme_org(org_code);
*/
alter table logrami.tj_coefficientconversion_coe  drop CONSTRAINT c_pk_coe;
alter table logrami.tj_coefficientconversion_coe add coe_org_code character varying(30) NOT NULL;
alter table logrami.tj_coefficientconversion_coe add constraint c_pk_coe PRIMARY KEY (coe_tax_code, coe_std_code, coe_qte_code, coe_date_debut, coe_date_fin,coe_org_code);
ALTER TABLE logrami.tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code) REFERENCES ref.ts_organisme_org(org_code);


-- si des données sont déjà présentes
/*
alter table logrami.tj_coefficientconversion_coe  drop CONSTRAINT c_pk_coe;
alter table logrami.tj_coefficientconversion_coe add coe_org_code character varying(30);
update logrami.tj_coefficientconversion_coe set coe_org_code='logrami';
alter table logrami.tj_coefficientconversion_coe ADD constraint c_nn_coe_org_code check (coe_org_code IS NOT NULL);
alter table logrami.tj_coefficientconversion_coe add constraint c_pk_coe PRIMARY KEY (coe_tax_code, coe_std_code, coe_qte_code, coe_date_debut, coe_date_fin,coe_org_code);
ALTER TABLE logrami.tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code) REFERENCES ref.ts_organisme_org(org_code);
*/

alter table logrami.t_operationmarquage_omq  drop CONSTRAINT c_pk_omg CASCADE;
alter table logrami.t_operationmarquage_omq add omq_org_code character varying(30) NOT NULL;
alter table logrami.t_operationmarquage_omq ADD CONSTRAINT c_pk_omq PRIMARY KEY (omq_reference,omq_org_code);
ALTER TABLE logrami.t_operationmarquage_omq ADD CONSTRAINT c_fk_omq_org_code FOREIGN KEY (omq_org_code) REFERENCES ref.ts_organisme_org(org_code);
ALTER TABLE logrami.t_marque_mqe ADD CONSTRAINT c_fk_mqe_omq_reference FOREIGN KEY (mqe_omq_reference,mqe_org_code)
      REFERENCES logrami.t_operationmarquage_omq (omq_reference,omq_org_code) ;

-- mise à jour de la table de maintenance

INSERT INTO logrami.ts_maintenance_main( main_ticket,main_description) VALUES (72,'creation d''une ref.ts_messagerlang_mrl  pour l''internationalisation');
INSERT INTO logrami.ts_maintenance_main( main_ticket,main_description) VALUES (74,'Insertion de deux nouveaux paramètres');
