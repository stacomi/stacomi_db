﻿-- script créé le 22/12/2012

--select * from ref.ts_messagerlang_mrl where mrl_text like '%EPACS%'

-- ci dessous pour aide recherche d'un message en particulier
/*
select * from ref.ts_messager_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id
where msr_element||'.'||msr_number='interface_Bilan_lot.7';

select * from ref.ts_messager_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id
where msr_element||'.'||msr_number='Bilan_lot.4';

select * from ref.ts_messager_msr join ref.ts_messagerlang_mrl on mrl_msr_id=msr_id
where msr_element='interface_Bilan_lot' order by mrl_lang, cast(msr_number as integer)
*/
-- Bilan migration interannuel 23/12/12
-- correction
--EPACS je ne sais plus ce que ça veut dire....
update ref.ts_messagerlang_mrl set mrl_text='", Effectifs cumulés"' where mrl_id=117;
update ref.ts_messagerlang_mrl set mrl_text='", Cumulated numbers"' where mrl_id=449;
update ref.ts_messagerlang_mrl set mrl_text='", Acumulado real"' where mrl_id=781;
-- Bilan_lot 24/12/12
-- changing name from ggplot2 interface to dotplot
update ref.ts_messagerlang_mrl set mrl_text='"dotplot"' where mrl_id=227;
update ref.ts_messagerlang_mrl set mrl_text='"dotplot"' where mrl_id=559;
update ref.ts_messagerlang_mrl set mrl_text='"dotplot"' where mrl_id=891;

-- Bilan_lot 24/12/12
-- modification de l'objet graphique de p vers g
update ref.ts_messagerlang_mrl set mrl_text=
	'"Pour recuperer l''objet graphique, tapper : g, voir http://trac.eptb-vilaine.fr:8066/tracstacomi/wiki/Recette%20BilanLot pour de l''aide"'
	where mrl_id=16;
update ref.ts_messagerlang_mrl set mrl_text=
	'"To obtain the graphical object, type : g, see http://trac.eptb-vilaine.fr:8066/tracstacomi/wiki/Recette%20BilanLot for help"'
	where mrl_id=348;
update ref.ts_messagerlang_mrl set mrl_text=		
'"Para recuperar el objeto gráfico, escribir: g, see http://trac.eptb-vilaine.fr:8066/tracstacomi/wiki/Recette%20BilanLot for help"'
	where mrl_id=680;
-- Bilan_lot 24/12/12
-- adding new plot
insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) 
	select max(msr_id)+1, 'interface_Bilan_lot',10,'interface',FALSE,'tooltip and name' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"boxplot"','French' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"boxplot"','English' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"boxplot"','Spanish' from ref.ts_messager_msr;
-- Bilan_lot 24/12/12
-- adding new plot
insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) 
	select max(msr_id)+1, 'interface_Bilan_lot',11,'interface',FALSE,'tooltip and name' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"densité"','French' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"density"','English' from ref.ts_messager_msr;
insert into ref.ts_messagerlang_mrl (mrl_msr_id,mrl_text,mrl_lang) 
	select max(msr_id), '"density"','Spanish' from ref.ts_messager_msr;


-- select * from ref.ts_messager_msr where msr_element like '%Connexion%'
update ref.ts_messager_msr set msr_element='ConnectionODBC' where msr_element='ConnexionODBC'
-- OK select * from ref.ts_messagerlang_mrl

/*
AJOUT D'UNE TABLE DE MAINTENANCE DANS REF
*/
/*
DROP TABLE IF EXISTS ref.ts_maintenance_main;
CREATE TABLE ref.ts_maintenance_main
(
  main_identifiant serial NOT NULL,
  main_ticket integer,
  main_description text,
  CONSTRAINT ts_maintenance_main_pkey PRIMARY KEY (main_identifiant )
)
WITH (
  OIDS=FALSE
);
ALTER TABLE ref.ts_maintenance_main
  OWNER TO postgres;
COMMENT ON TABLE ref.ts_maintenance_main
  IS 'Table de suivi des operations de maintenance de la base';
insert into ref.ts_maintenance_main  select * from saumonrhin.ts_maintenance_main;
delete from ref.ts_maintenance_main where main_identifiant=4;
delete from ref.ts_maintenance_main where main_identifiant=7;
*/

INSERT INTO saumonrhin.ts_maintenance_main( main_ticket,main_description ) VALUES (104,'modification des messages dans ref.ts_messagerlang_mrl');
INSERT INTO ref.ts_maintenance_main( main_ticket,main_description ) VALUES (104,'modification des messages dans ref.ts_messagerlang_mrl');

