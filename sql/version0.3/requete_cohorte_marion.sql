﻿Select  s.sta_nom,
    CASE WHEN c.car_val_identifiant=61 THEN extract(year from op.ope_date_debut) + integer '1'
    ELSE extract(year from op.ope_date_debut)
    END,
    t.tax_nom_commun,
    l.lot_tax_code,
    st.std_libelle,
    sum(l.lot_effectif)
From t_station_sta s
left join t_ouvrage_ouv o on (s.sta_code=o.ouv_sta_code)
left join t_dispositiffranchissement_dif df on (df.dif_ouv_identifiant=o.ouv_identifiant)
left join t_dispositifcomptage_dic dc on (dc.dic_dif_identifiant=df.dif_dis_identifiant)
left join t_operation_ope op on (op.ope_dic_identifiant = dc.dic_dis_identifiant)
left join t_lot_lot l on (l.lot_ope_identifiant = op.ope_identifiant)
left join tj_caracteristiquelot_car c on (c.car_lot_identifiant=l.lot_identifiant and c.car_par_code='A126')
JOIN ref.tr_taxon_tax t ON (t.tax_code::text = l.lot_tax_code::text)
JOIN ref.tr_stadedeveloppement_std st on (st.std_code=l.lot_std_code)
where (st.std_libelle like 'Adulte' or st.std_libelle like 'AGG') and (l.lot_tax_code like '2220' or l.lot_tax_code like '2219') --or l.lot_tax_code like '2038' or l.lot_tax_code like '2224' or l.lot_tax_code like '2219' or l.lot_tax_code like '2055' or l.lot_tax_code like '2014') 
group by 1,2,3,4,5
order by 1,2,3
