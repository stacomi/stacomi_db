﻿/*
ticket_101_creation_produceur_finaceur.sql
 ticket 101 CREATION DES TABLES DE PRODUCTEUR ET FINANCEMENT RATTACHEES AU DISPOSITIF DE COMPTAGE.
--http://trac.eptb-vilaine.fr:8066/tracstacomi/ticket/101
auteurs Cédric Briand / Armelle Bask
Version provisoire, les tests d'imports restent à faire, la structure de la table financement est peut-être à revoir
date 7 mai 2012
*/

-- La table des producteurs

drop table if exists bgm.t_producteur_pro CASCADE;
create table bgm.t_producteur_pro(
	pro_identifiant serial, 
	pro_dic_dis_identifiant integer ,
	pro_typemaitrise character varying(40), 
	pro_nommaitrise character varying (35), 
	pro_datedebut date, 
	pro_datefin date, 
	pro_personneressource character varying(50), 
	pro_commentaire text, 
	pro_org_code character varying(30), 
	CONSTRAINT c_pk_pro primary key (pro_identifiant,pro_org_code),
	CONSTRAINT c_fk_pro_dic_dis_identifiant FOREIGN KEY (pro_dic_dis_identifiant,pro_org_code) REFERENCES bgm.t_dispositifcomptage_dic (dic_dis_identifiant,dic_org_code),
	CONSTRAINT c_ck_pro_typemaitrise CHECK (pro_typemaitrise='maitre d''ouvrage' or pro_typemaitrise='maitre d''oeuvre' or pro_typemaitrise='maitre d''ouvrage et maitre d''oeuvre'),
	CONSTRAINT c_ck_pro_finapresdebut CHECK (CASE WHEN pro_datefin is null then TRUE ELSE pro_datefin>pro_datedebut END),
	CONSTRAINT c_fk_pro_org_code FOREIGN KEY (pro_org_code) REFERENCES ref.ts_organisme_org(org_code));

Comment on table bgm.t_producteur_pro is 'table créée le 7 mai 2012 et rajoutée au schéma stacomi à la demande de BGM, essai de codage';
Comment on column bgm.t_producteur_pro.pro_identifiant IS 'identifiant unique du producteur de la donnée';
Comment on column bgm.t_producteur_pro.pro_dic_dis_identifiant IS 'identifiant du dispositif de comptage';
Comment on column bgm.t_producteur_pro.pro_typemaitrise IS 'type de maitrise (maitrise d''ouvrage, ou maitrise d''oeuvre ou maitrise d''ouvrage et maitrise d''oeuvre)';
Comment on column bgm.t_producteur_pro.pro_nommaitrise IS 'nom du maitre d''ouvrage ou du maitre d''oeuvre';
Comment on column bgm.t_producteur_pro.pro_datedebut IS 'date de début de la production de données par le maitre d''ouvrage ou le maitre d''oeuvre';
Comment on column bgm.t_producteur_pro.pro_datefin IS 'date de fin';
Comment on column bgm.t_producteur_pro.pro_personneressource IS 'nom de la personne ressource du producteur de données';
Comment on column bgm.t_producteur_pro.pro_commentaire IS 'commentaires sur la production de la donnée';
Comment on column bgm.t_producteur_pro.pro_org_code IS 'code de l''organisme de banquarisation de la donnée (nom du schéma)';
grant all on table bgm.t_producteur_pro to bgm

-- La table des financements, ça change beaucoup plus que les producteurs, d'où l'intérêt de le mettre dans une nouvelle table
drop table if exists bgm.t_financement_fin;
create table bgm.t_financement_fin(
fin_identifiant serial,
fin_pro_identifiant integer,
fin_financeur character varying(50),
fin_cadrefinancement character varying(50),
fin_datedebut date,
fin_datefin date,
fin_commentaire text,
fin_org_code character varying(30), 
CONSTRAINT c_pk_fin primary key (fin_identifiant,fin_org_code),
CONSTRAINT c_fk_fin_pro_identifiant FOREIGN KEY (fin_pro_identifiant,fin_org_code) REFERENCES bgm.t_producteur_pro(pro_identifiant,pro_org_code),
CONSTRAINT c_ck_fin_finapresdebut CHECK (CASE WHEN fin_datefin is null then TRUE ELSE fin_datefin>fin_datedebut END),
CONSTRAINT c_fk_fin_org_code FOREIGN KEY (fin_org_code) REFERENCES ref.ts_organisme_org(org_code),
CONSTRAINT c_uk_financeur UNIQUE (fin_financeur, fin_datedebut,fin_pro_identifiant,fin_org_code));
Comment on table bgm.t_financement_fin is 'table créée le 7 mai 2012 et rajoutée au schéma stacomi à la demande de BGM, essai de codage';
comment on column bgm.t_financement_fin.fin_identifiant is 'identifiant unique du financement';
comment on column bgm.t_financement_fin.fin_cadrefinancement is 'description du cadre du financement, exemple contrat de projet...';
comment on column bgm.t_financement_fin.fin_datedebut is 'date de début du financement';
comment on column bgm.t_financement_fin.fin_datefin is 'date de fin du financement (peut être nulle)';
comment on column bgm.t_financement_fin.fin_commentaire is 'Commentaire sur le financement';
comment on column bgm.t_financement_fin.fin_org_code is 'Code d''organisme de la table ref.ts_organisme_org';
grant all on table bgm.t_financement_fin to bgm
-- creation d'un trigger pour vérifier les dates de la table de financement

-- DROP FUNCTION bgm.fin_date();

CREATE OR REPLACE FUNCTION bgm.fin_date()
  RETURNS trigger AS
$BODY$ 
 	DECLARE pro_datedebut_ date  ;
 	DECLARE pro_datefin_ 	date  ;
 	BEGIN
 	 	-- Recuperation des date du producteur
 	 	SELECT pro_datedebut, pro_datefin INTO pro_datedebut_, pro_datefin_
 	 	FROM   bgm.t_producteur_pro
 	 	WHERE  pro_identifiant = NEW.fin_pro_identifiant;
  	        -- verification de la date de debut
 	 	IF ((NEW.fin_datedebut < pro_datedebut_) OR (NEW.fin_datedebut > pro_datefin_)) THEN
 	 	 	RAISE EXCEPTION 'Le debut du financement ne correspond pas a la periode de production de la donnee (table t_producteur_pro)'  ;
 	 	END IF  ;
 	 	-- verification de la date de fin
 	 	IF ((NEW.fin_datefin < pro_datedebut_) OR (NEW.fin_datefin > pro_datefin_)) THEN
			RAISE EXCEPTION 'La fin du financement ne correspond pas a la periode de production de la donnee (table t_producteur_pro)'  ;
 	 	END IF  ; 	 	
		RETURN NEW ;
 	END  ;
$BODY$
  LANGUAGE plpgsql ;
 
-- DROP TRIGGER trg_fin_date ON bgm.t_financement_fin;

CREATE TRIGGER trg_fin_date
  AFTER INSERT OR UPDATE
  ON bgm.t_financement_fin
  FOR EACH ROW
  EXECUTE PROCEDURE bgm.fin_date();

-- renseignement de l'exécution du ticket dans la base (à ne lancer que quand le script sera sûr)

INSERT INTO bgm.ts_maintenance_main( main_ticket,main_description) VALUES (101,'integration des tables producteur et financeur v0.5');  