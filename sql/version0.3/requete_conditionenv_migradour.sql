﻿SET search_path TO migradour, public;

-- Il te manque une contrainte, pas grave mais peut créer des pb plus tard

ALTER TABLE migradour.tj_conditionenvironnementale_env add CONSTRAINT c_fk_env_val_identifiant FOREIGN KEY (env_val_identifiant)
      REFERENCES ref.tr_valeurparametrequalitatif_val (val_identifiant) MATCH SIMPLE;
-- la même avec des valeurs texte
DROP VIEW migradour.env_chopolo;
CREATE OR REPLACE VIEW migradour.env_chopolo as (
SELECT * FROM crosstab('SELECT env_date_debut, env_stm_identifiant, val_libelle FROM migradour.tj_conditionenvironnementale_env
						join ref.tr_valeurparametrequalitatif_val on   (val_identifiant=env_val_identifiant) 
                                               WHERE env_stm_identifiant in (''20'',''21'',''22'',''23'',''24'',''25'',''61'')
                                                   ORDER BY env_date_debut, env_stm_identifiant',
                                               'SELECT distinct env_stm_identifiant FROM migradour.tj_conditionenvironnementale_env WHERE env_stm_identifiant in
                                                                               (''20'',''21'',''22'',''23'',''24'',''25'',''61'') ORDER BY env_stm_identifiant')
                                               AS ct(env_date_debut timestamp, 
                                               "Déversement" text, --20
                                               "Etat du cône" text, --21
                                               "Etat de la grille" text, --22
                                               "Météo" text, --23    
                                               "Niveau" text, --24
                                               "Débit" text, --25
                                               "Turbidité" text -- 61                                               
                                               )
                                               ) ;
COMMENT ON VIEW  migradour.env_chopolo is 'Données environnementales de la station de Chopolo';      
select * from  migradour.env_chopolo;

SELECT  s.sta_nom AS "Nom station",
                op.ope_date_debut AS "Date activation",
                op.ope_date_fin AS "Date relève",
                op.ope_commentaires AS "Commentaires",
                "Déversement",
                "Etat du cône",
                "Etat de la grille",
                "Météo",
                "Niveau",
                "Débit",
                "Turbidité"
FROM t_station_sta s
LEFT JOIN t_ouvrage_ouv o on (s.sta_code=o.ouv_sta_code)
LEFT JOIN t_dispositiffranchissement_dif df on (df.dif_ouv_identifiant=o.ouv_identifiant)
LEFT JOIN t_dispositifcomptage_dic dc on (dc.dic_dif_identifiant=df.dif_dis_identifiant)
LEFT JOIN t_operation_ope op on (op.ope_dic_identifiant=dc.dic_dis_identifiant)
LEFT JOIN env_chopolo cho on (cho.env_date_debut=op.ope_date_debut)
WHERE (s.sta_nom='Chopolo') --choix des stations
                and op.ope_date_fin>'2017-02-01' and op.ope_date_fin<'2018-02-01' --choix de la période
ORDER BY sta_nom, ope_date_debut; --ordre de sortie

select * from  migradour.env_chopolo;      
