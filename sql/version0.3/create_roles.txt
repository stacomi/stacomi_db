/*
SCRIPT DE CREATION DES ROLES SUR UN NOUVEAU SERVEUR
ici un serveur 192.168.1.1 pour l'exemple.
A adapter...
*/


psql -U postgres -h 192.168.1.1 -c "create user bgm with password 'bgm'"
psql -U postgres -h 192.168.1.1 -c "create user logrami with password 'logrami'"
psql -U postgres -h 192.168.1.1 -c "create user migado with password 'migado'"
psql -U postgres -h 192.168.1.1 -c "create user fd80 with password 'fd80'"
psql -U postgres -h 192.168.1.1 -c "create user charente with password 'charente'"
psql -U postgres -h 192.168.1.1 -c "create user mrm with password 'mrm'"
psql -U postgres -h 192.168.1.1 -c "create user bgm with password 'bgm'"
psql -U postgres -h 192.168.1.1 -c "create user saumonrhin with password 'saumonrhin'"
psql -U postgres -h 192.168.1.1 -c "create user migradour with password 'migradour'"
psql -U postgres -h 192.168.1.1 -c "create user iav with password 'iav'"
psql -U postgres -h 192.168.1.1 -c "create user nat with password 'nat'"
psql -U postgres -h 192.168.1.1 -c "create user pmp with password 'pmp'"
psql -U postgres -h 192.168.1.1 -c "create user invite with password 'invite'"
psql -U postgres -h 192.168.1.1 -c "create user inra with password 'inra'"
psql -U postgres -h 192.168.1.1 -c "create user smatah with password 'smatah'"
#psql -U postgres -h 192.168.1.1 -c "drop user stacomi_user"
psql -U postgres -h 192.168.1.1 -c "create group stacomi_user with password 'stacomi_user'"
psql -U postgres -h 192.168.1.1 -c "grant stacomi_user to inra;grant stacomi_user to  bgm;grant stacomi_user to  logrami;grant stacomi_user to  migado;grant stacomi_user to  migradour;grant stacomi_user to  fd80;grant stacomi_user to charente;grant stacomi_user to mrm;grant stacomi_user to saumonrhin;grant stacomi_user to  iav;grant stacomi_user to  nat;grant stacomi_user to  pmp;"
