-- droits_pmp.sql


GRANT ALL ON SCHEMA pmp TO pmp;
GRANT SELECT,INSERT,UPDATE ON pmp.t_station_sta TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.t_bilanmigrationjournalier_bjo TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.t_bilanmigrationmensuel_bme TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.tj_caracteristiquelot_car TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.tj_conditionenvironnementale_env TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.tj_stationmesure_stm TO pmp;
GRANT SELECT ON ref.tr_valeurparametrequalitatif_val TO pmp;
GRANT SELECT ON ref.tr_parametrequalitatif_qal TO pmp;
GRANT SELECT ON ref.tr_parametrequantitatif_qan TO pmp;
GRANT SELECT ON ref.tg_parametre_par TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.tj_dfesttype_dft TO pmp;
GRANT SELECT ON ref.tr_typedf_tdf TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.tj_dfestdestinea_dtx TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.tj_tauxechappement_txe TO pmp;
GRANT SELECT ON ref.tr_niveauechappement_ech TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.tj_coefficientconversion_coe TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.tj_pathologieconstatee_pco TO pmp;
GRANT SELECT ON ref.tr_pathologie_pat TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.tj_actionmarquage_act TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.t_marque_mqe TO pmp;
GRANT SELECT ON ref.tr_localisationanatomique_loc TO pmp;
GRANT SELECT ON ref.tr_naturemarque_nmq TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.t_operationmarquage_omq TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.t_lot_lot TO pmp;
GRANT SELECT ON ref.tr_devenirlot_dev TO pmp;
GRANT SELECT ON ref.tr_typequantitelot_qte TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.ts_taxonvideo_txv TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.ts_taillevideo_tav TO pmp;
GRANT SELECT ON ref.tr_stadedeveloppement_std TO pmp;
GRANT SELECT ON ref.tr_taxon_tax TO pmp;
GRANT SELECT ON ref.tr_niveautaxonomique_ntx TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.t_operation_ope TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.t_periodefonctdispositif_per TO pmp;
GRANT SELECT ON ref.tr_typearretdisp_tar TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.t_dispositifcomptage_dic TO pmp;
GRANT SELECT ON ref.tr_typedc_tdc TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.t_dispositiffranchissement_dif TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.tg_dispositif_dis TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.t_ouvrage_ouv TO pmp;
GRANT SELECT ON ref.tr_natureouvrage_nov TO pmp;
GRANT SELECT ON ref.tr_prelevement_pre TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON ref.ts_sequence_seq TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.tj_stationmesure_stm TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.tj_prelevementlot_prl TO pmp;
GRANT SELECT,INSERT,UPDATE,DELETE ON pmp.t_marque_mqe TO pmp;
GRANT SELECT ON ref.ts_messager_msr TO pmp;
GRANT SELECT ON ref.ts_messagerlang_mrl TO pmp;

GRANT SELECT, UPDATE ON pmp.tg_dispositif_dis_dis_identifiant_seq TO pmp;
GRANT SELECT, UPDATE ON pmp.t_lot_lot_lot_identifiant_seq TO pmp;
GRANT SELECT, UPDATE ON pmp.t_operation_ope_ope_identifiant_seq TO pmp;
GRANT SELECT, UPDATE ON pmp.t_ouvrage_ouv_ouv_identifiant_seq TO pmp;
GRANT SELECT, UPDATE ON pmp.tj_stationmesure_stm_stm_identifiant_seq TO pmp;
GRANT SELECT, UPDATE ON pmp.t_bilanmigrationmensuel_bme_bme_identifiant_seq TO pmp;
GRANT SELECT, UPDATE ON pmp.t_bilanmigrationjournalier_bjo_bjo_identifiant_seq TO pmp;

GRANT SELECT ON ref.ts_sequence_seq TO pmp;
GRANT SELECT ON TABLE ref.ts_sequence_seq TO invite;

-- vues
GRANT SELECT, UPDATE, INSERT ON TABLE pmp.v_taxon_tax TO pmp;
GRANT SELECT, UPDATE, INSERT ON TABLE pmp.vue_lot_ope_car TO pmp;
GRANT SELECT, UPDATE, INSERT ON TABLE pmp.vue_lot_ope_car_qan TO pmp;
GRANT SELECT, UPDATE, INSERT ON TABLE pmp.vue_ope_lot_ech_parqual TO pmp;
GRANT SELECT, UPDATE, INSERT ON TABLE pmp.vue_ope_lot_ech_parquan TO pmp;


-- GRANT select to invite for pmp
GRANT SELECT ON pmp.t_station_sta TO invite;
GRANT SELECT ON pmp.t_bilanmigrationjournalier_bjo TO invite;
GRANT SELECT ON pmp.t_bilanmigrationmensuel_bme TO invite;
GRANT SELECT ON pmp.tj_caracteristiquelot_car TO invite;
GRANT SELECT ON pmp.tj_conditionenvironnementale_env TO invite;
GRANT SELECT ON pmp.tj_stationmesure_stm TO invite;
GRANT SELECT ON pmp.tj_dfesttype_dft TO invite;
GRANT SELECT ON pmp.tj_dfestdestinea_dtx TO invite;
GRANT SELECT ON pmp.tj_tauxechappement_txe TO invite;
GRANT SELECT ON pmp.tj_coefficientconversion_coe TO invite;
GRANT SELECT ON pmp.tj_pathologieconstatee_pco TO invite;
GRANT SELECT ON pmp.tj_actionmarquage_act TO invite;
GRANT SELECT ON pmp.t_marque_mqe TO invite;
GRANT SELECT ON pmp.t_operationmarquage_omq TO invite;
GRANT SELECT ON pmp.t_lot_lot TO invite;
GRANT SELECT ON pmp.ts_taxonvideo_txv TO invite;
GRANT SELECT ON pmp.ts_taillevideo_tav TO invite;
GRANT SELECT ON pmp.t_operation_ope TO invite;
GRANT SELECT ON pmp.t_periodefonctdispositif_per TO invite;
GRANT SELECT ON pmp.t_dispositifcomptage_dic TO invite;
GRANT SELECT ON pmp.t_dispositiffranchissement_dif TO invite;
GRANT SELECT ON pmp.tg_dispositif_dis TO invite;
GRANT SELECT ON pmp.t_ouvrage_ouv TO invite;
GRANT SELECT ON pmp.t_station_sta TO invite;
GRANT SELECT ON pmp.tj_stationmesure_stm TO invite;
GRANT SELECT ON pmp.tj_prelevementlot_prl TO invite;
GRANT SELECT ON pmp.t_marque_mqe TO invite;
--vues
GRANT SELECT ON pmp.v_taxon_tax TO invite;
GRANT SELECT ON pmp.vue_lot_ope_car TO invite;
GRANT SELECT ON pmp.vue_lot_ope_car_qan TO invite;
GRANT SELECT ON pmp.vue_ope_lot_ech_parqual TO invite;
GRANT SELECT ON pmp.vue_ope_lot_ech_parquan TO invite;


GRANT SELECT, UPDATE ON pmp.tg_dispositif_dis_dis_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON pmp.t_lot_lot_lot_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON pmp.t_operation_ope_ope_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON pmp.t_ouvrage_ouv_ouv_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON pmp.tj_stationmesure_stm_stm_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON pmp.t_bilanmigrationmensuel_bme_bme_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON pmp.t_bilanmigrationjournalier_bjo_bjo_identifiant_seq TO invite;
