﻿--Ticket 121

--#######################################################################################################################################
--## Requête sur bd_contmig_nat - schéma logrami : ajout de la notion de cohorte pour les saumons passant très précocément (année n-1) ##
--## Auteur : Marion Legrand - LOGRAMI                                                                                                 ##
--#######################################################################################################################################



--Modification de l'explication dans tg_parametre_par
update ref.tg_parametre_par set par_definition='Classification des âges chez le saumon par l''INRA (définition SANDRE à fournir) et utilisation pour spécifier une cohorte (logrami)'
where par_definition like 'Classification des âges chez le saumon par l''INRA (définition SANDRE à fournir)';

--Ajout du code A126 dans tr_parametrequal (modalités non fournies jusqu'à présent par l'inra d'où A126 non créé dans la table)
insert into ref.tr_parametrequalitatif_qal (qal_par_code,qal_valeurs_possibles)
select 'A126','cohorte année reproduction n / cohorte année reproduction n+1';

--Ajout des modalités dans tr_valeurparametrequalitatif_val
insert into ref.tr_valeurparametrequalitatif_val (val_identifiant,val_qal_code,val_rang,val_libelle)
select 60,'A126',1,'Cohorte de reproduction de l''année n';

insert into ref.tr_valeurparametrequalitatif_val (val_identifiant, val_qal_code,val_rang,val_libelle)
select 61,'A126',2,'Cohorte de reproduction de l''année n+1';

select ref.updatesql('{"azti","bgm","bresle","charente","fd80","iav","inra","logrami","migado","migradour","mrm","saumonrhin","smatah"}',
'INSERT INTO ts_maintenance_main( main_ticket,main_description) VALUES (121,'ajout de la notion de cohorte pour les saumons passant très précocément')');

INSERT INTO iav.ts_maintenance_main( main_ticket,main_description) VALUES (121,'ajout de la notion de cohorte pour les saumons passant très précocément');