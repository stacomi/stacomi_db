﻿/* ticket 110 Marion
problèmes de clés étrangères sur la base
*/
/* fonction ref.updatesql
Cette fonction permet de lancer des requêtes de mise à jour dans tous les schemas
*/
CREATE OR REPLACE FUNCTION ref.updatesql(myschemas varchar[],scriptsql text)RETURNS int AS $$
	DECLARE
	totalcount int;	
	nbschema int;
	i integer;
	BEGIN	  
		select INTO nbschema array_length(myschemas,1);
		i=1;
		While (i <=nbschema) LOOP
		EXECUTE 'SET search_path TO '||myschemas[i]||', public';
		EXECUTE scriptsql;	
		i=i+1;
		END LOOP;	
	RETURN nbschema;
	END;
	$$
LANGUAGE 'plpgsql' ;
COMMENT ON FUNCTION ref.updatesql (myschemas varchar[],scriptsql text) IS 'fonction pour lancer un script de mise à jour sur chaque schema';

/* 
lancement de la fonction sur tous les schémas de la base
ATTENTION ENLEVER LES SCHEMAS QUI NE SONT PAS DANS LA BASE
*/
select ref.updatesql('{"azti","bgm","bresle","charente","fd80","iav","inra","logrami","migado","migradour","mrm","saumonrhin","smatah"}',
'ALTER TABLE t_ouvrage_ouv ADD CONSTRAINT c_fk_ouv_sta_code FOREIGN KEY (ouv_sta_code, ouv_org_code)
      REFERENCES t_station_sta (sta_code, sta_org_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION');--13
 select * from information_schema.constraint_column_usage  where table_catalog='bd_contmig_nat' and constraint_name='c_fk_dft_tdf_code'  ;
 -- Azti et inra à retirer de la liste ci dessous 
 select ref.updatesql('{"bgm","bresle","charente","fd80","iav","logrami","migado","migradour","mrm","saumonrhin","smatah"}',
'ALTER TABLE tj_dfesttype_dft ADD CONSTRAINT c_fk_dft_tdf_code FOREIGN KEY (dft_tdf_code) 
REFERENCES ref.tr_typedf_tdf (tdf_code) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION');--11
-- mise à jour des tables système de la base
select ref.updatesql('{"azti","bgm","bresle","charente","fd80","iav","inra","logrami","migado","migradour","mrm","saumonrhin","smatah"}',
'INSERT INTO ts_maintenance_main( main_ticket,main_description ) VALUES (110,''problèmes de clé étrangères dans les tables ouvrage'')');--13
 


