﻿/*
Etat = Encours pas terminé
12/02/2016
Script pour passer d'un format court à un format long pour données civelles
Cédric
*/


create extension tablefunc;

select distinct car_par_code from iav.vue_lot_ope_car_qan  
WHERE ( ope_date_debut ,  ope_date_fin ) 
overlaps (DATE ' 1996-08-01 ',DATE ' 2016-08-01 ')  
 AND ope_dic_identifiant=6 
 AND std_libelle='civelle' 
 AND  lot_effectif =1 
 AND upper(car_methode_obtention::text) = 'MESURE'::text ;
 /*
 "1791"
"1786"
"A111"
*/
/*
Cet essai avec crosstab ne marche pas car les lignes avec les valeurs nulles
renvoient les poids dans la ligne des tailles
*/
SELECT * FROM crosstab($$
SELECT lot_identifiant, -- colonne ind
case when car_par_code='1786' then 'taille'
     when car_par_code='A111' then 'poids' 
 end as car_par_code, -- colonne catégorie
car_valeur_quantitatif -- colonne valeur
FROM iav.vue_lot_ope_car_qan WHERE ( ope_date_debut ,  ope_date_fin ) 
overlaps (DATE ' 1996-08-01 ',DATE ' 2016-08-01 ')  
 AND ope_dic_identifiant=6 
 AND std_libelle='civelle' 
 AND  lot_effectif =1 
 AND upper(car_methode_obtention::text) = 'MESURE'::text 
 and car_par_code = ANY ('{1786, A111}')
AND car_valeur_quantitatif is not null
order by 1,2$$)
AS ct(lot_identifiant integer,poids real,taille real);



/*
Essai avec un outer join simple
*/
--poids
create view iav.civelle_taille_poids_stade as (
with poids as (
SELECT lot_identifiant, 
car_valeur_quantitatif as poids
FROM iav.vue_lot_ope_car_qan WHERE ( ope_date_debut ,  ope_date_fin ) 
overlaps (DATE ' 1996-08-01 ',DATE ' 2016-08-01 ')  
 AND ope_dic_identifiant=6 
 AND std_libelle='civelle' 
 AND  lot_effectif =1 
 AND upper(car_methode_obtention::text) = 'MESURE'::text 
 and car_par_code = 'A111'
AND car_valeur_quantitatif is not null),
--taile
taille as  (
SELECT lot_identifiant, 
car_valeur_quantitatif as taille
FROM iav.vue_lot_ope_car_qan WHERE ( ope_date_debut ,  ope_date_fin ) 
overlaps (DATE ' 1996-08-01 ',DATE ' 2016-08-01 ')  
 AND ope_dic_identifiant=6 
 AND std_libelle='civelle' 
 AND  lot_effectif =1 
 AND upper(car_methode_obtention::text) = 'MESURE'::text 
 and car_par_code = '1786'
AND car_valeur_quantitatif is not null),
--stade
stade as(
SELECT lot_identifiant, 
car_val_identifiant as stade 
FROM iav.vue_lot_ope_car_qan WHERE ( ope_date_debut ,  ope_date_fin ) 
overlaps (DATE ' 1996-08-01 ',DATE ' 2016-08-01 ')  
 AND ope_dic_identifiant=6 
 AND std_libelle='civelle' 
 AND  lot_effectif =1 
 AND upper(car_methode_obtention::text) = 'MESURE'::text 
 and car_par_code = '1791'
AND car_valeur_quantitatif is not null)
select 
case when poids.lot_identifiant is not null then poids.lot_identifiant
     when poids.lot_identifiant is null and stade.lot_identifiant is null then taille.lot_identifiant
end as lot_identifiant,
taille,poids, stade from poids 
full outer join taille on poids.lot_identifiant=taille.lot_identifiant
full outer join stade on stade.lot_identifiant=poids.lot_identifiant);

full outer join


