-- Mise à jour des descriptions des modèles de masques, si besoin à partir de ceux définis par le script N3
-- Timothée, 16/12/2016

select ref.updatesql('{"logramiang","iav"}',
'UPDATE ts_masque_mas AS mas
SET mas_description= mas_upd.mas_description
FROM (values(''lot_ang'',''Masque pour les anguilles jaunes (passe piège)''),
(''lot_gd_salmo'', ''Masque pour les salmonidés''),
(''lot_civ_indiv'', ''Masque pour les lots de civelles (taille-poids-stade)''),
(''lot_argentée'', ''Masque pour la saisie des argentées''),
(''lot_smot'', ''Masque pour la saisie des données pour les smolts''),
(''lot_I'', ''Masque avec saisie d''''échantillons''),
(''lot_N'', ''Masque pour la saisie des lots en individuel'')	
) AS mas_upd(mas_code,mas_description)
WHERE mas_upd.mas_code = mas.mas_code
'
);