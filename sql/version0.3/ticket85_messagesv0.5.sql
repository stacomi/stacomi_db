﻿-- ticket 85_messagesv0.5 version 0.5
-- start with 337
-- le developpement des messages reste à faire après todo

insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) values (342,'interface_graphique',21,'interface',TRUE,'message de lancement du handler des migrations multiples');
-- la contrainte uk sur (msr_id et mrl_lang) ne sert à rien, modification et ajout d'une contrainte
alter table ref.ts_messagerlang_mrl add  CONSTRAINT c_uk2_ts_messagerlang_mrl UNIQUE (mrl_msr_id, mrl_lang);
-- todo
insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) values (337,'interface_BilanMigrationMult',1,'interface',TRUE,'');
insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) values (338,'interface_BilanMigrationMult',2,'interface',FALSE,' tooltip');
insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) values (339,'interface_BilanMigrationMult',3,'interface',FALSE,'');
insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) values (340,'interface_BilanMigrationMult',4,'interface',FALSE,'');
insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) values (341,'interface_BilanMigrationMult',5,'interface',FALSE,'');
insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) values (91,'BilanMigration',1,'class',TRUE,'');
insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) values (92,'BilanMigration',2,'class',TRUE,'');
insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) values (93,'BilanMigration',3,'class',TRUE,'');
insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) values (94,'BilanMigration',4,'class',TRUE,'');
insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) values (95,'BilanMigration',5,'class',TRUE,'');
insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) values (96,'BilanMigration',6,'class',FALSE,'');
insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) values (97,'BilanMigration',7,'class',FALSE,'');
insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) values (98,'BilanMigration',8,'class',TRUE,'');
insert into ref.ts_messager_msr(msr_id,msr_element,msr_number,msr_type,msr_endofline,msr_comment) values (99,'BilanMigration',9,'class',TRUE,'');

-- FR

-- select * from ref.ts_messagerlang_mrl where mrl_msr_id=320
update ref.ts_messagerlang_mrl set mrl_text= '"Bilan Migration multiples"' where mrl_lang='French' and mrl_msr_id=320;
update ref.ts_messagerlang_mrl set mrl_text= '"Multiple migration report"' where mrl_lang='English' and mrl_msr_id=320;
update ref.ts_messagerlang_mrl set mrl_text= '"Informes de migración múltiples "' where mrl_lang='Spanish' and mrl_msr_id=320;
update ref.ts_messagerlang_mrl set mrl_text= '"Bilan Poids Moyen"' where mrl_lang='French' and mrl_msr_id=230; --bilan_poids_moyen.1
update ref.ts_messagerlang_mrl set mrl_text= '"Mean weight report"' where mrl_lang='English' and mrl_msr_id=230;
update ref.ts_messagerlang_mrl set mrl_text= '"Informes de pesos moyen "' where mrl_lang='Spanish' and mrl_msr_id=230;
update ref.ts_messager_msr set msr_comment='glabel of title bilan poids moyen' where msr_id=230;
update ref.ts_messagerlang_mrl set mrl_text= '"Bilan des poids moyens en vue du calcul des relations poids effectif."' where mrl_lang='French' and mrl_msr_id=296; --interface graphique bilan_poids_moyen.1
update ref.ts_messagerlang_mrl set mrl_text= '"Summary of average weight for the calculation of the relation between length and number."' where mrl_lang='English' and mrl_msr_id=296;
update ref.ts_messagerlang_mrl set mrl_text= '"conclusión de pesos medios para calcular la relaciones de las cantidades de pesos"' where mrl_lang='Spanish' and mrl_msr_id=296;
--interface graphique bilan_poids_moyen.2
update ref.ts_messagerlang_mrl set mrl_text= '"charge"' where mrl_lang='French' and mrl_msr_id=231; 
update ref.ts_messagerlang_mrl set mrl_text= '"load"' where mrl_lang='English' and mrl_msr_id=231;
update ref.ts_messagerlang_mrl set mrl_text= '"load"' where mrl_lang='Spanish' and mrl_msr_id=231;
update ref.ts_messager_msr set msr_comment='gbutton' where msr_id=231;
--interface graphique bilan_poids_moyen.3
update ref.ts_messagerlang_mrl set mrl_text= '"table"' where mrl_lang='French' and mrl_msr_id=232; 
update ref.ts_messagerlang_mrl set mrl_text= '"table"' where mrl_lang='English' and mrl_msr_id=232;
update ref.ts_messagerlang_mrl set mrl_text= '"table"' where mrl_lang='Spanish' and mrl_msr_id=232;
update ref.ts_messager_msr set msr_comment='gbutton' where msr_id=232;
--interface graphique bilan_poids_moyen.4
update ref.ts_messagerlang_mrl set mrl_text= '"quitter"' where mrl_lang='French' and mrl_msr_id=233; 
update ref.ts_messagerlang_mrl set mrl_text= '"exit"' where mrl_lang='English' and mrl_msr_id=233;
update ref.ts_messagerlang_mrl set mrl_text= '"salir"' where mrl_lang='Spanish' and mrl_msr_id=233;
update ref.ts_messager_msr set msr_comment='gbutton' where msr_id=233;
--interface graphique bilan_poids_moyen.5
update ref.ts_messagerlang_mrl set mrl_text= '"choix de la catégorie d''effectif"' where mrl_lang='French' and mrl_msr_id=234; 
update ref.ts_messagerlang_mrl set mrl_text= '"choice of number in sample (one, several,all)"' where mrl_lang='English' and mrl_msr_id=234;
update ref.ts_messagerlang_mrl set mrl_text= '"elección de número en la muestra (uno, varios o todos)"' where mrl_lang='Spanish' and mrl_msr_id=234;
update ref.ts_messager_msr set msr_comment='frame' where msr_id=234;
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (342,'"Bilan migration pour plusieurs DC, taxons, ou stades"','French');

-- todo
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (337,'"Chargement des listes taxons et stades et dc"','French');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (338,'"Calcul des effectifs par pas de temps" ','French');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (339,'"Graphe bilan"','French');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (340,'"Graphe cumul"','French');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (341,'"Tables bilan en .csv"','French');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('91','"Attention le choix du pas de temps n''a pas ete effectue, calcul avec la valeur par defaut "','French');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('92','"Debut du bilan migration... patientez "','French');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('93','"L''objet Bilan est stocke dans l''environnement envir_stacomi, ecrire  ecrire bilanMigration=get(''bilanMigration'',envir_stacomi)"','French');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('94','"Ecriture du tableau de bilan des migrations dans l''environnement envir_stacomi : ecrire tableau=get(''tableau'',envir_stacomi)"','French');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('95','"Il faut faire tourner les calculs avant, cliquer sur calc "','French');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('96','"Migration cumulee"','French');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('97','"Effectif cumule, "','French');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('98','"Attention cette fonction est pour les bilans annuels "','French');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('99','"Statistiques concernant la migration : "','French');


-- EN
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (342,'"Daily migration for several DC, species, or stage"','English');
-- todo
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (337,'Loading of the lists for taxons, stages and counting devices','English');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (338,'Calculation of numbers by time step','English');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (339,'Balance graphic','English');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (340,'Cumulative graphic','English');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (341,'Balance sheet in .csv','English');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('91','"Attention, no time step selected, compunting with default value"','English');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('92','"Starting migration summary ... be patient"','English');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('93','"Summary object is stocked into envir_stacomi environment : write bilanMigration=get(bilanMigration",envir_stacomi)"','English');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('94','"Writing the migration summary table into envir_stacomi environment : write tableau=get(tableau",envir_stacomi)"','English');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('95','"You need to launch computation first, clic on calc"','English');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('96','"Cumulative migration"','English');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('97','"Cumulative count"','English');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('98','"Attention, this function applies for annual summaries"','English');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('99','"Statistics about migration :"','English');

--SP
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (342,'"La migración al día durante varios DC, especie, o etapa de la vida"','Spanish');
--todo
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (337,'"Carga de listas de taxones, estadíos y dc"','Spanish');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (338,'"Cálculo de la cantidad por periodos"','Spanish');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (339,'"Gráfica conclusiones"','Spanish');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (340,'"Gráfico de acumulación"','Spanish');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values (341,'"Tabla Conclusiones en .csv"','Spanish');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('91','"Atención la elección de periodo no ha sido efectuada, cálculo con el valor por defecto"','Spanish');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('92','"Comienzo del Informe de migración… espere"','Spanish');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('93','"El objeto Conclusión está almacenado en el entorno envir_stacomi, escribir  ecrire bilanMigration=get(''bilanMigration'',envir_stacomi)"','Spanish');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('94','"Escritura de tabla de Conclusión de migraciones con el medio ambiente envir_stacomi : escribir tableau=get(''tableau'',envir_stacomi)"','Spanish');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('95','"Se deben de realizar los cálculos antes, haga click sobre calc"','Spanish');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('96','"Migración acumulada"','Spanish');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('97','"Cantidad acumulada"','Spanish');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('98','"Atención esta función es para los Conclusiones anuales"','Spanish');
insert into ref.ts_messagerlang_mrl(mrl_msr_id,mrl_text,mrl_lang) values ('99','"Estadisticas concernientes a la migración"','Spanish');



-- todo mise à jour de la table de maintenance
INSERT INTO migradour.ts_maintenance_main( main_ticket,main_description) VALUES (85,'integration dans le referentiel de l''interface graphique v0.5');