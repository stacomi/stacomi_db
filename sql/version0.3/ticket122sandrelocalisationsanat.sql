﻿/* ticket 110 Marion
problèmes de clés étrangères sur la base
*/
/* fonction ref.updatesql
Cette fonction permet de lancer des requêtes de mise à jour dans tous les schemas
*/
CREATE OR REPLACE FUNCTION ref.updatesql(myschemas varchar[],scriptsql text)RETURNS int AS $$
	DECLARE
	totalcount int;	
	nbschema int;
	i integer;
	BEGIN	  
		select INTO nbschema array_length(myschemas,1);
		i=1;
		While (i <=nbschema) LOOP
		EXECUTE 'SET search_path TO '||myschemas[i]||', public';
		EXECUTE scriptsql;	
		i=i+1;
		END LOOP;	
	RETURN nbschema;
	END;
	$$
LANGUAGE 'plpgsql' ;
COMMENT ON FUNCTION ref.updatesql (myschemas varchar[],scriptsql text) IS 'fonction pour lancer un script de mise à jour sur chaque schema';


/* 
Insertions dans la tables des localisations anatomiques
*/
INSERT INTO ref.tr_localisationanatomique_loc values ('W','Tronc');
INSERT INTO ref.tr_localisationanatomique_loc values ('K','Pédoncule caudal');
INSERT INTO ref.tr_localisationanatomique_loc values ('P','Nageoire pelvienne');
select ref.updatesql('{"azti","bgm","bresle","charente","fd80","iav","inra","logrami","migado","migradour","mrm","saumonrhin","smatah"}',
'INSERT INTO ts_maintenance_main(main_ticket,main_description) values (''122'',''Mise à jour des localisations anatomiques'')');
