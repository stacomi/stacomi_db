﻿set client_encoding to win1252;
insert into ref.tg_parametre_par values ('1553','hauteur de précipitations','mm','ENVIRONNEMENTAL', 'Hauteur des précipitations');
insert into ref.tg_parametre_par values ('1295','Turbidité Formazine Néphélométrique','NTU','ENVIRONNEMENTAL', 'Réduction de la transparence d’un liquide due à la présence de matières non dissoutes, 
mesurée à un angle de 90° par rapport à la lumière incidente (néphélomètre),
Les unités J.T.U. (Jackson Turbidity Units) et N.T.U. (Nephelometric Turbidity Units) sont équivalentes.
La norme française demande l’expression des mesures sous un angle de 90° (la turbidité Néphélométrique) et sous un angle de 0° (Turbidité Formazine). 
Dans les deux cas l’unité utilisée est l’unité formazine.');
INSERT INTO migradour.ts_maintenance_main( main_ticket,main_description) VALUES (74,'Insertion de deux nouveaux paramètres');
