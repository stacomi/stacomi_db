-- DEPRECATED FILE (MAINTENANT DANS to_format_national.sql
ALTER TABLE iav.t_bilanmigrationjournalier_bjo DROP CONSTRAINT c_fk_bjo_std_code;
ALTER TABLE iav.t_bilanmigrationjournalier_bjo ADD CONSTRAINT c_fk_bjo_std_code FOREIGN KEY (bjo_std_code)
      REFERENCES ref.tr_stadedeveloppement_std (std_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.t_bilanmigrationjournalier_bjo DROP CONSTRAINT c_fk_bjo_tax_code;
ALTER TABLE iav.t_bilanmigrationjournalier_bjo ADD CONSTRAINT c_fk_bjo_tax_code FOREIGN KEY (bjo_tax_code)
      REFERENCES ref.tr_taxon_tax (tax_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION

ALTER TABLE iav.t_bilanmigrationmensuel_bme DROP CONSTRAINT c_fk_bme_std_code;
ALTER TABLE iav.t_bilanmigrationmensuel_bme ADD CONSTRAINT c_fk_bme_std_code FOREIGN KEY (bme_std_code)
      REFERENCES ref.tr_stadedeveloppement_std (std_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.t_bilanmigrationmensuel_bme DROP CONSTRAINT c_fk_bme_tax_code;
ALTER TABLE iav.t_bilanmigrationmensuel_bme ADD CONSTRAINT c_fk_bme_tax_code FOREIGN KEY (bme_tax_code)
      REFERENCES ref.tr_taxon_tax (tax_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;


ALTER TABLE iav.t_dispositifcomptage_dic DROP CONSTRAINT c_fk_dic_org_code;
ALTER TABLE iav.t_dispositifcomptage_dic ADD CONSTRAINT c_fk_dic_org_code FOREIGN KEY (dic_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.t_dispositifcomptage_dic DROP CONSTRAINT c_fk_dic_tdc_code;
ALTER TABLE iav.t_dispositifcomptage_dic ADD CONSTRAINT c_fk_dic_tdc_code FOREIGN KEY (dic_tdc_code)
      REFERENCES ref.tr_typedc_tdc (tdc_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;


ALTER TABLE iav.t_dispositiffranchissement_dif DROP CONSTRAINT c_fk_dif_org_code;
ALTER TABLE iav.t_dispositiffranchissement_dif ADD CONSTRAINT c_fk_dif_org_code FOREIGN KEY (dif_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;


ALTER TABLE iav.t_lot_lot DROP CONSTRAINT c_fk_lot_dev_code;
ALTER TABLE iav.t_lot_lot ADD CONSTRAINT c_fk_lot_dev_code FOREIGN KEY (lot_dev_code)
      REFERENCES ref.tr_devenirlot_dev (dev_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.t_lot_lot DROP CONSTRAINT c_fk_lot_org_code;
ALTER TABLE iav.t_lot_lot ADD CONSTRAINT c_fk_lot_org_code FOREIGN KEY (lot_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.t_lot_lot DROP CONSTRAINT c_fk_lot_qte_code;
ALTER TABLE iav.t_lot_lot ADD CONSTRAINT c_fk_lot_qte_code FOREIGN KEY (lot_qte_code)
      REFERENCES ref.tr_typequantitelot_qte (qte_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.t_lot_lot DROP CONSTRAINT c_fk_lot_std_code;
ALTER TABLE iav.t_lot_lot ADD CONSTRAINT c_fk_lot_std_code FOREIGN KEY (lot_std_code)
      REFERENCES ref.tr_stadedeveloppement_std (std_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.t_lot_lot DROP CONSTRAINT c_fk_lot_tax_code;
ALTER TABLE iav.t_lot_lot ADD CONSTRAINT c_fk_lot_tax_code FOREIGN KEY (lot_tax_code)
      REFERENCES ref.tr_taxon_tax (tax_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;




ALTER TABLE iav.t_marque_mqe DROP CONSTRAINT c_fk_mqe_loc_code;
ALTER TABLE iav.t_marque_mqe ADD CONSTRAINT  c_fk_mqe_loc_code FOREIGN KEY (mqe_loc_code)
      REFERENCES ref.tr_localisationanatomique_loc (loc_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.t_marque_mqe DROP CONSTRAINT c_fk_mqe_nmq_code;
ALTER TABLE iav.t_marque_mqe ADD CONSTRAINT  c_fk_mqe_nmq_code FOREIGN KEY (mqe_nmq_code)
      REFERENCES ref.tr_naturemarque_nmq (nmq_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.t_marque_mqe DROP CONSTRAINT c_fk_mqe_org_code;
ALTER TABLE iav.t_marque_mqe ADD CONSTRAINT c_fk_mqe_org_code FOREIGN KEY (mqe_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;




ALTER TABLE iav.t_operation_ope DROP CONSTRAINT c_fk_ope_org_code;
ALTER TABLE iav.t_operation_ope ADD CONSTRAINT c_fk_ope_org_code FOREIGN KEY (ope_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE iav.t_ouvrage_ouv DROP CONSTRAINT c_fk_ouv_nov_code;
ALTER TABLE iav.t_ouvrage_ouv ADD CONSTRAINT c_fk_ouv_nov_code FOREIGN KEY (ouv_nov_code)
      REFERENCES ref.tr_natureouvrage_nov (nov_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.t_ouvrage_ouv DROP CONSTRAINT c_fk_ouv_org_code;
ALTER TABLE iav.t_ouvrage_ouv ADD CONSTRAINT c_fk_ouv_org_code FOREIGN KEY (ouv_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE iav.t_periodefonctdispositif_per DROP CONSTRAINT c_fk_per_org_code;
ALTER TABLE iav.t_periodefonctdispositif_per ADD CONSTRAINT c_fk_per_org_code FOREIGN KEY (per_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.t_periodefonctdispositif_per DROP CONSTRAINT c_fk_per_tar_code;
ALTER TABLE iav.t_periodefonctdispositif_per ADD CONSTRAINT c_fk_per_tar_code FOREIGN KEY (per_tar_code)
      REFERENCES ref.tr_typearretdisp_tar (tar_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE iav.t_station_sta DROP CONSTRAINT c_fk_sta_org_code;
ALTER TABLE iav.t_station_sta ADD CONSTRAINT c_fk_sta_org_code FOREIGN KEY (sta_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;


ALTER TABLE iav.tg_dispositif_dis DROP CONSTRAINT c_fk_sta_org_code;
ALTER TABLE iav.tg_dispositif_dis ADD CONSTRAINT c_fk_sta_org_code FOREIGN KEY (dis_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE iav.tj_actionmarquage_act DROP CONSTRAINT c_fk_act_org_code;
ALTER TABLE iav.tj_actionmarquage_act ADD CONSTRAINT c_fk_act_org_code FOREIGN KEY (act_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;




ALTER TABLE iav.tj_caracteristiquelot_car DROP CONSTRAINT c_fk_car_org_code;
ALTER TABLE iav.tj_caracteristiquelot_car ADD CONSTRAINT c_fk_car_org_code FOREIGN KEY (car_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.tj_caracteristiquelot_car DROP CONSTRAINT c_fk_car_par_code;
ALTER TABLE iav.tj_caracteristiquelot_car ADD CONSTRAINT c_fk_car_par_code FOREIGN KEY (car_par_code)
      REFERENCES ref.tg_parametre_par (par_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.tj_caracteristiquelot_car DROP CONSTRAINT c_fk_car_val_identifiant;
ALTER TABLE iav.tj_caracteristiquelot_car ADD CONSTRAINT c_fk_car_val_identifiant FOREIGN KEY (car_val_identifiant)
      REFERENCES ref.tr_valeurparametrequalitatif_val (val_identifiant) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;




ALTER TABLE iav.tj_coefficientconversion_coe DROP CONSTRAINT c_fk_coe_qte_code;
ALTER TABLE iav.tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_qte_code FOREIGN KEY (coe_qte_code)
      REFERENCES ref.tr_typequantitelot_qte (qte_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.tj_coefficientconversion_coe DROP CONSTRAINT c_fk_coe_std_code;
ALTER TABLE iav.tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_std_code FOREIGN KEY (coe_std_code)
      REFERENCES ref.tr_stadedeveloppement_std (std_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.tj_coefficientconversion_coe DROP CONSTRAINT c_fk_coe_tax_code;
ALTER TABLE iav.tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_tax_code FOREIGN KEY (coe_tax_code)
      REFERENCES ref.tr_taxon_tax (tax_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;








ALTER TABLE iav.tj_conditionenvironnementale_env DROP CONSTRAINT c_fk_env_org_code;
ALTER TABLE iav.tj_conditionenvironnementale_env ADD CONSTRAINT c_fk_env_org_code FOREIGN KEY (env_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.tj_conditionenvironnementale_env DROP CONSTRAINT c_fk_env_val_identifiant;
ALTER TABLE iav.tj_conditionenvironnementale_env ADD CONSTRAINT c_fk_env_val_identifiant FOREIGN KEY (env_val_identifiant)
      REFERENCES ref.tr_valeurparametrequalitatif_val (val_identifiant) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;





ALTER TABLE iav.tj_dfestdestinea_dtx DROP CONSTRAINT c_fk_dtx_org_code;
ALTER TABLE iav.tj_dfestdestinea_dtx ADD CONSTRAINT c_fk_dtx_org_code FOREIGN KEY (dtx_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.tj_dfestdestinea_dtx DROP CONSTRAINT c_fk_dtx_tax_code;
ALTER TABLE iav.tj_dfestdestinea_dtx ADD CONSTRAINT c_fk_dtx_tax_code FOREIGN KEY (dtx_tax_code)
      REFERENCES ref.tr_taxon_tax (tax_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;




ALTER TABLE iav.tj_dfesttype_dft DROP CONSTRAINT c_fk_dft_org_code;
ALTER TABLE iav.tj_dfesttype_dft ADD CONSTRAINT c_fk_dft_org_code FOREIGN KEY (dft_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.tj_dfesttype_dft DROP CONSTRAINT c_fk_dft_tdf_code;
ALTER TABLE iav.tj_dfesttype_dft ADD CONSTRAINT c_fk_dft_tdf_code FOREIGN KEY (dft_tdf_code)
      REFERENCES ref.tr_typedf_tdf (tdf_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;




ALTER TABLE iav.tj_pathologieconstatee_pco DROP CONSTRAINT c_fk_pco_loc_code;
ALTER TABLE iav.tj_pathologieconstatee_pco ADD CONSTRAINT c_fk_pco_loc_code FOREIGN KEY (pco_loc_code)
      REFERENCES ref.tr_localisationanatomique_loc (loc_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.tj_pathologieconstatee_pco DROP CONSTRAINT c_fk_pco_org_code;
ALTER TABLE iav.tj_pathologieconstatee_pco ADD CONSTRAINT c_fk_pco_org_code FOREIGN KEY (pco_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.tj_pathologieconstatee_pco DROP CONSTRAINT c_fk_pco_pat_code;
ALTER TABLE iav.tj_pathologieconstatee_pco ADD CONSTRAINT c_fk_pco_pat_code FOREIGN KEY (pco_pat_code)
      REFERENCES ref.tr_pathologie_pat (pat_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;


ALTER TABLE iav.tj_prelevementlot_prl DROP CONSTRAINT c_fk_prl_loc_code;
ALTER TABLE iav.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_loc_code FOREIGN KEY (prl_loc_code)
      REFERENCES ref.tr_localisationanatomique_loc (loc_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.tj_prelevementlot_prl DROP CONSTRAINT c_fk_prl_pre_nom;
ALTER TABLE iav.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_pre_nom FOREIGN KEY (prl_pre_typeprelevement)
      REFERENCES ref.tr_prelevement_pre (pre_typeprelevement) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;




ALTER TABLE iav.tj_stationmesure_stm DROP CONSTRAINT c_fk_stm_org_code;
ALTER TABLE iav.tj_stationmesure_stm ADD CONSTRAINT c_fk_stm_org_code FOREIGN KEY (stm_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.tj_stationmesure_stm DROP CONSTRAINT c_fk_stm_par_code;
ALTER TABLE iav.tj_stationmesure_stm ADD CONSTRAINT c_fk_stm_par_code FOREIGN KEY (stm_par_code)
      REFERENCES ref.tg_parametre_par (par_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;







ALTER TABLE iav.tj_tauxechappement_txe DROP CONSTRAINT c_fk_txe_ech_code;
ALTER TABLE iav.tj_tauxechappement_txe ADD CONSTRAINT c_fk_txe_ech_code FOREIGN KEY (txe_ech_code)
      REFERENCES ref.tr_niveauechappement_ech (ech_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.tj_tauxechappement_txe DROP CONSTRAINT c_fk_txe_org_code;
ALTER TABLE iav.tj_tauxechappement_txe ADD CONSTRAINT c_fk_txe_org_code FOREIGN KEY (txe_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.tj_tauxechappement_txe DROP CONSTRAINT c_fk_txe_std_code;
ALTER TABLE iav.tj_tauxechappement_txe ADD CONSTRAINT c_fk_txe_std_code FOREIGN KEY (txe_std_code)
      REFERENCES ref.tr_stadedeveloppement_std (std_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.tj_tauxechappement_txe DROP CONSTRAINT c_fk_txe_tax_code;
ALTER TABLE iav.tj_tauxechappement_txe ADD CONSTRAINT c_fk_txe_tax_code FOREIGN KEY (txe_tax_code)
      REFERENCES ref.tr_taxon_tax (tax_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;



ALTER TABLE iav.ts_taillevideo_tav DROP CONSTRAINT c_fk_tav_org_code;
ALTER TABLE iav.ts_taillevideo_tav ADD CONSTRAINT c_fk_tav_org_code FOREIGN KEY (tav_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;




ALTER TABLE iav.ts_taxonvideo_txv DROP CONSTRAINT c_fk_std_code;
ALTER TABLE iav.ts_taxonvideo_txv ADD CONSTRAINT c_fk_std_code FOREIGN KEY (txv_std_code)
      REFERENCES ref.tr_stadedeveloppement_std (std_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.ts_taxonvideo_txv DROP CONSTRAINT c_fk_txv_org_code;
ALTER TABLE iav.ts_taxonvideo_txv ADD CONSTRAINT c_fk_txv_org_code FOREIGN KEY (txv_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE iav.ts_taxonvideo_txv DROP CONSTRAINT c_fk_txv_tax_code;
ALTER TABLE iav.ts_taxonvideo_txv ADD CONSTRAINT c_fk_txv_tax_code FOREIGN KEY (txv_tax_code)
      REFERENCES ref.tr_taxon_tax (tax_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;




CREATE OR REPLACE VIEW iav.v_taxon_tax AS 
 SELECT tax.tax_code, tax.tax_nom_latin, tax.tax_nom_commun, tax.tax_ntx_code, tax.tax_tax_code, tax.tax_rang, txv.txv_code, txv.txv_tax_code, txv.txv_std_code, std.std_code, std.std_libelle, std.std_rang
   FROM ref.tr_taxon_tax tax
   RIGHT JOIN iav.ts_taxonvideo_txv txv ON tax.tax_code::text = txv.txv_tax_code::text
   LEFT JOIN ref.tr_stadedeveloppement_std std ON txv.txv_std_code::text = std.std_code::text;
ALTER TABLE iav.v_taxon_tax OWNER TO postgres;


CREATE OR REPLACE VIEW iav.vue_lot_ope AS 
 SELECT t_operation_ope.ope_identifiant, t_lot_lot.lot_identifiant, t_operation_ope.ope_dic_identifiant, t_lot_lot.lot_lot_identifiant AS lot_pere, t_lot_lot.lot_methode_obtention, t_operation_ope.ope_date_debut, t_operation_ope.ope_date_fin, t_lot_lot.lot_effectif, t_lot_lot.lot_quantite, t_lot_lot.lot_tax_code, tr_taxon_tax.tax_nom_latin, tr_stadedeveloppement_std.std_libelle, tr_devenirlot_dev.dev_code, tr_devenirlot_dev.dev_libelle
   FROM iav.t_operation_ope
   JOIN iav.t_lot_lot ON t_lot_lot.lot_ope_identifiant = t_operation_ope.ope_identifiant
   LEFT JOIN ref.tr_typequantitelot_qte ON tr_typequantitelot_qte.qte_code::text = t_lot_lot.lot_qte_code::text
   LEFT JOIN ref.tr_devenirlot_dev ON tr_devenirlot_dev.dev_code::text = t_lot_lot.lot_dev_code::text
   JOIN ref.tr_taxon_tax ON tr_taxon_tax.tax_code::text = t_lot_lot.lot_tax_code::text
   JOIN ref.tr_stadedeveloppement_std ON tr_stadedeveloppement_std.std_code::text = t_lot_lot.lot_std_code::text
  ORDER BY t_operation_ope.ope_date_debut;


  CREATE OR REPLACE VIEW iav.vue_lot_ope_car AS 
 SELECT t_operation_ope.ope_identifiant, t_lot_lot.lot_identifiant, t_operation_ope.ope_dic_identifiant, t_lot_lot.lot_lot_identifiant AS lot_pere, t_operation_ope.ope_date_debut, t_operation_ope.ope_date_fin, t_lot_lot.lot_effectif, t_lot_lot.lot_quantite, t_lot_lot.lot_tax_code, t_lot_lot.lot_std_code, tr_taxon_tax.tax_nom_latin, tr_stadedeveloppement_std.std_libelle, tr_devenirlot_dev.dev_code, tr_devenirlot_dev.dev_libelle, tg_parametre_par.par_nom, tj_caracteristiquelot_car.car_par_code, tj_caracteristiquelot_car.car_methode_obtention, tj_caracteristiquelot_car.car_val_identifiant, tj_caracteristiquelot_car.car_valeur_quantitatif, tr_valeurparametrequalitatif_val.val_libelle
   FROM iav.t_operation_ope
   JOIN iav.t_lot_lot ON t_lot_lot.lot_ope_identifiant = t_operation_ope.ope_identifiant
   LEFT JOIN ref.tr_typequantitelot_qte ON tr_typequantitelot_qte.qte_code::text = t_lot_lot.lot_qte_code::text
   LEFT JOIN ref.tr_devenirlot_dev ON tr_devenirlot_dev.dev_code::text = t_lot_lot.lot_dev_code::text
   JOIN ref.tr_taxon_tax ON tr_taxon_tax.tax_code::text = t_lot_lot.lot_tax_code::text
   JOIN ref.tr_stadedeveloppement_std ON tr_stadedeveloppement_std.std_code::text = t_lot_lot.lot_std_code::text
   JOIN iav.tj_caracteristiquelot_car ON tj_caracteristiquelot_car.car_lot_identifiant = t_lot_lot.lot_identifiant
   LEFT JOIN ref.tg_parametre_par ON tj_caracteristiquelot_car.car_par_code::text = tg_parametre_par.par_code::text
   LEFT JOIN ref.tr_parametrequalitatif_qal ON tr_parametrequalitatif_qal.qal_par_code::text = tg_parametre_par.par_code::text
   LEFT JOIN ref.tr_valeurparametrequalitatif_val ON tj_caracteristiquelot_car.car_val_identifiant = tr_valeurparametrequalitatif_val.val_identifiant
  ORDER BY t_operation_ope.ope_date_debut;



CREATE OR REPLACE VIEW iav.vue_lot_ope_car_qan AS 
 SELECT t_operation_ope.ope_identifiant, t_lot_lot.lot_identifiant, t_operation_ope.ope_dic_identifiant, t_lot_lot.lot_lot_identifiant AS lot_pere, t_operation_ope.ope_date_debut, t_operation_ope.ope_date_fin, t_lot_lot.lot_effectif, t_lot_lot.lot_quantite, t_lot_lot.lot_tax_code, tr_taxon_tax.tax_nom_latin, tr_stadedeveloppement_std.std_libelle, tr_devenirlot_dev.dev_code, tr_devenirlot_dev.dev_libelle, tg_parametre_par.par_nom, tj_caracteristiquelot_car.car_par_code, tj_caracteristiquelot_car.car_methode_obtention, tj_caracteristiquelot_car.car_val_identifiant, tj_caracteristiquelot_car.car_valeur_quantitatif, tr_valeurparametrequalitatif_val.val_libelle
   FROM iav.t_operation_ope
   JOIN iav.t_lot_lot ON t_lot_lot.lot_ope_identifiant = t_operation_ope.ope_identifiant
   LEFT JOIN ref.tr_typequantitelot_qte ON tr_typequantitelot_qte.qte_code::text = t_lot_lot.lot_qte_code::text
   LEFT JOIN ref.tr_devenirlot_dev ON tr_devenirlot_dev.dev_code::text = t_lot_lot.lot_dev_code::text
   JOIN ref.tr_taxon_tax ON tr_taxon_tax.tax_code::text = t_lot_lot.lot_tax_code::text
   JOIN ref.tr_stadedeveloppement_std ON tr_stadedeveloppement_std.std_code::text = t_lot_lot.lot_std_code::text
   JOIN iav.tj_caracteristiquelot_car ON tj_caracteristiquelot_car.car_lot_identifiant = t_lot_lot.lot_identifiant
   LEFT JOIN ref.tg_parametre_par ON tj_caracteristiquelot_car.car_par_code::text = tg_parametre_par.par_code::text
   LEFT JOIN ref.tr_parametrequantitatif_qan ON tr_parametrequantitatif_qan.qan_par_code::text = tg_parametre_par.par_code::text
   LEFT JOIN ref.tr_valeurparametrequalitatif_val ON tj_caracteristiquelot_car.car_val_identifiant = tr_valeurparametrequalitatif_val.val_identifiant
  ORDER BY t_operation_ope.ope_date_debut;





CREATE OR REPLACE VIEW iav.vue_ope_lot_ech_parqual AS 
 SELECT t_operation_ope.ope_identifiant, t_operation_ope.ope_dic_identifiant, t_operation_ope.ope_date_debut, t_operation_ope.ope_date_fin, lot.lot_identifiant, lot.lot_methode_obtention, lot.lot_effectif, lot.lot_quantite, lot.lot_tax_code, lot.lot_std_code, tr_taxon_tax.tax_nom_latin, tr_stadedeveloppement_std.std_libelle, dev.dev_code, dev.dev_libelle, par.par_nom, car.car_par_code, car.car_methode_obtention, car.car_val_identifiant, val.val_libelle, lot.lot_lot_identifiant AS lot_pere, lot_pere.lot_effectif AS lot_pere_effectif, lot_pere.lot_quantite AS lot_pere_quantite, dev_pere.dev_code AS lot_pere_dev_code, dev_pere.dev_libelle AS lot_pere_dev_libelle, parqual.par_nom AS lot_pere_par_nom, parqual.car_par_code AS lot_pere_par_code, parqual.car_methode_obtention AS lot_pere_car_methode_obtention, parqual.car_val_identifiant AS lot_pere_val_identifiant, parqual.val_libelle AS lot_pere_val_libelle
   FROM iav.t_operation_ope
   JOIN iav.t_lot_lot lot ON lot.lot_ope_identifiant = t_operation_ope.ope_identifiant
   LEFT JOIN ref.tr_typequantitelot_qte qte ON qte.qte_code::text = lot.lot_qte_code::text
   LEFT JOIN ref.tr_devenirlot_dev dev ON dev.dev_code::text = lot.lot_dev_code::text
   JOIN ref.tr_taxon_tax ON tr_taxon_tax.tax_code::text = lot.lot_tax_code::text
   JOIN ref.tr_stadedeveloppement_std ON tr_stadedeveloppement_std.std_code::text = lot.lot_std_code::text
   JOIN iav.tj_caracteristiquelot_car car ON car.car_lot_identifiant = lot.lot_identifiant
   LEFT JOIN ref.tg_parametre_par par ON car.car_par_code::text = par.par_code::text
   JOIN ref.tr_parametrequalitatif_qal qal ON qal.qal_par_code::text = par.par_code::text
   LEFT JOIN ref.tr_valeurparametrequalitatif_val val ON car.car_val_identifiant = val.val_identifiant
   LEFT JOIN iav.t_lot_lot lot_pere ON lot_pere.lot_identifiant = lot.lot_lot_identifiant
   LEFT JOIN ref.tr_typequantitelot_qte qte_pere ON qte_pere.qte_code::text = lot_pere.lot_qte_code::text
   LEFT JOIN ref.tr_devenirlot_dev dev_pere ON dev_pere.dev_code::text = lot_pere.lot_dev_code::text
   LEFT JOIN ( SELECT car_pere.car_lot_identifiant, car_pere.car_par_code, car_pere.car_methode_obtention, car_pere.car_val_identifiant, car_pere.car_valeur_quantitatif, car_pere.car_precision, car_pere.car_commentaires, par_pere.par_code, par_pere.par_nom, par_pere.par_unite, par_pere.par_nature, par_pere.par_definition, qal_pere.qal_par_code, qal_pere.qal_valeurs_possibles, val_pere.val_identifiant, val_pere.val_qal_code, val_pere.val_rang, val_pere.val_libelle
   FROM iav.tj_caracteristiquelot_car car_pere
   LEFT JOIN ref.tg_parametre_par par_pere ON car_pere.car_par_code::text = par_pere.par_code::text
   JOIN ref.tr_parametrequalitatif_qal qal_pere ON qal_pere.qal_par_code::text = par_pere.par_code::text
   LEFT JOIN ref.tr_valeurparametrequalitatif_val val_pere ON car_pere.car_val_identifiant = val_pere.val_identifiant) parqual ON parqual.car_lot_identifiant = lot_pere.lot_identifiant;



CREATE OR REPLACE VIEW iav.vue_ope_lot_ech_parquan AS 
 SELECT t_operation_ope.ope_identifiant, t_operation_ope.ope_dic_identifiant, t_operation_ope.ope_date_debut, t_operation_ope.ope_date_fin, lot.lot_identifiant, lot.lot_methode_obtention, lot.lot_effectif, lot.lot_quantite, lot.lot_tax_code, lot.lot_std_code, tr_taxon_tax.tax_nom_latin, tr_stadedeveloppement_std.std_libelle, dev.dev_code, dev.dev_libelle, par.par_nom, car.car_par_code, car.car_methode_obtention, car.car_valeur_quantitatif, lot.lot_lot_identifiant AS lot_pere, lot_pere.lot_effectif AS lot_pere_effectif, lot_pere.lot_quantite AS lot_pere_quantite, dev_pere.dev_code AS lot_pere_dev_code, dev_pere.dev_libelle AS lot_pere_dev_libelle, parqual.par_nom AS lot_pere_par_nom, parqual.car_par_code AS lot_pere_par_code, parqual.car_methode_obtention AS lot_pere_car_methode_obtention, parqual.car_val_identifiant AS lot_pere_val_identifiant, parqual.val_libelle AS lot_pere_val_libelle
   FROM iav.t_operation_ope
   JOIN iav.t_lot_lot lot ON lot.lot_ope_identifiant = t_operation_ope.ope_identifiant
   LEFT JOIN ref.tr_typequantitelot_qte qte ON qte.qte_code::text = lot.lot_qte_code::text
   LEFT JOIN ref.tr_devenirlot_dev dev ON dev.dev_code::text = lot.lot_dev_code::text
   JOIN ref.tr_taxon_tax ON tr_taxon_tax.tax_code::text = lot.lot_tax_code::text
   JOIN ref.tr_stadedeveloppement_std ON tr_stadedeveloppement_std.std_code::text = lot.lot_std_code::text
   JOIN iav.tj_caracteristiquelot_car car ON car.car_lot_identifiant = lot.lot_identifiant
   LEFT JOIN ref.tg_parametre_par par ON car.car_par_code::text = par.par_code::text
   JOIN ref.tr_parametrequantitatif_qan qan ON qan.qan_par_code::text = par.par_code::text
   LEFT JOIN iav.t_lot_lot lot_pere ON lot_pere.lot_identifiant = lot.lot_lot_identifiant
   LEFT JOIN ref.tr_typequantitelot_qte qte_pere ON qte_pere.qte_code::text = lot_pere.lot_qte_code::text
   LEFT JOIN ref.tr_devenirlot_dev dev_pere ON dev_pere.dev_code::text = lot_pere.lot_dev_code::text
   LEFT JOIN ( SELECT car_pere.car_lot_identifiant, car_pere.car_par_code, car_pere.car_methode_obtention, car_pere.car_val_identifiant, car_pere.car_valeur_quantitatif, car_pere.car_precision, car_pere.car_commentaires, par_pere.par_code, par_pere.par_nom, par_pere.par_unite, par_pere.par_nature, par_pere.par_definition, qal_pere.qal_par_code, qal_pere.qal_valeurs_possibles, val_pere.val_identifiant, val_pere.val_qal_code, val_pere.val_rang, val_pere.val_libelle
   FROM iav.tj_caracteristiquelot_car car_pere
   LEFT JOIN ref.tg_parametre_par par_pere ON car_pere.car_par_code::text = par_pere.par_code::text
   JOIN ref.tr_parametrequalitatif_qal qal_pere ON qal_pere.qal_par_code::text = par_pere.par_code::text
   LEFT JOIN ref.tr_valeurparametrequalitatif_val val_pere ON car_pere.car_val_identifiant = val_pere.val_identifiant) parqual ON parqual.car_lot_identifiant = lot_pere.lot_identifiant;




  
DROP TABLE ref.tr_devenirlot_dev;
DROP TABLE ref.tr_localisationanatomique_loc;
DROP TABLE ref.tr_naturemarque_nmq;
DROP TABLE ref.tr_natureouvrage_nov;
DROP TABLE ref.tr_niveauechappement_ech;
DROP TABLE ref.tr_taxon_tax;
DROP TABLE ref.tr_niveautaxonomique_ntx; -- ici
DROP TABLE ref.tr_valeurparametrequalitatif_val;
DROP TABLE ref.tr_parametrequalitatif_qal;
DROP TABLE ref.tr_parametrequantitatif_qan;
DROP TABLE ref.tr_pathologie_pat;
DROP TABLE ref.tr_prelevement_pre;
DROP TABLE ref.tr_stadedeveloppement_std;
DROP TABLE ref.tr_typearretdisp_tar;
DROP TABLE ref.tr_typedc_tdc;
DROP TABLE ref.tr_typedf_tdf;
DROP TABLE ref.tr_typequantitelot_qte;
DROP TABLE iav.ts_organisme_org;
DROP TABLE iav.ts_sequence_seq;



























