﻿select max(tax_rang) from ref.tr_taxon_tax;
/*
code pour l'insertion de taxon dans le référentiel
le rang doit être plus grand que le dernier rang rentré dans la table
pour l'instant on en est à 88
Il faut remplacer les valeurs par les bonnes valeurs
*/
insert into ref.tr_taxon_tax (
(
  tax_code,
  tax_nom_latin,
  tax_nom_commun,
  tax_ntx_code,
  tax_tax_code,
  tax_rang) values (
code_sandre,'nom latin','nom commun',15 si espece 13 si genre,'code du genre si present dans la table',88);