﻿/* 
Certaines table n'ont pas la colonne organisme, c'est une erreur
*/
alter table iav.tj_prelevementlot_prl  drop CONSTRAINT c_pk_prl;
alter table iav.tj_prelevementlot_prl add prl_org_code character varying(30) NOT NULL;
alter table iav.tj_prelevementlot_prl add constraint c_pk_prl PRIMARY KEY (prl_pre_typeprelevement, prl_lot_identifiant, prl_code,prl_org_code);
ALTER TABLE iav.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_org_code FOREIGN KEY (prl_org_code) REFERENCES ref.ts_organisme_org(org_code);
--modif de la table create_new_schema
--modif sur centraldata
-- existe déjà en localhost

-- modif pour le schema bgm
alter table bgm.tj_prelevementlot_prl  drop CONSTRAINT c_pk_prl;
alter table bgm.tj_prelevementlot_prl add prl_org_code character varying(30) NOT NULL;
alter table bgm.tj_prelevementlot_prl add constraint c_pk_prl PRIMARY KEY (prl_pre_typeprelevement, prl_lot_identifiant, prl_code,prl_org_code);
ALTER TABLE bgm.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_org_code FOREIGN KEY (prl_org_code) REFERENCES ref.ts_organisme_org(org_code);


alter table charente.tj_prelevementlot_prl  drop CONSTRAINT c_pk_prl;
alter table charente.tj_prelevementlot_prl add prl_org_code character varying(30) NOT NULL;
alter table charente.tj_prelevementlot_prl add constraint c_pk_prl PRIMARY KEY (prl_pre_typeprelevement, prl_lot_identifiant, prl_code,prl_org_code);
ALTER TABLE charente.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_org_code FOREIGN KEY (prl_org_code) REFERENCES ref.ts_organisme_org(org_code);

alter table inra.tj_prelevementlot_prl  drop CONSTRAINT c_pk_prl;
alter table inra.tj_prelevementlot_prl add prl_org_code character varying(30);
update inra.tj_prelevementlot_prl set prl_org_code='INRA';
alter table inra.tj_prelevementlot_prl ADD constraint c_nn_prl_org_code check (prl_org_code IS NOT NULL);
alter table inra.tj_prelevementlot_prl add constraint c_pk_prl PRIMARY KEY (prl_pre_typeprelevement, prl_lot_identifiant, prl_code,prl_org_code);
ALTER TABLE inra.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_org_code FOREIGN KEY (prl_org_code) REFERENCES ref.ts_organisme_org(org_code);

alter table logrami.tj_prelevementlot_prl  drop CONSTRAINT c_pk_prl;
alter table logrami.tj_prelevementlot_prl add prl_org_code character varying(30) NOT NULL;
alter table logrami.tj_prelevementlot_prl add constraint c_pk_prl PRIMARY KEY (prl_pre_typeprelevement, prl_lot_identifiant, prl_code,prl_org_code);
ALTER TABLE logrami.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_org_code FOREIGN KEY (prl_org_code) REFERENCES ref.ts_organisme_org(org_code);

alter table migado.tj_prelevementlot_prl  drop CONSTRAINT c_pk_prl;
alter table migado.tj_prelevementlot_prl add prl_org_code character varying(30) NOT NULL;
alter table migado.tj_prelevementlot_prl add constraint c_pk_prl PRIMARY KEY (prl_pre_typeprelevement, prl_lot_identifiant, prl_code,prl_org_code);
ALTER TABLE migado.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_org_code FOREIGN KEY (prl_org_code) REFERENCES ref.ts_organisme_org(org_code);

alter table migradour.tj_prelevementlot_prl  drop CONSTRAINT c_pk_prl;
alter table migradour.tj_prelevementlot_prl add prl_org_code character varying(30) NOT NULL;
alter table migradour.tj_prelevementlot_prl add constraint c_pk_prl PRIMARY KEY (prl_pre_typeprelevement, prl_lot_identifiant, prl_code,prl_org_code);
ALTER TABLE migradour.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_org_code FOREIGN KEY (prl_org_code) REFERENCES ref.ts_organisme_org(org_code);

alter table mrm.tj_prelevementlot_prl  drop CONSTRAINT c_pk_prl;
alter table mrm.tj_prelevementlot_prl add prl_org_code character varying(30) NOT NULL;
alter table mrm.tj_prelevementlot_prl add constraint c_pk_prl PRIMARY KEY (prl_pre_typeprelevement, prl_lot_identifiant, prl_code,prl_org_code);
ALTER TABLE mrm.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_org_code FOREIGN KEY (prl_org_code) REFERENCES ref.ts_organisme_org(org_code);

alter table saumonrhin.tj_prelevementlot_prl  drop CONSTRAINT c_pk_prl;
alter table saumonrhin.tj_prelevementlot_prl add prl_org_code character varying(30) NOT NULL;
alter table saumonrhin.tj_prelevementlot_prl add constraint c_pk_prl PRIMARY KEY (prl_pre_typeprelevement, prl_lot_identifiant, prl_code,prl_org_code);
ALTER TABLE saumonrhin.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_org_code FOREIGN KEY (prl_org_code) REFERENCES ref.ts_organisme_org(org_code);

/* 
table tj_coefficient_conversion
*/

/* 
tj_coefficientconversion_coe 
*/
alter table iav.tj_coefficientconversion_coe  drop CONSTRAINT c_pk_coe;
alter table iav.tj_coefficientconversion_coe add coe_org_code character varying(30);
update iav.tj_coefficientconversion_coe set coe_org_code='IAV';
alter table iav.tj_coefficientconversion_coe ADD constraint c_nn_coe_org_code check (coe_org_code IS NOT NULL);
alter table iav.tj_coefficientconversion_coe add constraint c_pk_coe PRIMARY KEY (coe_tax_code, coe_std_code, coe_qte_code, coe_date_debut, coe_date_fin,coe_org_code);
ALTER TABLE iav.tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code) REFERENCES ref.ts_organisme_org(org_code);

--modif de la table create_new_schema
--modif sur centraldata
-- existe déjà en localhost

-- modif pour le schema bgm
alter table bgm.tj_coefficientconversion_coe  drop CONSTRAINT c_pk_coe;
alter table bgm.tj_coefficientconversion_coe add coe_org_code character varying(30) NOT NULL;
alter table bgm.tj_coefficientconversion_coe add constraint c_pk_coe PRIMARY KEY (coe_tax_code, coe_std_code, coe_qte_code, coe_date_debut, coe_date_fin,coe_org_code);
ALTER TABLE bgm.tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code) REFERENCES ref.ts_organisme_org(org_code);

alter table charente.tj_coefficientconversion_coe  drop CONSTRAINT c_pk_coe;
alter table charente.tj_coefficientconversion_coe add coe_org_code character varying(30) NOT NULL;
alter table charente.tj_coefficientconversion_coe add constraint c_pk_coe PRIMARY KEY (coe_tax_code, coe_std_code, coe_qte_code, coe_date_debut, coe_date_fin,coe_org_code);
ALTER TABLE charente.tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code) REFERENCES ref.ts_organisme_org(org_code);

alter table inra.tj_coefficientconversion_coe  drop CONSTRAINT c_pk_coe;
alter table inra.tj_coefficientconversion_coe add coe_org_code character varying(30) NOT NULL;
alter table inra.tj_coefficientconversion_coe add constraint c_pk_coe PRIMARY KEY (coe_tax_code, coe_std_code, coe_qte_code, coe_date_debut, coe_date_fin,coe_org_code);
ALTER TABLE inra.tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code) REFERENCES ref.ts_organisme_org(org_code);

alter table logrami.tj_coefficientconversion_coe  drop CONSTRAINT c_pk_coe;
alter table logrami.tj_coefficientconversion_coe add coe_org_code character varying(30) NOT NULL;
alter table logrami.tj_coefficientconversion_coe add constraint c_pk_coe PRIMARY KEY (coe_tax_code, coe_std_code, coe_qte_code, coe_date_debut, coe_date_fin,coe_org_code);
ALTER TABLE logrami.tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code) REFERENCES ref.ts_organisme_org(org_code);


alter table migado.tj_coefficientconversion_coe  drop CONSTRAINT c_pk_coe;
alter table migado.tj_coefficientconversion_coe add coe_org_code character varying(30) NOT NULL;
alter table migado.tj_coefficientconversion_coe add constraint c_pk_coe PRIMARY KEY (coe_tax_code, coe_std_code, coe_qte_code, coe_date_debut, coe_date_fin,coe_org_code);
ALTER TABLE migado.tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code) REFERENCES ref.ts_organisme_org(org_code);


alter table migradour.tj_coefficientconversion_coe  drop CONSTRAINT c_pk_coe;
alter table migradour.tj_coefficientconversion_coe add coe_org_code character varying(30) NOT NULL;
alter table migradour.tj_coefficientconversion_coe add constraint c_pk_coe PRIMARY KEY (coe_tax_code, coe_std_code, coe_qte_code, coe_date_debut, coe_date_fin,coe_org_code);
ALTER TABLE migradour.tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code) REFERENCES ref.ts_organisme_org(org_code);

alter table mrm.tj_coefficientconversion_coe  drop CONSTRAINT c_pk_coe;
alter table mrm.tj_coefficientconversion_coe add coe_org_code character varying(30) NOT NULL;
alter table mrm.tj_coefficientconversion_coe add constraint c_pk_coe PRIMARY KEY (coe_tax_code, coe_std_code, coe_qte_code, coe_date_debut, coe_date_fin,coe_org_code);
ALTER TABLE mrm.tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code) REFERENCES ref.ts_organisme_org(org_code);


alter table saumonrhin.tj_coefficientconversion_coe  drop CONSTRAINT c_pk_coe;
alter table saumonrhin.tj_coefficientconversion_coe add coe_org_code character varying(30) NOT NULL;
alter table saumonrhin.tj_coefficientconversion_coe add constraint c_pk_coe PRIMARY KEY (coe_tax_code, coe_std_code, coe_qte_code, coe_date_debut, coe_date_fin,coe_org_code);
ALTER TABLE saumonrhin.tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code) REFERENCES ref.ts_organisme_org(org_code);

 --- t_operationmarquage_omq

alter table iav.t_operationmarquage_omq  drop CONSTRAINT c_pk_omg CASCADE;
alter table iav.t_operationmarquage_omq add omq_org_code character varying(30);
update iav.t_operationmarquage_omq set omq_org_code='IAV';
alter table iav.t_operationmarquage_omq ADD constraint c_nn_omq_org_code check (omq_org_code IS NOT NULL);
alter table iav.t_operationmarquage_omq ADD CONSTRAINT c_pk_omq PRIMARY KEY (omq_reference,omq_org_code);
ALTER TABLE iav.t_operationmarquage_omq ADD CONSTRAINT c_fk_omq_org_code FOREIGN KEY (omq_org_code) REFERENCES ref.ts_organisme_org(org_code);
ALTER TABLE iav.t_marque_mqe ADD CONSTRAINT c_fk_mqe_omq_reference FOREIGN KEY (mqe_omq_reference,mqe_org_code)
      REFERENCES iav.t_operationmarquage_omq (omq_reference,omq_org_code) ;

--modif de la table create_new_schema
--modif sur centraldata
-- existe déjà en localhost



alter table bgm.t_operationmarquage_omq drop CONSTRAINT c_pk_omg CASCADE;
alter table bgm.t_operationmarquage_omq add omq_org_code character varying(30);
alter table bgm.t_operationmarquage_omq ADD CONSTRAINT c_pk_omq PRIMARY KEY (omq_reference,omq_org_code);
ALTER TABLE bgm.t_operationmarquage_omq ADD CONSTRAINT c_fk_omq_org_code FOREIGN KEY (omq_org_code) REFERENCES ref.ts_organisme_org(org_code);
ALTER TABLE bgm.t_marque_mqe ADD CONSTRAINT c_fk_mqe_omq_reference FOREIGN KEY (mqe_omq_reference,mqe_org_code)
      REFERENCES bgm.t_operationmarquage_omq (omq_reference,omq_org_code) ;

      
alter table charente.t_operationmarquage_omq  drop CONSTRAINT c_pk_omg CASCADE;
alter table charente.t_operationmarquage_omq add omq_org_code character varying(30) NOT NULL;
alter table charente.t_operationmarquage_omq ADD CONSTRAINT c_pk_omq PRIMARY KEY (omq_reference,omq_org_code);
ALTER TABLE charente.t_operationmarquage_omq ADD CONSTRAINT c_fk_omq_org_code FOREIGN KEY (omq_org_code) REFERENCES ref.ts_organisme_org(org_code);
ALTER TABLE charente.t_marque_mqe ADD CONSTRAINT c_fk_mqe_omq_reference FOREIGN KEY (mqe_omq_reference,mqe_org_code)
      REFERENCES charente.t_operationmarquage_omq (omq_reference,omq_org_code) ;




alter table inra.t_operationmarquage_omq  drop CONSTRAINT c_pk_omg CASCADE;
alter table inra.t_operationmarquage_omq add omq_org_code character varying(30) NOT NULL;
alter table inra.t_operationmarquage_omq ADD CONSTRAINT c_pk_omq PRIMARY KEY (omq_reference,omq_org_code);
ALTER TABLE inra.t_operationmarquage_omq ADD CONSTRAINT c_fk_omq_org_code FOREIGN KEY (omq_org_code) REFERENCES ref.ts_organisme_org(org_code);
ALTER TABLE inra.t_marque_mqe ADD CONSTRAINT c_fk_mqe_omq_reference FOREIGN KEY (mqe_omq_reference,mqe_org_code)
      REFERENCES inra.t_operationmarquage_omq (omq_reference,omq_org_code) ;

alter table logrami.t_operationmarquage_omq  drop CONSTRAINT c_pk_omg CASCADE;
alter table logrami.t_operationmarquage_omq add omq_org_code character varying(30) NOT NULL;
alter table logrami.t_operationmarquage_omq ADD CONSTRAINT c_pk_omq PRIMARY KEY (omq_reference,omq_org_code);
ALTER TABLE logrami.t_operationmarquage_omq ADD CONSTRAINT c_fk_omq_org_code FOREIGN KEY (omq_org_code) REFERENCES ref.ts_organisme_org(org_code);
ALTER TABLE logrami.t_marque_mqe ADD CONSTRAINT c_fk_mqe_omq_reference FOREIGN KEY (mqe_omq_reference,mqe_org_code)
      REFERENCES logrami.t_operationmarquage_omq (omq_reference,omq_org_code) ;


alter table migado.t_operationmarquage_omq  drop CONSTRAINT c_pk_omg CASCADE;
alter table migado.t_operationmarquage_omq add omq_org_code character varying(30) NOT NULL;
alter table migado.t_operationmarquage_omq ADD CONSTRAINT c_pk_omq PRIMARY KEY (omq_reference,omq_org_code);
ALTER TABLE migado.t_operationmarquage_omq ADD CONSTRAINT c_fk_omq_org_code FOREIGN KEY (omq_org_code) REFERENCES ref.ts_organisme_org(org_code);
ALTER TABLE migado.t_marque_mqe ADD CONSTRAINT c_fk_mqe_omq_reference FOREIGN KEY (mqe_omq_reference,mqe_org_code)
      REFERENCES migado.t_operationmarquage_omq (omq_reference,omq_org_code) ;


alter table migradour.t_operationmarquage_omq  drop CONSTRAINT c_pk_omg CASCADE;
alter table migradour.t_operationmarquage_omq add omq_org_code character varying(30) NOT NULL;
alter table migradour.t_operationmarquage_omq ADD CONSTRAINT c_pk_omq PRIMARY KEY (omq_reference,omq_org_code);
ALTER TABLE migradour.t_operationmarquage_omq ADD CONSTRAINT c_fk_omq_org_code FOREIGN KEY (omq_org_code) REFERENCES ref.ts_organisme_org(org_code);
ALTER TABLE migradour.t_marque_mqe ADD CONSTRAINT c_fk_mqe_omq_reference FOREIGN KEY (mqe_omq_reference,mqe_org_code)
      REFERENCES migradour.t_operationmarquage_omq (omq_reference,omq_org_code) ;

alter table mrm.t_operationmarquage_omq  drop CONSTRAINT c_pk_omg CASCADE;
alter table mrm.t_operationmarquage_omq add omq_org_code character varying(30) NOT NULL;
alter table mrm.t_operationmarquage_omq ADD CONSTRAINT c_pk_omq PRIMARY KEY (omq_reference,omq_org_code);
ALTER TABLE mrm.t_operationmarquage_omq ADD CONSTRAINT c_fk_omq_org_code FOREIGN KEY (omq_org_code) REFERENCES ref.ts_organisme_org(org_code);
ALTER TABLE mrm.t_marque_mqe ADD CONSTRAINT c_fk_mqe_omq_reference FOREIGN KEY (mqe_omq_reference,mqe_org_code)
      REFERENCES mrm.t_operationmarquage_omq (omq_reference,omq_org_code) ;


alter table saumonrhin.t_operationmarquage_omq  drop CONSTRAINT c_pk_omg CASCADE;
alter table saumonrhin.t_operationmarquage_omq add omq_org_code character varying(30) NOT NULL;
alter table saumonrhin.t_operationmarquage_omq ADD CONSTRAINT c_pk_omq PRIMARY KEY (omq_reference,omq_org_code);
ALTER TABLE saumonrhin.t_operationmarquage_omq ADD CONSTRAINT c_fk_omq_org_code FOREIGN KEY (omq_org_code) REFERENCES ref.ts_organisme_org(org_code);
ALTER TABLE saumonrhin.t_marque_mqe ADD CONSTRAINT c_fk_mqe_omq_reference FOREIGN KEY (mqe_omq_reference,mqe_org_code)
      REFERENCES saumonrhin.t_operationmarquage_omq (omq_reference,omq_org_code) ;


CREATE TABLE iav.ts_maintenance_main(
main_identifiant serial PRIMARY KEY,
main_ticket integer,
main_description text);
COMMENT ON  TABLE iav.ts_maintenance_main IS 'Table de suivi des operations de maintenance de la base';

INSERT INTO iav.ts_maintenance_main( main_ticket,main_description ) VALUES (59,'creation de la table de maintenance');
INSERT INTO iav.ts_maintenance_main( main_ticket,main_description ) VALUES (40,'ajout des clé étrangères manquantes');
INSERT INTO iav.ts_maintenance_main( main_ticket,main_description ) VALUES (42,'modification des propriétaires sur les tables à séquence et grant select sur ref.tr_typedf_tdf oublié');
INSERT INTO iav.ts_maintenance_main( main_ticket,main_description ) VALUES (67,'org code rajouté dans les tables t_operationmarquage_omq, tj_coefficientconversion_coe,tj_prelevementlot_prl');


CREATE TABLE bgm.ts_maintenance_main(
main_identifiant serial PRIMARY KEY,
main_ticket integer,
main_description text);
COMMENT ON  TABLE bgm.ts_maintenance_main IS 'Table de suivi des operations de maintenance de la base';

INSERT INTO bgm.ts_maintenance_main( main_ticket,main_description ) VALUES (59,'creation de la table de maintenance');
INSERT INTO bgm.ts_maintenance_main( main_ticket,main_description ) VALUES (40,'ajout des clé étrangères manquantes');
INSERT INTO bgm.ts_maintenance_main( main_ticket,main_description ) VALUES (42,'modification des propriétaires sur les tables à séquence et grant select sur ref.tr_typedf_tdf oublié');
INSERT INTO bgm.ts_maintenance_main( main_ticket,main_description ) VALUES (67,'org code rajouté dans les tables t_operationmarquage_omq, tj_coefficientconversion_coe,tj_prelevementlot_prl');



CREATE TABLE inra.ts_maintenance_main(
main_identifiant serial PRIMARY KEY,
main_ticket integer,
main_description text);
COMMENT ON  TABLE inra.ts_maintenance_main IS 'Table de suivi des operations de maintenance de la base';

INSERT INTO inra.ts_maintenance_main( main_ticket,main_description ) VALUES (59,'creation de la table de maintenance');
INSERT INTO inra.ts_maintenance_main( main_ticket,main_description ) VALUES (40,'ajout des clé étrangères manquantes');
INSERT INTO inra.ts_maintenance_main( main_ticket,main_description ) VALUES (42,'modification des propriétaires sur les tables à séquence et grant select sur ref.tr_typedf_tdf oublié');
INSERT INTO inra.ts_maintenance_main( main_ticket,main_description ) VALUES (67,'org code rajouté dans les tables t_operationmarquage_omq, tj_coefficientconversion_coe,tj_prelevementlot_prl');

CREATE TABLE logrami.ts_maintenance_main(
main_identifiant serial PRIMARY KEY,
main_ticket integer,
main_description text);
COMMENT ON  TABLE logrami.ts_maintenance_main IS 'Table de suivi des operations de maintenance de la base';

INSERT INTO logrami.ts_maintenance_main( main_ticket,main_description ) VALUES (59,'creation de la table de maintenance');
INSERT INTO logrami.ts_maintenance_main( main_ticket,main_description ) VALUES (40,'ajout des clé étrangères manquantes');
INSERT INTO logrami.ts_maintenance_main( main_ticket,main_description ) VALUES (42,'modification des propriétaires sur les tables à séquence et grant select sur ref.tr_typedf_tdf oublié');
INSERT INTO logrami.ts_maintenance_main( main_ticket,main_description ) VALUES (67,'org code rajouté dans les tables t_operationmarquage_omq, tj_coefficientconversion_coe,tj_prelevementlot_prl');

CREATE TABLE migado.ts_maintenance_main(
main_identifiant serial PRIMARY KEY,
main_ticket integer,
main_description text);
COMMENT ON  TABLE migado.ts_maintenance_main IS 'Table de suivi des operations de maintenance de la base';

INSERT INTO migado.ts_maintenance_main( main_ticket,main_description ) VALUES (59,'creation de la table de maintenance');
INSERT INTO migado.ts_maintenance_main( main_ticket,main_description ) VALUES (40,'ajout des clé étrangères manquantes');
INSERT INTO migado.ts_maintenance_main( main_ticket,main_description ) VALUES (42,'modification des propriétaires sur les tables à séquence et grant select sur ref.tr_typedf_tdf oublié');
INSERT INTO migado.ts_maintenance_main( main_ticket,main_description ) VALUES (67,'org code rajouté dans les tables t_operationmarquage_omq, tj_coefficientconversion_coe,tj_prelevementlot_prl');

CREATE TABLE migradour.ts_maintenance_main(
main_identifiant serial PRIMARY KEY,
main_ticket integer,
main_description text);
COMMENT ON  TABLE migradour.ts_maintenance_main IS 'Table de suivi des operations de maintenance de la base';

INSERT INTO migradour.ts_maintenance_main( main_ticket,main_description ) VALUES (59,'creation de la table de maintenance');
INSERT INTO migradour.ts_maintenance_main( main_ticket,main_description ) VALUES (40,'ajout des clé étrangères manquantes');
INSERT INTO migradour.ts_maintenance_main( main_ticket,main_description ) VALUES (42,'modification des propriétaires sur les tables à séquence et grant select sur ref.tr_typedf_tdf oublié');
INSERT INTO migradour.ts_maintenance_main( main_ticket,main_description ) VALUES (67,'org code rajouté dans les tables t_operationmarquage_omq, tj_coefficientconversion_coe,tj_prelevementlot_prl');

CREATE TABLE mrm.ts_maintenance_main(
main_identifiant serial PRIMARY KEY,
main_ticket integer,
main_description text);
COMMENT ON  TABLE mrm.ts_maintenance_main IS 'Table de suivi des operations de maintenance de la base';

INSERT INTO mrm.ts_maintenance_main( main_ticket,main_description ) VALUES (59,'creation de la table de maintenance');
INSERT INTO mrm.ts_maintenance_main( main_ticket,main_description ) VALUES (40,'ajout des clé étrangères manquantes');
INSERT INTO mrm.ts_maintenance_main( main_ticket,main_description ) VALUES (42,'modification des propriétaires sur les tables à séquence et grant select sur ref.tr_typedf_tdf oublié');
INSERT INTO mrm.ts_maintenance_main( main_ticket,main_description ) VALUES (67,'org code rajouté dans les tables t_operationmarquage_omq, tj_coefficientconversion_coe,tj_prelevementlot_prl');

CREATE TABLE saumonrhin.ts_maintenance_main(
main_identifiant serial PRIMARY KEY,
main_ticket integer,
main_description text);
COMMENT ON  TABLE saumonrhin.ts_maintenance_main IS 'Table de suivi des operations de maintenance de la base';

INSERT INTO saumonrhin.ts_maintenance_main( main_ticket,main_description ) VALUES (59,'creation de la table de maintenance');
INSERT INTO saumonrhin.ts_maintenance_main( main_ticket,main_description ) VALUES (40,'ajout des clé étrangères manquantes');
INSERT INTO saumonrhin.ts_maintenance_main( main_ticket,main_description ) VALUES (42,'modification des propriétaires sur les tables à séquence et grant select sur ref.tr_typedf_tdf oublié');
INSERT INTO saumonrhin.ts_maintenance_main( main_ticket,main_description ) VALUES (67,'org code rajouté dans les tables t_operationmarquage_omq, tj_coefficientconversion_coe,tj_prelevementlot_prl');

