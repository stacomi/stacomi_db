CREATE TABLE ref.ts_sequence_seq (
    seq_sequence character varying(60) NOT NULL,
    seq_table character varying(40),
    seq_column character varying(40)
);


ALTER TABLE ref.ts_sequence_seq OWNER TO postgres;


ALTER TABLE ONLY ref.ts_sequence_seq
    ADD CONSTRAINT c_pk_seq PRIMARY KEY (seq_sequence);


--
-- TOC entry 2006 (class 0 OID 0)
-- Dependencies: 1698
-- Name: ts_sequence_seq; Type: ACL; Schema: iav; Owner: postgres
--

REVOKE ALL ON TABLE ref.ts_sequence_seq FROM PUBLIC;
REVOKE ALL ON TABLE ref.ts_sequence_seq FROM postgres;
GRANT ALL ON TABLE ref.ts_sequence_seq TO postgres;
GRANT SELECT ON ref.ts_sequence_seq TO iav;
GRANT SELECT ON TABLE ref.ts_sequence_seq TO invite;