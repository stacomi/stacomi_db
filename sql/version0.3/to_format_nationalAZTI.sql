-- in bd_contmig_nat

INSERT INTO ref.ts_organisme_org VALUES ('AZTI','research institute AZTI technalia');
-- creation de la table organisme
CREATE TABLE ts_organisme_org
(
	org_code varchar(30) NOT NULL,
	org_description text,	
	CONSTRAINT c_pk_org PRIMARY KEY (org_code),
	CONSTRAINT c_uq_org UNIQUE (org_code)
)
WITH (OIDS=TRUE);
-- DC
ALTER TABLE t_dispositifcomptage_dic ADD COLUMN dic_org_code varchar(30);
ALTER TABLE t_dispositifcomptage_dic ADD CONSTRAINT c_fk_dic_org_code FOREIGN KEY (dic_org_code) REFERENCES ts_organisme_org (org_code) MATCH FULL;
UPDATE t_dispositifcomptage_dic SET dic_org_code = 'AZTI';
ALTER TABLE t_dispositifcomptage_dic DROP CONSTRAINT c_pk_dic CASCADE;
ALTER TABLE t_dispositifcomptage_dic ADD CONSTRAINT c_pk_dic PRIMARY KEY (dic_code, dic_org_code);	
ALTER TABLE t_dispositifcomptage_dic ALTER COLUMN dic_org_code SET NOT NULL;
-- TODO modifier c_fk_dic_dif_identifiant -> (dif_identifiant,dif_org_code)

-- DF
ALTER TABLE t_dispositiffranchissement_dif ADD COLUMN dif_org_code varchar(30);
ALTER TABLE t_dispositiffranchissement_dif ADD CONSTRAINT c_fk_dif_org_code FOREIGN KEY (dif_org_code) REFERENCES ts_organisme_org (org_code) MATCH FULL;
UPDATE t_dispositiffranchissement_dif SET dif_org_code = 'AZTI';
ALTER TABLE t_dispositiffranchissement_dif DROP CONSTRAINT c_pk_dif CASCADE;
ALTER TABLE t_dispositiffranchissement_dif ADD CONSTRAINT c_pk_dif PRIMARY KEY (dif_code, dif_org_code);	
ALTER TABLE t_dispositiffranchissement_dif ALTER COLUMN dif_org_code SET NOT NULL;
-- TODO modifier c_fk_dif_ouv_identifiant -> (dif_identifiant,ouv_org_code)

-- ouvrage	
ALTER TABLE t_ouvrage_ouv ADD COLUMN ouv_org_code varchar(30);
ALTER TABLE t_ouvrage_ouv ADD CONSTRAINT c_fk_ouv_org_code FOREIGN KEY (ouv_org_code) REFERENCES ts_organisme_org (org_code) MATCH FULL;
UPDATE t_ouvrage_ouv SET ouv_org_code = 'AZTI';
ALTER TABLE t_ouvrage_ouv DROP CONSTRAINT c_pk_ouv CASCADE; -- cl� primaire modifi�e
ALTER TABLE t_ouvrage_ouv ADD CONSTRAINT c_pk_ouv PRIMARY KEY (ouv_identifiant, ouv_org_code);	
ALTER TABLE t_ouvrage_ouv ALTER COLUMN ouv_org_code SET NOT NULL; -- alter table set organisme NOT NULL
-- TODO modifier c_fk_ouv_sta_code -> (sta_code,sta_org_code)


-- station
ALTER TABLE t_station_sta ADD COLUMN sta_org_code varchar(30);
ALTER TABLE t_station_sta ADD CONSTRAINT c_fk_sta_org_code FOREIGN KEY (sta_org_code) REFERENCES ts_organisme_org (org_code) MATCH FULL;
UPDATE t_station_sta SET sta_org_code = 'AZTI';
ALTER TABLE t_station_sta DROP CONSTRAINT c_pk_sta CASCADE; -- cl� primaire modifi�e
ALTER TABLE t_station_sta ADD CONSTRAINT c_pk_sta PRIMARY KEY (sta_code, sta_org_code);	
ALTER TABLE t_station_sta ALTER COLUMN sta_org_code SET NOT NULL; -- alter table set organisme NOT NULL


-- dispositif
ALTER TABLE tg_dispositif_dis ADD COLUMN dis_org_code varchar(30);
ALTER TABLE tg_dispositif_dis ADD CONSTRAINT c_fk_sta_org_code FOREIGN KEY (dis_org_code) REFERENCES ts_organisme_org (org_code) MATCH FULL;
UPDATE tg_dispositif_dis SET dis_org_code = 'AZTI';
ALTER TABLE tg_dispositif_dis DROP CONSTRAINT c_pk_dis CASCADE; -- cl� primaire modifi�e
ALTER TABLE tg_dispositif_dis ADD CONSTRAINT c_pk_dis PRIMARY KEY (dis_identifiant, dis_org_code);	
ALTER TABLE tg_dispositif_dis ALTER COLUMN dis_org_code SET NOT NULL;

-- operation	
ALTER TABLE t_operation_ope ADD COLUMN ope_org_code varchar(30);
ALTER TABLE t_operation_ope ADD CONSTRAINT c_fk_ope_org_code FOREIGN KEY (ope_org_code) REFERENCES ts_organisme_org (org_code) MATCH FULL;
DROP TRIGGER trg_ope_date ON t_operation_ope; -- on enl�ve le trigger qui est trop long pour la mise a jour !
UPDATE t_operation_ope SET ope_org_code = 'AZTI';
CREATE TRIGGER trg_ope_date -- on remet le trigger
  AFTER INSERT OR UPDATE
  ON t_operation_ope
  FOR EACH ROW
  EXECUTE PROCEDURE fct_ope_date();
ALTER TABLE t_operation_ope DROP CONSTRAINT c_pk_ope CASCADE; -- cl� primaire modifi�e
ALTER TABLE t_operation_ope ADD CONSTRAINT c_pk_ope PRIMARY KEY (ope_identifiant, ope_org_code);	
ALTER TABLE t_operation_ope ALTER COLUMN ope_org_code SET NOT NULL; -- alter table set organisme NOT NULL

-- lot	
ALTER TABLE t_lot_lot ADD COLUMN lot_org_code varchar(30);
ALTER TABLE t_lot_lot ADD CONSTRAINT c_fk_lot_org_code FOREIGN KEY (lot_org_code) REFERENCES ts_organisme_org (org_code) MATCH FULL;
UPDATE t_lot_lot SET lot_org_code = 'AZTI';
ALTER TABLE t_lot_lot DROP CONSTRAINT c_pk_lot CASCADE; -- cl� primaire modifi�e
ALTER TABLE t_lot_lot ADD CONSTRAINT c_pk_lot PRIMARY KEY (lot_identifiant, lot_org_code);	
ALTER TABLE t_lot_lot ALTER COLUMN lot_org_code SET NOT NULL;

-- DF est type
ALTER TABLE tj_dfesttype_dft ADD COLUMN dft_org_code varchar(30);
ALTER TABLE tj_dfesttype_dft ADD CONSTRAINT c_fk_dft_org_code FOREIGN KEY (dft_org_code) REFERENCES ts_organisme_org (org_code) MATCH FULL;
UPDATE tj_dfesttype_dft SET dft_org_code = 'AZTI';
ALTER TABLE tj_dfesttype_dft DROP CONSTRAINT c_pk_dft CASCADE; -- cl� primaire modifi�e
ALTER TABLE tj_dfesttype_dft ADD CONSTRAINT c_pk_dft PRIMARY KEY (dft_df_identifiant, dft_tdf_code, dft_rang, dft_org_code);	
ALTER TABLE tj_dfesttype_dft ALTER COLUMN dft_org_code SET NOT NULL;

-- DF est destine a
ALTER TABLE tj_dfestdestinea_dtx ADD COLUMN dtx_org_code varchar(30);
ALTER TABLE tj_dfestdestinea_dtx ADD CONSTRAINT c_fk_dtx_org_code FOREIGN KEY (dtx_org_code) REFERENCES ts_organisme_org (org_code) MATCH FULL;
UPDATE tj_dfestdestinea_dtx SET dtx_org_code = 'AZTI';
ALTER TABLE tj_dfestdestinea_dtx DROP CONSTRAINT c_pk_dtx CASCADE; -- cl� primaire modifi�e
ALTER TABLE tj_dfestdestinea_dtx ADD CONSTRAINT c_pk_dtx PRIMARY KEY (dtx_dif_identifiant, dtx_tax_code, dtx_org_code);	
ALTER TABLE tj_dfestdestinea_dtx ALTER COLUMN dtx_org_code SET NOT NULL;

-- p�riode de fonctionnement
ALTER TABLE t_periodefonctdispositif_per ADD COLUMN per_org_code varchar(30);
ALTER TABLE t_periodefonctdispositif_per ADD CONSTRAINT c_fk_per_org_code FOREIGN KEY (per_org_code) REFERENCES ts_organisme_org (org_code) MATCH FULL;
UPDATE t_periodefonctdispositif_per SET per_org_code = 'AZTI';
ALTER TABLE t_periodefonctdispositif_per DROP CONSTRAINT c_pk_per CASCADE; -- cl� primaire modifi�e
ALTER TABLE t_periodefonctdispositif_per ADD CONSTRAINT c_pk_per PRIMARY KEY (per_dis_identifiant, per_org_code, per_date_debut, per_date_fin);	
ALTER TABLE t_periodefonctdispositif_per ALTER COLUMN per_org_code SET NOT NULL;

-- taux d'�chappement
ALTER TABLE tj_tauxechappement_txe ADD COLUMN txe_org_code varchar(30);
ALTER TABLE tj_tauxechappement_txe ADD CONSTRAINT c_fk_txe_org_code FOREIGN KEY (txe_org_code) REFERENCES ts_organisme_org (org_code) MATCH FULL;
UPDATE tj_tauxechappement_txe SET txe_org_code = 'AZTI';
ALTER TABLE tj_tauxechappement_txe DROP CONSTRAINT c_pk_txe CASCADE; -- cl� primaire modifi�e
ALTER TABLE tj_tauxechappement_txe ADD CONSTRAINT c_pk_txe PRIMARY KEY (txe_ouv_identifiant, txe_org_code, txe_tax_code, txe_std_code, txe_date_debut, txe_date_fin);
ALTER TABLE tj_tauxechappement_txe ALTER COLUMN txe_org_code SET NOT NULL;

-- station mesure *	 
ALTER TABLE tj_stationmesure_stm ADD COLUMN stm_org_code varchar(30);
ALTER TABLE tj_stationmesure_stm ADD CONSTRAINT c_fk_stm_org_code FOREIGN KEY (stm_org_code) REFERENCES ts_organisme_org (org_code) MATCH FULL;
UPDATE tj_stationmesure_stm SET stm_org_code = 'AZTI';
ALTER TABLE tj_stationmesure_stm DROP CONSTRAINT c_pk_stm_code CASCADE; -- cl� primaire modifi�e
ALTER TABLE tj_stationmesure_stm ADD CONSTRAINT c_pk_stm PRIMARY KEY (stm_identifiant, stm_org_code);	
ALTER TABLE tj_stationmesure_stm ALTER COLUMN stm_org_code SET NOT NULL; -- alter table set organisme NOT NULL

-- caract�ristique *
ALTER TABLE tj_caracteristiquelot_car ADD COLUMN car_org_code varchar(30);
ALTER TABLE tj_caracteristiquelot_car ADD CONSTRAINT c_fk_car_org_code FOREIGN KEY (car_org_code) REFERENCES ts_organisme_org (org_code) MATCH FULL;
UPDATE tj_caracteristiquelot_car SET car_org_code = 'AZTI';
ALTER TABLE tj_caracteristiquelot_car DROP CONSTRAINT c_pk_car CASCADE; -- cl� primaire modifi�e
ALTER TABLE tj_caracteristiquelot_car ADD CONSTRAINT c_pk_car PRIMARY KEY (car_lot_identifiant, car_par_code, car_org_code);	
ALTER TABLE tj_caracteristiquelot_car ALTER COLUMN car_org_code SET NOT NULL; -- alter table set organisme NOT NULL

-- condition env *
ALTER TABLE tj_conditionenvironnementale_env ADD COLUMN env_org_code varchar(30);
ALTER TABLE tj_conditionenvironnementale_env ADD CONSTRAINT c_fk_env_org_code FOREIGN KEY (env_org_code) REFERENCES ts_organisme_org (org_code) MATCH FULL;
UPDATE tj_conditionenvironnementale_env SET env_org_code = 'AZTI';
ALTER TABLE tj_conditionenvironnementale_env DROP CONSTRAINT c_pk_env CASCADE; -- cl� primaire modifi�e
ALTER TABLE tj_conditionenvironnementale_env ADD CONSTRAINT c_pk_env PRIMARY KEY (env_stm_identifiant, env_date_debut, env_date_fin, env_org_code);	
ALTER TABLE tj_conditionenvironnementale_env ALTER COLUMN env_org_code SET NOT NULL; -- alter table set organisme NOT NULL

-- coefficient de conversion (inutile car rattach� � un taxon/stade/type quantit�)
/*ALTER TABLE tj_coefficientconversion_coe ADD COLUMN coe_org_code varchar(30);
ALTER TABLE tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code) REFERENCES ts_organisme_org (org_code) MATCH FULL;
UPDATE tj_coefficientconversion_coe SET coe_org_code = 'AZTI';
ALTER TABLE tj_coefficientconversion_coe DROP CONSTRAINT c_pk_coe CASCADE; -- cl� primaire modifi�e
ALTER TABLE tj_coefficientconversion_coe ADD CONSTRAINT c_pk_coe PRIMARY KEY (coe_tax_code, coe_std_code, coe_qte_code, coe_date_debut, coe_date_fin, coe_org_code);	
ALTER TABLE tj_coefficientconversion_coe ALTER COLUMN coe_org_code SET NOT NULL; -- alter table set organisme NOT NULL
-- TODO modifier FK vers 
*/

-- pathologie constat�e 
ALTER TABLE tj_pathologieconstatee_pco ADD COLUMN pco_org_code varchar(30);
ALTER TABLE tj_pathologieconstatee_pco ADD CONSTRAINT c_fk_pco_org_code FOREIGN KEY (pco_org_code) REFERENCES ts_organisme_org (org_code) MATCH FULL;
UPDATE tj_pathologieconstatee_pco SET pco_org_code = 'AZTI';
ALTER TABLE tj_pathologieconstatee_pco DROP CONSTRAINT c_pk_pco CASCADE; -- cl� primaire modifi�e
ALTER TABLE tj_pathologieconstatee_pco ADD CONSTRAINT c_pk_pco PRIMARY KEY (pco_lot_identifiant, pco_pat_code, pco_loc_code, pco_org_code);	
ALTER TABLE tj_pathologieconstatee_pco ALTER COLUMN pco_org_code SET NOT NULL; -- alter table set organisme NOT NULL

-- action de marquage
ALTER TABLE tj_actionmarquage_act ADD COLUMN act_org_code varchar(30);
ALTER TABLE tj_actionmarquage_act ADD CONSTRAINT c_fk_act_org_code FOREIGN KEY (act_org_code) REFERENCES ts_organisme_org (org_code) MATCH FULL;
UPDATE tj_actionmarquage_act SET act_org_code = 'AZTI';
ALTER TABLE tj_actionmarquage_act DROP CONSTRAINT c_pk_act CASCADE; -- cl� primaire modifi�e
ALTER TABLE tj_actionmarquage_act ADD CONSTRAINT c_pk_act PRIMARY KEY (act_lot_identifiant, act_mqe_reference, act_org_code);	
ALTER TABLE tj_actionmarquage_act ALTER COLUMN act_org_code SET NOT NULL; -- alter table set organisme NOT NULL

-- marque
ALTER TABLE t_marque_mqe ADD COLUMN mqe_org_code varchar(30);
ALTER TABLE t_marque_mqe ADD CONSTRAINT c_fk_mqe_org_code FOREIGN KEY (mqe_org_code) REFERENCES ts_organisme_org (org_code) MATCH FULL;
UPDATE t_marque_mqe SET mqe_org_code = 'AZTI';
ALTER TABLE t_marque_mqe DROP CONSTRAINT c_pk_mqe CASCADE; -- cl� primaire modifi�e
ALTER TABLE t_marque_mqe ADD CONSTRAINT c_pk_mqe PRIMARY KEY (mqe_reference, mqe_org_code);	
ALTER TABLE t_marque_mqe ALTER COLUMN mqe_org_code SET NOT NULL; -- alter table set organisme NOT NULL

-- taille vid�o
ALTER TABLE ts_taillevideo_tav ADD COLUMN tav_org_code varchar(30);
ALTER TABLE ts_taillevideo_tav ADD CONSTRAINT c_fk_tav_org_code FOREIGN KEY (tav_org_code) REFERENCES ts_organisme_org (org_code) MATCH FULL;
UPDATE ts_taillevideo_tav SET tav_org_code = 'AZTI';
ALTER TABLE ts_taillevideo_tav DROP CONSTRAINT c_pk_tav CASCADE; -- cl� primaire modifi�e
ALTER TABLE ts_taillevideo_tav ADD CONSTRAINT c_pk_tav PRIMARY KEY (tav_distance, tav_dic_identifiant, tav_org_code);	
ALTER TABLE ts_taillevideo_tav ALTER COLUMN tav_org_code SET NOT NULL; -- alter table set organisme NOT NULL

-- taxon vid�o
ALTER TABLE ts_taxonvideo_txv ADD COLUMN txv_org_code varchar(30);
ALTER TABLE ts_taxonvideo_txv ADD CONSTRAINT c_fk_txv_org_code FOREIGN KEY (txv_org_code) REFERENCES ts_organisme_org (org_code) MATCH FULL;
UPDATE ts_taxonvideo_txv SET txv_org_code = 'AZTI';
ALTER TABLE ts_taxonvideo_txv DROP CONSTRAINT c_pk_txv CASCADE; -- cl� primaire modifi�e
ALTER TABLE ts_taxonvideo_txv ADD CONSTRAINT c_pk_txv PRIMARY KEY (txv_code, txv_org_code);	
ALTER TABLE ts_taxonvideo_txv ALTER COLUMN txv_org_code SET NOT NULL; -- alter table set organisme NOT NULL



-- cl� �trang�re t_operation_ope -> tg_dispositif_dis
ALTER TABLE t_operation_ope ADD CONSTRAINT c_fk_ope_dic_identifiant 
	FOREIGN KEY (ope_dic_identifiant, ope_org_code) REFERENCES tg_dispositif_dis(dis_identifiant, dis_org_code) MATCH SIMPLE
       ON UPDATE NO ACTION ON DELETE NO ACTION;
	
-- cl� �trang�re t_lot_lot -> t_operation_ope
ALTER TABLE t_lot_lot ADD CONSTRAINT c_fk_lot_ope_identifiant 
	FOREIGN KEY (lot_ope_identifiant, lot_org_code) REFERENCES t_operation_ope(ope_identifiant, ope_org_code) MATCH SIMPLE
       ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE t_lot_lot DROP CONSTRAINT c_fk_lot_lot_identifiant;
-- cedric apparemment cette contrainte saute au moment de la modif de la cl� primaire
ALTER TABLE t_lot_lot ADD CONSTRAINT c_fk_lot_lot_identifiant 
	FOREIGN KEY (lot_lot_identifiant, lot_org_code) REFERENCES t_lot_lot(lot_identifiant, lot_org_code) MATCH SIMPLE
       ON UPDATE NO ACTION ON DELETE NO ACTION;

-- cl� �trang�re tj_dfesttype_tdf -> t_dispositiffranchissement_dif
/*ALTER TABLE tj_dfesttype_dft ADD CONSTRAINT c_fk_dft_df_identifiant FOREIGN KEY (dft_df_identifiant, dft_org_code)
      REFERENCES t_dispositiffranchissement_dif (dif_dis_identifiant, dif_org_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;*/

-- cl� �trang�re tj_dfestdestinea_dtx -> t_dispositiffranchissement_dif
/*ALTER TABLE tj_dfestdestinea_dtx ADD CONSTRAINT c_fk_dtx_df_identifiant FOREIGN KEY (dtx_df_identifiant)
      REFERENCES t_dispositiffranchissement_dif (dif_dis_identifiant) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;*/
	  
-- cl� �trang�re periode fonctionnement -> tg_dispositif_dis*
ALTER TABLE t_periodefonctdispositif_per ADD CONSTRAINT c_fk_per_dis_identifiant 
	FOREIGN KEY (per_dis_identifiant, per_org_code) REFERENCES tg_dispositif_dis(dis_identifiant, dis_org_code) MATCH SIMPLE
       ON UPDATE NO ACTION ON DELETE NO ACTION;

-- cl� �trang�re taux �chappement -> t_ouvrage_ouv*
ALTER TABLE tj_tauxechappement_txe ADD CONSTRAINT c_fk_txe_ouv_identifiant 
	FOREIGN KEY (txe_ouv_identifiant, txe_org_code) REFERENCES t_ouvrage_ouv(ouv_identifiant, ouv_org_code) MATCH SIMPLE
       ON UPDATE NO ACTION ON DELETE NO ACTION;
	   
-- cl� �trang�re Station de mesure -> station                              *
ALTER TABLE tj_stationmesure_stm ADD CONSTRAINT c_fk_stm_sta_code 
	FOREIGN KEY (stm_sta_code, stm_org_code) REFERENCES t_station_sta(sta_code, sta_org_code) MATCH SIMPLE
       ON UPDATE NO ACTION ON DELETE NO ACTION;

-- cl� �trang�re caract�ristique -> lot *
ALTER TABLE tj_caracteristiquelot_car ADD CONSTRAINT c_fk_car_lot_identifiant 
	FOREIGN KEY (car_lot_identifiant,car_org_code) REFERENCES t_lot_lot(lot_identifiant,lot_org_code) MATCH SIMPLE
       ON UPDATE NO ACTION ON DELETE NO ACTION;
	   
-- cl� �trang�re condition env -> station de mesure *
ALTER TABLE tj_conditionenvironnementale_env ADD CONSTRAINT c_fk_env_stm_identifiant 
	FOREIGN KEY (env_stm_identifiant,env_org_code) REFERENCES tj_stationmesure_stm(stm_identifiant,stm_org_code) MATCH SIMPLE
       ON UPDATE NO ACTION ON DELETE NO ACTION;

-- cl� �trang�re pathologie constat�e -> lot
ALTER TABLE tj_pathologieconstatee_pco ADD CONSTRAINT c_fk_pco_lot_identifiant 
	FOREIGN KEY (pco_lot_identifiant,pco_org_code) REFERENCES t_lot_lot(lot_identifiant,lot_org_code) MATCH SIMPLE
       ON UPDATE NO ACTION ON DELETE NO ACTION;

-- cl� �trang�re action marquage -> lot
ALTER TABLE tj_actionmarquage_act ADD CONSTRAINT c_fk_act_lot_identifiant 
	FOREIGN KEY (act_lot_identifiant,act_org_code) REFERENCES t_lot_lot(lot_identifiant,lot_org_code) MATCH SIMPLE
       ON UPDATE NO ACTION ON DELETE NO ACTION;

 -- cl� �trang�re action marquage -> marque
ALTER TABLE tj_actionmarquage_act ADD CONSTRAINT c_fk_act_mqe_reference
	FOREIGN KEY (act_mqe_reference,act_org_code) REFERENCES t_marque_mqe(mqe_reference,mqe_org_code) MATCH SIMPLE
       ON UPDATE NO ACTION ON DELETE NO ACTION;

/*
-- taille vid�o
ALTER TABLE ts_taillevideo_tav ADD CONSTRAINT c_fk_act_mqe_reference
	FOREIGN KEY (act_mqe_reference,act_org_code) REFERENCES t_marque_mqe(mqe_reference,mqe_org_code) MATCH SIMPLE
       ON UPDATE NO ACTION ON DELETE NO ACTION;
-- taxon vid�o
ALTER TABLE tj_actionmarquage_act ADD CONSTRAINT c_fk_act_mqe_reference
	FOREIGN KEY (act_mqe_reference,act_org_code) REFERENCES t_marque_mqe(mqe_reference,mqe_org_code) MATCH SIMPLE
       ON UPDATE NO ACTION ON DELETE NO ACTION;
*/

-- t_bilanmigrationjournalier_bjo
ALTER TABLE t_bilanmigrationjournalier_bjo ADD COLUMN bjo_org_code varchar(30);
ALTER TABLE t_bilanmigrationjournalier_bjo ADD CONSTRAINT c_fk_lot_org_code FOREIGN KEY (bjo_org_code) REFERENCES ts_organisme_org (org_code) MATCH FULL;
UPDATE t_bilanmigrationjournalier_bjo SET bjo_org_code = 'AZTI';
ALTER TABLE t_bilanmigrationjournalier_bjo DROP CONSTRAINT c_pk_bjo; -- cl� primaire modifi�e
ALTER TABLE t_bilanmigrationjournalier_bjo ADD CONSTRAINT c_pk_bjo PRIMARY KEY (bjo_identifiant, bjo_org_code);	
ALTER TABLE t_bilanmigrationjournalier_bjo ALTER COLUMN bjo_org_code SET NOT NULL; -- alter table set organisme NOT NULL

-- t_bilanmigrationmensuel_bme
ALTER TABLE t_bilanmigrationmensuel_bme ADD COLUMN bme_org_code varchar(30);
ALTER TABLE t_bilanmigrationmensuel_bme ADD CONSTRAINT c_fk_lot_org_code FOREIGN KEY (bme_org_code) REFERENCES ts_organisme_org (org_code) MATCH FULL;
UPDATE t_bilanmigrationmensuel_bme SET bme_org_code = 'AZTI';
ALTER TABLE t_bilanmigrationmensuel_bme DROP CONSTRAINT c_pk_bme; -- cl� primaire modifi�e
ALTER TABLE t_bilanmigrationmensuel_bme ADD CONSTRAINT c_pk_bme PRIMARY KEY (bme_identifiant, bme_org_code);	
ALTER TABLE t_bilanmigrationmensuel_bme ALTER COLUMN bme_org_code SET NOT NULL; -- alter table set organisme NOT NULL

/* 
 * A ce stade il faut sauvegarder le sch�ma
 * puis :modification du nom du schema
 */
ALTER SCHEMA public rename to AZTI

/*
 * restauration du sch�ma public
 * puis copie dans le sch�ma ref
 * a la place de faire �a on pourra directement utiliser le sh�ma ref de la base de l'AZTI
 */

ALTER SCHEMA public rename to ref

/* 
 * travail sur le schema ref
 * 
 */

DELETE FROM 
DROP TABLE ref.tj_caracteristiquelot_car CASCADE;
DROP TABLE ref.t_operationmarquage_omq CASCADE;
DROP TABLE ref.t_marque_mqe CASCADE;
DROP TABLE ref.tj_actionmarquage_act CASCADE;
DROP TABLE ref.tj_coefficientconversion_coe CASCADE;
DROP TABLE ref.t_lot_lot CASCADE;
DROP TABLE ref.t_operation_ope CASCADE;
DROP TABLE ref.tj_conditionenvironnementale_env CASCADE;
DROP TABLE ref.t_bilanmigrationjournalier_bjo;
DROP TABLE ref.t_bilanmigrationmensuel_bme;
DROP TABLE ref.tj_pathologieconstatee_pco CASCADE;
DROP TABLE ref.tj_prelevementlot_prl CASCADE;
DROP TABLE ref.tj_stationmesure_stm;
DROP TABLE ref.tj_tauxechappement_txe;
DROP TABLE ref.t_periodefonctdispositif_per;
DROP TABLE ref.tj_dfestdestinea_dtx;
DROP TABLE ref.tj_dfesttype_dft;
DROP TABLE ref.t_dispositifcomptage_dic;
DROP TABLE ref.t_dispositiffranchissement_dif;
DROP TABLE  ref.tg_dispositif_dis;
DROP TABLE ref.t_ouvrage_ouv;
DROP TABLE ref.t_station_sta;
DROP TABLE ref.ts_organisme_org;
DROP TABLE ref.ts_taillevideo_tav;
DROP TABLE ref.ts_taxonvideo_txv CASCADE;

-- pour r�cup�rer les noms de toutes les table
select relname from pg_stat_user_tables WHERE schemaname='ref'
select relname from pg_stat_user_tables WHERE schemaname='AZTI'
-- supression des tables inutiles dans le sch�ma AZTI

DROP TABLE AZTI.ts_description_donnee_dsc CASCADE; -- celle l� a peut �tre d�j� �t� supprim�e, elle ne sert pas
DROP TABLE AZTI.tg_parametre_par CASCADE;
DROP TABLE AZTI.tr_natureouvrage_nov CASCADE;
DROP TABLE AZTI.tr_naturemarque_nmq CASCADE;
DROP TABLE AZTI.tr_typearretdisp_tar CASCADE;
DROP TABLE AZTI.tr_niveautaxonomique_ntx CASCADE;
DROP TABLE AZTI.tr_parametrequantitatif_qan CASCADE;
DROP TABLE AZTI.tr_localisationanatomique_loc CASCADE;
DROP TABLE AZTI.tr_valeurparametrequalitatif_val CASCADE;
DROP TABLE AZTI.tr_typedc_tdc CASCADE;
DROP TABLE AZTI.tr_parametrequalitatif_qal CASCADE;
DROP TABLE AZTI.tr_devenirlot_dev CASCADE;
DROP TABLE AZTI.tr_stadedeveloppement_std CASCADE;
DROP TABLE AZTI.tr_niveauechappement_ech CASCADE;
DROP TABLE AZTI.tr_prelevement_pre CASCADE;
DROP TABLE AZTI.tr_taxon_tax CASCADE;
DROP TABLE AZTI.tr_typequantitelot_qte CASCADE;
DROP TABLE AZTI.tr_pathologie_pat CASCADE;
DROP TABLE AZTI.ts_organisme_org CASCADE;
DROP TABLE AZTI.ts_sequence_seq CASCADE;


--ALTER TABLE AZTI.t_bilanmigrationjournalier_bjo DROP CONSTRAINT c_fk_bjo_std_code;
ALTER TABLE AZTI.t_bilanmigrationjournalier_bjo ADD CONSTRAINT c_fk_bjo_std_code FOREIGN KEY (bjo_std_code)
      REFERENCES ref.tr_stadedeveloppement_std (std_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.t_bilanmigrationjournalier_bjo DROP CONSTRAINT c_fk_bjo_tax_code;
ALTER TABLE AZTI.t_bilanmigrationjournalier_bjo ADD CONSTRAINT c_fk_bjo_tax_code FOREIGN KEY (bjo_tax_code)
      REFERENCES ref.tr_taxon_tax (tax_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION

--ALTER TABLE AZTI.t_bilanmigrationmensuel_bme DROP CONSTRAINT c_fk_bme_std_code;
ALTER TABLE AZTI.t_bilanmigrationmensuel_bme ADD CONSTRAINT c_fk_bme_std_code FOREIGN KEY (bme_std_code)
      REFERENCES ref.tr_stadedeveloppement_std (std_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.t_bilanmigrationmensuel_bme DROP CONSTRAINT c_fk_bme_tax_code;
ALTER TABLE AZTI.t_bilanmigrationmensuel_bme ADD CONSTRAINT c_fk_bme_tax_code FOREIGN KEY (bme_tax_code)
      REFERENCES ref.tr_taxon_tax (tax_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE AZTI.t_dispositifcomptage_dic DROP CONSTRAINT c_fk_dic_org_code;
ALTER TABLE AZTI.t_dispositifcomptage_dic ADD CONSTRAINT c_fk_dic_org_code FOREIGN KEY (dic_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.t_dispositifcomptage_dic DROP CONSTRAINT c_fk_dic_tdc_code;
ALTER TABLE AZTI.t_dispositifcomptage_dic ADD CONSTRAINT c_fk_dic_tdc_code FOREIGN KEY (dic_tdc_code)
      REFERENCES ref.tr_typedc_tdc (tdc_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.t_dispositiffranchissement_dif DROP CONSTRAINT c_fk_dif_org_code;
ALTER TABLE AZTI.t_dispositiffranchissement_dif ADD CONSTRAINT c_fk_dif_org_code FOREIGN KEY (dif_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.t_lot_lot DROP CONSTRAINT c_fk_lot_dev_code;
ALTER TABLE AZTI.t_lot_lot ADD CONSTRAINT c_fk_lot_dev_code FOREIGN KEY (lot_dev_code)
      REFERENCES ref.tr_devenirlot_dev (dev_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.t_lot_lot DROP CONSTRAINT c_fk_lot_org_code;
ALTER TABLE AZTI.t_lot_lot ADD CONSTRAINT c_fk_lot_org_code FOREIGN KEY (lot_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.t_lot_lot DROP CONSTRAINT c_fk_lot_qte_code;
ALTER TABLE AZTI.t_lot_lot ADD CONSTRAINT c_fk_lot_qte_code FOREIGN KEY (lot_qte_code)
      REFERENCES ref.tr_typequantitelot_qte (qte_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.t_lot_lot DROP CONSTRAINT c_fk_lot_std_code;
ALTER TABLE AZTI.t_lot_lot ADD CONSTRAINT c_fk_lot_std_code FOREIGN KEY (lot_std_code)
      REFERENCES ref.tr_stadedeveloppement_std (std_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.t_lot_lot DROP CONSTRAINT c_fk_lot_tax_code;
ALTER TABLE AZTI.t_lot_lot ADD CONSTRAINT c_fk_lot_tax_code FOREIGN KEY (lot_tax_code)
      REFERENCES ref.tr_taxon_tax (tax_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;


--ALTER TABLE AZTI.t_marque_mqe DROP CONSTRAINT c_fk_mqe_loc_code;
ALTER TABLE AZTI.t_marque_mqe ADD CONSTRAINT  c_fk_mqe_loc_code FOREIGN KEY (mqe_loc_code)
      REFERENCES ref.tr_localisationanatomique_loc (loc_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.t_marque_mqe DROP CONSTRAINT c_fk_mqe_nmq_code;
ALTER TABLE AZTI.t_marque_mqe ADD CONSTRAINT  c_fk_mqe_nmq_code FOREIGN KEY (mqe_nmq_code)
      REFERENCES ref.tr_naturemarque_nmq (nmq_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.t_marque_mqe DROP CONSTRAINT c_fk_mqe_org_code;
ALTER TABLE AZTI.t_marque_mqe ADD CONSTRAINT c_fk_mqe_org_code FOREIGN KEY (mqe_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--#
--ALTER TABLE AZTI.t_operation_ope DROP CONSTRAINT c_fk_ope_org_code;
ALTER TABLE AZTI.t_operation_ope ADD CONSTRAINT c_fk_ope_org_code FOREIGN KEY (ope_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE AZTI.t_ouvrage_ouv DROP CONSTRAINT c_fk_ouv_nov_code;
ALTER TABLE AZTI.t_ouvrage_ouv ADD CONSTRAINT c_fk_ouv_nov_code FOREIGN KEY (ouv_nov_code)
      REFERENCES ref.tr_natureouvrage_nov (nov_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.t_ouvrage_ouv DROP CONSTRAINT c_fk_ouv_org_code;
ALTER TABLE AZTI.t_ouvrage_ouv ADD CONSTRAINT c_fk_ouv_org_code FOREIGN KEY (ouv_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.t_periodefonctdispositif_per DROP CONSTRAINT c_fk_per_org_code;
ALTER TABLE AZTI.t_periodefonctdispositif_per ADD CONSTRAINT c_fk_per_org_code FOREIGN KEY (per_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.t_periodefonctdispositif_per DROP CONSTRAINT c_fk_per_tar_code;
ALTER TABLE AZTI.t_periodefonctdispositif_per ADD CONSTRAINT c_fk_per_tar_code FOREIGN KEY (per_tar_code)
      REFERENCES ref.tr_typearretdisp_tar (tar_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.t_station_sta DROP CONSTRAINT c_fk_sta_org_code;
ALTER TABLE AZTI.t_station_sta ADD CONSTRAINT c_fk_sta_org_code FOREIGN KEY (sta_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.tg_dispositif_dis DROP CONSTRAINT c_fk_sta_org_code;
ALTER TABLE AZTI.tg_dispositif_dis ADD CONSTRAINT c_fk_sta_org_code FOREIGN KEY (dis_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.tj_actionmarquage_act DROP CONSTRAINT c_fk_act_org_code;
ALTER TABLE AZTI.tj_actionmarquage_act ADD CONSTRAINT c_fk_act_org_code FOREIGN KEY (act_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.tj_caracteristiquelot_car DROP CONSTRAINT c_fk_car_org_code;
ALTER TABLE AZTI.tj_caracteristiquelot_car ADD CONSTRAINT c_fk_car_org_code FOREIGN KEY (car_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.tj_caracteristiquelot_car DROP CONSTRAINT c_fk_car_par_code;
ALTER TABLE AZTI.tj_caracteristiquelot_car ADD CONSTRAINT c_fk_car_par_code FOREIGN KEY (car_par_code)
      REFERENCES ref.tg_parametre_par (par_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.tj_caracteristiquelot_car DROP CONSTRAINT c_fk_car_val_identifiant;
ALTER TABLE AZTI.tj_caracteristiquelot_car ADD CONSTRAINT c_fk_car_val_identifiant FOREIGN KEY (car_val_identifiant)
      REFERENCES ref.tr_valeurparametrequalitatif_val (val_identifiant) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.tj_coefficientconversion_coe DROP CONSTRAINT c_fk_coe_qte_code;
ALTER TABLE AZTI.tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_qte_code FOREIGN KEY (coe_qte_code)
      REFERENCES ref.tr_typequantitelot_qte (qte_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.tj_coefficientconversion_coe DROP CONSTRAINT c_fk_coe_std_code;
ALTER TABLE AZTI.tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_std_code FOREIGN KEY (coe_std_code)
      REFERENCES ref.tr_stadedeveloppement_std (std_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.tj_coefficientconversion_coe DROP CONSTRAINT c_fk_coe_tax_code;
ALTER TABLE AZTI.tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_tax_code FOREIGN KEY (coe_tax_code)
      REFERENCES ref.tr_taxon_tax (tax_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE AZTI.tj_conditionenvironnementale_env DROP CONSTRAINT c_fk_env_org_code;
ALTER TABLE AZTI.tj_conditionenvironnementale_env ADD CONSTRAINT c_fk_env_org_code FOREIGN KEY (env_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.tj_conditionenvironnementale_env DROP CONSTRAINT c_fk_env_val_identifiant;
ALTER TABLE AZTI.tj_conditionenvironnementale_env ADD CONSTRAINT c_fk_env_val_identifiant FOREIGN KEY (env_val_identifiant)
      REFERENCES ref.tr_valeurparametrequalitatif_val (val_identifiant) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.tj_dfestdestinea_dtx DROP CONSTRAINT c_fk_dtx_org_code;
ALTER TABLE AZTI.tj_dfestdestinea_dtx ADD CONSTRAINT c_fk_dtx_org_code FOREIGN KEY (dtx_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.tj_dfestdestinea_dtx DROP CONSTRAINT c_fk_dtx_tax_code;
ALTER TABLE AZTI.tj_dfestdestinea_dtx ADD CONSTRAINT c_fk_dtx_tax_code FOREIGN KEY (dtx_tax_code)
      REFERENCES ref.tr_taxon_tax (tax_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.tj_dfesttype_dft DROP CONSTRAINT c_fk_dft_org_code;
ALTER TABLE AZTI.tj_dfesttype_dft ADD CONSTRAINT c_fk_dft_org_code FOREIGN KEY (dft_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.tj_dfesttype_dft DROP CONSTRAINT c_fk_dft_tdf_code;
ALTER TABLE AZTI.tj_dfesttype_dft ADD CONSTRAINT c_fk_dft_tdf_code FOREIGN KEY (dft_tdf_code)
      REFERENCES ref.tr_typedf_tdf (tdf_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.tj_pathologieconstatee_pco DROP CONSTRAINT c_fk_pco_loc_code;
ALTER TABLE AZTI.tj_pathologieconstatee_pco ADD CONSTRAINT c_fk_pco_loc_code FOREIGN KEY (pco_loc_code)
      REFERENCES ref.tr_localisationanatomique_loc (loc_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
      
     
--ALTER TABLE AZTI.tj_pathologieconstatee_pco DROP CONSTRAINT c_fk_pco_org_code;
ALTER TABLE AZTI.tj_pathologieconstatee_pco ADD CONSTRAINT c_fk_pco_org_code FOREIGN KEY (pco_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.tj_pathologieconstatee_pco DROP CONSTRAINT c_fk_pco_pat_code;
ALTER TABLE AZTI.tj_pathologieconstatee_pco ADD CONSTRAINT c_fk_pco_pat_code FOREIGN KEY (pco_pat_code)
      REFERENCES ref.tr_pathologie_pat (pat_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

--####
--ALTER TABLE AZTI.tj_prelevementlot_prl DROP CONSTRAINT c_fk_prl_loc_code;
ALTER TABLE AZTI.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_loc_code FOREIGN KEY (prl_loc_code)
      REFERENCES ref.tr_localisationanatomique_loc (loc_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.tj_prelevementlot_prl DROP CONSTRAINT c_fk_prl_pre_nom;
ALTER TABLE AZTI.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_pre_nom FOREIGN KEY (prl_pre_typeprelevement)
      REFERENCES ref.tr_prelevement_pre (pre_typeprelevement) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.tj_stationmesure_stm DROP CONSTRAINT c_fk_stm_org_code;
ALTER TABLE AZTI.tj_stationmesure_stm ADD CONSTRAINT c_fk_stm_org_code FOREIGN KEY (stm_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.tj_stationmesure_stm DROP CONSTRAINT c_fk_stm_par_code;
ALTER TABLE AZTI.tj_stationmesure_stm ADD CONSTRAINT c_fk_stm_par_code FOREIGN KEY (stm_par_code)
      REFERENCES ref.tg_parametre_par (par_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;





--ALTER TABLE AZTI.tj_tauxechappement_txe DROP CONSTRAINT c_fk_txe_ech_code;
ALTER TABLE AZTI.tj_tauxechappement_txe ADD CONSTRAINT c_fk_txe_ech_code FOREIGN KEY (txe_ech_code)
      REFERENCES ref.tr_niveauechappement_ech (ech_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.tj_tauxechappement_txe DROP CONSTRAINT c_fk_txe_org_code;
ALTER TABLE AZTI.tj_tauxechappement_txe ADD CONSTRAINT c_fk_txe_org_code FOREIGN KEY (txe_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.tj_tauxechappement_txe DROP CONSTRAINT c_fk_txe_std_code;
ALTER TABLE AZTI.tj_tauxechappement_txe ADD CONSTRAINT c_fk_txe_std_code FOREIGN KEY (txe_std_code)
      REFERENCES ref.tr_stadedeveloppement_std (std_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.tj_tauxechappement_txe DROP CONSTRAINT c_fk_txe_tax_code;
ALTER TABLE AZTI.tj_tauxechappement_txe ADD CONSTRAINT c_fk_txe_tax_code FOREIGN KEY (txe_tax_code)
      REFERENCES ref.tr_taxon_tax (tax_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;



--ALTER TABLE AZTI.ts_taillevideo_tav DROP CONSTRAINT c_fk_tav_org_code;
ALTER TABLE AZTI.ts_taillevideo_tav ADD CONSTRAINT c_fk_tav_org_code FOREIGN KEY (tav_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;




--ALTER TABLE AZTI.ts_taxonvideo_txv DROP CONSTRAINT c_fk_std_code;
ALTER TABLE AZTI.ts_taxonvideo_txv ADD CONSTRAINT c_fk_std_code FOREIGN KEY (txv_std_code)
      REFERENCES ref.tr_stadedeveloppement_std (std_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.ts_taxonvideo_txv DROP CONSTRAINT c_fk_txv_org_code;
ALTER TABLE AZTI.ts_taxonvideo_txv ADD CONSTRAINT c_fk_txv_org_code FOREIGN KEY (txv_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE AZTI.ts_taxonvideo_txv DROP CONSTRAINT c_fk_txv_tax_code;
ALTER TABLE AZTI.ts_taxonvideo_txv ADD CONSTRAINT c_fk_txv_tax_code FOREIGN KEY (txv_tax_code)
      REFERENCES ref.tr_taxon_tax (tax_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;


CREATE OR REPLACE VIEW AZTI.v_taxon_tax AS 
 SELECT tax.tax_code, tax.tax_nom_latin, tax.tax_nom_commun, tax.tax_ntx_code, tax.tax_tax_code, tax.tax_rang, txv.txv_code, txv.txv_tax_code, txv.txv_std_code, std.std_code, std.std_libelle, std.std_rang
   FROM ref.tr_taxon_tax tax
   RIGHT JOIN AZTI.ts_taxonvideo_txv txv ON tax.tax_code::text = txv.txv_tax_code::text
   LEFT JOIN ref.tr_stadedeveloppement_std std ON txv.txv_std_code::text = std.std_code::text;
ALTER TABLE AZTI.v_taxon_tax OWNER TO postgres;

/*
CREATE OR REPLACE VIEW AZTI.vue_lot_ope AS 
 SELECT t_operation_ope.ope_identifiant, t_lot_lot.lot_identifiant, t_operation_ope.ope_dic_identifiant, t_lot_lot.lot_lot_identifiant AS lot_pere, t_lot_lot.lot_methode_obtention, t_operation_ope.ope_date_debut, t_operation_ope.ope_date_fin, t_lot_lot.lot_effectif, t_lot_lot.lot_quantite, t_lot_lot.lot_tax_code, tr_taxon_tax.tax_nom_latin, tr_stadedeveloppement_std.std_libelle, tr_devenirlot_dev.dev_code, tr_devenirlot_dev.dev_libelle
   FROM AZTI.t_operation_ope
   JOIN AZTI.t_lot_lot ON t_lot_lot.lot_ope_identifiant = t_operation_ope.ope_identifiant
   LEFT JOIN ref.tr_typequantitelot_qte ON tr_typequantitelot_qte.qte_code::text = t_lot_lot.lot_qte_code::text
   LEFT JOIN ref.tr_devenirlot_dev ON tr_devenirlot_dev.dev_code::text = t_lot_lot.lot_dev_code::text
   JOIN ref.tr_taxon_tax ON tr_taxon_tax.tax_code::text = t_lot_lot.lot_tax_code::text
   JOIN ref.tr_stadedeveloppement_std ON tr_stadedeveloppement_std.std_code::text = t_lot_lot.lot_std_code::text
  ORDER BY t_operation_ope.ope_date_debut;
*/

  CREATE OR REPLACE VIEW AZTI.vue_lot_ope_car AS 
 SELECT t_operation_ope.ope_identifiant, t_lot_lot.lot_identifiant, t_operation_ope.ope_dic_identifiant, t_lot_lot.lot_lot_identifiant AS lot_pere, t_operation_ope.ope_date_debut, t_operation_ope.ope_date_fin, t_lot_lot.lot_effectif, t_lot_lot.lot_quantite, t_lot_lot.lot_tax_code, t_lot_lot.lot_std_code, tr_taxon_tax.tax_nom_latin, tr_stadedeveloppement_std.std_libelle, tr_devenirlot_dev.dev_code, tr_devenirlot_dev.dev_libelle, tg_parametre_par.par_nom, tj_caracteristiquelot_car.car_par_code, tj_caracteristiquelot_car.car_methode_obtention, tj_caracteristiquelot_car.car_val_identifiant, tj_caracteristiquelot_car.car_valeur_quantitatif, tr_valeurparametrequalitatif_val.val_libelle
   FROM AZTI.t_operation_ope
   JOIN AZTI.t_lot_lot ON t_lot_lot.lot_ope_identifiant = t_operation_ope.ope_identifiant
   LEFT JOIN ref.tr_typequantitelot_qte ON tr_typequantitelot_qte.qte_code::text = t_lot_lot.lot_qte_code::text
   LEFT JOIN ref.tr_devenirlot_dev ON tr_devenirlot_dev.dev_code::text = t_lot_lot.lot_dev_code::text
   JOIN ref.tr_taxon_tax ON tr_taxon_tax.tax_code::text = t_lot_lot.lot_tax_code::text
   JOIN ref.tr_stadedeveloppement_std ON tr_stadedeveloppement_std.std_code::text = t_lot_lot.lot_std_code::text
   JOIN AZTI.tj_caracteristiquelot_car ON tj_caracteristiquelot_car.car_lot_identifiant = t_lot_lot.lot_identifiant
   LEFT JOIN ref.tg_parametre_par ON tj_caracteristiquelot_car.car_par_code::text = tg_parametre_par.par_code::text
   LEFT JOIN ref.tr_parametrequalitatif_qal ON tr_parametrequalitatif_qal.qal_par_code::text = tg_parametre_par.par_code::text
   LEFT JOIN ref.tr_valeurparametrequalitatif_val ON tj_caracteristiquelot_car.car_val_identifiant = tr_valeurparametrequalitatif_val.val_identifiant
  ORDER BY t_operation_ope.ope_date_debut;



CREATE OR REPLACE VIEW AZTI.vue_lot_ope_car_qan AS 
 SELECT t_operation_ope.ope_identifiant, t_lot_lot.lot_identifiant, t_operation_ope.ope_dic_identifiant, t_lot_lot.lot_lot_identifiant AS lot_pere, t_operation_ope.ope_date_debut, t_operation_ope.ope_date_fin, t_lot_lot.lot_effectif, t_lot_lot.lot_quantite, t_lot_lot.lot_tax_code, tr_taxon_tax.tax_nom_latin, tr_stadedeveloppement_std.std_libelle, tr_devenirlot_dev.dev_code, tr_devenirlot_dev.dev_libelle, tg_parametre_par.par_nom, tj_caracteristiquelot_car.car_par_code, tj_caracteristiquelot_car.car_methode_obtention, tj_caracteristiquelot_car.car_val_identifiant, tj_caracteristiquelot_car.car_valeur_quantitatif, tr_valeurparametrequalitatif_val.val_libelle
   FROM AZTI.t_operation_ope
   JOIN AZTI.t_lot_lot ON t_lot_lot.lot_ope_identifiant = t_operation_ope.ope_identifiant
   LEFT JOIN ref.tr_typequantitelot_qte ON tr_typequantitelot_qte.qte_code::text = t_lot_lot.lot_qte_code::text
   LEFT JOIN ref.tr_devenirlot_dev ON tr_devenirlot_dev.dev_code::text = t_lot_lot.lot_dev_code::text
   JOIN ref.tr_taxon_tax ON tr_taxon_tax.tax_code::text = t_lot_lot.lot_tax_code::text
   JOIN ref.tr_stadedeveloppement_std ON tr_stadedeveloppement_std.std_code::text = t_lot_lot.lot_std_code::text
   JOIN AZTI.tj_caracteristiquelot_car ON tj_caracteristiquelot_car.car_lot_identifiant = t_lot_lot.lot_identifiant
   LEFT JOIN ref.tg_parametre_par ON tj_caracteristiquelot_car.car_par_code::text = tg_parametre_par.par_code::text
   LEFT JOIN ref.tr_parametrequantitatif_qan ON tr_parametrequantitatif_qan.qan_par_code::text = tg_parametre_par.par_code::text
   LEFT JOIN ref.tr_valeurparametrequalitatif_val ON tj_caracteristiquelot_car.car_val_identifiant = tr_valeurparametrequalitatif_val.val_identifiant
  ORDER BY t_operation_ope.ope_date_debut;





CREATE OR REPLACE VIEW AZTI.vue_ope_lot_ech_parqual AS 
 SELECT t_operation_ope.ope_identifiant, t_operation_ope.ope_dic_identifiant, t_operation_ope.ope_date_debut, t_operation_ope.ope_date_fin, lot.lot_identifiant, lot.lot_methode_obtention, lot.lot_effectif, lot.lot_quantite, lot.lot_tax_code, lot.lot_std_code, tr_taxon_tax.tax_nom_latin, tr_stadedeveloppement_std.std_libelle, dev.dev_code, dev.dev_libelle, par.par_nom, car.car_par_code, car.car_methode_obtention, car.car_val_identifiant, val.val_libelle, lot.lot_lot_identifiant AS lot_pere, lot_pere.lot_effectif AS lot_pere_effectif, lot_pere.lot_quantite AS lot_pere_quantite, dev_pere.dev_code AS lot_pere_dev_code, dev_pere.dev_libelle AS lot_pere_dev_libelle, parqual.par_nom AS lot_pere_par_nom, parqual.car_par_code AS lot_pere_par_code, parqual.car_methode_obtention AS lot_pere_car_methode_obtention, parqual.car_val_identifiant AS lot_pere_val_identifiant, parqual.val_libelle AS lot_pere_val_libelle
   FROM AZTI.t_operation_ope
   JOIN AZTI.t_lot_lot lot ON lot.lot_ope_identifiant = t_operation_ope.ope_identifiant
   LEFT JOIN ref.tr_typequantitelot_qte qte ON qte.qte_code::text = lot.lot_qte_code::text
   LEFT JOIN ref.tr_devenirlot_dev dev ON dev.dev_code::text = lot.lot_dev_code::text
   JOIN ref.tr_taxon_tax ON tr_taxon_tax.tax_code::text = lot.lot_tax_code::text
   JOIN ref.tr_stadedeveloppement_std ON tr_stadedeveloppement_std.std_code::text = lot.lot_std_code::text
   JOIN AZTI.tj_caracteristiquelot_car car ON car.car_lot_identifiant = lot.lot_identifiant
   LEFT JOIN ref.tg_parametre_par par ON car.car_par_code::text = par.par_code::text
   JOIN ref.tr_parametrequalitatif_qal qal ON qal.qal_par_code::text = par.par_code::text
   LEFT JOIN ref.tr_valeurparametrequalitatif_val val ON car.car_val_identifiant = val.val_identifiant
   LEFT JOIN AZTI.t_lot_lot lot_pere ON lot_pere.lot_identifiant = lot.lot_lot_identifiant
   LEFT JOIN ref.tr_typequantitelot_qte qte_pere ON qte_pere.qte_code::text = lot_pere.lot_qte_code::text
   LEFT JOIN ref.tr_devenirlot_dev dev_pere ON dev_pere.dev_code::text = lot_pere.lot_dev_code::text
   LEFT JOIN ( SELECT car_pere.car_lot_identifiant, car_pere.car_par_code, car_pere.car_methode_obtention, car_pere.car_val_identifiant, car_pere.car_valeur_quantitatif, car_pere.car_precision, car_pere.car_commentaires, par_pere.par_code, par_pere.par_nom, par_pere.par_unite, par_pere.par_nature, par_pere.par_definition, qal_pere.qal_par_code, qal_pere.qal_valeurs_possibles, val_pere.val_identifiant, val_pere.val_qal_code, val_pere.val_rang, val_pere.val_libelle
   FROM AZTI.tj_caracteristiquelot_car car_pere
   LEFT JOIN ref.tg_parametre_par par_pere ON car_pere.car_par_code::text = par_pere.par_code::text
   JOIN ref.tr_parametrequalitatif_qal qal_pere ON qal_pere.qal_par_code::text = par_pere.par_code::text
   LEFT JOIN ref.tr_valeurparametrequalitatif_val val_pere ON car_pere.car_val_identifiant = val_pere.val_identifiant) parqual ON parqual.car_lot_identifiant = lot_pere.lot_identifiant;



CREATE OR REPLACE VIEW AZTI.vue_ope_lot_ech_parquan AS 
 SELECT t_operation_ope.ope_identifiant, t_operation_ope.ope_dic_identifiant, t_operation_ope.ope_date_debut, t_operation_ope.ope_date_fin, lot.lot_identifiant, lot.lot_methode_obtention, lot.lot_effectif, lot.lot_quantite, lot.lot_tax_code, lot.lot_std_code, tr_taxon_tax.tax_nom_latin, tr_stadedeveloppement_std.std_libelle, dev.dev_code, dev.dev_libelle, par.par_nom, car.car_par_code, car.car_methode_obtention, car.car_valeur_quantitatif, lot.lot_lot_identifiant AS lot_pere, lot_pere.lot_effectif AS lot_pere_effectif, lot_pere.lot_quantite AS lot_pere_quantite, dev_pere.dev_code AS lot_pere_dev_code, dev_pere.dev_libelle AS lot_pere_dev_libelle, parqual.par_nom AS lot_pere_par_nom, parqual.car_par_code AS lot_pere_par_code, parqual.car_methode_obtention AS lot_pere_car_methode_obtention, parqual.car_val_identifiant AS lot_pere_val_identifiant, parqual.val_libelle AS lot_pere_val_libelle
   FROM AZTI.t_operation_ope
   JOIN AZTI.t_lot_lot lot ON lot.lot_ope_identifiant = t_operation_ope.ope_identifiant
   LEFT JOIN ref.tr_typequantitelot_qte qte ON qte.qte_code::text = lot.lot_qte_code::text
   LEFT JOIN ref.tr_devenirlot_dev dev ON dev.dev_code::text = lot.lot_dev_code::text
   JOIN ref.tr_taxon_tax ON tr_taxon_tax.tax_code::text = lot.lot_tax_code::text
   JOIN ref.tr_stadedeveloppement_std ON tr_stadedeveloppement_std.std_code::text = lot.lot_std_code::text
   JOIN AZTI.tj_caracteristiquelot_car car ON car.car_lot_identifiant = lot.lot_identifiant
   LEFT JOIN ref.tg_parametre_par par ON car.car_par_code::text = par.par_code::text
   JOIN ref.tr_parametrequantitatif_qan qan ON qan.qan_par_code::text = par.par_code::text
   LEFT JOIN AZTI.t_lot_lot lot_pere ON lot_pere.lot_identifiant = lot.lot_lot_identifiant
   LEFT JOIN ref.tr_typequantitelot_qte qte_pere ON qte_pere.qte_code::text = lot_pere.lot_qte_code::text
   LEFT JOIN ref.tr_devenirlot_dev dev_pere ON dev_pere.dev_code::text = lot_pere.lot_dev_code::text
   LEFT JOIN ( SELECT car_pere.car_lot_identifiant, car_pere.car_par_code, car_pere.car_methode_obtention, car_pere.car_val_identifiant, car_pere.car_valeur_quantitatif, car_pere.car_precision, car_pere.car_commentaires, par_pere.par_code, par_pere.par_nom, par_pere.par_unite, par_pere.par_nature, par_pere.par_definition, qal_pere.qal_par_code, qal_pere.qal_valeurs_possibles, val_pere.val_identifiant, val_pere.val_qal_code, val_pere.val_rang, val_pere.val_libelle
   FROM AZTI.tj_caracteristiquelot_car car_pere
   LEFT JOIN ref.tg_parametre_par par_pere ON car_pere.car_par_code::text = par_pere.par_code::text
   JOIN ref.tr_parametrequalitatif_qal qal_pere ON qal_pere.qal_par_code::text = par_pere.par_code::text
   LEFT JOIN ref.tr_valeurparametrequalitatif_val val_pere ON car_pere.car_val_identifiant = val_pere.val_identifiant) parqual ON parqual.car_lot_identifiant = lot_pere.lot_identifiant;



CREATE OR REPLACE FUNCTION AZTI.fct_coe_date()
  RETURNS trigger AS
$BODY$   

 	DECLARE nbChevauchements INTEGER ;

 	BEGIN
 	 	-- verification des non-chevauchements pour les taux
 	 	SELECT COUNT(*) INTO nbChevauchements
 	 	FROM   AZTI.tj_coefficientconversion_coe
 	 	WHERE  coe_tax_code = NEW.coe_tax_code
 	 	       AND coe_std_code = NEW.coe_std_code
 	 	       AND coe_qte_code = NEW.coe_qte_code
 	 	       AND (coe_date_debut, coe_date_fin) OVERLAPS (NEW.coe_date_debut, NEW.coe_date_fin)
 	 	;

		-- Comme le trigger est declenche sur AFTER et non pas sur BEFORE, il faut (nbChevauchements > 1) et non pas >0, car l enregistrement a deja ete ajoute, donc il se chevauche avec lui meme, ce qui est normal !
 	 	IF (nbChevauchements > 1) THEN
 	 	 	RAISE EXCEPTION 'Les taux de conversion ne peuvent se chevaucher.'  ;
 	 	END IF  ;

		RETURN NEW ;
 	END  ;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION AZTI.fct_ope_date()
  RETURNS trigger AS
$BODY$   

 	DECLARE disCreation  	 	TIMESTAMP  ;
 	DECLARE disSuppression  	TIMESTAMP  ;
 	DECLARE nbChevauchements 	INTEGER    ;

 	BEGIN
 	 	-- Recuperation des dates du dispositif dans des variables
 	 	SELECT dis_date_creation, dis_date_suppression INTO disCreation, disSuppression
 	 	FROM   AZTI.tg_dispositif_dis
 	 	WHERE  dis_identifiant = NEW.ope_dic_identifiant
 	 	;

 	 	-- verification de la date de debut
 	 	IF ((NEW.ope_date_debut < disCreation) OR (NEW.ope_date_debut > disSuppression)) THEN
 	 	 	RAISE EXCEPTION 'Le debut de l operation doit etre inclus dans la periode d existence du DC.'  ;
 	 	END IF  ;

 	 	-- verification de la date de fin
 	 	IF ((NEW.ope_date_fin < disCreation) OR (NEW.ope_date_fin > disSuppression)) THEN
 	 	 	RAISE EXCEPTION 'La fin de l operation doit etre incluse dans la periode d existence du DC.'  ;
 	 	END IF  ;

 	 	-- verification des non-chevauchements pour les operations du dispositif
 	 	SELECT COUNT(*) INTO nbChevauchements
 	 	FROM   AZTI.t_operation_ope
 	 	WHERE  ope_dic_identifiant = NEW.ope_dic_identifiant 
 	 	       AND (ope_date_debut, ope_date_fin) OVERLAPS (NEW.ope_date_debut, NEW.ope_date_fin)
 	 	;

		-- Comme le trigger est declenche sur AFTER et non pas sur BEFORE, il faut (nbChevauchements > 1) et non pas >0, car l enregistrement a deja ete ajoute, donc il se chevauche avec lui meme, ce qui est normal !
 	 	IF (nbChevauchements > 1) THEN 
 	 		RAISE EXCEPTION 'Les operations ne peuvent se chevaucher.'  ;
 	 	END IF  ;

		RETURN NEW ;
 	END  ;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION AZTI.fct_per_date()
  RETURNS trigger AS
$BODY$ 

 	DECLARE disCreation  	 	TIMESTAMP  ;
 	DECLARE disSuppression  	TIMESTAMP  ;
 	DECLARE nbChevauchements 	INTEGER    ;

 	BEGIN
 	 	-- Recuperation des dates du dispositif dans des variables
 	 	SELECT dis_date_creation, dis_date_suppression INTO disCreation, disSuppression
 	 	FROM   AZTI.tg_dispositif_dis
 	 	WHERE  dis_identifiant = NEW.per_dis_identifiant
 	 	;

 	 	-- verification de la date de debut
 	 	IF ((NEW.per_date_debut < disCreation) OR (NEW.per_date_debut > disSuppression)) THEN
 	 		RAISE EXCEPTION 'Le debut de la periode doit etre inclus dans la periode d existence du dispositif.'  ;
 	 	END IF  ;

 	 	-- verification de la date de fin
 	 	IF ((NEW.per_date_fin < disCreation) OR (NEW.per_date_fin > disSuppression)) THEN
 	 	 	RAISE EXCEPTION 'La fin de la periode doit etre incluse dans la periode d existence du dispositif.'  ;
 	 	END IF  ;

 	 	-- verification des non-chevauchements pour les periodes du dispositif
 	 	SELECT COUNT(*) INTO nbChevauchements
 	 	FROM   AZTI.t_periodefonctdispositif_per
 	 	WHERE  per_dis_identifiant = NEW.per_dis_identifiant 
 	 	       AND (per_date_debut, per_date_fin) OVERLAPS (NEW.per_date_debut, NEW.per_date_fin)
 	 	;

		-- Comme le trigger est declenche sur AFTER et non pas sur BEFORE, il faut (nbChevauchements > 1) et non pas >0, car l enregistrement a deja ete ajoute, donc il se chevauche avec lui meme, ce qui est normal !
 	 	IF (nbChevauchements > 1) THEN
 	 	 	RAISE EXCEPTION 'Les periodes ne peuvent se chevaucher.'  ;
 	 	END IF  ;
		
		RETURN NEW ;
 	END  ;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION AZTI.fct_per_suppression()
  RETURNS trigger AS
$BODY$   

 	BEGIN
 	 	-- La periode precedent celle supprimee est prolongee
 	 	-- jusqu a la fin de la periode supprimee
 	 	UPDATE AZTI.t_periodefonctdispositif_per 
 	 	SET    per_date_fin = OLD.per_date_fin 
 	 	WHERE  per_date_fin= OLD.per_date_debut 
 	 	       AND per_dis_identifiant = OLD.per_dis_identifiant 
 	 	;
		RETURN NEW ;
 	END  ;

$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION AZTI.fct_txe_date()
  RETURNS trigger AS
$BODY$   

 	DECLARE nbChevauchements INTEGER ;

 	BEGIN
 	 	-- verification des non-chevauchements pour les taux
 	 	SELECT COUNT(*) INTO nbChevauchements
 	 	FROM   AZTI.tj_tauxechappement_txe
 	 	WHERE  txe_ouv_identifiant = NEW.txe_ouv_identifiant
 	 	       AND txe_tax_code = NEW.txe_tax_code
 	 	       AND txe_std_code = NEW.txe_std_code
 	 	       AND (txe_date_debut, txe_date_fin) OVERLAPS (NEW.txe_date_debut, NEW.txe_date_fin)
 	 	;

		-- Comme le trigger est declenche sur AFTER et non pas sur BEFORE, il faut (nbChevauchements > 1) et non pas >0, car l enregistrement a deja ete ajoute, donc il se chevauche avec lui meme, ce qui est normal !
 	 	IF (nbChevauchements > 1) THEN
 	 	 	RAISE EXCEPTION 'Les taux d echappement ne peuvent se chevaucher.'  ;
 	 	END IF  ;

		RETURN NEW ;

 	END  ;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION ref.fct_coe_date()
  RETURNS trigger AS
$BODY$   

 	DECLARE nbChevauchements INTEGER ;

 	BEGIN
 	 	-- verification des non-chevauchements pour les taux
 	 	SELECT COUNT(*) INTO nbChevauchements
 	 	FROM   tj_coefficientconversion_coe
 	 	WHERE  coe_tax_code = NEW.coe_tax_code
 	 	       AND coe_std_code = NEW.coe_std_code
 	 	       AND coe_qte_code = NEW.coe_qte_code
 	 	       AND (coe_date_debut, coe_date_fin) OVERLAPS (NEW.coe_date_debut, NEW.coe_date_fin)
 	 	;

		-- Comme le trigger est declenche sur AFTER et non pas sur BEFORE, il faut (nbChevauchements > 1) et non pas >0, car l enregistrement a deja ete ajoute, donc il se chevauche avec lui meme, ce qui est normal !
 	 	IF (nbChevauchements > 1) THEN
 	 	 	RAISE EXCEPTION 'Les taux de conversion ne peuvent se chevaucher.'  ;
 	 	END IF  ;

		RETURN NEW ;
 	END  ;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
-- lancer droit AZTI.sql et droits invite.sql
 