﻿-- ticket 40
ALTER TABLE pmp.t_operation_ope OWNER TO pmp;
ALTER TABLE pmp.t_lot_lot OWNER TO pmp;
ALTER TABLE pmp.t_bilanmigrationjournalier_bjo OWNER TO pmp;
ALTER TABLE pmp.t_bilanmigrationmensuel_bme OWNER TO pmp;
ALTER TABLE pmp.t_ouvrage_ouv OWNER TO pmp;
ALTER TABLE pmp.tg_dispositif_dis OWNER TO pmp;
ALTER TABLE pmp.tj_stationmesure_stm OWNER TO pmp;

-- ticket 42 

GRANT SELECT ON ref.tr_typedf_tdf TO pmp;

-- ticket 67

alter table pmp.tj_prelevementlot_prl  drop CONSTRAINT c_pk_prl;
alter table pmp.tj_prelevementlot_prl add prl_org_code character varying(30) NOT NULL;
alter table pmp.tj_prelevementlot_prl add constraint c_pk_prl PRIMARY KEY (prl_pre_typeprelevement, prl_lot_identifiant, prl_code,prl_org_code);
ALTER TABLE pmp.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_org_code FOREIGN KEY (prl_org_code) REFERENCES ref.ts_organisme_org(org_code);
-- si des données sont déjà présentes
/*
alter table pmp.tj_prelevementlot_prl  drop CONSTRAINT c_pk_prl;
alter table pmp.tj_prelevementlot_prl add prl_org_code character varying(30);
update pmp.tj_prelevementlot_prl set prl_org_code='pmp';
alter table pmp.tj_prelevementlot_prl ADD constraint c_nn_prl_org_code check (prl_org_code IS NOT NULL);
alter table pmp.tj_prelevementlot_prl add constraint c_pk_prl PRIMARY KEY (prl_pre_typeprelevement, prl_lot_identifiant, prl_code,prl_org_code);
ALTER TABLE pmp.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_org_code FOREIGN KEY (prl_org_code) REFERENCES ref.ts_organisme_org(org_code);
*/

alter table pmp.tj_coefficientconversion_coe  drop CONSTRAINT c_pk_coe;
alter table pmp.tj_coefficientconversion_coe add coe_org_code character varying(30) NOT NULL;
alter table pmp.tj_coefficientconversion_coe add constraint c_pk_coe PRIMARY KEY (coe_tax_code, coe_std_code, coe_qte_code, coe_date_debut, coe_date_fin,coe_org_code);
ALTER TABLE pmp.tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code) REFERENCES ref.ts_organisme_org(org_code);
-- si des données sont déjà présentes
/*
alter table pmp.tj_coefficientconversion_coe  drop CONSTRAINT c_pk_coe;
alter table pmp.tj_coefficientconversion_coe add coe_org_code character varying(30);
update pmp.tj_coefficientconversion_coe set coe_org_code='pmp';
alter table pmp.tj_coefficientconversion_coe ADD constraint c_nn_coe_org_code check (coe_org_code IS NOT NULL);
alter table pmp.tj_coefficientconversion_coe add constraint c_pk_coe PRIMARY KEY (coe_tax_code, coe_std_code, coe_qte_code, coe_date_debut, coe_date_fin,coe_org_code);
ALTER TABLE pmp.tj_coefficientconversion_coe ADD CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code) REFERENCES ref.ts_organisme_org(org_code);
*/

alter table pmp.t_operationmarquage_omq  drop CONSTRAINT c_pk_omg CASCADE;
alter table pmp.t_operationmarquage_omq add omq_org_code character varying(30) NOT NULL;
alter table pmp.t_operationmarquage_omq ADD CONSTRAINT c_pk_omq PRIMARY KEY (omq_reference,omq_org_code);
ALTER TABLE pmp.t_operationmarquage_omq ADD CONSTRAINT c_fk_omq_org_code FOREIGN KEY (omq_org_code) REFERENCES ref.ts_organisme_org(org_code);
ALTER TABLE pmp.t_marque_mqe ADD CONSTRAINT c_fk_mqe_omq_reference FOREIGN KEY (mqe_omq_reference,mqe_org_code)
      REFERENCES pmp.t_operationmarquage_omq (omq_reference,omq_org_code) ;

-- mise à jour de la table de maintenance



INSERT INTO pmp.ts_maintenance_main( main_ticket,main_description ) VALUES (59,'creation de la table de maintenance');
INSERT INTO pmp.ts_maintenance_main( main_ticket,main_description ) VALUES (40,'ajout des clé étrangères manquantes');
INSERT INTO pmp.ts_maintenance_main( main_ticket,main_description ) VALUES (42,'modification des propriétaires sur les tables à séquence et grant select sur ref.tr_typedf_tdf oublié');
INSERT INTO pmp.ts_maintenance_main( main_ticket,main_description ) VALUES (67,'org code rajouté dans les tables t_operationmarquage_omq, tj_coefficientconversion_coe,tj_prelevementlot_prl');
