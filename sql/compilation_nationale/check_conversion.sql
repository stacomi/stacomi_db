-- restauration de la base de donn�es


SELECT * FROM pmp.tj_actionmarquage_act;

select ref.updatesql('{"pmp"}',
'
--Ajout de la colonne act_nmq_code dans tj_actionmarquage
ALTER TABLE tj_actionmarquage_act ADD COLUMN act_nmq_code character varying(4);
--Mise � jour de ce champ par rapport au champ mqe_nmq_code
UPDATE tj_actionmarquage_act SET act_nmq_code=m.mqe_nmq_code FROM t_marque_mqe m where act_mqe_reference=m.mqe_reference;
--On ajoute une contrainte not null sur le champ act_nmq_code
ALTER TABLE tj_actionmarquage_act ALTER COLUMN act_nmq_code SET NOT NULL;
--On supprime les contrainte de cl� �trang�re / cl� primaire
ALTER TABLE tj_actionmarquage_act DROP CONSTRAINT c_fk_act_mqe_reference;
ALTER TABLE t_marque_mqe DROP CONSTRAINT c_pk_mqe;
--On les refait en ajoutant ce champ � la cl�
ALTER TABLE t_marque_mqe ADD CONSTRAINT c_pk_mqe PRIMARY KEY (mqe_reference, mqe_nmq_code, mqe_org_code);
ALTER TABLE tj_actionmarquage_act ADD CONSTRAINT c_fk_act_mqe_reference FOREIGN KEY (act_mqe_reference, act_nmq_code, act_org_code) REFERENCES t_marque_mqe (mqe_reference, mqe_nmq_code, mqe_org_code) MATCH SIMPLE ON UPDATE CASCADE ON DELETE NO ACTION;
');


select ref.updatesql('{"iav"}',
'insert into ts_maintenance_main
(
  main_ticket,
  main_description
  ) values
  (226,''Modification de la cl� �trang�re pour les marques'')'
);