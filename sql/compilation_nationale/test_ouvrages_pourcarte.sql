﻿/*
Test ouvrage pour carte.
Essai de compilation et de modification des données stacomi pour l'édition d'une carte
Ce script utilise postgis, les cartes sont importées et éditées dans Qgis
*/

-- utilisation des fonctions nettoye et compiletable pour copier les données provenant des différents schémas dans le schéma nat
DROP TABLE IF EXISTS nat.count;
CREATE TABLE nat.count(count integer,tablename character varying (25),datemaj date) ;

select nat.nettoye();
select nat.compiletable('iav','t_station_sta');--6
select nat.compiletable('azti','t_station_sta');--1
select nat.compiletable('bgm','t_station_sta');--8
--select nat.compiletable('charente','t_station_sta'); rien
select nat.compiletable('fd80','t_station_sta');--1

select nat.compiletable('inra','t_station_sta');--4
select nat.compiletable('logrami','t_station_sta');--1
delete from nat.t_station_sta where sta_nom='Uxondoa';
delete from nat.t_station_sta where sta_nom='Olha';
-- select nat.compiletable('mrm','t_station_sta'); rien
select nat.compiletable('saumonrhin','t_station_sta');--2
select nat.compiletable('smatah','t_station_sta');--1

-- examen des données
select * from nat.t_station_sta where sta_coordonnee_y is not null;
select sta_coordonnee_x,sta_coordonnee_y from nat.t_station_sta where sta_coordonnee_y is not null;

-- ajout d'une colonne géométrique dans la table nat.t_station_sta
SELECT AddGeometryColumn('nat', 't_station_sta','the_geom', 27572,'POINT',2);
-- remplissage à partir des coordonnées en lambert 2 étendu
UPDATE nat.t_station_sta
   SET the_geom=PointFromText('POINT(' || sta_coordonnee_x || ' ' || sta_coordonnee_y || ')',27572) where sta_coordonnee_y is not null; --23

-- Deux station IAV pour des essais de pêcherie de dévalaison que je vire...

delete from  nat.t_station_sta where sta_code='M4440002';--1
delete from  nat.t_station_sta where sta_code='M4560003';--1


-- stations FD80 (données envoyées par Aryendra)
delete from nat.t_station_sta where sta_code='M4800001';--1
select nat.compiletable('fd80','t_station_sta');--1
select * from  nat.t_station_sta where sta_org_code='FD80';
update nat.t_station_sta set (sta_coordonnee_x, sta_coordonnee_y)=(563661,2567558) where sta_code='M4800002';
update nat.t_station_sta set (sta_coordonnee_x, sta_coordonnee_y)=( 632479,2549330) where sta_code='M4800003';
update nat.t_station_sta set the_geom=st_transform(PointFromText('POINT(' || sta_coordonnee_x || ' ' || sta_coordonnee_y || ')',27572),2154) where sta_org_code='FD80';

-- migado (à partir des données du fichier excel
alter table migado.t_station_sta drop constraint c_ck_sta_coordonnee_x;
alter table migado.t_station_sta drop constraint c_ck_sta_coordonnee_y;
insert into migado.t_station_sta(sta_code,sta_nom,sta_localisation,sta_coordonnee_x,sta_coordonnee_y,sta_distance_mer,sta_date_creation,sta_org_code)
values(5,'Pointis','Pointis',505390,6224180,415000,'01/01/2003','MIGADO');
insert into migado.t_station_sta(sta_code,sta_nom,sta_localisation,sta_coordonnee_x,sta_coordonnee_y,sta_distance_mer,sta_date_creation,sta_org_code)
values(6,'Bazacle','Basacle',573410,6279590,370000,'01/01/1993','MIGADO');
insert into migado.t_station_sta(sta_code,sta_nom,sta_localisation,sta_coordonnee_x,sta_coordonnee_y,sta_distance_mer,sta_date_creation,sta_org_code)
values(7,'Mauzac','Mauzac',526646,6419631,195000,'01/01/2006','MIGADO');
-- rentrée des coordonnées y à la main pour les coordonnées manquantes.... directement dans la table
select nat.compiletable('MIGADO','t_station_sta');--7
update nat.t_station_sta set the_geom=PointFromText('POINT(' || sta_coordonnee_x || ' ' || sta_coordonnee_y || ')',2154) where sta_org_code='MIGADO';--7


-- maintenant je passe tout le monde en lambert 93
alter table nat.t_station_sta drop constraint enforce_srid_the_geom;
update nat.t_station_sta set the_geom=st_transform(the_geom,2154);
ALTER TABLE nat.t_station_sta  ADD CONSTRAINT enforce_srid_the_geom CHECK (st_srid(the_geom) = 2154);

-- modification manuelle de la table geometry_columns pour changer les coordonnées de référence sinon Qgis ne comprends pas et plante

-- Pour l'Oria ci dessous ne marche pas
update nat.t_station_sta set the_geom=st_transform(PointFromText('POINT(' || 462590|| ' ' || 2202820 || ')',27572),2154) where sta_code='sta01';


-- Les données de david (récupérées à partir d'un dump)
select nat.compiletable('migradour','t_station_sta');--13
update nat.t_station_sta set the_geom=st_transform(PointFromText('POINT(' || sta_coordonnee_x || ' ' || sta_coordonnee_y || ')',27572),2154) where sta_org_code='MIGRADOUR';

-- données de marion (récupérées à partir d'un dump laborieux de la table)
delete from nat.t_station_sta where sta_org_code='LOGRAMI';
select nat.compiletable('logrami','t_station_sta');--18
update nat.t_station_sta set the_geom=st_transform(PointFromText('POINT(' || sta_coordonnee_x || ' ' || sta_coordonnee_y || ')',27572),2154) where sta_org_code='LOGRAMI';--8

-- données Charente
select nat.compiletable('charente','t_station_sta');--1
--les coordonnées de la charente ne sont pas bonnes
http://maps.google.fr/maps?q=Crouin,+Cognac&hl=fr&ll=45.680238,-0.36198&spn=0.001484,0.003484&sll=45.985512,0.087891&sspn=3.022987,7.13562&oq=crouin+c&hnear=Crouin,+Merpins,+Poitou-Charentes&t=h&z=19

update nat.t_station_sta set the_geom=st_transform(geomfromtext('POINT(' || -0.362552 || ' ' || 45.679864 || ')', 4326),2154) where sta_org_code='CHARENTE';
-- attention il faut inverser l'ordre donnée par google maps qui donne latitude puis longitude geomfromtext('POINT(' || long || ' ' || lat || ')', 4326)

--AZTI
update nat.t_station_sta set the_geom=st_transform(PointFromText('POINT(-2.050632 43.262734)',4326),2154) where sta_code='oria'; -- 1 ici je suis allé sur google maps pour chercher les coordonnées

--BRESLE (pas sûr de la localisation exacte)...
insert into ref.ts_organisme_org values ('ONEMA','Office National de l''eau et des milieux aquatiques');
insert into nat.t_station_sta(sta_code, sta_nom, sta_org_code,the_geom) 
select 
'Bresle',
'Bresle',
'ONEMA',
 st_transform(PointFromText('POINT(1.489071 50.023685)',4326),2154);
-- MRM

select nat.compiletable('mrm','t_station_sta'); --6
select * from nat.t_station_sta where sta_org_code='MRM'
UPDATE nat.t_station_sta SET the_geom=ST_transform(PointFromText('POINT(' || sta_coordonnee_x || ' ' || sta_coordonnee_y || ')',27572),2154) where sta_org_code='MRM'; --6



-- mise à jour des x et y en lambert 93

update nat.t_station_sta set sta_coordonnee_x=  round(st_x(the_geom));--51
update nat.t_station_sta set sta_coordonnee_y= round(st_y(the_geom));--51

-- pour récupérer la carte sous google

alter table nat.t_station_sta add column googlemapcoord text;
update  nat.t_station_sta set googlemapcoord='http://maps.google.fr/maps?q='||st_y(st_transform(the_geom,4326))||','||st_x(st_transform(the_geom,4326));

--selection et sauvegarde des données 
select * from nat.t_station_sta;
set client_encoding to 'win1252';
COPY nat.t_station_sta to 'E:/IAV/stacomi/donnees_externes/tous/nat.t_station_sta.csv' with csv header delimiter as ';';






