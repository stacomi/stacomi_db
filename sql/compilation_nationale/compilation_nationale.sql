﻿create schema nat;
insert into  ref.ts_organisme_org values('nat','utilisateur de la base nationale');
CREATE USER nat WITH PASSWORD 'nat';
grant all on schema nat to invite;
-- correction d'erreurs
alter table nat.t_lot_lot add CONSTRAINT c_fk_lot_ope_identifiant FOREIGN KEY (lot_ope_identifiant, lot_org_code)
      REFERENCES nat.t_operation_ope (ope_identifiant, ope_org_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
alter table migradour.t_lot_lot add CONSTRAINT c_fk_lot_ope_identifiant FOREIGN KEY (lot_ope_identifiant, lot_org_code)
REFERENCES migradour.t_operation_ope (ope_identifiant, ope_org_code) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE NO ACTION;
-- pour tenter d'accélerer
DROP TRIGGER trg_ope_date ON nat.t_operation_ope;
-- pour récupérer les noms des tables
-- alter table 
/*
select table_name from information_schema.tables 
where table_schema ='iav'
and table_type ='BASE TABLE'
*/

CREATE OR REPLACE FUNCTION nat.compiletable(myschema varchar,mytable varchar)RETURNS int AS $$
	DECLARE
	linetablecount int;
	BEGIN	  	
		RAISE NOTICE 'table source  =%',myschema||'.'||mytable;
		EXECUTE 'SELECT count(*) FROM '||myschema||'.'||mytable INTO linetablecount;
		EXECUTE 'INSERT INTO nat.'||mytable||' select * from '||myschema||'.'||mytable;	
		RAISE NOTICE 'lignes insérées =%',linetablecount;
	RETURN linetablecount;
	END;
	$$
LANGUAGE 'plpgsql' ;
COMMENT ON FUNCTION nat.compiletable (myschema varchar,mytable varchar) IS 'fonction pour compiler un schema et une table appelee par la fonction compile ';
-- select nat.compiletable('iav','t_station_sta');
--select split_part('t_operation_ope','_',3)
-- vérifier si le record ci dessous est nécessaire
CREATE OR REPLACE FUNCTION nat.compile_table_per_bunch(myschema varchar,mytable varchar,_size int)RETURNS int AS $$
	DECLARE
	linetablecount int;
	currentrow int;
	result record;
	pk varchar;	
	BEGIN	  	
		RAISE NOTICE 'table source  =%',myschema||'.'||mytable;
		EXECUTE 'SELECT count(*) FROM '||myschema||'.'||mytable INTO linetablecount;
		--OPEN curs FOR EXECUTE 'select * from '||myschema||'.'||mytable;	
		currentrow=0;
		-- récupération de la clé primaire integer de la table
		EXECUTE 'SELECT attname from (SELECT pg_attribute.attname, 
		format_type(pg_attribute.atttypid, pg_attribute.atttypmod)
		FROM pg_index, pg_class, pg_attribute WHERE 
		pg_class.oid = '''||myschema||'.'||mytable||'''::regclass AND
		indrelid = pg_class.oid AND
		pg_attribute.attrelid = pg_class.oid AND 
		pg_attribute.attnum = any(pg_index.indkey)
		AND indisprimary) sub
		where format_type=''integer''' INTO pk;
		--pk=split_part(mytable,'_',3)||'_identifiant';
		while (currentrow<linetablecount) LOOP 
			EXECUTE 'INSERT INTO nat.'||mytable||' select * from '||myschema||'.'||mytable||' ORDER BY '||pk||' OFFSET '||currentrow|| ' LIMIT '|| _size ;		
			currentrow=currentrow+_size;
			RAISE NOTICE 'lignes insérées =%',currentrow;	
		END LOOP;
	--RAISE NOTICE 'lignes insérées =%',linetablecount;
	RETURN linetablecount;
	END;
	$$
LANGUAGE 'plpgsql' ;
COMMENT ON FUNCTION nat.compile_table_per_bunch(varchar,varchar,int) IS 'function allowing to write _size lines at a time ';

--select nat.compile_table_per_bunch('iav','t_operation_ope',1000);
-- attention ne marche pas nat.compile_table_per_bunch si il n'y a pas de clé primaire entière
-- ex nat.compile_table_per_bunch 

CREATE OR REPLACE FUNCTION nat.compile(myschema varchar)RETURNS int AS $$
	DECLARE
	totalcount int;
	linetablecount int;
	BEGIN	  
		SELECT INTO linetablecount nat.compiletable(myschema,'t_station_sta');
		totalcount=linetablecount;
		SELECT INTO linetablecount nat.compiletable(myschema,'t_ouvrage_ouv');
		totalcount=totalcount+linetablecount;	
		SELECT INTO linetablecount nat.compiletable(myschema,'t_dispositiffranchissement_dif');
		totalcount=totalcount+linetablecount;	
		SELECT INTO linetablecount nat.compiletable(myschema,'t_dispositifcomptage_dic');
		totalcount=totalcount+linetablecount;	
		SELECT INTO linetablecount nat.compiletable(myschema,'tg_dispositif_dis');
		totalcount=totalcount+linetablecount;			
		SELECT INTO linetablecount nat.compile_table_per_bunch(myschema,'t_operation_ope',1000);
		totalcount=totalcount+linetablecount;			
		SELECT INTO linetablecount nat.compile_table_per_bunch(myschema,'t_lot_lot',1000);
		totalcount=totalcount+linetablecount;			
		SELECT INTO linetablecount nat.compile_table_per_bunch(myschema,'tj_caracteristiquelot_car',1000);
		totalcount=totalcount+linetablecount;			
		SELECT INTO linetablecount nat.compile_table(myschema,'t_periodefonctdispositif_per');
		totalcount=totalcount+linetablecount;			
		SELECT INTO linetablecount nat.compiletable(myschema,'t_bilanmigrationjournalier_bjo');
		totalcount=totalcount+linetablecount;			
		SELECT INTO linetablecount nat.compiletable(myschema,'tj_stationmesure_stm');
		totalcount=totalcount+linetablecount;			
		SELECT INTO linetablecount nat.compiletable(myschema,'tj_coefficientconversion_coe');
		totalcount=totalcount+linetablecount;			
		SELECT INTO linetablecount nat.compiletable(myschema,'t_operationmarquage_omq');
		totalcount=totalcount+linetablecount;			
		SELECT INTO linetablecount nat.compiletable(myschema,'tj_dfesttype_dft');
		totalcount=totalcount+linetablecount;			
		SELECT INTO linetablecount nat.compiletable(myschema,'t_marque_mqe');
		totalcount=totalcount+linetablecount;
		SELECT INTO linetablecount nat.compiletable(myschema,'tj_actionmarquage_act');
		totalcount=totalcount+linetablecount;
		SELECT INTO linetablecount nat.compiletable(myschema,'tj_prelevementlot_prl');
		totalcount=totalcount+linetablecount;
		SELECT INTO linetablecount nat.compiletable(myschema,'tj_dfestdestinea_dtx');
		totalcount=totalcount+linetablecount;		
		SELECT INTO linetablecount nat.compile_table_per_bunch(myschema,'tj_conditionenvironnementale_env',1000);
		totalcount=totalcount+linetablecount;		
		SELECT INTO linetablecount nat.compiletable(myschema,'t_bilanmigrationmensuel_bme');
		totalcount=totalcount+linetablecount;		
		SELECT INTO linetablecount nat.compiletable(myschema,'tj_tauxechappement_txe');
		totalcount=totalcount+linetablecount;			
	RETURN totalcount;
	END;
	$$
LANGUAGE 'plpgsql' ;
COMMENT ON FUNCTION nat.compile (myschema varchar) IS 'fonction pour compiler le schema d''un operateur';


CREATE OR REPLACE FUNCTION nat.nettoye()RETURNS void AS $$
	DECLARE
	totalcount int;
	linetablecount int;
	BEGIN	  
		delete from nat.tj_caracteristiquelot_car;					
		delete from nat.tj_coefficientconversion_coe;					
		delete from nat.tj_dfesttype_dft;
		delete from nat.tj_actionmarquage_act;		
		delete from nat.tj_dfesttype_dft;		
		delete from nat.tj_prelevementlot_prl;		
		delete from nat.tj_dfestdestinea_dtx;				
		delete from nat.tj_conditionenvironnementale_env;
		delete from nat.tj_stationmesure_stm;				
		delete from nat.tj_tauxechappement_txe;
		delete from nat.t_lot_lot;
		delete from nat.t_operation_ope;	
		delete from nat.t_periodefonctdispositif_per;
		delete from nat.t_dispositifcomptage_dic;	
		delete from nat.t_dispositiffranchissement_dif;			
		delete from nat.tg_dispositif_dis;					
		delete from nat.t_ouvrage_ouv;					
		delete from nat.t_station_sta;		
		delete from nat.t_marque_mqe;		
		delete from nat.t_bilanmigrationjournalier_bjo;
		delete from nat.t_bilanmigrationmensuel_bme;
		delete from nat.t_operationmarquage_omq;			
	RETURN;
	END;
	$$
LANGUAGE 'plpgsql' ;




-- DROP FUNCTION nat.nettoye(_schema);

CREATE OR REPLACE FUNCTION nat.nettoye(myschema varchar) RETURNS void AS
$BODY$
	DECLARE
	totalcount int;
	linetablecount int;
	BEGIN	  
		EXECUTE 'delete from nat.tj_caracteristiquelot_car where car_org_code='''||myschema||'''';					
		EXECUTE 'delete from nat.tj_coefficientconversion_coe where coe_org_code='''||myschema||'''';								
		EXECUTE 'delete from nat.tj_actionmarquage_act where act_org_code='''||myschema||'''';				
		EXECUTE 'delete from nat.tj_dfesttype_dft where dft_org_code='''||myschema||'''';				
		EXECUTE 'delete from nat.tj_prelevementlot_prl where prl_org_code='''||myschema||'''';			
		EXECUTE 'delete from nat.tj_dfestdestinea_dtx where dtx_org_code='''||myschema||'''';					
		EXECUTE 'delete from nat.tj_conditionenvironnementale_env where env_org_code='''||myschema||'''';	
		EXECUTE 'delete from nat.tj_stationmesure_stm where stm_org_code='''||myschema||'''';					
		EXECUTE 'delete from nat.tj_tauxechappement_txe where txe_org_code='''||myschema||'''';	
		EXECUTE 'delete from nat.t_lot_lot where lot_org_code='''||myschema||'''';	
		EXECUTE 'delete from nat.t_operation_ope where ope_org_code='''||myschema||'''';		
		EXECUTE 'delete from nat.t_periodefonctdispositif_per where per_org_code='''||myschema||'''';	
		EXECUTE 'delete from nat.t_dispositifcomptage_dic where dic_org_code='''||myschema||'''';
		EXECUTE 'delete from nat.t_dispositiffranchissement_dif where dif_org_code='''||myschema||'''';			
		EXECUTE 'delete from nat.tg_dispositif_dis where dis_org_code='''||myschema||'''';					
		EXECUTE 'delete from nat.t_ouvrage_ouv where ouv_org_code='''||myschema||'''';					
		EXECUTE 'delete from nat.t_station_sta where sta_org_code='''||myschema||'''';	
		EXECUTE 'delete from nat.t_marque_mqe where mqe_org_code='''||myschema||'''';		
		EXECUTE 'delete from nat.t_bilanmigrationjournalier_bjo where bjo_org_code='''||myschema||'''';
		EXECUTE 'delete from nat.t_bilanmigrationmensuel_bme where bme_org_code='''||myschema||'''';
		EXECUTE 'delete from nat.t_operationmarquage_omq where omq_org_code='''||myschema||'''';			
	RETURN;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION nat.nettoye(myschema varchar)
  OWNER TO postgres;
COMMENT ON FUNCTION nat.nettoye(myschema varchar) is 'Fonction pour supprimer les données d''un schema seulement dans la table nationale'
--select nat.nettoye('SMATAH')
