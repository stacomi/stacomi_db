/*
 CREATE ROLE fd80 LOGIN
  ENCRYPTED PASSWORD 'md58e5734611d4529be607150ca826167f5'
  NOSUPERUSER NOINHERIT NOCREATEDB NOCREATEROLE;
  */
GRANT ALL ON SCHEMA fd80 TO fd80;
GRANT SELECT,INSERT,UPDATE ON fd80.t_station_sta TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.t_bilanmigrationjournalier_bjo TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.t_bilanmigrationmensuel_bme TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.tj_caracteristiquelot_car TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.tj_conditionenvironnementale_env TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.tj_stationmesure_stm TO fd80;
GRANT SELECT ON ref.tr_valeurparametrequalitatif_val TO fd80;
GRANT SELECT ON ref.tr_parametrequalitatif_qal TO fd80;
GRANT SELECT ON ref.tr_parametrequantitatif_qan TO fd80;
GRANT SELECT ON ref.tg_parametre_par TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.tj_dfesttype_dft TO fd80;
GRANT SELECT ON ref.tr_typedf_tdf TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.tj_dfestdestinea_dtx TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.tj_tauxechappement_txe TO fd80;
GRANT SELECT ON ref.tr_niveauechappement_ech TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.tj_coefficientconversion_coe TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.tj_pathologieconstatee_pco TO fd80;
GRANT SELECT ON ref.tr_pathologie_pat TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.tj_actionmarquage_act TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.t_marque_mqe TO fd80;
GRANT SELECT ON ref.tr_localisationanatomique_loc TO fd80;
GRANT SELECT ON ref.tr_naturemarque_nmq TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.t_operationmarquage_omq TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.t_lot_lot TO fd80;
GRANT SELECT ON ref.tr_devenirlot_dev TO fd80;
GRANT SELECT ON ref.tr_typequantitelot_qte TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.ts_taxonvideo_txv TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.ts_taillevideo_tav TO fd80;
GRANT SELECT ON ref.tr_stadedeveloppement_std TO fd80;
GRANT SELECT ON ref.tr_taxon_tax TO fd80;
GRANT SELECT ON ref.tr_niveautaxonomique_ntx TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.t_operation_ope TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.t_periodefonctdispositif_per TO fd80;
GRANT SELECT ON ref.tr_typearretdisp_tar TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.t_dispositifcomptage_dic TO fd80;
GRANT SELECT ON ref.tr_typedc_tdc TO fd80;
GRANT SELECT ON ref.ts_messagerlang_mrl TO fd80;
GRANT SELECT ON ref.ts_messager_msr TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.t_dispositiffranchissement_dif TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.tg_dispositif_dis TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.t_ouvrage_ouv TO fd80;
GRANT SELECT ON ref.tr_natureouvrage_nov TO fd80;
GRANT SELECT ON ref.tr_prelevement_pre TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON ref.ts_sequence_seq TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.tj_stationmesure_stm TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.tj_prelevementlot_prl TO fd80;
GRANT SELECT,INSERT,UPDATE,DELETE ON fd80.t_marque_mqe TO fd80;

GRANT SELECT, UPDATE ON fd80.tg_dispositif_dis_dis_identifiant_seq TO fd80;
GRANT SELECT, UPDATE ON fd80.t_lot_lot_lot_identifiant_seq TO fd80;
GRANT SELECT, UPDATE ON fd80.t_operation_ope_ope_identifiant_seq TO fd80;
GRANT SELECT, UPDATE ON fd80.t_ouvrage_ouv_ouv_identifiant_seq TO fd80;
GRANT SELECT, UPDATE ON fd80.tj_stationmesure_stm_stm_identifiant_seq TO fd80;
GRANT SELECT, UPDATE ON fd80.t_bilanmigrationmensuel_bme_bme_identifiant_seq TO fd80;
GRANT SELECT, UPDATE ON fd80.t_bilanmigrationjournalier_bjo_bjo_identifiant_seq TO fd80;

GRANT SELECT ON ref.ts_sequence_seq TO fd80;
GRANT SELECT ON TABLE ref.ts_sequence_seq TO invite;

-- vues
GRANT SELECT, UPDATE, INSERT ON TABLE fd80.v_taxon_tax TO fd80;
GRANT SELECT, UPDATE, INSERT ON TABLE fd80.vue_lot_ope_car TO fd80;
GRANT SELECT, UPDATE, INSERT ON TABLE fd80.vue_lot_ope_car_qan TO fd80;
GRANT SELECT, UPDATE, INSERT ON TABLE fd80.vue_ope_lot_ech_parqual TO fd80;
GRANT SELECT, UPDATE, INSERT ON TABLE fd80.vue_ope_lot_ech_parquan TO fd80;



GRANT SELECT ON fd80.t_station_sta TO invite;
GRANT SELECT ON fd80.t_bilanmigrationjournalier_bjo TO invite;
GRANT SELECT ON fd80.t_bilanmigrationmensuel_bme TO invite;
GRANT SELECT ON fd80.tj_caracteristiquelot_car TO invite;
GRANT SELECT ON fd80.tj_conditionenvironnementale_env TO invite;
GRANT SELECT ON fd80.tj_stationmesure_stm TO invite;
GRANT SELECT ON ref.tr_valeurparametrequalitatif_val TO invite;
GRANT SELECT ON ref.tr_parametrequalitatif_qal TO invite;
GRANT SELECT ON ref.tr_parametrequantitatif_qan TO invite;
GRANT SELECT ON ref.tg_parametre_par TO invite;
GRANT SELECT ON fd80.tj_dfesttype_dft TO invite;
GRANT SELECT ON ref.tr_typedf_tdf TO invite;
GRANT SELECT ON fd80.tj_dfestdestinea_dtx TO invite;
GRANT SELECT ON fd80.tj_tauxechappement_txe TO invite;
GRANT SELECT ON ref.tr_niveauechappement_ech TO invite;
GRANT SELECT ON fd80.tj_coefficientconversion_coe TO invite;
GRANT SELECT ON fd80.tj_pathologieconstatee_pco TO invite;
GRANT SELECT ON ref.tr_pathologie_pat TO invite;
GRANT SELECT ON fd80.tj_actionmarquage_act TO invite;
GRANT SELECT ON fd80.t_marque_mqe TO invite;
GRANT SELECT ON ref.tr_localisationanatomique_loc TO invite;
GRANT SELECT ON ref.tr_naturemarque_nmq TO invite;
GRANT SELECT ON fd80.t_operationmarquage_omq TO invite;
GRANT SELECT ON fd80.t_lot_lot TO invite;
GRANT SELECT ON ref.tr_devenirlot_dev TO invite;
GRANT SELECT ON ref.tr_typequantitelot_qte TO invite;
GRANT SELECT ON fd80.ts_taxonvideo_txv TO invite;
GRANT SELECT ON fd80.ts_taillevideo_tav TO invite;
GRANT SELECT ON ref.tr_stadedeveloppement_std TO invite;
GRANT SELECT ON ref.tr_taxon_tax TO invite;
GRANT SELECT ON ref.tr_niveautaxonomique_ntx TO invite;
GRANT SELECT ON fd80.t_operation_ope TO invite;
GRANT SELECT ON fd80.t_periodefonctdispositif_per TO invite;
GRANT SELECT ON ref.tr_typearretdisp_tar TO invite;
GRANT SELECT ON fd80.t_dispositifcomptage_dic TO invite;
GRANT SELECT ON ref.tr_typedc_tdc TO invite;
GRANT SELECT ON fd80.t_dispositiffranchissement_dif TO invite;
GRANT SELECT ON fd80.tg_dispositif_dis TO invite;
GRANT SELECT ON fd80.t_ouvrage_ouv TO invite;
GRANT SELECT ON ref.tr_natureouvrage_nov TO invite;
GRANT SELECT ON fd80.t_station_sta TO invite;
GRANT SELECT ON ref.tr_prelevement_pre TO invite;
GRANT SELECT ON ref.ts_organisme_org TO invite;
GRANT SELECT ON ref.ts_sequence_seq TO invite;
GRANT SELECT ON fd80.tj_stationmesure_stm TO invite;
GRANT SELECT ON fd80.tj_prelevementlot_prl TO invite;
GRANT SELECT ON fd80.t_marque_mqe TO invite;
--vues
GRANT SELECT ON fd80.v_taxon_tax TO invite;
GRANT SELECT ON fd80.vue_lot_ope_car TO invite;
GRANT SELECT ON fd80.vue_lot_ope_car_qan TO invite;
GRANT SELECT ON fd80.vue_ope_lot_ech_parqual TO invite;
GRANT SELECT ON fd80.vue_ope_lot_ech_parquan TO invite;
-- sequences
GRANT SELECT, UPDATE ON fd80.tg_dispositif_dis_dis_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON fd80.t_lot_lot_lot_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON fd80.t_operation_ope_ope_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON fd80.t_ouvrage_ouv_ouv_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON fd80.tj_stationmesure_stm_stm_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON fd80.t_bilanmigrationmensuel_bme_bme_identifiant_seq TO invite;
GRANT SELECT, UPDATE ON fd80.t_bilanmigrationjournalier_bjo_bjo_identifiant_seq TO invite;

-- USAGE ne suffit pas 
alter table fd80.t_operation_ope owner to fd80;
alter table fd80.t_lot_lot owner to fd80;
alter table fd80.t_ouvrage_ouv owner to fd80;
alter table fd80.tg_dispositif_dis owner to fd80;
alter table fd80.tj_stationmesure_stm owner to fd80;

ALTER SEQUENCE fd80.t_lot_lot_lot_identifiant_seq owner to fd80;
ALTER SEQUENCE fd80.t_operation_ope_ope_identifiant_seq owner to fd80;
ALTER SEQUENCE fd80.t_ouvrage_ouv_ouv_identifiant_seq owner to fd80;
ALTER SEQUENCE fd80.tg_dispositif_dis_dis_identifiant_seq owner to fd80;
ALTER SEQUENCE fd80.tj_stationmesure_stm_stm_identifiant_seq owner to fd80;