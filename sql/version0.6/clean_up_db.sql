-- SCRIPT TO CREAN UP IAV and only retain years 1996-2015
/*
 #save db from clarisse
 pg_dump -U postgres -f "test.sql" --schema test bd_contmig_nat.sql
psql -U postgres -f "test.sql" bd_contmig_nat 
# save to new db 
pg_dump -U postgres -f "bd_contmig_nat_test.sql" --schema ref --schema test bd_contmig_nat
CREATEDB -U postgres bd_contmig_nat_test2
psql -U postgres -c "create extension postgis" bd_contmig_nat_test
ALTER ROLE test WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS ;
psql -U postgres -f "bd_contmig_nat_test.sql" bd_contmig_nat_test


*/

ALTER TABLE test.tj_stationmesure_stm DROP CONSTRAINT c_fk_stm_sta_code ;
ALTER TABLE test.tj_stationmesure_stm ADD CONSTRAINT c_fk_stm_sta_code FOREIGN KEY (stm_sta_code, stm_org_code)
REFERENCES test.t_station_sta(sta_code, sta_org_code) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE test.tj_conditionenvironnementale_env DROP CONSTRAINT c_fk_env_stm_identifiant;
ALTER TABLE test.tj_conditionenvironnementale_env ADD CONSTRAINT c_fk_env_stm_identifiant FOREIGN KEY (env_stm_identifiant, env_org_code) 
REFERENCES test.tj_stationmesure_stm(stm_identifiant, stm_org_code) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE test.tj_caracteristiquelot_car DROP CONSTRAINT c_fk_car_lot_identifiant;
ALTER TABLE test.tj_caracteristiquelot_car ADD CONSTRAINT c_fk_car_lot_identifiant FOREIGN KEY (car_lot_identifiant,car_org_code) 
  REFERENCES test.t_lot_lot(lot_identifiant,lot_org_code) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE test.tj_actionmarquage_act DROP CONSTRAINT c_fk_act_lot_identifiant;
ALTER TABLE test.tj_actionmarquage_act ADD CONSTRAINT c_fk_act_lot_identifiant
FOREIGN KEY (act_lot_identifiant,act_org_code) 
  REFERENCES test.t_lot_lot(lot_identifiant,lot_org_code) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE test.t_lot_lot DROP CONSTRAINT c_fk_lot_ope_identifiant;
ALTER TABLE test.t_lot_lot ADD CONSTRAINT c_fk_lot_ope_identifiant 
FOREIGN KEY (lot_ope_identifiant, lot_org_code) REFERENCES test.t_operation_ope(ope_identifiant, ope_org_code) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE test.tj_pathologieconstatee_pco DROP CONSTRAINT c_fk_pco_lot_identifiant;
ALTER TABLE test.tj_pathologieconstatee_pco ADD CONSTRAINT c_fk_pco_lot_identifiant 
FOREIGN KEY (pco_lot_identifiant, pco_org_code) REFERENCES test.t_lot_lot(lot_identifiant, lot_org_code) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE test.t_ouvrage_ouv DROP CONSTRAINT c_fk_ouv_sta_code ;
ALTER TABLE test.t_ouvrage_ouv ADD CONSTRAINT c_fk_ouv_sta_code FOREIGN KEY (ouv_sta_code, ouv_org_code) 
REFERENCES test.t_station_sta(sta_code, sta_org_code) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE test.t_dispositiffranchissement_dif DROP CONSTRAINT c_fk_dif_dis_identifiant;
ALTER TABLE test.t_dispositiffranchissement_dif ADD CONSTRAINT c_fk_dif_dis_identifiant FOREIGN KEY (dif_dis_identifiant, dif_org_code) 
REFERENCES test.tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE test.t_dispositiffranchissement_dif DROP CONSTRAINT c_fk_dif_ouv_identifiant;
ALTER TABLE test.t_dispositiffranchissement_dif ADD CONSTRAINT c_fk_dif_ouv_identifiant FOREIGN KEY (dif_ouv_identifiant, dif_org_code) REFERENCES test.t_ouvrage_ouv(ouv_identifiant, ouv_org_code) 
ON UPDATE CASCADE ON DELETE CASCADE;


ALTER TABLE test.t_dispositifcomptage_dic DROP CONSTRAINT c_fk_dic_dif_identifiant;
ALTER TABLE test.t_dispositifcomptage_dic ADD CONSTRAINT c_fk_dic_dif_identifiant FOREIGN KEY (dic_dif_identifiant, dic_org_code) 
REFERENCES test.t_dispositiffranchissement_dif(dif_dis_identifiant, dif_org_code) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE test.t_dispositifcomptage_dic DROP CONSTRAINT c_fk_dic_dis_identifiant;
ALTER TABLE test.t_dispositifcomptage_dic ADD CONSTRAINT c_fk_dic_dis_identifiant FOREIGN KEY (dic_dis_identifiant, dic_org_code) 
REFERENCES test.tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE test.t_periodefonctdispositif_per DROP CONSTRAINT c_fk_per_dis_identifiant;
ALTER TABLE test.t_periodefonctdispositif_per ADD CONSTRAINT c_fk_per_dis_identifiant FOREIGN KEY (per_dis_identifiant, per_org_code) 
REFERENCES test.tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE ON DELETE CASCADE;


ALTER TABLE test.tj_dfestdestinea_dtx DROP CONSTRAINT c_fk_dtx_dif_identifiant;
ALTER TABLE test.tj_dfestdestinea_dtx ADD CONSTRAINT c_fk_dtx_dif_identifiant FOREIGN KEY (dtx_dif_identifiant, dtx_org_code) 
REFERENCES test.tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE test.tj_dfesttype_dft DROP CONSTRAINT c_fk_dft_df_identifiant;
ALTER TABLE test.tj_dfesttype_dft ADD CONSTRAINT c_fk_dft_df_identifiant FOREIGN KEY (dft_df_identifiant, dft_org_code) 
REFERENCES test.tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE test.tj_prelevementlot_prl DROP CONSTRAINT c_fk_prl_lot_identifiant;
ALTER TABLE test.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_lot_identifiant FOREIGN KEY (prl_lot_identifiant, prl_org_code) 
REFERENCES test.t_lot_lot(lot_identifiant, lot_org_code) ON UPDATE CASCADE ON DELETE CASCADE;



DELETE FROM test.t_station_sta WHERE sta_code IN('M4440002','M4560003','M4440001','M4560002','M4350001');
DELETE FROM test.t_operation_ope WHERE ope_date_debut>='2014-01-01';
DELETE FROM test.t_operation_ope WHERE ope_date_fin<'2010-01-01';

ALTER TABLE test.tj_conditionenvironnementale_env DROP CONSTRAINT c_fk_env_stm_identifiant;
ALTER TABLE test.tj_conditionenvironnementale_env ADD CONSTRAINT c_fk_env_stm_identifiant FOREIGN KEY (env_stm_identifiant, env_org_code) 
REFERENCES test.tj_stationmesure_stm(stm_identifiant, stm_org_code) ON UPDATE CASCADE;

ALTER TABLE test.tj_stationmesure_stm DROP CONSTRAINT c_fk_stm_sta_code ;
ALTER TABLE test.tj_stationmesure_stm ADD CONSTRAINT c_fk_stm_sta_code FOREIGN KEY (stm_sta_code, stm_org_code)
REFERENCES test.t_station_sta(sta_code, sta_org_code) ON UPDATE CASCADE;

ALTER TABLE test.tj_caracteristiquelot_car DROP CONSTRAINT c_fk_car_lot_identifiant;
ALTER TABLE test.tj_caracteristiquelot_car ADD CONSTRAINT c_fk_car_lot_identifiant FOREIGN KEY (car_lot_identifiant,car_org_code) 
  REFERENCES test.t_lot_lot(lot_identifiant,lot_org_code) ON UPDATE CASCADE;

ALTER TABLE test.tj_actionmarquage_act DROP CONSTRAINT c_fk_act_lot_identifiant;
ALTER TABLE test.tj_actionmarquage_act ADD CONSTRAINT c_fk_act_lot_identifiant
FOREIGN KEY (act_lot_identifiant,act_org_code) 
  REFERENCES test.t_lot_lot(lot_identifiant,lot_org_code) ON UPDATE CASCADE;

ALTER TABLE test.t_lot_lot DROP CONSTRAINT c_fk_lot_ope_identifiant;
ALTER TABLE test.t_lot_lot ADD CONSTRAINT c_fk_lot_ope_identifiant 
FOREIGN KEY (lot_ope_identifiant, lot_org_code) REFERENCES test.t_operation_ope(ope_identifiant, ope_org_code) 
ON UPDATE CASCADE ;

ALTER TABLE test.tj_pathologieconstatee_pco DROP CONSTRAINT c_fk_pco_lot_identifiant;
ALTER TABLE test.tj_pathologieconstatee_pco ADD CONSTRAINT c_fk_pco_lot_identifiant 
FOREIGN KEY (pco_lot_identifiant, pco_org_code) REFERENCES test.t_lot_lot(lot_identifiant, lot_org_code) 
ON UPDATE CASCADE ;


ALTER TABLE test.t_ouvrage_ouv DROP CONSTRAINT c_fk_ouv_sta_code ;
ALTER TABLE test.t_ouvrage_ouv ADD CONSTRAINT c_fk_ouv_sta_code FOREIGN KEY (ouv_sta_code, ouv_org_code) 
REFERENCES test.t_station_sta(sta_code, sta_org_code) ON UPDATE CASCADE;

ALTER TABLE test.t_dispositiffranchissement_dif DROP CONSTRAINT c_fk_dif_dis_identifiant;
ALTER TABLE test.t_dispositiffranchissement_dif ADD CONSTRAINT c_fk_dif_dis_identifiant FOREIGN KEY (dif_dis_identifiant, dif_org_code) 
REFERENCES test.tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE;

ALTER TABLE test.t_dispositiffranchissement_dif DROP CONSTRAINT c_fk_dif_ouv_identifiant;
ALTER TABLE test.t_dispositiffranchissement_dif ADD CONSTRAINT c_fk_dif_ouv_identifiant FOREIGN KEY (dif_ouv_identifiant, dif_org_code) REFERENCES test.t_ouvrage_ouv(ouv_identifiant, ouv_org_code) 
ON UPDATE CASCADE;

ALTER TABLE test.t_dispositifcomptage_dic DROP CONSTRAINT c_fk_dic_dif_identifiant;
ALTER TABLE test.t_dispositifcomptage_dic ADD CONSTRAINT c_fk_dic_dif_identifiant FOREIGN KEY (dic_dif_identifiant, dic_org_code) 
REFERENCES test.t_dispositiffranchissement_dif(dif_dis_identifiant, dif_org_code) ON UPDATE CASCADE;

ALTER TABLE test.t_dispositifcomptage_dic DROP CONSTRAINT c_fk_dic_dis_identifiant;
ALTER TABLE test.t_dispositifcomptage_dic ADD CONSTRAINT c_fk_dic_dis_identifiant FOREIGN KEY (dic_dis_identifiant, dic_org_code) 
REFERENCES test.tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE;

ALTER TABLE test.t_periodefonctdispositif_per DROP CONSTRAINT c_fk_per_dis_identifiant;
ALTER TABLE test.t_periodefonctdispositif_per ADD CONSTRAINT c_fk_per_dis_identifiant FOREIGN KEY (per_dis_identifiant, per_org_code) 
REFERENCES test.tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE;


ALTER TABLE test.tj_dfestdestinea_dtx DROP CONSTRAINT c_fk_dtx_dif_identifiant;
ALTER TABLE test.tj_dfestdestinea_dtx ADD CONSTRAINT c_fk_dtx_dif_identifiant FOREIGN KEY (dtx_dif_identifiant, dtx_org_code) 
REFERENCES test.tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE;

ALTER TABLE test.tj_dfesttype_dft DROP CONSTRAINT c_fk_dft_df_identifiant;
ALTER TABLE test.tj_dfesttype_dft ADD CONSTRAINT c_fk_dft_df_identifiant FOREIGN KEY (dft_df_identifiant, dft_org_code) 
REFERENCES test.tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE;

ALTER TABLE test.tj_prelevementlot_prl DROP CONSTRAINT c_fk_prl_lot_identifiant;
ALTER TABLE test.tj_prelevementlot_prl ADD CONSTRAINT c_fk_prl_lot_identifiant FOREIGN KEY (prl_lot_identifiant, prl_org_code) 
REFERENCES test.t_lot_lot(lot_identifiant, lot_org_code) ON UPDATE CASCADE;


CREATE OR REPLACE FUNCTION test.fct_coe_date()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$   

  DECLARE nbChevauchements INTEGER ;

  BEGIN
    -- verification des non-chevauchements pour les taux
    SELECT COUNT(*) INTO nbChevauchements
    FROM   test.tj_coefficientconversion_coe
    WHERE  coe_tax_code = NEW.coe_tax_code
           AND coe_std_code = NEW.coe_std_code
           AND coe_qte_code = NEW.coe_qte_code
           AND (coe_date_debut, coe_date_fin) OVERLAPS (NEW.coe_date_debut, NEW.coe_date_fin)
    ;

    -- Comme le trigger est declenche sur AFTER et non pas sur BEFORE, il faut (nbChevauchements > 1) et non pas >0, car l enregistrement a deja ete ajoute, donc il se chevauche avec lui meme, ce qui est normal !
    IF (nbChevauchements > 1) THEN
      RAISE EXCEPTION 'Les taux de conversion ne peuvent se chevaucher.'  ;
    END IF  ;

    RETURN NEW ;
  END  ;
$function$
;

CREATE OR REPLACE FUNCTION test.fct_ope_date()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$   

  DECLARE disCreation     TIMESTAMP  ;
  DECLARE disSuppression    TIMESTAMP  ;
  DECLARE nbChevauchements  INTEGER    ;

  BEGIN
    -- Recuperation des dates du dispositif dans des variables
    SELECT dis_date_creation, dis_date_suppression INTO disCreation, disSuppression
    FROM   test.tg_dispositif_dis
    WHERE  dis_identifiant = NEW.ope_dic_identifiant
    ;

    -- verification de la date de debut
    IF ((NEW.ope_date_debut < disCreation) OR (NEW.ope_date_debut > disSuppression)) THEN
      RAISE EXCEPTION 'Le debut de l operation doit etre inclus dans la periode d existence du DC.'  ;
    END IF  ;

    -- verification de la date de fin
    IF ((NEW.ope_date_fin < disCreation) OR (NEW.ope_date_fin > disSuppression)) THEN
      RAISE EXCEPTION 'La fin de l operation doit etre incluse dans la periode d existence du DC.'  ;
    END IF  ;

    -- verification des non-chevauchements pour les operations du dispositif
    SELECT COUNT(*) INTO nbChevauchements
    FROM   test.t_operation_ope
    WHERE  ope_dic_identifiant = NEW.ope_dic_identifiant 
           AND (ope_date_debut, ope_date_fin) OVERLAPS (NEW.ope_date_debut, NEW.ope_date_fin)
    ;

    -- Comme le trigger est declenche sur AFTER et non pas sur BEFORE, il faut (nbChevauchements > 1) et non pas >0, car l enregistrement a deja ete ajoute, donc il se chevauche avec lui meme, ce qui est normal !
    IF (nbChevauchements > 1) THEN 
      RAISE EXCEPTION 'Les operations ne peuvent se chevaucher.'  ;
    END IF  ;

    RETURN NEW ;
  END  ;
$function$
;

CREATE OR REPLACE FUNCTION test.fct_per_date()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$ 

  DECLARE disCreation     TIMESTAMP  ;
  DECLARE disSuppression    TIMESTAMP  ;
  DECLARE nbChevauchements  INTEGER    ;

  BEGIN
    -- Recuperation des dates du dispositif dans des variables
    SELECT dis_date_creation, dis_date_suppression INTO disCreation, disSuppression
    FROM   test.tg_dispositif_dis
    WHERE  dis_identifiant = NEW.per_dis_identifiant
    ;

    -- verification de la date de debut
    IF ((NEW.per_date_debut < disCreation) OR (NEW.per_date_debut > disSuppression)) THEN
      RAISE EXCEPTION 'Le debut de la periode doit etre inclus dans la periode d existence du dispositif.'  ;
    END IF  ;

    -- verification de la date de fin
    IF ((NEW.per_date_fin < disCreation) OR (NEW.per_date_fin > disSuppression)) THEN
      RAISE EXCEPTION 'La fin de la periode doit etre incluse dans la periode d existence du dispositif.'  ;
    END IF  ;

    -- verification des non-chevauchements pour les periodes du dispositif
    SELECT COUNT(*) INTO nbChevauchements
    FROM   test.t_periodefonctdispositif_per
    WHERE  per_dis_identifiant = NEW.per_dis_identifiant 
           AND (per_date_debut, per_date_fin) OVERLAPS (NEW.per_date_debut, NEW.per_date_fin)
    ;

    -- Comme le trigger est declenche sur AFTER et non pas sur BEFORE, il faut (nbChevauchements > 1) et non pas >0, car l enregistrement a deja ete ajoute, donc il se chevauche avec lui meme, ce qui est normal !
    IF (nbChevauchements > 1) THEN
      RAISE EXCEPTION 'Les periodes ne peuvent se chevaucher.'  ;
    END IF  ;
    
    RETURN NEW ;
  END  ;
$function$
;

CREATE OR REPLACE FUNCTION test.fct_per_suppression()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$   

  BEGIN
    -- La periode precedent celle supprimee est prolongee
    -- jusqu a la fin de la periode supprimee
    UPDATE test.t_periodefonctdispositif_per 
    SET    per_date_fin = OLD.per_date_fin 
    WHERE  per_date_fin= OLD.per_date_debut 
           AND per_dis_identifiant = OLD.per_dis_identifiant 
    ;
    RETURN NEW ;
  END  ;

$function$
;
CREATE OR REPLACE FUNCTION test.fct_txe_date()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$   

  DECLARE nbChevauchements INTEGER ;

  BEGIN
    -- verification des non-chevauchements pour les taux
    SELECT COUNT(*) INTO nbChevauchements
    FROM   test.tj_tauxechappement_txe
    WHERE  txe_sta_code = NEW.txe_sta_code
           AND txe_tax_code = NEW.txe_tax_code
           AND txe_std_code = NEW.txe_std_code
           AND (txe_date_debut, txe_date_fin) OVERLAPS (NEW.txe_date_debut, NEW.txe_date_fin)
    ;

    -- Comme le trigger est declenche sur AFTER et non pas sur BEFORE, il faut (nbChevauchements > 1) et non pas >0, car l enregistrement a deja ete ajoute, donc il se chevauche avec lui meme, ce qui est normal !
    IF (nbChevauchements > 1) THEN
      RAISE EXCEPTION 'Les taux d echappement ne peuvent se chevaucher.'  ;
    END IF  ;

    RETURN NEW ;

  END  ;
$function$
;

CREATE OR REPLACE FUNCTION test.masqueope()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$   
DECLARE test text;


  BEGIN
    -- Recuperation du type
    SELECT mas_type into test
    FROM   test.ts_masque_mas
    WHERE  mas_id = NEW.mao_mas_id
    ;

    -- verification 
    IF (test!='ope') THEN
      RAISE EXCEPTION 'Attention le masque n'' est pas un masque operation, changez le type de masque'  ;
    END IF  ;     

    RETURN NEW ;
  END  ;
$function$
;

/*
 * 
 * pg_dump -U postgres -f "bd_contmig_nat_test.sql" bd_contmig_nat_test
 */

SELECT count (*) FROM test.t_lot_lot
-- UPDATE "ref".ts_organisme_org SET org_code ='TEST' WHERE org_code='IAV';
-- changement � la main apr�s dump


GRANT ALL ON SCHEMA test TO test;
ALTER TABLE test.t_operation_ope OWNER TO test;
ALTER TABLE test.t_lot_lot OWNER TO test;
ALTER TABLE test.t_bilanmigrationjournalier_bjo OWNER TO test;
ALTER TABLE test.t_bilanmigrationmensuel_bme OWNER TO test;
ALTER TABLE test.t_ouvrage_ouv OWNER TO test;
ALTER TABLE test.tg_dispositif_dis OWNER TO test;
ALTER TABLE test.tj_stationmesure_stm OWNER TO test;
ALTER TABLE test.t_dispositifcomptage_dic OWNER TO test;
ALTER TABLE test.t_dispositiffranchissement_dif OWNER TO test;
ALTER TABLE test.t_periodefonctdispositif_per OWNER TO test;
ALTER TABLE test.t_marque_mqe OWNER TO test;
ALTER TABLE test.t_operationmarquage_omq OWNER TO test;
ALTER TABLE test.t_station_sta OWNER TO test;
ALTER TABLE test.tj_actionmarquage_act OWNER TO test;
ALTER TABLE test.tj_caracteristiquelot_car OWNER TO test;
ALTER TABLE test.tj_coefficientconversion_coe OWNER TO test;
ALTER TABLE test.tj_conditionenvironnementale_env OWNER TO test;
ALTER TABLE test.tj_dfestdestinea_dtx OWNER TO test;
ALTER TABLE test.tj_dfesttype_dft OWNER TO test;
ALTER TABLE test.tj_pathologieconstatee_pco OWNER TO test;
ALTER TABLE test.tj_prelevementlot_prl OWNER TO test;
ALTER TABLE test.tj_tauxechappement_txe OWNER TO test;
ALTER TABLE test.ts_maintenance_main OWNER TO test;
ALTER TABLE test.ts_masque_mas OWNER TO test;
ALTER TABLE test.ts_masquecaracteristiquelot_mac OWNER TO test;
ALTER TABLE test.ts_masqueconditionsenvironnementales_mae OWNER TO test;
ALTER TABLE test.ts_masquelot_mal OWNER TO test;
ALTER TABLE test.ts_masqueope_mao OWNER TO test;
ALTER TABLE test.ts_masqueordreaffichage_maa OWNER TO test;
ALTER TABLE test.ts_taillevideo_tav OWNER TO test;
ALTER TABLE test.ts_taxonvideo_txv OWNER TO test;

select ref.updatesql('{"test"}',
'

ALTER TABLE ONLY tj_actionmarquage_act
    DROP CONSTRAINT IF EXISTS c_fk_act_lot_identifiant;

ALTER TABLE ONLY tj_actionmarquage_act
    ADD CONSTRAINT c_fk_act_lot_identifiant FOREIGN KEY (act_lot_identifiant, act_org_code) REFERENCES t_lot_lot(lot_identifiant, lot_org_code) ON UPDATE CASCADE;

ALTER TABLE ONLY tj_actionmarquage_act 
DROP CONSTRAINT IF EXISTS c_fk_act_mqe_reference ;

ALTER TABLE ONLY tj_actionmarquage_act
    ADD CONSTRAINT c_fk_act_mqe_reference FOREIGN KEY (act_mqe_reference, act_org_code) REFERENCES t_marque_mqe(mqe_reference, mqe_org_code) ON UPDATE CASCADE;

ALTER TABLE ONLY tj_actionmarquage_act
    DROP CONSTRAINT IF EXISTS c_fk_act_org_code ;

ALTER TABLE ONLY tj_actionmarquage_act
    ADD CONSTRAINT c_fk_act_org_code FOREIGN KEY (act_org_code) REFERENCES ref.ts_organisme_org(org_code)  MATCH FULL ON UPDATE CASCADE;

-------------------------------------------------------



ALTER TABLE ONLY t_bilanmigrationjournalier_bjo
    DROP CONSTRAINT IF EXISTS c_fk_bjo_std_code;

ALTER TABLE ONLY t_bilanmigrationjournalier_bjo
    ADD CONSTRAINT c_fk_bjo_std_code FOREIGN KEY (bjo_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_bilanmigrationjournalier_bjo
    DROP CONSTRAINT IF EXISTS c_fk_bjo_tax_code;

ALTER TABLE ONLY t_bilanmigrationjournalier_bjo
    ADD CONSTRAINT c_fk_bjo_tax_code FOREIGN KEY (bjo_tax_code) REFERENCES ref.tr_taxon_tax(tax_code) ON UPDATE CASCADE;

/* contrainte manquante v0.4*/


ALTER TABLE t_bilanmigrationjournalier_bjo
  DROP CONSTRAINT IF EXISTS c_fk_bjo_org_code;

ALTER TABLE t_bilanmigrationjournalier_bjo
  ADD CONSTRAINT c_fk_bjo_org_code FOREIGN KEY (bjo_org_code)
      REFERENCES ref.ts_organisme_org (org_code) MATCH FULL
      ON UPDATE CASCADE ON DELETE NO ACTION;

-------------------------------------------------------



ALTER TABLE ONLY t_bilanmigrationmensuel_bme
    DROP CONSTRAINT IF EXISTS c_fk_bme_std_code;

ALTER TABLE ONLY t_bilanmigrationmensuel_bme
    ADD CONSTRAINT c_fk_bme_std_code FOREIGN KEY (bme_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_bilanmigrationmensuel_bme
    DROP CONSTRAINT IF EXISTS c_fk_bme_tax_code;

ALTER TABLE ONLY t_bilanmigrationmensuel_bme
    ADD CONSTRAINT c_fk_bme_tax_code FOREIGN KEY (bme_tax_code) REFERENCES ref.tr_taxon_tax(tax_code) ON UPDATE CASCADE;


-------------------------------------------------------


ALTER TABLE ONLY tj_caracteristiquelot_car
    DROP CONSTRAINT IF EXISTS c_fk_car_lot_identifiant;

ALTER TABLE ONLY tj_caracteristiquelot_car
    ADD CONSTRAINT c_fk_car_lot_identifiant FOREIGN KEY (car_lot_identifiant, car_org_code) REFERENCES t_lot_lot(lot_identifiant, lot_org_code) ON UPDATE CASCADE;

ALTER TABLE ONLY tj_caracteristiquelot_car
    DROP CONSTRAINT IF EXISTS c_fk_car_org_code;
    
ALTER TABLE ONLY tj_caracteristiquelot_car
    ADD CONSTRAINT c_fk_car_org_code FOREIGN KEY (car_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;

ALTER TABLE ONLY tj_caracteristiquelot_car
    DROP CONSTRAINT IF EXISTS c_fk_car_par_code;
    
ALTER TABLE ONLY tj_caracteristiquelot_car
    ADD CONSTRAINT c_fk_car_par_code FOREIGN KEY (car_par_code) REFERENCES ref.tg_parametre_par(par_code) ON UPDATE CASCADE;

ALTER TABLE ONLY tj_caracteristiquelot_car
   DROP CONSTRAINT IF EXISTS c_fk_car_val_identifiant;

ALTER TABLE ONLY tj_caracteristiquelot_car
    ADD CONSTRAINT c_fk_car_val_identifiant FOREIGN KEY (car_val_identifiant) REFERENCES ref.tr_valeurparametrequalitatif_val(val_identifiant) ON UPDATE CASCADE;

-------------------------------------------------------


ALTER TABLE ONLY tj_coefficientconversion_coe
    DROP CONSTRAINT IF EXISTS c_fk_coe_org_code;
    
ALTER TABLE ONLY tj_coefficientconversion_coe
    ADD CONSTRAINT c_fk_coe_org_code FOREIGN KEY (coe_org_code) REFERENCES ref.ts_organisme_org(org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY tj_coefficientconversion_coe
    DROP CONSTRAINT IF EXISTS c_fk_coe_qte_code;

ALTER TABLE ONLY tj_coefficientconversion_coe
    ADD CONSTRAINT c_fk_coe_qte_code FOREIGN KEY (coe_qte_code) REFERENCES ref.tr_typequantitelot_qte(qte_code) ON UPDATE CASCADE;


ALTER TABLE ONLY tj_coefficientconversion_coe
    DROP CONSTRAINT IF EXISTS c_fk_coe_std_code;

ALTER TABLE ONLY tj_coefficientconversion_coe
    ADD CONSTRAINT c_fk_coe_std_code FOREIGN KEY (coe_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code) ON UPDATE CASCADE;

ALTER TABLE ONLY tj_coefficientconversion_coe
    DROP CONSTRAINT IF EXISTS c_fk_coe_tax_code;

ALTER TABLE ONLY tj_coefficientconversion_coe
    ADD CONSTRAINT c_fk_coe_tax_code FOREIGN KEY (coe_tax_code) REFERENCES ref.tr_taxon_tax(tax_code) ON UPDATE CASCADE;


-------------------------------------------------------


ALTER TABLE ONLY tj_dfesttype_dft
    DROP CONSTRAINT IF EXISTS c_fk_dft_df_identifiant;

ALTER TABLE ONLY tj_dfesttype_dft
    ADD CONSTRAINT c_fk_dft_df_identifiant FOREIGN KEY (dft_df_identifiant, dft_org_code) REFERENCES tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY tj_dfesttype_dft
    DROP CONSTRAINT IF EXISTS c_fk_dft_org_code;

ALTER TABLE ONLY tj_dfesttype_dft
    ADD CONSTRAINT c_fk_dft_org_code FOREIGN KEY (dft_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;

-------------------------------------------------------


-- ? contrainte unique manquante? 


ALTER TABLE ONLY t_dispositiffranchissement_dif
    DROP CONSTRAINT IF EXISTS c_uk_dif_dis_identifiant CASCADE;

ALTER TABLE ONLY t_dispositiffranchissement_dif
    ADD CONSTRAINT  c_uk_dif_dis_identifiant unique (dif_dis_identifiant,dif_org_code);

    --(manque une contrainte unique dans la table t_dispositifcomptage_dic
ALTER TABLE t_dispositifcomptage_dic DROP CONSTRAINT IF EXISTS c_uk_dic_dis_identifiant CASCADE;

ALTER TABLE t_dispositifcomptage_dic ADD CONSTRAINT c_uk_dic_dis_identifiant unique (dic_dis_identifiant,dic_org_code);

    
ALTER TABLE ONLY t_dispositifcomptage_dic
    DROP CONSTRAINT IF EXISTS c_fk_dic_dif_identifiant ;

ALTER TABLE ONLY t_dispositifcomptage_dic
    ADD CONSTRAINT c_fk_dic_dif_identifiant FOREIGN KEY (dic_dif_identifiant, dic_org_code) REFERENCES t_dispositiffranchissement_dif(dif_dis_identifiant, dif_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_dispositifcomptage_dic
    DROP CONSTRAINT IF EXISTS c_fk_dic_dis_identifiant;
    
ALTER TABLE ONLY t_dispositifcomptage_dic
    ADD CONSTRAINT c_fk_dic_dis_identifiant FOREIGN KEY (dic_dis_identifiant, dic_org_code) REFERENCES tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_dispositifcomptage_dic
    DROP CONSTRAINT IF EXISTS c_fk_dic_org_code;

ALTER TABLE ONLY t_dispositifcomptage_dic
    ADD CONSTRAINT c_fk_dic_org_code FOREIGN KEY (dic_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


ALTER TABLE ONLY t_dispositifcomptage_dic
    DROP CONSTRAINT IF EXISTS c_fk_dic_tdc_code;

ALTER TABLE ONLY t_dispositifcomptage_dic
    ADD CONSTRAINT c_fk_dic_tdc_code FOREIGN KEY (dic_tdc_code) REFERENCES ref.tr_typedc_tdc(tdc_code) ON UPDATE CASCADE;



-------------------------------------------------------




ALTER TABLE ONLY t_dispositiffranchissement_dif
    DROP CONSTRAINT IF EXISTS c_fk_dif_dis_identifiant;

ALTER TABLE ONLY t_dispositiffranchissement_dif
    ADD CONSTRAINT c_fk_dif_dis_identifiant FOREIGN KEY (dif_dis_identifiant, dif_org_code) REFERENCES tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_dispositiffranchissement_dif
    DROP CONSTRAINT IF EXISTS c_fk_dif_org_code;

ALTER TABLE ONLY t_dispositiffranchissement_dif
    ADD CONSTRAINT c_fk_dif_org_code FOREIGN KEY (dif_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


ALTER TABLE ONLY t_dispositiffranchissement_dif
    DROP CONSTRAINT IF EXISTS c_fk_dif_ouv_identifiant;

ALTER TABLE ONLY t_dispositiffranchissement_dif
    ADD CONSTRAINT c_fk_dif_ouv_identifiant FOREIGN KEY (dif_ouv_identifiant, dif_org_code) REFERENCES t_ouvrage_ouv(ouv_identifiant, ouv_org_code) ON UPDATE CASCADE;


-------------------------------------------------------




ALTER TABLE ONLY tj_dfestdestinea_dtx
    DROP CONSTRAINT IF EXISTS c_fk_dtx_dif_identifiant;

ALTER TABLE ONLY tj_dfestdestinea_dtx
    ADD CONSTRAINT c_fk_dtx_dif_identifiant FOREIGN KEY (dtx_dif_identifiant, dtx_org_code) REFERENCES tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY tj_dfestdestinea_dtx
    DROP CONSTRAINT IF EXISTS c_fk_dtx_org_code;

ALTER TABLE ONLY tj_dfestdestinea_dtx
    ADD CONSTRAINT c_fk_dtx_org_code FOREIGN KEY (dtx_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


ALTER TABLE ONLY tj_dfestdestinea_dtx
    DROP CONSTRAINT IF EXISTS c_fk_dtx_tax_code;

ALTER TABLE ONLY tj_dfestdestinea_dtx
    ADD CONSTRAINT c_fk_dtx_tax_code FOREIGN KEY (dtx_tax_code) REFERENCES ref.tr_taxon_tax(tax_code) ON UPDATE CASCADE;



-------------------------------------------------------
-- valeur manquante (iav)

ALTER TABLE tj_dfesttype_dft
  DROP CONSTRAINT IF EXISTS c_fk_dft_tdf_code;


ALTER TABLE tj_dfesttype_dft
  ADD CONSTRAINT c_fk_dft_tdf_code FOREIGN KEY (dft_tdf_code)
      REFERENCES ref.tr_typedf_tdf (tdf_code) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;

-------------------------------------------------------



ALTER TABLE ONLY tj_conditionenvironnementale_env
    DROP CONSTRAINT IF EXISTS c_fk_env_org_code;

ALTER TABLE ONLY tj_conditionenvironnementale_env
    ADD CONSTRAINT c_fk_env_org_code FOREIGN KEY (env_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


ALTER TABLE ONLY tj_conditionenvironnementale_env
    DROP CONSTRAINT IF EXISTS c_fk_env_stm_identifiant;

ALTER TABLE ONLY tj_conditionenvironnementale_env
    ADD CONSTRAINT c_fk_env_stm_identifiant FOREIGN KEY (env_stm_identifiant, env_org_code) REFERENCES tj_stationmesure_stm(stm_identifiant, stm_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY tj_conditionenvironnementale_env
    DROP CONSTRAINT IF EXISTS c_fk_env_val_identifiant;

ALTER TABLE ONLY tj_conditionenvironnementale_env
    ADD CONSTRAINT c_fk_env_val_identifiant FOREIGN KEY (env_val_identifiant) REFERENCES ref.tr_valeurparametrequalitatif_val(val_identifiant) ON UPDATE CASCADE;



-------------------------------------------------------

ALTER TABLE ONLY t_lot_lot
    DROP CONSTRAINT IF EXISTS c_fk_lot_ope_identifiant;

ALTER TABLE ONLY t_lot_lot
    ADD CONSTRAINT c_fk_lot_ope_identifiant FOREIGN KEY (lot_ope_identifiant,lot_org_code) REFERENCES t_operation_ope(ope_identifiant,ope_org_code) ON UPDATE CASCADE;




ALTER TABLE ONLY t_lot_lot
    DROP CONSTRAINT IF EXISTS c_fk_lot_dev_code;

ALTER TABLE ONLY t_lot_lot
    ADD CONSTRAINT c_fk_lot_dev_code FOREIGN KEY (lot_dev_code) REFERENCES ref.tr_devenirlot_dev(dev_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_lot_lot
    DROP CONSTRAINT IF EXISTS c_fk_lot_lot_identifiant;

ALTER TABLE ONLY t_lot_lot
    ADD CONSTRAINT c_fk_lot_lot_identifiant FOREIGN KEY (lot_lot_identifiant, lot_org_code) REFERENCES t_lot_lot(lot_identifiant, lot_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_lot_lot
    DROP CONSTRAINT IF EXISTS c_fk_lot_org_code;

ALTER TABLE ONLY t_lot_lot
    ADD CONSTRAINT c_fk_lot_org_code FOREIGN KEY (lot_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


ALTER TABLE ONLY t_lot_lot
    DROP CONSTRAINT IF EXISTS c_fk_lot_qte_code;

ALTER TABLE ONLY t_lot_lot
    ADD CONSTRAINT c_fk_lot_qte_code FOREIGN KEY (lot_qte_code) REFERENCES ref.tr_typequantitelot_qte(qte_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_lot_lot
    DROP CONSTRAINT IF EXISTS c_fk_lot_std_code;

ALTER TABLE ONLY t_lot_lot
    ADD CONSTRAINT c_fk_lot_std_code FOREIGN KEY (lot_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_lot_lot
    DROP CONSTRAINT IF EXISTS c_fk_lot_tax_code;

ALTER TABLE ONLY t_lot_lot
    ADD CONSTRAINT c_fk_lot_tax_code FOREIGN KEY (lot_tax_code) REFERENCES ref.tr_taxon_tax(tax_code) ON UPDATE CASCADE;


-------------------------------------------------------




ALTER TABLE ONLY t_marque_mqe
    DROP CONSTRAINT IF EXISTS c_fk_mqe_loc_code;

ALTER TABLE ONLY t_marque_mqe
    ADD CONSTRAINT c_fk_mqe_loc_code FOREIGN KEY (mqe_loc_code) REFERENCES ref.tr_localisationanatomique_loc(loc_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_marque_mqe
    DROP CONSTRAINT IF EXISTS c_fk_mqe_nmq_code;

ALTER TABLE ONLY t_marque_mqe
    ADD CONSTRAINT c_fk_mqe_nmq_code FOREIGN KEY (mqe_nmq_code) REFERENCES ref.tr_naturemarque_nmq(nmq_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_marque_mqe
    DROP CONSTRAINT IF EXISTS c_fk_mqe_omq_reference;

ALTER TABLE ONLY t_marque_mqe
    ADD CONSTRAINT c_fk_mqe_omq_reference FOREIGN KEY (mqe_omq_reference, mqe_org_code) REFERENCES t_operationmarquage_omq(omq_reference, omq_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_marque_mqe
    DROP CONSTRAINT IF EXISTS c_fk_mqe_org_code;

ALTER TABLE ONLY t_marque_mqe
    ADD CONSTRAINT c_fk_mqe_org_code FOREIGN KEY (mqe_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;



-------------------------------------------------------



ALTER TABLE ONLY t_operationmarquage_omq
    DROP CONSTRAINT IF EXISTS c_fk_omq_org_code;

ALTER TABLE ONLY t_operationmarquage_omq
    ADD CONSTRAINT c_fk_omq_org_code FOREIGN KEY (omq_org_code) REFERENCES ref.ts_organisme_org(org_code) ON UPDATE CASCADE;

-------------------------------------------------------



ALTER TABLE ONLY t_operation_ope
    DROP CONSTRAINT IF EXISTS c_fk_ope_dic_identifiant;

ALTER TABLE ONLY t_operation_ope
    ADD CONSTRAINT c_fk_ope_dic_identifiant FOREIGN KEY (ope_dic_identifiant, ope_org_code) REFERENCES tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_operation_ope
    DROP CONSTRAINT IF EXISTS c_fk_ope_org_code;

ALTER TABLE ONLY t_operation_ope
    ADD CONSTRAINT c_fk_ope_org_code FOREIGN KEY (ope_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


-------------------------------------------------------


ALTER TABLE ONLY t_ouvrage_ouv
    DROP CONSTRAINT IF EXISTS c_fk_ouv_nov_code;

ALTER TABLE ONLY t_ouvrage_ouv
    ADD CONSTRAINT c_fk_ouv_nov_code FOREIGN KEY (ouv_nov_code) REFERENCES ref.tr_natureouvrage_nov(nov_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_ouvrage_ouv
    DROP CONSTRAINT IF EXISTS c_fk_ouv_org_code;

ALTER TABLE ONLY t_ouvrage_ouv
    ADD CONSTRAINT c_fk_ouv_org_code FOREIGN KEY (ouv_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;

-- contrainte manquante au sch�ma IAV

ALTER TABLE t_ouvrage_ouv
  DROP CONSTRAINT IF EXISTS c_fk_ouv_sta_code;

ALTER TABLE t_ouvrage_ouv
  ADD CONSTRAINT c_fk_ouv_sta_code FOREIGN KEY (ouv_sta_code, ouv_org_code)
      REFERENCES t_station_sta (sta_code, sta_org_code) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;

-------------------------------------------------------




ALTER TABLE ONLY tj_pathologieconstatee_pco
    DROP CONSTRAINT IF EXISTS c_fk_pco_loc_code;

ALTER TABLE ONLY tj_pathologieconstatee_pco
    ADD CONSTRAINT c_fk_pco_loc_code FOREIGN KEY (pco_loc_code) REFERENCES ref.tr_localisationanatomique_loc(loc_code) ON UPDATE CASCADE;


ALTER TABLE ONLY tj_pathologieconstatee_pco
    DROP CONSTRAINT IF EXISTS c_fk_pco_lot_identifiant;

ALTER TABLE ONLY tj_pathologieconstatee_pco
    ADD CONSTRAINT c_fk_pco_lot_identifiant FOREIGN KEY (pco_lot_identifiant, pco_org_code) REFERENCES t_lot_lot(lot_identifiant, lot_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY tj_pathologieconstatee_pco
    DROP CONSTRAINT IF EXISTS c_fk_pco_org_code ;

ALTER TABLE ONLY tj_pathologieconstatee_pco
    ADD CONSTRAINT c_fk_pco_org_code FOREIGN KEY (pco_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


ALTER TABLE ONLY tj_pathologieconstatee_pco
    DROP CONSTRAINT IF EXISTS c_fk_pco_pat_code ;

ALTER TABLE ONLY tj_pathologieconstatee_pco
    ADD CONSTRAINT c_fk_pco_pat_code FOREIGN KEY (pco_pat_code) REFERENCES ref.tr_pathologie_pat(pat_code) ON UPDATE CASCADE;

/*
--v0.4 insertion d''une contrainte vers une nouvelle table.

 alter table tj_pathologieconstatee_pco DROP CONSTRAINT IF EXISTS c_fk_pco_imp_code ;

 alter table tj_pathologieconstatee_pco ADD CONSTRAINT c_fk_pco_imp_code FOREIGN KEY (pco_imp_code)
      REFERENCES ref.tr_importancepatho_imp (imp_code) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION; 
*/

-------------------------------------------------------



ALTER TABLE ONLY t_periodefonctdispositif_per
    DROP CONSTRAINT IF EXISTS c_fk_per_dis_identifiant;

ALTER TABLE ONLY t_periodefonctdispositif_per
    ADD CONSTRAINT c_fk_per_dis_identifiant FOREIGN KEY (per_dis_identifiant, per_org_code) REFERENCES tg_dispositif_dis(dis_identifiant, dis_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY t_periodefonctdispositif_per
    DROP CONSTRAINT IF EXISTS c_fk_per_org_code ;

ALTER TABLE ONLY t_periodefonctdispositif_per
    ADD CONSTRAINT c_fk_per_org_code FOREIGN KEY (per_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


ALTER TABLE ONLY t_periodefonctdispositif_per
    DROP CONSTRAINT IF EXISTS c_fk_per_tar_code;

ALTER TABLE ONLY t_periodefonctdispositif_per
    ADD CONSTRAINT c_fk_per_tar_code FOREIGN KEY (per_tar_code) REFERENCES ref.tr_typearretdisp_tar(tar_code) ON UPDATE CASCADE;



-------------------------------------------------------



ALTER TABLE ONLY tj_prelevementlot_prl
    DROP CONSTRAINT IF EXISTS c_fk_prl_loc_code;

ALTER TABLE ONLY tj_prelevementlot_prl
    ADD CONSTRAINT c_fk_prl_loc_code FOREIGN KEY (prl_loc_code) REFERENCES ref.tr_localisationanatomique_loc(loc_code) ON UPDATE CASCADE;



ALTER TABLE ONLY tj_prelevementlot_prl
    DROP CONSTRAINT IF EXISTS c_fk_prl_lot_identifiant;

ALTER TABLE ONLY tj_prelevementlot_prl
    ADD CONSTRAINT c_fk_prl_lot_identifiant FOREIGN KEY (prl_lot_identifiant, prl_org_code) REFERENCES t_lot_lot(lot_identifiant, lot_org_code) ON UPDATE CASCADE;


ALTER TABLE ONLY tj_prelevementlot_prl
    DROP CONSTRAINT IF EXISTS c_fk_prl_typeprelevement;

ALTER TABLE ONLY tj_prelevementlot_prl
    ADD CONSTRAINT c_fk_prl_typeprelevement FOREIGN KEY (prl_pre_typeprelevement) REFERENCES ref.tr_prelevement_pre(pre_typeprelevement) ON UPDATE CASCADE;

ALTER TABLE ONLY tj_prelevementlot_prl
    DROP CONSTRAINT IF EXISTS c_fk_prl_org_code;

ALTER TABLE ONLY tj_prelevementlot_prl
    ADD CONSTRAINT c_fk_prl_org_code FOREIGN KEY (prl_org_code) REFERENCES ref.ts_organisme_org (org_code) ON UPDATE CASCADE;

ALTER TABLE ONLY tj_prelevementlot_prl
    DROP CONSTRAINT IF EXISTS c_fk_prl_pre_nom;

ALTER TABLE ONLY tj_prelevementlot_prl 
    ADD CONSTRAINT c_fk_prl_pre_nom FOREIGN KEY (prl_pre_typeprelevement)
      REFERENCES ref.tr_prelevement_pre (pre_typeprelevement) MATCH SIMPLE
      ON UPDATE CASCADE;

-------------------------------------------------------


ALTER TABLE ONLY t_station_sta
    DROP CONSTRAINT IF EXISTS c_fk_sta_org_code;

ALTER TABLE ONLY t_station_sta
    ADD CONSTRAINT c_fk_sta_org_code FOREIGN KEY (sta_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;


-------------------------------------------------------



ALTER TABLE ONLY tg_dispositif_dis
    DROP CONSTRAINT IF EXISTS c_fk_sta_org_code;

ALTER TABLE ONLY tg_dispositif_dis
    ADD CONSTRAINT c_fk_sta_org_code FOREIGN KEY (dis_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;




-------------------------------------------------------






ALTER TABLE ONLY tj_stationmesure_stm
    DROP CONSTRAINT IF EXISTS c_fk_stm_org_code;

ALTER TABLE ONLY tj_stationmesure_stm
    ADD CONSTRAINT c_fk_stm_org_code FOREIGN KEY (stm_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;



ALTER TABLE ONLY tj_stationmesure_stm
    DROP CONSTRAINT IF EXISTS c_fk_stm_par_code;

ALTER TABLE ONLY tj_stationmesure_stm
    ADD CONSTRAINT c_fk_stm_par_code FOREIGN KEY (stm_par_code) REFERENCES ref.tg_parametre_par(par_code) ON UPDATE CASCADE;



ALTER TABLE ONLY tj_stationmesure_stm
    DROP CONSTRAINT IF EXISTS c_fk_stm_sta_code;

ALTER TABLE ONLY tj_stationmesure_stm
    ADD CONSTRAINT c_fk_stm_sta_code FOREIGN KEY (stm_sta_code, stm_org_code) REFERENCES t_station_sta(sta_code, sta_org_code) ON UPDATE CASCADE;





-------------------------------------------------------



ALTER TABLE ONLY tj_tauxechappement_txe
    DROP CONSTRAINT IF EXISTS c_fk_txe_ech_code;

ALTER TABLE ONLY tj_tauxechappement_txe
    ADD CONSTRAINT c_fk_txe_ech_code FOREIGN KEY (txe_ech_code) REFERENCES ref.tr_niveauechappement_ech(ech_code) ON UPDATE CASCADE;



ALTER TABLE ONLY tj_tauxechappement_txe
    DROP CONSTRAINT IF EXISTS c_fk_txe_org_code;

ALTER TABLE ONLY tj_tauxechappement_txe
    ADD CONSTRAINT c_fk_txe_org_code FOREIGN KEY (txe_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;

/*
--changement v0.4 ci dessous la cl� �trang�re se rattache � une station 
-- ATTENTION NECESSSITE V0.4

ALTER TABLE ONLY tj_tauxechappement_txe
    DROP CONSTRAINT IF EXISTS c_fk_txe_sta_identifiant;

ALTER TABLE ONLY tj_tauxechappement_txe
    ADD CONSTRAINT c_fk_txe_sta_identifiant FOREIGN KEY (txe_sta_identifiant, txe_org_code) REFERENCES t_station_sta(sta_identifiant, ouv_org_code) ON UPDATE CASCADE;
*/

ALTER TABLE ONLY tj_tauxechappement_txe
    DROP CONSTRAINT IF EXISTS c_fk_txe_std_code;

ALTER TABLE ONLY tj_tauxechappement_txe
    ADD CONSTRAINT c_fk_txe_std_code FOREIGN KEY (txe_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code) ON UPDATE CASCADE;



ALTER TABLE ONLY tj_tauxechappement_txe
    DROP CONSTRAINT IF EXISTS c_fk_txe_tax_code;

ALTER TABLE ONLY tj_tauxechappement_txe
    ADD CONSTRAINT c_fk_txe_tax_code FOREIGN KEY (txe_tax_code) REFERENCES ref.tr_taxon_tax(tax_code) ON UPDATE CASCADE;


-------------------------------------------------------


-- cl� manquante org

ALTER TABLE ONLY ts_taxonvideo_txv
    DROP CONSTRAINT IF EXISTS c_fk_txv_org_code;

ALTER TABLE ONLY ts_taxonvideo_txv
    ADD CONSTRAINT c_fk_txv_org_code FOREIGN KEY (txv_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;

ALTER TABLE ONLY ts_taxonvideo_txv
    DROP CONSTRAINT IF EXISTS c_fk_txv_tax_code ;

ALTER TABLE ONLY ts_taxonvideo_txv
    ADD CONSTRAINT c_fk_txv_tax_code FOREIGN KEY (txv_tax_code) REFERENCES ref.tr_taxon_tax(tax_code) ON UPDATE CASCADE;

ALTER TABLE ONLY ts_taxonvideo_txv
    DROP CONSTRAINT IF EXISTS c_fk_std_code;

ALTER TABLE ONLY ts_taxonvideo_txv
    ADD CONSTRAINT c_fk_std_code FOREIGN KEY (txv_std_code) REFERENCES ref.tr_stadedeveloppement_std(std_code) ON UPDATE CASCADE;


-------------------------------------------------------




ALTER TABLE ONLY ts_taillevideo_tav
    DROP CONSTRAINT IF EXISTS c_fk_tav_dic_identifiant;

ALTER TABLE ONLY ts_taillevideo_tav
    ADD CONSTRAINT c_fk_tav_dic_identifiant FOREIGN KEY (tav_dic_identifiant, tav_org_code) REFERENCES t_dispositifcomptage_dic(dic_dis_identifiant, dic_org_code) ON UPDATE CASCADE;

-- cl� manquante org

ALTER TABLE ONLY ts_taillevideo_tav
    DROP CONSTRAINT IF EXISTS c_fk_tav_org_code;

ALTER TABLE ONLY ts_taillevideo_tav
    ADD CONSTRAINT c_fk_tav_org_code FOREIGN KEY (tav_org_code) REFERENCES ref.ts_organisme_org(org_code) MATCH FULL ON UPDATE CASCADE;




');


GRANT SELECT ON ALL TABLES IN SCHEMA information_schema TO test;
