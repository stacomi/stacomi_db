--=================================
--correction base version 0.4
--=================================

--correction sur les paramètres qualitatifs

DELETE FROM "ref".tr_valeurparametrequalitatif_val 
WHERE val_qal_code = 'A126' 
AND EXISTS (SELECT 1 FROM "ref".tr_valeurparametrequalitatif_val WHERE val_qal_code = 'A126'); --sert à s'assurer que l'utilisateur a bien le val_qal_code 'A126'

INSERT INTO "ref".tg_parametre_par (par_code,
par_nom,
par_unite,
par_nature,
par_definition)
SELECT 'B003', 'Classe de taille (anguilles Marais Poitevin)',  
'Sans objet',  'BIOLOGIQUE', 
'Classe pour la séparation des petites et grandes anguilles lors du piégeage'
ON CONFLICT (par_code) DO NOTHING;

INSERT INTO "ref".tr_parametrequalitatif_qal (qal_par_code,qal_valeurs_possibles) 
SELECT 'B003',  '<150;>150'
ON CONFLICT (qal_par_code) DO NOTHING;

INSERT INTO "ref".tr_valeurparametrequalitatif_val (val_identifiant,
val_qal_code,
val_rang,
val_libelle) 
SELECT 60,  'B003',  1, '<150'
ON CONFLICT (val_identifiant) DO NOTHING;

INSERT INTO "ref".tr_valeurparametrequalitatif_val (val_identifiant,
val_qal_code,
val_rang,
val_libelle) 
SELECT 61,  'B003',  1, '>150'
ON CONFLICT (val_identifiant) DO NOTHING;

--On met la contrainte unique sur ts_maintenance_main (manque sur schéma ref masi applique partout pour être sûr)
--attention dans ref.ts_maintenance_main on a 2 lignes avec main_ticket=72. On fusionne le main_description et 
--on supprime la 2eme ligne
WITH pb_line AS (
    SELECT *
    FROM ref.ts_maintenance_main tmm 
    WHERE main_ticket = 72
),
merged AS (
    -- Fusionner les descriptions en concaténant les valeurs
    SELECT
        MIN(main_identifiant) AS keep_id,  -- Conserver l'id de la première ligne
        string_agg(main_description, ' - ') AS merged_description  -- Fusionner les descriptions
    FROM pb_line
    GROUP BY main_ticket
)
-- Mettre à jour la ligne fusionnée
UPDATE ref.ts_maintenance_main
SET main_description = m.merged_description
FROM merged m
WHERE ref.ts_maintenance_main.main_ticket = 72
AND ref.ts_maintenance_main.main_identifiant = m.keep_id;

-- Supprimer la deuxième ligne
WITH pb_line AS (
    SELECT *
    FROM ref.ts_maintenance_main tmm 
    WHERE main_ticket = 72
)
DELETE FROM ref.ts_maintenance_main
WHERE main_ticket = 72
AND main_identifiant = (
    SELECT max(main_identifiant)
    FROM pb_line
    WHERE main_ticket = 72
);

--on ajoute une contrainte unique
SELECT ref.updatesql(
    (SELECT ARRAY(
        SELECT format('"%s"', nspname)  
        FROM pg_namespace n
        JOIN pg_roles r ON n.nspowner = r.oid
        WHERE n.nspname NOT LIKE 'pg_%' 
        AND n.nspname not in('information_schema','public')
        
    )),
'
ALTER TABLE ts_maintenance_main DROP CONSTRAINT IF EXISTS c_uk_main_ticket;
ALTER TABLE ts_maintenance_main ADD CONSTRAINT c_uk_main_ticket UNIQUE (main_ticket)
');


insert into ref.ts_maintenance_main
(
  main_ticket,
  main_description
  ) SELECT 124,'Version 0.4, Ajout d''un paramètre qualitatif classe de taille pour les anguilles'
    WHERE NOT EXISTS (
    SELECT 1 FROM ref.ts_maintenance_main WHERE main_ticket=124);

--======================
--Version 0.6.0
--======================
--------------------------------------------------------------
-- on met a jour le referentiel taxon pour la bouvière #4
--------------------------------------------------------------

WITH tax_rang_max AS (
	select max(tax_rang)+1 as max_rang
	from ref.tr_taxon_tax)
INSERT INTO ref.tr_taxon_tax (tax_code,tax_nom_latin,tax_nom_commun,tax_ntx_code,tax_rang) 
SELECT '2131','Rhodeus amarus','Bouvière',15,max_rang
FROM tax_rang_max
WHERE NOT EXISTS (
    SELECT 1 FROM ref.tr_taxon_tax WHERE tax_code = '2131'
);

insert into ref.ts_maintenance_main
(
  main_ticket,
  main_description
  ) SELECT 4,'Update to version 0.6.0, update taxa ref table bouvière'
    WHERE NOT EXISTS (
    SELECT 1 FROM ref.ts_maintenance_main WHERE main_ticket=4);

------------------------------------
--on ajoute le Crapet de roche #5
--------------------------------------
  
WITH tax_rang_max AS (
	select max(tax_rang)+1 as max_rang
	from ref.tr_taxon_tax)
INSERT INTO ref.tr_taxon_tax (tax_code,tax_nom_latin,tax_nom_commun,tax_ntx_code,tax_rang) 
SELECT '2048','Ambloplites rupestris','Crapet de roche',15,max_rang
FROM tax_rang_max
WHERE NOT EXISTS (
    SELECT 1 FROM ref.tr_taxon_tax WHERE tax_code = '2048');

insert into ref.ts_maintenance_main
(
  main_ticket,
  main_description
  ) SELECT 5,'Update to version 0.6.0, update taxa ref table crapet de roche'
    WHERE NOT EXISTS (
    SELECT 1 FROM ref.ts_maintenance_main WHERE main_ticket=5);

-----------------------------------
--on ajoute le crabe chinois #8
----------------------------------
  
WITH tax_rang_max AS (
	select max(tax_rang)+1 as max_rang
	from ref.tr_taxon_tax)
INSERT INTO ref.tr_taxon_tax (tax_code,tax_nom_latin,tax_nom_commun,tax_ntx_code,tax_rang) 
SELECT '879','Eriocheir sinensis','Crabe chinois',15,max_rang
FROM tax_rang_max
WHERE NOT EXISTS (
    SELECT 1 FROM ref.tr_taxon_tax WHERE tax_code = '879');

insert into ref.ts_maintenance_main
(
  main_ticket,
  main_description
  ) SELECT 8,'Update to version 0.6.0, update taxa ref table crabe chinois'
    WHERE NOT EXISTS (
    SELECT 1 FROM ref.ts_maintenance_main WHERE main_ticket=8);
   
-----------------------------------
--on ajoute 13 taxons
----------------------------------

WITH tax_rang_max AS (
	select max(tax_rang)+1 as max_rang
	from ref.tr_taxon_tax)
INSERT INTO ref.tr_taxon_tax (tax_code,tax_nom_latin,tax_nom_commun,tax_ntx_code,tax_rang) 
SELECT '3303','Rana','Grenouille',13,max_rang
FROM tax_rang_max
WHERE NOT EXISTS (
    SELECT 1 FROM ref.tr_taxon_tax WHERE tax_code = '3303');

WITH tax_rang_max AS (
	select max(tax_rang)+1 as max_rang
	from ref.tr_taxon_tax)
INSERT INTO ref.tr_taxon_tax (tax_code,tax_nom_latin,tax_nom_commun,tax_ntx_code,tax_rang) 
SELECT '31693','Atherina Presbyter','Athérina Presbyter',15,max_rang
FROM tax_rang_max
WHERE NOT EXISTS (
    SELECT 1 FROM ref.tr_taxon_tax WHERE tax_code = '31693');

WITH tax_rang_max AS (
	select max(tax_rang)+1 as max_rang
	from ref.tr_taxon_tax)
INSERT INTO ref.tr_taxon_tax (tax_code,tax_nom_latin,tax_nom_commun,tax_ntx_code,tax_rang) 
SELECT '2028','Procambarus clarkii','Ecrevisse Clarkii',15,max_rang
FROM tax_rang_max
WHERE NOT EXISTS (
    SELECT 1 FROM ref.tr_taxon_tax WHERE tax_code = '2028');

WITH tax_rang_max AS (
	select max(tax_rang)+1 as max_rang
	from ref.tr_taxon_tax)
INSERT INTO ref.tr_taxon_tax (tax_code,tax_nom_latin,tax_nom_commun,tax_ntx_code,tax_rang) 
SELECT '56870','Oncorhynchus gorbuscha','Saumon rose',15,max_rang
FROM tax_rang_max
WHERE NOT EXISTS (
    SELECT 1 FROM ref.tr_taxon_tax WHERE tax_code = '56870');

WITH tax_rang_max AS (
	select max(tax_rang)+1 as max_rang
	from ref.tr_taxon_tax)
INSERT INTO ref.tr_taxon_tax (tax_code,tax_nom_latin,tax_nom_commun,tax_ntx_code,tax_rang) 
SELECT '2110','Cyprinus carpio','Carpe commune',15,max_rang
FROM tax_rang_max
WHERE NOT EXISTS (
    SELECT 1 FROM ref.tr_taxon_tax WHERE tax_code = '2110');

WITH tax_rang_max AS (
	select max(tax_rang)+1 as max_rang
	from ref.tr_taxon_tax)
INSERT INTO ref.tr_taxon_tax (tax_code,tax_nom_latin,tax_nom_commun,tax_ntx_code,tax_rang) 
SELECT '29117','Neogobius melanostomus','Gobie à tache noire',15,max_rang
FROM tax_rang_max
WHERE NOT EXISTS (
    SELECT 1 FROM ref.tr_taxon_tax WHERE tax_code = '29117');
   
   
WITH tax_rang_max AS (
	select max(tax_rang)+1 as max_rang
	from ref.tr_taxon_tax)
INSERT INTO ref.tr_taxon_tax (tax_code,tax_nom_latin,tax_nom_commun,tax_ntx_code,tax_rang) 
SELECT '2167','Pungitius pungitius','Epinochette',15,max_rang
FROM tax_rang_max
WHERE NOT EXISTS (
    SELECT 1 FROM ref.tr_taxon_tax WHERE tax_code = '2167');

WITH tax_rang_max AS (
	select max(tax_rang)+1 as max_rang
	from ref.tr_taxon_tax)
INSERT INTO ref.tr_taxon_tax (tax_code,tax_nom_latin,tax_nom_commun,tax_ntx_code,tax_rang) 
SELECT '871','Orconectes limosus','Ecrevisse américaine',15,max_rang
FROM tax_rang_max
WHERE NOT EXISTS (
    SELECT 1 FROM ref.tr_taxon_tax WHERE tax_code = '871');

WITH tax_rang_max AS (
	select max(tax_rang)+1 as max_rang
	from ref.tr_taxon_tax)
INSERT INTO ref.tr_taxon_tax (tax_code,tax_nom_latin,tax_nom_commun,tax_ntx_code,tax_rang) 
SELECT '2165','Gasterosteus aculeatus','Epinoche',15,max_rang
FROM tax_rang_max
WHERE NOT EXISTS (
    SELECT 1 FROM ref.tr_taxon_tax WHERE tax_code = '2165');
   
WITH tax_rang_max AS (
	select max(tax_rang)+1 as max_rang
	from ref.tr_taxon_tax)
INSERT INTO ref.tr_taxon_tax (tax_code,tax_nom_latin,tax_nom_commun,tax_ntx_code,tax_rang) 
SELECT '2226','Salvelinus alpinus','Omble chevalier',15,max_rang
FROM tax_rang_max
WHERE NOT EXISTS (
    SELECT 1 FROM ref.tr_taxon_tax WHERE tax_code = '2226');
   
WITH tax_rang_max AS (
	select max(tax_rang)+1 as max_rang
	from ref.tr_taxon_tax)
INSERT INTO ref.tr_taxon_tax (tax_code,tax_nom_latin,tax_nom_commun,tax_ntx_code,tax_rang) 
SELECT '2218','Oncorhynchus kisutch','Saumon coho',15,max_rang
FROM tax_rang_max
WHERE NOT EXISTS (
    SELECT 1 FROM ref.tr_taxon_tax WHERE tax_code = '2218');
   
WITH tax_rang_max AS (
	select max(tax_rang)+1 as max_rang
	from ref.tr_taxon_tax)
INSERT INTO ref.tr_taxon_tax (tax_code,tax_nom_latin,tax_nom_commun,tax_ntx_code,tax_rang) 
SELECT '5204','Abramis sapa','Brême du Danube',15,max_rang
FROM tax_rang_max
WHERE NOT EXISTS (
    SELECT 1 FROM ref.tr_taxon_tax WHERE tax_code = '5204');
   
WITH tax_rang_max AS (
	select max(tax_rang)+1 as max_rang
	from ref.tr_taxon_tax)
INSERT INTO ref.tr_taxon_tax (tax_code,tax_nom_latin,tax_nom_commun,tax_ntx_code,tax_rang) 
SELECT '00','Natrix natrix','Couleuvre à collier',15,max_rang
FROM tax_rang_max
WHERE NOT EXISTS (
    SELECT 1 FROM ref.tr_taxon_tax WHERE tax_code = '00');
   
insert into ref.ts_maintenance_main
(
  main_ticket,
  main_description
  ) SELECT 30,'Update to version 0.6.0, update 13 new taxa into ref.tr_taxon_tax'
    WHERE NOT EXISTS (
    SELECT 1 FROM ref.ts_maintenance_main WHERE main_ticket=30);
   
-----------------------------------
--Add tax_nom_commun to display name when species is only selected at the genus level #3
----------------------------------

 UPDATE ref.tr_taxon_tax as taxon
    SET
    tax_nom_commun = genres.tax_nom_commun
    FROM (values
    ('Lampetra', 'Lamproie (genre)'),
    ('Barbus', 'Barbeau (genre)'),
    ('Micropterus','Black bass (genre)'),
    ('Alosa','Alose (genre)'),
    ('Carassius','Carassin (genre)'),
    ('Leuciscus','Cyprinidé (genre Leuciscus)'),
    ('Chelon','Mulet (genre Chelon)'),
    ('Liza','Mulet (genre Liza)'),
    ('Salmo','Salmonidé (genre Salmo)')  
     ) as genres(tax_nom_latin, tax_nom_commun) 
     WHERE genres.tax_nom_latin = taxon.tax_nom_latin;

  
-----------------------------------
-- Change name Liza ramada to Chelon ramada #3
----------------------------------  

UPDATE ref.tr_taxon_tax set tax_nom_latin ='Chelon ramada' WHERE tax_code= '2183';   

insert into ref.ts_maintenance_main
(
  main_ticket,
  main_description
  ) SELECT 3,'Update to version 0.6.0, Add tax_nom_commun to display name when species is only selected at the genus level and change the species name of mullet'
    WHERE NOT EXISTS (
    SELECT 1 FROM ref.ts_maintenance_main WHERE main_ticket=3);

   
-----------------------------------
-- Unique constraint on tax_rang #7
-- if rang <100 then adding one to the count is wrong (it will create another duplicate) so we add 100
-- there are many 100 for those 100+row number -1 will remove duplicated values (we don't need to change the first duplicate)
----------------------------------  

WITH duplicates_tax_rang AS(
SELECT count(*) OVER (PARTITION BY tax_rang) AS c, ROW_NUMBER()  OVER (PARTITION BY tax_rang) AS rn, tax_code, tax_rang FROM "ref".tr_taxon_tax
) 
--SELECT * FROM duplicates_tax_rang WHERE c>1
UPDATE "ref".tr_taxon_tax SET tax_rang =
	CASE WHEN tr_taxon_tax.tax_rang<100 THEN tr_taxon_tax.tax_rang+200
	ELSE tr_taxon_tax.tax_rang+rn-1 END
FROM duplicates_tax_rang 
WHERE duplicates_tax_rang.tax_code=tr_taxon_tax.tax_code
AND c>1
AND rn>1 ;

ALTER TABLE "ref".tr_taxon_tax DROP CONSTRAINT IF EXISTS c_uk_tax_rang;
ALTER TABLE "ref".tr_taxon_tax ADD CONSTRAINT c_uk_tax_rang UNIQUE(tax_rang);

insert into ref.ts_maintenance_main
(
  main_ticket,
  main_description
  ) SELECT 7,'Update to version 0.6.0, Add unique constraint on rank in tr_taxon_tax.tax_rang'
    WHERE NOT EXISTS (
    SELECT 1 FROM ref.ts_maintenance_main WHERE main_ticket=7);

   
--------------------------
-- #26 ajout cyprinidae
--------------------------
INSERT INTO "ref".tr_niveautaxonomique_ntx
(ntx_code, ntx_libelle, ntx_mnemonique)
VALUES('10', 'Famille', 'F')
ON CONFLICT (ntx_code) DO NOTHING;

UPDATE "ref".tr_niveautaxonomique_ntx
SET ntx_mnemonique = 'ES'
WHERE ntx_code = '15';

-- correction pour mise en conformité avec eaufrance https://id.eaufrance.fr/nsa/222#10
update ref.tr_taxon_tax set tax_ntx_code=10 where tax_code='2084';

/*Dans le cas où l'utilisateur n'aurait pas encore les cyprinidés dans sa table de référence
 * INSERT INTO ref.tr_taxon_tax (tax_code,tax_nom_latin,tax_nom_commun,tax_ntx_code,tax_rang) 
SELECT '2084','Cyprinidae','Cyprinidés',10,max_rang
FROM tax_rang_max
WHERE NOT EXISTS (
    SELECT 1 FROM ref.tr_taxon_tax WHERE tax_code = '2084');*/

insert into ref.ts_maintenance_main
(
  main_ticket,
  main_description
  ) SELECT 26,'Update to version 0.6.0, update taxa ref famille Cyprinidae'
    WHERE NOT EXISTS (
    SELECT 1 FROM ref.ts_maintenance_main WHERE main_ticket=26);

--========================================================
-- Creation de cles d'unicite sur les tables bjo et bme
--========================================================
-- On règle d'abord les problèmes de valeurs dupliquées liees au manque de cette contrainte d'unicite

SELECT ref.updatesql(
    (SELECT ARRAY(
        SELECT format('"%s"', nspname)  
        FROM pg_namespace n
        JOIN pg_roles r ON n.nspowner = r.oid
        WHERE n.nspname NOT LIKE 'pg_%' 
        AND n.nspname not in('information_schema','public','ref')
        
    )),
'
  WITH dupl AS (
    SELECT *, rank() 
     OVER (PARTITION BY bme_dis_identifiant, bme_tax_code, bme_std_code, bme_annee, bme_mois, bme_labelquantite ORDER BY bme_annee, bme_mois, bme_horodateexport DESC) 
    FROM t_bilanmigrationmensuel_bme)
  DELETE FROM t_bilanmigrationmensuel_bme 
    USING dupl 
    WHERE dupl.bme_identifiant = t_bilanmigrationmensuel_bme.bme_identifiant
    AND RANK >1;  --3490
  ');

 SELECT ref.updatesql(
    (SELECT ARRAY(
        SELECT format('"%s"', nspname)  
        FROM pg_namespace n
        JOIN pg_roles r ON n.nspowner = r.oid
        WHERE n.nspname NOT LIKE 'pg_%' 
        AND n.nspname not in('information_schema','public','ref')
        
    )),
'
  WITH dupl AS (
    SELECT *, rank() 
     OVER (PARTITION BY bjo_dis_identifiant, bjo_tax_code, bjo_std_code, bjo_annee, bjo_labelquantite ORDER BY bjo_annee, bjo_horodateexport DESC) 
    FROM t_bilanmigrationjournalier_bjo)
  DELETE FROM t_bilanmigrationjournalier_bjo 
    USING dupl 
    WHERE dupl.bjo_identifiant = t_bilanmigrationjournalier_bjo.bjo_identifiant
    AND RANK >1;  --3490
  ');

   --Ajout de la contrainte d'unicite
SELECT ref.updatesql(
    (SELECT ARRAY(
        SELECT format('"%s"', nspname)  
        FROM pg_namespace n
        JOIN pg_roles r ON n.nspowner = r.oid
        WHERE n.nspname NOT LIKE 'pg_%' 
        AND n.nspname not in('information_schema','public','ref')
        
    )),
'
  ALTER TABLE t_bilanmigrationjournalier_bjo DROP CONSTRAINT IF EXISTS C_UK_bjo;
  ALTER TABLE t_bilanmigrationmensuel_bme DROP CONSTRAINT IF EXISTS C_UK_bme;
  ALTER TABLE t_bilanmigrationjournalier_bjo DROP CONSTRAINT IF EXISTS c_uk_bjo;
  ALTER TABLE t_bilanmigrationmensuel_bme DROP CONSTRAINT IF EXISTS c_uk_bme;
  ALTER TABLE t_bilanmigrationjournalier_bjo ADD CONSTRAINT c_uk_bjo UNIQUE(bjo_dis_identifiant, bjo_tax_code, bjo_std_code, bjo_jour, bjo_labelquantite, bjo_org_code);
  ALTER TABLE t_bilanmigrationmensuel_bme ADD CONSTRAINT c_uk_bme UNIQUE(bme_dis_identifiant, bme_tax_code, bme_std_code, bme_annee, bme_mois, bme_labelquantite, bme_org_code);
');


--On met a jour la table ts_maintenance_main
SELECT ref.updatesql(
    (SELECT ARRAY(
        SELECT format('"%s"', nspname)  
        FROM pg_namespace n
        JOIN pg_roles r ON n.nspowner = r.oid
        WHERE n.nspname NOT LIKE 'pg_%' 
        AND n.nspname not in('information_schema','public','ref')
        
    )),
'

insert into ts_maintenance_main
(
  main_ticket,
  main_description
  ) values
  (2,''Update to version 0.6.0, unique key for t_bilanmigrationmensuel_bme and t_bilanmigrationjournalier_bjo'')
	ON CONFLICT (main_ticket) DO NOTHING;

');   
   

--================================================================================================
-- Création group_stacomi pour switcher dans stacoshiny d'un schéma à un autre sans se relogger
--================================================================================================
--on crée un rôle de groupe nommé group_stacomi
create role group_stacomi INHERIT NOLOGIN;

-- Méthode automatique pour donner les droits à group_stacomi sur l'ensemble des schémas de l'utilisateur
DO $$ 
DECLARE
    schema_name TEXT;
   	owner_name text;
    group_name TEXT := 'group_stacomi';
BEGIN
    -- Boucle sur tous les schémas appartenant à l'utilisateur
   FOR schema_name IN 
        SELECT nspname 
        FROM pg_namespace n
        JOIN pg_roles r ON n.nspowner = r.oid
        WHERE n.nspname NOT LIKE 'pg_%'  -- Exclure les schémas système commençant par 'pg_'
        AND n.nspname NOT IN ('information_schema','public','ref')
	
        
    loop
	    if schema_name='test' then owner_name='stacomi_test';
	    else owner_name=schema_name; 
	    end if;
        -- Appliquer les privilèges de base au schéma
        EXECUTE format('GRANT ALL ON SCHEMA %I TO group_stacomi', schema_name);
        -- On donne tous les droits à group_stacomi sur toutes les tables des schéma utilisateur
        EXECUTE format('GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA %I TO %I', owner_name, group_name);
        
        -- Réassigner les objets de l'utilisateur au groupe
        EXECUTE format('REASSIGN OWNED BY %I TO %I', owner_name, group_name);
        
        -- Accorder les droits au groupe sur tous les objets du schéma
        EXECUTE format('GRANT group_stacomi TO %I', owner_name);
        
    END LOOP;
  GRANT ALL ON SCHEMA "ref" TO group_stacomi;
  GRANT ALL ON ALL TABLES IN SCHEMA "ref" TO group_stacomi;
END $$;


-- below owned by just ensures that the sequence is dropped if the column is dropped also.
-- also use ref.updatesql to apply changes to a schema.
SELECT ref.updatesql(
    (SELECT ARRAY(
        SELECT format('"%s"', nspname)  
        FROM pg_namespace n
        JOIN pg_roles r ON n.nspowner = r.oid
        WHERE n.nspname NOT LIKE 'pg_%' 
        AND n.nspname not in('information_schema','public','ref')
        
    )),
'
ALTER TABLE t_bilanmigrationjournalier_bjo OWNER TO group_stacomi;
ALTER TABLE t_bilanmigrationmensuel_bme OWNER TO group_stacomi;
ALTER TABLE t_lot_lot OWNER TO group_stacomi;
ALTER TABLE t_operation_ope OWNER TO group_stacomi;
ALTER TABLE t_ouvrage_ouv OWNER TO group_stacomi;
ALTER TABLE tg_dispositif_dis OWNER TO group_stacomi;
ALTER TABLE tj_stationmesure_stm OWNER TO group_stacomi;
ALTER TABLE ts_maintenance_main OWNER TO group_stacomi;
ALTER TABLE ts_masque_mas OWNER TO group_stacomi;
ALTER TABLE ts_masquecaracteristiquelot_mac OWNER TO group_stacomi;
ALTER TABLE ts_masqueordreaffichage_maa OWNER TO group_stacomi;
ALTER SEQUENCE t_bilanmigrationjournalier_bjo_bjo_identifiant_seq OWNER TO group_stacomi;
ALTER SEQUENCE t_bilanmigrationjournalier_bjo_bjo_identifiant_seq OWNER TO group_stacomi;
ALTER SEQUENCE t_bilanmigrationmensuel_bme_bme_identifiant_seq OWNER TO group_stacomi;
ALTER SEQUENCE t_lot_lot_lot_identifiant_seq OWNER TO group_stacomi;
ALTER SEQUENCE t_operation_ope_ope_identifiant_seq OWNER TO group_stacomi;
ALTER SEQUENCE t_ouvrage_ouv_ouv_identifiant_seq OWNER TO group_stacomi;
ALTER SEQUENCE tg_dispositif_dis_dis_identifiant_seq OWNER TO group_stacomi;
ALTER SEQUENCE tj_stationmesure_stm_stm_identifiant_seq OWNER TO group_stacomi;
ALTER SEQUENCE ts_maintenance_main_main_identifiant_seq OWNER TO group_stacomi;
ALTER SEQUENCE ts_masque_mas_mas_id_seq OWNER TO group_stacomi;
ALTER SEQUENCE ts_masquecaracteristiquelot_mac_mac_id_seq OWNER TO group_stacomi;
ALTER SEQUENCE ts_masqueordreaffichage_maa_maa_id_seq OWNER TO group_stacomi;
');


SELECT ref.updatesql(
    (SELECT ARRAY(
        SELECT format('"%s"', nspname)  
        FROM pg_namespace n
        JOIN pg_roles r ON n.nspowner = r.oid
        WHERE n.nspname NOT LIKE 'pg_%' 
        AND n.nspname not in('information_schema','public','ref')
        
    )),
'
insert into ts_maintenance_main
(
  
  main_ticket,
  main_description
  ) SELECT
  22,''Mise à jour des droits avec un utilisateur de group_stacomi''
  ON CONFLICT (main_ticket) DO NOTHING;
  ;

');
