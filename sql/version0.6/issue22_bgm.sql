-- see https://forgemia.inra.fr/stacomi/stacomi_db/-/issues/22


ALTER ROLE bgm WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS ;


--create role group_stacomi_sequence NOINHERIT NOLOGIN; -- no inherit ensures that the privileges cannot be passed to another group role created later
--create role group_stacomi INHERIT NOLOGIN; -- group_stacomi will be able to inherit from privileges set in  group_stacomi_sequence

--GRANT group_stacomi TO bgm;
GRANT ALL ON SCHEMA bgm TO bgm;
GRANT SELECT ON ALL TABLES IN SCHEMA ref TO bgm;
GRANT ALL ON SCHEMA bgm TO admin_bgm;

ALTER TABLE bgm.t_operation_ope OWNER TO bgm;
ALTER TABLE bgm.t_lot_lot OWNER TO bgm;
ALTER TABLE bgm.t_bilanmigrationjournalier_bjo OWNER TO bgm;
ALTER TABLE bgm.t_bilanmigrationmensuel_bme OWNER TO bgm;
ALTER TABLE bgm.t_ouvrage_ouv OWNER TO bgm;
ALTER TABLE bgm.tg_dispositif_dis OWNER TO bgm;
ALTER TABLE bgm.tj_stationmesure_stm OWNER TO bgm;
ALTER TABLE bgm.t_dispositifcomptage_dic OWNER TO bgm;
ALTER TABLE bgm.t_dispositiffranchissement_dif OWNER TO bgm;
ALTER TABLE bgm.t_marque_mqe OWNER TO bgm;
ALTER TABLE bgm.t_operationmarquage_omq OWNER TO bgm;
ALTER TABLE bgm.t_station_sta OWNER TO bgm;
ALTER TABLE bgm.tj_actionmarquage_act OWNER TO bgm;
ALTER TABLE bgm.tj_caracteristiquelot_car OWNER TO bgm;
ALTER TABLE bgm.tj_coefficientconversion_coe OWNER TO bgm;
ALTER TABLE bgm.tj_conditionenvironnementale_env OWNER TO bgm;
ALTER TABLE bgm.tj_dfestdestinea_dtx OWNER TO bgm;
ALTER TABLE bgm.tj_dfesttype_dft OWNER TO bgm;
ALTER TABLE bgm.tj_pathologieconstatee_pco OWNER TO bgm;
ALTER TABLE bgm.tj_prelevementlot_prl OWNER TO bgm;
ALTER TABLE bgm.tj_tauxechappement_txe OWNER TO bgm;
ALTER TABLE bgm.ts_maintenance_main OWNER TO bgm;
ALTER TABLE bgm.ts_masque_mas OWNER TO bgm;
ALTER TABLE bgm.ts_masquecaracteristiquelot_mac OWNER TO bgm;
ALTER TABLE bgm.ts_masqueconditionsenvironnementales_mae OWNER TO bgm;
ALTER TABLE bgm.ts_masquelot_mal OWNER TO bgm;
ALTER TABLE bgm.ts_masqueope_mao OWNER TO bgm;
ALTER TABLE bgm.ts_masqueordreaffichage_maa OWNER TO bgm;
ALTER TABLE bgm.ts_taillevideo_tav OWNER TO bgm;
ALTER TABLE bgm.ts_taxonvideo_txv OWNER TO bgm;
GRANT ALL ON SCHEMA bgm TO group_stacomi;
REASSIGN OWNED BY bgm TO group_stacomi;
GRANT ALL ON ALL TABLES IN SCHEMA bgm TO bgm;
GRANT pg_read_server_files TO admin_bgm;


-- this was forgotten and caused the code to fail
GRANT group_stacomi TO bgm;

-- below owned by just ensures that the sequence is dropped if the column is dropped also.
-- also use ref.updatesql to apply changes to a schema.
select ref.updatesql('{"bgm"}',
'
ALTER SEQUENCE t_bilanmigrationjournalier_bjo_bjo_identifiant_seq OWNED BY t_bilanmigrationjournalier_bjo.bjo_identifiant_seq OWNER TO group_stacomi;
ALTER SEQUENCE t_bilanmigrationmensuel_bme_bme_identifiant_seq OWNED BY t_bilanmigrationmensuel_bme.bme_identifiant_seq OWNER TO group_stacomi;
ALTER SEQUENCE t_lot_lot_lot_identifiant_seq OWNED BY t_lot_lot.lot_identifiant_seq OWNER TO group_stacomi;
ALTER SEQUENCE t_operation_ope_ope_identifiant_seq OWNED BY t_operation_ope.ope_identifiant_seq OWNER TO group_stacomi;
ALTER SEQUENCE t_ouvrage_ouv_ouv_identifiant_seq OWNED BY t_ouvrage_ouv.ope_identifiant_seq OWNER TO group_stacomi;
ALTER SEQUENCE tg_dispositif_dis_dis_identifiant_seq OWNED BY tg_dispositif_dis.dis_identifiant_seq OWNER TO group_stacomi;
ALTER SEQUENCE tj_stationmesure_stm_stm_identifiant_seq OWNED BY tj_stationmesure_stm.stm_identifiant_seq OWNER TO group_stacomi;
ALTER SEQUENCE ts_maintenance_main_main_identifiant_seq  OWNED BY ts_maintenance_main.main_identifiant_seq OWNER TO group_stacomi;
ALTER SEQUENCE ts_masque_mas_mas_id_seq  OWNED BY ts_masque_mas.mas_id_seq OWNER TO group_stacomi;
ALTER SEQUENCE ts_masquecaracteristiquelot_mac_mac_id_seq  OWNED BY ts_masquecaracteristiquelot_mac.mac_mac_id OWNER TO group_stacomi;
ALTER SEQUENCE ts_masqueordreaffichage_maa_maa_id_seq  OWNED BY ts_masqueordreaffichage_maa.maa_id_seq OWNER TO group_stacomi;
')



select ref.updatesql('{"bgm"}',
'
insert into ts_maintenance_main
(
  
  main_ticket,
  main_description
  ) SELECT
  22,''Mise � jour des droits avec un utilisateur de groupe''
  WHERE NOT EXISTS (
    SELECT 1 FROM ref.ts_maintenance_main WHERE main_ticket=22)
  ;

');



-- bgm is not the owner of the sequence bd_contmig_nat_bgm the owner is admin_bgm 

SELECT d.datname as "Name",
pg_catalog.pg_get_userbyid(d.datdba) as "Owner"
FROM pg_catalog.pg_database d
ORDER BY 1;

select * from pg_tables where tablename = 't_operation_ope';

-- owner is group stacomi, same for the sequence 

GRANT group_stacomi TO bgm;

